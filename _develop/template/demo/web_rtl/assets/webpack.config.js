const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: {
        app: [
            './js/main/bootstrap.min.js',
            './js/main/owl.carousel.min.js',
            './js/main/fontawesome-all.min.js',
            './js/main/components.js',
            './js/layouts.js',
            './js/home.js',
            './js/list.js',
            // './js/single.js',
            // './js/checkout.js',
        ]
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.bundle.js',
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
        })
    ]
};