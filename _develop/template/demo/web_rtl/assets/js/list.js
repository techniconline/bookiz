// var mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');

(function(page) { 
    // console.log('mapboxgl', mapboxgl)
    // const access_token = 'pk.eyJ1IjoiYWxpbTEyNSIsImEiOiJjanM0dHdxMGswODA4NDRvM2t6czRldmRwIn0.s8f6OrxjWOmZOOm-EL-9gg';
    // mapboxgl.accessToken = access_token;
    // var map = new mapboxgl.Map({
    //     container: 'map',
    //     style: 'mapbox://styles/mapbox/streets-v11',
    //     center: [-77.04, 38.907],
    //     zoom: 11.15
    // });  

    if(typeof mapboxgl !== 'undefined' && $('#map').length === 1) {
        // const access_token = 'pk.eyJ1IjoiYWxpbTEyNSIsImEiOiJjanM0dHdxMGswODA4NDRvM2t6czRldmRwIn0.s8f6OrxjWOmZOOm-EL-9gg';
        // const client = new MapboxClient(access_token);
        mapboxgl.accessToken = 'pk.eyJ1IjoiYWxpbTEyNSIsImEiOiJjanM0dHdxMGswODA4NDRvM2t6czRldmRwIn0.s8f6OrxjWOmZOOm-EL-9gg';
        var map = new mapboxgl.Map({
            container: 'map', 
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [-77.04, 38.907],
            zoom: 11.15
        });

        var geojson = {
            "type": "FeatureCollection",
            "features": [{
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [-77.04, 38.907]
                }
            }]
        };
        console.log('loaded');
        map.on('load', function() {
            
            map.addSource('point', {
                "type": "geojson",
                "data": geojson
            });

            map.addLayer({
                "id": "point",
                "type": "circle",
                "source": "point",
                "paint": {
                    "circle-radius": 8,
                    "circle-color": "#F06292"
                }
            });

            map.on('click', 'point', function() {
                console.log('clicked')
            });

        });
    }
})('list-page');