(function(page) {
    const p = '.' + page + ' ';

    /* ===== Category Services ===== */
    var category = $(p+'.categories li');
    category.click(function(){
        var type = $(this).data('type');
        var content = $('.category-contents .category-content');
        category.removeClass('active');
        content.removeClass('active');
        $(this).addClass('active');
        content.each(function() {
            if($(this).data('category') === type) {
                $(this).addClass('active');
            }
        });
    });

    /* ===== Basket ===== */
    let basket = [];
    let totla_price = 0;

    $(p+'.services-list li a').click(function (e) {
        e.preventDefault();
        const id = $(this).parent().data('id');
        const price = Number($(this).parent().data('price'));
        const ind = basket.indexOf(id);
        if(ind === -1) {
            basket.push(id);
            $(p+'.services-list li[data-id="'+id+'"]').each(function () {
               $(this).addClass('selected');
            });
            totla_price += price;
        }else {
            basket.splice(ind, 1);
            $(p+'.services-list li[data-id="'+id+'"]').each(function () {
                $(this).removeClass('selected');
            });
            totla_price -= price;
        }

        $(p+'.basket .price').text(totla_price + '$');
        $(p+'.basket .count').text(basket.length);
        $(p+'.basket').addClass('selected');
        if(basket.length > 0 ) {
        }else {
            $(p+'.basket').removeClass('selected');
        }
    });

})('single-page');