(function(page) {
    const p = '.' + page + ' ';
    const tab = $(p + '.search-box .tabs li');
    const box = $(p + '.search-box .box');
    tab.click(function() {
        tab.removeClass('active');
        const type = $(this).data('type');
        $(this).addClass('active');
        box.each(function() {
            console.log('box', $(this).data('type'));
            if(type === $(this).data('type')) {
                box.removeClass('active');
                $(this).addClass('active');
            }
        });

    });
})('home-page');