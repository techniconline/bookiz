(function(page) {
    const p = '.' + page + ' ';

    /* ===== Category Services ===== */
    var select_time = $(p+'.select-times li');
    select_time.click(function(){
        select_time.removeClass('active');
        $(this).addClass('active');
    });

    // Create the calendar
    const calendarEl = document.getElementById("date-calendar");
    
    // Get Selected Dates By HTML
    let selected_dates = $("#date-calendar").data('dates');
    $("#date-calendar").removeAttr('data-dates');

    if(calendarEl) {
        const calendar = jsCalendar.new(calendarEl);
        if(selected_dates.length > 0) 
            calendar.set(selected_dates[0]);
            
        calendar.select(selected_dates);

        // Get Selected Dates By AJAX
        $.get("url").done(function(result) {
            selected_dates=[result];
            }).fail(function(error) {
              console.log("error", error);
            });

        // Add events
        calendar.onDateClick(function(event, date){
            // Update calendar date
            console.log('date click', event, date); 
            let day = date.getDate();            
            day = (day < 10) ? '0'+day : day;
            let month = date.getMonth()+1;            
            month = (month < 10) ? '0'+month : month;
            let year = date.getFullYear();            
            year = (year < 10) ? '0'+year : year;
            if(selected_dates.indexOf(day+'/'+month+'/'+year) !== -1) {
                calendar.set(date);
            }else {
                const content = `This date is not set. Please choose other date.`;
                $(".toast").html(content);
                $(".toast").addClass('active');
                setTimeout(function(){
                    $(".toast").removeClass('active');
                }, 2000);
            }
        });
    }

})('checkout-page');

(function(page) {
    const p = '.' + page + ' ';

    $(p+'#forYou input').change(function(e) {
        console.log(e.target.value);
        if(Number(e.target.value) === 1) {
            $(p+'.guest-name').show();
        }else {
            $(p+'.guest-name').hide();
        }
    });

})('checkout-step2');
