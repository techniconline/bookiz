
/* ===== Navigation Home Burger ===== */
$('.home-page .navigation .toggle-menu').click(function() {
    const menu = $(this).next();
    menu.toggleClass('active');
    menu.click(function() {
       $(this).removeClass('active');
    });
});

/* ===== Header Scrolling ===== */
$(document).scroll(function(e){
    const headerContainer = $('.header-container');
    if(headerContainer && headerContainer.offset()) {
        if($(document).scrollTop() > 200){
            headerContainer.addClass('scroll-down');
        }else{
            headerContainer.removeClass('scroll-down');
        }
    }
});

/* ===== Header Page Account ===== */
$('.header-container .account').click(function() {
    $(this).toggleClass('active');
});

/* ===== Menu and Submenu ===== */
let menuHovered = false;
$('.header-container .menu-container .menu li').mouseover(function() {
    
    $(this).parent().find('.submenu').removeClass('active');
    if($(this).find('.submenu').length > 0) {
        menuHovered = true;    
        $(this).find('.submenu').addClass('active');
        setTimeout(() => {
            $(this).parent().find('.submenu').css({'opacity': 0});
            $(this).find('.submenu').css({'opacity': 1});
        }, 100);
        if(menuHovered && $('body .menu-layout').length === 0) $('body').append('<div class="menu-layout"></div>');

        $(this).mouseout(function() {
            menuHovered = false;
            setTimeout(() => {
                if(!menuHovered) {
                    $(this).find('.submenu').removeClass('active');
                    $(this).find('.submenu').css({'opacity': 0});
                    $('body .menu-layout').remove();
                }
            }, 200);
        });
    } 
    
});
