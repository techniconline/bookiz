<div style="width:600px; height:170px; margin:0px auto; border:1px solid #e7e7e7 ; border-bottom:0px; background-image:url('{{ url('/themes/default/email/Mail-Header.jpg') }}'); text-align:center;">
    <a href="{{ $url }}" target="_blank">
        <img src="{{ url('/themes/default/email/logo.png') }}" style="margin:0 auto; margin:10px 0; width: 120px" />
    </a>
    <h1 style="text-align: center; font-size:12px; font-weight:bold;font-family:tahoma; margin-top:10px; color:#FFF">
        {{ $slot }}
    </h1>
</div>