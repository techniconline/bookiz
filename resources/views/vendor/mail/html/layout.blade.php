<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <style>
        @media only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>

    <table class="wrapper" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <table class="content" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="header" style="padding-bottom: 0px">
                            {{ $header or '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="width:600px; margin:0px auto; padding:10px 0px; border:1px solid #e7e7e7 ; border-top:0px; border-bottom: 0px; background-color:#f5f5f5;  font-size:12px; font-family:tahoma;  line-height:1.5; direction:rtl; text-align:justify;">
                                <div style="width:90% ; margin:10px 3% ; background-color:#FFF; padding:10px">
                                    <span style="width:100%; float:right; text-align:center;">
                                        <img src="{{ url('/themes/default/email/pic1.jpg') }}" style="margin:0 auto; margin:10px 0;" />
                                    </span>
                                        {{ Illuminate\Mail\Markdown::parse($slot) }}
                                        {{ $subcopy or '' }}
                                    <br style="clear:both;" />
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="footer">
                            {{ $footer or '' }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
