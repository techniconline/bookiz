<div style="width:600px; margin:0px auto; padding:10px 0px; background-color:#f5f5f5; border:1px solid #e7e7e7 ; border-top:0px;  font-size:12px; font-family:tahoma;  line-height:1.5; direction:rtl; text-align:justify;">
    <div style="width:90% ; margin:10px 3% ; background-color:#FFF; padding:10px; text-align:center; direction:rtl;">

        <a href="https://t.me/techniconline" title="telegram" target="_blank"><img src="{{ url('/themes/default/email/icon3.png') }}" style="margin:0 auto; margin:4px  ;" /></a>
        <a href="https://www.instagram.com/techniconline/" title="instagram" target="_blank"><img src="{{ url('/themes/default/email/icon1.png') }}" style="margin:0 auto; margin:4px  ;" /></a>
    </div>

    {{--<div style="width:90% ; margin:10px 3% ;   padding:10px; text-align:center; direction:rtl;">--}}

    {{--<a href="#" target="_blank"><img src="{{ url('/themes/default/email/icon5.png') }}" style="margin:0 auto; margin:4px  ;" /></a>--}}
    {{--<a href="#" target="_blank"><img src="{{ url('/themes/default/email/icon6.png') }}" style="margin:0 auto; margin:4px  ;" /></a>--}}

    {{--</div>--}}
    <div style="width:90% ; margin:10px 3% ;   padding:10px; text-align:center; direction:ltr;">
        {{ $slot }}
    </div>

</div>