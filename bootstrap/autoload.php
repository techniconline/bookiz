<?php

use Carbon\Carbon;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Theme;


$user_agent = $_SERVER['HTTP_USER_AGENT'];

/**
 * @return mixed|string
 */
function getOS() {

    global $user_agent;

    $os_platform  = "Unknown OS Platform";

    $os_array     = array(
        '/windows nt 10/i'      =>  'Windows 10',
        '/windows nt 6.3/i'     =>  'Windows 8.1',
        '/windows nt 6.2/i'     =>  'Windows 8',
        '/windows nt 6.1/i'     =>  'Windows 7',
        '/windows nt 6.0/i'     =>  'Windows Vista',
        '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
        '/windows nt 5.1/i'     =>  'Windows XP',
        '/windows xp/i'         =>  'Windows XP',
        '/windows nt 5.0/i'     =>  'Windows 2000',
        '/windows me/i'         =>  'Windows ME',
        '/win98/i'              =>  'Windows 98',
        '/win95/i'              =>  'Windows 95',
        '/win16/i'              =>  'Windows 3.11',
        '/macintosh|mac os x/i' =>  'Mac', //'Mac OS X',
        '/mac_powerpc/i'        =>  'Mac', //'Mac OS 9',
        '/linux/i'              =>  'Linux',
        '/ubuntu/i'             =>  'Ubuntu',
        '/iphone/i'             =>  'iOS', //'iPhone',
        '/ipod/i'               =>  'iOS', //'iPod',
        '/ipad/i'               =>  'iOS', //'iPad',
        '/android/i'            =>  'Android',
        '/blackberry/i'         =>  'BlackBerry',
        '/webos/i'              =>  'Mobile'
    );

    foreach ($os_array as $regex => $value){
        if (preg_match($regex, $user_agent))
            $os_platform = $value;
    }

    return $os_platform;
}

/**
 * @return mixed|string
 */
function getBrowser() {

    global $user_agent;

    $browser        = "Unknown Browser";

    $browser_array = array(
        '/msie/i'      => 'Internet Explorer',
        '/firefox/i'   => 'Firefox',
        '/safari/i'    => 'Safari',
        '/chrome/i'    => 'Chrome',
        '/edge/i'      => 'Edge',
        '/opera/i'     => 'Opera',
        '/netscape/i'  => 'Netscape',
        '/maxthon/i'   => 'Maxthon',
        '/konqueror/i' => 'Konqueror',
        '/mobile/i'    => 'Handheld Browser'
    );

    foreach ($browser_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $browser = $value;

    return $browser;
}

function asset($url, $secure = null)
{
    $instanceObject = app('getInstanceObject');
    $instance = $instanceObject->getInstance();
    $language = $instanceObject->getLanguage();

    $url = app('url')->asset($url, $secure);

    $url = str_replace(request()->getScheme() . "://", "", $url);
    $url = str_replace(request()->getHost(), "", $url);
    $url = implode("/", array_values(array_filter(explode("/", $url))));
    $urlArr = explode("/", $url);

    $hasLang = false;
    $instanceSegment = 0;
    if (isset($urlArr[0]) && strlen($urlArr[0]) == 2) {
        $hasLang = true;
        $instanceSegment = 1;
    }


    $url = collect($urlArr)->filter(function ($item, $index) use ($instance, $language, $hasLang, $instanceSegment) {

        if ($index > $instanceSegment)
            return $item;

        if (!$index && strlen($item) == 2) {
            return null;
        }

        if ($index == $instanceSegment && $item == $instance->name) {
            return null;
        }

        return $item;

    })->values()->implode("/");

    $url = (request()->getScheme() . "://" . request()->getHost() . "/" . $url);

    return $url;
}


function view($view = null, $data = [], $mergeData = [])
{
    $factory = app(ViewFactory::class);
    if (func_num_args() === 0) {
        return $factory;
    }
    $theme=app('getInstanceObject')->getInstanceTheme();
    if($theme && Theme::exists($theme->code)){
        $themeView=str_replace('::','.',$view);
        $themePath=base_path(Theme::path($theme->code)).DIRECTORY_SEPARATOR.'renderer';
        $themeViewPath=$themePath.DIRECTORY_SEPARATOR.str_replace('.',DIRECTORY_SEPARATOR,$themeView).'.blade.php';
        if(File::exists($themeViewPath)){
            $factory->addNamespace('theme', $themePath);
            return $factory->make('theme::'.$themeView, $data, $mergeData);
        }
    }
    return $factory->make($view, $data, $mergeData);
}

if (!function_exists('getTimestampToJalali')) {

    function getTimestampToJalali($timestamp = null, $format = '%Y/%m/%d')
    {
        $timestamp = $timestamp ? $timestamp : now();
        return jdate($timestamp)->format($format);
    }
}

if (!function_exists('get_instance')) {

    function get_instance()
    {
        return app('getInstanceObject');
    }
}

if (!function_exists('getJalaliDateToTimestamp')) {

    function getJalaliDateToTimestamp($str, $time = '00:00:00', $separator = '/')
    {
        $date = getDateToArray($str, $separator);
        $gDate = \Morilog\Jalali\jDateTime::toGregorian($date['year'], $date['month'], $date['day']);
        return \Morilog\Jalali\jDate::forge($gDate[0] . '-' . $gDate[1] . '-' . $gDate[2] . ' ' . $time)->time();
    }
}

if (!function_exists('getJalaliByField')) {

    function getJalaliByField($field = null, $timestamp = null)
    {
        $date = getTimestampToJalali($timestamp);
        $dateArray = getDateToArray($date, '/');
        if ($field) {
            return $dateArray[$field];
        }
        return $dateArray;
    }
}

if (!function_exists('getDateTimeFromTimestamp')) {

    function getDateTimeFromTimestamp($timestamp)
    {
        $timestamp = $timestamp ? $timestamp : now();
        return Carbon::createFromTimestamp($timestamp)->toDateTimeString();
    }
}

if (!function_exists('getDateToArray')) {

    function getDateToArray($str, $separator = '/', $rtl = false)
    {
        $dateArray = explode($separator, $str);
        if (!isset($dateArray[0], $dateArray[1], $dateArray[1])) {
            return getJalaliByField();
        }
        if ($rtl) {
            $date = ['year' => $dateArray[2], 'month' => $dateArray[1], 'day' => $dateArray[0]];
        } else {
            $date = ['year' => $dateArray[0], 'month' => $dateArray[1], 'day' => $dateArray[2]];

        }
        return $date;
    }
}

/**
 * @param $string
 * @return array[]|false|string[]
 */
function splitAtUpperCase($string) {
    return preg_split('/(?=[A-Z])/', $string, -1, PREG_SPLIT_NO_EMPTY);
}

/**
 * @param $string
 * @return string
 */
function get_lowercase_separated_by_underscores($string){
    $arr = splitAtUpperCase($string);
    $string='';
    foreach ($arr as $value){
        if(!empty($value)){
            $string.=strtolower($value).'_';
        }
    }
    return rtrim($string,'_');
}

/**
 * @param $string
 * @return string
 */
function get_uppercase_by_underscores($string){
    $arr = explode('_',$string);
    $string='';
    foreach ($arr as $value){
        if(!empty($value)){
            $string.=ucfirst($value);
        }
    }
    return $string;
}

function create_alias($string)
{
    $string = trim($string);

    if (ctype_digit($string)) {
        return $string;
    } else {
        // replace accented chars
        $accents = '/&([A-Za-z پچجحخهیعغفقثصضآشسیكبيلاتنکمکگژیوئدذرزطظ ]{1,2})(grave|acute|circ|cedil|uml|lig);/';
        $string_encoded = htmlentities($string, ENT_COMPAT, 'UTF-8');

        $string = preg_replace($accents, '$1', $string_encoded);

        // clean out the rest
        //Type A: Remove all duplicated character placements like "this-is-idollobicom"

        $replace = array('([\40])', '([^a-zA-Z0-9- پچجحخهیعغفقثصضآشسیكبيلاتنکمکگژیوئدذرزطظ ])', '(-{2,})');
        $with = array('-', '', '-');

        // Type B: reserve all character placements like "this-is--idollobi-com-"

        $replace = array('([\40])', '([^a-zA-Z0-9- پچجحخهیعغفقثصضآشسیكبيلاتنکمکگژیوئدذرزطظ ])');
        $with = array('-', '-');
        $string = preg_replace($replace, $with, $string);


        $string = str_replace(array(" ", "/", ".", "?", "#"), array("-"), $string);
        $string = str_replace('--', "", $string);
    }

    return $string;

}

function create_tags($string)
{
    $string = trim($string);

    if (ctype_digit($string)) {
        return $string;
    } else {
        // replace accented chars
        $accents = '/&([A-Za-z پچجحخهیعغفقثصضآشسیكبيلاتنکمکگژیوئدذرزطظ ]{1,2})(grave|acute|circ|cedil|uml|lig);/';
        $string_encoded = htmlentities($string, ENT_COMPAT, 'UTF-8');

        $string = preg_replace($accents, '$1', $string_encoded);

        // clean out the rest
        //Type A: Remove all duplicated character placements like "this-is-idollobicom"

        $replace = array('([\40])', '([^a-zA-Z0-9- پچجحخهیعغفقثصضآشسیكبيلاتنکمکگژیوئدذرزطظ ])', '(-{2,})');
        $with = array(',', '', ',');

        // Type B: reserve all character placements like "this-is--idollobi-com-"

        $replace = array('([\40])', '([^a-zA-Z0-9- پچجحخهیعغفقثصضآشسیكبيلاتنکمکگژیوئدذرزطظ ])');
        $with = array(',', ',');
        $string = preg_replace($replace, $with, $string);


        $string = str_replace(array(" ", "/", ".", "?", "#"), array(","), $string);
        $string = str_replace(',,', "", $string);
    }

    if ($string) {
        $strArr = explode(",", $string);
        $newStrArr = null;
        foreach ($strArr as $item)
            if (strlen($item) > 4) {
                $newStrArr[] = $item;
            }

        if ($newStrArr) {
            $string = implode(",", $newStrArr);
        } else {
            return null;
        }
    }

    return ($string);

}
