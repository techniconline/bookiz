(function (page) {
    const p = '.' + page + ' ';

    // Get Selected Dates By HTML
    var selected_dates = [];
    var validTime = [];

    // SELECT SERVICE and SHOW Employees
    var employeesSelectBox = $(p + '#employees');
    var $time_list = $(p + 'ul#time_list');
    var select_time_service = $(p + 'a._service_row');
    var $parent = null;
    var $employee_selected = null;

    employeesSelectBox.change(function (event) {
        // event.preventDefault();
        if (!$parent) {
            alert("Select Service! Please.");
        }
        var $selected = $(this);
        $employee_selected = $selected.val();
        console.log($employee_selected);
        $parent.find('input._employee').val($employee_selected);
    });

    // console.log(select_time_service);
    select_time_service.click(function (event) {
        event.preventDefault();
        select_time_service.removeClass('active');
        $(this).addClass('active');
        select_time_service = $(this);
        $time_list.find('li').remove();

        $parent = select_time_service.parents('li').first();

        var $service_id = select_time_service.attr("data-id");
        var $data_employees = $employees[$service_id];
        var $data_calendar = $calendar[$service_id];
        selected_dates = [];
        validTime = [];
        $.each($data_calendar, function ($i, $times) {
            validTime[$i] = $times;
            var $arr = $i.split("-");
            $i = $arr.reverse().join("/");
            selected_dates.push($i);
        });
        calendar.clearSelected();
        calendar.select(selected_dates);
        calendar.isVisible(selected_dates[0]);

        if (selected_dates.length > 0)
            calendar.set(selected_dates[0]);


        employeesSelectBox.find('option').not('._except').remove();
        $.each($data_employees, function ($item, $value) {
            $("<option value='" + $item + "'>" + $value + "</option>").appendTo(employeesSelectBox);
        });
    });

    /* ===== Category Services ===== */
    // var select_time = $(p + '.select-times li');
    $time_list.on('click', 'li', function () {
        var $clicked = $(this);
        console.log($clicked);
        $time_list.find('li').removeClass('active');
        $time_list.find('li .fa-check').remove();
        $('<i class="fa fa-check"></i>').appendTo($clicked);
        $clicked.addClass('active');
        var $time = $clicked.attr("data-time");
        var $date = $clicked.attr("data-date");
        $parent.find('input._time').val($time);
        $parent.find('input._date').val($date);
    });

    // Create the calendar
    const calendarEl = document.getElementById("date-calendar");

    $("#date-calendar").removeAttr('data-dates');

    if (calendarEl) {
        var calendar = jsCalendar.new({
            target: calendarEl,
            //date : "30/01/2017",
            navigator: true,
            navigatorPosition: "right",
            zeroFill: true,
            monthFormat: "month YYYY",
            dayFormat: "DDD",
            language: "fa"
        });

        if (selected_dates.length > 0)
            calendar.set(selected_dates[0]);

        //calendar.select(selected_dates);

        // Get Selected Dates By AJAX
        // $.get("url").done(function(result) {
        //     selected_dates=[result];
        // }).fail(function(error) {
        //     console.log("error", error);
        // });

        // Add events
        calendar.onDateClick(function (event, date) {
            // Update calendar date
            // console.log('date click', event, date);
            // console.log($parent);

            let day = date.getDate();
            day = (day < 10) ? '0' + day : day;
            let month = date.getMonth() + 1;
            month = (month < 10) ? '0' + month : month;
            let year = date.getFullYear();
            year = (year < 10) ? '0' + year : year;
            if (selected_dates.indexOf(day + '/' + month + '/' + year) !== -1) {
                calendar.set(date);

                var $key = year + '-' + month + '-' + day;
                if (validTime.indexOf($key)) {
                    $time_list.find('li').remove();
                    var $times = validTime[$key];
                    // console.log($times);
                    $.each($times, function ($i, $value) {
                        // console.log($i, $value);
                        if ($value.valid) {
                            $('<li data-date="' + $key + '" data-time="' + $i + '" ><span class="time">' + $i +  '</span> </li>').appendTo($time_list);
                        }
                    });

                }

                //console.log(calendar.getSelected({sort : "desc", type : "DD-MM-YYYY"}));
            } else {
                const content = `This date is not set. Please choose other date.`;
                $(".toast").html(content);
                $(".toast").addClass('active');
                setTimeout(function () {
                    $(".toast").removeClass('active');
                }, 2000);
            }
        });
    }

})('checkout-page');

(function (page) {
    const p = '.' + page + ' ';

    $(p + '#forYou input').change(function (e) {
        console.log(e.target.value);
        if (Number(e.target.value) === 1) {
            $(p + '.guest-name').show();
        } else {
            $(p + '.guest-name').hide();
        }
    });

})('checkout-step2');