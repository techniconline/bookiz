(function ($) {

    $.fn.runCounter = function (options) {
        var defaults = {
            start: 1,
            end: 100000,
            duration: 5
        };

        var opt = $.extend(defaults, options);

        return this.each(function () {
            var $para = $(this);
            $({ ctr: opt.start }).animate({ ctr: opt.end }, {
                duration: opt.duration,
                easing: 'linear',
                step: function () {
                    $para.text(Math.round(this.ctr));
                }
            });
        });
    }

})(jQuery);


jQuery(document).ready(function() {
    if (jQuery(".counter-runner").length > 0){
        $('.counter-runner').each(function (index) {
            var id='#'+$(this).attr('id');
            var start=parseInt($(this).attr('data-start'));
            var end=parseInt($(this).attr('data-end'));
            var duration=parseInt($(this).attr('data-duration'));
            if(duration){
                duration=duration*1000;
            }else{
                duration=5000;
            }
            $(id).runCounter({ start:start, end: end, duration: duration });
        });
    }
})