@auth
    @if($user=auth()->user())
        @if(isset($user->confirm) && !$user->confirm)
            <div class="FullWidth verify-danger">
                <div class="container verify-container">
                    <div class="col-lg-12 col-md-12 verify-box">
                        <p>
                            @lang('user::message.user.verify_message')
                            <a class="btn btn-danger" href="{{ route('user.verify') }}">
                                @lang('user::message.user.verify_account')
                            </a>
                        </p>
                    </div>
                </div>

            </div>
         @endif
    @endif
@endauth
