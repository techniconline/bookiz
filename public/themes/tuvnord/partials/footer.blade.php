<footer class=" FullWidth  floatright mrgtop30   Foter-V2 ">
    @if(Theme::hasArgument('special_widgets.footer'))
        @foreach(Theme::getContentArgument('special_widgets.footer') as $position => $values)
            @foreach($values as $value)
                {!! $value !!}
            @endforeach
        @endforeach
    @endif
</footer>