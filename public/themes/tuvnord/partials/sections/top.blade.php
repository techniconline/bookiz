@if(Theme::hasArgument('widgets.top'))
    <section id="_top">
        @foreach(Theme::getContentArgument('widgets.top') as $value)
            <div>
                {!! $value !!}
            </div>
        @endforeach
    </section>
@endif