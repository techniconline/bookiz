@partial('component.error')
@if(Theme::hasArgument('widgets.center'))
	{{--<section id="_main">--}}
		@foreach(Theme::getContentArgument('widgets.center') as $value)
			{{--<div>--}}
				{!! $value !!}
			{{--</div>--}}
		@endforeach
	{{--</section>--}}
	@elseif(Theme::hasArgument('widgets.content'))
		@foreach(Theme::getContentArgument('widgets.content') as $value)
			{!! $value !!}
		@endforeach
@endif