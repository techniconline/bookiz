@if(Theme::hasArgument('widgets.left'))
    <section id="_left">
        @foreach(Theme::getContentArgument('widgets.left') as $value)
            <div>
                {!! $value !!}
            </div>
        @endforeach
    </section>
@endif