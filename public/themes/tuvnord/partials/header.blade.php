@partial('component.verify')
@partial('component.app_download')
<header class="FullWidth" id="Header">
        @if(Theme::hasArgument('special_widgets.header'))
            @foreach(Theme::getContentArgument('special_widgets.header') as $position => $values)
                @foreach($values as $value)
                    {!! $value !!}
                @endforeach
            @endforeach
        @endif
</header>