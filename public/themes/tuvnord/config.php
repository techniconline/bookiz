<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists, this
    | is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities this is cool
    | feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => array(

        'before' => function ($theme) {
            $theme->setTitle('Techniconline');
            $theme->setAuthor('Techniconline');
        },

        'asset' => function ($asset) {

            $language = app('getInstanceObject')->getLanguage();
            $direction = $language ? $language->direction : null;

            $path = 'themes/default/assets/';
//            $asset->container('up_theme_js')->themePath()->add('app.bundle', 'dist/app.bundle.js?v=1.js');
//            $asset->container('up_theme_js')->add('bootstrap_js', asset( $path.'js/bootstrap.min.js'));
//            $asset->container('up_theme_js')->add('owl.carousel', asset( $path.'global/plugins/owlcarousel/owl.carousel.min.js'));
            $asset->container('up_theme_js')->add('jquery', asset('themes/admin/assets/global/plugins/jquery.min.js'));

            $asset->add('sweetalert', asset('/assets/global/plugins/sweetalert/sweetalert2.min.css'));
            $asset->container('up_theme_js')->add('sweetalert', asset('/assets/global/plugins/sweetalert/sweetalert2.min.js'));

            $asset->themePath()->add([

                ['carousel', 'dist/css/owl.carousel.min.css'],
                ['default', 'dist/css/owl.theme.default.min.css'],
                ['fontawesome', 'fontawesome/css/all.css'],
                ['fontiran', 'fonts/fontiran.css'],
//                ['css_app', 'dist/css/app.css?v=1.css'],
                ['js_app', 'dist/app.bundle.js?v=1.js'],
            ]);

//            $asset->add('js-scripts', '/js/scripts.js');
            if (strtolower($direction) == "rtl") {
//                $asset->add('components', asset('themes/admin/assets/global/css/components-rtl.min.css'));
                $asset->themePath()->add([
//                    ['fontiran', 'fonts/fontiran.css'],
                    ['css_app', 'dist/css/app_rtl.css?v=1.css'],
                    ]);
            } else {
                $asset->themePath()->add([
//                    ['fontawesome', 'fontawesome/css/all.css'],
                    ['css_app', 'dist/css/app.css?v=1.css'],
                ]);
            }

            $asset->cook('backbone', function ($asset)  {
                //$asset->add('googleapis', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false');

                //TODO
//                $asset->container('up_theme_js')->add('googleapis', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDLJkvq8VOv1ZUpn9zTTV2ckN-Ki4yt6oM');
                $asset->container('up_theme_js')->add('map-box-api', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js');
                $asset->container('theme_style')->add('map-box-api-css','https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css');

//                $asset->container('up_theme_js')->add('map-box-es6', 'https://unpkg.com/es6-promise@4.2.4/dist/es6-promise.auto.min.js');
//                $asset->container('up_theme_js')->add('map-box-sdk', 'https://unpkg.com/@mapbox/mapbox-sdk/umd/mapbox-sdk.min.js');

            });

            //$asset->container('inline')->add('sample', asset('assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css'));

            // You may use elixir to concat styles and scripts.

            /*$asset->themePath()->add([
                                        ['styles', 'dist/css/styles.css'],
                                        ['scripts', 'dist/js/scripts.js']
                                     ]);*/


            // Or you may use this event to set up your assets.
            /*
            $asset->themePath()->add('core', 'core.js');
            $asset->add([
                            ['jquery', 'vendor/jquery/jquery.min.js'],
                            ['jquery-ui', 'vendor/jqueryui/jquery-ui.min.js', ['jquery']]
                        ]);
            */
        },


        'beforeRenderTheme' => function ($theme) {
            $theme->asset()->serve('backbone');
            // To render partial composer
            /*

            $theme->partialComposer('header', function($view){
                $view->with('auth', Auth::user());
            });
            */
        },

        'beforeRenderLayout' => array(

            'mobile' => function ($theme) {
                // $theme->asset()->themePath()->add('ipad', 'css/layouts/ipad.css');
            }

        )

    )

);