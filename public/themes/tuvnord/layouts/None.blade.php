@if(Theme::hasArgument('widgets.content'))
    @foreach(Theme::getContentArgument('widgets.content') as $value)
        {!! $value !!}
    @endforeach
@endif