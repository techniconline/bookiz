<!DOCTYPE html>
<html lang="{!! get_instance()->getLanguageCode() !!}" dir="{!! get_instance()->getLanguageDirection() !!}">
<head>
    @partial('head')
</head>

<body>
@partial('header')
@partial('sections.top')
<div class="FullWidth _faq2 ">
    <div class="container">
        @php $user=BridgeHelper::getAccess()->getUser(); @endphp
        @if($user)
        <div class="col-xl-3 col-lg-3 col-md-4  floatright dash5     ">
            <div class="col-xl-12 col-lg-12 col-md-12  floatright dash2    ">
                <div class="dash3">
                    <img src="{{ $user->small_avatar_url }}" />
                    <h1>
                        {{ $user->first_name.' '.$user->last_name }}
                    </h1>
                </div>

            </div>
            <div class="  floatright dash4    ">
                <ul>
                    <li> <a href="{!! route("course.user.my.courses") !!}"> <i class="fas fa-window-restore"></i>@lang("course::course.my_courses")</a></li>
                    <li> <a href="{!! route("sale.order.user") !!}"> <i class="fas fa-shopping-cart"></i> @lang("sale::sale.my_orders") </a></li>
                    <li> <a href="{!! route("user.profile.edit") !!}"> <i class="fas fa-pen-square"></i>@lang("user::form.titles.profile")</a></li>
                    <li> <a href="{!! route("user.profile.qrcode_login") !!}"><i class="fa fa-qrcode"></i>@lang("user::form.titles.qrcode_login")</a></li>
                    <li> <a href="{!! route("user.profile.password.form") !!}"> <i class="fas fa-credit-card"></i> @lang("user::form.titles.change_password") </a></li>
                    <li>
                        <a href="{!! route('logout') !!}"   onclick="event.preventDefault(); document.getElementById('logout-form-layout').submit();">
                            <i class="fas fa-sign-out-alt"></i> {{ trans('user::form.titles.logout') }}</a>
                        <form id="logout-form-layout" action="{!! route('logout') !!}" method="POST"
                              style="display: none;">
                            {!! csrf_field() !!}
                        </form>
                    </li>

                </ul>
            </div>
        </div>
        @endif
        <div class="col-xl-9 col-lg-9 col-md-8  floatright    ">
            @partial('sections.main')
        </div>
    </div>
</div>

@partial('sections.bottom')

@partial('footer')
@partial('foot')
</body>
</html>