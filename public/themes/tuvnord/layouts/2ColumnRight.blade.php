<!DOCTYPE html>
<html lang="{!! get_instance()->getLanguageCode() !!}" dir="{!! get_instance()->getLanguageDirection() !!}">

<head>
    @partial('head')
</head>

<body class="home-page">
@partial('header')
@partial('sections.top')
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12  fltr    ">
            @partial('sections.right')
        </div>
        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12  fltr    ">
            @partial('sections.main')
        </div>

    </div>
</div>
@partial('sections.bottom')
@partial('footer')

@partial('foot')

</body>

</html>
