<!DOCTYPE html>
<html lang="{!! get_instance()->getLanguageCode() !!}" dir="{!! get_instance()->getLanguageDirection() !!}">

<head>
    @partial('head')
</head>

<body class="home-page">
@partial('header')
@partial('sections.top')
<div class="FullWidth floatright">
    @partial('sections.main')
</div>
@partial('sections.bottom')
@partial('footer')
@partial('foot')

</body>

</html>
