<div class="container-fluid bg_1 floatright">
    <div class="container">
        <div class="_shop_basket2   ">
            @lang('sale::cart.cart')
        </div>
        <div class="_shop_basket31 ">
            <div class=" col-lx-8 mx-auto col-md-10 col-sm-12 col-xs-12  _tbl_hover _shop_basket63">
                @php $currentCart=$blockViewModel->getCurrentCart();  @endphp
                @if($cart=$currentCart->getCart())
                    <table class="table table-hover Basket_tbl1">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">@lang('sale::cart.product_code')</th>
                            <th scope="col">@lang('sale::cart.title')</th>
                            <th scope="col">@lang('sale::cart.cost') </th>
                            <th scope="col">@lang('sale::cart.discount') </th>
                            <th scope="col">@lang('sale::cart.qty') </th>
                            <th scope="col">@lang('sale::cart.total') </th>
                            <th scope="col" class="txt-center"> @lang('sale::cart.delete') </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $disountedOnProducts=0.00 @endphp
                        @foreach($cart->cartItems as $cartItem)
                            @php
                                $item=$blockViewModel->getSaleItem($cartItem->item_type,$cartItem->item_id,$cartItem->options_array);
                                $itemDiscountPrice=BridgeHelper::getCurrencyHelper()->getConvertPrice($cartItem->discount_amount,$cartItem->currency_id,$cart->currency_id);
                                $disountedOnProducts=$disountedOnProducts+$itemDiscountPrice;
                            @endphp
                            @if($item)
                                @php $itemExtraData=$item->getItemExtraData(); @endphp
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ isset($cartItem->options->product_key)?$cartItem->options->product_key:'-' }}</td>
                                    <td>
                                        @if($itemExtraData && isset($itemExtraData['url']))
                                            <a href="{!! $itemExtraData['url'] !!}">
                                                {{ $cartItem->name }}
                                            </a>
                                            @else
                                            {{ $cartItem->name }}
                                        @endif
                                    </td>
                                    <td class="color6">{{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cartItem->amount+$cartItem->discount_amount,$cartItem->currency_id,true) }}</td>
                                    <td>{{ ($itemDiscountPrice==0)?'-':BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($itemDiscountPrice,$cartItem->currency_id,true) }}</td>
                                    <td> {{ $cartItem->qty }}</td>
                                    <td class="color6">{{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cartItem->total_amount,$cartItem->currency_id,true) }}</td>
                                    <td class="txt-center">
                                        <a class="text-danger" href="{{ route('sale.cart.delete',['id'=>$cartItem->id]) }}" >
                                            <i class="fas fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            @else
                                @php
                                    $deletedId=[$cartItem->id];
                                    $cartItem->delete();
                                    $currentCart->updateCart(null,$deletedId);
                                @endphp
                            @endif
                        @endforeach
                        <tr>
                            <td colspan="7"><strong>@lang('sale::cart.total_amount')</strong></td>
                            <td><strong>{{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cart->amount,$cart->currency_id,true) }}</strong></td>
                        </tr>
                        @include('sale::cart.promotion',['cart'=>$cart,'col'=>7])
                        @include('sale::cart.affiliate',['cart'=>$cart,'col'=>6])
                        @include('sale::cart.discount',['cart'=>$cart,'col'=>6,'discount'=>$disountedOnProducts])
                        <tr>
                            <td class="text-success" colspan="7"><strong>@lang('sale::cart.payable_amount')</strong></td>
                            <td class="text-success"><strong> {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cart->payable_amount,$cart->currency_id,true) }}</strong></td>
                        </tr>
                        </tbody>
                    </table>
                @else
                    {!! $blockViewModel->getComponent('message',['info'=>trans('sale::message.cart_empty')]) !!}
                @endif
            </div>

            @if($cart=$currentCart->getCart())

                <div class=" col-xl-8   mx-auto col-md-12 col-sm-12 col-xs-12  _tbl_hover ">
                </div>
                <div class="UnderTableLinks_">

                    <a class="" href="{{ route('sale.cart.invoice') }}">
                        <div class="btn btn1 bgcolor4 color5">

                        <span class="btn2 Fsize18">
                            @lang('sale::cart.save_order')
                        </span>
                            <span class="btn3 ">
                            @lang("sale::cart.pay_order")
                        </span>
                            <span class="btn4 Fsize18">
                           <i class="fas fa-arrow-left"></i>
                        </span>
                        </div>
                    </a>

                </div>

            @endif
        </div>
    </div>
</div>
