{!! $blockViewModel->getConfig('before_html') !!}
@php $entities =$blockViewModel->getEntitiesFun() @endphp
@if($entities->isNotEmpty())

    <div class="mini-slider">
        <h3>{{ $blockViewModel->getConfig('title') }}</h3>
        <div class="owl-carousel owl-5">

            @foreach($entities as $entity)
                @php $rate = (array)$entity->rate; @endphp
                @php $media_image = (array)$entity->media_image; @endphp

                <a href="{{ route('entity.view',['id'=>$entity->id,'slug'=>$entity->slug]) }}" title="{{ $entity->short_description }}">
                    <div class="item" data-title="{{ $entity->short_description }}">
                        <img class="owl-lazy" data-src="{!! $media_image?$media_image['url_thumbnail']:null !!}" />
                        <h3>{{ $entity->title }}</h3>

                        @if(isset($entity->address) && $entity->address)
                            <p class="distance">{!! number_format($entity->address->dist) !!} {!! $entity->address->unit !!}</p>
                        @endif

                        <div class="rate">

                            <div class="stars">
                                @php $rate = (array)$entity->rate; @endphp
                                @for($i = 1; 5>=$i; $i++)
                                    <i class="{!! (($rate['data_rate'])>=$i) || (($rate['data_rate'])>4.8)?'fas':'far'  !!} fa-star"></i>
                                @endfor

                            </div>

                            <div class="num">{!! $rate['data_rate'] !!}</div>
                        </div>
                    </div>
                </a>
            @endforeach

        </div>
    </div>

@endif
{!! $blockViewModel->getConfig('after_html') !!}