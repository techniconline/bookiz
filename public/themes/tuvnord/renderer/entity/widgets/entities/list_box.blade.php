{!! $blockViewModel->getConfig('before_html') !!}

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad_btn_50">
    <div class="container Hsticky ">
        <div class="row bg1 ">
            <h4 class="bgcolor5  brd-rad-05 Shadow3 ">
                    <span class="bgcolor2">
                        @if($image_logo=$blockViewModel->getConfig('logo_image'))
                            <img src="{{ url($image_logo) }}" alt="{{ $blockViewModel->getConfig('title') }}"/>
                        @endif
                    </span>&nbsp;&nbsp;&nbsp;{{ $blockViewModel->getConfig('title') }}
            </h4>
        </div>
    </div>
    <div class="container  ">
        <div class="row">
            @php $entities = $blockViewModel->getEntitiesListPaginate() @endphp
            @foreach($entities as $entity)
                <div class=" col-lg-3 col-md-4  col-sm-6 col-xs-12 entity-box">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mrg_top_15 mrg_btn_10  ItemBx">
                        <a href="{{ route('entity.view',['id'=>$entity->id]) }}">
                            <div class="row bgcolor6 bx_5 brd-rad-05  NoOverflow ">
                                <img src="{{ strlen($entity->image)?$entity->image:BridgeHelper::getConfig()->getSettings('default_poster','instance','entity') }}"
                                     class="_Full100precent2"/>
                            </div>
                            <div class="row   bx_4  brd-rad-10 noSpan pad_10 ">
                                <h3 class="LimitedtString">
                                    {{ $entity->title }}
                                </h3>
                                <div class="spn_1">
                                    {!! BridgeHelper::getRate()->getRateObject()
                                        ->setModel($entity->getTable(), $entity->id)
                                        ->setWithData(true)
                                        ->setWithForm(true)
                                        ->setWithSaveRate(false)
                                        ->setWithTextRate(false)
                                        ->setConfigs(["container_js" => "block_js", "container_css" => "block_style"])
                                        ->init()
                                        ->getForm()
                                        !!}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
            <div class="paginate">
                {!! $entities->links() !!}
            </div>
        </div>
    </div>
</div>

{!! $blockViewModel->getConfig('after_html') !!}