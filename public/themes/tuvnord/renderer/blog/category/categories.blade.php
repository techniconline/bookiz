<div class="blog-catagories">

    <div class="head-wrapper">
        <div class="head">@lang('blog::blog.categories')</div>
    </div>

    <ul class="">
        <li class="">
            <a class="" style="font-weight: bold"
               href="{!! route("blog.category.list.blogs", ["id" => 0, "alias" => str_replace(" ","-",trans("blog::blog.all_categories"))]).'?'.http_build_query(request()->except("category_id","language_id")) !!}">
                {{ trans("blog::blog.all_categories") }}
            </a>
            <ul>
                @foreach($viewModel->getBlogCategories() as $blog_category)
                    @include('blog::category.sub_categories',['dept'=>1])
                @endforeach
            </ul>
        </li>
    </ul>

</div>