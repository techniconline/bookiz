{!! $blockViewModel->getConfig('before_html') !!}
<style>
    .post-page .body .boxs .box img {
        height: auto;
        min-height: 180px;
        max-height: 400px;
    }
</style>
@php $blogs = $blockViewModel->getBlogsListPaginate() @endphp


<div class="post-page">
    {{--<div class="head" style='background-image: url("https://www.treatwell.co.uk/inspiration/wp-content/uploads/sites/35/2016/12/TW_UK_Spa-2017_Landingpage_Hero_MP_v01.jpg")'>--}}
    {{--<div class="content">--}}
    {{--<h1 class="text-right">Super spas for groups</h1>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="body">

        <div class="boxs">

            @foreach($blogs as $index => $item)
                @php $mod = fmod($index, 2); @endphp

                @if($mod)
                    <div class="box box-ground" style="">
                        <img class="scale"
                             src="{{ $item->image?:'https://cdn1.treatwell.net/images/view/v2.i1513595.w540.h192.xDF38EF5F.png' }}"
                             alt="{{ $item->title }}"/>
                    </div>

                    <div class="box">
                        <h4>{{ $item->title }}</h4>
                        <p>{!! $item->short_description !!}</p>
                        <a title="{{ $item->title }}" href="{{ route('blog.view',['id'=>$item->id]) }}"
                           class="btn btn-danger">@lang('blog::blog.blog_view')</a>
                    </div>

                @else

                    <div class="box">
                        <h4>{{ $item->title }}</h4>
                        <p>{!! $item->short_description !!}</p>
                        <a title="{{ $item->title }}" href="{{ route('blog.view',['id'=>$item->id]) }}"
                           class="btn btn-danger">@lang('blog::blog.blog_view')</a>
                    </div>
                    <div class="post-date">
                        <div data-hover="{{ $item->created_small_date_day }}"
                             class="date">{{ $item->created_small_date_day }}</div>
                        <div class="month">{{ $item->created_small_date_month }}</div>
                    </div>
                    <div class="box box-ground" style="">
                        <img class="scale"
                             src="{{ $item->image?:'https://cdn1.treatwell.net/images/view/v2.i1513595.w540.h192.xDF38EF5F.png' }}"
                             alt="{{ $item->title }}"/>
                    </div>

                @endif


            @endforeach


        </div>

        {!! $blogs->links() !!}

    </div>
</div>


<div class="col-lg-6 col-md-6 col-sm-6 makeitfade" style="display: none">
    <div class="head">
        {{ $blockViewModel->getConfig('title') }}
    </div>
    <div class="latest-blog">

        @foreach($blogs as $item)

            <div class="latest-post">
                <div class="post-date">
                    <div data-hover="{{ $item->created_small_date_day }}"
                         class="date">{{ $item->created_small_date_day }}</div>
                    <div class="month">{{ $item->created_small_date_month }}</div>
                </div>
                <div class="detail">
                    <a href="{{ route('blog.view',['id'=>$item->id]) }}" class="title">
                        {{ $item->title }}
                    </a>
                    <p>{!! $item->short_description !!}</p>
                </div>
            </div>

@endforeach

{!! $blockViewModel->getConfig('after_html') !!}