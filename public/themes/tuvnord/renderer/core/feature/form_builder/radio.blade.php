@if(isset($featureGroup) && $featureGroup)
    @if($builder->getShowFeatureGroup())
        {!! Form::label('label_name_group', $featureGroup->title , ['class'=>'feature_group group_'.$featureGroup->id]) !!}
    @endif

@endif

@if($feature->featureValues)

    {!! FormHelper::label('label_name_'.$feature->id , $feature->title
    , ['class'=>'group_'.isset($featureGroup->id)?$featureGroup->id:str_random(4), 'data-group-id'=>isset($featureGroup->id)?$featureGroup->id:0 ]) !!}


    @foreach($feature->featureValues as $featureValue)
        <?php
        $checked = $activeValue && in_array($featureValue->id, $activeValue) ? true : false;
        ?>
        <label class="mt-checkbox" data-active="{!! json_encode($activeValue) !!}">

            {!! FormHelper::radio('feature['.$featureValue->feature_id.'][]'
                ,$featureValue->id, $checked
                , ["id"=>"radio_".$featureValue->id
                ,'label_style'=>'text-align: right;'
                , "title"=>$featureValue->title] ) !!}

        </label>

    @endforeach


@endif

@if(isset($featureGroup) && $featureGroup)

@endif