@if(isset($featureGroup) && $featureGroup)

        @endif
        <?php
        $group_id = isset($featureGroup->id) ? $featureGroup->id : str_random(4);
        ?>
<table class="col-lg-12">
    <tr>
        <td>
            @if($builder->getShowFeatureGroup())
                {!! FormHelper::label('label_name_group', $featureGroup->title , ['class'=>'feature_group group_'.$group_id]) !!}
            @endif

            {!! FormHelper::textarea('feature['.$feature->id.']',old('feature['.$feature->id.']',isset($activeValue)?$activeValue:null)
    ,['title'=>$builder->getRowNumber() .'. '.$feature->title
    ,/*'helper'=>$feature->title,*/ "class"=>'group_'.$group_id
    ,'parent_class'=>'col-lg-12'
    ,'cols'=>'100'
    ,'label_style'=>'text-align: right;'
    ]) !!}
        </td>
    </tr>
</table>



    @if(isset($featureGroup) && $featureGroup)

@endif