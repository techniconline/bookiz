{!! $blockViewModel->getConfig('after_html') !!}
@if($blockViewModel->getConfig('is_mobile_box'))


    {{--<div class="{{ $blockViewModel->getConfig('class_div')?$blockViewModel->getConfig('class_div'):'search-container' }}">--}}
        {{--<form class="{{ $blockViewModel->getConfig('class_form') }}" method="get"--}}
              {{--action="{{ \Illuminate\Support\Facades\Route::has($blockViewModel->getConfig('route_name'))?route($blockViewModel->getConfig('route_name')):null }}">--}}
        {{--<i class="fal fa-search"></i>--}}
        {{--<input type="search" placeholder="@lang('core::form.helper.search')" class="_search"/>--}}
        {{--<ul class="autocomplete-data">--}}
            {{--<li><a href="#">Auto Complete</a></li>--}}
            {{--<li><a href="#">Auto Complete</a></li>--}}
        {{--</ul>--}}
        {{--</form>--}}
    {{--</div>--}}

@else

    <div class="{{ $blockViewModel->getConfig('class_div')?$blockViewModel->getConfig('class_div'):'search-container' }}">
        <form style="width: 100%" class="{{ $blockViewModel->getConfig('class_form') }}" method="get"
              action="{{ \Illuminate\Support\Facades\Route::has($blockViewModel->getConfig('route_name'))?route($blockViewModel->getConfig('route_name')):null }}">
            <i class="fas fa-search"></i>
            <input type="search" name="{!! $blockViewModel->getConfig('input_name', 'search_text') !!}" placeholder="@lang('core::form.helper.placeholder_search')" class="_search"/>
            <ul class="autocomplete-data">
                <li><a href="#">Auto Complete</a></li>
                <li><a href="#">Auto Complete</a></li>
            </ul>
        </form>
    </div>

@endif
{!! $blockViewModel->getConfig('after_html') !!}