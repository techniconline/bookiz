{!! $blockViewModel->getConfig('before_html') !!}
@php  $insTheme = app('getInstanceObject')->getInstanceTheme(); @endphp
<div class="{{ $blockViewModel->getConfig('class_div') }}" id="{{ $blockViewModel->getConfig('id') }}">
    @if($blockViewModel->getConfig('mobile_menu'))
        <button id="{{$blockViewModel->getConfig('id')}}"
                class="toggle-menu">{{ $blockViewModel->getConfig('mobile_title') }}<i class="fas fa-bars"></i></button>
    @endif

    <ul class="{{ $blockViewModel->getConfig('class_ul') }}">
        @foreach($blockViewModel->getMenus() as $menu)
            @include('themes.'.($insTheme->code).'.renderer.core.widgets.menu.submenu',['pre'=>'mobile_'])
        @endforeach
    </ul>

</div>
{!! $blockViewModel->getConfig('after_html') !!}