@php  $insTheme = app('getInstanceObject')->getInstanceTheme(); @endphp
<li class="{{ $blockViewModel->getConfig($pre.'class_li') }}">
    <a class="{{ $blockViewModel->getConfig($pre.'class_link') }}"
       href="{{ BridgeHelper::getMenuHelper()->getMenuUrl($menu) }}">
        {{ $menu->title }}
    </a>
    @if($menu->childes->count())
        <div class="submenu">
            <ul>
                @foreach($menu->childes as $menu)
                    @include('themes.'.($insTheme->code).'.renderer.core.widgets.menu.submenu',['pre'=>$pre])
                @endforeach
            </ul>
        </div>
    @endif
</li>