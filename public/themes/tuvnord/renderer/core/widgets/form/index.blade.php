{!! $blockViewModel->getConfig('before_html') !!}
<div style="padding: 30px;" class="{{ $blockViewModel->getConfig('class_div') }}" id="{{ $blockViewModel->getConfig('id') }}">

    @foreach($blockViewModel->getFromMetaGroups() as $formMetaGroup)

        {!!  FormHelper::legend($formMetaGroup->metaGroup->title,['style'=>'font-weight: bold;']) !!}

        @if($blockViewModel->getConfig('with_actions',true))
            {!! FormHelper::open(['role'=>'form','url'=>route('core.formBuilder.meta.features.value.save'  , ['form_id'=>$blockViewModel->getFormObject()->id, 'form_meta_group_id'=>$formMetaGroup->id])
            ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
        @endif

        @php  FormHelper::setCustomAttribute('label','class','')->setCustomAttribute('element','class','input-group'); $counter=1; @endphp

        @foreach($formMetaGroup->metaGroup->featureGroups as $featureGroup)
            {{--<table>--}}
            @foreach($featureGroup->featureGroupRelations as $featureGroupRelation)

                @if($featureGroupRelation->feature)
                    {{--<tr>--}}
                        {{--<td>--}}

                            <?php
                            $activeValue = null;
                            $metaFeatureDataList = $blockViewModel->getFeatureData();
                            if (isset($metaFeatureDataList) && $metaFeatureDataList && isset($metaFeatureDataList[$formMetaGroup->id][$featureGroupRelation->feature->id])) {
                                $activeValue = $metaFeatureDataList[$formMetaGroup->id][$featureGroupRelation->feature->id];
                            }
                            ?>
                            {!! $blockViewModel->setRowNumber($counter)->setShowFeatureGroup(false)->setFeatureGroup($featureGroup)->setFeatureModel($featureGroupRelation->feature)->setActiveValueFeature($activeValue)->getHtml() !!}


                        {{--</td>--}}
                    {{--</tr>--}}
                    @php $counter++; @endphp
                @endif

            @endforeach
            {{--</table>--}}
        @endforeach
        {!! FormHelper::openAction(['style'=>'text-align: center;']) !!}

        @if($blockViewModel->getConfig('with_actions',true))
            {!! FormHelper::submitOnly(['title'=>trans("core::formBuilder.submit"),'class'=>'btn btn-success green _save', 'style'=>'border-radius: 50px']) !!}
        @endif

        {!! FormHelper::closeAction() !!}
        {!! FormHelper::close() !!}

    @endforeach

</div>
{!! $blockViewModel->getConfig('after_html') !!}