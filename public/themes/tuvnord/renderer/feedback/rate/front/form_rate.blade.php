<div class="rate form_rate{!! $viewModel->user_id?"":"0" !!}"
     data-confirm-message="{!! trans("feedback::rate.confirm") !!}"
     data-token="{!! csrf_token() !!}"
     data-url="{!! $viewModel->with_save_rate?route('feedback.rate.storeRate',["model"=>$viewModel->model_name, "model_id"=>$viewModel->model_id]):'' !!}">
    {!! csrf_field() !!}

    @if($viewModel->getTypes())
        <div class="ratings">

            <div class="total-rate">
                <span class="num">{!! $viewModel->getTotalRate() !!}</span>
                <div>
                    <div class="stars">

                        @for($i = 1; 5>=$i; $i++)
                            @if($viewModel->with_save_rate)
                                <a class="_save_rate" title="{!! $i !!}"
                                   href="{!! route('feedback.rate.storeRate',["model"=>$viewModel->model_name, "model_id"=>$viewModel->model_id, "rate"=>$i]) !!}">
                                    @endif
                                    <i class="{!! (($viewModel->getTotalRate())>=$i) || (($viewModel->getTotalRate())>4.8)?'fas':'far'  !!} fa-star"
                                       data-rate="{!! $viewModel->getTotalRate() !!}"></i>
                                    @if($viewModel->with_save_rate)
                                </a>
                            @endif
                        @endfor

                    </div>
                    <span class="review-count">{!! $viewModel->getCountRated() !!} @lang("feedback::rate.reviews")</span>
                </div>
            </div>

            <div class="detail-rate">
                @foreach($viewModel->getTypes() as $key => $type)

                    <div class="item" data-model="{!! $key !!}">
                        <div class="title">{!! $type['title'] !!}</div>
                        <div class="stars">

                            @for($i = 1; 5>=$i; $i++)
                                @if($viewModel->with_save_rate)
                                    <a class="_save_rate" title="{!! $i !!}"
                                       href="{!! route('feedback.rate.storeRate',["model"=>$viewModel->getModelTypes($key), "model_id"=>$viewModel->model_id, "rate"=>$i]) !!}">
                                        @endif
                                        @php $typeData = $viewModel->getTotalRateType($key); @endphp
                                        <i class="{!! (($typeData['rate'])>=$i) || (($typeData['rate'])>4.8)?'fas':'far'  !!} fa-star"
                                           data-rate="{!! $typeData['rate'] !!}"></i>
                                        @if($viewModel->with_save_rate)
                                    </a>
                                @endif
                            @endfor

                        </div>
                    </div>

                @endforeach
            </div>
        </div>
    @else

        @if($viewModel->with_text_rate)
            {{--        <span class="num"> {!! trans("feedback::rate.rated_text",["total_rate"=> $viewModel->getTotalRate(), "count_rated"=> $viewModel->getCountRated()]) !!}</span>--}}
            <span class="num"> {!! $viewModel->getTotalRate() !!}</span>
        @endif

        <div class="stars">

            @for($i = 1; 5>=$i; $i++)
                @if($viewModel->with_save_rate)
                    <a class="_save_rate" title="{!! $i !!}"
                       href="{!! route('feedback.rate.storeRate',["model"=>$viewModel->model_name, "model_id"=>$viewModel->model_id, "rate"=>$i]) !!}">
                        @endif
                        <i class="{!! (($viewModel->getTotalRate())>=$i) || (($viewModel->getTotalRate())>4.8)?'fas':'far'  !!} fa-star"
                           data-rate="{!! $viewModel->getTotalRate() !!}"></i>
                        @if($viewModel->with_save_rate)
                    </a>
                @endif
            @endfor

        </div>

    @endif


</div>
