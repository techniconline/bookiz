﻿<div class="auth-page">
    <form class="auth-container" id="signin" action="{{ route('login') }}" method="post">
        {{ csrf_field() }}
        {!! FormHelper::input('hidden','referer',old('referer',$viewModel->getReferer())) !!}

        <h1>@lang('user::form.helper.login')</h1>
        <div class="body">
            <div class="form-group {{ $errors->has('connector') ? ' has-error' : '' }}">
                <label for="email">{{ $viewModel->getConnectorLabel() }} :</label>
                <input type="email" value="{{ old('connector') }}" id="connector" name="connector" class="form-control" placeholder="{{ $viewModel->getConnectorLabel() }}" />
                @if ($errors->has('connector'))
                    <span class="help-block {{ $errors->has('connector') ? ' text-danger' : '' }} ">
                        <strong>{{ $errors->first('connector') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password">@lang('user::form.fields.password'):</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="@lang('user::form.fields.password')" />
            </div>
            <input type="checkbox" id="remember" name="remember" class="_rember"  value="1" {!! old('remember') ? 'checked="checked"':'' !!}>
            <label for="remember" class="control-label">@lang('user::form.fields.remember')</label>
            <button type="submit" class="btn btn-dark btn-block">@lang('user::form.fields.login')</button>
        </div>


        <div class="auth-links">
            <a href="{{ route('forgot') }}"> @lang('user::form.titles.forgotToken')</a>

            @if(!BridgeHelper::getConfig()->getSettings('register_hide','instance','core'))
                <a href="{{ route('register') }}">@lang('user::form.titles.register')  </a>
            @endif
        </div>
    </form>
</div>
