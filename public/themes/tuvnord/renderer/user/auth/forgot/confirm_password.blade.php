<div class="auth-page">
    <form class="auth-container" id="change_password" action="{{ route('forgot.reset',['id'=>$viewModel->getResetId()]) }}" method="post">
        {{ csrf_field() }}

        <h1>@lang('user::form.titles.reset_password')</h1>
        <div class="body">
            <div class="form-group {{ $errors->has('token') ? ' has-error' : '' }}">
                <label for="token">@lang('user::form.fields.token'):</label>
                <input id="token" type="token" class="form-control" name="token" placeholder="@lang('user::form.fields.token')" required />
                @if ($errors->has('token'))
                    <span class="help-block {{ $errors->has('token') ? ' text-danger' : '' }} ">
                        <strong>{{ $errors->first('token') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password">@lang('user::form.fields.password'):</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="@lang('user::form.fields.password')" />
            </div>
            <div class="form-group">
                <label for="password_confirmation">@lang('user::form.fields.password_confirmation'):</label>
                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="@lang('user::form.fields.password_confirmation')" />
            </div>


            <button type="submit" class="btn btn-dark btn-block">@lang('user::form.fields.password_change')</button>
        </div>


        <div class="auth-links">
            <a href="{{ route('forgot') }}"> @lang('user::form.titles.forgotToken')</a>

            @if(!BridgeHelper::getConfig()->getSettings('register_hide','instance','core'))
                <a href="{{ route('register') }}">@lang('user::form.titles.register')  </a>
            @endif
        </div>
    </form>
</div>
