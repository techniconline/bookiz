﻿{!! $blockViewModel->getConfig('before_html') !!}

<div class="account">
    <img src="{!! $user->small_avatar_url !!}" alt="{{ $user->full_name }}"/>
    <p class="name">{{ $user->full_name }}</p>
    <i class="fas fa-angle-down"></i>
    <ul class="account-options">
        <li><a href="{!! route("sale.order.user") !!}"> <i class="fas fa-shopping-cart"></i> @lang("sale::sale.my_orders") </a></li>
        <li><a href="{!! route("user.profile.password.form") !!}"><i  class="fa fa-lock"></i>@lang("user::form.titles.change_password")</a></li>
        <li><a href="{!! route("user.profile.edit") !!}"><i class="fa fa-user-edit"></i>@lang("user::form.titles.profile")</a></li>
        <li><a href="{!! route('logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form-menu').submit();"><i class="fas fa-sign-out-alt"></i> {{ trans('user::form.titles.logout') }}</a></li>
    </ul>
</div>

<form id="logout-form-menu" action="{!! route('logout') !!}" method="POST"
      style="display: none;">
    {!! csrf_field() !!}
</form>


<form id="logout-form" action="{!! route('logout') !!}" method="POST"
      style="display: none;">
    {!! Form::token() !!}
</form>

{!! $blockViewModel->getConfig('after_html') !!}