﻿<!--<div class="bgcolor4 _shop_basket1 brd-rad-05 userColor  fltl ">
<a data-toggle="modal" data-target="#userModal" href="#">
    <i class="fa fa-user"></i>
</a>
</div>-->
{!! $blockViewModel->getConfig('before_html') !!}

<style>
    .modal-content > .auth-page  {
        margin-top: 0px !important;
    }
</style>

<div class="modal fade" id="userModal" role="dialog">
    <div class="modal-dialog ">
        <!-- Modal content-->
        <div class="modal-content">
            {!!  $blockViewModel->getLoginForm() !!}
        </div>
    </div>
</div>

<div class="account" style="width: auto !important; ">
    <a data-toggle="modal" data-target="#userModal" href="#">
        <img src="{!! asset('themes/default/assets/image/me_waiting_for_login_avatar.png') !!}" alt=""/>
    </a>&nbsp;&nbsp;
    <div class="name">@lang('user::form.helper.welcome')</div>
    {{--<i class="fal fa-angle-down"></i>--}}
    {{--<ul class="account-options">--}}
    {{--@if(!BridgeHelper::getConfig()->getSettings('register_hide','instance','core'))--}}
    {{--<li><a href="{{ route('register') }}">@lang('user::form.titles.register')</a></li>--}}
    {{--@endif--}}
    {{--</ul>--}}
</div>

{!! $blockViewModel->getConfig('after_html') !!}