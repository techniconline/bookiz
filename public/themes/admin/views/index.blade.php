<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">

    <!-- BEGIN SIDEBAR -->
    @sections('right')
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    @sections('main')
    <!-- END CONTENT -->

    <!-- BEGIN QUICK SIDEBAR -->
    @sections('left')
    <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->