<?php
return array(

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists, this
    | is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities this is cool
    | feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => array(

        'before' => function ($theme) {
            $theme->setTitle(env('APP_NAME', 'Techniconline'));
            $theme->setAuthor(env('APP_NAME', 'Techniconline'). ' .Team');
        },

        'asset' => function ($asset) {
            $language = app('getInstanceObject')->getLanguage();
            //$themplate = app('getInstanceObject')->getInstance()->backendTemplate()->first();
            $direction = $language ? $language->direction : null;
            //			$asset->themePath()->add([
//										['style', 'css/style.css'],
//										['script', 'js/script.js']
//									 ]);


            $asset->add('font-awesome', asset('themes/admin/assets/global/plugins/font-awesome/css/font-awesome.min.css'));
            $asset->add('simple-line-icons', asset('themes/admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css'));

            if (strtolower($direction) == "rtl") {

                $asset->add('bootstrap', asset('themes/admin/assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css'));
                $asset->add('bootstrap-switch', asset('themes/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css'));
                //GLOBAL
                $asset->add('components', asset('themes/admin/assets/global/css/components-rtl.min.css'));
                $asset->add('plugins', asset('themes/admin/assets/global/css/plugins-rtl.min.css'));

                //THEME LAYOUT
                $asset->add('layout', asset('themes/admin/assets/layouts/layout/css/layout-rtl.min.css'));
                $asset->add('darkblue', asset('themes/admin/assets/layouts/layout/css/themes/darkblue-rtl.min.css'));
                $asset->add('custom', asset('themes/admin/assets/layouts/layout/css/custom-rtl.min.css'));


            } else {
                $asset->add('bootstrap', asset('themes/admin/assets/global/plugins/bootstrap/css/bootstrap.min.css'));
                $asset->add('bootstrap-switch', asset('themes/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'));
                //GLOBAL
                $asset->add('components', asset('themes/admin/assets/global/css/components.min.css'));
                $asset->add('plugins', asset('themes/admin/assets/global/css/plugins.min.css'));

                //THEME LAYOUT
                $asset->add('layout', asset('themes/admin/assets/layouts/layout/css/layout.min.css'));
                $asset->add('darkblue', asset('themes/admin/assets/layouts/layout/css/themes/darkblue.min.css'));
                $asset->add('custom', asset('themes/admin/assets/layouts/layout/css/custom.min.css'));

            }
            $asset->add('sweetalert', asset('/assets/global/plugins/sweetalert/sweetalert2.min.css'));
            $asset->container('up_theme_js')->add('jquery', asset('themes/admin/assets/global/plugins/jquery.min.js'));
            $asset->container('up_theme_js')->add('bootstrap_js', asset('themes/admin/assets/global/plugins/bootstrap/js/bootstrap.min.js'));
            $asset->container('up_theme_js')->add('sweetalert', asset('/assets/global/plugins/sweetalert/sweetalert2.min.js'));

            $asset->add('js.cookie', asset('themes/admin/assets/global/plugins/js.cookie.min.js'));
            $asset->add('bootstrap-hover', asset('themes/admin/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'));
            $asset->add('jquery.slimscroll', asset('themes/admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'));
            $asset->add('jquery.blockui', asset('themes/admin/assets/global/plugins/jquery.blockui.min.js'));
            $asset->add('bootstrap-switch', asset('themes/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'));

            $asset->container('theme_js')->add('app', asset('themes/admin/assets/global/scripts/app.min.js'));
            $asset->container('theme_js')->add('jquery', asset('themes/admin/assets/layouts/layout/scripts/layout.min.js'));

            $asset->cook('backbone', function ($asset)  {
                //$asset->add('googleapis', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false');
//                $asset->container('plugin_general')->add('googleapis', 'http://maps.google.com/maps/api/js?sensor=false&.js');
                $asset->container('up_theme_js')->add('googleapis', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDLJkvq8VOv1ZUpn9zTTV2ckN-Ki4yt6oM');

            });

            // You may use elixir to concat styles and scripts.
            /*
            $asset->themePath()->add([
                                        ['styles', 'dist/css/styles.css'],
                                        ['scripts', 'dist/js/scripts.js']
                                     ]);
            */

            // Or you may use this event to set up your assets.
            /*
            $asset->themePath()->add('core', 'core.js');
            $asset->add([
                            ['jquery', 'vendor/jquery/jquery.min.js'],
                            ['jquery-ui', 'vendor/jqueryui/jquery-ui.min.js', ['jquery']]
                        ]);
            */
        },


        'beforeRenderTheme' => function ($theme) {
            $theme->asset()->serve('backbone');
            // To render partial composer
            /*
            $theme->partialComposer('header', function($view){
                $view->with('auth', Auth::user());
            });
            */

        },

        'beforeRenderLayout' => array(

            'mobile' => function ($theme) {
                // $theme->asset()->themePath()->add('ipad', 'css/layouts/ipad.css');
            }

        )

    )

);