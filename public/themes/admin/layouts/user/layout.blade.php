<!DOCTYPE html>
<html lang="{!! get_instance()->getLanguageCode() !!}" dir="{!! get_instance()->getLanguageDirection() !!}">

<head>
    <meta charset="utf-8"/>
    {!! meta_init() !!}
    <meta name="keywords" content="@get('keywords')">
    <meta name="description" content="@get('description')">
    <meta name="author" content="@get('author')">
    <link rel="icon" href="/logo-fav.png?v=1.2">

    <title>@get('title')</title>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    @styles()
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    @styles('general_style')

    @styles('theme_style')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    @styles('general_style')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME STYLES -->
    @styles('inline_style')
    <!-- END THEME STYLES -->
    @scripts('up_theme_js')

    @scripts('up_theme_js_2')

    @scripts('plugin_general')

    @scripts('plugin_general_2')

</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        @partial('header')
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix"></div>
<div class="page-container">
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse">
            @partial('sections.right')
        </div>
        <!-- END SIDEBAR -->
    </div>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                @partial('sections.main')
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <section id="left">
        @partial('sections.left')
    </section>
</div>
<!-- BEGIN FOOTER -->
<div class="page-footer">
    @partial('footer')
</div>
<!-- END FOOTER -->
<!-- BEGIN CORE PLUGINS -->
@scripts()
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
@scripts('general_js')

@scripts('theme_js')

@scripts('theme_js_2')
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
@scripts('script_general')
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
@scripts('script_general_2')
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME SCRIPTS -->
@scripts('inline')
<!-- END THEME SCRIPTS -->

</body>

</html>
