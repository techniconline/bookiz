<!DOCTYPE html>
<html lang="{!! get_instance()->getLanguageCode() !!}" dir="{!! get_instance()->getLanguageDirection() !!}">
<head>
    <meta charset="utf-8"/>
    {!! meta_init() !!}
    <meta name="keywords" content="@get('keywords')">
    <meta name="description" content="@get('description')">
    <meta name="author" content="@get('author')">
    <link rel="icon" href="/logo-fav.png?v=1.2">

    <title>@get('title')</title>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    @styles()
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    @styles('theme_style')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    @styles('general_style')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME STYLES -->
    @styles('inline_style')
    <!-- END THEME STYLES -->
    @scripts('up_theme_js')

    @scripts('up_theme_js_2')

    @scripts('plugin_general')

    @scripts('plugin_general_2')

</head>

<body >
 

	 <div class="container-fliud  fullH ">
              

            <div class="modal-content col-lg-6 col-md-8 col-sm-6 col-xs-10 NRdR mrg_btn_50 fullH fltr"   >
			  <div class="col-lg-6 col-md-6 col-sm-10 col-xs-12 UnSetFlt  "  >    @partial('sections.main') </div>
            </div>

			  <div class="col-lg-6 col-md-4 col-sm-6 col-xs-2  bgIMG-pop  bgb_all  NRdL mrg-1-x fullH fltr"  > 
			  
	 
				
				
				</div>
              
 </div>
              
 
<!-- END FOOTER -->
<!-- BEGIN CORE PLUGINS -->
@scripts()
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
@scripts('theme_js')

@scripts('theme_js_2')
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
@scripts('script_general')
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
@scripts('script_general_2')
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME SCRIPTS -->
@scripts('inline')
<!-- END THEME SCRIPTS -->

</body>

</html>
