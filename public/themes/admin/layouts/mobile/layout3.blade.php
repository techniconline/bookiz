<!DOCTYPE html>
<html lang="{!! get_instance()->getLanguageCode() !!}" dir="{!! get_instance()->getLanguageDirection() !!}">

    <head>
        {!! meta_init() !!}
        <meta name="keywords" content="@get('keywords')">
        <meta name="description" content="@get('description')">
        <meta name="author" content="@get('author')">
        <link rel="icon" href="/logo-fav.png?v=1.2">

        <title>@get('title')</title>

        @styles()
        
    </head>

    <body>
        {{--@partial('header')--}}

        @content()

        {{--@partial('footer')--}}

        @scripts()
    </body>

</html>
