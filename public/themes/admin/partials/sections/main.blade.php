@partial('component.error')
@if(Theme::hasArgument('widgets.center'))
    <section id="main_admin">
        @foreach(Theme::getContentArgument('widgets.center') as $value)
            <div>
                {!! $value !!}
            </div>
        @endforeach
    </section>
@endif

@if(Theme::hasArgument('widgets.content'))
    @foreach(Theme::getContentArgument('widgets.content') as $value)
        {!! $value !!}
    @endforeach
@endif
