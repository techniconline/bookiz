<!-- BEGIN SIDEBAR Menu-->
<!-- BEGIN SIDEBAR -->
@if(Theme::hasArgument('widgets.right_top'))
    @foreach(Theme::getContentArgument('widgets.right_top') as $value)
        {!! $value !!}
    @endforeach
@endif
<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                    <span></span>
                </a>
<span></span>
<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true"
    data-slide-speed="200" style="padding-top: 20px">
	
	
    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
    <li class="sidebar-toggler-wrapper hide">
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="sidebar-toggler">
            <span></span>
        </div>
		
		
		
        <!-- END SIDEBAR TOGGLER BUTTON -->
    </li>
    <!--
    <li class="sidebar-search-wrapper">
        <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
            <a href="javascript:;" class="remove">
                <i class="icon-close"></i>
            </a>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                                        <a href="javascript:;" class="btn submit">
                                            <i class="icon-magnifier"></i>
                                        </a>
                                    </span>
            </div>
        </form>
    </li> -->

    @foreach(Theme::getContentArgument('backend_menus') as $key_section => $sectionValues)
        <li class="heading">
            <h3 class="uppercase bold">{!! trans("core::menu_sections.".$key_section) !!}</h3>
        </li>
        @foreach($sectionValues as $key => $items)
            @if(!isset($items["list"]))
                @continue
            @endif
            <?php
            $isActive = false;
            $isActive = collect($items["list"])->map(function ($value) {
                if ($value["route"] && (request()->routeIs($value["route"]))) {
                    return true;
                }
            })->filter()->isNotEmpty();
            ?>
            <li class="nav-item {!! $isActive?"active open":"" !!} {{--start active open--}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="{!! $items["icon_class"]?:"icon-list" !!}"></i>
                    <span class="title">@lang($items["key_trans"])</span>
                    {!! $isActive?'<span class="selected"></span>':'' !!}
                    <span class="arrow {!! $isActive?"open":"" !!}"></span>
                </a>
                <ul class="sub-menu">
                    @foreach($items["list"] as $menu)
                        <li class="nav-item  {!! $menu["route"]&&request()->routeIs($menu["route"])?"active open":"" !!} {{--start active open--}} {!! $menu["class"] !!}" {!! $menu["id"] !!}>
                            <a target="{!! isset($menu["target"])?$menu['target']:"" !!}" href="{!! $menu["route"]?route($menu["route"]):"#" !!}" class="nav-link">
                                <i class="{!! $menu["icon_class"]?:"icon-link" !!}"></i>
                                <span class="title">@lang($menu["key_trans"])</span>
                                {{--<span class="selected"></span>--}}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>
    @endforeach
@endforeach
</ul>
<!-- END SIDEBAR Menu -->
    <!-- BEGIN SIDEBAR -->
@if(Theme::hasArgument('widgets.right'))
    @foreach(Theme::getContentArgument('widgets.right') as $value)
        {!! $value !!}
    @endforeach
@endif
