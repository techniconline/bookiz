@if(Theme::hasArgument('special_widgets.header'))
	
    @foreach(Theme::getContentArgument('special_widgets.header') as $position => $values)
        @foreach($values as $value)

            {!! $value !!}

        @endforeach
		
		
    @endforeach
	
	<a href="javascript:;" class="menu-toggler TogglePlace responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                    <span></span>
                </a>
<span></span>
@endif