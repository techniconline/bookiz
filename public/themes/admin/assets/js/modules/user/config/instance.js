jQuery(document).ready(function () {
    $('.select2').select2();

    $("#config_form").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden inpu
        errorPlacement: function(error, element) {
            if (element.is(':checkbox')) {
                error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
            } else if (element.is(':radio')) {
                error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        highlight: function(element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function(element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        invalidHandler: function(event, validator) { //display error alert on form submit
            $("#config_form .alert-danger").show();
            App.scrollTo($("#config_form .alert-danger"), -200);
        },

        submitHandler: function(form) {
            $("#config_form .alert-danger").hide();
            form.submit();
        }
    });
});

