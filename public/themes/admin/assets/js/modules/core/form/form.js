var filemanger_url_obj;
var filemanger_url_win;

function formRunner($form_selector) {
    /*select by ajax relation*/
    if ($($form_selector + 'select[data-init="1"]').length) {
        $($form_selector + 'select[data-init="1"]').each(function (index) {
            var parent = $(this).attr('data-parent');
            if (parent) {
                var selectAutoFill = this;
                $('#' + parent).change(function (e) {
                    var value = $(this).val();
                    if (value && value.length) {
                        var data = {};
                        data[parent] = value;
                        autoFillSelectByUrl(selectAutoFill, data);
                    }
                });
                $('#' + parent).trigger('change');
            } else {
                autoFillSelectByUrl(this, {});
            }
        });
    }

    function autoFillSelectByUrl($select, data) {
        var url = $($select).attr('data-fill-url');
        if (!url) {
            console.log('Error: select "' + $($select).attr("name") + '" auto fill configuration fails.');
            return false;
        }
        if (!data) {
            data = {};
        }
        var trigger = $($select).attr('data-change-trigger');
        var selected = $($select).attr('data-value');
        var multiple = false;
        var hasLoding = $($select).parent().children('.loading').length;
        if ($($select)[0].hasAttribute('multiple')) {
            selected = JSON.parse(selected);
            multiple = true;
        }
        if (hasLoding) {
            $($select).parent().children('.loading').removeClass('hidden');
        }
        $.getJSON(url, data, function (items) {
            if (!items.data) {
                console.log('Error: "' + url + '" can not load true data.');
                return false;
            }
            var html = '<option value=""></option>';
            $.each(items.data, function (index) {
                html += '<option value="' + items.data[index].value + '">' + items.data[index].title + '</option>';
            });
            $($select).html(html);
            if (trigger) {
                $($select).change(function (e) {
                    if ($($select).val()) {
                        data[$($select).attr('name')] = $($select).val();
                        autoFillSelectByUrl('#' + trigger, data);
                    }
                });
            }
            if (selected) {
                if (multiple) {
                    $.each(selected, function (index) {
                        $($select).children("option[value='" + selected[index] + "']").prop("selected", true);
                    });
                } else {
                    $($select).children("option[value='" + selected + "']").prop("selected", true);
                }
                $($select).trigger('change');
            }
            if (hasLoding) {
                $($select).parent().children('.loading').addClass('hidden');
            }
        });
    }

    /*select date*/
    if ($($form_selector + 'input[data-date="1"]').length) {
        $('input[data-date="1"]').each(function (index) {
            addDateTimeController($(this));
        });
    }

    function addDateTimeController($input) {
        var parentObj = $($input).parent();
        var isDateTime = $($input).attr('data-time');
        var yearObject = $(parentObj).children("#date-year");
        var monthObject = $(parentObj).children("#date-month");
        var dayObject = $(parentObj).children("#date-day");
        $(yearObject).removeAttr('name');
        $(monthObject).removeAttr('name');
        $(dayObject).removeAttr('name');
        if (isDateTime) {
            var hourObject = $(parentObj).children("#date-hour");
            var minuteObject = $(parentObj).children("#date-minute");
            $(hourObject).removeAttr('name');
            $(minuteObject).removeAttr('name');
        }

        var setInputValue = function ($input, isDateTime, yearObject, monthObject, dayObject, hourObject, minuteObject) {
            var year = $(yearObject).val();
            var month = $(monthObject).val();
            var day = $(dayObject).val();
            if (typeof year == "undefined") {
                year = 0;
            }
            if (typeof month == "undefined") {
                month = 0;
            }
            if (typeof day == "undefined") {
                day = 0;
            }

            var ivalue = year + '-' + month + '-' + day;

            if (isDateTime) {
                var hour = $(hourObject).val();
                var minute = $(minuteObject).val();
                if (typeof hour == "undefined") {
                    hour = 0;
                }
                if (typeof minute == "undefined") {
                    minute = 0;
                }
                ivalue = ivalue + ' ' + hour + ':' + minute + ':00';
            }
            if (ivalue == '--' || ivalue == '-- ::00') {
                ivalue = '';
            }
            $($input).val(ivalue);
            return false;
        };

        $(monthObject).change(function (e) {
            if ($(this).val()) {
                var $days = parseInt($(this).find(":selected").attr('data-days'));
                for ($i = 28; $i <= 31; $i++) {
                    if ($i > $days) {
                        $(dayObject).find('option[value="' + $i + '"]').addClass('hidden');
                    } else {
                        $(dayObject).find('option[value="' + $i + '"]').removeClass('hidden');
                    }
                }
            }
        });

        if (isDateTime) {
            $(yearObject).add(monthObject).add(dayObject).add(hourObject).add(minuteObject).change(function (e) {
                setInputValue($input, isDateTime, yearObject, monthObject, dayObject, hourObject, minuteObject);
            });
            setInputValue($input, isDateTime, yearObject, monthObject, dayObject, hourObject, minuteObject);
        } else {
            $(yearObject).add(monthObject).add(dayObject).change(function (e) {
                setInputValue($input, isDateTime, yearObject, monthObject, dayObject, null, null);
            });
            setInputValue($input, isDateTime, yearObject, monthObject, dayObject, null, null);
        }

    }


    /*select2*/
    if ($($form_selector + '.select2').length) {
        $($form_selector + '.select2').each(function (index) {
            createSelect2(this);
        });
    }

    function createSelect2($select) {
        var $ajax_url = $($select).attr('data-ajax-url');
        var $placeholder = $($select).attr('data-placeholder');
        var $minimumInputLength = $($select).attr('data-minimum-input-length');
        var $templateResult = $($select).attr('data-template-result');
        var $templateSelection = $($select).attr('data-template-selection');
        var $tagsInput = $($select).attr('data-tag');
        var $options = {};
        if ($placeholder) {
            $options['placeholder'] = $placeholder;
        }
        if ($minimumInputLength) {
            $options['minimumInputLength'] = $minimumInputLength;
        }
        if ($templateSelection) {
            $options['templateSelection'] = $templateSelection;
        }
        if ($templateResult) {
            $options['templateResult'] = $templateResult;
        }
        if ($tagsInput) {
            $options['tags'] = true;
            $options['selectOnBlur'] = true;
        }
        if ($ajax_url) {
            var $ajax_values = $($select).attr('data-ajax-values');
            var hasLoding = $($select).parent().children('.loading').length;
            $options['ajax'] = {
                url: $ajax_url,
                dataType: 'json',
                processResults: function (data) {
                    return {
                        results: data.data
                    };
                },
            };
            $($select).select2($options);
            if ($ajax_values) {
                if (hasLoding) {
                    $($select).parent().children('.loading').removeClass('hidden');
                }
                var iSelect = $($select);
                var values = JSON.parse($ajax_values).toString();
                $.ajax({
                    type: 'GET',
                    url: $ajax_url,
                    data: {selected: values}
                }).then(function (data) {
                    $.each(data.data, function (index) {
                        // create the option and append to Select2
                        var option = new Option(data.data[index].text, data.data[index].id, true, true);
                        iSelect.append(option).trigger('change');
                        // manually trigger the `select2:select` event
                        iSelect.trigger({
                            type: 'select2:select',
                            params: {
                                data: data.data[index]
                            }
                        });
                    })
                    if (hasLoding) {
                        $($select).parent().children('.loading').addClass('hidden');
                    }
                });
            }
        } else {
            $($select).select2($options);
        }
    }


    /*editor*/
    if ($($form_selector + '.html-editor').length) {
        $($form_selector + '.html-editor').each(function (index) {
            var id = $(this).attr('id');
            CKEDITOR.replace(id, null);
        });
    }


    /*file manager*/
    if ($($form_selector + '.fileServerInput').length) {
        $($form_selector + '.fileServerInput').each(function (index) {
            var $url = $(this).attr('data-url');
            var $type = $(this).attr('data-type');
            var $folder = $(this).attr('data-folder');
            var $input = $(this).attr('data-input');
            if (!$type) {
                $type = 'all';
            }
            if (!$folder) {
                $folder = '/';
            }
            $url = $url + '&relative_url=1&langCode=en&type=' + $type + '&fldr=' + $folder + '&popup=1&field_id=' + $input;
            $(this).click(function (e) {
                var $input = $(this).attr('data-input');
                filemanger_url_obj = '#'+$input;
                open_popup_window($url, 'FileManager')
            })
        });
    }

    if ($($form_selector + '.fileServerPreview').length) {
        $($form_selector + '.fileServerPreview').each(function (index) {
            $(this).click(function (e) {
                var $input = $(this).attr('data-input');
                var $val = $('#' + $input).val();
                if ($val) {
                    open_popup_window($val, $('#' + $input).attr('name'))
                } else {
                    alert('Input File Is Empty!');
                }
            });
        });
    }

}


function open_popup_window($url,$title) {
    var w = 880;
    var h = 570;
    var l = Math.floor((screen.width - w) / 2);
    var t = Math.floor((screen.height - h) / 2);
    $(window).on('message', handlePostMessage);
    filemanger_url_win = window.open($url, $title, "scrollbars=1,toolbar=no,status=no,resizable=yes,dependent=yes,width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
}

function handlePostMessage(e) {
    var data = e.originalEvent.data;
    console.log('js-data', data);
    if (data.source === 'richfilemanager') {
        console.log(filemanger_url_obj);
        $(filemanger_url_obj).val(data.preview_url);
        filemanger_url_win.close();
    }
    // remove an event handler
    $(window).off('message', handlePostMessage);
}


$(function () {
    formRunner('');
});