var Menu = function () {

    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
        } else {
            output.val('JSON...');
        }
    };

    var changePosition = function (details) {

        var parent = details.sourceEl.parents('.dd-item').first();
        var next = details.sourceEl.next('.dd-item').first().data("id");
        var prev = details.sourceEl.prev('.dd-item').first().data("id");
        var parent_id = parent.data("id");

        console.log(next, prev, parent_id, details);

    };


    return {
        //main function to initiate the module
        init: function () {

/*            $('#nestable_list_category_tree').nestable({
                dropCallback: function (details) {

                    var $inputs = {};
                    var parent = details.sourceEl.parents('.dd-item').first();
                    $inputs["next"] = details.sourceEl.next('.dd-item').first().data("id");
                    $inputs["_token"] = $('div#nestable_list_category_tree').attr("data-token");
                    $inputs["prev"] = details.sourceEl.prev('.dd-item').first().data("id");
                    $inputs["parent_id"] = details.destId;
                    $inputs["source_id"] = details.sourceId;
                    $inputs["_method"] = "PUT";
                    var $href = parent.find('li[data-id="' + details.sourceId + '"] div._change_position').first().attr('change-position-url');

                    $.ajax({
                        url: $href,
                        method: 'PUT',
                        data: $inputs
                        , success: function (data) {

                        }
                    });

                }
                // group: 1
            });*/
            // .on('change', updateOutput);

            updateOutput($('#nestable_list_file_tree').data('output', $('#nestable_list_category_output')));

        }

    };

}();

jQuery(document).ready(function () {
    Menu.init();

    $("body").on("click", "button._copy_files", function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $form = $clicked.parents("form").first();
        var $sourceInput = $form.find('input[name="folder"]:checked');
        var $inputFiles = $sourceInput.parents('div._folder').first().find('div._files input[name="file[]"]');
        var $href = $form.attr("action");
        var $token = $form.find('input[name="_token"]').val();

        $ajaxUpload($href, $token, $sourceInput, $clicked);

        // $.each($inputFiles, function ($index, $file) {
        //
        //     $file = $(this);
        //     console.log($file.val());
        //
        // });
        //
        // console.log($inputFiles, $href, $token);


    });

    $("body").on("click", "a._delete_directory", function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $href = $clicked.attr("href");
        var $confirm_message = $clicked.attr("data-confirm-message");

        var $data = {};
        var $method = "DELETE";
        $data["_method"] = "DELETE";
        $data["directory"] = $clicked.attr("data-directory");
        $data["force"] = true;

        ajaxCall($clicked, $href, $method, $data, $confirm_message, true);

    });

});

var ajaxCall = function ($this, $url, $method, $data, $confirm_message, $refresh) {

    var $resultConfirm = confirm($confirm_message ? $confirm_message : "Are You Sure?");

    if (!$resultConfirm) {
        return false;
    }

    $.ajax({
        method: $method,
        headers: {
            'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
            'ajax': 1
        },
        url: $url,
        data: $data,
        success: function ($result) {

            alert($result.message);
            if ($result.action && $refresh) {
                location.reload();
                return true;
            }


        },
        error: function (e) {
            alert('Error In Process!');
        }
    });
};

var $ajaxUpload = function ($href, $token, $sourceInput, $clicked) {

    var $file = $sourceInput.parents('div._folder').first().find('div._files input[data-uploaded="0"]').first();
    var $group = $sourceInput.parents('div._folder').first().find('div._files').first();
    var $check_has_file = $('div#nestable_list_file_tree').find('input[name="check_has_file"]').first();
    if($check_has_file.prop("checked")){
        $check_has_file = 1;
    }else{
        $check_has_file = 0;
    }
    $group = $group.attr("data-group");

    if (!$file.length) {
        // location.reload();
        $clicked.prop("disabled", false);
        return false;
    }

    var $file_value = $file.val();
    var $last_file = $file.attr("data-last-file");
    $clicked.prop("disabled", true);

    $.ajax({
        url: $href,
        headers: {
            'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
            'ajax': 1
        },
        method: 'POST',
        data: {"file": $file_value,"last_file": $last_file,"_token": $token,"group": $group,"check_has_file": $check_has_file},
        success: function ($result) {
            var $targetMsg = $sourceInput.parents('div._folder').first().find("div._messages");
            var $targetMsg1 = $sourceInput.parents('form').first().find("div.form-actions div._messages");
            $("<div>" + $result.message + "</div>").appendTo($targetMsg);
            $("<div>" + $result.message + "</div>").appendTo($targetMsg1);

            // console.log($result.action);
            if ($result.action) {
                $sourceInput.parents('form').first().find('input[name="file[]"]').filter(function () {
                    return this.value == $file_value
                }).attr("data-uploaded", 1);
                $file.attr("data-uploaded", 1);
            } else {
                $file.attr("data-uploaded", 1);
            }

            $ajaxUpload($href, $token, $sourceInput,$clicked);

        }
        , error: function ($err, $dd, $fff) {
            // console.log($fff);
            alert("Error In Upload System!");
        }
    });

}