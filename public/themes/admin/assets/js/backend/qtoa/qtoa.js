jQuery(document).ready(function ($) {


    $("body").on("change", "select#_model_type_search", function (event) {
        event.preventDefault();
        var $clicked = $(this);

        $('div._model_types').find('select').prop('disabled',true);
        $('div._model_types').fadeOut();
        console.log($clicked.val());

        var $target = $('#_model_'+$clicked.val());
        console.log($target.length);

        $target.find('select').prop('disabled', false);
        $target.fadeIn();

        // var $href = $('option:selected', $clicked).attr('data-url');
        // var $target = $('select#_model_search').attr('data-ajax-url', $href);
        // var $target = $('select#_model_search').attr('ajax--url', $href);

    });

});

var ajaxCall = function ($this, $url, $method, $data, $confirm_message, $refresh) {

    var $resultConfirm = confirm($confirm_message ? $confirm_message : "Are You Sure?");

    if (!$resultConfirm) {
        return false;
    }

    $.ajax({
        method: $method,
        headers: {
            'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
            'ajax': 1
        },
        url: $url,
        data: $data,
        success: function ($result) {

            alert($result.message);
            if ($result.action && $refresh) {
                location.reload();
                return true;
            }


        },
        error: function (e) {
            alert('Error In Process!');
        }
    });
};
