jQuery(document).ready(function () {

    $('body').on('click', 'a._delete_slider_image', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $parent = $clicked.parents('div').first();
        var $href = $clicked.attr("href");
        var $confirm_message = $clicked.attr("data-confirm-message");
        var $method = "DELETE";
        ajaxCall($clicked, $href, $method, [], $confirm_message, true);
    });


    $('body').on('click', 'a._delete', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $href = $clicked.attr("href");
        var $confirm_message = $clicked.attr("data-confirm-message");
        var $method = "DELETE";
        ajaxCall($clicked, $href, $method, [], $confirm_message, true);
    });




});


var ajaxCall = function ($this, $url, $method, $data, $confirm_message, $refresh) {

    $confirm_message = $confirm_message ? $confirm_message : "Are You Sure?";

    swal({
        title:  $confirm_message,
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                method: $method,
                headers: {
                    'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                    'ajax': 1
                },
                url: $url,
                data: $data,
                success: function ($result) {

                    swal($result.message,'','success');
                    if ($result.action && $refresh) {
                        location.reload();
                        return true;
                    }


                },
                error: function (e) {
                    alert('Error In Process!');
                }
            });
        }
    });
};
