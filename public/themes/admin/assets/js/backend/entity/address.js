jQuery(document).ready(function () {


});

var $latitude = document.getElementById('latitude').value;
var $longitude = document.getElementById('longitude').value;
var $zoom = 6;

if ($latitude && $longitude){
    $zoom = 15;
}

if (!$latitude){
    $latitude =24.549;
}

if (!$longitude){
    $longitude =55.182;
}
console.log($latitude);
var map = new google.maps.Map(document.getElementById('map_canvas'), {
    zoom: $zoom,
    center: new google.maps.LatLng($latitude, $longitude),
    mapTypeId: google.maps.MapTypeId.ROADMAP
});

var myMarker = new google.maps.Marker({
    position: new google.maps.LatLng($latitude, $longitude),
    draggable: true
});

google.maps.event.addListener(myMarker, 'dragend', function (evt) {
    document.getElementById('longitude').value = evt.latLng.lng().toFixed(6);
    document.getElementById('latitude').value = evt.latLng.lat().toFixed(6);
    // document.getElementById('current').innerHTML = '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>';
});

google.maps.event.addListener(myMarker, 'dragstart', function (evt) {
    // document.getElementById('current').innerHTML = '<p>Currently dragging marker...</p>';
});

map.setCenter(myMarker.position);
myMarker.setMap(map);