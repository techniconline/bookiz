jQuery(document).ready(function () {

    var $form = $('div#form_feature').find('form').first();
    var $selectBoxType = $form.find('select._types');
    var $isArr = ['text', 'textarea', 'hidden'];

    $('#form_feature').on('click', 'input._custom', function (event) {
        var $clicked = $(this);

        $isArr = ['text', 'textarea', 'hidden'];
        if ($clicked.prop('checked')) {
            $isArr = ['checkbox', 'select', 'selectTag', 'radio'];
        }

        disableSelectOptions($selectBoxType, $isArr, true);


    });

    //disable option is not custom
    if ($('div#form_feature').find('select._types').length == 1) {
        disableSelectOptions($selectBoxType, $isArr, true);
    }

    //disable option is custom
    if ($('div#form_feature').find('input._custom').prop('checked')) {
        $isArr = ['checkbox', 'select','selectTag', 'radio'];
        disableSelectOptions($selectBoxType, $isArr, true);
    }

});

function disableSelectOptions($selectElem, $arrDisabled, $status) {
    var $options = $selectElem.find('option');
    $options.prop('disabled', false).css('display', 'block');
    $options.each(function ($i, $item) {
        $item = $($item);
        if ($.inArray($item.val(), $arrDisabled) !== -1) {
            $item.prop('disabled', $status);
            $item.css('display', 'none');
        }
    });
}