jQuery(document).ready(function ($) {

    $("body").on("click", "a._copy_video_meta_group", function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $href = $clicked.attr("href");
        var $confirm_message = $clicked.attr("data-confirm-message");

        var $data = {};
        var $method = "POST";
        $data["_method"] = "POST";

        ajaxCall($clicked, $href, $method, $data, $confirm_message, true);

    });

    $("body").on("click", "a._del_video_meta_group", function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $href = $clicked.attr("href");
        var $confirm_message = $clicked.attr("data-confirm-message");

        var $data = {};
        var $method = "DELETE";
        $data["_method"] = "DELETE";

        ajaxCall($clicked, $href, $method, $data, $confirm_message, true);

    });

});

var ajaxCall = function ($this, $url, $method, $data, $confirm_message, $refresh) {

    var $resultConfirm = confirm($confirm_message ? $confirm_message : "Are You Sure?");

    if (!$resultConfirm) {
        return false;
    }

    $.ajax({
        method: $method,
        headers: {
            'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
            'ajax': 1
        },
        url: $url,
        data: $data,
        success: function ($result) {

            alert($result.message);
            if ($result.action && $refresh) {
                location.reload();
                return true;
            }


        },
        error: function (e) {
            alert('Error In Process!');
        }
    });
};
