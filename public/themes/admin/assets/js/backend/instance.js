jQuery(document).ready(function () {


    $('#form_instance').on('change', 'select._select_backend_template, select._select_frontend_template', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $form = $clicked.parents('form').first();

        if ($clicked.val() == "") {
            $form.find('select._select_backend_template').prop('disabled', false);
            $form.find('select._select_frontend_template').prop('disabled', false);
            return false;
        }

        if ($clicked.hasClass('_select_backend_template')) {
            $form.find('select._select_backend_template').prop('disabled', false);
            $form.find('select._select_frontend_template').prop('disabled', true);
        } else {
            $form.find('select._select_backend_template').prop('disabled', true);
            $form.find('select._select_frontend_template').prop('disabled', false);
        }

    });


});