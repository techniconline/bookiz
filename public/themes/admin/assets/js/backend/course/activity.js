jQuery(document).ready(function () {



    $('body').on('click', 'a._delete_file', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $parent = $clicked.parents('div.row').first();
        var $href = $clicked.attr("href");
        var $confirm_message = $clicked.attr("data-confirm-message");
        var $data = {};
        var $method = "DELETE";
        $data["_method"] = "DELETE";

        ajaxCall($clicked, $href, $method, $data, $confirm_message, true);

    });


});


var ajaxCall = function ($this, $url, $method, $data, $confirm_message, $refresh) {

    var  $confirm_message = $confirm_message ? $confirm_message : "Are You Sure?";

    swal({
        title:  $confirm_message,
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                method: $method,
                headers: {
                    'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                    'ajax': 1
                },
                url: $url,
                data: $data,
                success: function ($result) {

                    swal($result.message,'','success');
                    if ($result.action && $refresh) {
                        location.reload();
                        return true;
                    }


                },
                error: function (e) {
                    alert('Error In Process!');
                }
            });
        }
    });
};
