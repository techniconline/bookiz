jQuery(document).ready(function () {

    $('#duration_type').change(function(){
        var value=$(this).val();
        if(value=='expierd_duration'){
            $('.duration_box').show();
        }else{
            $('.duration_box').hide();
        }
    });
    $('#duration_type').trigger('change');
    $('form#form-enrollment').on('change', 'select._enrollment', function (event) {
        var $clicked = $(this);
        $clicked = $clicked.find(":selected");
        var $select = $clicked.parents("select").first();
        var $href = $select.attr("data-url");
        var $enrollment = $clicked.val();

        if(+$enrollment==0){
            return false;
        }

        $href = $href.replace("view/0", "view/"+$enrollment);
        console.log($href);
        var $target = $("div#_enrollment_rendered");
        ajaxRenderView($clicked, $href, "GET", $target);
    });


});

var ajaxCall = function ($this, $url, $method, $data, $confirm_message, $refresh) {

    var $resultConfirm = confirm($confirm_message ? $confirm_message : "Are You Sure?");

    if (!$resultConfirm) {
        return false;
    }

    $.ajax({
        method: $method,
        headers: {
            'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
            'ajax': 1
        },
        url: $url,
        data: $data,
        success: function ($result) {

            alert($result.message);
            if ($result.action && $refresh) {
                location.reload();
                return true;
            }


        },
        error: function (e) {
            alert('Error In Process!');
        }
    });
};


var ajaxRenderView = function ($this, $url, $method, $target) {

    $target.addClass("loading");
    $.ajax({
        method: $method,
        headers: {
            'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
            'ajax': 1
        },
        url: $url,
        success: function ($result) {

            if ($result.action && $target) {
                $target.html($result.data);
            }

            $target.removeClass("loading");
        },
        error: function (e) {
            alert('Error In Process!');
            $target.removeClass("loading");
        }
    });
};
