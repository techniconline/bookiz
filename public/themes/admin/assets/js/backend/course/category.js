var Menu = function () {

    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
        } else {
            output.val('JSON...');
        }
    };

    var changePosition = function (details) {

        var parent = details.sourceEl.parents('.dd-item').first();
        var next = details.sourceEl.next('.dd-item').first().data("id");
        var prev = details.sourceEl.prev('.dd-item').first().data("id");
        var parent_id = parent.data("id");

        console.log(next, prev, parent_id, details);

    };


    return {
        //main function to initiate the module
        init: function () {

            $('#nestable_list_category_tree').nestable({
                dropCallback: function (details) {

                    var $inputs = {};
                    var parent = details.sourceEl.parents('.dd-item').first();
                    $inputs["next"] = details.sourceEl.next('.dd-item').first().data("id");
                    $inputs["_token"] = $('div#nestable_list_category_tree').attr("data-token");
                    $inputs["prev"] = details.sourceEl.prev('.dd-item').first().data("id");
                    $inputs["parent_id"] = details.destId;
                    $inputs["source_id"] = details.sourceId;
                    $inputs["_method"] = "PUT";
                    var $href = parent.find('li[data-id="' + details.sourceId + '"] div._change_position').first().attr('change-position-url');

                    $.ajax({
                        url: $href,
                        method: 'PUT',
                        data: $inputs
                        , success: function (data) {

                        }
                    });

                }
                // group: 1
            });
            // .on('change', updateOutput);

            updateOutput($('#nestable_list_category_tree').data('output', $('#nestable_list_category_output')));

        }

    };

}();

jQuery(document).ready(function () {
    Menu.init();

    $('#nestable_list_category_tree').on('click', 'a._add_child', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $parent = $clicked.parents('li').first();
        var $parent_id = $parent.attr("data-id");

        $('#nestable_list_category_tree').find('#form_category').remove();
        var $row = $clicked.parents("li.dd-item").first();
        var $html = $('div#form_category').clone().appendTo($row);
        var $href = $clicked.attr("href");

        //default instance and language for all rows is disabled
        var $is_root = $row.attr('data-is-root');
        var $status_show = 'none';
        var $disabled = true;
        $html.find('div._language_list, div._instance_list').css('display', $status_show)
        $html.find('div._language_list select, div._instance_list select').prop('disabled', $disabled)

        $html.find('form').attr('action', $href);
        $html.find('input._parent_id').val($parent_id);


    });

    $('#nestable_list_category_tree').on('change', 'select._url_type', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $option_selected = $clicked.find(":selected");
        var $form = $('#nestable_list_category_tree div#form_category');
        if ($option_selected.val() == 'route') {
            $form.find('div._route_list').css('display', 'block');
            $form.find('div._url').css('display', 'none');
        } else {
            $form.find('div._route_list').css('display', 'none');
            $form.find('div._url').css('display', 'block');
        }

    });

    $('#nestable_list_category_tree').on('click', 'a._edit', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        $('#nestable_list_category_tree').find('#form_category').remove();
        var $row = $clicked.parents("li.dd-item").first();
        var $href = $clicked.attr("href");
        var $html = $('div#form_category').clone().prependTo($row);

        var $is_root = $row.attr('data-is-root');
        var $status_show = 'none';
        var $disabled = true;
        if ($is_root == "1") {
            $status_show = 'block';
            $disabled = false;
        }

        $html.find('div._language_list, div._instance_list').css('display', $status_show)
        $html.find('div._language_list select, div._instance_list select').prop('disabled', $disabled)

        var $category_id = $row.attr('data-id');
        var $data_model = $json_data[$category_id];
        var $inputs = $html.find('input, select');
        $html.find('form').attr('action', $href);
        $html.find('form').attr('method', 'PUT');
        $html.find('form').find('input[name="_method"]').val('PUT');
        var $allAttributesInput = $html.find('form').find('input[name="attributes*"]');
        $inputs.each(function ($index, $item) {
            var $name = $item.name;
            var $key = null;
            // console.log($name, $name.indexOf("attributes"));
            if ($data_model[$name] && $item.type != 'select-one') {
                $item.value = $data_model[$item.name];
            } else if ($item.type == 'select-one') {
                $($item).prop('selected', true).val($data_model[$name]);
            } else if ($name.indexOf("attributes") >= 0) {
                $key = $name.replace("attributes[","");
                $key = $key.replace("]","");
                $item.value = $data_model["attributes"][$key];
            }

        })

    });

    $('#nestable_list_category_tree').on('click', 'input._save', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $parent = $clicked.parents('li').first();
        var $parent_id = $parent.attr("data-id");

        var $form = $clicked.parents('form').first();
        var $inputs = $form.serialize();
        var $li = $clicked.parents("li.dd-item").first();
        var $href = $form.attr("action");
        var $ol = $li.find('ol').first();

        $.ajax({
            url: $href,
            method: $form.find('input[name="_method"]').val(),
            data: $inputs
            , success: function (data) {

                if (data.action) {

                    if (!data.data.updated) {
                        var $new_row = '';
                        if (!$ol.length) {
                            $new_row += '<ol class="dd-list">'
                        }

                        $new_row += '<li class="dd-item dd3-item" data-id="' + data.data.id + '">' +
                            '<div class="dd-handle dd3-handle _change_position" change-position-url="/"></div>' +
                            '                    <div class="dd3-content">  \n' +
                            '                        <a href="' + data.data.url_edit + '" class="btn btn-xs btn-default blue _edit"><i class="fa fa-edit"></i></a> ' +
                            '                        <a href="' + data.data.url_add_child + '" class="btn btn-xs btn-default green _add_child"><i class="fa fa-plus-circle"></i></a> ' +
                            '                        <a href="' + data.data.url_delete + '" class="btn btn-xs btn-default red _delete"><i class="fa fa-trash"></i></a> ' +
                            '                    ' + data.data.title +
                            '                    </div>' +
                            '</li>';

                        if ($ol.length) {
                            $ol.append($new_row);
                        } else {
                            $new_row += '</ol>'
                            $li.after($new_row);
                        }
                    }

                    $form.find('a._cancel').click();
                } else {
                    alert(data.message)
                }

            }
        });


    });

    $('#nestable_list_category_tree').on('click', 'a._cancel', function (event) {
        event.preventDefault();
        $('#nestable_list_category_tree').find('#form_category').remove();
    });


    $('#nestable_list_category_tree').on('click', 'a._delete', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $href = $clicked.attr("href");
        var $inputs = {};
        $inputs["_token"] = $('div#nestable_list_category_tree').attr("data-token");
        $inputs["_method"] = "DELETE";
        var $li = $clicked.parents("li.dd-item").first();
        var $confirm_message = $clicked.attr('data-text-confirm');
        var $resultConfirm = confirm($confirm_message ? $confirm_message : "Are You Sure?");
        if (!$resultConfirm) {
            return false;
        }

        $.ajax({
            url: $href,
            method: 'DELETE',
            data: $inputs
            , success: function (data) {

                $li.fadeOut();

            }
        });

    })

});