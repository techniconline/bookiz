jQuery(document).ready(function () {

    $('body').on('click', 'a._add_row_course_activity', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $parent = $clicked.parents('div.row').first();
        var $form = $parent.find('#form_activity_item');
        $form.fadeIn();
    });

    $('body').on('click', '#form_activity_item a._save', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $parent = $clicked.parents('div.row').first();
        var $form = $parent.find('#form_activity_item');
        var $href = $clicked.attr("href");
        var $confirm_message = $clicked.attr("data-confirm-message");

        var $data = $form.find('input, select').serialize();
        var $method = "POST";

        ajaxCall($clicked, $href, $method, $data, $confirm_message, true);

    });

    $('body').on('click', 'a._delete_row_course_activity', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $parent = $clicked.parents('div.row').first();
        var $href = $clicked.attr("href");
        var $confirm_message = $clicked.attr("data-confirm-message");
        var $data = {};
        var $method = "DELETE";
        $data["_method"] = "DELETE";

        ajaxCall($clicked, $href, $method, $data, $confirm_message, true);

    });

    $('body div._list_course_sections').on('click', 'a._edit_row_course_section', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $parent = $clicked.parents('div.row').first();
        var $parentTd = $clicked.parents('td').first();
        var $form = $parent.find('form#form_section').first();
        $form.fadeIn();
        var $inputs = $form.find('input, textarea');
        var $href = $clicked.attr("href");
        var $data = $parentTd.find('input, textarea');
        $form.attr('action', $href);

        $.each($data, function ($index, $item) {
            $item = $(this);
            var $name = $item.attr('name');
            if ($name.indexOf('date') >= 0) {
                if($item.val()){
                    setDateTimeSelectBox($item.val(), $name, 'form_section');
                }

            }

            $form.find('[name="' + $name + '"]').val($item.val());
        });
        $form.find('[name="_method"]').val("PUT");

    });

    $('body div._simple_list_course_sections').on('click', 'a._add_row_course_section_simple', function (event) {
        event.preventDefault();
        var $template = $("table#simple_list_course_sections").find("tr._template_row").first();
        var $counter = $template.attr("data-index");
        var $section_template_title = $template.attr("data-title");
        $counter ++;
        var $clone = $template.clone();
        var $body = $("table#simple_list_course_sections").find("tbody");
        $clone.find("td._row_number").html($counter);
        $clone.attr("class","_copied");
        $clone.removeAttr("data-index")
        $clone.find("input[name='title_new[0]']").attr("name","title_new["+$counter+"]").val($section_template_title + " " + $counter);
        $clone.find("input[name='sort_new[0]']").attr("name","sort_new["+$counter+"]").val($counter);
        $body.append($clone.fadeIn());
        $template.attr("data-index", $counter);
    });

    $('body div._simple_list_course_sections').on('click', 'a._edit_row_course_section_simple', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        $clicked.parents("tr").first().find('input').prop("disabled", false);

    });

    $('body div._simple_list_course_sections').on('click', 'a._delete_row_course_section_simple', function (event) {
        event.preventDefault();
        var $template = $("table#simple_list_course_sections").find("tr._template_row").first();
        var $counter = $template.attr("data-index");
        var $count = +$template.attr("data-index-static");
        var $section_template_title = $template.attr("data-title");
        $counter --;
        $template.attr("data-index", $counter);
        var $clicked = $(this);
        $clicked.parents("tr").first().remove();

        // decorate
        var $inputs = $("table#simple_list_course_sections").find("input._new_item");

        var $parent = null;
        $.each($inputs, function ($index, $item) {
            var $row_number = $count + $index;
            $item = $(this);
            $parent = $item.parents('tr').first();
            var $sec_item = $parent.find('input._sort');

            $item.attr('name','title_new['+$index+']').val($section_template_title + " " + $row_number);
            $sec_item.attr('name','sort_new['+$index+']').val($row_number);;

        })


        // decorate

    });

    $('body').on('click', 'a._delete_row_course_section', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $parent = $clicked.parents('div.row').first();
        var $href = $clicked.attr("href");
        var $confirm_message = $clicked.attr("data-confirm-message");
        var $data = {};
        var $method = "DELETE";
        $data["_method"] = "DELETE";

        ajaxCall($clicked, $href, $method, $data, $confirm_message, true);

    });

    $('body').on('click', 'a._add_row_course_section', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $parent = $clicked.parents('div.row._section').first();
        var $form = $parent.find('#form_section');
        $form.attr('action', $form.attr('data-action-default'));
        $form.find('input[type="text"], select, textarea').val("");
        $form.find('[name="_method"]').val("POST");

        $form.fadeIn();
    });

    $('body').on('click', 'a._cancel_section', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $parent = $clicked.parents('div.row._section').first();
        var $form = $parent.find('#form_section');
        $form.attr('action', $form.attr('data-action-default'));
        $form.fadeOut();
    });

    $('body').on('click', '#form_section button._save', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $form = $clicked.parents('form#form_section').first();
        var $href = $form.attr("action");
        var $confirm_message = $clicked.attr("data-confirm-message");

        var $data = $form.find('input, select, textarea').serialize();
        var $method = "POST";
        ajaxCall($clicked, $href, $method, $data, $confirm_message, true);

    });

    $('body').on('click', 'a._edit_sort', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $parent = $clicked.parents('table').first();
        $parent.find('input._sort').prop('disabled', false);
        $clicked.siblings('a._save_sort').fadeIn();
        $clicked.fadeOut();
    });

    $('body').on('click', 'a._save_sort', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $parent = $clicked.parents('table').first();
        var $data = $parent.find('input._sort').serialize();
        $clicked.siblings('a._edit_sort').fadeIn();
        $clicked.fadeOut();
        var $confirm_message = $clicked.attr("data-confirm-message");
        var $href = $clicked.attr("href");
        var $method = "POST";
        $data["_method"] = $method;
        ajaxCall($clicked, $href, $method, $data, $confirm_message, true);
    });

    $('body').on('change', 'select._video_select', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var selected = $clicked.find("option:selected");
        var videoUrl = selected.attr('data-video');
        var videoPlayer = $clicked.parents("div.row").first().find('video').first();
        videoPlayer.find('source').attr('src', videoUrl);
        videoPlayer.load();
    });


    $("body").on("click", "a._copy_course_meta_group", function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $href = $clicked.attr("href");
        var $confirm_message = $clicked.attr("data-confirm-message");

        var $data = {};
        var $method = "POST";
        $data["_method"] = "POST";

        ajaxCall($clicked, $href, $method, $data, $confirm_message, true);

    });

    $("body").on("click", "a._del_course_meta_group", function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $href = $clicked.attr("href");
        var $confirm_message = $clicked.attr("data-confirm-message");

        var $data = {};
        var $method = "DELETE";
        $data["_method"] = "DELETE";

        ajaxCall($clicked, $href, $method, $data, $confirm_message, true);

    });


});

var setDateTimeSelectBox = function ($dateTime, $inputName, $form_id) {
    var $input = null;
    if ($form_id) {
        $input = $('body form#' + $form_id).find('[name="' + $inputName + '"]');
    } else {
        $input = $('body').find('[name="' + $inputName + '"]');
    }

    var $parent = $input.parents("div").first();
    var $dateTimeArr = $dateTime.split(' ');

    var $selectBox = $parent.find('select');

    if ($dateTimeArr[1] != 'undefined') {
        var $timeArr = $dateTimeArr[1].split(":");
        var $h = $timeArr[0];
        var $m = $timeArr[1];
        //var $s = $timeArr[2];
        $.each($selectBox, function ($index, $item) {
            $item = $(this);
            if ($item.hasClass('date-hour')) {
                $item.val($h);
            } else if ($item.hasClass('date-minute')) {
                $item.val($m);
                $item.trigger('change');
            }
        });
    }

    if ($dateTimeArr[0] != "") {
        var $dateArr = $dateTimeArr[0].split("-");
        var $y = $dateArr[0];
        $m = $dateArr[1];
        var $d = $dateArr[2];
        //console.log($selectBox, 22222);

        $.each($selectBox, function ($index, $item) {
            $item = $(this);
            if ($item.hasClass('date-year')) {
                $item.val($y);
            } else if ($item.hasClass('date-month')) {
                $item.val($m);
            } else if ($item.hasClass('date-day')) {
                $item.val($d);
                $item.trigger('change');
            }
        });

    }

}

var ajaxCall = function ($this, $url, $method, $data, $confirm_message, $refresh) {

    var  $confirm_message = $confirm_message ? $confirm_message : "Are You Sure?";

    swal({
        title:  $confirm_message,
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                method: $method,
                headers: {
                    'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                    'ajax': 1
                },
                url: $url,
                data: $data,
                success: function ($result) {

                    swal($result.message,'','success');
                    if ($result.action && $refresh) {
                        location.reload();
                        return true;
                    }


                },
                error: function (e) {
                    alert('Error In Process!');
                }
            });
        }
    });
};
