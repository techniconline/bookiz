/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};

CKEDITOR.on('dialogDefinition', function (event)
{
    var editor = event.editor;
    var dialogDefinition = event.data.definition;
    var dialogName = event.data.name;

    var cleanUpFuncRef = CKEDITOR.tools.addFunction(function ()
    {
        // Do the clean-up of filemanager here (called when an image was selected or cancel was clicked)
        $('#fm-iframe').remove();
        $("body").css("overflow-y", "scroll");
    });

    var tabCount = dialogDefinition.contents.length;
    for (var i = 0; i < tabCount; i++) {
        var dialogTab = dialogDefinition.contents[i];
        if (!(dialogTab && typeof dialogTab.get === 'function')) {
            continue;
        }

        var browseButton = dialogTab.get('browse');
        if (browseButton !== null) {
            browseButton.hidden = false;
            browseButton.onClick = function (dialog, i) {
                editor._.filebrowserSe = this;
                var iframe = $("<iframe id='fm-iframe' class='fm-modal'/>").attr({
                    src: '/plugins/filemanager/index.html' + // Change it to wherever  Filemanager is stored.
                    '?CKEditorFuncNum=' + CKEDITOR.instances[event.editor.name]._.filebrowserFn +
                    '&CKEditorCleanUpFuncNum=' + cleanUpFuncRef +
                    '&langCode=en' +
                    '&CKEditor=' + event.editor.name
                });

                $("body").append(iframe);
                $("body").css("overflow-y", "hidden");  // Get rid of possible scrollbars in containing document
            }
        }
    }
});
