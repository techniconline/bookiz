<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists, this
    | is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities this is cool
    | feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => array(

        'before' => function ($theme) {
            $theme->setTitle('Techniconline');
            $theme->setAuthor('Techniconline');
        },

        'asset' => function ($asset) {

            $language = app('getInstanceObject')->getLanguage();
            $direction = $language ? $language->direction : null;

            $path = 'themes/default/assets/';
//            $asset->container('up_theme_js')->themePath()->add('app.bundle', 'dist/app.bundle.js?v=1.js');
//            $asset->container('up_theme_js')->add('bootstrap_js', asset( $path.'js/bootstrap.min.js'));
//            $asset->container('up_theme_js')->add('owl.carousel', asset( $path.'global/plugins/owlcarousel/owl.carousel.min.js'));
            $asset->container('up_theme_js')->themePath()->add('jquery', ('js/main/jquery-3.3.1.min.js'));

            $asset->container('up_theme_js')->themePath()->add('map-box-api', 'js/main/mapbox-gl.js?v=1.js');
            $asset->container('theme_style')->themePath()->add('map-box-api-css','dist/css/mapbox-gl.css?v=1.css');


//            $asset->add('sweetalert', asset('/assets/global/plugins/sweetalert/sweetalert2.min.css'));
//            $asset->container('up_theme_js')->add('sweetalert', asset('/assets/global/plugins/sweetalert/sweetalert2.min.js'));

            $asset->themePath()->add([

//                ['carousel', 'dist/css/owl.carousel.min.css'],
//                ['default', 'dist/css/owl.theme.default.min.css'],
                ['fontawesome', 'fontawesome/css/all.css'],
                ['fontiran', 'fonts/fontiran.css'], //TODO
//                ['css_app', 'dist/css/app.css?v=1.css'],
                ['fontawesome-js', 'fontawesome/js/all.js'],
//                ['jquery-js', 'js/jquery-3.3.1.min.js'],
                ['bootstrap-js', 'js/main/bootstrap.min.js'],
                ['jsCalendar-js', 'js/main/jsCalendar.js'],
                ['masonry-js', 'js/main/masonry.min.js'],
                ['owl-carousel-min-js', 'js/main/owl.carousel.min.js'],
                ['components', 'js/main/components.js?v=1.js'],
                ['js_app', 'js/main/main.js?v=1.js'],
//                ['js_app', 'dist/app.bundle.js?v=1.js'],
            ]);

//            $asset->add('js-scripts', '/js/scripts.js');
            if (strtolower($direction) == "rtl") {
//                $asset->add('components', asset('themes/admin/assets/global/css/components-rtl.min.css'));
                $asset->themePath()->add([
//                    ['fontiran', 'fonts/fontiran.css'],
//                    ['css_app', 'dist/css/app_rtl.css?v=1.css'],
                    ['css_app', 'dist/css/style_rtl.css?v=1.css'],
                    ]);
            } else {
                $asset->themePath()->add([
//                    ['fontawesome', 'fontawesome/css/all.css'],
//                    ['css_app', 'dist/css/app.css?v=1.css'],
                    ['css_app', 'dist/css/style_ltr.css?v=1.css'],
                ]);
            }

            $asset->cook('backbone', function ($asset)  {
                //$asset->add('googleapis', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false');

                //TODO
//                $asset->container('theme_style')->add('map-box-api-css','https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css');
//                $asset->container('up_theme_js')->add('map-box-api', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js');

//                $asset->container('up_theme_js')->add('map-box-es6', 'https://unpkg.com/es6-promise@4.2.4/dist/es6-promise.auto.min.js');
//                $asset->container('up_theme_js')->add('map-box-sdk', 'https://unpkg.com/@mapbox/mapbox-sdk/umd/mapbox-sdk.min.js');

            });


        },


        'beforeRenderTheme' => function ($theme) {
            $theme->asset()->serve('backbone');
            // To render partial composer
            /*

            $theme->partialComposer('header', function($view){
                $view->with('auth', Auth::user());
            });
            */
        },

        'beforeRenderLayout' => array(

            'mobile' => function ($theme) {
                // $theme->asset()->themePath()->add('ipad', 'css/layouts/ipad.css');
            }

        )

    )

);