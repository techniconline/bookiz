{!! meta_init() !!}
<link rel="icon" href="/logo-fav.png?v=1.2">
<meta name="keywords" content="@get('keywords')">
<meta name="description" content="@get('description')">
<meta name="author" content="@get('author')">

<title>@get('title')</title>

@styles()
@styles('block_style')
@styles('general_style')
@styles('theme_style')

@scripts('up_theme_js')
@scripts('up_js')

