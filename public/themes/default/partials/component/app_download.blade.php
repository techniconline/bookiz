@if(in_array(getOS(), ['Android']) && \Illuminate\Support\Facades\Lang::has('core::messages.os_alert.'.getOS()))
    <div class="FullWidth verify-danger">
        <div class="container verify-container">
            <div class="alert col-lg-12 col-md-12 verify-box dlappbox">
			                <a href="javascript:void(0);" title="بستن" class="close CloseY"><i class="fa fa-times"></i></a>

                <p>
                    <img width="40px" src="{!! asset('assets/img/logo_app_download.png') !!}" alt="">
                    {!! trans('core::messages.os_alert.'.getOS()) !!}
                    <a class="btn btn-success" href="{{ url('/core/page/show/application') }}">
                        @lang('core::messages.os_alert.download')
                    </a>
                </p>
            </div>
        </div>
    </div>
@endif


<script>

    function getMobileOperatingSystem() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        //console.log(userAgent);
        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/Linux/i.test(userAgent)) {
            return "Linux";
        }

        if (/Mac OS X/i.test(userAgent)) {
            return "MacOS";
        }

        if (/android/i.test(userAgent)) {
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            return "iOS";
        }

        return "unknown";
    }

    var os = getMobileOperatingSystem();
    //console.log(os);
</script>