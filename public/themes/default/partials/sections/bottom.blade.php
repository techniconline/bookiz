@if(Theme::hasArgument('widgets.bottom'))
    <section id="_bottom">
        @foreach(Theme::getContentArgument('widgets.bottom') as $value)
            <div>
                {!! $value !!}
            </div>
        @endforeach
    </section>
@endif