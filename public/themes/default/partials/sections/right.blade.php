@if(Theme::hasArgument('widgets.right'))
    <section id="_right">
        @foreach(Theme::getContentArgument('widgets.right') as $value)
            <div>
                {!! $value !!}
            </div>
        @endforeach
    </section>
@endif