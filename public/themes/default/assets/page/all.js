$(document).ready(function () {
    if($('.owl-carousel').length){
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            rtl: true,
            margin: 10,
            nav: true,
            loop: false,
            rewind: true ,

            responsive: {
                0: {   items: 1   },
                600: {    items: 2  },
                700: {    items: 3   },
                1000: {    items: 4   }  }
        })
    }
    if($('.owl-Four').length) {

        $('.owl-Four').owlCarousel({
            loop: true,
            rtl: true,
            play: true,
            margin: 10,
            nav: true,
            responsive: 
			{
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 6   }
            }
        });
    }
	
	
	 if($('.owl-Three').length) {

        $('.owl-Three').owlCarousel({
            loop: true,
            rtl: true,
            play: true,
            margin: 10,
            nav: true,
            responsive: {
                  0: {    items: 1  },
                600: {    items: 1  },
                700: {    items: 1  },
               1000: {    items: 2  } ,  
			   1200: {    items: 3  } 
            }
        });
    }
	
}) 



 
            // Select all links with hashes
            $('.smooth_link > a[href*="#"]')
                // Remove links that don't actually link to anything
                .not('[href="#"]')
                .not('[href="#0"]')
                .click(function (event) {
                    // On-page links
                    if (
                        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                        &&
                        location.hostname == this.hostname
                    ) {
                        // Figure out element to scroll to
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        // Does a scroll target exist?
                        if (target.length) {
                            // Only prevent default if animation is actually gonna happen
                            event.preventDefault();
                            $('html, body').animate({
                                scrollTop: target.offset().top
                            }, 1000, function () {
                                // Callback after animation
                                // Must change focus!
                                var $target = $(target);
                                $target.focus();
                                if ($target.is(":focus")) { // Checking if the target was focused
                                    return false;
                                } else {
                                    $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                    $target.focus(); // Set focus again
                                };
                            });
                        }
                    }
                });
      

