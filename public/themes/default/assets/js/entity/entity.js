jQuery(document).ready(function () {

    if (typeof $data !== 'undefined') {
        $($data).each(function ($index, $item) {
            if ($item !== undefined) {
                // addMarker($item.latitude, $item.longitude, $item);
            }

        });
    }


    $('body .pagination11').on('click', 'a', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $href = $clicked.attr("href");
        var $method = "GET";
        var $target = $('body').find('div.entities');
        var $dataRequest = {paginate: 1};

        ajaxCallSimple($clicked, $href, $method, $dataRequest, false, $target);
    });


    $('body').on('click', 'a._add_cart', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $href = $clicked.attr("href");
        var $method = "POST";
        var $target = null;
        var $dataRequest = {};
        $clicked.parents('li').first().addClass('selected');
        ajaxCallSimple($clicked, $href, $method, $dataRequest, false, $target);
    });


});

(function(page) {
    const p = '.' + page + ' ';

    /* ===== Category Services ===== */
    var category = $(p+'.categories li');
    category.click(function(){
        var type = $(this).data('type');
        var content = $('.category-contents .category-content');
        category.removeClass('active');
        content.removeClass('active');
        $(this).addClass('active');
        content.each(function() {
            if($(this).data('category') === type) {
                $(this).addClass('active');
            }
        });
    });

    /* ===== Basket ===== */
    let basket = [];
    let totla_price = 0;

    $(p+'.services-list1 li a').click(function (e) {
        e.preventDefault();
        const id = $(this).parent().data('id');
        const price = Number($(this).parent().data('price'));
        const ind = basket.indexOf(id);
        if(ind === -1) {
            basket.push(id);
            $(p+'.services-list li[data-id="'+id+'"]').each(function () {
                $(this).addClass('selected');
            });
            totla_price += price;
        }else {
            basket.splice(ind, 1);
            $(p+'.services-list li[data-id="'+id+'"]').each(function () {
                $(this).removeClass('selected');
            });
            totla_price -= price;
        }

        $(p+'.basket .price').text(totla_price + '$');
        $(p+'.basket .count').text(basket.length);
        $(p+'.basket').addClass('selected');
        if(basket.length > 0 ) {
        }else {
            $(p+'.basket').removeClass('selected');
        }
    });

})('single-page');

// var $latitude = 0;
// var $longitude = 0;
// var $zoom = 6;
// var latlngbounds = new google.maps.LatLngBounds();
//
// var map = new google.maps.Map(document.getElementById('map'), {
//     zoom: $zoom,
//     center: new google.maps.LatLng($latitude, $longitude),
//     mapTypeId: google.maps.MapTypeId.ROADMAP
// });

var ajaxCallSimple = function ($this, $url, $method, $data, $refresh, $target) {

    $.ajax({
        method: $method,
        headers: {
            'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
            'ajax': 1
        },
        url: $url,
        data: $data,
        success: function ($result) {

            if($result.action){

                if($('.basket-info .count').length){
                    $('.basket-info .count').parents('.basket').first().addClass('selected');
                    $('.basket-info .count').html($result.data.count);
                }

                if ($result.html) {
                    $target.append($result.html);
                    return true
                }

                if ($result.action && $refresh) {
                    location.reload();
                    return true;
                }

                swal({
                    type: 'success',
                    title:'',
                    text:$result.message,
                    // footer:$result.data.buttons,
                    showConfirmButton:false,
                    showCloseButton: true,
                    showCancelButton: false,
                });
            }else{
                swal({
                    type: 'error',
                    title:'',
                    text:$result.message,
                    showConfirmButton:false,
                    showCloseButton: true,
                    showCancelButton: false,
                });
            }





        },
        error: function (e) {
            alert('Error In Process!');
        }
    });

};

var ajaxCall = function ($this, $url, $method, $data, $confirm_message, $refresh) {

    $confirm_message = $confirm_message ? $confirm_message : "Are You Sure?";

    swal({
        title: $confirm_message,
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                method: $method,
                headers: {
                    'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                    'ajax': 1
                },
                url: $url,
                data: $data,
                success: function ($result) {

                    swal($result.message, '', 'success');
                    if ($result.action && $refresh) {
                        location.reload();
                        return true;
                    }


                },
                error: function (e) {
                    alert('Error In Process!');
                }
            });
        }
    });
};

function addMarker($latitude, $longitude, $data) {

    var $title = $data.title;
    var $latLng = new google.maps.LatLng($latitude, $longitude);
    var myMarker = new google.maps.Marker({
        position: $latLng,
        draggable: false,
        title: $title
    });

    latlngbounds.extend($latLng);

    map.fitBounds(latlngbounds);
    map.setCenter(myMarker.position);
    myMarker.setMap(map);
}

function addMarkerSimple($latitude, $longitude, $data) {

    var $title = $data.title;
    var $latLng = new google.maps.LatLng($latitude, $longitude);
    var myMarker = new google.maps.Marker({
        position: $latLng,
        draggable: false,
        title: $title
    });

    map.setCenter(myMarker.position);
    map.setZoom(16);
    myMarker.setMap(map);
}
