(function(page) {
    if(typeof mapboxgl !== 'undefined') {
        mapboxgl.accessToken = 'pk.eyJ1IjoiYWxpbTEyNSIsImEiOiJjampidXFnYmcxbHV3M3FwMnRwem0xanNnIn0.LU1hZp2RQAl7uw41Uy6VyA';
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [-77.04, 38.907],
            zoom: 11.15
        });

        var geojson = {
            "type": "FeatureCollection",
            "features": [{
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [-77.04, 38.907]
                }
            }]
        };
        map.on('load', function() {
            map.addSource('point', {
                "type": "geojson",
                "data": geojson
            });

            map.addLayer({
                "id": "point",
                "type": "circle",
                "source": "point",
                "paint": {
                    "circle-radius": 8,
                    "circle-color": "#F06292"
                }
            });

            map.on('click', 'point', function() {
                console.log('clicked')
            });

        });
    }


})('list-page');