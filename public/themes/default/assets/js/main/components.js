// var $ = require('jquery');
// var jQueryBridget = require('jquery-bridget');
// var Masonry = require('masonry-layout');
// // make Masonry a jQuery plugin
// jQueryBridget( 'masonry', Masonry, $ );

/* ===== Owl Carousel 5 Items ===== */
$('.owl-carousel.owl-5').owlCarousel({
    loop: false,
    margin: 0,
    responsiveClass: true,
    nav: false,
    lazyLoad: true,
    responsive:{
        0: { items: 2 },
        600: { items: 3 },
        1000: { items: 4 },
        1200: { items: 5 },
        1600: { items: 6 }
    }
});

/* ===== Owl Carousel 2 Items ===== */
$('.owl-carousel.owl-2').owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    nav: false,
    dots: false,
    lazyLoad: true,
    autoplay: true,
    autoplayHoverPause: true,
    responsive:{
        0: { items: 1 },
        600: { items: 2 },
    }
});

/* ===== Masonry ===== */
$('.grid').masonry({
    // options
    columnWidth: 80
});