/*
*
*   #Layout
*       #Navigation Home Burger
*       #Header Page Account
*       #Menu and Submenu
*       #Search autocomplete
*   #Home
*   #List
*   #Single
*       #Category Services
*   #Checkout
*   #Checkout2
*
* */


/* ===== #Layout Start =====  */

/* ===== #Layout background ===== */
$('body').on('click', '.menu-layout', function() {
    $('.header-container .header-links > li').removeClass('active');
    $('.header-container .header-links > li .submenu').css({'opacity': 0});
    $('.header-container .account').removeClass('active');
    $('.header-container .header .search-container').removeClass('active');
    $('.head-menu').removeClass('open-sidebar');
    $('.menu-layout').remove();
});

/* ===== #Navigation Home Burger ===== */
$('.home-page .navigation .toggle-menu').click(function() {
    const menu = $(this).next();
    menu.toggleClass('active');
    menu.click(function() {
        $(this).removeClass('active');
    });
});

/* ===== #Header Page Account ===== */
$('.header-container .account').click(function() {
    if(!$(this).hasClass('active')) {
        $(this).addClass('active');
        if($('.menu-layout').length === 0) $('body').append('<div class="menu-layout"></div>');
    }else {
        $(this).removeClass('active');
        $('.menu-layout').remove();
    }
});

/* ===== #Menu and Submenu ===== */
$('.header-container .header-links > li > a').click(function(e) {
    if($(this).parent().find('.submenu').length > 0) {
        e.preventDefault();
        if(!$(this).parent().hasClass('active')) {
            $('.header-container .header-links > li').removeClass('active');
            $(this).parent().addClass('active');
            setTimeout(() => {
                $('.header-container .header-links > li .submenu').css({'opacity': 0});
                $(this).parent().find('.submenu').css({'opacity': 1});
            }, 100);
            if($('.menu-layout').length === 0) $('body').append('<div class="menu-layout"></div>');
        }else {
            $('.menu-layout').remove();
            $(this).parent().removeClass('active');
            $(this).parent().find('.submenu').css({'opacity': 0});
        }
    }
});

/* ===== #Menu and Submenu ===== */
$('#head-sidebar-btn').click(function(){
    if(!$('.head-menu').hasClass('open-sidebar')) {
        $('.head-menu').addClass('open-sidebar');
        if($('.menu-layout').length === 0) $('body').append('<div class="menu-layout"></div>');
    }else {
        $('.menu-layout').remove();
        $('.head-menu').removeClass('open-sidebar');
    }
});
$('#sidebar-close-button').click(function(){
    $('.menu-layout').remove();
    $('.head-menu').removeClass('open-sidebar');
});

/* ===== #Search autocomplete ===== */
$('.header-container .header .search-container input').on('keyup', function() {
    const $this = $(this);
    if($(this).val().length > 1) {
        const url = 'http://api.beautyday.ir/entity/search?text=%D8%AD%D9%85%DB%8C%D8%AF&wstoken=e8c37c4ac2a3ab2d202cc8bc12584fd05521ee69';
        $.ajax({url: url}).done(function(){
            $this.parent().addClass('active');
            if($('.menu-layout').length === 0) $('body').append('<div class="menu-layout"></div>');
        });
    }else {
        $(this).parent().removeClass('active');
        $('.menu-layout').remove();
    }
    
});

/* ===== #Layout End =====  */

/* ===== #Home Start =====  */
(function(page) {
    const p = '.' + page + ' ';

})('home-page');
/* ===== #Home End =====  */

/* ===== #List Start =====  */
(function(page) {
    if(typeof mapboxgl !== 'undefined' && $('#map1').length === 1) {

        var $features = [];
        $size = 40;

        mapboxgl.accessToken = 'pk.eyJ1IjoidGVjaG5pY29ubGluZSIsImEiOiJjanN3b3I4bWowZWt2NDN0MzZ1dWVuZDJnIn0.fdtQtg1NYhPdHC3nu-89Ug';
        mapboxgl.setRTLTextPlugin('https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.2.0/mapbox-gl-rtl-text.js');

        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            center: [51.41266000, 35.73579000],
            zoom: 10
        });

        $('.entities .entity').each(function(){
            const data = $(this).data('map');
            $features.push({
                "type": "Feature",
                "properties": {
                    "message": data.title,
                    "address": data.address,
                    "image": data.image,
                    "icon": "http://beautyday.ir/assets/img/marker-blue-1.jpg",
                    "iconSize": [$size, $size]
                },
                "geometry": {
                    "type": "Point",
                    "coordinates": [data.longitude, data.latitude]
                }
            });
        });
        var geojson = {
            "type": "FeatureCollection",
            "features": $features
        };
        map.on('load', function() {
            var bounds = new mapboxgl.LngLatBounds();
            map.addControl(new mapboxgl.NavigationControl());

            geojson.features.forEach(function (marker) {
                // create a DOM element for the marker
                var el = document.createElement('div');

                bounds.extend(marker.geometry.coordinates);
                el.addEventListener('click', function () {
                    window.alert(marker.properties.message);
                });
                // add marker to map // TODO add el in Marker()  !!!!
                new mapboxgl.Marker()
                    .setLngLat(marker.geometry.coordinates)
                    .addTo(map);

            });
            map.fitBounds(bounds);
        });
    }

    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
    
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };
    //
    // function entity_item(entity) {
    //     var rate = Number(entity.rate.data_rate);
    //     var address = '';
    //     if(entity.entity_address !== null) {
    //         address = entity.entity_address.address;
    //         new mapboxgl.Marker()
    //         .setLngLat([entity.entity_address.longitude, entity.entity_address.latitude])
    //         .addTo(map);
    //     }
    //
    //     var entity_html = `
    //         <div class="entity" data-id="${entity.id}">
    //             <div class="img-container">
    //                 <img src="${entity.media_image.url_image}" alt="" />
    //                 <div class="rate">
    //                     <i class="fas fa-star ${(rate > 0.5) ? 'active': ''}"></i>
    //                     <i class="fas fa-star ${(rate > 1.5) ? 'active': ''}"></i>
    //                     <i class="fas fa-star ${(rate > 2.5) ? 'active': ''}"></i>
    //                     <i class="fas fa-star ${(rate > 3.5) ? 'active': ''}"></i>
    //                     <i class="fas fa-star ${(rate > 4.5) ? 'active': ''}"></i>
    //                 </div>
    //             </div>
    //             <div class="details">
    //                 <h3>${entity.title}</h3>
    //                 <p class="address">${address}</p>
    //                 <a href="#" class="more">
    //                     MORE
    //                     <i class="fas fa-chevron-left"></i>
    //                 </a>
    //             </div>
    //         </div>`;
    //         $(entity_html).insertBefore('.entities .pagination-list');
    // }
    //
    // const text = getUrlParameter('text');
    // var url = 'http://api.beautyday.ir/entity/search?text='+text+'&wstoken=e8c37c4ac2a3ab2d202cc8bc12584fd05521ee69';
    //
    // $('#nextPage').click(function(){
    //     $this = $(this);
    //     if(url !== null) {
    //         $.get(url).done(function(response){
    //             if(response.action) {
    //                 if(response.data.next_page_url === null) $this.remove();
    //                 url = response.data.next_page_url;
    //                 response.data.data.map(function(entity){
    //                     entity_item(entity);
    //                 });
    //             }
    //         });
    //     }
    // });
    
})('list-page');
/* ===== #List End =====  */

/* ===== #Single Start =====  */
(function(page) {
    const p = '.' + page + ' ';

    /* ===== #Category Services ===== */
    var category = $(p+'.categories li');
    category.click(function(){
        var type = $(this).data('type');
        var content = $('.category-contents .category-content');
        category.removeClass('active');
        content.removeClass('active');
        $(this).addClass('active');
        content.each(function() {
            if($(this).data('category') === type) {
                $(this).addClass('active');
            }
        });
    });

    /* ===== #Basket ===== */
    let basket = [];
    let totla_price = 0;

    $(p+'.services-list li a').click(function (e) {
        e.preventDefault();
        const id = $(this).parent().data('id');
        const price = Number($(this).parent().data('price'));
        const ind = basket.indexOf(id);
        if(ind === -1) {
            basket.push(id);
            $(p+'.services-list li[data-id="'+id+'"]').each(function () {
                $(this).addClass('selected');
            });
            totla_price += price;
        }else {
            basket.splice(ind, 1);
            $(p+'.services-list li[data-id="'+id+'"]').each(function () {
                $(this).removeClass('selected');
            });
            totla_price -= price;
        }

        $(p+'.basket .price').text(totla_price + '$');
        $(p+'.basket .count').text(basket.length);
        $(p+'.basket').addClass('selected');
        if(basket.length > 0 ) {
        }else {
            $(p+'.basket').removeClass('selected');
        }
    });

})('single-page');
/* ===== #Single End =====  */

/* ===== #Checkout Start =====  */
(function(page) {
    const p = '.' + page + ' ';

    /* ===== Category Services ===== */
    var select_time = $(p+'.select-times li');
    select_time.click(function(){
        select_time.removeClass('active');
        $(this).addClass('active');
    });

    // Create the calendar
    const calendarEl = document.getElementById("date-calendar");

    // Get Selected Dates By HTML
    let selected_dates = $("#date-calendar").data('dates');
    $("#date-calendar").removeAttr('data-dates');

    if(calendarEl) {
        const calendar = jsCalendar.new(calendarEl);
        if(selected_dates.length > 0)
            calendar.set(selected_dates[0]);

        calendar.select(selected_dates);

        // Get Selected Dates By AJAX
        $.get("url").done(function(result) {
            selected_dates=[result];
        }).fail(function(error) {
            console.log("error", error);
        });

        // Add events
        calendar.onDateClick(function(event, date){
            // Update calendar date
            console.log('date click', event, date);
            let day = date.getDate();
            day = (day < 10) ? '0'+day : day;
            let month = date.getMonth()+1;
            month = (month < 10) ? '0'+month : month;
            let year = date.getFullYear();
            year = (year < 10) ? '0'+year : year;
            if(selected_dates.indexOf(day+'/'+month+'/'+year) !== -1) {
                calendar.set(date);
            }else {
                const content = `This date is not set. Please choose other date.`;
                $(".toast").html(content);
                $(".toast").addClass('active');
                setTimeout(function(){
                    $(".toast").removeClass('active');
                }, 2000);
            }
        });
    }

})('checkout-page');
/* ===== #Checkout End  =====  */

/* ===== #Checkout2 Start  =====  */
(function(page) {
    const p = '.' + page + ' ';

    $(p+'#forYou input').change(function(e) {
        console.log(e.target.value);
        if(Number(e.target.value) === 1) {
            $(p+'.guest-name').show();
        }else {
            $(p+'.guest-name').hide();
        }
    });

})('checkout-step2');
/* ===== #Checkout2 End  =====  */