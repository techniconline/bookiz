{!! $blockViewModel->getConfig('before_html') !!}
<div class="{{ $blockViewModel->getConfig('class_div') }}">
        @if($blockViewModel->getConfig('show_image'))
            <img src="{{  $blockViewModel->getConfig('logo') }}" alt="{{  $blockViewModel->getConfig('title') }}"
                 class="{{$blockViewModel->getConfig('class_image')}}"/>
        @endif
        @if($blockViewModel->getConfig('show_title'))
            <h4>
                {{  $blockViewModel->getConfig('title') }}
            </h4>
        @endif

        @if($blockViewModel->getConfig('show_links'))
            @for($i=1;$i<=3;$i++)
                @if($blockViewModel->getConfig('link_title_'.$i))
                    @if($i>1)
                        <h6>
                            /
                        </h6>
                    @endif
                    <h2>
                        <a href="{{$blockViewModel->getConfig('link_url_'.$i)}}">{{$blockViewModel->getConfig('link_title_'.$i)}}</a>
                    </h2>
                @endif
            @endfor
        @endif

        @if($blockViewModel->getConfig('show_phone'))
            <div class="phoneLine">
                @lang('core::form.fields.phone')
                <span>{{BridgeHelper::getConfig()->getSettings('phone','instance','core')}} </span>
            </div>
        @endif

        @if($blockViewModel->getConfig('show_email'))
            <div class="address">
                @lang('core::form.fields.email')
                <span>{{BridgeHelper::getConfig()->getSettings('email','instance','core')}} </span>
            </div>
        @endif
        @if($blockViewModel->getConfig('show_address'))
            <div class="address">
                {{ BridgeHelper::getConfig()->getSettings('address','instance','core') }}
            </div>
        @endif
        @if($blockViewModel->getConfig('socials_active'))
            <ul class="socials">
                @foreach(trans('core::data.socials') as $name=>$trans)
                    @if($blockViewModel->getConfig('socials_show_'.$name) && $url=BridgeHelper::getConfig()->getSettings($name,'instance','core'))
                        <li>
                            <a href="{{ $url }}" target="_blank"><i class="fab fa-{{$name}}"></i></a>
                        </li>
                    @endif
                @endforeach
            </ul>
    @endif
</div>
{!! $blockViewModel->getConfig('after_html') !!}