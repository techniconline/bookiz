{!! $blockViewModel->getConfig('before_html') !!}
@php $hasLink=$blockViewModel->getConfig('homeLink') @endphp

<div class="{!! ($blockViewModel->getConfig('class'))?$blockViewModel->getConfig('class'):'logo-container' !!}">
    @if($hasLink)
        <a href="{{ route('index') }}">
    @endif
            <img src="{{ $blockViewModel->getLogoSrc() }}" alt="">
    @if($hasLink)
        </a>
    @endif
</div>

{!! $blockViewModel->getConfig('after_html') !!}