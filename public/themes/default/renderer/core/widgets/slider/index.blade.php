{!! $blockViewModel->getConfig('before_html') !!}
<div id="carousel" class="carousel slide" data-ride="carousel">
    {{--@if($blockViewModel->getConfig('is_mobile_box'))--}}
    {{--@else--}}
    <!-- Indicators -->
        <ul class="carousel-indicators">
        @for($i=1;$i<=6;$i++)
            @if($blockViewModel->getConfig('image_url_'.$i))
                    <li data-target="#carousel" data-slide-to="{!! $i !!}" class="{!! $i>1?'':'active' !!}"></li>
            @endif
        @endfor
        </ul>

        <!-- The slideshow -->
        <div class="carousel-inner">
            @for($i=1;$i<=6;$i++)
            @if($blockViewModel->getConfig('image_url_'.$i))
                    <div class="carousel-item {!! $i>1?'':'active' !!}">

                        <img src="{{ $blockViewModel->getConfig('image_url_'.$i) }}" alt="">
                        <div class="carousel-caption">
                            @if($blockViewModel->getConfig('caption_'.$i))
                                @if($blockViewModel->getConfig('link_url_1'.$i))
                                    <a href="{!! $blockViewModel->getConfig('link_url_1'.$i) !!}">
                                @endif
                                        <h3>{{ $blockViewModel->getConfig('caption_'.$i) }}</h3>
                                @if($blockViewModel->getConfig('link_url_1'.$i))
                                    </a>
                                @endif
                            @endif

                            @if($blockViewModel->getConfig('description_'.$i))
                            <p>{{ $blockViewModel->getConfig('description_'.$i) }}</p>
                            @endif
                        </div>
                    </div>
            @endif
        @endfor
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#carousel" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#carousel" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>

    {{--@endif--}}
</div>
{!! $blockViewModel->getConfig('after_html') !!}