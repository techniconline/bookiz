@php  $insTheme = app('getInstanceObject')->getInstanceTheme(); @endphp
<li class="{{ $blockViewModel->getConfig($pre.'class_li') }}">
    <a class="{{ $blockViewModel->getConfig($pre.'class_link') }}"
       href="{{ BridgeHelper::getMenuHelper()->getMenuUrl($menu) }}">
        {{ $menu->title }}
    </a>
    @if($menu->childes->count())
        <div class="submenu">
            <ul>
                @foreach($menu->childes as $menu)
                    @include('themes.'.($insTheme->code).'.renderer.core.widgets.menu.submenu',['pre'=>$pre])
                @endforeach
            </ul>
        </div>
    @endif
</li>

{{--<div class="submenu">--}}
    {{--<ul>--}}
        {{--<li><a href="#">Haircuts and Hairdressing</a></li>--}}
        {{--<li><a href="#">Blow Dry</a></li>--}}
        {{--<li><a href="#">Ladies' Hair Colouring & Highlights</a></li>--}}
        {{--<li><a href="#">Ladies' Brazilian Blow Dry</a></li>--}}
        {{--<li><a href="#">Balayage & Ombre</a></li>--}}
        {{--<li><a href="#">Men's Haircut</a></li>--}}
        {{--<li><a href="#">See all hair treatments</a></li>--}}
    {{--</ul>--}}
    {{--<a href="#" class="img-card">--}}
        {{--<img src="https://cdn1.treatwell.net/images/view/v2.i1549312.w680.h340.x2EC4B123.jpg" alt=""/>--}}
        {{--<h3>Last minute? Cuts near you</h3>--}}
    {{--</a>--}}
    {{--<a href="#" class="img-card">--}}
        {{--<img src="https://cdn1.treatwell.net/images/view/v2.i1549312.w680.h340.x2EC4B123.jpg" alt=""/>--}}
        {{--<h3>Hair treatments: the lowdown</h3>--}}
    {{--</a>--}}
{{--</div>--}}