{!! $blockViewModel->getConfig('before_html') !!}
@php  $insTheme = app('getInstanceObject')->getInstanceTheme(); @endphp
@if($blockViewModel->getConfig('mobile_menu'))
    <button id="{{$blockViewModel->getConfig('id','head-sidebar-btn')}}"><i class="fas fa-bars"></i></button>
@endif
<div class="{{ $blockViewModel->getConfig('class_div','head-menu') }}" id="{{ $blockViewModel->getConfig('id') }}">

    @if($blockViewModel->getConfig('mobile_menu'))
        <button id="sidebar-close-button"><i class="fas fa-arrow-left"></i></button>
    @endif


    <ul class="{{ $blockViewModel->getConfig('class_ul','header-links') }}">
        @foreach($blockViewModel->getMenus() as $menu)
            @include('themes.'.($insTheme->code).'.renderer.core.widgets.menu.submenu',['pre'=>'mobile_'])
        @endforeach
    </ul>

</div>
{!! $blockViewModel->getConfig('after_html') !!}
