{!! $blockViewModel->getConfig('before_html') !!}

<div class="section-gray _partner">
    <div class="content">
        <h3 class="content-title">{!! $blockViewModel->getConfig('title') !!}</h3>
        <ul class="cities">
            @for($i=1;$i<=12;$i++)
                @if($blockViewModel->getConfig('name_'.$i))
                    <li style="width: 15%; text-align: center">
                        <a href="{!! $blockViewModel->getConfig('link_url_'.$i)?:'#' !!}">{!! $blockViewModel->getConfig('name_'.$i) !!}</a>
                        @if($blockViewModel->getConfig('image_url_'.$i))
                            <img src="{{ $blockViewModel->getConfig('image_url_'.$i) }}" class="bnr1"/>
                        @endif
                    </li>
                @endif
            @endfor
        </ul>
    </div>
</div>


{!! $blockViewModel->getConfig('after_html') !!}