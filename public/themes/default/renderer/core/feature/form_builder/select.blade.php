@if(isset($featureGroup) && $featureGroup)
    @if($builder->getShowFeatureGroup())
        {!! FormHelper::label('label_name_group', $featureGroup->title , ['class'=>'feature_group group_'.$featureGroup->id]) !!}
    @endif

            @endif

            @if($feature->featureValues)

                {!! FormHelper::select('feature['.$feature->id.']',$feature->featureValues->pluck('title','id')->toArray(),$activeValue
                            ,['title'=>$builder->getRowNumber() .'. '. $feature->title
                            ,/*'helper'=> $feature->title,*/ "id"=>"select_".$feature->id
                            ,'parent_class'=>'col-lg-6 col-md-6 col-sm-6'
                            ,'style'=>'width: 30em'
                            ,'label_style'=>'text-align: right;'
                            /*,'placeholder'=> $feature->title*/]) !!}

            @endif

            @if(isset($featureGroup) && $featureGroup)

@endif
