@if(isset($featureGroup) && $featureGroup)

    @if($builder->getShowFeatureGroup())
        {!! FormHelper::label('label_name_group', $featureGroup->title , ['class'=>'feature_group group_'.$featureGroup->id]) !!}
    @endif

    {{--<div class="form-group" id="group_id_{!! $featureGroup->id !!}">--}}
@endif
<table>
    <tr>
        <td>
            {!! FormHelper::label('label_name_'.$feature->id , $builder->getRowNumber() .'. '.$feature->title
            , ['class'=>'group_'.isset($featureGroup->id)?$featureGroup->id:str_random(4), 'data-group-id'=>isset($featureGroup->id)?$featureGroup->id:0 ]) !!}
        </td>
    </tr>
    <tr>
        <td>
            <ul style="direction: ltr; text-align: right">
                <div class="mt-checkbox-list">
                @foreach($feature->featureValues as $featureValue)
                    @php
                        $checked = boolval($activeValue && in_array($featureValue->id,$activeValue));
                    @endphp

                    <li style="list-style-type: none" class="col-md-6" data-active="{!! json_encode($activeValue) !!}">
                        <span class="mt-checkbox mt-checkbox-outline">{!! $featureValue->title !!}</span>
                        <input value="{!! $featureValue->id !!}" {!! $checked?'checked="checked"':'' !!} type="checkbox" name="feature[{!! $featureValue->feature_id !!}][]">
                        {{--{!! FormHelper::checkbox('feature['.$featureValue->feature_id.'][]'--}}
                        {{--,$featureValue->id, $checked--}}
                        {{--, ["id"=>"checkbox_".$featureValue->id , "title"=>$featureValue->title,'parent_class'=>'col-lg-12'] ) !!}--}}
                    </li>

                @endforeach
                </div>
            </ul>
        </td>
    </tr>

</table>

@if(isset($featureGroup) && $featureGroup)
    {{--</div>--}}
@endif

{{--<div class="form-group">--}}
    {{--<label>Outline Checkboxes</label>--}}
    {{--<div class="mt-checkbox-list">--}}
        {{--<label class="mt-checkbox mt-checkbox-outline"> Checkbox 1--}}
            {{--<input type="checkbox" value="1" name="test">--}}
            {{--<span></span>--}}
        {{--</label>--}}
        {{--<label class="mt-checkbox mt-checkbox-outline"> Checkbox 2--}}
            {{--<input type="checkbox" value="1" name="test">--}}
            {{--<span></span>--}}
        {{--</label>--}}
        {{--<label class="mt-checkbox mt-checkbox-outline"> Checkbox 3--}}
            {{--<input type="checkbox" value="1" name="test">--}}
            {{--<span></span>--}}
        {{--</label>--}}
    {{--</div>--}}
{{--</div>--}}