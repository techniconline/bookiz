@if(isset($featureGroup) && $featureGroup)
    {{--<div class="col-lg-6 col-md-6 col-sm-6 ">--}}
        {{--<div class="form-group" id="group_id_{!! $featureGroup->id !!}">--}}
            @endif
            <?php
            $group_id = isset($featureGroup->id) ? $featureGroup->id : str_random(4);
            ?>
            @if($builder->getShowFeatureGroup())
                {!! FormHelper::label('label_name_group', $featureGroup->title , ['class'=>'feature_group group_'.$group_id]) !!}
            @endif

            {!! FormHelper::input('text','feature['.$feature->id.']',old('feature['.$feature->id.']'
            ,isset($activeValue)?$activeValue:null)
            ,['title'=>$builder->getRowNumber() .'. '. $feature->title,/*'helper'=>$feature->title,*/ 'class'=>'group_'.$group_id
            ,'parent_class'=>'col-lg-6'
            ,'style'=>'width: 30em'
            ,'label_style'=>'text-align: right;'
            ]) !!}

            @if(isset($featureGroup) && $featureGroup)
        {{--</div>--}}
    {{--</div>--}}
@endif