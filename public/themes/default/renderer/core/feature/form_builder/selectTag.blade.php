@if(isset($featureGroup) && $featureGroup)
    @if($builder->getShowFeatureGroup())
        {!! FormHelper::label('label_name_group', $featureGroup->title , ['class'=>'feature_group group_'.$featureGroup->id]) !!}
    @endif

@endif

@if($feature->featureValues)

    {!! FormHelper::selectTag('feature['.$feature->id.']',$feature->featureValues->pluck('title','id')->toArray()
        ,old('feature['.$feature->id.']',$activeValues),['multiple'=>'multiple'
         , 'title'=>$feature->title,'helper'=>$feature->title]) !!}

@endif

@if(isset($featureGroup) && $featureGroup)

@endif
