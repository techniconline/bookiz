{!! $blockViewModel->getConfig('before_html') !!}
@php $entities =$blockViewModel->getEntitiesList() @endphp
@if($entities->isNotEmpty())
    <div class="section-gray" data-background="{{ $blockViewModel->getConfig('back_image') }}">
        <div class="content">
            <h3 class="content-title">{{ $blockViewModel->getConfig('title') }}</h3>
            <div class="mini-slider">
                <div class="owl-carousel owl-5">
                    @foreach($entities as $entity)
                        <a href="{{ route('entity.view',['id'=>$entity['id'],'slug'=>$entity['slug']]) }}" title="{{ $entity['short_description'] }}">
                        <div class="item card-white" data-title="{{ $entity['short_description'] }}">
                            <img class="owl-lazy" data-src="{!! $entity['media_image']?$entity['media_image']['url_thumbnail']:null !!}" />
                            <h3>{{ $entity['title'] }}</h3>
                            {{--<p class="distance">0.3 mi away</p>--}}
                            <div class="rate">

                                <div class="stars">

                                    @for($i = 1; 5>=$i; $i++)
                                    <i class="{!! (($entity->rate['data_rate'])>=$i) || (($entity->rate['data_rate'])>4.8)?'fas':'far'  !!} fa-star"></i>
                                    @endfor

                                </div>

                                <div class="num">{!! $entity->rate['data_rate'] !!}</div>
                            </div>
                        </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endif
{!! $blockViewModel->getConfig('after_html') !!}