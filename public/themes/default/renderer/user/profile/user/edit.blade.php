<div class="container profile-page">
    <div class="profile">
        <div class="detail">
            <img src="{!! $BlockViewModel->getUserData('small_avatar_url','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQiowrV0bVwR6zOZJ7ZDjNS29YakE2QpD0L_dZTgeVh6j4b4N3u') !!}"
                 alt=""/>
            <div class="info">
                <h1>@lang('user::routes.user.manage_edit_user',['full_name'=>$BlockViewModel->getUserData('full_name')])</h1>
                <span class="email">@lang('user::form.fields.gender')
                    : {!! $BlockViewModel->getUserData('gender_text') !!}</span>

                <span class="email">
                <strong>@lang('user::form.fields.age'): </strong>
                <span>{!! $BlockViewModel->getUserData('age') !!}</span>
                </span>

                <span class="email">
                <strong>@lang('user::form.fields.national_code'): </strong>
                <span>{!! $BlockViewModel->getUserData('national_code', '-') !!}</span>
                </span>

                <div class="btn btn-outline-success _edit_profile">@lang('user::routes.user.manage_edit')</div>
            </div>


        </div>
        {{--<div class="others">--}}
        {{--<div class="list">--}}
        {{--<div class="item">--}}
        {{--<strong>@lang('user::form.fields.age'): </strong>--}}
        {{--<span>{!! $BlockViewModel->getUserData('age') !!}</span>--}}
        {{--</div>--}}
        {{--<div class="item">--}}
        {{--<strong>@lang('user::form.fields.national_code'): </strong>--}}
        {{--<span>{!! $BlockViewModel->getUserData('national_code', '-') !!}</span>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>

    <div class="profile _user_profile" style="display: none">
        <div class="detail">

            <div class="col-xl-12 col-lg-12 col-md-12 ">

                {!! FormHelper::open(['role'=>'form','url'=>route('user.profile.edit.save'),'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="">

                    <div style="display: none">
                        {!! FormHelper::input('text','username',old('username',$BlockViewModel->getUserData('username')),['required'=>'required','title'=>trans('user::form.fields.username'),'helper'=>trans('user::form.helper.username')]) !!}
                    </div>

                    {!! FormHelper::input('text','first_name',old('first_name',$BlockViewModel->getUserData('first_name')),['required'=>'required','title'=>trans('user::form.fields.first_name'),'helper'=>trans('user::form.helper.first_name')]) !!}

                    {!! FormHelper::input('text','last_name',old('last_name',$BlockViewModel->getUserData('last_name')),['required'=>'required','title'=>trans('user::form.fields.last_name'),'helper'=>trans('user::form.helper.last_name')]) !!}

                    {!! FormHelper::input('text','father_name',old('father_name',$BlockViewModel->getUserData('father_name')),['title'=>trans('user::form.fields.father_name'),'helper'=>trans('user::form.helper.father_name')]) !!}

                    {!! FormHelper::input('text','national_code',old('national_code',$BlockViewModel->getUserData('national_code')),['title'=>trans('user::form.fields.national_code'),'helper'=>trans('user::form.helper.national_code')]) !!}

                    {!! FormHelper::date('birthday',old('birthday',$BlockViewModel->getUserData('birthday')),['title'=>trans('user::form.fields.birthday'),'helper'=>trans('user::form.helper.birthday'),'date-year-current'=>0,'date-year-before'=>100,'set-default'=>0]) !!}

                    {!! FormHelper::select('country_id',[],old('country_id',$BlockViewModel->getUserData('country_id')),['data-init'=>1,'data-fill-url'=>route('core.location.json'),'data-change-trigger'=>'state_id','title'=>trans('user::form.fields.country'),'helper'=>trans('user::form.helper.country')]) !!}

                    {!! FormHelper::select('state_id',[],old('state_id',$BlockViewModel->getUserData('state_id')),['data-fill-url'=>route('core.location.json'),'data-change-trigger'=>'city_id','title'=>trans('user::form.fields.state_id'),'helper'=>trans('user::form.helper.state_id')]) !!}

                    {!! FormHelper::select('city_id',[],old('city_id',$BlockViewModel->getUserData('city_id')),['data-fill-url'=>route('core.location.json'),'title'=>trans('user::form.fields.city_id'),'helper'=>trans('user::form.helper.city_id')]) !!}

                    {!! FormHelper::select('gender',trans('user::data.gender'),old('gender',$BlockViewModel->getUserData('gender')),['title'=>trans('user::form.fields.gender'),'helper'=>trans('user::form.helper.gender'),'placeholder'=>trans('user::form.placeholder.please_select')]) !!}

                    {!! FormHelper::select('marital',trans('user::data.marital'),old('marital',$BlockViewModel->getUserData('marital')),['title'=>trans('user::form.fields.marital'),'helper'=>trans('user::form.helper.marital'),'placeholder'=>trans('user::form.placeholder.please_select')]) !!}

                    {!! FormHelper::avatar('avatar',$BlockViewModel->getUserData('small_avatar_url'),['title'=>trans('user::form.fields.avatar'),'helper'=>trans('user::form.helper.avatar')]) !!}

                    {!! FormHelper::openAction() !!}
                    {!! FormHelper::submitOnly() !!}
                    {!! FormHelper::closeAction() !!}
                    {!! FormHelper::close() !!}
                </div>
            </div>

        </div>
    </div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs beauty-tabs mt-2">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#favorties">@lang('user::routes.user.favorites')</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#manage_user_connectors">@lang('user::routes.user.manage_user_connectors')</a>
        </li>
        {{--<li class="nav-item">--}}
        {{--<a class="nav-link" data-toggle="tab" href="#menu2">Menu 2</a>--}}
        {{--</li>--}}
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active container" id="favorties">
            ....
        </div>
        <div class="tab-pane container" id="manage_user_connectors">

            {!! $BlockViewModel->connectorViewModel->getUserConnectors() !!}

        </div>
        <div class="tab-pane container" id="menu2">...</div>
    </div>
</div>


<script>
    jQuery(document).ready(function () {

        $('body').on('click', '._edit_profile', function (event) {

            event.preventDefault();
            var $clicked = $(this);
            var $parent = $clicked.parents('div.container').first();
            var $form = $parent.find('._user_profile');
            if ($clicked.hasClass('active')){
                $clicked.removeClass('active')
                $form.fadeOut();
            } else{
                $clicked.addClass('active')
                $form.fadeIn();
            }

        });
    });
</script>