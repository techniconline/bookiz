﻿<div class="auth-page">
    <form class="auth-container" id="signin" action="{{ route('user.verify') }}" method="post">
        {{ csrf_field() }}
        <h1>@lang('user::form.helper.confirm')</h1>
            <div class="body">
                <div class="form-group">
                    <input type="text" value="{{ $connector->value }}" class="form-control" readonly>
                </div>

                <div class="form-group {{ $errors->has('connector') ? ' has-error' : '' }}">
                    <label for="token">@lang('user::form.fields.token') :</label>
                    <input type="text" name="token" value="{{ old('token') }}" class="form-control"
                           placeholder="@lang('user::form.fields.token')" required/>
                    @if ($errors->has('token'))
                        <span class="help-block {{ $errors->has('connector') ? ' text-danger' : '' }} ">
              <strong>{{ $errors->first('token') }}</strong>
            </span>
                    @endif

                </div>

                <button type="submit" class="btn btn-dark btn-block">@lang('user::form.fields.SubmitVerfyCode')</button>
            </div>


            <div class="auth-links">
                <a id="_timer" class="form-control btn btn-default _timer_resend"
                        onclick="return clickit();"></a>
                <a class="form-control btn btn-info "
                   href="{{ route('user.verify.change') }}">@lang('user::form.fields.change',['text'=>$viewModel->getConnectorLabel()]) </a>
            </div>
            <div class="auth-links">
                <a href="{{ route('forgot') }}"> @lang('user::form.titles.forgotToken')</a>

                @if(!BridgeHelper::getConfig()->getSettings('register_hide','instance','core'))
                    <a href="{{ route('register') }}">@lang('user::form.titles.register') </a>
                @endif
            </div>
    </form>
</div>


<script>
    var timeLeft = 30;
    var elem = document.getElementById('_timer');
    var timerId = setInterval(countdown, 1000);

    function clickit() {
        window.location = "{{ route('user.verify.resend') }}";
    }

    function countdown() {
        if (timeLeft == -1) {
            clearTimeout(timerId);
            EnableButtn();
        } else {
            elem.innerHTML = timeLeft + ' @lang('user::form.titles.time_wait_for_resend')';
            elem.disabled = true;
            timeLeft--;
        }
    }

    function EnableButtn() {
        elem.disabled = false;
        elem.innerHTML = '@lang('user::form.titles.resend_verify_token')';
        elem.classList.add("btn-warning");
        elem.classList.remove("_timer_resend");
    }


</script>
