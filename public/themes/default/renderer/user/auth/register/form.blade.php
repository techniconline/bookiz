<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
    <h5 class="modal-title">@lang('user::form.helper.register')</h5>
</div>
<div class="modal-body">
    <form class="auth-container" id="signup" action="{{ route('register') }}" method="post">
        {{ csrf_field() }}
        {!! FormHelper::input('hidden','referer',old('referer',$viewModel->getReferer())) !!}

        <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
            <label for="first_name">@lang('user::form.fields.first_name'):</label>
            <input type="text" value="{{ old('first_name') }}" id="first_name" name="first_name"
                   class="form-control" placeholder="@lang('user::form.fields.first_name')"/>
            @if ($errors->has('first_name'))
                <span class="help-block {{ $errors->has('first_name') ? ' text-danger' : '' }} ">
                        <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
            <label for="last_name">@lang('user::form.fields.last_name'):</label>
            <input type="text" value="{{ old('last_name') }}" id="last_name" name="last_name" class="form-control"
                   placeholder="@lang('user::form.fields.last_name')"/>
            @if ($errors->has('last_name'))
                <span class="help-block {{ $errors->has('last_name') ? ' text-danger' : '' }} ">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('connector') ? ' has-error' : '' }}">
            <label for="connector">{{ $viewModel->getConnectorLabel() }}:</label>
            <input type="text" value="{{ old('connector') }}" id="connector" name="connector" class="form-control"
                   placeholder="{{ $viewModel->getConnectorLabel() }}"/>
            @if ($errors->has('connector'))
                <span class="help-block {{ $errors->has('connector') ? ' text-danger' : '' }} ">
                        <strong>{{ $errors->first('connector') }}</strong>
                    </span>
            @endif
        </div>

        <div class="form-group">
            <label for="password">@lang('user::form.fields.password'):</label>
            <input type="password" class="form-control" name="password" id="password"
                   placeholder="@lang('user::form.fields.password')"/>
        </div>
        <div class="form-group">
            <label for="password_confirmation">@lang('user::form.fields.password_confirmation'):</label>
            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation"
                   placeholder="@lang('user::form.fields.password_confirmation')"/>
        </div>
        <button type="submit" class="btn btn-dark btn-block">@lang('user::form.fields.signup')</button>
    </form>

    <div class="auth-links">
        <a href="{{ route('forgot') }}"> @lang('user::form.titles.forgotToken')</a>

        @if(!BridgeHelper::getConfig()->getSettings('register_hide','instance','core'))
            <a href="{{ route('login') }}">@lang('user::form.titles.login')  </a>
        @endif
    </div>
</div>
