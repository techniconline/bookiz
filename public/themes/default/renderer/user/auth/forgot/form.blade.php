
<div class="auth-page">
    <form class="auth-container" id="forget" action="{{ route('forgot') }}" method="post">
        {{ csrf_field() }}

        <h1>@lang('user::form.titles.forgotToken')</h1>
        <div class="body">
            <div class="form-group {{ $errors->has('connector') ? ' has-error' : '' }}">
                <label for="email">{{ $viewModel->getConnectorLabel() }} :</label>
                <input type="text" value="{{ old('connector') }}" id="connector" name="connector" class="form-control" placeholder="{{ $viewModel->getConnectorLabel() }}" required/>
                @if ($errors->has('connector'))
                    <span class="help-block {{ $errors->has('connector') ? ' text-danger' : '' }} ">
                        <strong>{{ $errors->first('connector') }}</strong>
                    </span>
                @endif
            </div>

            <button type="submit" class="btn btn-dark btn-block">@lang('user::form.fields.forgot')</button>
        </div>


        <div class="auth-links">
            <a href="{{ route('forgot') }}"> @lang('user::form.titles.forgotToken')</a>

            @if(!BridgeHelper::getConfig()->getSettings('register_hide','instance','core'))
                <a href="{{ route('register') }}">@lang('user::form.titles.register')  </a>
            @endif
        </div>

    </form>
</div>
