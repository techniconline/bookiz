﻿<!--<div class="bgcolor4 _shop_basket1 brd-rad-05 userColor  fltl ">
<a data-toggle="modal" data-target="#userModal" href="#">
    <i class="fa fa-user"></i>
</a>
</div>-->
{!! $blockViewModel->getConfig('before_html') !!}

<style>
    .modal-content > .auth-page {
        margin-top: 0px !important;
    }
</style>

<div class="auth-modal modal fade" id="loginModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            {!!  $blockViewModel->getLoginForm() !!}
        </div>
    </div>
</div>

<div class="auth">
    <button type="button" class="btn btn-dark btn-sm" data-toggle="modal" data-target="#loginModal">@lang('user::form.fields.signin')</button>
    <button type="button" class="btn btn-dark btn-sm" data-toggle="modal" data-target="#registerModal">@lang('user::form.fields.signup')</button>
</div>



<div class="auth-modal modal fade" id="registerModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            {!!  $blockViewModel->getRegisterForm() !!}

        </div>
    </div>
</div>

{!! $blockViewModel->getConfig('after_html') !!}