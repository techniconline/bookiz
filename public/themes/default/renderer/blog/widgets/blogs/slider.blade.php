{!! $blockViewModel->getConfig('before_html') !!}
@php $blogs = $blockViewModel->getBlogsList() @endphp
@if($blogs && $blogs->isNotEmpty())

    @if($image_logo=$blockViewModel->getConfig('logo_image'))
        <div class="section-heading">
            <a href="#">
                    <img src="{{ url($image_logo) }}" alt="{{ $blockViewModel->getConfig('title') }}"/>
                {{ $blockViewModel->getConfig('title') }}
            </a>
        </div>
    @endif

    <div class="section-white">
        <div class="content">
            @foreach($blogs as $index => $blog)
                @if(!fmod($index, 2))
                    <div class="big-cards">
                @endif
                            <div class="big-card">
                                <img src="{{ $blog->image?:'https://cdn1.treatwell.net/images/view/v2.i1513595.w540.h192.xDF38EF5F.png' }}"
                                     alt="{{ $blog->title }}"/>
                                <div class="detail">
                                    <h3>{{ $blog->title }}</h3>
                                    <p class="desc">{{ $blog->short_description }}</p>
                                    <a href="{{ route('blog.view',['id'=>$blog->id]) }}" class="btn btn-dark">@lang('blog::blog.blog_view')</a>
                                </div>
                            </div>
                @if(fmod($index, 2))
                    </div>
                @endif

            @endforeach

        </div>
    </div>

@endif

{!! $blockViewModel->getConfig('after_html') !!}