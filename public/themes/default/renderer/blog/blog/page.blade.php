@php $blog=$blockViewModel->getBlog() @endphp
<div class="post-page">

    <div class="head" style='background-image: url("{{ $blog->image }}")'>
        <div class="content">
            <h1>{{ $blog->title }}</h1>
            <p>{{ str_limit(strip_tags($blog->short_description)) }}</p>
        </div>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h2>{{ $blog->title }}</h2>
                <p>
                    {{ $blog->short_description }}
                </p>
            </div>
        </div>
        <p>
            {!! $blog->description !!}
        </p>
    </div>

</div>
