@if(isset($viewModel->listBlogs) && $viewModel->listBlogs)

    <section id="page-head">

        <div class="head-image parallax  BG2"></div>

        <div class="page-line parallax">
            <div class="container">

                <div class="page-title">
                    @lang('blog::blog.list')
                </div>
                {{--<div class="row">--}}
                    {{--<div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">--}}
                        {{--<div class="page-info">--}}
                            {{--<p></p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>



    </section>

    <section id="blog-holder" class="blog-holder-detail">

        <div class="container">

            <div class="row">

                <div class="col-lg-9 col-md-9 col-sm-9">
                    @php $listBlogs = $viewModel->listBlogs @endphp
                    @foreach($listBlogs as $item)

                        <article class="post-single">

                            <a href="{{ route('blog.view',['id'=>$item->id]) }}">
                                <div class="post-visual">
                                    <img src="{{ strlen($item->image)?$item->image:BridgeHelper::getConfig()->getSettings('default_poster','instance','blog') }}" />
                                </div>

                                <div class="head">
                                    <h1>{{ $item->title }}</h1>
                                </div>

                                <div class="blog-info">
                                    <ul>
                                        {{--<li>توسط<a href="#">ادمین</a></li>--}}
                                        {{--<li><a href="#">22 نظر</a></li>--}}
                                        <li><a href="#">{{ $item->created_small_date_day }}, {{ $item->created_small_date_month }}</a></li>
                                    </ul>
                                </div>

                                <div class="post-detail">
                                    {!! $item->short_description !!}
                                </div>
                            </a>

                        </article>

                    @endforeach
                    {!! $listBlogs->links() !!}
                </div>


                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="blog-sidebar">

                    {!! $viewModel->renderCategoriesView($current_category) !!}

                    <!--\\=========================\\-->
                        {{--<div class="blog-tags">--}}

                            {{--<div class="head-wrapper">--}}
                                {{--<div class="head">برچسب ها</div>--}}
                            {{--</div>--}}

                            {{--<ul>--}}

                                {{--<li><a href="#">آثار هنری</a></li>--}}
                                {{--<li class="active"><a href="#">عکس</a></li>--}}
                                {{--<li><a href="#">ویدیو</a></li>--}}
                                {{--<li><a href="#">طراحی</a></li>--}}
                                {{--<li><a href="#">فروش</a></li>--}}
                                {{--<li><a href="#">هنر</a></li>--}}
                                {{--<li><a href="#">پیکسل</a></li>--}}
                                {{--<li><a href="#">تمیز کردن</a></li>--}}
                                {{--<li><a href="#">منحصر به فرد</a></li>--}}

                            {{--</ul>--}}

                        {{--</div>--}}


                    </div>

                </div>


            </div><!--\\row-->

        </div>

    </section>

@endif

