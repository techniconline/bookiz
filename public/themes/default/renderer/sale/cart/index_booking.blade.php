@php $currentCart=$blockViewModel->getCurrentCart();  @endphp
<script>
    var $employees = [];
    var $calendar = [];
</script>
<div class="checkout-page">
    <div class="content">
        <div class="options">
            <div class="form-group _selector_employee">
                {{--<label for="employees">@lang('sale::cart.select_employee')</label>--}}
                <select class="form-control" id="employees" data-id="0">
                    <option class="_except" value="">@lang('sale::cart.select_employee')</option>
                    <option class="_except" value="-1">@lang('sale::cart.any_employee')</option>
                </select>
            </div>
            <div class="form-group">
                <label for="employees">@lang('sale::cart.select_time')</label>
                <div id="date-calendar" data-dates='["01/02/2019","02/02/2019","03/02/2019","05/02/2019"]'
                     class="material-theme"></div>

                <ul class="select-times" id="time_list">

                </ul>

                {{--<ul class="select-times">--}}
                {{--<li class="active">--}}
                {{--<span class="time">10:45 AM</span>--}}
                {{--<span class="prices">--}}
                {{--<span class="old-price">$175</span>--}}
                {{--<span class="new-price">$165</span>--}}
                {{--</span>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<span class="time">11:00 AM</span>--}}
                {{--<span class="prices">--}}
                {{--<span class="old-price">$175</span>--}}
                {{--<span class="new-price">$165</span>--}}
                {{--</span>--}}
                {{--</li>--}}
                {{--</ul>--}}

            </div>
        </div>


        <div class="basket">
            <h4>@lang('sale::cart.cart')</h4>
            @if($cart=$currentCart->getCart())
                <form action="{!! route('sale.cart.booking.save') !!}" method="POST">
                    {!! csrf_field() !!}
                    <ul>
                        @php $discountedOnProducts=0.00 @endphp
                        @foreach($cart->cartItems as $index => $cartItem)
                            @php
                                $item=$blockViewModel->getSaleItem($cartItem->item_type,$cartItem->item_id,$cartItem->options_array);
                                $itemDiscountPrice=BridgeHelper::getCurrencyHelper()->getConvertPrice($cartItem->discount_amount,$cartItem->currency_id,$cart->currency_id);
                                $discountedOnProducts=$discountedOnProducts+$itemDiscountPrice;
                            @endphp
                            @if($item)
                                @php $itemExtraData=$item->getItemExtraData(); @endphp

                                <script>
                                    $employees[{!! $cartItem->item_id !!}] = {!! $itemExtraData['employees_selectable']->toJson() !!};
                                    $calendar[{!! $cartItem->item_id !!}] = {!! $itemExtraData['timesheet']->toJson() !!};
                                </script>
                                <li>
                                    <div style="display: none">
                                        {!! FormHelper::select('entity_employee['.$cartItem->item_id.']',$itemExtraData['employees_selectable'],old('entity_employee['.$cartItem->item_id.']',null)
                                            ,['title'=>trans("sale::cart.select_employee"),'helper'=>trans("sale::cart.select_employee")
                                            ,'placeholder'=>trans("sale::cart.select_employee"), 'class'=>'_service_employees', 'parent_class'=>'_employee_box']) !!}
                                    </div>


                                    <a class="_service_row" href="#" data-id="{!! $cartItem->item_id !!}">
                                    <span class="detail">
                                        <span class="title">{{ $cartItem->name }}</span>
                                        @if($cartItem->discount_amount)
                                            <span class="discount"
                                                  style="color: green; font-size: 12px">@lang('sale::cart.discount') {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cartItem->discount_amount,$cartItem->currency_id,true) }}</span>
                                        @endif
                                        @if($itemExtraData['calendar'])
                                            <span class="duration">{!! $itemExtraData['calendar']->period_time  !!}</span>
                                        @endif
                                    </span>
                                    </a>

                                    <input type="hidden" class="_service"
                                           name="booking_details[{!! $index !!}][service]"
                                           value="{!! $cartItem->item_id !!}">

                                    <input type="hidden" class="_booking_model_id"
                                           name="booking_details[{!! $index !!}][booking_model_id]"
                                           value="{!! $itemExtraData['booking_model_id'] !!}">


                                    <input type="hidden" class="_booking_model_type"
                                           name="booking_details[{!! $index !!}][booking_model_type]"
                                           value="{!! $itemExtraData['booking_model_type'] !!}">


                                    <input type="hidden" class="_model_id"
                                           name="booking_details[{!! $index !!}][model_id]"
                                           value="{!! $itemExtraData['booking_details_model_id'] !!}">


                                    <input type="hidden" class="_model_type"
                                           name="booking_details[{!! $index !!}][model_type]"
                                           value="{!! $itemExtraData['booking_details_model_type'] !!}">


                                    <input type="hidden" class="_entity_service"
                                           name="booking_details[{!! $index !!}][entity_service]"
                                           value="{!! $itemExtraData['entity_relation_service']?$itemExtraData['entity_relation_service']->entity_service_id:0 !!}">

                                    <input type="hidden" class="_entity"
                                           name="booking_details[{!! $index !!}][entity]"
                                           value="{!! $itemExtraData['entity_relation_service']?$itemExtraData['entity_relation_service']->entity_id:0 !!}">

                                    <input type="hidden" class="_booking_calendar"
                                           name="booking_details[{!! $index !!}][booking_calendar]"
                                           value="{!! $itemExtraData['calendar']?$itemExtraData['calendar']->id:0 !!}">

                                    <input type="hidden" class="_employee"
                                           name="booking_details[{!! $index !!}][employee]" value="0">

                                    <input type="hidden" class="_date"
                                           name="booking_details[{!! $index !!}][date]" value="">

                                    <input type="hidden" class="_time"
                                           name="booking_details[{!! $index !!}][time]" value="">

                                    <span class="right">
                                        <span class="price">{{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cartItem->total_amount,$cartItem->currency_id,true) }}</span>

                                        {{--<a href="javascript:void(0)" class="delete">x</a>--}}
                                        <a class="text-danger delete"
                                           href="{{ route('sale.cart.delete',['id'=>$cartItem->id]) }}">
                                   <i class="fas fa-trash-alt"></i>
                                </a>
                            </span>
                                </li>
                            @else
                                @php
                                    $deletedId=[$cartItem->id];
                                    $cartItem->delete();
                                    $currentCart->updateCart(null,$deletedId);
                                @endphp
                            @endif

                        @endforeach
                        <li>
                            <a class="add-more" href="{!! $itemExtraData['entity_url'] !!}"><i
                                        class="fas fa-plus-circle"></i> @lang('sale::cart.add_another_service')</a>
                        </li>
                    </ul>

                    @if($cart->payable_amount)
                        {!! $blockViewModel->getMasterPayment()->getViewForm('payment') !!}
                    @else
                        {!! FormHelper::input('hidden','payment','0') !!}
                    @endif

                    <button class="btn btn-block" type="submit">@lang('sale::cart.save_order')</button>
                    {{--<a href="{!! route('sale.cart.booking.invoice') !!}" type="button" class="btn btn-block">@lang('sale::cart.save_order')</a>--}}
                </form>

            @else
                <ul>
                    <li>
                        <a class="add-more" href="{!! request()->header('referer') !!}"><i
                                    class="fas fa-plus-circle"></i> @lang('sale::cart.add_another_service')</a>
                    </li>
                </ul>
            @endif

        </div>

    </div>
</div>
<script>
    // console.log($employees);
</script>