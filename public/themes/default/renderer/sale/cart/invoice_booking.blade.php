<div class="checkout-step2">
    <div class="content">
        <div class="form-register">
            <h4>Your Details</h4>
            <div class="form-group">
                <label for="full_name">Full Name</label>
                <input type="text" class="form-control form-control-sm" id="full_name" placeholder="Your Full Name" />
            </div>
            <div class="contact">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control form-control-sm" id="email" placeholder="Your Email" />
                </div>
                <div class="form-group">
                    <label for="phone">Phone number</label>
                    <input type="text" class="form-control form-control-sm" id="phone" placeholder="Your Phone Number" />
                </div>
            </div>
            <div class="form-group" id="forYou">
                <label>Is this for you? </label>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" name="foryou" class="form-check-input" value="1"> Yes
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" name="foryou" class="form-check-input" checked value="0"> No
                    </label>
                </div>
            </div>
            <div class="form-group guest-name" style="display: none">
                <label for="guest">Who is the lucky recipient?</label>
                <input type="text" class="form-control form-control-sm" id="guest" placeholder="Guest Name" />
            </div>
            <div class="form-group">
                <label>Have you been to this salon in the past 12 months? </label>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" name="used_before" class="form-check-input" value="1"> Yes
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" name="used_before" class="form-check-input" checked value="0"> No
                    </label>
                </div>
            </div>
        </div>
        <div class="basket">
            <h4>My basket</h4>
            <ul>
                <li>
                    <span class="detail">
                        <span class="title">Standard Pedicure</span>
                        <span class="duration">1 hr 30 mins</span>
                    </span>
                    <span class="right">
                        <span class="price">28$</span>
                        <a href="javascript:void(0)" class="delete">x</a>
                    </span>
                </li>
                <li>
                    <span class="detail">
                        <span class="title">Standard Pedicure</span>
                        <span class="duration">1 hr 30 mins</span>
                    </span>
                    <span class="right">
                        <span class="price">28$</span>
                        <a href="javascript:void(0)" class="delete">x</a>
                    </span>
                </li>
                <li >
                    <a class="add-more" href="single.html"><i class="fas fa-plus-circle"></i> Add another service from this salon</a>
                </li>
            </ul>
            <button type="button" class="btn btn-block">GO TO CHECKOUT</button>
        </div>
    </div>
</div>
