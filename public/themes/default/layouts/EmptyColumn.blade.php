<!DOCTYPE html>
<html lang="{!! get_instance()->getLanguageCode() !!}" dir="{!! get_instance()->getLanguageDirection() !!}">

<head>
    @partial('head')
</head>

<body class="home-page">

    @content()

    @partial('foot')

</body>

</html>
