<!DOCTYPE html>
<html lang="{!! get_instance()->getLanguageCode() !!}" dir="{!! get_instance()->getLanguageDirection() !!}">

    <head>
        @partial('head')
    </head>

    <body class="">
        @partial('header')
        @partial('sections.top')
        <div class="_main">
            @partial('sections.main')
        </div>
        @partial('sections.bottom')

        @partial('foot')

    </body>

</html>
