<!DOCTYPE html>
<html lang="{!! get_instance()->getLanguageCode() !!}" dir="{!! get_instance()->getLanguageDirection() !!}">

    <head>
        @partial('head')
    </head>

    <body class="Body_scroll"> 

	<div class="container-fliud  fullH  ">
        @content()
	  </div>

    </body>

</html>
