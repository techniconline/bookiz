<!DOCTYPE html>
<html lang="{!! get_instance()->getLanguageCode() !!}" dir="{!! get_instance()->getLanguageDirection() !!}">

<head>
    @partial('head')
</head>

<body class="home-page">

    @partial('header')

    @partial('sections.top')

    @partial('sections.right')
    @partial('sections.main')
    @partial('sections.left')
    @partial('sections.bottom')

    @partial('footer')

    @partial('foot')

</body>

</html>
