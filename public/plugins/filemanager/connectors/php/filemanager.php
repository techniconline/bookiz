<?php
/**
 * Entry point for PHP connector, put your customizations here.
 *
 * @license     MIT License
 * @author      Pavel Solomienko <https://github.com/servocoder/>
 * @copyright   Authors
 */

// only for debug
// error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
// ini_set('display_errors', '1');

require_once 'vendor/autoload.php';
require_once __DIR__ . '/events.php';
$config = require __DIR__ .'/configs/config.php';


// fix display non-latin chars correctly
// https://github.com/servocoder/RichFilemanager/issues/7
setlocale(LC_CTYPE, 'en_US.UTF-8');

// fix for undefined timezone in php.ini
// https://github.com/servocoder/RichFilemanager/issues/43
if(!ini_get('date.timezone')) {
    date_default_timezone_set('GMT');
}
// example to override the default config
//$config = [
//    'security' => [
//        'readOnly' => true,
//        'extensions' => [
//            'policy' => 'ALLOW_LIST',
//            'restrictions' => [
//                'jpg',
//                'jpe',
//                'jpeg',
//                'gif',
//                'png',
//            ],
//        ],
//    ],
//];

$app = new \RFM\Application();

// uncomment to use events
//$app->registerEventsListeners();

//$local = new \RFM\Repository\Local\Storage($config);
//
//// example to setup files root folder
//$local->setRoot('userfiles', true, true);
//
//$app->setStorage($local);
//
//// set application API
//$app->api = new RFM\Api\LocalApi();



$s3 = new \RFM\Repository\S3\Storage($config);
$s3->setRoot('/', true);
//$s3 = new \RFM\Repository\S3\Storage($config);
$app->setStorage($s3);

$app->api = new \RFM\Api\AwsS3Api();


$app->run();