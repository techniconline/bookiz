<?php
/**
 * Events handlers of API operations.
 *
 * @license     MIT License
 * @author      Pavel Solomienko <https://github.com/servocoder/>
 * @copyright   Authors
 */



// This function is called for every server connection. It must return true.
//
// Implement this function to authenticate the user, for example to check a
// password login, or restrict client IP address.
//
// This function only authorizes the user to connect and/or load the initial page.
// Authorization for individual files or dirs is provided by the two functions below.
//
// NOTE: If using session variables, the session must be started first (session_start()).
function fm_authenticate()
{
    // Customize this code as desired.
    return true;

    // If this function returns false, the user will just see an error.
    // If this function returns an array with "redirect" key, the user will be redirected to the specified URL:
    // return ['redirect' => 'http://domain.my/login'];
}


// This function is called before any filesystem read operation, where
// $filepath is the file or directory being read. It must return true,
// otherwise the read operation will be denied.
//
// Implement this function to do custom individual-file permission checks, such as
// user/group authorization from a database, or session variables, or any other custom logic.
//
// Note that this is not the only permissions check that must pass. The read operation
// must also pass:
//   * Filesystem permissions (if any), e.g. POSIX `rwx` permissions on Linux
//   * The $filepath must be allowed according to config['patterns'] and config['extensions']
//
function fm_has_read_permission($filepath)
{
    // Customize this code as desired.
    return true;
}


// This function is called before any filesystem write operation, where
// $filepath is the file or directory being written to. It must return true,
// otherwise the write operation will be denied.
//
// Implement this function to do custom individual-file permission checks, such as
// user/group authorization from a database, or session variables, or any other custom logic.
//
// Note that this is not the only permissions check that must pass. The write operation
// must also pass:
//   * Filesystem permissions (if any), e.g. POSIX `rwx` permissions on Linux
//   * The $filepath must be allowed according to config['patterns'] and config['extensions']
//   * config['read_only'] must be set to false, otherwise all writes are disabled
//
function fm_has_write_permission($filepath)
{
    // Customize this code as desired.
    return true;
}


/**
 * Event listener on after "readfolder" API method successfully executed.
 *
 * @param \RFM\Event\Api\AfterFolderReadEvent $event
 */
function fm_event_api_after_folder_read($event)
{
    $data = $event->getFolderData();
    $list = $event->getFolderContent();
}

/**
 * Event listener on after "seekfolder" API method successfully executed.
 *
 * @param \RFM\Event\Api\AfterFolderSeekEvent $event
 */
function fm_event_api_after_folder_seek($event)
{
    $data = $event->getFolderData();
    $list = $event->getSearchResult();
    $string = $event->getSearchString();
}

/**
 * Event listener on after "addfolder" API method successfully executed.
 *
 * @param \RFM\Event\Api\AfterFolderCreateEvent $event
 */
function fm_event_api_after_folder_create($event)
{
    $data = $event->getFolderData();
}

/**
 * Event listener on after "upload" API method successfully executed.
 *
 * @param \RFM\Event\Api\AfterFileUploadEvent $event
 */
function fm_event_api_after_file_upload($event)
{
    $data = $event->getUploadedFileData();
}

/**
 * Event listener on after "extract" API method successfully executed.
 *
 * @param \RFM\Event\Api\AfterFileExtractEvent $event
 */
function fm_event_api_after_file_extract($event)
{
    $data = $event->getArchiveData();
    $list = $event->getArchiveContent();
}

/**
 * Event listener on after "rename" API method successfully executed.
 *
 * @param \RFM\Event\Api\AfterItemRenameEvent $event
 */
function fm_event_api_after_item_rename($event)
{
    $data = $event->getItemData();
    $originalData = $event->getOriginalItemData();
}

/**
 * Event listener on after "copy" API method successfully executed.
 *
 * @param \RFM\Event\Api\AfterItemCopyEvent $event
 */
function fm_event_api_after_item_copy($event)
{
    $data = $event->getItemData();
    $originalData = $event->getOriginalItemData();
}

/**
 * Event listener on after "move" API method successfully executed.
 *
 * @param \RFM\Event\Api\AfterItemMoveEvent $event
 */
function fm_event_api_after_item_move($event)
{
    $data = $event->getItemData();
    $originalData = $event->getOriginalItemData();
}

/**
 * Event listener on after "delete" API method successfully executed.
 *
 * @param \RFM\Event\Api\AfterItemDeleteEvent $event
 */
function fm_event_api_after_item_delete($event)
{
    $data = $event->getOriginalItemData();
}

/**
 * Event listener on after "download" API method successfully executed.
 *
 * @param \RFM\Event\Api\AfterItemDownloadEvent $event
 */
function fm_event_api_after_item_download($event)
{
    $data = $event->getDownloadedItemData();
}