<?php
// boot laravel
$path=__DIR__ .'/../../../../';
require $path.'bootstrap/autoload.php';
require $path.'vendor/autoload.php';
$app = require $path.'bootstrap/app.php';

$kernel = $app->make('Illuminate\Contracts\Http\Kernel');

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$id = $app['encrypter']->decrypt($_COOKIE[$app['config']['session.cookie']],false);
$app['session']->driver()->setId($id);
$app['session']->driver()->start();

if(!$app['auth']->check()){
    die('can not access');
}
$instance=$app['getInstanceObject']->setCurrentInstance()->getCurrentInstance();
$language=$app['getInstanceObject']->getLanguage();
$HTTPHOST=((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http"). "://". $instance->name.'.'.env('APP_HOST','farayad.org');
if(!$instance){
    die('can not find instance');
}
$source_path=config('filesystems.file_manager_storage').DIRECTORY_SEPARATOR.$instance->name.DIRECTORY_SEPARATOR;
if(!File::exists($source_path)){
    File::makeDirectory($source_path,0777,true,true);
}
$thumbs_path=config('filesystems.file_manager_storage').DIRECTORY_SEPARATOR.'thumbs'.DIRECTORY_SEPARATOR.$instance->name.DIRECTORY_SEPARATOR;
if(!File::exists($thumbs_path)){
    File::makeDirectory($thumbs_path,0777,true,true);
}
$langCode=$language->code;
$thumbs_upload_dir='/storage/thumbs/'.$instance->name.DIRECTORY_SEPARATOR;
$upload_dir='/storage/'.$instance->name.DIRECTORY_SEPARATOR;
