$(function() {


    var submitFunc=[];
    var onPos='';
    var onEdit=false;

    function editorSortable(){
        $( ".editor-blocks" ).sortable({
            connectWith:'.editor-blocks',
            items:'.block',
            placeholder:'block-place',
        });
    }
    editorSortable();
    function ajaxLoder(){
        $(".editor-ajax-box").html('<div class="loader"></div>');
    }

    function executeFunctionByName(functionName) {
        eval(functionName)
    }

    function loadDynamicFile(name,type,content,iscontent,$callback) {
        if( type == 'js') {
            if(iscontent){
                $('.modal-head').append('<script type="text/javascript">'+content+'<\/script>');
            }else{
                $.getScript(content,function () {
                    if($callback && $callback.length){
                        executeFunctionByName($callback);
                    }
                });
            }
        }else if( type == 'css' ) {
            if(iscontent){
                $('.modal-head').append('<style type="text/css">'+content+'</style>');
            }else{
                $('.modal-head').append('<link href="'+content+'" rel="stylesheet" type="text/css">');
            }
        }
    }

    var modalBaseHtml=$(".editor-ajax-box").html();

    var ajaxUrl=function($url,$method,$data){
        if($method=='POST'){
            $sendData=$data;
        }else{
            $sendData=false;
        }
        jQuery.ajax({
            method: $method,
            dataType: 'json',
            xhrFields: {
                withCredentials: true
            },
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                'ajax':1
            },
            url: $url,
            data: $sendData,
            processData: false,
            success: function($result){
                if($result.error){
                    $('.editor-ajax-box').html($result.content);
                }else if($result.action && $result.action=='set'){
                    if(onEdit===false){
                        for(var key in $result.content){
                            $(onPos+' .editor-blocks').append($result.content[key]);
                        }
                    }else{
                        for(var key in $result.content){
                            $(onEdit).eq(0).replaceWith($result.content[key]);
                        }
                    }
                    updateEditorJson();
                    $('.editor-modal').modal('hide');
                }else{
                    $('.editor-ajax-box').html($result.content);
                    if($result.asset){
                        Object.keys($result.asset).forEach(function(k){
                            loadDynamicFile($result.asset[k].name,$result.asset[k].type,$result.asset[k].content,$result.asset[k].iscontent,$result.asset[k].callback);
                        });
                    }
                    if($result.submitFunc){
                        submitFunc=$result.submitFunc;
                    }
                }
                $('.editor-modal').find('.submit').each(function () {
                    $form = $(this.form);
                    $($form).submit(function (e) {
                        e.preventDefault();
                        if(submitFunc.length){
                            Object.keys(submitFunc).forEach(function(k){
                                executeFunctionByName(submitFunc[k].name+'()');
                            });
                        }
                        var formData = new FormData(this);
                        ajaxLoder();
                        ajaxUrl($($form).attr('action'),'POST',formData);
                        return false;
                    });
                });
                editorSortable();
            },
            error: function(e){

            }
        });
    };

    $(document).on('click','.editor-modal .a-btn-ajax',function(e){
        modalBaseHtml=$(".editor-ajax-box").html();
        ajaxLoder();
        e.preventDefault();
        ajaxUrl($(this).attr('href'),'GET',{});
        return false;
    });

    $(document).on('click','.editor-block-remover',function(e){
        var parentBlock=$(this).parents('.block');
        var group=$(parentBlock).attr('data-group');
        $(parentBlock).remove();
        if(typeof group !== "undefined" && group.length > 0){
            $('.'+group).remove();
        }
        updateEditorJson();
        e.preventDefault();
        return false;
    });

    $(document).on('click','.editor-block-edit',function(e){
        e.preventDefault();
        var parentBlock=$(this).parents('.block');
        var group=$(parentBlock).attr('data-group');
        if(typeof group !== "undefined" && group.length > 0){
            $json={};
            $i=0;
            $('.'+group).find('.editor-json-value').each(function( index ) {
                $json[$i]=JSON.parse($(this).val());
                $i=$i+1;
            });
            onEdit='.'+group;
        }else{
            $json=JSON.parse($(parentBlock).find('.editor-json-value').val());
            onEdit='#'+$(parentBlock).attr('id');
        }
        var formData = jsonToFormData($json);
        ajaxLoder();
        ajaxUrl(layoutEditUrl,'POST',formData);
        $('.editor-modal').modal();
        return false;
    });
    $(document).on('click','.editor-block-show',function(e){
        e.preventDefault();
        var parentBlock=$(this).parents('.block');
        var group=$(parentBlock).attr('data-group');
        if($(this).hasClass('show')){
            $removeClass='fa-eye';
            $addClass='fa-eye-slash';
            $active=0;
        }else{
            $removeClass='fa-eye-slash';
            $addClass='fa-eye';
            $active=1;
        }
        if(typeof group !== "undefined" && group.length > 0){
            $('.'+group).find('.editor-json-value').each(function( index ) {
                var parentBlock=$(this).parents('.block');
                $json=JSON.parse($(this).val());
                $json.active=$active;
                $(this).val(JSON.stringify($json));
                $(parentBlock).find('.editor-block-show .fa').removeClass($removeClass).addClass($addClass);
                if(!$active){
                    $(parentBlock).find('.editor-block-show').removeClass('show');
                }
            });
            onEdit='.'+group;
        }else{
            blockInput=$(parentBlock).find('.editor-json-value');
            $json=JSON.parse($(blockInput).val());
            $json.active=$active;
            $(blockInput).val(JSON.stringify($json));
            $(this).find('i.fa').removeClass($removeClass).addClass($addClass);
            if(!$active){
                $(this).removeClass('show');
            }
        }
        updateEditorJson();
        return false;
    });

    function jsonToFormData($json,$formData){
        if(typeof $formData == "undefined"){
            $formData=new FormData();
        }
        for ( var key in $json ) {
            if(typeof $json[key] === "object"){
                jsonToFormData($json[key],$formData)
            }else{
                $formData.append(key, $json[key]);
            }
        }
        return $formData;
    }

    $(document).on('click','.editor-modal-run',function(e){
        onEdit=false;
        onPos='#'+$(this).attr('data-id');
        $('.editor-modal').modal();
    });



    $('.editor-modal').on('hidden.bs.modal', function (e) {
        submitFunc=[];
        $(".editor-ajax-box").html( modalBaseHtml);
    });



    function updateEditorJson(){
        $result={};
        $( ".editor-box-blocks" ).each(function( index ) {
            var position=$(this).attr('data-id');
            $json={};
            $i=0;
            $(this).find('.editor-json-value').each(function( index ) {
                $json[$i]=JSON.parse($(this).val());
                $i=$i+1;
            });
            $result[position]=$json;
        });
        $('.editor-all-json').val(JSON.stringify($result));
    }

    updateEditorJson();
});