if(!Array.isArray(video_resize_func)){
    var video_resize_func=[];
}
function set_player_bitrate(info){
    this.currentvideobitrate=info.height;
}

function videoAjaxApi(value,qid,videoid){
    if(video_player_host.length){
        iget=video_player_host+'/save/'+videoid+'/?qid='+qid+'&value='+value;
        $.ajax({
            method: "GET",
            dataType: 'json',
            xhrFields: {
                withCredentials: true
            },
            url: iget,
            success: function(data){
                console.log(data);
                return true;
            },
            error: function(e){
                //console.log(e);
                return false;
            }
        });
    }

}



function videoLogApi(iplayer,videoid,action){
    if(video_player_host.length) {
        iplayer.saveaction=action;
        var CurrentTime=parseInt(iplayer.getCurrentTime());
        if(action=='timeupdate'){
            if(CurrentTime==0 || CurrentTime < (iplayer.CurrentTimeSave + 5)){
                return false;
            }
        }
        iplayer.CurrentTimeSave=CurrentTime;
        iget=video_player_host+'/log/'+videoid+'/?action='+action+'&currenttime='+CurrentTime
            +(iplayer.uuid==undefined?'':'&uuid='+iplayer.uuid)
            +(iplayer.currentvideobitrate==undefined?'&quality=auto':'&quality='+iplayer.currentvideobitrate);
        $.ajax({
            method: "GET",
            dataType: 'json',
            xhrFields: {
                withCredentials: true
            },
            url: iget,
            success: function(data){
                if(action=='play'){
                    if(data.uuid != iplayer.uuid){
                        iplayer.uuid=data.uuid;
                    }
                }
                return true;
            },
            error: function(e){
                //console.log(e);
                return false;
            }
        });
    }

}


function addEventsCrossBrowsers(elemenet,event,func){
    if(elemenet.addEventListener){
        elemenet.addEventListener(event,func,false);
    }else if(elemenet.attachEvent){
        elemenet.attachEvent("on"+event,func);
    }
}

function removeEventsCrossBrowsers(elemenet,event,func){
    if (elemenet.removeEventListener) {                   // For all major browsers, except IE 8 and earlier
        elemenet.removeEventListener(event, func);
    } else if (elemenet.detachEvent) {                    // For IE 8 and earlier versions
        elemenet.detachEvent("on"+event, func);
    }
}



function addResizeEventsCrossBrowsers(func) {
    if(Array.isArray(video_resize_func) && video_resize_func.length){
        video_resize_func.forEach(function(last_func){
            var id=last_func.name.replace("resizeP", "p");
            var element=document.getElementById(id);
            if(!element){
                removeEventsCrossBrowsers(window, "resize", last_func);
                removeEventsCrossBrowsers(document, "fullscreenchange", last_func);
                removeEventsCrossBrowsers(document, "mozfullscreenchange", last_func);
                removeEventsCrossBrowsers(document, "webkitfullscreenchange", last_func);
                removeEventsCrossBrowsers(document, "msfullscreenchange", last_func);
            }
        })
    }
    video_resize_func.push(func);
    addEventsCrossBrowsers(window, "resize", func);
    addEventsCrossBrowsers(document, "fullscreenchange", func);
    addEventsCrossBrowsers(document, "mozfullscreenchange", func);
    addEventsCrossBrowsers(document, "webkitfullscreenchange", func);
    addEventsCrossBrowsers(document, "msfullscreenchange", func);
}
