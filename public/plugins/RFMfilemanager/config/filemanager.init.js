$('.fm-container').richFilemanager({
    callbacks: {
        beforeCreateImageUrl: function (resourceObject, url) {
            return url;
        },
        beforeCreatePreviewUrl: function (resourceObject, url) {
            return '/storage'+resourceObject.attributes.path;
        },
        beforeSelectItem: function (resourceObject, url) {
            return '/storage'+resourceObject.attributes.path;
        },
    },
    // options for the plugin initialization step and callback functions, see:
    // https://github.com/servocoder/RichFilemanager/wiki/Configuration-options#plugin-parameters
});