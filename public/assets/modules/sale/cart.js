$(function() {
    $(document).on('click','.swal2-close-custom',function(e){
        if($('.swal2-close').length){
            $('.swal2-close').trigger("click");
        }
    });
    $('.cart-add-submit').each(function () {
        $form=$(this.form);
        $($form).submit(function(e){
            e.preventDefault();
            $formData = new FormData(this);
            $.ajax({
                method: 'POST',
                dataType: 'json',
                contentType: false,
                xhrFields: {
                    withCredentials: true
                },
                headers: {
                    'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                    'ajax': 1,
                },
                url: $(this).attr('action'),
                data: $formData ,
                processData: false,
                success: function ($result) {
                    if($result.action){
                        if($('.MyBasket_No').length){
                            $('.MyBasket_No').html($result.data.count);
                        }
                        swal({
                           type: 'success',
                           title:'',
                           text:$result.message,
                           footer:$result.data.buttons,
                           showConfirmButton:false,
                           showCloseButton: true,
                           showCancelButton: false,
                        });
                    }else{
                        swal({
                                type: 'error',
                                title:'',
                                text:$result.message,
                                showConfirmButton:false,
                                showCloseButton: true,
                                showCancelButton: false,
                            });
                    }
                },
                error: function (e) {
                    swal({
                        type: 'error',
                        title:'',
                        text:e.message,
                        showConfirmButton:false,
                        showCloseButton: true,
                        showCancelButton: false,
                    });
                },
            });
            return false;
        });
    })

});