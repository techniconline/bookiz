           $(function() {
               $(document).on('click','.wysiwyg-close',function(e){
                    closeWysiwyg();
                    e.preventDefault();
                    return false;
                });
                $(document).on('click','.wysiwyg-open',function(e){
                    openWysiwyg();
                    e.preventDefault();
                    return false;
                });
           });
           
           function submitWysiwyg(){
               if(tinymce.activeEditor){
                    document.getElementById('wysiwyg-mce').value=tinyMCE.get('wysiwyg-mce').getContent();
               }
           }
           
           function closeWysiwyg(){
                tinymce.remove();
                $('.wysiwyg-close').hide();
                $('.wysiwyg-open').show();
           }
         
           function openWysiwyg(){
               tinymce.init({
                  selector: '.wysiwyg',
                  height: 300,
                  theme: 'modern',
                  plugins: 'directionality searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount  contextmenu colorpicker textpattern',
                  toolbar1: 'formatselect | ltr rtl | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
                  image_advtab: true,
                  style_formats: [],
               });
               $('.wysiwyg-open').hide();
               $('.wysiwyg-close').show();
           }
