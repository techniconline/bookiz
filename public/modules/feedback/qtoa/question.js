jQuery(document).ready(function ($) {

    $("body").on("click", "a._voiceanswer", function (event) {
        event.preventDefault();
        var $clicked = $(this);

        var $root = $('ul#list_question');
        $root.find('._voice_recorder').css('display', 'none');

        var $question = $clicked.parents('._question').first();


        $question.find('._voice_recorder').css('display', 'block');
        $question.find('._answer_user_form').css('display', 'block');
        $question.find('._answer_user_form').css('display', 'block');
        $question.find('.form-horizontal').css('display', 'none');
        $question.find('._answers').css('display', 'none');


      

    });






    $("body").on("click", "div._question a._answer_user_active", function (event) {
        event.preventDefault();
        var $clicked = $(this);

        //null attr blob of voice
        $blob = null;

        var $root = $('ul#list_question');
        $root.find('._answer_user_form').css('display', 'none');
        $root.find('._answer_user_form input, textarea').prop('disabled', true);


        var $question = $clicked.parents('._question').first();
        $question.find('._answer_user_form input, textarea').prop('disabled', false);

        $question.find('._answer_user_form').fadeIn();
        $root.find('._answers').css('display', 'none');
        $root.find('.form-horizontal').css('display', 'block');
        $root.find('._voice_recorder').css('display', 'none');

    });

    $("body").on("click", "div._question a._answer_list_active", function (event) {
        event.preventDefault();
        var $clicked = $(this);

        var $root = $('ul#list_question');
        $root.find('._answers').css('display', 'none');

        var $question = $clicked.parents('._question').first();
        $question.find('._answers').fadeIn();

        $root.find('._answer_user_form').css('display', 'none');


    });

    $("body").on("click", "div._question a._like, a._dislike", function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $href = $clicked.attr('href');
        var $root = $('ul#list_question');
        var $text_confirm = $root.attr('data-confirm-text');
        ajaxCall($clicked, $href, "POST", [], $text_confirm, false);
    });

    $("body").on("click", "input._save", function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $form = $clicked.parents('form').first();
        var $href = $form.attr('action');
        var $root = $('ul#list_question');
        var $text_confirm = $root.attr('data-confirm-text');
        var $data = $form.serialize();
        ajaxCall($clicked, $href, "POST", $data, $text_confirm, false);
    });

    $("body").on("click", ".pagination_question a", function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $href = $clicked.attr('href');
        var $target = $('div#_questions');
        ajaxPaginate($clicked, $href, $target);
    });

    $("body").on("click", ".pagination_answer a", function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $href = $clicked.attr('href');

        var $target = $clicked.parents('._answers');
        ajaxPaginate($clicked, $href, $target);
    });

});

var ajaxPaginate = function ($this, $url, $target) {
    $.ajax({
        method: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
            'ajax': 1
        },
        url: $url,
        success: function ($result) {
            if ($result.action) {
                $target.html($result.content);
            } else {
                alert($result.message);
            }

        },
        error: function (e) {
            alert('Error In Process!');
        }
    });
};

var ajaxCall = function ($this, $url, $method, $data, $confirm_message, $refresh) {

    $confirm_message = $confirm_message ? $confirm_message : "Are You Sure?";
    swal({
        title: $confirm_message,
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                method: $method,
                headers: {
                    'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                    'ajax': 1
                },
                url: $url,
                data: $data,
                success: function ($result) {
                    if ($result.action) {
                        swal({
                            type: 'success',
                            title: '',
                            text: $result.message,
                            showConfirmButton: false,
                            showCloseButton: true,
                            showCancelButton: false,
                        });
                        if ($refresh) {
                            location.reload();
                            return true;
                        }
                    } else {
                        swal({
                            type: 'error',
                            title: '',
                            text: $result.message,
                            showConfirmButton: false,
                            showCloseButton: true,
                            showCancelButton: false,
                        });
                    }


                },
                error: function (e) {
                    swal({
                        type: 'error',
                        title: '',
                        text: e.message,
                        showConfirmButton: false,
                        showCloseButton: true,
                        showCancelButton: false,
                    });
                },
            });
        }
    });

};

// voice record

var recorder;
var irecordTime = false;
var state = true;
var $blob = null;

function createAudioElement(blob) {
    $blob = blob;
    blobUrl = URL.createObjectURL(blob);
    $("#verse-record").attr('src', blobUrl);
    setTimeout(function () {
        autoPlay();
    }, 500);
}

function autoPlay() {
    $(".btn-record-play").find('i').removeClass('fa-play').addClass('fa-pause');
    versePlay = document.getElementById('verse-record');
    versePlay.play();
    versePlay.onended = function () {
        $(".btn-record-play").removeClass('onPlay');
        $(".btn-record-play").find('i').removeClass('fa-pause').addClass('fa-play');
    };
}

function startRecord() {
    // request permission to access audio stream
    navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {
        // store streaming data chunks in array
        const chunks = [];
        // create media recorder instance to initialize recording
        recorder = new MediaRecorder(stream);
        // function to be called when data is received
        recorder.ondataavailable = e => {
            // add stream data to chunks
            chunks.push(e.data);
            // if recorder is 'inactive' then recording has finished
            if (recorder.state == 'inactive') {
                // convert stream data chunks to a 'webm' audio format as a blob
                const blob = new Blob(chunks, { type: 'audio/webm' });
                // convert blob to URL so it can be assigned to a audio src attribute
                createAudioElement(blob);
            }
        };
        // start recording with 1 second time between receiving 'ondataavailable' events
        recorder.start(120000);
        // setTimeout to stop recording after 4 seconds
        /*setTimeout(() => {
            // this will trigger one final 'ondataavailable' event and set recorder state to 'inactive'
            recorder.stop();
        }, 30000);*/
    }).catch(console.error);
}

function stopRecord(auto) {
    if (recorder) {
        if (auto) {
            $(".btn-record.onRecord").removeClass('onRecord');
        }
        recorder.stop();
        if (auto) {
            setTimeout(function () {
                versePlay = document.getElementById('verse-record');
                versePlay.pause();
            }, 700);
        }
    }
}

$(function () {


    $(".btn-record").on("click", function () {
        state = true;
        $(this).addClass('onRecord');
        $(this).find('i').removeClass('fa-microphone').addClass('fa-dot-circle-o');

        $('.btn-record-play').removeClass('onPlay');
        startRecord();
        irecordTime = setTimeout(function () {
            $(".btn-record-play").trigger('click');
        }, 120000);
        console.log('record');
    });

    $(".btn-save-record").on("click", function (event) {
        event.preventDefault();
        if ($blob) {
            var $clicked = $(this);
            saveAudio($blob, $clicked);
        } else {
            alert('Not find voice record!')
        }
    });

    $(".btn-record-play").on("click", function () {
        $(this).addClass('onPlay');
        if (!state) {
            autoPlay();
            return;
        }
        if (irecordTime != false) {
            clearTimeout(irecordTime);
            irecordTime = false;
        }
        state = false;
        $('.btn-record').removeClass('onRecord');
        $('.btn-record').find('i').removeClass('fa-dot-circle-o').addClass('fa-microphone');
        stopRecord(false);
        console.log('play');
    });


});

function saveAudio(audio, $clicked) {
    var data = new FormData();
    data.append('data', audio);
    var $url = $clicked.attr('href');
    var $method = 'POST';
    var $confirm_message = $clicked.attr('data-confirm-message');
    $clicked.prop('disabled', true);

    swal({
        title: $confirm_message,
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: $url,
                type: $method,
                data: data,
                xhrFields: {
                    withCredentials: true
                },
                headers: {
                    'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                    'ajax': 1
                },
                contentType: false,
                processData: false,
                success: function ($result) {
                    if ($result.action) {
                        swal({
                            type: 'success',
                            title: '',
                            text: $result.message,
                            showConfirmButton: false,
                            showCloseButton: true,
                            showCancelButton: false,
                        });
                    } else {
                        swal({
                            type: 'error',
                            title: '',
                            text: $result.message,
                            showConfirmButton: false,
                            showCloseButton: true,
                            showCancelButton: false,
                        });
                    }
                    $clicked.prop('disabled', false);
                },
                error: function (e) {
                    swal({
                        type: 'error',
                        title: '',
                        text: e.message,
                        showConfirmButton: false,
                        showCloseButton: true,
                        showCancelButton: false,
                    });
                    $clicked.prop('disabled', false);
                },
            });
        }
    });
}



