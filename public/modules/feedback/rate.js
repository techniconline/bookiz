jQuery(document).ready(function () {

    $('body').on('click', '.form_rate input._rate', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $form = $clicked.parents('form').first();
        var $href = $form.attr("action");
        var $confirm_message = $clicked.parents("div.form_rate").first().attr("data-confirm-message");

        var $data = $(this).serialize();
        var $method = "POST";

        ajaxCall($clicked, $href, $method, $data, $confirm_message, true);

    });

    $('body').on('click', 'a._save_rate', function (event) {
        event.preventDefault();
        var $clicked = $(this);
        var $form = $clicked.parents('.form_rate').first();
        var $href = $clicked.attr("href");
        var $confirm_message = $form.attr("data-confirm-message");
        var $data = $form.serialize();
        var $method = "POST";
        ajaxCall($clicked, $href, $method, $data, $confirm_message, true);
    });


});

var ajaxCall = function ($this, $url, $method, $data, $confirm_message, $refresh) {

    $confirm_message = $confirm_message ? $confirm_message : "Are You Sure?";

    swal({
        title:  $confirm_message,
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                method: $method,
                headers: {
                    'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                    'ajax': 1
                },
                url: $url,
                data: $data,
                success: function ($result) {

                    alert($result.message);
                    if ($result.action && $refresh) {
                        // location.reload();
                        return true;
                    }


                },
                error: function (e) {
                    alert('Error In Process!');
                }
            });
        }
    });
};
