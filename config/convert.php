<?php

return [

    'video' => [
        "source_video_files" => env("VIDEO_CONVERT_SOURCE","/var/www/html/farayad-convert/storage/app/public/videos/source"),
        "destination_video_files" => env("VIDEO_CONVERT_DESTINATION","/var/www/html/farayad-convert/storage/app/public/videos/converted"),
        "origin_path" => storage_path("app/public"),
        "max_concurrent_convert" => 2,
        "url_server_convert" => env("URL_SERVER_CONVERT","http://www.farayad.local/convert/video/ffmpeg/callVideoConvert"),
        "url_call_back_convert" => env("URL_CALL_BACK_CONVERT","http://www.farayad.local/video/converted/"),
        "url_video_convert" => env("URL_VIDEO_CONVERT","http://www.farayad.local/convert/video/ffmpeg/convert"),
        "url_get_video_properties" => env("URL_GET_VIDEO_PROPERTIES","http://www.farayad-convert.local/convert/video/ffmpeg/getStatusProcess"),
        "source_video_files_local" => env("SOURCE_VOD_PATH","/mnt/nfs/vod/"),
    ],

];
