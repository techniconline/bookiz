<?php

return [
    'namespace'       => 'App',
    'base_class_name' => \Modules\Core\Models\BaseModel::class,
    'output_path'     => null,
    'no_timestamps'   => null,
    'date_format'     => null,
    'connection'      => null,
    'db_types' => [
        'enum' => 'string',
    ],
];