<?php

return [
    "version"=>1,
    "prefix_back_url_to_app"=>"intent://beautyday.ir#Intent;scheme=https;package=org.beautyday.beauty",
    "post_back_url_to_app"=>";end",
    "back_url_to_owner_panel"=>"http://panel.beautyday.ir/#/bookings",
    'splash' => [
        'ANDROID_FORCE_VER'=> env("ANDROID_FORCE_VER", "1.0.0"),
        'ANDROID_LAST_VER'=> env("ANDROID_LAST_VER", "1.0.0"),
        'IOS_FORCE_VER'=> env("IOS_FORCE_VER", "1.0.0"),
        'IOS_LAST_VER'=> env("IOS_LAST_VER", "1.0.0"),
        "BASE_URL" => env("BASE_URL", "http://api.beautyday.ir"),
        "SUPPORT_NUMBER" => env("SUPPORT_NUMBER", "02188176609"),
        "SUPPORT_CHAT_LINK" => '',
    ],
];
