<?php

namespace Modules\Entity\Models\Service;

use Modules\Core\Models\BaseModel;
use Modules\Entity\Traits\Service\EntityRelationServicePrices as TraitModel;

class EntityRelationServicePrices extends BaseModel
{

    /**
     * @var array
     */
    protected $fillable = ['entity_id', 'entity_relation_service_id', 'price', 'created_at', 'updated_at'];

    use TraitModel;

    public $api_fields = [];
    public $list_fields = [];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;

}
