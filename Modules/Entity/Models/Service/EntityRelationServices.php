<?php

namespace Modules\Entity\Models\Service;


use Modules\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Entity\Traits\Service\EntityRelationServices as TraitModel;

class EntityRelationServices extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['entity_id', 'entity_service_id', 'title', 'price', 'discount', 'currency_id', 'image', 'icon', 'description', 'params', 'period', 'active', 'deleted_at', 'created_at', 'updated_at'];
    use SoftDeletes;
    use TraitModel;

    public $api_fields = ['id', 'entity_id', 'entity_service_id', 'title', 'price', 'discount', 'currency_id', 'image', 'icon', 'description'];
    public $list_fields = ['id', 'entity_id', 'entity_service_id', 'title', 'price', 'discount', 'currency_id', 'image', 'icon', 'description'];
    public $api_append_fields = ['amount', 'amount_text', 'discount_text'];
    public $list_append_fields = ['amount', 'amount_text', 'discount_text'];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];


}
