<?php

namespace Modules\Entity\Models\Service;

use Modules\Core\Models\BaseModel;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Entity\Traits\Service\EntityServices as TraitModel;

class EntityServices extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'language_id', 'parent_id', 'title', 'slug', 'image', 'icon', 'params', 'sort', 'description', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;
    use TraitModel;

    public $api_fields = ['id', 'instance_id', 'language_id', 'parent_id', 'title', 'slug', 'image', 'icon', 'description'];
    public $list_fields = ['id', 'instance_id', 'language_id', 'parent_id', 'title', 'slug', 'image', 'icon', 'description'];
    public $api_append_fields = ["entity_count"];
    public $list_append_fields = ["entity_count"];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];


}
