<?php

namespace Modules\Entity\Models\Category;

use Modules\Core\Models\BaseModel;
use Modules\Entity\Traits\Category\EntityCategoryRelation as TraitModel;

class EntityCategoryRelation extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['entity_id', 'category_id'];

    public $messages = [];
    public $rules = [];

    public $timestamps = false;
    protected $primaryKey = ['entity_id', 'category_id'];
    public $incrementing = false;
}
