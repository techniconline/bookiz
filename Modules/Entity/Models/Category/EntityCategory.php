<?php

namespace Modules\Entity\Models\Category;

use Modules\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Entity\Traits\Category\EntityCategory as TraitModel;

class EntityCategory extends BaseModel
{

    use SoftDeletes;
    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['parent_id', 'instance_id', 'language_id', 'root_id', 'title', 'alias', 'attributes', 'short_description', 'sort', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $api_fields = ['id', 'title', 'alias', 'attributes', 'short_description', 'sort'];
    public $list_fields = ['id', 'title', 'alias', 'attributes', 'short_description', 'sort'];
    public $api_append_fields = ['url_get_childes', 'url_entities', 'entities_count'];
    public $list_append_fields = ['url_get_childes', 'url_entities', 'entities_count', 'childes_count'];


    protected $appends = ['url_get_childes', 'url_entities'];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
