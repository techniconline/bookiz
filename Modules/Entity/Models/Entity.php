<?php

namespace Modules\Entity\Models;

use Modules\Core\Models\BaseModel;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Entity\Traits\Entity\EntityTrait as TraitModel;

class Entity extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'language_id', 'category_id', 'parent_id', 'user_id', 'title'
        , 'slug', 'short_description', 'description', 'params', 'active_date', 'expire_date'
        , 'active', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;
    use TraitModel;

    public $api_fields = ['id','category_id', 'title', 'slug', 'short_description', 'description','rate','rate_others'];
    public $list_fields = ['id','category_id', 'title', 'slug', 'short_description','rate'];
    public $api_append_fields = ["media_image", 'liked', 'params_json'];
    public $list_append_fields = ["media_image", 'liked'];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $appends = ['rate','rate_others'];



}
