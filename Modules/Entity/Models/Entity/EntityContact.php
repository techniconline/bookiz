<?php

namespace Modules\Entity\Models\Entity;


use Modules\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Entity\Traits\Entity\EntityContact as TraitModel;


class EntityContact extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['entity_id', 'media_type', 'media_value', 'is_default', 'active', 'deleted_at', 'created_at', 'updated_at'];
    use SoftDeletes;
    use TraitModel;

    protected $appends=["media_class", "media_link"];

    public $api_fields = [];
    public $list_fields = [];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
