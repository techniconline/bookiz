<?php

namespace Modules\Entity\Models\Entity;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Entity\Traits\Entity\EntityEmployeeServices as TraitModel;

class EntityEmployeeServices extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['entity_employee_id', 'entity_relation_service_id', 'for_gender', 'params', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;
    use TraitModel;

    public $api_fields = ['id','entity_employee_id', 'entity_relation_service_id', 'for_gender', 'params'];
    public $list_fields = ['id','entity_employee_id', 'entity_relation_service_id', 'for_gender', 'params'];
    public $api_append_fields = ['params_json'];
    public $list_append_fields = ['prams_json'];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];


}
