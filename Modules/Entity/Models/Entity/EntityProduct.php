<?php

namespace Modules\Entity\Models\Entity;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Entity\Traits\Entity\EntityProduct as TraitModel;

class EntityProduct extends BaseModel
{
    use SoftDeletes;
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['entity_id', 'title', 'unit', 'quantity', 'order_point', 'notify', 'params', 'active', 'deleted_at', 'created_at', 'updated_at'];
    public $api_fields = [];
    public $list_fields = [];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];


}
