<?php

namespace Modules\Entity\Models\Entity;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Entity\Traits\Entity\EntityProductService as TraitModel;

class EntityProductService extends BaseModel
{
    use SoftDeletes;
    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['entity_product_id', 'entity_relation_service_id', 'used', 'params', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $api_fields = [];
    public $list_fields = [];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
