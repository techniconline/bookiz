<?php

namespace Modules\Entity\Models\Entity;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;

class EntityEmployeeExperienceInfo extends BaseModel
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'entity_employee_experience_info';

    /**
     * @var array
     */
    protected $fillable = ['entity_employee_id', 'experience', 'certificate_start_date', 'certificate_end_date'
                            , 'work_experience', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;

    public $api_fields = [];
    public $list_fields = [];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityEmployee()
    {
        return $this->belongsTo(EntityEmployee::class);
    }
}
