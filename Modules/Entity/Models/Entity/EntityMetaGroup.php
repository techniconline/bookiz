<?php

namespace Modules\Entity\Models\Entity;

use Modules\Core\Models\BaseModel;
use Modules\Entity\Traits\Entity\EntityMetaGroup as TraitModel;

class EntityMetaGroup extends BaseModel
{

    /**
     * @var array
     */
    protected $fillable = ['entity_id', 'meta_group_id', 'position'];

    use TraitModel;

    public $api_fields = [];
    public $list_fields = [];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [];
    public $timestamps = false;

}
