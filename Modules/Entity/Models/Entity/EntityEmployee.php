<?php

namespace Modules\Entity\Models\Entity;

use Modules\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Entity\Traits\Entity\EntityEmployee as TraitModel;

class EntityEmployee extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['entity_id', 'user_id', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;
    use TraitModel;

    public $api_fields = ['id','entity_id', 'user_id'];
    public $list_fields = ['id','entity_id', 'user_id'];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
