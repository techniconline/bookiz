<?php

namespace Modules\Entity\Models\Entity;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;

class EntityEmployeeInfo extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'entity_employee_info';

    /**
     * @var array
     */
    protected $fillable = ['entity_employee_id', 'married', 'count_childes', 'nationality', 'religion', 'blood_type', 'passport_start_date'
        , 'passport_end_date', 'stay_start_date', 'stay_end_date', 'driving_license_start_date', 'driving_license_end_date', 'certificate_start_date'
        , 'certificate_end_date', 'weight', 'length', 'history_disease', 'drug_history', 'active', 'deleted_at', 'created_at', 'updated_at'];
    use SoftDeletes;

    public $api_fields = ['id', 'married', 'count_childes', 'nationality', 'religion', 'blood_type', 'weight', 'length', 'history_disease', 'drug_history'];
    public $list_fields = ['id', 'married', 'count_childes', 'nationality', 'religion', 'blood_type'
        , 'passport_end_date', 'stay_end_date', 'driving_license_end_date'
        , 'certificate_end_date', 'weight', 'length'];
    public $api_append_fields = [
        'passport_start_date_by_mini_format',
        'passport_end_date_by_mini_format',
        'stay_start_date_by_mini_format',
        'stay_end_date_by_mini_format',
        'driving_license_start_date_by_mini_format',
        'driving_license_end_date_by_mini_format',
        'certificate_start_date_by_mini_format',
        'certificate_end_date_by_mini_format'
    ];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];


    /**
     * @return mixed
     */
    public function getPassportStartDateByMiniFormatAttribute()
    {
        $value = $this->passport_start_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.date_medium'));
        }
        return $value;
    }

    /**
     * @return mixed
     */
    public function getPassportEndDateByMiniFormatAttribute()
    {
        $value = $this->passport_end_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.date_medium'));
        }
        return $value;
    }

    /**
     * @return mixed
     */
    public function getStayStartDateByMiniFormatAttribute()
    {
        $value = $this->stay_start_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.date_medium'));
        }
        return $value;
    }

    /**
     * @return mixed
     */
    public function getStayEndDateByMiniFormatAttribute()
    {
        $value = $this->stay_end_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.date_medium'));
        }
        return $value;
    }

    /**
     * @return mixed
     */
    public function getDrivingLicenseStartDateByMiniFormatAttribute()
    {
        $value = $this->driving_license_start_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.date_medium'));
        }
        return $value;
    }

    /**
     * @return mixed
     */
    public function getDrivingLicenseEndDateByMiniFormatAttribute()
    {
        $value = $this->driving_license_end_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.date_medium'));
        }
        return $value;
    }

    /**
     * @return mixed
     */
    public function getCertificateStartDateByMiniFormatAttribute()
    {
        $value = $this->certificate_start_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.date_medium'));
        }
        return $value;
    }

    /**
     * @return mixed
     */
    public function getCertificateEndDateByMiniFormatAttribute()
    {
        $value = $this->certificate_end_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.date_medium'));
        }
        return $value;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityEmployee()
    {
        return $this->belongsTo(EntityEmployee::class);
    }
}
