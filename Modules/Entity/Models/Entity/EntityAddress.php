<?php

namespace Modules\Entity\Models\Entity;

use Modules\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Entity\Traits\Entity\EntityAddress as TraitModel;

class EntityAddress extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['entity_id', 'country_id', 'province_id', 'city_id', 'address', 'latitude', 'longitude', 'location', 'is_default', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;
    use TraitModel;

    public $api_fields = ['id','entity_id','province_id', 'city_id', 'address', 'latitude', 'longitude','is_default'];
    public $list_fields = ['id','entity_id','province_id', 'city_id', 'address', 'latitude', 'longitude','is_default'];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
