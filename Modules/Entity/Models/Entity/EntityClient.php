<?php

namespace Modules\Entity\Models\Entity;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Entity\Models\Entity;
use Modules\User\Models\User;

class EntityClient extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['entity_id', 'user_id', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;

    public $api_fields = ['id','entity_id', 'user_id'];
    public $list_fields = ['id','entity_id', 'user_id'];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(BridgeHelper::getUser()->getUserClass());
    }
}
