<?php

namespace Modules\Entity\Models\Entity;

use Modules\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Entity\Traits\Entity\EntityMedias as TraitModel;

class EntityMedias extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['entity_id', 'entity_relation_service_id', 'type', 'src', 'description', 'sort', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;
    use TraitModel;

    public $api_fields = ['id','entity_id', 'entity_relation_service_id', 'type', 'description', 'sort'];
    public $list_fields = ['id','entity_id', 'entity_relation_service_id', 'type', 'sort'];
    public $api_append_fields = ['url_thumbnail','url_image'];
    public $list_append_fields = ['url_thumbnail'];

    public $messages = [];
    public $rules = [];
    const STORAGE_PREFIX = 'storage';

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $appends = ['url_thumbnail','url_image'];

}
