<?php

return [
    'version' => '20180619001',
    'blocks' => [
        'left' => [],
        'top' => []
    ],
    'menus' => [
        'management' => [],
        'management_admin' => [
            [
                'alias' => '', //**
                'route' => 'entity.index', //**
                'key_trans' => 'entity::menu.entity_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-map-marker', //fa-XXXXX
                'class' => '',
                'id' => '',
                'group_menu' => 'admin_entities',
            ],
            [
                'alias' => '', //**
                'route' => 'entity.category.index', //**
                'key_trans' => 'entity::menu.entity_category_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-sitemap', //fa-XXXXX
                'class' => '',
                'id' => '',
                'group_menu' => 'admin_entities',
            ],
            [
                'alias' => '', //**
                'route' => 'entity.service.index', //**
                'key_trans' => 'entity::menu.entity_service_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-tags', //fa-XXXXX
                'class' => '',
                'id' => '',
                'group_menu' => 'admin_entities',
            ],
            ['alias' => 'entity.config.instance.index', //**
                'route' => 'entity.config.instance.index', //**
                'key_trans' => 'entity::menu.config.instance', //**
                'order' => null, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-cog', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_manage_settings',
            ],

        ],
    ],
    'group_menu' => [
        'management' => [],
        'management_admin' => [
            ['alias' => 'admin_entities', //**
                'key_trans' => 'entity::menu.group.entities', //**
                'icon_class' => 'fa fa-building-o',
                'before' => '',
                'after' => '',
                'order' => 10,
            ],
        ]
    ],

];
