@if($contacts)
    <ul>
        @foreach($contacts as $contact)
        <li style="display:inline-block; float: left; padding: 3px; font-size: 1.2em">
            <a href="{!! $contact->media_link !!}">
                <i class="{!! $contact->media_class !!}"></i>
            </a>
        </li>
        @endforeach
    </ul>
@endif