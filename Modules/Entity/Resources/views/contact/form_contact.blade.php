<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("entity::entity.".($view_model->getModelData('id')?'edit':'add'))
                </div>
                <div class="tools">
                    <a href="" class="collapse"> </a>
                </div>
            </div>
            {{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>($view_model->getModelData('id')? route('entity.contact.update', ['entity_id'=>$view_model->getModelData('id')]) : route('entity.contact.save'))
                        ,'method'=>($view_model->getModelData('id')?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("entity::entity.tabs.public") </a>
                                </li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::selectTag('media_type',trans("entity::entity.contacts.media_types"),old('media_type',$view_model->getModelData('media_type')),['title'=>trans("entity::entity.contacts.media_type"),'helper'=>trans("entity::entity.contacts.media_type")]) !!}

                                    {!! FormHelper::input('text','media_value',old('media_value',$view_model->getModelData('media_value'))
                                        ,['required'=>'required','title'=>trans("entity::entity.contacts.media_value"),'helper'=>trans("entity::entity.contacts.media_value")]) !!}

                                    {!! FormHelper::checkbox('is_default',1,old('is_default',$view_model->getModelData('is_default')) ,['title'=>trans("entity::entity.contacts.is_default")]) !!}

                                    {!! FormHelper::selectTag('entity_id',[],old('entity_id',$view_model->getModelData('entity_id',$view_model->request->get("entity_id"))),['data-ajax-url'=>route('entity.get_entity_by_api')
                                      ,'title'=>trans("entity::entity.contacts.title_entity"),'helper'=>trans("entity::entity.contacts.title_entity")]) !!}

                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("entity::entity.submit")], ['title'=>trans("entity::entity.cancel"), 'url'=>route('entity.contact.index',["entity_id"=>$view_model->getModelData('entity_id',$view_model->request->get("entity_id"))])]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>

        </div>

    </div>

</div>
