<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("entity::entity.".($view_model->getModelData('id')?'edit':'add'))
                </div>
                <div class="tools">
                    <a href="" class="collapse"> </a>
                </div>
            </div>
            {{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>($view_model->getModelData('id')? route('entity.address.update', ['entity_id'=>$view_model->getModelData('id')]) : route('entity.address.save'))
                        ,'method'=>($view_model->getModelData('id')?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("entity::entity.tabs.public") </a>
                                </li>

                            </ul>
                            <div class="tab-content">

                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::select('country_id',[],old('country_id',$view_model->getModelData('country_id')),['data-init'=>1,'data-fill-url'=>route('core.location.json'),'data-change-trigger'=>'province_id','title'=>trans('user::form.fields.country'),'helper'=>trans('user::form.helper.country')]) !!}

                                    {!! FormHelper::select('province_id',[],old('province_id',$view_model->getModelData('province_id')),['data-fill-url'=>route('core.location.json'),'data-change-trigger'=>'city_id','title'=>trans('user::form.fields.state_id'),'helper'=>trans('user::form.helper.state_id')]) !!}

                                    {!! FormHelper::select('city_id',[],old('city_id',$view_model->getModelData('city_id')),['data-fill-url'=>route('core.location.json'),'title'=>trans('user::form.fields.city_id'),'helper'=>trans('user::form.helper.city_id')]) !!}

                                    {!! FormHelper::input('text','address',old('address',$view_model->getModelData('address'))
                                        ,['required'=>'required','title'=>trans("entity::entity.addresses.address"),'helper'=>trans("entity::entity.addresses.address")]) !!}

                                    {!! FormHelper::input('text','longitude',old('longitude',$view_model->getModelData('longitude'))
                                        ,['id'=>'longitude','title'=>trans("entity::entity.addresses.longitude"),'helper'=>trans("entity::entity.addresses.longitude")]) !!}

                                    {!! FormHelper::input('text','latitude',old('latitude',$view_model->getModelData('latitude'))
                                        ,['id'=>'latitude', 'title'=>trans("entity::entity.addresses.latitude"),'helper'=>trans("entity::entity.addresses.latitude")]) !!}

                                    {!! FormHelper::checkbox('is_default',1,old('is_default',$view_model->getModelData('is_default')) ,['title'=>trans("entity::entity.addresses.is_default")]) !!}

                                    {!! FormHelper::selectTag('entity_id',[],old('entity_id',$view_model->getModelData('entity_id',$view_model->request->get("entity_id"))),['data-ajax-url'=>route('entity.get_entity_by_api')
                                      ,'title'=>trans("entity::entity.addresses.title_entity"),'helper'=>trans("entity::entity.addresses.title_entity")]) !!}

                                    <section class="col-md-12">
                                        <div id='map_canvas' class="col-md-12" style="height: 400px"></div>
                                        {{--<div id="current">Nothing yet...</div>--}}
                                    </section>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("entity::entity.submit")], ['title'=>trans("entity::entity.cancel"), 'url'=>route('entity.address.index',["entity_id"=>$view_model->getModelData('entity_id',$view_model->request->get("entity_id"))])]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>

        </div>

    </div>

</div>
