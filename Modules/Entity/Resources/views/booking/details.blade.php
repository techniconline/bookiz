<div class="row" id="form_instance">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> {{ trans('entity::entity.bookings.detail') }}
                </div>
            </div>
            <div class="portlet-body form">
                <div class="col-md-12">
                        @php $arr = $BlockViewModel->getModelData('params_array'); @endphp
                        @foreach($arr as $key => $value)
                            <div>
                                <span>{!! $key !!}: </span>{!! $value !!}
                            </div>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>