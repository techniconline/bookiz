{!! $viewModel->getViewListComments() !!}
<div class="  row2 pad_btn_10 pad_top_10  mrg_05 fltr text-justify dirr core9 lineH1-5 bgcolor5">
    {!! FormHelper::open(['role'=>'form','url'=>route('feedback.comment.storeComment',["model"=>$viewModel->model_name, "model_id"=>$viewModel->model_id]),'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
    @php  FormHelper::setCustomAttribute('element','class','col-md-12') @endphp

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <span>
                {!! FormHelper::input('text','name',old('name',$viewModel->user?$viewModel->user->full_name:null) ,['required'=>'required','class'=>'core11 mrg_05 pad_05 dirr','placeholder'=>trans('course::form.fields.fullname')]) !!}
        </span>
        <span>
            {!! FormHelper::input('text','email',old('email',$viewModel->user?$viewModel->user->email:null) ,['required'=>'required','class'=>'core11 mrg_05 pad_05 dirr','placeholder'=>trans('course::form.fields.email')]) !!}
        </span>
    </div>

    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <span>
            {!! FormHelper::textarea('comment',old('comment',null) ,[ 'cols'=>"5",'rows'=>"4",'class'=>'','placeholder'=>trans('course::form.fields.message')]) !!}
        </span>
    </div>

    {!! FormHelper::openAction() !!}
    {!! FormHelper::submitOnly(['style'=>'margin-left: 30px;','class'=>'btn btn-success','title'=>trans("feedback::comment.submit"),'data-confirm-message'=>trans("feedback::comment.confirm")]) !!}
    {!! FormHelper::closeAction() !!}
    {!! FormHelper::close() !!}
</div>
