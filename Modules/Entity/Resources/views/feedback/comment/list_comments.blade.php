@if($comments = $viewModel->getDataComments())
    <ul class="comments" id="list_comments">
        @foreach($comments as $index => $comment)
            <li style="font-size: 14px">

                <p class="content">{!! $comment->comment !!}</p>
                <span class="comment-bottom" style="font-size: 10px">
                    <span class="user">{!! $comment->name !!}</span>
                    <span class="time">{!! DateHelper::setDateTime($comment->created_at)->getLocaleFormat(trans('core::date.datetime.medium')); !!}</span>
                    {{--<a href="#" class="report">Report</a>--}}
                </span>
            </li>
        @endforeach
    </ul>
    <ul class="comments" id="paginate">
        <li>
            {!! $comments->links() !!}
        </li>
    </ul>

@endif

{{--<ul class="comments">--}}
{{--<li>--}}
{{--<span class="stars">--}}
{{--<i class="fas fa-star"></i>--}}
{{--<i class="fas fa-star"></i>--}}
{{--<i class="fas fa-star"></i>--}}
{{--<i class="fas fa-star"></i>--}}
{{--<i class="far fa-star"></i>--}}
{{--</span>--}}
{{--<p class="content">This was the first time I had the treatment. I quite enjoyed and my skin felt good and--}}
{{--hydrated.</p>--}}
{{--<span class="comment-bottom">--}}
{{--<span class="user">Sarbjeet</span>--}}
{{--<span class="time">12 hours ago</span>--}}
{{--<a href="#" class="report">Report</a>--}}
{{--</span>--}}
{{--</li>--}}
{{--</ul>--}}
