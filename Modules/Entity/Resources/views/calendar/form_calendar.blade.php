<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("entity::entity.".($view_model->getModelData('id')?'edit':'add'))
                </div>
                <div class="tools">
                    <a href="" class="collapse"> </a>
                </div>
            </div>
            {{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>($view_model->getModelData('id')? route('entity.calendar.update', ['booking_calendar_id'=>$view_model->getModelData('id')]) : route('entity.calendar.save'))
                        ,'method'=>($view_model->getModelData('id')?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("entity::entity.tabs.public") </a>
                                </li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::selectTag('entity_id',[],old('entity_id',$view_model->getModelData('entity_id')?$view_model->getModelData('entity_id'):$view_model->request->get("entity_id")),['data-ajax-url'=>route('entity.get_entity_by_api')
                                                           ,'title'=>trans("entity::entity.calendars.title_entity"),'helper'=>trans("entity::entity.calendars.title_entity")]) !!}

                                    {!! FormHelper::selectTag('entity_relation_service_id',[],old('entity_relation_service_id',$view_model->getModelData('entity_relation_service_id')?$view_model->getModelData('entity_relation_service_id'):null),['data-ajax-url'=>route('entity.service.relation.get_entity_relation_services_by_api')
                                       ,'title'=>trans("entity::entity.calendars.title_service"),'helper'=>trans("entity::entity.calendars.title_service")]) !!}

                                    {!! FormHelper::date('start_date',old('start_date',$view_model->getModelData('start_date'))
                                    ,['title'=>trans("entity::entity.calendars.start_date"),'helper'=>trans("entity::entity.calendars.start_date")
                                        ,'date-year-current'=>1,'date-year-before'=>1,'set-default'=>0]) !!}

                                    {!! FormHelper::date('end_date',old('end_date',$view_model->getModelData('end_date'))
                                    ,['title'=>trans("entity::entity.calendars.end_date"),'helper'=>trans("entity::entity.calendars.end_date")
                                        ,'date-year-current'=>1,'date-year-before'=>1,'set-default'=>0]) !!}

                                    {!! FormHelper::input('number','period',old('period',$view_model->getModelData('period', 30))
                                        ,['required'=>'required','title'=>trans("entity::entity.calendars.period"),'helper'=>trans("entity::entity.calendars.period")]) !!}

                                    {!! FormHelper::input('number','count_reservation_in_time',old('count_reservation_in_time',$view_model->getModelData('count_reservation_in_time', 3))
                                        ,['required'=>'required','title'=>trans("entity::entity.calendars.count_reservation_in_time"),'helper'=>trans("entity::entity.calendars.count_reservation_in_time")]) !!}

                                    {!! FormHelper::input('number','days_active_reserve',old('days_active_reserve',$view_model->getModelData('days_active_reserve', 14))
                                        ,['required'=>'required','title'=>trans("entity::entity.calendars.days_active_reserve"),'helper'=>trans("entity::entity.calendars.days_active_reserve")]) !!}

                                    {!! FormHelper::selectTag('closed[]',trans("booking::calendar.all_days"),old('closed[]',isset($view_model->modelData->params_json->closed)?$view_model->modelData->params_json->closed:null),['multiple'=>'multiple','title'=>trans("entity::entity.calendars.weekend_days"),'helper'=>trans("entity::entity.calendars.weekend_days")]) !!}

                                    {!! FormHelper::select('active',trans("entity::entity.statuses"),old('active',$view_model->getModelData('active',2))
                                    ,['title'=>trans("entity::entity.status"),'helper'=>trans("entity::entity.select_status") ,'placeholder'=>trans("entity::entity.select_status")]) !!}

                                    {!! FormHelper::select('for_gender',trans("booking::calendar.genders"),old('for_gender',$view_model->getModelData('for_gender','female'))
                                    ,['title'=>trans("booking::calendar.gender"),'helper'=>trans("booking::calendar.gender") ,'placeholder'=>trans("booking::calendar.gender")]) !!}

                                    @foreach(trans("booking::calendar.all_days") as $day => $title)

{{--                                        @php $color = array_random(['red','cornflowerblue','yellowgreen','orangered','orchid','orange']) @endphp--}}

                                        <div class="col-md-6">
                                            @php $open = isset($view_model->modelData->params_json->days->$day->open)?$view_model->modelData->params_json->days->$day->open:'09:00' @endphp

                                            {!! FormHelper::input('text','params[days]['.$day.'][open]',old('params[days]['.$day.'][open]',$open)
                                                 ,['title'=>trans("entity::entity.calendars.open_time_of",['day_name'=>$title]),'helper'=>trans("entity::entity.calendars.open_time")]) !!}
                                        </div>
                                        @php $close = isset($view_model->modelData->params_json->days->$day->close)?$view_model->modelData->params_json->days->$day->close:'21:00' @endphp
                                        <div class="col-md-6">
                                            {!! FormHelper::input('text','params[days]['.$day.'][close]',old('params[days]['.$day.'][close]',$close)
                                                ,['title'=>trans("entity::entity.calendars.close_time_of",['day_name'=>$title]),'helper'=>trans("entity::entity.calendars.close_time")]) !!}
                                        </div>

                                    @endforeach
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("entity::entity.submit")]
                , ['title'=>trans("entity::entity.cancel"), 'url'=>route('entity.calendar.index',['entity_id'=>$view_model->getModelData('entity_id')?$view_model->getModelData('entity_id'):$view_model->request->get("entity_id")])]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>

        </div>

    </div>

</div>
