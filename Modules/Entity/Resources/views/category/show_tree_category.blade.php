<div class="row">
    <div class="col-md-12 padding-tb-10">
        <a href="{!! route('entity.category.index') !!}"
           class="btn btn-lg blue circle-right">
            <i class="fa fa-list"></i>
            @lang("entity::category.categories_root_list")
        </a>
    </div>
</div>
<div style="display: none">
    @include("entity::category.form")
</div>

{!!  \Modules\Entity\Providers\Helpers\Category\EntityCategoryHelperFacade::createTreeByNestable($categories)  !!}

