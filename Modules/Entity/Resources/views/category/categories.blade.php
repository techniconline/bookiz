<div class="col-md-12 _filter" id="course_categories_list">
<i class="fas fa-align-justify _iconToper"></i>
{!! FormHelper::label('label_name_group', trans("entity::entity.categories") , ['class'=>'']) !!}
    <ul class="">
        <li class="_item_category_dept_0">
            <a class="_item_category cat_mainLink" href="{!! route("entity.category.list.courses", ["id" => 0, "alias" => str_replace(" ","-",trans("entity::entity.all_categories"))]).'?'.http_build_query(request()->except("category_id")) !!}" >
                {{ trans("entity::entity.all_categories") }}
            </a>
            <div class="sidenav">
                @foreach($viewModel->getCourseCategories() as $course_category)
                    @include('entity::category.sub_categories',['dept'=>1])
                @endforeach
            </div>
        </li>
    </ul>
</div>