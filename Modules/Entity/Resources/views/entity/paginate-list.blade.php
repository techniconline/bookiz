@if($entities->isNotEmpty())

    <div class="_list_pagonate">
        @foreach($entities as $index => $entity)
            <div class="entity">
                <div class="img-container">
                    <img src="{!! $entity->media_image['url_thumbnail'] !!}" alt=""/>
                    <div class="rate">

                        @for($i = 1; 5>=$i; $i++)
                            @php $rate = (int)($entity->rate['data_rate']); @endphp
                            <i class="{!! (($i<=$rate))?'fas':'far'  !!} fa-star {!! (($i<=$rate))?'active':''  !!}"></i>
                        @endfor

                        {{--<i class="fas fa-star active"></i>--}}
                        {{--<i class="fas fa-star active"></i>--}}
                        {{--<i class="fas fa-star"></i>--}}
                        {{--<i class="fas fa-star"></i>--}}
                        {{--<i class="fas fa-star"></i>--}}
                    </div>
                </div>

                <div class="details">
                    <h3>{!! $entity->title !!}</h3>
                    <p class="address">{!! $entity->entityAddress?str_limit($entity->entityAddress->address,30):null !!}</p>

                    <a href="{!! $entity->single_link !!}">
                        @lang("entity::entity.more")
                        <i class="fas fa-chevron-left"></i>
                    </a>
                    <script>
                        @if($entity->entityAddress)
                            $data[{!! $entity->id !!}] = {
                            title: '{{$entity->title}}',
                            latitude: '{{$entity->entityAddress->latitude}}',
                            longitude: '{{$entity->entityAddress->longitude}}'
                        };

                        $size = {{ (int)($entity->rate['data_rate']*10) }};
                        $features[{!! $entity->id !!}] = {
                            "type": "Feature",
                            "marker_id": {!! $entity->id !!},
                            "properties": {
                                "message": "{{$entity->title}}",
                                "address": "{{$entity->entityAddress->address}}",
                                "image": "{{$entity->media_image['url_thumbnail']}}",
                                {{--"icon": "{{asset('/assets/img/marker-blue-1.jpg')}}",--}}
                                "iconSize": [$size, $size],
                                "properties": {
                                    "description": "<strong><a href='{!! $entity->single_link !!}' target='_blank'>{!! $entity->title !!}</a></strong><p> {!! $entity->entityAddress?str_limit($entity->entityAddress->address,30):null !!}</p><p style='text-align: center;'><img width='150px' src='{!! $entity->media_image['url_thumbnail'] !!}' alt=''/></p>",
                                    "icon": "music"
                                },
                            },
                            "geometry": {
                                "type": "Point",
                                "coordinates": [
                                    {{$entity->entityAddress->longitude}}, {{$entity->entityAddress->latitude}}
                                ]
                            }
                        };
                        @endif
                    </script>
                    @if($entity->dist)
                        <div class="distance">
                            <strong>@lang("entity::entity.distance")</strong>
                            <span>{!! $entity->dist .' '.$entity->unit !!}</span>
                        </div>
                    @endif
                </div>

            </div>
        @endforeach
    </div>
    <div class="pagination-list">
        <button data-url="{!! $entities->url(1) !!}" id="nextPage" class="btn btn-dark btn-block">@lang("entity::entity.show_more")</button>
    </div>
@endif