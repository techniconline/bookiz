<script>
    var $data = [];
    var $features = [];
</script>


<div class="list-page">
    <div class="entities">
        @php $entities = $view_model->getModelData(); @endphp

        @include('entity::entity.paginate-list',["entities"=>$entities])

        {{--<div class="_paginate">--}}
        {{--{!! $entities->links() !!}--}}
        {{--</div>--}}
    </div>

    <div id='map'></div>
</div>
<style>
    .marker {
        background-image: url({{asset('/assets/img/marker-blue-1.jpg')}});
        background-size: cover;
        width: 50px;
        height: 50px;
        border-radius: 50%;
        cursor: pointer;
    }
    .mapboxgl-popup {
        max-width: 400px;
        font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
    }
</style>

<script>


    function entity_item(entity) {
        var rate = Number(entity.rate.data_rate);
        var address = '';
        if (entity.entity_address !== null) {
            address = entity.entity_address.address;

            bounds.extend([entity.entity_address.longitude, entity.entity_address.latitude]);

            map.fitBounds(bounds);

            new mapboxgl.Marker()
                .setLngLat([entity.entity_address.longitude, entity.entity_address.latitude])
                .addTo(map);
        }

        var entity_html = `
            <div class="entity" data-id="${entity.id}">
                <div class="img-container">
                    <img src="${entity.media_image.url_image}" alt="" />
                    <div class="rate">
                        <i class="fas fa-star ${(rate > 0.5) ? 'active' : ''}"></i>
                        <i class="fas fa-star ${(rate > 1.5) ? 'active' : ''}"></i>
                        <i class="fas fa-star ${(rate > 2.5) ? 'active' : ''}"></i>
                        <i class="fas fa-star ${(rate > 3.5) ? 'active' : ''}"></i>
                        <i class="fas fa-star ${(rate > 4.5) ? 'active' : ''}"></i>
                    </div>
                </div>
                <div class="details">
                    <h3>${entity.title}</h3>
                    <p class="address">${address}</p>
                    <a href="#" class="more">
                        @lang("entity::entity.more")
                        <i class="fas fa-chevron-left"></i>
                    </a>
                </div>
            </div>`;
        $(entity_html).insertBefore('.entities .pagination-list');
    }

    // const text = getUrlParameter('text');
    var url = '?wstoken=e8c37c4ac2a3ab2d202cc8bc12584fd05521ee69&page=2';

    jQuery(document).ready(function () {

        $('#nextPage').click(function () {
            $this = $(this);
            if (url !== null) {
                $.get(url).done(function (response) {
                    if (response.action) {
                        if (response.data.next_page_url === null) $this.remove();
                        url = response.data.next_page_url;
                        response.data.data.map(function (entity) {
                            entity_item(entity);
                        });
                    }
                });
            }
        });

    });


    mapboxgl.accessToken = 'pk.eyJ1IjoidGVjaG5pY29ubGluZSIsImEiOiJjanN3b3I4bWowZWt2NDN0MzZ1dWVuZDJnIn0.fdtQtg1NYhPdHC3nu-89Ug';

    @if(get_instance()->getLanguageDirection() == 'rtl')
    mapboxgl.setRTLTextPlugin('https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.2.0/mapbox-gl-rtl-text.js');
            @endif

    var geojson = {
            "id": "markers",
            "type": "FeatureCollection",
            "features": $features
        };

    var $feature = $.grep($features, function(obj) {
        if (obj) {
            return obj.marker_id === 17;
        }
    });

    console.log($features);

    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v10',
        // style: 'mapbox://styles/mapbox/outdoors-v10',
        // style: 'mapbox://styles/mapbox/navigation-preview-day-v2',
        // style: 'mapbox://styles/mapbox/navigation-preview-night-v2',
        // style: 'mapbox://styles/mapbox/dark-v9',
        // style: 'mapbox://styles/mapbox/light-v9',
        center: [51.41266000, 35.73579000],
        zoom: 10
    });

    var bounds = new mapboxgl.LngLatBounds();
    map.addControl(new mapboxgl.NavigationControl());

    geojson.features.forEach(function (marker) {

        var el = document.createElement('div');
        el.className = 'marker';

        // el.style.backgroundImage = 'url(https://placekitten.com/g/' + marker.properties.iconSize.join('/') + '/)';
        // el.style.backgroundImage = 'url('+ marker.properties.image + ')';

        // el.style.width = marker.properties.iconSize[0] + 'px';
        // el.style.height = marker.properties.iconSize[1] + 'px';

        // create the popup
        var popup = new mapboxgl.Popup({ offset: 25 })
            .setHTML(marker.properties.properties.description);


        bounds.extend(marker.geometry.coordinates);

        // el.addEventListener('click', function () {
        //     alert('1111');
        // });

        //TODO add el to Marker(el)
        new mapboxgl.Marker()
            .setLngLat(marker.geometry.coordinates)
            .setPopup(popup) // sets a popup on this marker
            .addTo(map);
    });

    map.fitBounds(bounds);


</script>


