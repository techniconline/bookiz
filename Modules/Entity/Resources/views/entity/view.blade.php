<div class="single-page">
    <div class="slider owl-carousel owl-theme owl-2">
        @php $sliders = $view_model->getModelDataRelation('entitySliderMedias') @endphp
        @if($sliders)
            @foreach($sliders as $slider)
                <div class="item">
                    <img src="{!! $slider->url_image !!}"/>
                </div>
            @endforeach
        @else
            <div class="item">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTE9gNOnOeFQEB1c9_05suIqW9Rde_rZbkyOr95Ws7b1tfvA_2F"/>
            </div>
            <div class="item">
                <img src="https://www.biltmorehotel.com/assets/img/spa/slider/mb/the-biltmore-spa-miami.jpg"/>
            </div>
        @endif

    </div>
    <div class="details">
        <div class="info">
            <h1>{!! $view_model->getModelData('title') !!}</h1>

            {!!
            BridgeHelper::getRate()->getRateObject()
                ->setModel($view_model->getModelData()->getTable(), $view_model->getModelData('id'))
                ->setWithData(true)
                ->setWithForm(true)
                ->setWithSaveRate(false)
                ->setWithTextRate(true)
                ->setConfigs(["container_js" => "block_js", "container_css" => "block_style"])
                ->init()
                ->getForm()
            !!}

        </div>
        <h4 class="address">{!! $view_model->getModelDataRelation('entityAddress.address') !!}</h4>
        <h4 class="address">{!! $view_model->getModelData('short_description') !!}</h4>

        @include("entity::contact.renderer", ["contacts"=>$view_model->getModelDataRelation('entityContacts')])

        {{--<p class="discount">--}}
        {{--<i class="fas fa-percent"></i>--}}
        {{--Off-peak discounts--}}
        {{--</p>--}}

    </div>


    <div class="services">
        <div class="categories-container">
            <ul class="categories">
                @php $services = $view_model->getModelData('entity_services') @endphp
                @if($services)
                    <li data-type="all" class="active">
                        <img src="{!! asset('themes/default/assets/images/svg-icons/all.svg') !!}"/>
                        <img class="img-active"
                             src="{!! asset('themes/default/assets/images/svg-icons/all-active.svg') !!}"/>
                        <span>@lang("entity::entity.services.all_services") </span>
                    </li>
                    @foreach($services as $service)

                        <li data-type="service_{!! $service->id !!}">
                            <img src="{!! $service->icon !!}"/>
                            <img class="img-active" src="{!! $service->icon_active !!}"/>
                            <span>{!! $service->title !!}</span>
                        </li>

                    @endforeach

                @endif
            </ul>
        </div>
        <div class="category-contents">

        @if($services)
            @php $childes_services = []; $counter=0; @endphp

            <!-- ALL -->
                <div class="category-content active" data-category="all">
                    <ul class="nav nav-tabs sub-categories">
                        @foreach($services as $row_num => $service)
                            @php $childes_services[$service->id] = $service->childes; @endphp

                            @foreach($service->childes as $index => $child)
                                @php $entity_service_relations[$child->id] = $child->entityRelationServices; $counter++; @endphp

                                @if($entity_service_relations[$child->id]->isEmpty())
                                    @continue;
                                @endif

                                <li class="nav-item">
                                    <a  class="nav-link {!! $counter>1?'':'active' !!}" data-toggle="tab"
                                       href="#service_{!! $child->id !!}">
                                        <span class="text">{!! $child->title !!} {!! $child->description?' - '.$child->description:'' !!} </span>
                                        <span class="num">({!! $child->entityRelationServices->count() !!})</span>
                                    </a>
                                </li>

                            @endforeach

                        @endforeach
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        @php $counter=0; @endphp
                        @foreach($services as $service)
                            @foreach($service->childes as $index => $child)

                                @if($child->entityRelationServices->isEmpty())
                                    @continue;
                                @endif

                                    <div class="tab-pane {!! $counter?'':'active' !!}" id="service_{!! $child->id !!}">
                                    <ul class="services-list">

                                        @foreach($entity_service_relations[$child->id] as $entity_relation_service)
                                            {{--@php $calendar = $entity_relation_service->bookingCalendar()->first(); @endphp--}}
                                            <li data-id="{!! $entity_relation_service->id !!}"
                                                data-price="{!! $entity_relation_service->price - $entity_relation_service->discount !!}">
                                                <a data-confirm-message="@lang("entity::entity.confirm")" class="_add_cart" href="{!! route('sale.cart.add',['item'=>$entity_relation_service->getTable(), 'item_id'=>$entity_relation_service->id]) !!}">
                                            <span class="info">
                                            <span class="title">{!! $entity_relation_service->title !!}</span>
                                            <span class="time">{!! $entity_relation_service->period_by_format !!}</span>
                                            </span>
                                            <span class="prices">

                                            @if($entity_relation_service->discount)
                                                    <span class="new-price">{!! $entity_relation_service->price - $entity_relation_service->discount !!}</span>
                                                    <span class="old-price">{!! $entity_relation_service->price !!}</span>
                                            @else
                                                    <span class="new-price">{!! $entity_relation_service->price !!}</span>
                                            @endif

                                            </span>
                                                    <span class="select">@lang("entity::entity.services.select_service")</span>
                                                </a>
                                            </li>
                                        @endforeach


                                    </ul>
                                </div>

                                    @php $counter++; @endphp
                            @endforeach
                        @endforeach
                    </div>

                </div>
                <!-- ALL -->

                <!-- Service Segments -->
                @foreach($services as $row_num => $service)

                    <div class="category-content" data-category="service_{!! $service->id !!}">
                        <ul class="nav nav-tabs sub-categories">
                            @php $childes_services[$service->id] = $service->childes; @endphp
                            @php $counter = 0 @endphp
                            @foreach($service->childes as $index => $child)
                                @php $entity_service_relations[$child->id] = $child->entityRelationServices; @endphp

                                @if($entity_service_relations[$child->id]->isEmpty())
                                    @continue;
                                @endif


                                <li class="nav-item">
                                    <a class="nav-link {!! $counter?'':'active' !!}" data-toggle="tab"
                                       href="#service_{!! $child->id.'_'.sha1($child->id) !!}">
                                        <span class="text">{!! $child->title !!} {!! $child->description?' - '.$child->description:'' !!} </span>
                                        <span class="num">({!! $child->entityRelationServices->count() !!})</span>
                                    </a>
                                </li>

                                @php $counter++; @endphp
                            @endforeach

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            @php $counter = 0 @endphp
                            @foreach($service->childes as $index => $child)

                                @if($child->entityRelationServices->isEmpty())
                                    @continue;
                                @endif

                                <div class="tab-pane {!! $counter?'':'active' !!}"
                                     id="service_{!! $child->id.'_'.sha1($child->id) !!}">
                                    <ul class="services-list">


                                        @foreach($entity_service_relations[$child->id] as $entity_relation_service)
                                            <li data-id="{!! $entity_relation_service->id !!}"
                                                data-price="{!! $entity_relation_service->price - $entity_relation_service->discount !!}">
                                                <a href="#">
                                        <span class="info">
                                        <span class="title">{!! $entity_relation_service->title !!}</span>
                                            <span class="time">{!! $entity_relation_service->period_by_format !!}</span>
                                        </span>

                                                    <span class="prices">

                                            @if($entity_relation_service->discount)
                                                            <span class="new-price">{!! $entity_relation_service->price - $entity_relation_service->discount !!}</span>
                                                            <span class="old-price">{!! $entity_relation_service->price !!}</span>
                                                        @else
                                                            <span class="new-price">{!! $entity_relation_service->price !!}</span>
                                                        @endif

                                        </span>
                                                    <span class="select">@lang("entity::entity.services.select_service")</span>
                                                </a>
                                            </li>
                                        @endforeach


                                    </ul>
                                </div>
                                @php $counter++; @endphp
                            @endforeach
                        </div>

                    </div>
                @endforeach

            <!-- Service Segments -->

            @endif

        </div>
    </div>


    {!!
    BridgeHelper::getRate()->getRateObject()
        ->setModel($view_model->getModelData()->getTable(), $view_model->getModelData('id'))
        ->setWithData(true)
        ->setWithForm(true)
        ->setWithSaveRate(true)
        ->setWithTextRate(true)
        ->setTypes([
            'ambiance'=>['title'=>trans("entity::entity.ambiance")],
            'cleanliness'=>['title'=>trans("entity::entity.cleanliness")],
            'staff'=>['title'=>trans("entity::entity.staff")],
            'value'=>['title'=>trans("entity::entity.value")],
        ])
        ->setConfigs(["container_js" => "block_js", "container_css" => "block_style"])
        ->init()
        ->getForm()
    !!}


    {!!  BridgeHelper::getComment()->getCommentObject()
          ->setModel($view_model->getModelData()->getTable(), $view_model->getModelData('id'))
          ->setWithData(true)
          ->setWithForm(true)
          ->setConfigs(["perPage" => 5, "container_js" =>
          "block_js",
          "container_css" => "block_css" ,
          "template" => "entity::feedback.comment.form_comment" ,
          "template_list" => "entity::feedback.comment.list_comments"])
          ->init()
          ->getForm() !!}


    <div class="about">
        <h3>@lang("entity::entity.about")</h3>
        <div class="content" style="padding: 0px; width: 100%">
            <input type="hidden" id="longitude"
                   value="{!! $view_model->getModelDataRelation('entityAddress.longitude') !!}">
            <input type="hidden" id="latitude"
                   value="{!! $view_model->getModelDataRelation('entityAddress.latitude') !!}">
            <div id='map_cnv' style="max-width: 60%; flex: 1; height: 300px"></div>
            <div class="body">
                <h3 class="title">{!! $view_model->getModelData('title') !!}</h3>
                <p class="address">{!! $view_model->getModelDataRelation('entityAddress.address') !!}</p>
            </div>
        </div>
    </div>

    <div class="others">
        <div class="">
            {!! $view_model->getModelData('description') !!}
        </div>
        {{--<ul class="salon-times">--}}
            {{--<li class="closed">--}}
                    {{--<span class="day">--}}
                        {{--<span class="badge"></span>--}}
                        {{--<span class="text">Monday</span>--}}
                    {{--</span>--}}
                {{--<span class="time">Closed</span>--}}
            {{--</li>--}}
            {{--<li>--}}
                    {{--<span class="day">--}}
                        {{--<span class="badge"></span>--}}
                        {{--<span class="text">Tuesday</span>--}}
                    {{--</span>--}}
                {{--<span class="time">10:00 AM	– 7:00 PM</span>--}}
            {{--</li>--}}
            {{--<li>--}}
                    {{--<span class="day">--}}
                        {{--<span class="badge"></span>--}}
                        {{--<span class="text">Tuesday</span>--}}
                    {{--</span>--}}
                {{--<span class="time">10:00 AM	– 7:00 PM</span>--}}
            {{--</li>--}}
            {{--<li>--}}
                    {{--<span class="day">--}}
                        {{--<span class="badge"></span>--}}
                        {{--<span class="text">Tuesday</span>--}}
                    {{--</span>--}}
                {{--<span class="time">10:00 AM	– 7:00 PM</span>--}}
            {{--</li>--}}
        {{--</ul>--}}
    </div>

    {!! BridgeHelper::getBlockHelper()
        ->getBlock('entities_block')
        ->setConfigs([
        'title'=>trans("entity::entity.near_entities")
        ,'near_dist'=>1000
        ,'longitude'=>$view_model->getModelDataRelation('entityAddress.longitude')
        ,'latitude'=>$view_model->getModelDataRelation('entityAddress.latitude')
        ,'template'=>'view'
        ,'method_name'=>'Near'
        ])
        ->render()
    !!}

    @php $user_cart = $view_model->getModelData('user_cart') @endphp
    <div class="basket {!! $user_cart?'selected':'' !!}">
        <div class="basket-info" style="padding: 5px">
            <span class="count">{!! ($user_cart && $user_cart->cartItems) ? $user_cart->cartItems->count():0 !!}</span>
            <span class="text">@lang("entity::entity.services.service")</span>
            {{--<span class="price">0</span>--}}
        </div>
        <a href="{!! route('sale.cart.booking') !!}"
           class="link-basket">@lang("entity::entity.services.select_time")</a>
    </div>
</div>
<script>
    jQuery(document).ready(function () {

        var $data = {
            title: '{{$view_model->getModelData('title')}}',
            latitude: '{{$view_model->getModelDataRelation('entityAddress.latitude')}}',
            longitude: '{{$view_model->getModelDataRelation('entityAddress.longitude')}}'
        };

        mapboxgl.accessToken = 'pk.eyJ1IjoidGVjaG5pY29ubGluZSIsImEiOiJjanN3b3I4bWowZWt2NDN0MzZ1dWVuZDJnIn0.fdtQtg1NYhPdHC3nu-89Ug';

        @if(get_instance()->getLanguageDirection() == 'rtl')
        mapboxgl.setRTLTextPlugin('https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.2.0/mapbox-gl-rtl-text.js');
        @endif

        var map = new mapboxgl.Map({
            container: 'map_cnv',
            style: 'mapbox://styles/mapbox/outdoors-v10',
            // center: [51.0, 35.0],
            interactive: false,
            zoom: 14
        });

        var bounds = new mapboxgl.LngLatBounds();
        bounds.extend([$data.longitude,$data.latitude]);

        map.addControl(new mapboxgl.NavigationControl());

        new mapboxgl.Marker()
            .setLngLat([$data.longitude,$data.latitude])
            .addTo(map);

        map.fitBounds(bounds,{ zoom: 14 });
        // map.setZoom(14);

{{--        addMarkerSimple({{$view_model->getModelDataRelation('entityAddress.latitude')}},{{$view_model->getModelDataRelation('entityAddress.longitude')}},$data);--}}
    });

</script>