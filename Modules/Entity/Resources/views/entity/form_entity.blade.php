<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("entity::entity.".($view_model->getModelData('id')?'edit':'add'))
                </div>
                <div class="tools">
                    <a href="" class="collapse"> </a>
                </div>
            </div>
            {{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>($view_model->getModelData('id')? route('entity.update', ['entity_id'=>$view_model->getModelData('id')]) : route('entity.save'))
                        ,'method'=>($view_model->getModelData('id')?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("entity::entity.tabs.public") </a>
                                </li>

                                <li>
                                    <a href="#advance" data-toggle="tab"> @lang("entity::entity.tabs.advance") </a>
                                </li>

                                <li>
                                    <a href="#images" data-toggle="tab"> @lang("entity::entity.tabs.images") </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::input('text','title',old('title',$view_model->getModelData('title'))
                                        ,['required'=>'required','title'=>trans("entity::entity.title"),'helper'=>trans("entity::entity.title")]) !!}

                                    {!! FormHelper::selectTag('category_id',[],old('category_id',$view_model->getModelData('category_id')),['data-ajax-url'=>route('entity.category.get_category_by_api')
                                      , 'title'=>trans("entity::entity.category"),'helper'=>trans("entity::entity.select_category")]) !!}

                                    {!! FormHelper::selectTag('categories[]',[],old('categories',$view_model->getActiveCategories()),['required'=>'required','multiple'=>'multiple', 'data-ajax-url'=>route('entity.category.get_category_by_api')
                                         , 'title'=>trans("entity::entity.categories"),'helper'=>trans("entity::entity.select_category")]) !!}

                                    {!! FormHelper::selectTag('owners[]',[],old('owners',$view_model->getActiveOwners()),['required'=>'required','multiple'=>'multiple'
                                        , 'data-ajax-url'=>BridgeHelper::getUser()->getUrlSearchUser()
                                         , 'title'=>trans("entity::entity.owners"),'helper'=>trans("entity::entity.select_owner")]) !!}

                                    {!! FormHelper::textarea('short_description',old('short_description',$view_model->getModelData('short_description'))
                                    ,['title'=>trans("entity::entity.short_description"),'helper'=>trans("entity::entity.short_description")]) !!}

                                </div>

                                <div class="tab-pane" id="advance">
                                    @php
                                        $params = $view_model->getModelData('params_json');
                                    @endphp
                                    {!! FormHelper::input('text','slug',old('slug',$view_model->getModelData('slug', "ENT".get_instance()->getCurrentInstanceId()."-".strtoupper(str_random(10))) )
                                                     ,['title'=>trans("entity::entity.slug"),'helper'=>trans("entity::entity.slug")]) !!}

                                    {!! FormHelper::datetime('active_date',old('active_date',$view_model->getModelData('active_date'))
                                    ,['title'=>trans("entity::entity.active_date"),'helper'=>trans("entity::entity.active_date")
                                        ,'date-year-current'=>1,'date-year-before'=>2,'set-default'=>0]) !!}

                                    {!! FormHelper::datetime('expire_date',old('expire_date',$view_model->getModelData('expire_date'))
                                    ,['title'=>trans("entity::entity.expire_date"),'helper'=>trans("entity::entity.expire_date")
                                        ,'date-year-current'=>2,'date-year-before'=>2,'set-default'=>0]) !!}

                                    {!! FormHelper::select('active',trans("entity::entity.statuses"),old('active',$view_model->getModelData('active',2))
                                    ,['title'=>trans("entity::entity.status"),'helper'=>trans("entity::entity.select_status")
                                    ,'placeholder'=>trans("entity::entity.select_status")]) !!}

                                    {!! FormHelper::editor('description',old('description',$view_model->getModelData('description'))
                                    ,['title'=>trans("entity::entity.description"),'helper'=>trans("entity::entity.description")]) !!}


                                    {{--{!! FormHelper::legend(trans("entity::entity.configs")) !!}--}}

                                    <div class="col-md-6">
                                        {{--{!! FormHelper::select('params[access_comment]',trans("course::course.config_items.access_comments"),old('params[access_comment]',isset($params->access_comment)?$params->access_comment:"only_user")--}}
                                        {{--,['title'=>trans("entity::entity.config_items.access_comment"),'helper'=>trans("entity::entity.config_items.access_comment")--}}
                                        {{--,'placeholder'=>trans("entity::entity.config_items.access_comment")]) !!}--}}
                                    </div>

                                </div>

                                <div class="tab-pane" id="images">

                                    @if($medias = $view_model->getSliderMedias())
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                            <tr>
                                                <th class="hidden-xs">
                                                    <i class="fa fa-image"></i> @lang("entity::entity.image_src")
                                                </th>
                                                <th> @lang("entity::entity.action") </th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($medias as $media)
                                                @if($media->url_thumbnail)
                                                    <tr>
                                                        <td class="hidden-xs">
                                                            <img width="100px" src="{!!  $media->url_thumbnail !!}">
                                                        </td>
                                                        <td>
                                                            <a data-confirm-message="@lang("entity::entity.confirm_delete")"
                                                               class="btn red btn-sm btn-outline sbold uppercase _delete_slider_image"
                                                               href="{!! route('entity.media.delete',['id'=>$media->id]) !!}">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </td>
                                                    </tr>

                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>

                                    @endif

                                    @if($view_model->getModelData('id'))

                                        <div class="portlet box blue ">

                                            <div style="display: none">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-list"></i> @lang("video::video.upload")
                                                    </div>
                                                    {{--<div class="tools">--}}
                                                    {{--<a href="" class="collapse"> </a>--}}
                                                    {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                                                    {{--<a href="" class="reload"> </a>--}}
                                                    {{--<a href="" class="remove"> </a>--}}
                                                    {{--</div>--}}
                                                </div>

                                                <div class="portlet-body form">
                                                    <!-- Instantiating: -->
                                                    <div id="uploader_video">
                                                        {{--<p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>--}}
                                                    </div>

                                                    <script>
                                                        $('#uploader_video').plupload({
                                                            runtimes: 'html5,flash,silverlight,html4',
                                                            url: '{!! route('entity.media.upload_media', ['type'=>'video','model_id'=>$view_model->getModelData('id'), 'model_type'=>'entity']) !!}',
                                                            filters: [
                                                                {
                                                                    title: "Video files",
                                                                    extensions: "mp4,mov,avi,mpeg,mkv,wmv"
                                                                },
                                                            ],
                                                            headers: {
                                                                "Accept": "application/json",
                                                                "X-CSRF-TOKEN": "{!! csrf_token() !!}"
                                                            },
                                                            views: {
                                                                list: false,
                                                                thumbs: true, // Show thumbs
                                                                active: 'thumbs'
                                                            },
                                                            multipart_params: {"is_ajax": true},
                                                            chunk_size: "1000kb",
                                                            file_data_name: "file",
                                                            multipart: true,
                                                            multi_selection: true,
                                                            dragdrop: true,

                                                            // rename: true,
                                                            // sortable: true,
                                                            {{--flash_swf_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.swf") !!}',--}}
                                                                    {{--silverlight_xap_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.xap") !!}',--}}
                                                            init: {
                                                                FilesAdded: function (up, files) {
                                                                    // console.log('FilesAdded');

                                                                    var max_files = 10;
                                                                    plupload.each(files, function (file) {
                                                                        if (up.files.length > max_files) {
                                                                            alert('{!! trans("core::messages.alert.maxFileUpload",['max_upload'=>1]) !!}');
                                                                            up.removeFile(file);
                                                                        }
                                                                    });
                                                                    if (up.files.length >= max_files) {
                                                                        $('#pickfiles').hide('slow');
                                                                    }


                                                                },
                                                                FileUploaded: function (up, file, response) {
                                                                    // console.log('FileUploaded');
                                                                },
                                                                UploadComplete: function (up, files) {
                                                                    // console.log('UploadComplete');
                                                                },
                                                                FilesRemoved: function (up, files) {
                                                                    // console.log('Destroy');
                                                                },
                                                                BeforeUpload: function (up, files) {
                                                                    // console.log('BeforeUpload',files);
                                                                },
                                                                Init: function ($img_object, file, response) {

                                                                }
                                                            }
                                                        });
                                                    </script>
                                                </div>
                                            </div>

                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-list"></i> @lang("video::video.upload_poster")
                                                </div>
                                                {{--<div class="tools">--}}
                                                {{--<a href="" class="collapse"> </a>--}}
                                                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                                                {{--<a href="" class="reload"> </a>--}}
                                                {{--<a href="" class="remove"> </a>--}}
                                                {{--</div>--}}
                                            </div>

                                            <div class="portlet-body form">

                                                <div id="uploader_image">
                                                    {{--<p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>--}}
                                                </div>

                                                <script>

                                                    $('#uploader_image').plupload({
                                                        runtimes: 'html5,flash,silverlight,html4',
                                                        url: '{!! route('entity.media.upload_media', ['type'=>'image','model_id'=>$view_model->getModelData('id'), 'model_type'=>'entity']) !!}',
                                                        filters: [
                                                            {title: "Image files", extensions: "jpg,png"},
                                                        ],
                                                        headers: {
                                                            "Accept": "application/json",
                                                            "X-CSRF-TOKEN": "{!! csrf_token() !!}"
                                                        },
                                                        views: {
                                                            list: false,
                                                            thumbs: true, // Show thumbs
                                                            active: 'thumbs'
                                                        },
                                                        multipart_params: {"is_ajax": true},
                                                        // chunk_size: "500kb",
                                                        file_data_name: "file",
                                                        // multipart: true,
                                                        multi_selection: true,
                                                        dragdrop: true,

                                                        // rename: true,
                                                        // sortable: true,
                                                        {{--flash_swf_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.swf") !!}',--}}
                                                                {{--silverlight_xap_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.xap") !!}',--}}
                                                        init: {
                                                            FilesAdded: function (up, files) {
                                                                // console.log('FilesAdded',files.length);
                                                                var max_files = 10;
                                                                plupload.each(files, function (file) {
                                                                    if (up.files.length > max_files) {
                                                                        alert('{!! trans("core::messages.alert.maxFileUpload",['max_upload'=>1]) !!}');
                                                                        up.removeFile(file);
                                                                    }
                                                                });
                                                                if (up.files.length >= max_files) {
                                                                    $('#pickfiles').hide('slow');
                                                                }

                                                            },
                                                            FileUploaded: function (up, file, response) {
                                                                // console.log('FileUploaded');
                                                            },
                                                            UploadComplete: function (up, files) {
                                                                // console.log('UploadComplete');
                                                            },
                                                            FilesRemoved: function (up, files) {
                                                                // console.log('Destroy');
                                                            },
                                                            BeforeUpload: function (up, files) {
                                                                // console.log('BeforeUpload');
                                                            },
                                                            Init: function ($img_object, file, response) {

                                                            }
                                                        }
                                                    });
                                                    // Invoking methods:
                                                    // $('#uploader').plupload(options);

                                                    // Display welcome message in the notification area
                                                    // $('#uploader_image').plupload('notify', 'info', "This might be obvious, but you need to click 'Add Files' to add some files.");

                                                    // Subscribing to the events...
                                                    // ... on initialization:
                                                </script>
                                            </div>


                                        </div>

                                    @endif

                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("entity::entity.submit")], ['title'=>trans("entity::entity.cancel"), 'url'=>route('entity.index')]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>

        </div>

    </div>

</div>
