<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("entity::entity.".($view_model->getModelData('id')?'edit':'add'))
                </div>
                <div class="tools">
                    <a href="" class="collapse"> </a>
                </div>
            </div>
            {{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>($view_model->getModelData('id')? route('entity.service.update', ['entity_id'=>$view_model->getModelData('id')]) : route('entity.service.save'))
                        ,'method'=>($view_model->getModelData('id')?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("entity::entity.tabs.public") </a>
                                </li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::input('text','title',old('title',$view_model->getModelData('title'))
                                        ,['required'=>'required','title'=>trans("entity::entity.services.title"),'helper'=>trans("entity::entity.services.title")]) !!}

                                    {!! FormHelper::input('text','slug',old('slug',$view_model->getModelData('slug'))
                                        ,['title'=>trans("entity::entity.services.slug"),'helper'=>trans("entity::entity.services.slug")]) !!}

                                    {!! FormHelper::selectTag('parent_id',[],old('parent_id',$view_model->getModelData('parent_id')),['data-ajax-url'=>route('entity.service.get_entity_services_by_api',['_root'=>1])
                                       ,'title'=>trans("entity::entity.services.relation.title_service"),'helper'=>trans("entity::entity.services.relation.title_service")]) !!}

                                    {!! FormHelper::inputByButton('image',old('image',$view_model->getModelData('image_admin')),['type'=>'image','title'=>trans('entity::entity.image'),'helper'=>trans('entity::entity.image')],trans('entity::entity.image'),[],true) !!}

                                    {!! FormHelper::inputByButton('icon',old('icon',$view_model->getModelData('icon_admin')),['type'=>'icon','title'=>trans('entity::entity.services.icon'),'helper'=>trans('entity::entity.services.icon')],trans('entity::entity.services.icon'),[],true) !!}

                                    {!! FormHelper::select('active',trans("entity::entity.statuses"),old('active',$view_model->getModelData('active',2))
                                    ,['title'=>trans("entity::entity.status"),'helper'=>trans("entity::entity.select_status")
                                    ,'placeholder'=>trans("entity::entity.select_status")]) !!}

                                    {!! FormHelper::editor('description',old('description',$view_model->getModelData('description'))
                                    ,['title'=>trans("entity::entity.description"),'helper'=>trans("entity::entity.description")]) !!}


                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancelWithLanguage(['title'=>trans("entity::entity.submit")]
                , ['title'=>trans("entity::entity.cancel"), 'url'=>route('entity.service.index')]
                , ['selected'=>$view_model->getModelData('language_id')?$view_model->getModelData('language_id'):null]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>

        </div>

    </div>

</div>
