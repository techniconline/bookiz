{!! $blockViewModel->getConfig('before_html') !!}
@php $entities =$blockViewModel->getEntitiesList() @endphp
@if($entities->isNotEmpty())

    <div class="mini-slider">
        <h3>{{ $blockViewModel->getConfig('title') }}</h3>
        <div class="owl-carousel owl-5">

            @foreach($entities as $entity)
                <a href="{{ route('entity.view',['id'=>$entity['id'],'slug'=>$entity['slug']]) }}" title="{{ $entity['short_description'] }}">
                    <div class="item" data-title="{{ $entity['short_description'] }}">
                        <img class="owl-lazy" data-src="{!! $entity['media_image']?$entity['media_image']['url_thumbnail']:null !!}" />
                        <h3>{{ $entity['title'] }}</h3>
                        {{--<p class="distance">0.3 mi away</p>--}}
                        <div class="rate">

                            <div class="stars">

                                @for($i = 1; 5>=$i; $i++)
                                    <i class="{!! (($entity->rate['data_rate'])>=$i) || (($entity->rate['data_rate'])>4.8)?'fas':'far'  !!} fa-star"></i>
                                @endfor

                            </div>

                            <div class="num">{!! $entity->rate['data_rate'] !!}</div>
                        </div>
                    </div>
                </a>
            @endforeach

        </div>
    </div>

@endif
{!! $blockViewModel->getConfig('after_html') !!}