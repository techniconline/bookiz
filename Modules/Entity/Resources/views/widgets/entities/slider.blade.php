{!! $blockViewModel->getConfig('before_html') !!}

<div class="services-slider" style="background-image: url({{ $blockViewModel->getConfig('back_image') }}) !important;">
    <h1>
        {{ $blockViewModel->getConfig('title') }}
    </h1>
    <div class="owl-carousel owl-5">
        @foreach($blockViewModel->getEntitiesList() as $entity)
        <div class="service">
            <a href="{{ route('entity.view',['id'=>$entity->id, 'slug'=>$entity->slug]) }}">
            <img class="owl-lazy" data-src="{!! $entity->media_image->url_thumbnail !!}" />
            <div class="details">
                <h1>{{ $entity->title }}</h1>
                <p>
                    {{ $entity->short_decription }}
                </p>
            </div>
            </a>
        </div>
        @endforeach
    </div>
</div>

{!! $blockViewModel->getConfig('after_html') !!}