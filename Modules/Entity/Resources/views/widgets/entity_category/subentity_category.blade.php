<li class="{{ $blockViewModel->getConfig($pre.'class_li') }}">
    <a class="{{ $blockViewModel->getConfig($pre.'class_link') }}"
       href="{{ $blockViewModel->getEntityCategoryUrl($course_category) }}">
        {{ $entity_category["title"] }}
    </a>
    @if(isset($entity_category["childes"]) && $entity_category["childes"]->count())
        <ul class="submenu">
            @foreach($entity_category["childes"] as $entity_category)
                @include('entity::widgets.entity_category.subentity_category',['pre'=>$pre])
            @endforeach
        </ul>
    @endif
</li>