{!! FormHelper::input('text','id',old('id',$blockViewModel->getData('id')),['title'=>trans('entity::form.fields.id'),'helper'=>trans('entity::form.helper.id')]) !!}
{!! FormHelper::input('text','class_div',old('class_ul',$blockViewModel->getData('class_div')),['title'=>trans('entity::form.fields.class_div'),'helper'=>trans('entity::form.helper.class_div')]) !!}
{!! FormHelper::input('text','class_ul',old('class_ul',$blockViewModel->getData('class_ul')),['title'=>trans('entity::form.fields.class_ul'),'helper'=>trans('entity::form.helper.class_ul')]) !!}
{!! FormHelper::input('text','class_li',old('class_li',$blockViewModel->getData('class_li')),['title'=>trans('entity::form.fields.class_li'),'helper'=>trans('entity::form.helper.class_li')]) !!}
{!! FormHelper::input('text','class_link',old('class_link',$blockViewModel->getData('class_link')),['title'=>trans('entity::form.fields.class_link'),'helper'=>trans('entity::form.helper.class_link')]) !!}
