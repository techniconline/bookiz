{!! $blockViewModel->getConfig('before_html') !!}
<div class="{{ $blockViewModel->getConfig('class_div') }}" id="{{ $blockViewModel->getConfig('id') }}">
    <ul class="{{ $blockViewModel->getConfig('class_ul') }}">
	 
        @foreach($blockViewModel->getEntityCategories() as $entity_category)
            @include('entity::widgets.entity_category.subentity_category',['pre'=>''])
        @endforeach
    </ul>
    @if($blockViewModel->getConfig('mobile_entity_category'))
    <button data-toggle="collapse" data-target="#{{$blockViewModel->getConfig('id')}}" class="hidden-lg hidden-md TowerCollMenu">
        {{ $blockViewModel->getConfig('mobile_title') }}<i class="fas fa-sort-down"> </i>
    </button>
    <div id="top-menu" class="{{ $blockViewModel->getConfig('mobile_class_div') }}">
        <ul class="{{ $blockViewModel->getConfig('mobile_class_ul') }}">
            @foreach($blockViewModel->getEntityCategories() as $entity_category)
                @include('entity::widgets.entity_category.subentity_category',['pre'=>'mobile_'])
            @endforeach
			
        </ul>
    </div>
    @endif
</div>
{!! $blockViewModel->getConfig('after_html') !!}