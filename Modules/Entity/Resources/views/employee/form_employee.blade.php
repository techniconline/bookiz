<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("entity::entity.".($view_model->getModelData('id')?'edit':'add'))
                </div>
                <div class="tools">
                    <a href="" class="collapse"> </a>
                </div>
            </div>
            {{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>($view_model->getModelData('id')? route('entity.employee.update', ['entity_employee_id'=>$view_model->getModelData('id')]) : route('entity.employee.save'))
                        ,'method'=>($view_model->getModelData('id')?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">

                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("entity::entity.tabs.public") </a>
                                </li>

                                <li>
                                    <a href="#services" data-toggle="tab">  @lang("entity::entity.tabs.services") </a>
                                </li>

                                <li>
                                    <a href="#info_employee"
                                       data-toggle="tab">  @lang("entity::entity.tabs.info_employee") </a>
                                </li>

                                <li>
                                    <a href="#info_experience_employee"
                                       data-toggle="tab">  @lang("entity::entity.tabs.info_experience_employee") </a>
                                </li>

                            </ul>
                            <div class="tab-content">

                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::selectTag('entity_id',[],old('entity_id',$view_model->getModelData('entity_id')?$view_model->getModelData('entity_id'):$view_model->request->get("entity_id")),['data-ajax-url'=>route('entity.get_entity_by_api')
                                                           ,'title'=>trans("entity::entity.employee.title_entity"),'helper'=>trans("entity::entity.employee.title_entity")]) !!}

                                    {!! FormHelper::selectTag('user_id',[],old('user_id',$view_model->getModelData('user_id')?$view_model->getModelData('user_id'):null)
                                    ,['data-ajax-url'=>BridgeHelper::getUser()->getUrlSearchUser()
                                       ,'title'=>trans("entity::entity.employee.full_name"),'helper'=>trans("entity::entity.employee.full_name")]) !!}

                                    {!! FormHelper::select('active',trans("entity::entity.statuses"),old('active',$view_model->getModelData('active',2))
                                    ,['title'=>trans("entity::entity.status"),'helper'=>trans("entity::entity.select_status") ,'placeholder'=>trans("entity::entity.select_status")]) !!}

                                </div>

                                <div class="tab-pane disabled" id="services">

                                    @if($view_model->getModelData('id'))

                                        @php $employeeServices = $view_model->getEmployeeServices(); @endphp

                                        @if($employeeServices)
                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                <thead>
                                                <tr>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-toolbox"></i> @lang("entity::entity.services.title")
                                                    </th>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-genderless"></i> @lang("entity::entity.employee.for_gender")
                                                    </th>
                                                    <th> @lang("entity::entity.action") </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($employeeServices as $item)

                                                    <tr>
                                                        <td class="hidden-xs">
                                                            {!!  $item->entityRelationService->title !!}
                                                        </td>
                                                        <td class="hidden-xs">
                                                            {!!  trans("booking::calendar.genders.".$item->for_gender) !!}
                                                        </td>
                                                        <td>
                                                            <a data-confirm-message="@lang("entity::entity.confirm_delete")"
                                                               class="btn red btn-sm btn-outline sbold uppercase _delete"
                                                               href="{!! route('entity.employee.service.delete',['id'=>$item->id]) !!}">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </td>
                                                    </tr>

                                                @endforeach
                                                </tbody>
                                            </table>
                                        @endif

                                        {!! FormHelper::legend(trans("entity::entity.employee.new_service")) !!}

                                        {!! FormHelper::selectTag('entity_relation_service_ids[]',$view_model->getEntityRelationService(),old('entity_relation_service_ids[]',null)
                                               ,['multiple'=>'multiple' ,'title'=>trans("entity::entity.employee.select_service"),'helper'=>trans("entity::entity.employee.select_service")]) !!}

                                        {!! FormHelper::select('for_gender',trans("booking::calendar.genders"),old('for_gender',$view_model->getModelData('for_gender','female'))
                                        ,['title'=>trans("booking::calendar.gender"),'helper'=>trans("booking::calendar.gender") ,'placeholder'=>trans("booking::calendar.gender")]) !!}

                                    @else
                                        <h1 style="color: red">@lang("entity::entity.after_create_model")</h1>
                                    @endif
                                </div>

                                <div class="tab-pane" id="info_employee">

                                    @if($view_model->getModelData('id'))

                                        <div class="col-md-4">
                                            {!! FormHelper::input('text','info_employee[nationality]',old('info_employee[nationality]',$view_model->getEmployeeInfo('nationality'))
                                            ,['title'=>trans("entity::entity.employee.nationality"),'helper'=>trans("entity::entity.employee.nationality")]) !!}
                                        </div>
                                        <div class="col-md-4">
                                            {!! FormHelper::input('text','info_employee[religion]',old('info_employee[religion]',$view_model->getEmployeeInfo('religion'))
                                             ,['title'=>trans("entity::entity.employee.religion"),'helper'=>trans("entity::entity.employee.religion")]) !!}
                                        </div>

                                        <div class="col-md-4">
                                            {!! FormHelper::input('text','info_employee[blood_type]',old('info_employee[blood_type]',$view_model->getEmployeeInfo('blood_type'))
                                            ,['title'=>trans("entity::entity.employee.blood_type"),'helper'=>trans("entity::entity.employee.blood_type")]) !!}
                                        </div>

                                        <div class="col-md-6">

                                            {!! FormHelper::select('info_employee[married]',trans("entity::entity.employee.married_statues"),old('info_employee[married]',$view_model->getEmployeeInfo('married'))
                                            ,['title'=>trans("entity::entity.employee.married"),'helper'=>trans("entity::entity.employee.married") ,'placeholder'=>trans("entity::entity.employee.married")]) !!}

                                        </div>

                                        <div class="col-md-6">
                                            {!! FormHelper::input('number','info_employee[count_childes]',old('info_employee[count_childes]',$view_model->getEmployeeInfo('count_childes'))
                                            ,['title'=>trans("entity::entity.employee.count_childes"),'helper'=>trans("entity::entity.employee.count_childes")]) !!}
                                        </div>

                                        <div class="col-md-6">
                                            {!! FormHelper::date('info_employee[passport_start_date]',old('info_employee[passport_start_date]',$view_model->getEmployeeInfo('passport_start_date'))
                                               ,['title'=>trans("entity::entity.employee.passport_start_date"),'helper'=>trans("entity::entity.employee.passport_start_date")
                                               ,'date-year-current'=>1,'date-year-before'=>10,'set-default'=>0]) !!}
                                        </div>
                                        <div class="col-md-6">
                                            {!! FormHelper::date('info_employee[passport_end_date]',old('info_employee[passport_end_date]',$view_model->getEmployeeInfo('passport_end_date'))
                                              ,['title'=>trans("entity::entity.employee.passport_end_date"),'helper'=>trans("entity::entity.employee.passport_end_date")
                                              ,'date-year-current'=>10,'date-year-before'=>10,'set-default'=>0]) !!}
                                        </div>

                                        <div class="col-md-6">

                                            {!! FormHelper::date('info_employee[stay_start_date]',old('info_employee[stay_start_date]',$view_model->getEmployeeInfo('stay_start_date'))
                                            ,['title'=>trans("entity::entity.employee.stay_start_date"),'helper'=>trans("entity::entity.employee.stay_start_date")
                                               ,'date-year-current'=>1,'date-year-before'=>10,'set-default'=>0]) !!}

                                        </div>
                                        <div class="col-md-6">

                                            {!! FormHelper::date('info_employee[stay_end_date]',old('info_employee[stay_end_date]',$view_model->getEmployeeInfo('stay_end_date'))
                                            ,['title'=>trans("entity::entity.employee.stay_end_date"),'helper'=>trans("entity::entity.employee.stay_end_date")
                                              ,'date-year-current'=>10,'date-year-before'=>10,'set-default'=>0]) !!}

                                        </div>
                                        <div class="col-md-6">

                                            {!! FormHelper::date('info_employee[driving_license_start_date]',old('info_employee[driving_license_start_date]',$view_model->getEmployeeInfo('driving_license_start_date'))
                                            ,['title'=>trans("entity::entity.employee.driving_license_start_date"),'helper'=>trans("entity::entity.employee.driving_license_start_date")
                                               ,'date-year-current'=>1,'date-year-before'=>10,'set-default'=>0]) !!}

                                        </div>
                                        <div class="col-md-6">

                                            {!! FormHelper::date('info_employee[driving_license_end_date]',old('info_employee[driving_license_end_date]',$view_model->getEmployeeInfo('driving_license_end_date'))
                                            ,['title'=>trans("entity::entity.employee.driving_license_end_date"),'helper'=>trans("entity::entity.employee.driving_license_end_date")
                                              ,'date-year-current'=>10,'date-year-before'=>10,'set-default'=>0]) !!}

                                        </div>

                                        <div class="col-md-6">

                                            {!! FormHelper::date('info_employee[certificate_start_date]',old('info_employee[certificate_start_date]',$view_model->getEmployeeInfo('certificate_start_date'))
                                            ,['title'=>trans("entity::entity.employee.certificate_start_date"),'helper'=>trans("entity::entity.employee.certificate_start_date")
                                               ,'date-year-current'=>1,'date-year-before'=>10,'set-default'=>0]) !!}

                                        </div>
                                        <div class="col-md-6">

                                            {!! FormHelper::date('info_employee[certificate_end_date]',old('info_employee[certificate_end_date]',$view_model->getEmployeeInfo('certificate_end_date'))
                                            ,['title'=>trans("entity::entity.employee.certificate_end_date"),'helper'=>trans("entity::entity.employee.certificate_end_date")
                                              ,'date-year-current'=>10,'date-year-before'=>10,'set-default'=>0]) !!}

                                        </div>

                                        <div class="col-md-6">

                                            {!! FormHelper::input('number','info_employee[weight]',old('info_employee[weight]',$view_model->getEmployeeInfo('weight'))
                                            ,['title'=>trans("entity::entity.employee.weight"),'helper'=>trans("entity::entity.employee.weight")]) !!}

                                        </div>

                                        <div class="col-md-6">

                                            {!! FormHelper::input('number','info_employee[length]',old('info_employee[length]',$view_model->getEmployeeInfo('length'))
                                            ,['title'=>trans("entity::entity.employee.length"),'helper'=>trans("entity::entity.employee.length")]) !!}

                                        </div>

                                        <div class="col-md-6">

                                            {!! FormHelper::textarea('info_employee[history_disease]',old('info_employee[history_disease]',$view_model->getEmployeeInfo('history_disease'))
                                            ,['title'=>trans("entity::entity.employee.history_disease"),'helper'=>trans("entity::entity.employee.history_disease")]) !!}

                                        </div>

                                        <div class="col-md-6">

                                            {!! FormHelper::textarea('info_employee[drug_history]',old('info_employee[drug_history]',$view_model->getEmployeeInfo('drug_history'))
                                            ,['title'=>trans("entity::entity.employee.drug_history"),'helper'=>trans("entity::entity.employee.drug_history")]) !!}

                                        </div>

                                    @else
                                        <h1 style="color: red">@lang("entity::entity.after_create_model")</h1>
                                    @endif
                                </div>

                                <div class="tab-pane" id="info_experience_employee">

                                    @if($view_model->getModelData('id'))

                                        @php $employeeExperienceInfo = $view_model->getEmployeeExperienceInfo(); @endphp

                                        @if($employeeExperienceInfo)
                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                <thead>
                                                <tr>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-certificate"></i> @lang("entity::entity.employee.experience")
                                                    </th>
                                                    <th class="hidden-xs">
                                                        <i class="fa hourglass"></i> @lang("entity::entity.employee.work_experience")
                                                    </th>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-calendar"></i> @lang("entity::entity.employee.certificate_start_date")
                                                    </th>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-calendar"></i> @lang("entity::entity.employee.certificate_end_date")
                                                    </th>
                                                    <th> @lang("entity::entity.action") </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($employeeExperienceInfo as $item)

                                                    <tr>
                                                        <td class="hidden-xs">
                                                            {!!  $item->experience !!}
                                                        </td>
                                                        <td class="hidden-xs">
                                                            {!!  $item->work_experience !!}
                                                        </td>
                                                        <td class="hidden-xs">
                                                            {!!  $item->certificate_start_date !!}
                                                        </td>
                                                        <td class="hidden-xs">
                                                            {!!  $item->certificate_end_date !!}
                                                        </td>
                                                        <td>
                                                            <a data-confirm-message="@lang("entity::entity.confirm_delete")"
                                                               class="btn red btn-sm btn-outline sbold uppercase _delete"
                                                               href="{!! route('entity.employee.experience.delete',['id'=>$item->id]) !!}">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </td>
                                                    </tr>

                                                @endforeach
                                                </tbody>
                                            </table>
                                        @endif

                                        {!! FormHelper::legend(trans("entity::entity.employee.new_experience")) !!}

                                        @for($i=0; $i<5;$i++)

                                            <div class="col-lg-12 _source_experience"
                                                 style="border-bottom: 1px solid gray">

                                                <div class="col-md-6">
                                                    {!! FormHelper::input('text','info_experience_employee['.$i.'][experience]',old('info_experience_employee['.$i.'][experience]',null)
                                                    ,['title'=>trans("entity::entity.employee.experience"),'helper'=>trans("entity::entity.employee.experience")]) !!}
                                                </div>

                                                <div class="col-md-6">
                                                    {!! FormHelper::input('number','info_experience_employee['.$i.'][work_experience]',old('info_experience_employee['.$i.'][work_experience]',null)
                                                    ,['title'=>trans("entity::entity.employee.work_experience"),'helper'=>trans("entity::entity.employee.work_experience")]) !!}
                                                </div>

                                                <div class="col-md-6">
                                                    {!! FormHelper::date('info_experience_employee['.$i.'][certificate_start_date]',old('info_experience_employee['.$i.'][certificate_start_date]',null)
                                                       ,['title'=>trans("entity::entity.employee.certificate_start_date"),'helper'=>trans("entity::entity.employee.certificate_start_date")
                                                       ,'date-year-current'=>1,'date-year-before'=>10,'set-default'=>0]) !!}
                                                </div>

                                                <div class="col-md-6">
                                                    {!! FormHelper::date('info_experience_employee['.$i.'][certificate_end_date]',old('info_experience_employee['.$i.'][certificate_end_date]',null)
                                                      ,['title'=>trans("entity::entity.employee.certificate_end_date"),'helper'=>trans("entity::entity.employee.certificate_end_date")
                                                      ,'date-year-current'=>10,'date-year-before'=>10,'set-default'=>0]) !!}
                                                </div>

                                            </div>

                                        @endfor

                                    @else
                                        <h1 style="color: red">@lang("entity::entity.after_create_model")</h1>
                                    @endif

                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("entity::entity.submit")]
                , ['title'=>trans("entity::entity.cancel"), 'url'=>route('entity.employee.index',['entity_id'=>$view_model->getModelData('entity_id')?$view_model->getModelData('entity_id'):$view_model->request->get("entity_id")])]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>

        </div>

    </div>

</div>
