<?php

return [

    'course' => [
        'title_required'=>'نام را مشخص کنید!',
        'instance_id_required'=>'سامانه را مشخص کنید!',
        'language_id_required'=>'زبان نامشخص است!',
        'category_id_required'=>'دسته بندی نامشخص است!',
        'root_id_required'=>'سرشاخه نامشخص است!',
        'type_required'=>'نوع نامشخص است!',
        'show_type_required'=>'نوع نامشخص است!',
        'course_id_required'=>'درس نامشخص است!',
        'ca_id_required'=>'فعالیت درس نامشخص است!',
        'item_id_required'=>'موضوع مورد اتصال نامشخص است!',
    ],

];