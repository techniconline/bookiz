<?php

return [
    'group' => [
        'entities' => 'مدیریت اماکن',
    ],
    'config' => [
        'instance' => 'تنظیمات اماکن',
    ],
    'entity_list' => 'لیست اماکن',
    'entity_category_list' => 'لیست شاخه ها',
    'entity_service_list' => 'لیست سرویس ها',

];