<?php

return [
    'titles' => [
        'defaults' => 'پیش فرض های اماکن',
    ],
    'fields' => [
        'default_poster' => 'پوستر پیش فرض اماکن',
        'message' => 'متن مورد نظر شما',
        'entity_category' => 'دسته بندی',
        'entity_category_deep' => 'عمق دسته بندی',
        'mobile_entity_category' => 'دسته بندی موبایلی',
        'category' => 'دسته بندی',
        'show_title' => 'نمایش عنوان',
        'class_div' => 'کلاس پدر',
        'class_ul' => 'کلاس UL',
        'class_li' => 'کلاس LI',
        'class_link' => 'کلاس لینک',
        'files_html' => 'فایل های مورد نیاز js , css',
        'templates' => [
            'slider'=>'اسلایدری',
            'list_box'=>'باکس - لیستی',
        ],
        'id' => 'شناسه',
        'title' => 'عنوان',
        'template' => 'قالب',
        'logo_image' => 'لوگو',
        'back_image' => 'عکس بک گراند',
        'blog_count_show' => 'تعداد نمایش',
        'sort_by' => 'مرتب سازی',
        'show_color' => 'نمایش آیکون',
        'show_icon' => 'استفاده از رنگ تنظیمات',
        'show_type_list' => 'نوع نمایش لیست',
        'take_number' => 'تعداد نمایش',
        'is_random' => 'انتخاب تصادفی',
        'list_types' => [
            'box_list'=>'به صورت جعبه ی',
            'normal'=>'عادی',
        ],

    ],
    'helper' => [
        'category' => 'دسته بندی',
        'show_title' => '',
        'mobile_entity_category' => '',
        'class_div' => '',
        'class_ul' => '',
        'class_li' => '',
        'class_link' => '',
        'entity_category_deep' => 'عمق منو',
        'show_type_list' => 'نوع نمایش لیست',

    ],
];