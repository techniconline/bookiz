<?php

return [
    'module' => 'اماکن',
    'entities_block'=>'بلاک اماکن',
    'advanced_block'=>'پیشرفته',
    'advanced'=>'پیشرفته',
    'mobile'=>'موبایل',
    'entity_category_block'=>'بلاک دسته بندی اماکن',
    'entity_category'=>'دسته بندی اماکن',
    ];