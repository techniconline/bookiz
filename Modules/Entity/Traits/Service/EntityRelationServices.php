<?php

namespace Modules\Entity\Traits\Service;

use Illuminate\Support\Facades\DB;
use Modules\Booking\Models\Booking\BookingDetails;
use Modules\Booking\Models\Calendar\BookingCalendar;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityServices;

trait EntityRelationServices
{

    /**
     * @param $query
     * @param $user_id
     * @param $entity_id
     * @return mixed
     */
    public function scopeIsOwner($query, $user_id, $entity_id)
    {
        return $query->where('entity_id', DB::raw('(select entity_id from entity_owners where user_id='.$user_id.' and entity_id='.$entity_id.' and deleted_at is null and active=1)'));
    }

    public function validityData($model_id)
    {
        $entityRelationService = \Modules\Entity\Models\Service\EntityRelationServices::enable()->find($model_id);
        if (!$entityRelationService) {
            return ['action' => false, 'message' => trans("booking::messages.alert.not_find_data")];
        }

        $entity = Entity::enable()->isOwner()->find($entityRelationService->entity_id);
        if (!$entity) {
            return ['action' => false, 'message' => trans("booking::messages.alert.access_error")];
        }
        return ['action' => true];

    }

    /**
     * @return null
     */
    public function getPeriodByFormatAttribute()
    {
        $value = isset($this->period) ? $this->period : null;
        if ($value) {
            return $this->convertTime($value);
        }
        return $value;
    }

    /**
     * @param $minutes
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function convertTime($minutes)
    {
        $hr = (int)($minutes / 60);
        $mins = fmod($minutes, 60);
        if ($hr && $mins) {
            $text = trans("entity::entity.services.text_time_full", ["hr" => $hr, "mins" => $mins]);
        } elseif ($hr) {
            $text = trans("entity::entity.services.text_time_hr", ["hr" => $hr]);
        } else {
            $text = trans("entity::entity.services.text_time_mins", ["mins" => $mins]);
        }
        return $text;
    }

    /**
     * @param $value
     * @return mixed|string
     */
    public function getImageAttribute($value)
    {
        if ($value) {
            return get_instance()->getCurrentUrl(asset($value));
        }
        return get_instance()->getCurrentUrl(asset("assets/modules/entity/images/services/default-service-image.jpg"));
    }

    /**
     * @param $value
     * @return mixed|string
     */
    public function getIconAttribute($value)
    {
        if ($value) {
            return get_instance()->getCurrentUrl(asset($value));
        }
        return get_instance()->getCurrentUrl(asset("assets/modules/entity/images/services/icons/default-service-icon.png"));
    }

    /**
     * @return mixed
     */
    public function getIconAdminAttribute()
    {
        return $this->attributes["icon"];
    }

    /**
     * @return mixed
     */
    public function getImageAdminAttribute()
    {
        return $this->attributes["image"];
    }

    /**
     * @return null
     */
    public function getAmountTextAttribute()
    {
        $currency_id = $this->currency_id ?: BridgeHelper::getCurrencyHelper()->getCurrentCurrency()->id;
        if ($this->price && $currency_id) {
            $value = $this->price - $this->discount;
            return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($value, $currency_id, true);
        }
        return null;
    }

    /**
     * @return null
     */
    public function getAmountAttribute()
    {
        $currency_id = $this->currency_id ?: BridgeHelper::getCurrencyHelper()->getCurrentCurrency()->id;
        $value = $this->price - $this->discount;
        if ($value && $currency_id) {
            return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($value, $currency_id, false);
        }
        return $value;
    }

    /**
     * @return null
     */
    public function getDiscountTextAttribute()
    {
        $currency_id = $this->currency_id ?: BridgeHelper::getCurrencyHelper()->getCurrentCurrency()->id;
        if ($this->discount && $currency_id) {
            return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->discount, $currency_id, true);
        }
        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityService()
    {
        return $this->belongsTo(EntityServices::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookingCalendars()
    {
        return $this->hasMany(BookingCalendar::class, 'model_id')->where("model_type", $this->getTable());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bookingCalendar()
    {
        return $this->hasOne(BookingCalendar::class, 'model_id')->where("model_type", $this->getTable())->filterGender()->active()->orderBy("id", "DESC");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookingDetails()
    {
        return $this->hasMany(BookingDetails::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityMedias()
    {
        return $this->hasMany(Entity\EntityMedias::class, 'entity_relation_service_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityRelationServicePrices()
    {
        return $this->hasMany(\Modules\Entity\Models\Service\EntityRelationServicePrices::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityEmployeeServices()
    {
        return $this->hasMany(Entity\EntityEmployeeServices::class, 'entity_relation_service_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityProductServices()
    {
        return $this->hasMany(Entity\EntityProductService::class);
    }

}
