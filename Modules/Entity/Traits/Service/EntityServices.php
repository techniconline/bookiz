<?php

namespace Modules\Entity\Traits\Service;

use Illuminate\Support\Facades\DB;
use Modules\Booking\Models\Booking\BookingDetails;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;

trait EntityServices
{
    private $is_default = false;


    /**
     * @return mixed|null
     */
    public function getParamsJsonAttribute()
    {
        if ($params = $this->params) {
            return json_decode($params);
        }
        return null;
    }

    /**
     * @return mixed|null
     */
    public function getParamsArrayAttribute()
    {
        if ($params = $this->params) {
            return json_decode($params, true);
        }
        return null;
    }

    /**
     * @param $value
     * @return mixed|string
     */
    public function getImageAttribute($value)
    {
        if ($value){
            return get_instance()->getCurrentUrl(asset($value));
        }
        return get_instance()->getCurrentUrl(asset("assets/modules/entity/images/services/default-service-image.jpg"));
    }

    /**
     * @return mixed
     */
    public function getImageAdminAttribute()
    {
        return $this->attributes["image"];
    }

    /**
     * @return mixed
     */
    public function getIconAdminAttribute()
    {
        return $this->attributes["icon"];
    }

    /**
     * @param $value
     * @return mixed|string
     */
    public function getIconAttribute($value)
    {
        if ($value){
            return get_instance()->getCurrentUrl(asset($value));
        }
        return get_instance()->getCurrentUrl(asset("assets/modules/entity/images/services/icons/default-service-icon.png"));
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getIconActiveAttribute()
    {
        if ($src = $this->icon){
            $file_name = pathinfo($src, PATHINFO_FILENAME);
            $file_ext = pathinfo($src, PATHINFO_EXTENSION);
            $file_path = pathinfo($src, PATHINFO_DIRNAME);
            $path = $file_path.DIRECTORY_SEPARATOR.$file_name.'-active.'.$file_ext;
            return get_instance()->getCurrentUrl($path);
        }
        return get_instance()->getCurrentUrl(asset("assets/modules/entity/images/services/icons/default-service-icon-active.png"));
    }

    /**
     * @return mixed
     */
    public function getEntityCountAttribute()
    {
        $count = \Modules\Entity\Models\Service\EntityRelationServices::active()->where("entity_service_id", $this->id)->count(DB::raw("DISTINCT entity_id"));
        return $count;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo(Instance::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    /**
     * @return mixed
     */
    public function parent()
    {
        return $this->belongsTo(\Modules\Entity\Models\Service\EntityServices::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childes()
    {
        return $this->hasMany(\Modules\Entity\Models\Service\EntityServices::class, "parent_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookingDetails()
    {
        return $this->hasMany(BookingDetails::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityRelationServices()
    {
        return $this->hasMany(\Modules\Entity\Models\Service\EntityRelationServices::class,"entity_service_id");
    }

}
