<?php

namespace Modules\Entity\Traits\Service;

use Modules\Core\Models\Location\Location;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;

trait EntityRelationServicePrices
{


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityRelationService()
    {
        return $this->belongsTo(EntityRelationServices::class);
    }

}
