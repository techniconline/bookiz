<?php

namespace Modules\Entity\Traits\Category;

use Modules\Entity\Models\Entity;

trait EntityCategoryRelation
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityCategory()
    {
        return $this->belongsTo(\Modules\Entity\Models\Category\EntityCategory::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }
}
