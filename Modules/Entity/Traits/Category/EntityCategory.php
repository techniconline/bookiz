<?php

namespace Modules\Entity\Traits\Category;


use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;
use Modules\Entity\Models\Entity;
use Illuminate\Database\Eloquent\Collection;

trait EntityCategory
{


    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'title.required' => trans('course::errors.course.title_required'),
            'language_id.required' => trans('course::errors.course.language_id_required'),
            'instance_id.required' => trans('course::errors.course.instance_id_required'),
            'root_id.required' => trans('course::errors.course.root_id_required'),

        ];
        return $this;
    }

    /**
     * @param $value
     * @return array|mixed
     */
    public function getAttributesAttribute($value)
    {
        if ($value) {
            $items = json_decode($value);
            $items = collect($items)->map(function ($value, $key){
                if(strpos($key, "icon")!==false){
                    return $value?:"fa fa-graduation-cap";
                }elseif(strpos($key, "color")!==false){
                    if(strpos($key, "font")!==false){
                        return $value?:"#ffffff";
                    }elseif(strpos($key, "background")!==false){
                        return $value?:"#d9585c";
                    }
                    return $value?:"#dddddd";

                }
                return $value?:"fa fa-list";
            });
            return $items;
        } else {
            return $this->getAttributesDefault();
        }
    }

    /**
     * @return array
     */
    private function getAttributesDefault()
    {
        return [
            "btn_icon" => "fas fa-caret-left",
            "btn_font_color" => "#FFFFFF",
            "btn_background_color" => "#FF3333",
        ];
    }

    /**
     * @return mixed
     */
    public function getEntitiesCountAttribute()
    {
        $cats = $this->getChildesId($this->id);
        $count = \Modules\Entity\Models\Category\EntityCategoryRelation::whereIn("category_id",$cats )->with("entity")->filterRelations(["entity"])->count("course_id");
        return $count;
    }

    /**
     * @return int
     */
    public function getChildesCountAttribute()
    {
        return $this->categoryChildes()->count();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeGetRootCategory($query)
    {
        return $query->whereNull("parent_id");
    }

    /**
     * @return string
     */
    public function getUrlGetChildesAttribute()
    {
        return route("entity.category.api.get_childes_categories", ["parent_id" => $this->id]);
    }

    /**
     * @return string
     */
    public function getUrlEntitiesAttribute()
    {
        return route("entity.list_by_category", ["id" => $this->id]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo(Instance::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseCategory()
    {
        return $this->belongsTo(\Modules\Entity\Models\Category\EntityCategory::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entities()
    {
        return $this->hasMany(Entity::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function listEntities()
    {
        return $this->belongsToMany(\Modules\Entity\Models\Category\EntityCategory::class, 'entity_category_relations', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentEntityCategory()
    {
        return $this->belongsTo(\Modules\Entity\Models\Category\EntityCategory::class, 'parent_id');
    }

    /**
     * @param Collection $data
     * @return \Illuminate\Support\Collection|static
     */
    public function getParentsIds($data)
    {
        return $data->map(function ($category) {
            $cat[] = $category->id;
            if ($category->parentRecursive) {
                $cat[] = $this->getParentsIds(collect([$category->parentRecursive]));
            }
            return $cat;
        })->flatten();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentRecursive()
    {
        return $this->parentEntityCategory()->with('parentRecursive');
    }

    /**
     * @param $category_id
     * @return null
     */
    public function getRootParent($category_id)
    {
        $results = $this->where("id", $category_id)->with("parentRecursive")->first();
        return $this->getParent($results);
    }

    /**
     * @param $parent
     * @return null
     */
    private function getParent($parent)
    {
        if ($parent->parent_id === null){
            return $parent;
        }else{
            return $this->getParent($parent->parentRecursive);
        }
        return null;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoryChildes()
    {
        return $this->hasMany(\Modules\Entity\Models\Category\EntityCategory::class, 'parent_id', 'id')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childrenRecursive()
    {
        return $this->categoryChildes()->with('childrenRecursive');
    }

    /**
     * @param $category_id
     * @return mixed
     */
    public function getChildesId($category_id)
    {
        $result = $this->where("id", $category_id)->with("childrenRecursive")->get();
        $result = $this->getIds($result);
        return $result;
    }

    /**
     * @param Collection $data
     * @return \Illuminate\Support\Collection|static
     */
    private function getIds(Collection $data)
    {
        return $data->map(function ($category) {
            $cat[] = $category->id;
            if ($category->childrenRecursive) {
                $cat[] = $this->getIds($category->childrenRecursive);
            }
            return $cat;
        })->flatten();
    }

}
