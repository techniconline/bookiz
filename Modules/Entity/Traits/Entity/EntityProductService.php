<?php

namespace Modules\Entity\Traits\Entity;

use Modules\Entity\Models\Service\EntityRelationServices;

trait EntityProductService
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityProduct()
    {
        return $this->belongsTo(\Modules\Entity\Models\Entity\EntityProduct::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityRelationService()
    {
        return $this->belongsTo(EntityRelationServices::class);
    }
}
