<?php

namespace Modules\Entity\Traits\Entity;

use Modules\Entity\Models\Entity;

trait EntityProduct
{

    /**
     * @return mixed|null
     */
    public function getParamsJsonAttribute()
    {
        if($this->params){
            return json_decode($this->params);
        }
        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityProductServices()
    {
        return $this->hasMany(Entity\EntityProductService::class);
    }
}
