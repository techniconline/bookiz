<?php

namespace Modules\Entity\Traits\Entity;

use Modules\Core\Models\Location\Location;
use Modules\Entity\Models\Entity;
use Modules\User\Models\User;

trait EntityOwner
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
