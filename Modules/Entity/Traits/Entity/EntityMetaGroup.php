<?php

namespace Modules\Entity\Traits\Entity;

use Modules\Core\Models\Meta\MetaGroup;
use Modules\Entity\Models\Entity;

trait EntityMetaGroup
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function metaGroup()
    {
        return $this->belongsTo(MetaGroup::class);
    }

}
