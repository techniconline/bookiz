<?php

namespace Modules\Entity\Traits\Entity;

use Modules\Booking\Models\Booking;
use Modules\Booking\Models\Calendar\BookingCalendar;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Entity\Models\Category\EntityCategory;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServicePrices;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\Models\Service\EntityServices;
use Modules\Feedback\Models\Like\Like;
use Modules\User\Models\User;

trait EntityTrait
{

    /**
     * @return array
     */
    public function getLikedAttribute()
    {
        $user = BridgeHelper::getAccess()->getUser();
        $likeModel = BridgeHelper::getLike()->getLikeModel();
        $count = $likeModel->active()->where('like', 1)->where('model_type', 'entity')->where('model_id', $this->id)->count();
        $liked = $likeModel->active()->where('like', 1)->where('model_type', 'entity')->where('model_id', $this->id)->where('user_id', $user ? $user->id : 0)->count();
        $count_dislike = $likeModel->active()->where('like', 0)->where('model_type', 'entity')->where('model_id', $this->id)->count();
        $disliked = $likeModel->active()->where('like', 0)->where('model_type', 'entity')->where('model_id', $this->id)->where('user_id', $user ? $user->id : 0)->count();
        $like = [
            'total_count' => $count,
            'liked' => boolval($liked),
            'total_dislike_count' => $count_dislike,
            'disliked' => boolval($disliked),
        ];
        return $like;
    }

    public function setParamsAttribute($val)
    {
        if (!is_array($val) && !is_object($val)) {
            $this->attributes['params'] = $val;
        } else {
            $this->attributes['params'] = json_encode($val);
        }
    }

    /**
     * @return mixed|null
     */
    public function getParamsJsonAttribute()
    {
        if ($this->params) {
            $arr = json_decode($this->params, true);
            $json = $this->getAccessData($arr);
            $params = json_encode($json);
            return json_decode($params);
        }
        $json = $this->getAccessData();
        $params = json_encode($json);
        return json_decode($params);
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getSingleLinkAttribute()
    {
        if ($this->id) {
            return route('entity.view', ['id' => $this->id, 'slug' => $this->slug ? $this->slug : $this->title]);
        }
        return url('/');
    }

    /**
     * @param array $params
     * @return array
     */
    private function getAccessData($params = [])
    {
        $access = [
            'access_rate' => 'all',
            'access_comment' => 'all',
        ];
        $json = array_merge($params, $access);
        return $json;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsOwner($query)
    {
        return $query->join('entity_owners as eo', 'eo.entity_id', 'entities.id')
            ->where('eo.user_id', auth()->user()->id)->where('eo.active', 1)
            ->whereNull('eo.deleted_at')->select("entities.*");
    }

    /**
     * @return mixed
     */
    public function defaultEntityMedia()
    {
        return (new Entity\EntityMedias())->getDefaultImageAttribute();
    }

    /**
     * @return mixed
     */
    public function getMediaImageAttribute()
    {
        if (!$this->relationLoaded('entityImageMedia')) {
            $this->load('entityImageMedia');
        }
        return $this->getRelation('entityImageMedia') ?: $this->defaultEntityMedia();
//        return $this->defaultEntityMedia();
    }

    /**
     * @return mixed
     */
    public function getRateAttribute()
    {
        $rate = BridgeHelper::getRate()->getRateObject()->setModel($this->getTable(), $this->id)->setWithData(true)->init()->getData();
        return $rate;
    }

    /**
     * @return mixed
     */
    public function getRateOthersAttribute()
    {
        return BridgeHelper::getRate()->getRateObject()->setModel($this->getTable(), $this->id)->setTypes([
            'ambiance' => ['title' => trans("entity::entity.ambiance")],
            'cleanliness' => ['title' => trans("entity::entity.cleanliness")],
            'staff' => ['title' => trans("entity::entity.staff")],
            'value' => ['title' => trans("entity::entity.value")],
        ])->setWithData(true)->init()->getData();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeSortBy($query)
    {
        $sorted = false;
        if ($sort_by = $this->getRequestItem("sort_by")) {
            $sArr = explode("_", $sort_by);
            foreach ($sArr as &$item) {
                $item = ucfirst($item);
            }
            $methodName = implode("", $sArr);
            if (method_exists($this, "getSortBy" . $methodName)) {
                $sorted = true;
                $query = $this->{"getSortBy" . $methodName}($query);
            }

        }
        if (!$sorted) {
            return $query->orderBy("created_at", "DESC");;
        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    private function getSortByNew($query)
    {
        return $query->orderBy("created_at", "DESC");
    }

    /**
     * @param $query
     * @return mixed
     */
    private function getSortByViewCount($query)
    {
        return $query;
    }


    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByCategories($query)
    {
        if ($category = $this->getRequestItem("category_id")) {
            $query = $query->whereHas("entityCategories", function ($q) use ($category) {
                if (is_array($category) && $category) {
                    $q = $q->whereIn("category_id", $category);
                } else {
                    $q = $q->where("category_id", $category);
                }
                return $q;
            });


        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByAddress($query)
    {
        if (($lat = $this->getRequestItem('latitude')) && ($lon = $this->getRequestItem('longitude'))) {

            $query = $query->whereHas('entityAddress', function ($q) {
                if (($lat = $this->getRequestItem('latitude')) && ($lon = $this->getRequestItem('longitude'))) {
                    $near_dist = $this->getRequestItem('near_dist', 1000);
                    $q = $q->searchDistance($lat, $lon, $near_dist);
                }

                if ($category = $this->getRequestItem("category_id")) {
                    $q = $q->whereRaw('entity_id in (select entity_id from entity_category_relations where category_id in (' . (is_array($category) ? implode(",", $category) : $category) . '))');
                }
                return $q;
            });

        } else {

            $query = $query->orWhereHas('entityAddress', function ($q) {
                if ($address = $this->getRequestItem('text')) {
                    $q = $q->where('address', 'LIKE', '%' . $address . '%');
                }

                if ($category = $this->getRequestItem("category_id")) {
                    $q = $q->whereRaw('entity_id in (select entity_id from entity_category_relations where category_id in (' . (is_array($category) ? implode(",", $category) : $category) . '))');
                }
                return $q;
            });

        }

        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterEntity($query)
    {
        if ($term = $this->getRequestItem('text')) {
            $query = $query->where(function ($q) use ($term) {
                $q->orWhere('title', 'LIKE', '%' . $term . '%')
                    ->orWhere('short_description', 'LIKE', '%' . $term . '%');
            });
        }
        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityCategory()
    {
        return $this->belongsTo(EntityCategory::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo(Instance::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityParent()
    {
        return $this->belongsTo(Entity::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookingCalendars()
    {
        return $this->hasMany(BookingCalendar::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany(Booking::class, "model_id")->where("model_type", $this->getTable());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityAddresses()
    {
        return $this->hasMany(Entity\EntityAddress::class)->enable();
    }

    /**
     * @return mixed
     */
    public function entityAddress()
    {
        return $this->hasOne(Entity\EntityAddress::class)->active()->isDefault();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function entityCategories()
    {
        return $this->belongsToMany(EntityCategory::class, 'entity_category_relations', null, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityContacts()
    {
        return $this->hasMany(Entity\EntityContact::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityEmployees()
    {
        return $this->hasMany(Entity\EntityEmployee::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function entityEmployee()
    {
        return $this->hasOne(Entity\EntityEmployee::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityMedias()
    {
        return $this->hasMany(Entity\EntityMedias::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entitySliderMedias()
    {
        return $this->hasMany(Entity\EntityMedias::class)->isSliderMedias();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function entityImageMedia()
    {
        return $this->hasOne(Entity\EntityMedias::class)->isSliderMedias()->orderBy("sort");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityMetaGroups()
    {
        return $this->hasMany(Entity\EntityMetaGroup::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityOwners()
    {
        return $this->hasMany(Entity\EntityOwner::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function entityOwner()
    {
        return $this->hasOne(Entity\EntityOwner::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function entityOwnerUser()
    {
        return $this->hasOne(Entity\EntityOwner::class)->with('user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityRelationServicePrices()
    {
        return $this->hasMany(EntityRelationServicePrices::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityRelationServices()
    {
        return $this->hasMany(EntityRelationServices::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityProducts()
    {
        return $this->hasMany(Entity\EntityProduct::class);
    }

}
