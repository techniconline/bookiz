<?php

namespace Modules\Entity\Traits\Entity;

use Modules\Entity\Models\Service\EntityRelationServices;

trait EntityEmployeeServices
{


    /**
     * @return mixed
     */
    public function getParamsJsonAttribute()
    {
        $value = $this->params;
        if ($value){
            $value = json_decode($value);
        }
        return $value;
    }

    /**
     * @return mixed
     */
    public function getParamsArrayAttribute()
    {
        $value = $this->params;
        if ($value){
            $value = json_decode($value, true);
        }
        return $value;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityEmployee()
    {
        return $this->belongsTo(\Modules\Entity\Models\Entity\EntityEmployee::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityRelationService()
    {
        return $this->belongsTo(EntityRelationServices::class);
    }

}
