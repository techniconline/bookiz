<?php

namespace Modules\Entity\Traits\Entity;

use Illuminate\Support\Facades\DB;
use Modules\Core\Models\Location\Location;
use Modules\Entity\Models\Entity;

trait EntityAddress
{

    /**
     * @param $query
     * @param $lat
     * @param $lon
     * @param int $near_dist
     * @param string $type_near_dist -> m OR km
     * @return mixed
     */
    public function scopeSearchDistance($query, $lat, $lon, $near_dist = 1000, $type_near_dist = 'm')
    {
        $value_near_dist = 0;
        if ($type_near_dist == 'km') {
            $value_near_dist = 1000;
        }
        $query->where(DB::raw('calc_distance_point(' . $lat . ',' . $lon . ',location,' . $value_near_dist . ',null,null)'), '<=', $near_dist);
        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(Location::class, 'city_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo(Location::class, 'province_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Location::class, 'country_id');
    }

}
