<?php

namespace Modules\Entity\Traits\Entity;


use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;

trait EntityMedias
{

    /**
     * @return array
     */
    public function getDefaultImageAttribute()
    {
        $url = BridgeHelper::getConfig()->getSettings('default_poster', 'instance', 'entity');
        return [
            'id' => 0
            , 'type' => 'image'
            , 'url_image' => $url ? get_instance()->getCurrentUrl(url($url)) : $url
            , 'url_thumbnail' => $url ? get_instance()->getCurrentUrl(url($url)) : $url

        ];
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getUrlImageAttribute()
    {
        $value = self::STORAGE_PREFIX  .$this->src;
        return $value ? get_instance()->getCurrentUrl(url($value) . '?_v=' . strtotime($this->updated_at ? $this->updated_at : $this->created_at)) : ((BridgeHelper::getConfig()->getSettings('default_poster', 'instance', 'entity')) ? get_instance()->getCurrentUrl(url(BridgeHelper::getConfig()->getSettings('default_poster', 'instance', 'entity'))) : $value);
    }

    /**
     * @return string
     */
    public function getUrlThumbnailAttribute()
    {
        if ($value = $this->src) {
            $path = pathinfo($value, PATHINFO_DIRNAME);
            $file = pathinfo($value, PATHINFO_FILENAME);
            $extension = pathinfo($value, PATHINFO_EXTENSION);
            $value = self::STORAGE_PREFIX  . $path . DIRECTORY_SEPARATOR . $file . '_thumbnail.' . $extension;
            if (!file_exists(public_path($value))){
                $value = null;
            }
        }
        return $value ? get_instance()->getCurrentUrl(url($value) . '?_v=' . strtotime($this->updated_at ? $this->updated_at : $this->created_at)) : ((BridgeHelper::getConfig()->getSettings('default_poster', 'instance', 'entity')) ? get_instance()->getCurrentUrl(url(BridgeHelper::getConfig()->getSettings('default_poster', 'instance', 'entity'))) : $value);
    }

    /**
     * @return \stdClass
     */
    public function getListAttribute()
    {
        $listDetails = new \stdClass();
        $listDetails->id = $this->id;
        $listDetails->url_thumbnail = $this->url_thumbnail;
        $listDetails->url_image = $this->url_image;
        $listDetails->type = $this->type;
        $listDetails->sort = $this->sort;
        $listDetails->description = $this->description;
        return $listDetails;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsSliderMedias($query)
    {
        return $query->whereNull('entity_relation_service_id')->where("type", "image");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityRelationService()
    {
        return $this->belongsTo(EntityRelationServices::class);
    }

    /**
     * @return EntityRelationServices
     */
    public function getEntityRelationServiceModel()
    {
        return new EntityRelationServices();
    }

    /**
     * @return Entity
     */
    public function getEntityModel()
    {
        return new Entity();
    }

}
