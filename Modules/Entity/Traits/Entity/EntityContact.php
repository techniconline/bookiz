<?php

namespace Modules\Entity\Traits\Entity;

use Modules\Entity\Models\Entity;

trait EntityContact
{

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getMediaClassAttribute()
    {
        if ($this->media_type){
            return trans("entity::entity.contacts.media_type_class.".$this->media_type);
        }
        return null;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getMediaLinkAttribute()
    {
        if ($this->media_type){
            return trans("entity::entity.contacts.media_type_link.".$this->media_type, ["value"=>$this->media_value]);
        }
        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

}
