<?php

namespace Modules\Entity\Traits\Entity;


use Modules\Booking\Models\Booking\BookingDetails;
use Modules\Booking\Models\Calendar\BookingCalendarDetails;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Entity\Models\Entity;

trait EntityEmployee
{

    /**
     * @param $query
     * @param null $term
     * @return mixed
     */
    public function scopeFilterUser($query, $term = null)
    {
        $q = $term?:(isset($this->requestItems['q']) ? $this->requestItems['q'] : null);
        $query = $query->whereHas('user', function ($query) use ($q) {
            $query->where(function ($query) use ($q) {

                if ($q){
                    $query->orWhere('username', 'LIKE', '%' . $q . '%');
                    $query->orWhere('last_name', 'LIKE', '%' . $q . '%');
                    $query->orWhere('first_name', 'LIKE', '%' . $q . '%');
                }

            })->active();
        })->filterCurrentInstance();

        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(BridgeHelper::getUser()->getUserModel());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookingCalendarDetails()
    {
        return $this->hasMany(BookingCalendarDetails::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookingDetails()
    {
        return $this->hasMany(BookingDetails::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityEmployeeServices()
    {
        return $this->hasMany(Entity\EntityEmployeeServices::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityEmployeeExperienceInfos()
    {
        return $this->hasMany(Entity\EntityEmployeeExperienceInfo::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entityEmployeeInfos()
    {
        return $this->hasMany(Entity\EntityEmployeeInfo::class);
    }

}
