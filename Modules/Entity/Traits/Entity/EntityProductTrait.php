<?php

namespace Modules\Entity\Traits\Entity;

trait EntityProductTrait
{

    /**
     * @param $entity_product_id
     * @param $used
     * @return bool
     */
    public function usedProduct($entity_product_id, $used)
    {
        $product = \Modules\Entity\Models\Entity\EntityProduct::active()->find($entity_product_id);
        if (!$product) {
            return false;
        }
        $quantity = $product->quantity;
        $quantity = $quantity - $used;

        $product->quantity = $quantity;
        if ($product->quantity <= $product->order_point && $product->notify) {
            $this->notifyOrderPoint($product);
        }
        return $product->save();
    }

    public function notifyOrderPoint($product)
    {
        // TODO
    }

}
