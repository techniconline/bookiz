<?php

namespace Modules\Entity\Traits\EntityRelationService;

use BridgeHelper;
use DateHelper;

trait EntityRelationServiceSaleTrait
{
    /**
     * @param $entity_relation_service
     * @return array
     */
    public function getCartData($entity_relation_service)
    {
        $item = [
            'item_type' => 'entity_relation_services',
            'item_id' => $entity_relation_service->id,
            'options' => [
                'entity_id' => $entity_relation_service->entity_id,
                'entity_service_id' => $entity_relation_service->entity_service_id,
                'entity_client_id' => request()->get('entity_client_id'),
            ],
            'cost' => $entity_relation_service->price,
            'sale_cost' => $entity_relation_service->price - $entity_relation_service->discount,
            'text' => $this->getPriceText($entity_relation_service->price, $entity_relation_service->currency_id),
            'percent_discount' => $entity_relation_service->discount ? (($entity_relation_service->discount * 100) / $entity_relation_service->price) : 0,
            'currency' => $entity_relation_service->currency_id,
        ];
        $item['name'] = $entity_relation_service->title;
        $item['options']['product_key'] = sha1('entity_' . $entity_relation_service->entity_id . '_entity_relation_service_' . $entity_relation_service->id);
        $item['qty'] = 1;
        return $item;
    }

    /**
     * @param $price
     * @param $currency
     * @return mixed
     */
    public function getPriceText($price, $currency)
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($price, $currency, true);
    }

    /**
     * @param $user_id
     * @param $entity_relation_service_id
     * @param $course_id
     * @return mixed
     */
    public function addUserToActivity($user_id, $entity_relation_service_id, $course_id)
    {
        $time = time() + 157680000;
        $expireDate = DateHelper::setDateTime(date(trans('core::date.database.datetime'), $time), null, trans('core::date.database.datetime'))->getDateTime(trans('core::date.database.datetime'));
        $userEnroll = new CourseUserEnrollment();
        $userEnroll->user_id = $user_id;
        $userEnroll->course_id = $course_id;
        $userEnroll->entity_relation_service_id = $entity_relation_service_id;
        $userEnroll->type = CourseUserEnrollment::TYPE_ACTIVITY;
        $userEnroll->active_date = date(trans('core::date.database.datetime'), time());
        $userEnroll->expire_date = $expireDate;
        $userEnroll->active = 1;
        return $userEnroll->save();
    }
}
