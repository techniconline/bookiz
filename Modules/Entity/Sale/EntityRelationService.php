<?php

namespace Modules\Entity\Sale;

use Modules\Core\Patterns\Sale\Item\SaleItemPattern;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\Traits\EntityRelationService\EntityRelationServiceSaleTrait;
use Modules\Entity\ViewModels\Entity\Owner\EntityEmployeeViewModel;
use Modules\Sale\Models\Cart;

class EntityRelationService extends SaleItemPattern
{

    use EntityRelationServiceSaleTrait;

    protected $type = 'entity_relation_service';
    protected $itemID = false;

    private $entity_relation_service = false;
    private $options = [];

    /**
     * @param $item_id
     * @return $this|mixed
     */
    public function setItemId($item_id)
    {
        $this->itemID = $item_id;
        $this->entity_relation_service = EntityRelationServices::active()->with('entity', 'bookingCalendar', 'entityEmployeeServices.entityEmployee.user')->find($item_id);
        return $this;
    }

    /**
     * @param $options
     * @return $this|mixed
     */
    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return bool
     */
    public function checkItemSale()
    {
        return true;
    }

    /**
     * @param $cart
     * @return bool
     */
    public function canSaleItemInCart($cart)
    {
        if ($cart && $cart->cartItems) {
            $cartItems = $cart->cartItems;
            $items = $cartItems->pluck('item_id', 'item_type');
            $values = $items->values();
            $entityRelationServices = EntityRelationServices::active()->whereIn('id', $values)->get();
            foreach ($entityRelationServices as $item) {
                if ($this->entity_relation_service && $this->entity_relation_service->entity_id != $item->entity_id) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @return bool|mixed
     */
    public function canSaleItem()
    {
        if (!$this->entity_relation_service) {
            return false;
        }
        return $this->checkItemSale();
    }

    /**
     * @return int|mixed
     */
    public function getMaxQtyForSale()
    {
        return 1;
    }

    /**
     * @return bool
     */
    public function canDeleteItem()
    {
        return true;
    }

    /**
     * @return mixed
     */
    public function getSaleItem()
    {
        $data = $this->getCartData($this->entity_relation_service);
        //$data['options']['cat_ids']=$this->getCategoryIds();
        return $data;
    }

    /**
     * @return int|mixed
     */
    public function getItemQty()
    {
        return 1;
    }

    /**
     * @param $order
     * @param $item
     * @return bool|mixed
     */
    public function setOrderComplete($order, $item)
    {
//        $this->setEnrollments($order->user_id);
//        if ($this->enrollment) {
//            try {
//                $methodName = 'setOrderFor' . get_uppercase_by_underscores($this->entity_relation_service->type);
//                return $this->$methodName($order);
//            } catch (\Exception $exception) {
//                report($exception);
//            }
//
//        }
        return false;
    }

    /**
     * @param $order
     * @return array
     */
    public function setOrderForPackage($order)
    {
        $messages = [];
        $messages[] = $this->setOrderForNormal($order);
        $entity_relation_servicePackageEnrollments = CoursePackageEnrollment::active()->with('entity_relation_servicePackage')->where('entity_relation_service_id', $this->entity_relation_service->id)->get();
        foreach ($entity_relation_servicePackageEnrollments as $entity_relation_servicePackage) {
            $result = $this->enrollUserOnCoursePackage($order->user_id, $this->options['enrollment_id'], $entity_relation_servicePackage->package_entity_relation_service_id, $entity_relation_servicePackage->entity_relation_service_id);
            if ($result) {
                $messages[] = trans('entity_relation_service::enrollment.enroll_success_message', ['id' => $entity_relation_servicePackage->package_entity_relation_service_id, 'name' => $entity_relation_servicePackage->entity_relation_servicePackage->title]);
            }
        }
        return $messages;
    }

    /**
     * @param $order
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function setOrderForNormal($order)
    {
        if ($this->isEnroll()) {
            return trans('entity_relation_service::enrollment.is_enroll_message', ['id' => $this->entity_relation_service->id, 'name' => $this->entity_relation_service->title]);
        }

        // set entity_relation_service_enrollment TODO
        $result = $this->enrollUserOnCourse($order->user_id, $this->options['enrollment_id'], $this->entity_relation_service->id);
        if ($result) {
            return trans('entity_relation_service::enrollment.enroll_success_message', ['id' => $this->entity_relation_service->id, 'name' => $this->entity_relation_service->title]);
        } else {
            return trans('entity_relation_service::enrollment.enroll_error_message', ['id' => $this->entity_relation_service->id, 'name' => $this->entity_relation_service->title]);
        }
    }

    /**
     * @return array|mixed
     */
    public function getCategoryIds()
    {
        $cats = [];
        if ($this->entity_relation_service->category_id) {
            $cats[] = $this->entity_relation_service->category_id;
            $CourseCategory = new CourseCategory();
            $categories = CourseCategory::with('parentRecursive')->where('id', $this->entity_relation_service->category_id)->get();
            $parents = $CourseCategory->getParentsIds($categories);
            $cats = $parents->toArray();
        }
        return $cats;
    }


    /**
     * @return mixed
     * @throws \Throwable
     */
    public function getItemExtraData()
    {
        if (!$this->entity_relation_service) {
            return false;
        }

        if (!$this->entity_relation_service->entity) {
            return false;
        }

//        $employees = $this->getEmployeesViewModel($this->entity_relation_service->entity_id, $this->entity_relation_service->id);
        $employees = $this->getEmployees();
        $employeesSelectable = $this->getEmployeesSelect();
        $url = $this->entity_relation_service->entity->single_link;
        $timesheet = $this->getCalendarTimeSheet();
        $entity = isset($this->entity_relation_service->entity)?$this->entity_relation_service->entity:null;
        $data = [
            'image' => $this->entity_relation_service->image,
            'icon' => $this->entity_relation_service->icon,
            'booking_model_id' => $entity?$entity->id:null,
            'booking_model_type' => $entity?$entity->getTable():null,
            'entity_url' => $url,
            'booking_details_model_id' => $this->entity_relation_service?$this->entity_relation_service->id:null,
            'booking_details_model_type' => $this->entity_relation_service?$this->entity_relation_service->getTable():null,
            'employees' => $employees,
            'entity_relation_service' => $this->entity_relation_service,
            'employees_selectable' => $employeesSelectable,
            'calendar' => $this->entity_relation_service->bookingCalendar,
            'timesheet' => $this->getCalendarTimeSheet(),
        ];
        return $data;
    }

    /**
     * @return \Illuminate\Support\Collection|string
     */
    public function getCalendarTimeSheet()
    {
        $calendar = $this->entity_relation_service->bookingCalendar;
        if (!$calendar){
            return collect();
        }
        $viewModel = BridgeHelper::getBooking()->getBookingCalendarViewModel();
        $calendarTimeSheet = $viewModel->setRequest(request())->getCalendar($calendar->id,null, true);
        return collect($calendarTimeSheet);
    }

    /**
     * @return static
     */
    public function getEmployees()
    {
        $employees = $this->entity_relation_service->entityEmployeeServices;
        if ($employees) {
            $employees = collect($employees)->mapWithKeys(function ($row, $index) {
                return [$index => ["entity_employee_id" => $row->entityEmployee->id, "for_gender" => $row->for_gender, "full_name" => $row->entityEmployee->user->full_name]];
            });
        }
        return $employees;
    }

    /**
     * @return static
     */
    public function getEmployeesSelect()
    {
        $employees = $this->entity_relation_service->entityEmployeeServices;
        if ($employees) {
            $employees = collect($employees)->mapWithKeys(function ($row) {
                return [$row->entityEmployee->id => $row->entityEmployee->user->full_name . '('.$row->for_gender.')'];
            });
        }
        return $employees;
    }

    /**
     * @param $entity_id
     * @param $entity_relation_service_id
     * @return array|\Illuminate\Support\Collection|EntityEmployeeViewModel
     * @throws \Throwable
     */
    public function getEmployeesViewModel($entity_id, $entity_relation_service_id)
    {
        $viewModel = new EntityEmployeeViewModel();
        $employees = $viewModel->setRequest(request())->showEmployees(true, $entity_id, true, [$entity_relation_service_id]);
        return $employees;
    }
}