<?php

namespace Modules\Entity\Http\Controllers\Entity;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class EntityBookingController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('entity.entityBookingGrid')
            ->setActionMethod("getBookingListGrid")->response();
    }

    /**
     * @param $booking_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function bookingDetails($booking_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['booking_id' => $booking_id], $request)->setViewModel('entity.entityBookingGrid')
            ->setActionMethod("getBookingDetailsListGrid")->response();
    }

    /**
     * @param $booking_details_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function showBookingDetails($booking_details_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['booking_details_id' => $booking_details_id], $request)->setViewModel('entity.entityBooking')
            ->setActionMethod("showBookingDetails")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['booking_id' => $id], $request)->setViewModel('entity.entityBooking')
            ->setActionMethod("destroyBooking")->response();
    }

    /**
     * @param $booking_details_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyDetail($booking_details_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['booking_details_id' => $booking_details_id], $request)->setViewModel('entity.entityBooking')
            ->setActionMethod("destroyBookingDetails")->response();
    }

}
