<?php

namespace Modules\Entity\Http\Controllers\Entity;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class EntityProductController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('entity.entityProductGrid')
            ->setActionMethod("getProductListGrid")->response();
    }

    /**
     * @param $entity_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create($entity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_id'=>$entity_id],$request)->setViewModel('entity.entityProduct')
            ->setActionMethod("createProduct")->response();
    }

    /**
     * @param $entity_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('entity.entityProduct')
            ->setActionMethod("saveProduct")->response();
    }

    /**
     * @param $entity_product_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($entity_product_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_product_id' => $entity_product_id], $request)->setViewModel('entity.entityProduct')
            ->setActionMethod("createProduct")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_product_id' => $id], $request)->setViewModel('entity.entityProduct')
            ->setActionMethod("saveProduct")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_product_id' => $id], $request)->setViewModel('entity.entityProduct')
            ->setActionMethod("destroyProduct")->response();
    }

}
