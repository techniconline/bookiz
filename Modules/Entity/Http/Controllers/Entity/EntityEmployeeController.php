<?php

namespace Modules\Entity\Http\Controllers\Entity;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class EntityEmployeeController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('entity.entityEmployeeGrid')
            ->setActionMethod("getEmployeeListGrid")->response();
    }

    /**
     * @param $entity_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create($entity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_id'=>$entity_id],$request)->setViewModel('entity.entityEmployee')
            ->setActionMethod("createEmployee")->response();
    }

    /**
     * @param $entity_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('entity.entityEmployee')
            ->setActionMethod("saveEmployee")->response();
    }

    /**
     * @param $entity_relation_service_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($entity_employee_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_id' => $entity_employee_id], $request)->setViewModel('entity.entityEmployee')
            ->setActionMethod("createEmployee")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_id' => $id], $request)->setViewModel('entity.entityEmployee')
            ->setActionMethod("saveEmployee")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_id' => $id], $request)->setViewModel('entity.entityEmployee')
            ->setActionMethod("destroyEmployee")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyEmployeeInfo($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_id' => $id], $request)->setViewModel('entity.entityEmployee')
            ->setActionMethod("destroyEmployeeInfo")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyEmployeeExperienceInfo($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_experience_id' => $id], $request)->setViewModel('entity.entityEmployee')
            ->setActionMethod("destroyEmployeeExperienceInfo")->response();
    }
    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyEmployeeService($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_service_id' => $id], $request)->setViewModel('entity.entityEmployee')
            ->setActionMethod("destroyEmployeeService")->response();
    }


    /**
     * Remove the specified resource from storage.
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getEntityEmployeeByApi(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('entity.entityEmployee')->setActionMethod("getEntityEmployeeByApi")->response();
    }

}
