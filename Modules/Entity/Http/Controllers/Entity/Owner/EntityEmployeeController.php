<?php

namespace Modules\Entity\Http\Controllers\Entity\Owner;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class EntityEmployeeController extends Controller
{

    /**
     * @param $entity_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($entity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_id' => $entity_id], $request)->setViewModel('entity.owner.entityEmployee')
            ->setActionMethod("saveEmployee")->response();
    }

    /**
     * @param $entity_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($entity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_id' => $entity_id], $request)->setViewModel('entity.owner.entityEmployee')
            ->setActionMethod("showEmployees")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_id' => $id], $request)->setViewModel('entity.owner.entityEmployee')
            ->setActionMethod("saveEmployee")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $entity_id
     * @param $user_id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addAvatar($entity_id, $user_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['user_id' => $user_id, 'entity_id' => $entity_id], $request)->setViewModel('entity.owner.entityEmployee')
            ->setActionMethod("addAvatar")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_id' => $id], $request)->setViewModel('entity.owner.entityEmployee')
            ->setActionMethod("destroyEmployee")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getUserForEmployeeByApi(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('entity.owner.entityEmployee')->setActionMethod("getUserForEmployeeByApi")->response();
    }
    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyEmployeeInfo($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_id' => $id], $request)->setViewModel('entity.owner.entityEmployeeInfo')
            ->setActionMethod("destroyEmployeeInfo")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getEmployeeInfo($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_id' => $id], $request)->setViewModel('entity.owner.entityEmployeeInfo')
            ->setActionMethod("getEmployeeInfo")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveEmployeeInfo($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_id' => $id], $request)->setViewModel('entity.owner.entityEmployeeInfo')
            ->setActionMethod("saveEmployeeInfo")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $entity_employee_experience_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyEmployeeExperienceInfo($entity_employee_experience_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_experience_id' => $entity_employee_experience_id], $request)->setViewModel('entity.owner.entityEmployeeInfo')
            ->setActionMethod("destroyEmployeeExperienceInfo")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getEmployeeExperienceInfo($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_id' => $id], $request)->setViewModel('entity.owner.entityEmployeeInfo')
            ->setActionMethod("getEmployeeExperienceInfo")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveEmployeeExperienceInfo($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_id' => $id], $request)->setViewModel('entity.owner.entityEmployeeInfo')
            ->setActionMethod("saveEmployeeExperienceInfo")->response();
    }


    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addServiceToEmployee($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_id' => $id], $request)->setViewModel('entity.owner.entityEmployee')
            ->setActionMethod("addServiceToEmployee")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyEmployeeService($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_employee_service_id' => $id], $request)->setViewModel('entity.owner.entityEmployee')
            ->setActionMethod("destroyEmployeeService")->response();
    }


}
