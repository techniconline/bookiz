<?php

namespace Modules\Entity\Http\Controllers\Entity\Owner;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class EntityServiceController extends Controller
{

    /**
     * @param $entity_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($entity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_id'=>$entity_id],$request)->setViewModel('entity.owner.entityService')
            ->setActionMethod("saveService")->response();
    }

    /**
     * @param $entity_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($entity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_id' => $entity_id], $request)->setViewModel('entity.owner.entityService')
            ->setActionMethod("showServices")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_relation_service_id' => $id], $request)->setViewModel('entity.owner.entityService')
            ->setActionMethod("saveService")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_relation_service_id' => $id], $request)->setViewModel('entity.owner.entityService')
            ->setActionMethod("destroyService")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getEntityServicesByApi(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('entity.front.entityService')->setActionMethod("getEntityServicesByApi")->response();
    }
}
