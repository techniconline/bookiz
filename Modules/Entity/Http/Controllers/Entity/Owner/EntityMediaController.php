<?php

namespace Modules\Entity\Http\Controllers\Entity\Owner;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class EntityMediaController extends Controller
{

    /**
     * Update the specified resource in storage.
     * @param $type is image, video or ...
     * @param $model_id
     * @param $model
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function uploadMedia($type, $model_id, $model, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['type' => $type, 'model_id' => $model_id, 'model_type' => $model], $request)->setViewModel('entity.entityMedia')
            ->setActionMethod("uploadMedia")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $entity_media_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($entity_media_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_media_id' => $entity_media_id], $request)->setViewModel('entity.entityMedia')
            ->setActionMethod("destroyEntityMedia")->response();
    }

}
