<?php
Route::group(['middleware' => ['web'], 'prefix' => 'entity', 'as' => 'entity.', 'namespace' => 'Modules\Entity\Http\Controllers'], function () {


    Route::group(['namespace' => '\Entity'], function () {
        Route::get('/search', 'EntityController@listEntities')->name('search');
        Route::get('/city/{lat}/{lon}/{near}', 'EntityController@getByLocationListEntities')->name('search_in_city');
        Route::get('/list', 'EntityController@listEntities')->name('list');
        Route::get('/list/by_category/{category_id}', 'EntityController@listEntitiesByCategory')->name('list_by_category');
        Route::get('/view/{entity_id}/{slug}', 'EntityController@show')->name('view');
        Route::get('/list/get', 'EntityController@getEntityByApi')->name('get_entity_by_api');
    });


    Route::group(['prefix' => 'category', 'as' => 'category.', 'namespace' => '\Category'], function () {

        Route::group(['middleware' => 'api', 'as' => 'api.'], function () {
            Route::get('/get/root/list', 'CategoryController@getRootCategories')->name('get_root_categories');
            Route::get('/get/{parent_id}/childes/list', 'CategoryController@getChildesCategories')->name('get_childes_categories');
        });

    });

    Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.', 'namespace' => '\Entity\Owner'], function () {

        Route::get('/list_entity', 'EntityController@listEntity')->name('list_entity');
        Route::get('/edit/{entity_id}', 'EntityController@edit')->name('edit');
        Route::post('/save', 'EntityController@store')->name('save');
        Route::put('/{entity_id}/update', 'EntityController@update')->name('update');
        Route::delete('/{entity_id}/delete', 'EntityController@destroy')->name('delete');

        Route::group(['prefix' => 'media', 'as' => 'media.'], function () {
            Route::post('/{type}/{model_id}/{model_type}/upload', 'EntityMediaController@uploadMedia')->name('upload_media');
            Route::delete('/{entity_media_id}/delete', 'EntityMediaController@destroy')->name('delete');
        });

        Route::group(['prefix' => 'service', 'as' => 'service.'], function () {
            Route::get('/list', 'EntityServiceController@getEntityServicesByApi')->name('list');
            Route::post('{entity_id}/save', 'EntityServiceController@store')->name('save');
            Route::get('/show/{entity_id}', 'EntityServiceController@show')->name('show');
            Route::put('/{entity_relation_service_id}/update', 'EntityServiceController@update')->name('update');
            Route::delete('/{entity_relation_service_id}/delete', 'EntityServiceController@destroy')->name('delete');
        });

        Route::group(['prefix' => 'product', 'as' => 'product.'], function () {
            Route::get('/units', 'EntityProductController@getUnitsOfProduct')->name('units');
            Route::get('/list', 'EntityProductController@getEntityProductsByApi')->name('list');
            Route::get('/show/{entity_id}', 'EntityProductController@show')->name('show');
            Route::get('/{entity_product_id}/edit', 'EntityProductController@edit')->name('edit');
            Route::post('{entity_id}/save', 'EntityProductController@store')->name('save');
            Route::put('/{entity_product_id}/update', 'EntityProductController@update')->name('update');
            Route::delete('/{entity_product_id}/delete', 'EntityProductController@destroy')->name('delete');

            Route::post('{entity_relation_service_id}/service/assign', 'EntityProductController@assignServiceToProducts')->name('assign_service_to_products');
            Route::delete('/{entity_product_service_id}/destroyProductService', 'EntityProductController@destroyProductService')->name('delete_product_service');
        });

        Route::group(['prefix' => 'employee', 'as' => 'employee.'], function () {
            Route::get('/search', 'EntityEmployeeController@getUserForEmployeeByApi')->name('search');
            Route::post('{entity_id}/save', 'EntityEmployeeController@store')->name('save');
            Route::get('/show/{entity_id}', 'EntityEmployeeController@show')->name('show');
            Route::post('/{entity_id}/{user_id}/add_avatar', 'EntityEmployeeController@addAvatar')->name('add_avatar');
            Route::put('/{entity_employee_id}/update', 'EntityEmployeeController@update')->name('update');
            Route::delete('/{entity_employee_id}/delete', 'EntityEmployeeController@destroy')->name('delete');

            Route::group(['prefix' => 'service', 'as' => 'service.'], function () {
                Route::post('{employee_id}/save', 'EntityEmployeeController@addServiceToEmployee')->name('add_service_to_employee');
                Route::delete('{entity_employee_service_id}/delete', 'EntityEmployeeController@destroyEmployeeService')->name('remove_service_to_employee');
            });

            Route::group(['prefix' => 'info', 'as' => 'info.'], function () {
                Route::get('/{entity_employee_id}', 'EntityEmployeeController@getEmployeeInfo')->name('get');
                Route::post('/{entity_employee_id}/save', 'EntityEmployeeController@saveEmployeeInfo')->name('save');
                Route::delete('/{entity_employee_id}/delete', 'EntityEmployeeController@destroyEmployeeInfo')->name('delete');
            });

            Route::group(['prefix' => 'experience', 'as' => 'experience.'], function () {
                Route::get('/{entity_employee_id}/list', 'EntityEmployeeController@getEmployeeExperienceInfo')->name('get');
                Route::post('/{entity_employee_id}/save', 'EntityEmployeeController@saveEmployeeExperienceInfo')->name('save');
                Route::delete('/{entity_employee_experience_id}/delete', 'EntityEmployeeController@destroyEmployeeExperienceInfo')->name('delete');
            });

        });

        Route::group(['prefix' => 'client', 'as' => 'client.'], function () {
            Route::get('/search', 'EntityClientController@getUserForClientByApi')->name('search');
            Route::post('{entity_id}/save', 'EntityClientController@store')->name('save');
            Route::get('/show/{entity_id}', 'EntityClientController@show')->name('show');
            Route::get('/info/{entity_client_id}', 'EntityClientController@showInfo')->name('show_info');
            Route::post('/{entity_id}/{user_id}/add_avatar', 'EntityClientController@addAvatar')->name('add_avatar');
            Route::put('/{entity_employee_id}/update', 'EntityClientController@update')->name('update');
            Route::delete('/{entity_employee_id}/delete', 'EntityClientController@destroy')->name('delete');

        });

        Route::group(['prefix' => 'address', 'as' => 'address.'], function () {
            Route::get('/list', 'EntityAddressController@getEntityAddressByApi')->name('list');
            Route::post('{entity_id}/save', 'EntityAddressController@store')->name('save');
            Route::get('/show/{entity_id}', 'EntityAddressController@show')->name('show');
            Route::put('/{entity_address_id}/update', 'EntityAddressController@update')->name('update');
            Route::delete('/{entity_address_id}/delete', 'EntityAddressController@destroy')->name('delete');
        });

        Route::group(['prefix' => 'contact', 'as' => 'contact.'], function () {
            Route::post('{entity_id}/save', 'EntityContactController@store')->name('save');
            Route::post('{entity_id}/storeContacts', 'EntityContactController@storeContacts')->name('store_contacts');
            Route::get('/show/{entity_id}', 'EntityContactController@show')->name('show');
            Route::put('/{entity_contact_id}/update', 'EntityContactController@update')->name('update');
            Route::delete('/{entity_contact_id}/delete', 'EntityContactController@destroy')->name('delete');
        });

    });

    Route::group(['prefix' => 'user', 'as' => 'user.', 'namespace' => '\Entity'], function () {

        Route::get('/show_services_entity/{entity_id}', 'EntityController@showServicesOfEntity')->name('showServicesOfEntity');

        Route::group(['prefix' => 'service', 'as' => 'service.'], function () {
            Route::get('/list', 'EntityServicesController@listServices')->name('list');
            Route::get('/entities/{entity_service_id}/list', 'EntityServicesController@listEntities')->name('listEntities');
        });

    });


});

Route::group(['middleware' => ['web', 'auth', 'admin'], 'prefix' => 'entity', 'as' => 'entity.', 'namespace' => 'Modules\Entity\Http\Controllers'], function () {

    Route::group(['prefix' => 'config', 'as' => 'config.', 'namespace' => '\Config'], function () {
        Route::get('/instance/index', 'InstanceController@index')->name('instance.index');
        Route::post('/instance/save', 'InstanceController@save')->name('instance.save');
    });

    Route::group(['namespace' => '\Entity'], function () {
        Route::get('/', 'EntityController@index')->name('index');
        Route::get('/create', 'EntityController@create')->name('create');
        Route::get('/{entity_id}/edit', 'EntityController@edit')->name('edit');
        Route::post('/save', 'EntityController@store')->name('save');
        Route::put('/{entity_id}/update', 'EntityController@update')->name('update');
        Route::delete('/{entity_id}/delete', 'EntityController@destroy')->name('delete');

        Route::group(['prefix' => 'media', 'as' => 'media.'], function () {
            Route::post('/{type}/{model_id}/{model_type}/upload', 'EntityMediaController@uploadMedia')->name('upload_media');
            Route::delete('/{entity_media_id}/delete', 'EntityMediaController@destroy')->name('delete');
        });


        Route::group(['prefix' => 'service', 'as' => 'service.'], function () {

            Route::get('/', 'EntityServicesController@index')->name('index');
            Route::get('/create', 'EntityServicesController@create')->name('create');
            Route::get('/{entity_service_id}/edit', 'EntityServicesController@edit')->name('edit');
            Route::post('/save', 'EntityServicesController@store')->name('save');
            Route::put('/{entity_service_id}/update', 'EntityServicesController@update')->name('update');
            Route::delete('/{entity_service_id}/delete', 'EntityServicesController@destroy')->name('delete');
            Route::get('/list/get', 'EntityServicesController@getEntityServicesByApi')->name('get_entity_services_by_api');

            Route::group(['prefix' => 'relation', 'as' => 'relation.'], function () {
                Route::get('/', 'EntityRelationServiceController@index')->name('index');
                Route::get('/{entity_id}/create', 'EntityRelationServiceController@create')->name('create');
                Route::get('/{entity_relation_service_id}/edit', 'EntityRelationServiceController@edit')->name('edit');
                Route::post('/save', 'EntityRelationServiceController@store')->name('save');
                Route::put('/{entity_relation_service_id}/update', 'EntityRelationServiceController@update')->name('update');
                Route::delete('/{entity_relation_service_id}/delete', 'EntityRelationServiceController@destroy')->name('delete');
                Route::get('/list/get', 'EntityRelationServiceController@getEntityRelationServiceByApi')->name('get_entity_relation_services_by_api');
            });

        });

        Route::group(['prefix' => 'calendar', 'as' => 'calendar.'], function () {
            Route::get('/', 'EntityCalendarController@index')->name('index');
            Route::get('/{entity_id}/create', 'EntityCalendarController@create')->name('create');
            Route::get('/{entity_product_id}/edit', 'EntityCalendarController@edit')->name('edit');
            Route::post('/save', 'EntityCalendarController@store')->name('save');
            Route::put('/{booking_calendar_id}/update', 'EntityCalendarController@update')->name('update');
            Route::delete('/{booking_calendar_id}/delete', 'EntityCalendarController@destroy')->name('delete');
        });

        Route::group(['prefix' => 'booking', 'as' => 'booking.'], function () {
            Route::get('/', 'EntityBookingController@index')->name('index');
            Route::put('/{booking_id}/update', 'EntityBookingController@update')->name('update');
            Route::delete('/{booking_id}/delete', 'EntityBookingController@destroy')->name('delete');

            Route::get('/{booking_id}/details', 'EntityBookingController@bookingDetails')->name('details');
            Route::get('details/{booking_details_id}/show', 'EntityBookingController@showBookingDetails')->name('details.show');
            Route::delete('details/{booking_details_id}/delete', 'EntityBookingController@destroyDetail')->name('details.delete');
        });

        Route::group(['prefix' => 'product', 'as' => 'product.'], function () {
            Route::get('/', 'EntityProductController@index')->name('index');
            Route::get('/{entity_id}/create', 'EntityProductController@create')->name('create');
            Route::get('/{entity_product_id}/edit', 'EntityProductController@edit')->name('edit');
            Route::post('/save', 'EntityProductController@store')->name('save');
            Route::put('/{entity_product_id}/update', 'EntityProductController@update')->name('update');
            Route::delete('/{entity_product_id}/delete', 'EntityProductController@destroy')->name('delete');
        });

        Route::group(['prefix' => 'employee', 'as' => 'employee.'], function () {
            Route::get('/', 'EntityEmployeeController@index')->name('index');
            Route::get('/{entity_id}/create', 'EntityEmployeeController@create')->name('create');
            Route::get('/{entity_employee_id}/edit', 'EntityEmployeeController@edit')->name('edit');
            Route::post('/save', 'EntityEmployeeController@store')->name('save');
            Route::put('/{entity_employee_id}/update', 'EntityEmployeeController@update')->name('update');
            Route::delete('/{entity_employee_id}/delete', 'EntityEmployeeController@destroy')->name('delete');
            Route::delete('info/{entity_employee_id}/delete', 'EntityEmployeeController@destroyEmployeeInfo')->name('info.delete');
            Route::delete('experience/{entity_employee_experience_id}/delete', 'EntityEmployeeController@destroyEmployeeExperienceInfo')->name('experience.delete');
            Route::delete('service/{entity_employee_service_id}/delete', 'EntityEmployeeController@destroyEmployeeService')->name('service.delete');
        });

        Route::group(['prefix' => 'contact', 'as' => 'contact.'], function () {
            Route::get('/', 'EntityContactController@index')->name('index');
            Route::get('/{entity_id}/create', 'EntityContactController@create')->name('create');
            Route::get('/{entity_contact_id}/edit', 'EntityContactController@edit')->name('edit');
            Route::post('/save', 'EntityContactController@store')->name('save');
            Route::put('/{entity_contact_id}/update', 'EntityContactController@update')->name('update');
            Route::delete('/{entity_contact_id}/delete', 'EntityContactController@destroy')->name('delete');
        });

        Route::group(['prefix' => 'address', 'as' => 'address.'], function () {
            Route::get('/', 'EntityAddressController@index')->name('index');
            Route::get('/{entity_id}/create', 'EntityAddressController@create')->name('create');
            Route::get('/{entity_address_id}/edit', 'EntityAddressController@edit')->name('edit');
            Route::post('/save', 'EntityAddressController@store')->name('save');
            Route::put('/{entity_address_id}/update', 'EntityAddressController@update')->name('update');
            Route::delete('/{entity_address_id}/delete', 'EntityAddressController@destroy')->name('delete');
        });

    });

    Route::group(['prefix' => 'category', 'as' => 'category.', 'namespace' => '\Category'], function () {
        Route::get('/', 'CategoryController@index')->name('index');
        Route::get('/create', 'CategoryController@create')->name('create');
        Route::get('/api/get', 'CategoryController@getCategoryByApi')->name('get_category_by_api');
        Route::post('/save', 'CategoryController@store')->name('save');
        Route::post('/add/child/{parent_id}', 'CategoryController@addChild')->name('add_child');
        Route::put('/{category_id}/change_position', 'CategoryController@changePosition')->name('change_position');
        Route::put('/{category_id}/update', 'CategoryController@update')->name('update');
        Route::get('/{category_id}/show', 'CategoryController@show')->name('show');
        Route::delete('/{category_id}/delete', 'CategoryController@destroy')->name('delete');

    });


});
