<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Entity\Providers\Helpers\Category;

use Illuminate\Support\Facades\Facade;

class EntityCategoryHelperFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "entityCategoryHelper";
    }
}