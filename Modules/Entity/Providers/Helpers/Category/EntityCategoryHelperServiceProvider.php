<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Entity\Providers\Helpers\Category;

use Illuminate\Support\ServiceProvider;

class EntityCategoryHelperServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->registerThisHelperBuilder();
        $this->app->alias("entityCategoryHelper", "Modules\Entity\Providers\Helpers\Category");
    }

    protected function registerThisHelperBuilder(){
        $this->app->singleton("entityCategoryHelper", function (){
            return new EntityCategoryHelper();
        });
    }

}