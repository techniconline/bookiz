<?php

namespace Modules\Entity\ViewModels\Blocks\EntityCategoryBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;
use Modules\Entity\Models\Category\EntityCategory;

class EntityCategoryBlockConfig
{
    use MasterBlockConfig;

    public $baseClass = 'EntityCategoryBlock';

    public function __construct()
    {
        $this->setDefaultName(trans('entity::block.entity_category'))
            ->addTab('entity_category', trans('entity::block.entity_category'), 'entity::widgets.entity_category.config.form')
            ->addTab('entity_category_advanced', trans('entity::block.advanced'), 'entity::widgets.entity_category.config.advanced')
            ->addTab('entity_category_mobile', trans('entity::block.mobile'), 'entity::widgets.entity_category.config.mobile');
    }

    /**
     * @return array
     */
    public function getBlockRules()
    {
        return [
//            'entity_category_id'=>'nullable|exists:entity_categories,id'
        ];
    }

    /**
     * @return array
     */
    public function getShowList()
    {
        $list = [
            'normal' => trans("entity::form.fields.list_types.normal"),
            'box_list' => trans("entity::form.fields.list_types.box_list"),
        ];
        return $list;
    }

    /**
     * @return array
     */
    public function getEntityCategoryRootList()
    {
        $entity_categoryList = EntityCategory::filterLanguage()->filterCurrentInstance()->whereNull("parent_id")->get();
        $list = [];
        $list[0] = "...";
        foreach ($entity_categoryList as $entity_category) {
            $list[$entity_category->id] = $entity_category->title;
        }
        return $list;
    }

    /**
     * @return array
     */
    public function getEntityCategoriesList()
    {
        $entity_categoryList = EntityCategory::filterLanguage()->filterCurrentInstance()->with("parentRecursive")->get();
        $list = [];
        $list[0] = "...";
        foreach ($entity_categoryList as $entity_category) {
            $this->parents = [];
            $this->getParents($entity_category);
            $list[$entity_category->id] = implode(" > ",$this->parents) ." > ". $entity_category->title;
        }
        return $list;
    }

    private $parents = [];

    /**
     * @param $row
     * @return mixed
     */
    public function getParents($row)
    {
        if ($row->parentRecursive) {
            $this->parents[] = $this->getParents($row->parentRecursive, false);
        }
        return $row->title;
    }

}
