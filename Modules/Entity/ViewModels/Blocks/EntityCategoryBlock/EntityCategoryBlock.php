<?php

namespace Modules\Entity\ViewModels\Blocks\EntityCategoryBlock;


use BridgeHelper;
use Modules\Core\Traits\Block\MasterBlock;
use Modules\Entity\Providers\Helpers\Category\EntityCategoryHelperFacade as CategoryHelper;

class EntityCategoryBlock
{
    use MasterBlock;

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel =& $this;
        return view('entity::widgets.entity_category.index', compact('blockViewModel'))->render();
    }

    /**
     * @return array
     */
    public function renderJson()
    {
        $categories = $this->getEntityCategories();
        $block = $this->setDataBlock($categories)->getBlock();
        return $block;
    }

    /**
     * @param $categories
     * @return mixed
     */
    private function getDataCategories($categories)
    {
        foreach ($categories as $category) {
            $category->url = $this->getEntityCategoryUrl($category);
            if ($category->childes->count()) {
                $this->getDataCategories($category->childes);
            }
        }
        return $categories;
    }

    /**
     * @param $entity_category
     * @return string
     */
    public function getEntityCategoryUrl($entity_category)
    {
        return $entity_category["url_entities"];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getEntityCategories()
    {
        $id = $this->getConfig('entity_category_id');
        if ($this->getConfig("root_entity_category_id")) {
            $id = $this->getConfig("root_entity_category_id");
        }
        if ($id){
            $entityCategories = CategoryHelper::getNestableEntityCategory($id, $this->getConfig('deep'),["entities_count"]);
        }else{
            $entityCategories = CategoryHelper::getNestableEntityCategories(0,["entities_count"]);
        }
//        dd($entityCategories);
        return $entityCategories;
    }


}
