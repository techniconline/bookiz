<?php

namespace Modules\Entity\ViewModels\Blocks\EntitiesBlock;

use Modules\Core\Traits\Block\MasterBlock;
use Modules\Core\Traits\Decorate\DecorateData;
use Modules\Entity\Models\Entity;
use Modules\Entity\ViewModels\Entity\Owner\EntityAddressViewModel;

class EntitiesBlock
{

    use MasterBlock;
    use DecorateData;

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel =& $this;
        return view('entity::widgets.entities.' . $this->getConfig("template", "slider"), compact('blockViewModel'))->render();
    }

    /**
     * @return array
     */
    public function renderJson()
    {
        $entities = $this->renderDataEntities($this->getEntitiesList());
        $block = $this->setDataBlock($entities)->getBlock();
        return $block;
    }

    /**
     * @param $entities
     * @return mixed
     */
    private function renderDataEntities($entities)
    {
        return $this->decorateList($entities);
    }

    /**
     * @return mixed
     */
    public function getEntitiesFun()
    {
        $type = $this->getConfig("method_name", "Near");
        $method = 'getEntities' . $type;

        if (method_exists($this, $method)) {
            $data = $this->{$method}();
        } else {
            $data = $this->getEntitiesList();
        }
        return $data;
    }

    private function getEntitiesNear()
    {
        $viewModel = new EntityAddressViewModel();
        $lat = $this->getConfig("latitude");
        $lon = $this->getConfig("longitude");
        $near_dist = $this->getConfig("near_dist", 1000);
        $result = $viewModel->setRequest(request())->getEntityAddressByApi($lat, $lon, $near_dist);
        $data = $result->data;
        $data = $this->getRandomData($data);
        $data = collect($data)->mapWithKeys(function ($item, $index) {
            $entity[$index] = $item['entity'];
            unset($item["entity"]);
            $entity[$index]["address"] = $item;
            return $entity;
        });
        $data = collect(json_decode(json_encode($data->toArray())));
        return $data;
    }

    /**
     * @param bool $paginate
     * @return mixed
     */
    public function getEntitiesList($paginate = false)
    {
        $model = new Entity();
        $take = $this->getConfig("entity_count_show", 8);
        $items = [
            "category_id" => $this->getConfig("entity_category_id", 0),
            "sort_by" => $this->getConfig("sort_by", "new"),
        ];
        $data = $model->setRequestItems($items)
//            ->filterLanguage()
            ->filterCurrentInstance()
            ->filterByCategories()->active()
            ->sortBy();

        if ($this->getConfig("is_random")) {
            $take = $take * 3;
        }

        if ($paginate) {
            $data = $data->paginate($take);
        } else {
            if ($take) {
                $data = $data->take($take);
            }
            $data = $data->get();

            if ($this->getConfig("is_random")) {
                $data = $this->getRandomData($data);
            }
        }
        return $data;
    }

    /**
     * @param $data
     * @return \Illuminate\Support\Collection|static
     */
    public function getRandomData($data)
    {
        try {
            $take = $this->getConfig("entity_count_show", 8);
            $data = collect($data);
            $count = $data->count();
            $data = $data->map(function ($row) use ($count) {
                $row['_sort_row'] = rand(0, $count);
                return $row;
            })->sortBy('_sort_row');
            return $data->take($take);
        } catch (\Exception $exception) {
            return $data;
        }
    }

    /**
     * @return mixed
     */
    public function getEntitiesListPaginate()
    {
        return $this->getEntitiesList(true);
    }


}
