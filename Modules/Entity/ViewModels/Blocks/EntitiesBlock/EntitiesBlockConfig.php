<?php

namespace Modules\Entity\ViewModels\Blocks\EntitiesBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;
use Modules\Entity\Models\Category\EntityCategory;

class EntitiesBlockConfig
{
    use MasterBlockConfig;

    public $baseClass = 'EntitiesBlock';

    public function __construct()
    {
        $this->setDefaultName(trans('entity::block.entities_block'));
        $this->addTab('entities_block', trans('entity::block.entities_block'), 'entity::widgets.entities.config.form');
        $this->addTab('advanced_block', trans('entity::block.advanced_block'), 'entity::widgets.entities.config.advanced');
    }


    /**
     * @return array
     */
    public function getTemplateList()
    {
        $list = [
            'slider' => trans("entity::form.fields.templates.slider"),
//            'list_box' => trans("entity::form.fields.templates.list_box"),
        ];
        return $list;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getSortList()
    {
        $list = trans("entity::entity.sort_items");
        return $list;
    }

    /**
     * @return array
     */
    public function getEntityCategoriesList()
    {
        $entity_categoryList = EntityCategory::active()->filterLanguage()->filterCurrentInstance()->with("parentRecursive")->get();
        $list = [];
        $list[0] = "...";
        foreach ($entity_categoryList as $entity_category) {
            $this->parents = [];
            $this->getParents($entity_category);
            $list[$entity_category->id] = implode(" > ", $this->parents) . " > " . $entity_category->title;
        }
        return $list;
    }

    private $parents = [];

    /**
     * @param $row
     * @return mixed
     */
    public function getParents($row)
    {
        if ($row->parentRecursive) {
            $this->parents[] = $this->getParents($row->parentRecursive, false);
        }
        return $row->title;
    }


}