<?php

namespace Modules\Entity\ViewModels\Entity;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\ViewModels\Entity\Owner\EntityEmployeeInfoViewModel;

class EntityEmployeeViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity_employee_info;
    private $entity_employee_experience_info;
    private $entity_relation_services;
    private $entity_employee_services;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {
            $access_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/entity/entity.js"), 'name' => 'entity-backend'];
        }

        return $access_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createEmployee()
    {
        $this->access_assets = true;
        $model = new Entity\EntityEmployee();
        $entity_employee_id = $this->request->get('entity_employee_id');
        $model = $model->find($entity_employee_id);
        $this->setModelData($model);

        $entity_id = $model ? $model->entity_id : $this->request->get('entity_id');
        $entity_service = EntityRelationServices::active()->with('entityService')->where('entity_id', $entity_id)->get();
        $entity_service = collect($entity_service)->mapWithKeys(function ($row) {
            $new[$row->id] = $row->title;
            return $new;
        });
        $this->entity_relation_services = $entity_service;

        if ($entity_employee_id && $model) {
            $viewModel = new EntityEmployeeInfoViewModel();
            $entity_employee_info = $viewModel->setRequest($this->request)->getEmployeeInfo(true, false);
            $this->entity_employee_info = isset($entity_employee_info["data"]) ? $entity_employee_info["data"] : null;
            $entity_employee_experience_info = $viewModel->setRequest($this->request)->getEmployeeExperienceInfo(true, false);
            $this->entity_employee_experience_info = isset($entity_employee_experience_info["data"]) ? $entity_employee_experience_info["data"] : null;
            $entity_employee_services = Entity\EntityEmployeeServices::active()->with('entityRelationService')->has('entityRelationService')->where('entity_employee_id', $entity_employee_id)->get();
            $this->entity_employee_services = $entity_employee_services;
        }

        $viewModel =& $this;
        $this->setTitlePage(trans('entity::entity.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("entity::employee.form_employee", ['view_model' => $viewModel], "form");
    }

    /**
     * @return mixed
     */
    public function getEntityRelationService()
    {
        return $this->entity_relation_services;
    }

    /**
     * @return mixed
     */
    public function getEmployeeServices()
    {
        return $this->entity_employee_services;
    }

    /**
     * @param null $key
     * @return null
     */
    public function getEmployeeInfo($key = null)
    {
        if ($key && $this->entity_employee_info) {
            if (isset($this->entity_employee_info[$key])) {
                return $this->entity_employee_info[$key];
            }
            return null;
        }
        return $this->entity_employee_info;
    }

    /**
     * @return mixed
     */
    public function getEmployeeExperienceInfo()
    {
        return $this->entity_employee_experience_info;
    }

    /**
     * @return Owner\EntityEmployeeViewModel
     */
    public function destroyEmployee()
    {
        $viewModel = new \Modules\Entity\ViewModels\Entity\Owner\EntityEmployeeViewModel();
        return $viewModel->setRequest($this->request)->destroyEmployee(false);
    }

    /**
     * @return EntityEmployeeInfoViewModel
     */
    public function destroyEmployeeInfo()
    {
        $viewModel = new EntityEmployeeInfoViewModel();
        return $viewModel->setRequest($this->request)->destroyEmployeeInfo(false);
    }

    /**
     * @return EntityEmployeeInfoViewModel
     */
    public function destroyEmployeeExperienceInfo()
    {
        $viewModel = new EntityEmployeeInfoViewModel();
        return $viewModel->setRequest($this->request)->destroyEmployeeExperienceInfo(false);
    }

    /**
     * @return Owner\EntityEmployeeViewModel
     */
    public function destroyEmployeeService()
    {
        $viewModel = new \Modules\Entity\ViewModels\Entity\Owner\EntityEmployeeViewModel();
        return $viewModel->setRequest($this->request)->destroyEmployeeService(false);
    }

    /**
     * @return $this
     */
    protected function saveEmployee()
    {
        $this->request = $this->requestValuesUpdate();
        $viewModel = new \Modules\Entity\ViewModels\Entity\Owner\EntityEmployeeViewModel();
        $data = $this->request->all();
        $response = $viewModel->serviceSaveEntityEmployee($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $info_employee = $this->request->get('info_employee');
            $info_employee['entity_employee_id'] = $this->request->get('entity_employee_id');
            if ($info_employee) {
                foreach ($info_employee as $key => &$item) {
                    if (!$item){
                        continue;
                    }
                    if (strpos($key, "date") !== false) {
                        $item = DateHelper::setLocaleDateTime($item, trans('core::date.datetime.date_medium'))->getDateTime(trans('core::date.datetime.date_medium'));
                    }
                }
                $responseInfo = $this->saveEmployeeInfo($info_employee);
                $data['data']['employee_info'] = $responseInfo;
            }

            $info_experience_employee = $this->request->get('info_experience_employee');
            $entity_employee_id = $this->request->get('entity_employee_id');
            $entity_relation_service_ids = $this->request->get('entity_relation_service_ids');
            if ($entity_relation_service_ids) {
                $responseEmployeeService = $this->addServiceToEmployee($entity_relation_service_ids, $entity_employee_id, $this->request->get('for_gender'));
                $data['data']['employee_service'] = $responseEmployeeService;
            }

            if ($info_experience_employee) {
                foreach ($info_experience_employee as $key => &$item) {
                    if (!$item){
                        continue;
                    }
                    if (strpos($key, "date") !== false) {
                        $item = DateHelper::setLocaleDateTime($item, trans('core::date.datetime.date_medium'))->getDateTime(trans('core::date.datetime.date_medium'));
                    }
                }
                $responseInfoExperience = $this->saveEmployeeInfoExperience($info_experience_employee, $entity_employee_id);
                $data['data']['employee_experience_info'] = $responseInfoExperience;
            }
            $this->redirect(route('entity.employee.edit', ['entity_employee_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }


    /**
     * @param $entity_relation_service_ids
     * @param $entity_employee_id
     * @param $for_gender
     * @param null $params
     * @return Owner\EntityEmployeeViewModel
     */
    public function addServiceToEmployee($entity_relation_service_ids, $entity_employee_id, $for_gender, $params = null)
    {
        $viewModel = new \Modules\Entity\ViewModels\Entity\Owner\EntityEmployeeViewModel();
        return $viewModel->setRequest($this->request)->addServiceToEmployee($entity_relation_service_ids, $entity_employee_id, $for_gender, $params, false);
    }

    /**
     * @param $data
     * @param $entity_employee_id
     * @return array
     */
    protected function saveEmployeeInfoExperience($data, $entity_employee_id)
    {
        $viewModel = new EntityEmployeeInfoViewModel();
        $response = [];
        foreach ($data as $datum) {
            $datum['entity_employee_id'] = $entity_employee_id;
            if (isset($datum['experience']) && $datum['experience']) {
                $response[] = $viewModel->serviceEmployeeExperienceInfo($datum, false);
            }
        }
        return $response;
    }

    /**
     * @param $data
     * @return array
     */
    protected function saveEmployeeInfo($data)
    {
        $viewModel = new EntityEmployeeInfoViewModel();
        $response = $viewModel->serviceSaveEmployeeInfo($data, false);
        return $response;
    }

    public function getEntityEmployeeByApi()
    {
        $q = false;
        $selected = false;
        $type = $this->request->get('_type');
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }
        $model = new Entity\EntityEmployee();
        $model = $model->setRequestItems($this->request->all())->active()->with('user');
        if ($q) {
            $items = $model->filterUser()->get();
        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->filterUser()->get();

        } else {
            $items = $type == 'query' ? $model->filterUser()->take(20)->orderBy('id', 'DESC')->get() : collect([]);

        }
        $items = $items->map(function ($item) {
            $item->text = $item->user->full_name . '(' . $item->user->username . ')';
            return $item;
        });
        return $this->setDataResponse($items)->setResponse(true);
    }

}
