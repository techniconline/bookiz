<?php

namespace Modules\Entity\ViewModels\Entity;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;

class EntityContactViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createContact()
    {
        $this->access_assets = true;
        $model = new Entity\EntityContact();
        $model = $model->find($this->request->get('entity_contact_id'));
        $this->setModelData($model);
        $viewModel =& $this;

        $this->setTitlePage(trans('entity::entity.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("entity::contact.form_contact", ['view_model' => $viewModel], "form");
    }

    /**
     * @return $this
     */
    protected function destroyContact()
    {
        $entity_contact_id = $this->request->get('entity_contact_id');
        $entity_contact = Entity\EntityContact::enable()->find($entity_contact_id);

        if (!$entity_contact) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        if ($entity_contact->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveContact()
    {
        $this->request = $this->requestValuesUpdate();
        $data = $this->request->all();
        $response = $this->serviceSaveEntityContact($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('entity.contact.edit', ['entity_contact_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }


    /**
     * @param $data
     * @return array
     */
    public function serviceSaveEntityContact($data)
    {
        try {
            $model = new Entity\EntityContact();
            $entity_contact_id = isset($data['entity_contact_id']) ? $data['entity_contact_id'] : 0;
            if ($entity_contact_id) {
                $model = $model->find($entity_contact_id);
                if (!$model) {
                    return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
                }
                $entity_id = $model->entity_id;
                if (!isset($data["is_default"]) && $model->is_default) {
                    $model->is_default = 0;
                }
            } else {
                $entity_id = isset($data['entity_id']) ? $data['entity_id'] : 0;
            }

            if (isset($data["is_default"]) && $data["is_default"]) {
                Entity\EntityContact::enable()->where('entity_id', $entity_id)
                    ->where(function ($q) use ($entity_contact_id) {
                        if ($entity_contact_id) {
                            $q->where('id', '!=', $entity_contact_id);
                        }
                    })->where("is_default", 1)->update(["is_default" => 0]);
            }

            if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
                $model->fill($data);
                if ($model->save()) {
                    return ['action' => true, 'message' => trans("entity::messages.alert.save_success"), 'data' => $model];
                } else {
                    return ['action' => false, 'message' => trans("entity::messages.alert.save_un_success")];
                }
            }
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors' => $this->errors];
        } catch (\Exception $exception) {
            report($exception);
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data")];
        }
    }

    /**
     * @return $this
     */
    public function getEntityContactByApi()
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }

        $model = new Entity\EntityContact();
        $model = $model->active()->select('id', DB::raw('media_value as text'));
        if ($q) {

            $model->where(function ($query) use ($q) {
                $query->where('media_value', 'LIKE', '%' . $q . '%');
            });
            $items = $model->get();

        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $items = $this->request->has('selected') ? [] : $model->orderBy('id', 'DESC')->take(20)->get();
        }

        return $this->setDataResponse($items)->setResponse(true);
    }


}
