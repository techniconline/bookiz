<?php

namespace Modules\Entity\ViewModels\Entity;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Category\EntityCategoryRelation;
use Modules\Entity\Models\Entity;

class EntityViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;

    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {
            $access_assets [] = ['container' => 'up_theme_js_2', 'src' => ("assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"), 'name' => 'ui.widget'];

            $access_assets [] = ['container' => 'plugin_general_2', 'src' => ("jquery-ui/jquery-ui.min.js"), 'name' => 'jquery-ui'];
            $access_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/plupload.full.min.js"), 'name' => 'plupload.full'];
            $access_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/jquery.ui.plupload.js"), 'name' => 'jquery.ui.plupload'];
            $access_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/i18n/fa.js"), 'name' => 'fa.js'];

            $access_assets [] = ['container' => 'general_style', 'src' => ("jquery-ui/jquery-ui.min.css"), 'name' => 'jquery-ui'];
            $access_assets [] = ['container' => 'general_style', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css"), 'name' => 'jquery.ui.plupload.css'];


            $access_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/entity/entity.js"), 'name' => 'entity-backend'];
        }

        return $access_assets;
    }


    /**
     * @return $this
     */
    protected function setAssetsCreateEntity()
    {
        $this->access_assets = true;
        return $this;
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createEntity()
    {
        $this->access_assets = true;
        $model = new Entity();
        $model = $model->find($this->request->get('entity_id'));
        $this->setModelData($model);
        $viewModel =& $this;

        $this->setTitlePage(trans('entity::entity.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("entity::entity.form_entity", ['view_model' => $viewModel], "form");
    }

    /**
     * @return int
     */
    public function getCountOfImages()
    {
        return 10;
    }

    /**
     * @param $entity
     * @return null
     */
    public function getSliderMedias($entity = null)
    {
        $entity = $entity ?: $this->modelData;
        if ($entity) {
            return Entity\EntityMedias::enable()->isSliderMedias()->where('entity_id', $entity->id)->get();
        }
        return null;
    }

    /**
     * @param $entity
     * @return array|mixed
     */
    public function getActiveCategories($entity = null)
    {
        $entity = $entity ?: $this->modelData;
        if (!$entity) {
            return null;
        }
        $activeCategories = $this->getCategories($entity->id);
        $activeCategories = $activeCategories ? $activeCategories->pluck('category_id') : [];
        return $activeCategories;
    }

    /**
     * @param null $entity
     * @return array|mixed|null
     */
    public function getActiveMedias($entity = null)
    {
        $entity = $entity ?: $this->modelData;
        if (!$entity) {
            return null;
        }
        $activeMedias = $this->getMedias($entity->id);
        $activeMedias = $activeMedias ? $activeMedias->pluck('list') : [];
        return $activeMedias;
    }

    /**
     * @param $entity_id
     * @param null|int|array $entity_relation_service_id
     * @return mixed
     */
    public function getMedias($entity_id, $entity_relation_service_id = null)
    {
        $model = new Entity\EntityMedias();
        $data = $model->where('entity_id', $entity_id)->active()
            ->where(function ($q)use($entity_relation_service_id){
                if ($entity_relation_service_id){
                    $q->whereIn('entity_relation_service_id', $entity_relation_service_id)->orderBy('entity_relation_service_id');
                }else{
                    $q->isSliderMedias();
                }
            })
            ->get();
        return $data;
    }

    /**
     * @param null $entity
     * @return array|mixed
     */
    public function getActiveOwners($entity = null)
    {
        $entity = $entity ?: $this->modelData;
        if (!$entity) {
            return null;
        }
        $activeOwners = $this->getOwners($entity->id);
        $activeOwners = $activeOwners ? $activeOwners->pluck('user_id') : [];
        return $activeOwners;
    }


    /**
     * @param $entity_id
     * @return mixed
     */
    public function getOwners($entity_id)
    {
        $model = new Entity\EntityOwner();
        $data = $model->where('entity_id', $entity_id)->active()->get();
        return $data;
    }

    /**
     * @param $entity_id
     * @return mixed
     */
    public function getCategories($entity_id)
    {
        $model = new EntityCategoryRelation();
        $data = $model->where('entity_id', $entity_id)->active()->get();
        return $data;
    }


    /**
     * @return EntityViewModel
     * @throws \Throwable
     */
    protected function editEntity()
    {
        return $this->createEntity();
    }

    /**
     * @return $this
     */
    protected function destroyEntity()
    {
        $entity_id = $this->request->get('entity_id');
        $entity = Entity::enable()->find($entity_id);

        if ($entity->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveEntity()
    {
        $user = BridgeHelper::getAccess()->getUser();
        $data = $this->request->all();
        $data['user_id'] = $user ? $user->id : 0;
        $data['instance_id'] = app('getInstanceObject')->getCurrentInstanceId();
        $response = $this->serviceSaveEntity($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('entity.edit', ['entity_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @param $model_type
     * @param $model_id
     * @param $data
     * @return array
     */
    public function serviceSaveEntity($data)
    {
        $model = new Entity();
        $entity_id = isset($data['entity_id']) ? $data['entity_id'] : 0;
        if ($entity_id) {
            $model = $model->enable()->find($entity_id);
            if (!$model) {
                return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
            }
        }

        if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
            $model->fill($data);
            if ($model->save()) {

                if (isset($data['categories']) && $data['categories']) {
                    $this->serviceSaveCategories($model->id, $data['categories']);
                    $this->serviceSaveOwners($model->id, $data['owners']);
                }


                return ['action' => true, 'message' => trans("entity::messages.alert.save_success"), 'data' => $model];
            } else {
                return ['action' => false, 'message' => trans("entity::messages.alert.save_un_success")];
            }
        }
        return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors' => $this->errors];
    }

    /**
     * @param $entity_id
     * @param $categories
     * @return bool
     */
    public function serviceSaveCategories($entity_id, $categories)
    {
        $model = new EntityCategoryRelation();
        $insertItems = [];
        $model->where('entity_id', $entity_id)->delete();

        foreach ($categories as $category) {
            $insertItems[] = ['entity_id' => $entity_id, 'category_id' => $category];
        }

        if ($insertItems) {
            return $model->insert($insertItems);
        }
        return false;
    }

    /**
     * @param $entity_id
     * @param $owners
     * @return bool
     */
    public function serviceSaveOwners($entity_id, $owners)
    {
        $model = new Entity\EntityOwner();
        $insertItems = [];
        $model->where('entity_id', $entity_id)->delete();

        foreach ($owners as $owner) {
            $insertItems[] = ['entity_id' => $entity_id, 'user_id' => $owner, 'active' => 1, 'created_at' => date("Y-m-d H:i:s")];
        }

        if ($insertItems) {
            return $model->insert($insertItems);
        }
        return false;
    }

    /**
     * @return $this
     */
    public function getEntityByApi()
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }

        $model = new Entity();
        $model = $model->enable()->filterCurrentInstance()->select('id', DB::raw('title as text'));
        if ($q) {

            $model->where(function ($query) use ($q) {
                $query->where('title', 'LIKE', '%' . $q . '%');
            });
            $items = $model->get();

        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $items = $this->request->has('selected') ? [] : $model->orderBy('id', 'DESC')->take(20)->get();
        }

        return $this->setDataResponse($items)->setResponse(true);
    }


}
