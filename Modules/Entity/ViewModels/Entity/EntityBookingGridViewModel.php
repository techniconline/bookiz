<?php

namespace Modules\Entity\ViewModels\Entity;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;

class EntityBookingGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $filter_items = [];
    private $access_assets = false;
    private $update_model = null;

    public function __construct()
    {

    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->useModel = BridgeHelper::getBooking()->getBookingModel();
        $this->filter_items = $params ? $params : request()->all();
        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function initDetails($params = [])
    {
        $this->useModel = BridgeHelper::getBooking()->getBookingDetailsModel();
        $this->filter_items = $params ? $params : request()->all();
        return $this;
    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->init();
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->filters()->enable()->with(['entity', 'bookingDetails', 'user']);
    }

    /**
     *
     */
    public function setDetailsGridModel()
    {
        $this->initDetails();
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->filters()->enable()->with(['entityEmployee.user', 'currency']);
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        //use bested assets
        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }


    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title_entity', trans('entity::entity.bookings.title_entity'), false);
        $this->addColumn('reservation_status_text', trans('entity::entity.bookings.reservation_status'), false);
        $this->addColumn('action_text', trans('entity::entity.bookings.action'), false);
        $this->addColumn('created_at', trans('entity::entity.created_at'), false);
        $this->addColumn('updated_at', trans('entity::entity.updated_at'), false);
        $this->addColumn('active', trans('entity::entity.status'), false);

        /*add action*/
        $add = array(
            'name' => 'entity.index',
            'parameter' => null
        );
        $this->addButton('list_entity', $add, trans('entity::entity.list'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);


        $show = array(
            'name' => 'entity.booking.details',
            'parameter' => ['id']
        );
        $this->addAction('content_details', $show, trans('entity::entity.bookings.detail'), 'fa fa-list', false, ['target' => '', 'class' => 'btn btn-sm btn-primary']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('entity::entity.confirm_delete')];
        $delete = array(
            'name' => 'entity.booking.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('entity::entity.delete'), 'fa fa-trash', false, $options);

        /**/
        $this->action_text = false;

        /*filter*/
//        $this->addFilter('title', 'text', ['relation' => 'entityRelationService']);
//        $this->addFilter('title_entity', 'text', ['relation' => 'entity']);
        return $this;
    }

    /**
     * @return $this
     */
    private function generateDetailsGridList()
    {
        $this->setDetailsGridModel();
        $this->update_model = 'details';
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title_service', trans('entity::entity.bookings.details.title_service'), false);
        $this->addColumn('employee', trans('entity::entity.bookings.details.employee'), false);
        $this->addColumn('by_owner', trans('entity::entity.bookings.details.by_owner_entity'), false);
        $this->addColumn('amount', trans('entity::entity.bookings.details.amount'), false);
        $this->addColumn('discount_amount', trans('entity::entity.bookings.details.discount_amount'), false);
        $this->addColumn('payable_amount', trans('entity::entity.bookings.details.payable_amount'), false);
        $this->addColumn('date', trans('entity::entity.bookings.details.date'), false);
        $this->addColumn('time', trans('entity::entity.bookings.details.time'), false);
        $this->addColumn('reservation_status_text', trans('entity::entity.bookings.reservation_status'), false);
        $this->addColumn('action_text', trans('entity::entity.bookings.action'), false);
        $this->addColumn('created_at', trans('entity::entity.created_at'), false);
        $this->addColumn('updated_at', trans('entity::entity.updated_at'), false);
        $this->addColumn('active', trans('entity::entity.status'), false);

        /*add action*/
        $booking = BridgeHelper::getBooking()->getBookingModel()->find($this->request->get("booking_id"));

        $add = array(
            'name' => 'entity.booking.index',
            'parameter' => ["entity_id" => ['value' => $booking->model_id]]
        );

        $this->addButton('list_booking', $add, trans('entity::entity.bookings.list'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);


        $options = ['class' => "gridAction ajax btn btn-sm btn-success"];
        $show = array(
            'name' => 'entity.booking.details.show',
            'parameter' => ['id']
        );
        $this->addAction('content_details', $show, trans('entity::entity.bookings.detail'), 'fa fa-file', false, $options);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('entity::entity.confirm_delete')];
        $delete = array(
            'name' => 'entity.booking.details.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('entity::entity.delete'), 'fa fa-trash', false, $options);

        /**/
        $this->action_text = false;

        /*filter*/
//        $this->addFilter('title', 'text', ['relation' => 'entityRelationService']);
//        $this->addFilter('title_entity', 'text', ['relation' => 'entity']);
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->active = trans('entity::entity.statuses.' . $row->active);
        if ($this->update_model == 'details') {
            $row->employee = isset($row->entityEmployee->user) ? $row->entityEmployee->user->full_name : '-';
            $row->by_owner = $row->by_owner ? $row->by_owner : '-';
            $row->title_service = $row->model_row ? $row->model_row->title : '-';
            $row->date = DateHelper::setDateTime($row->date, null, trans('core::date.datetime.date_medium'))->getLocaleFormat(trans('core::date.datetime.date_medium'));
        } else {
            $row->title_entity = $row->entity ? $row->entity->title : '-';
        }
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
//        if ($row->published) {
//            $action['publish']->icon = 'fa fa-eye';
//        } else {
//            $action['publish']->icon = 'fa fa-eye-slash';
//        }
        return $action;

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getBookingListGrid()
    {
        $this->setTitlePage(trans('entity::entity.bookings.list'));
        $this->generateGridList()->renderedView("entity::entity.index", ['view_model' => $this], "entity_booking_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getBookingDetailsListGrid()
    {
        $this->setTitlePage(trans('entity::entity.bookings.details.list'));
        $this->generateDetailsGridList()->renderedView("entity::entity.index", ['view_model' => $this], "entity_booking_details_list");
        return $this;
    }


}
