<?php

namespace Modules\Entity\ViewModels\Entity;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Entity\Models\Entity;

class EntityGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $filter_items = [];
    private $access_assets = false;

    public function __construct()
    {
        $this->useModel = new Entity();
    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->filter_items = $params ? $params : request()->all();
        return $this;
    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->init();
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->enable()
            ->filterCurrentInstance()
            ->filterLanguage()
            ->with(['entityOwnerUser', 'entityCategory'])->withCount('bookings');
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        //use bested assets
        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }


    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('entity::entity.title'), true);
        $this->addColumn('category', trans('entity::entity.category'), true);
        $this->addColumn('user', trans('entity::entity.owner'), false);
        $this->addColumn('bookings_count', trans('entity::entity.bookings_count'), false);
        $this->addColumn('open_user_bookings_count', trans('entity::entity.open_user_bookings_count'), false);
        $this->addColumn('open_entity_bookings_count', trans('entity::entity.open_entity_bookings_count'), false);
        $this->addColumn('created_at', trans('entity::entity.created_at'), false);
        $this->addColumn('updated_at', trans('entity::entity.updated_at'), false);
        $this->addColumn('active', trans('entity::entity.status'), false);

        /*add action*/
        $add = array(
            'name' => 'entity.create',
            'parameter' => null
        );
        $this->addButton('create_entity', $add, trans('entity::entity.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        $show = array(
            'name' => 'entity.edit',
            'parameter' => ['id']
        );
        $this->addAction('content_edit', $show, trans('entity::entity.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'entity.product.index',
            'parameter' => ['entity_id']
        );
        $this->addAction('content_products', $show, trans('entity::entity.products.list'), 'fa fa-list', false, ['target' => '', 'class' => 'btn btn-sm btn-primary']);

        $show = array(
            'name' => 'entity.service.relation.index',
            'parameter' => ['entity_id']
        );
        $this->addAction('content_services', $show, trans('entity::entity.services.show'), 'fa fa-tags', false, ['target' => '', 'class' => 'btn btn-sm btn-warning']);

        $show = array(
            'name' => 'entity.calendar.index',
            'parameter' => ['entity_id']
        );
        $this->addAction('content_calendars', $show, trans('booking::calendar.list'), 'fa fa-calendar', false, ['target' => '', 'class' => 'btn btn-sm btn-default']);

        $show = array(
            'name' => 'entity.employee.index',
            'parameter' => ['entity_id']
        );
        $this->addAction('content_employees', $show, trans('entity::entity.employee.list'), 'fa fa-users', false, ['target' => '', 'class' => 'btn yellow btn-sm btn-default']);

        $show = array(
            'name' => 'entity.booking.index',
            'parameter' => ['entity_id']
        );
        $this->addAction('content_bookings', $show, trans('booking::booking.list'), 'fa fa-shopping-cart', false, ['target' => '', 'class' => 'btn btn-sm btn-warning']);

        $show = array(
            'name' => 'entity.contact.index',
            'parameter' => ['entity_id']
        );
        $this->addAction('content_contacts', $show, trans('entity::entity.contacts.list'), 'fa fa-phone', false, ['target' => '', 'class' => 'btn blue btn-sm btn-default']);

        $show = array(
            'name' => 'entity.address.index',
            'parameter' => ['entity_id']
        );
        $this->addAction('content_addresses', $show, trans('entity::entity.addresses.list'), 'fa fa-map-signs', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('entity::entity.confirm_delete')];
        $delete = array(
            'name' => 'entity.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('entity::entity.delete'), 'fa fa-trash', false, $options);

        /**/
        $this->action_text = false;

        /*filter*/
        $this->addFilter('title', 'text');
        $this->addFilter('last_name', 'text', ['relation' => 'entityOwnerUser']);
        $this->addFilter('first_name', 'text', ['relation' => 'entityOwnerUser']);
        $this->addFilter('username', 'text', ['relation' => 'entityOwnerUser']);
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $modelBooking = BridgeHelper::getBooking()->getBookingModel();
        $row->active = trans('entity::entity.statuses.' . $row->active);
        $row->entity_id = $row->id;
        $row->open_user_bookings_count = $modelBooking->active()->where("model_id", $row->entity_id)->where("model_type", $row->getTable())->isOpenUser()->count();
        $row->open_entity_bookings_count = $modelBooking->active()->where("model_id", $row->entity_id)->where("model_type", $row->getTable())->isPendEntity()->count();
        $row->category = $row->entityCategory ? $row->entityCategory->title : '-';
        $row->user = $row->entityOwnerUser ? $row->entityOwnerUser->user->full_name : '-';
        $row->created_at = $row->created_at ? DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium')) : null;
        $row->updated_at = $row->updated_at ? DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium')) : null;
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
//        dd($action, $row);
//        if ($row->published) {
//            $action['publish']->icon = 'fa fa-eye';
//        } else {
//            $action['publish']->icon = 'fa fa-eye-slash';
//        }
        return $action;

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getListGrid()
    {
        $this->setTitlePage(trans('entity::entity.list'));
        $this->generateGridList()->renderedView("entity::entity.index", ['view_model' => $this], "entity_list");
        return $this;
    }


}
