<?php

namespace Modules\Entity\ViewModels\Entity\Front;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Category\EntityCategoryRelation;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\Models\Service\EntityServices;

class EntityServiceViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @param bool $json
     * @param bool $parent
     * @param bool $decorate
     * @return $this
     * @throws \Throwable
     */
    public function listServices($json = false, $parent = false, $decorate = false)
    {
        $services = EntityServices::active();
        $parent = $parent ?: $this->request->get("parent");
        $decorate = $decorate ?: $this->request->get("decorate");
        if ($entity_id = $this->request->get("entity_id")) {
            $services = $services->with(['childes.entityRelationServices' => function ($q) use ($entity_id) {
                $q->where('entity_id', $entity_id)->active();
            }, 'parent'])->has('childes.entityRelationServices');
        }

        if ($decorate){
            $services = $services->whereNull("parent_id")->with(['childes' => function ($q) {
                $q->active();
            }])->has('childes');
        }

        $services = $services->where(function ($q) use ($parent) {
            if ($parent) {
                $q->whereNull("parent_id");
            }
        })->get();

        if ($entity_id) {
            $services = $this->decorateServiceEntity($services);
        }

        if ($json) {
            return $services;
        }

        $this->setTitlePage(trans('entity::entity.services.show'));
        if (get_instance()->isAjax()) {
            $services = $this->decorateList($services);
            return $this->setDataResponse($services)->setResponse(true);
        }
        $this->setModelData($services);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.service.form_service", ['view_model' => $viewModel], "view");

    }

    /**
     * @param $services
     * @return static
     */
    private function decorateServiceEntity($services)
    {
        $services = collect($services)->filter(function ($service){
            $childes = $service->childes;
            $childes = collect($childes)->filter(function ($row){
                if ($row->entityRelationServices->isNotEmpty()){
                    return $row;
                }
            });
            if ($childes->isNotEmpty()){
                $service->childes = $childes;
                return $service;
            }
        });
        return $services;
    }

    private function decorateService($services)
    {
        $services = collect($services)->filter(function ($service){
            $childes = $service->childes;
            $childes = collect($childes)->filter(function ($row){
                if ($row->entityRelationServices->isNotEmpty()){
                    return $row;
                }
            });
            if ($childes->isNotEmpty()){
                $service->childes = $childes;
                return $service;
            }
        });
        return $services;
    }

    /**
     * @param null $entity_service_id
     * @param bool $json
     * @return $this
     * @throws \Throwable
     */
    protected function listEntities($entity_service_id = null, $json = false)
    {
        $entity_service_id = $entity_service_id ? $entity_service_id : $this->request->get("entity_service_id");
        $services = \Modules\Entity\Models\Service\EntityRelationServices::active()->where("entity_service_id", $entity_service_id)->select("entity_id")
            ->join("entities", "entities.id", '=', 'entity_relation_services.entity_id')
            ->where("entities.active", 1)
            ->whereNull("entities.deleted_at")
            ->distinct('entity_id')->paginate();
        $entity_ids = $services->pluck("entity_id")->toArray();

        $entities = Entity::active()->whereIn('id', $entity_ids)->get();
        $entities = $services->setCollection($entities);

        $this->setTitlePage(trans('entity::entity.services.list_entities'));
        if (get_instance()->isAjax() || $json) {
            $entities = $this->decorateList($entities);
            return $this->setDataResponse($entities)->setResponse(true);
        }
        $this->setModelData($entities);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.service.form_service", ['view_model' => $viewModel], "view");

    }

    /**
     * @return $this
     */
    public function getEntityServicesByApi()
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }

        $model = new EntityServices();
        $model = $model->enable()->filterLanguage()->filterCurrentInstance()
            ->where(function ($q) {
                if ($this->request->get('_root')) {
                    $q->whereNull('parent_id');
                } elseif ($this->request->get('_childes')) {
                    $q->whereNotNull('parent_id');
                }
            })
            ->select('id', DB::raw('title as text'));

        if ($this->request->get('_childes')) {
            $model = $model->orderBy('parent_id');
        }

        if ($q) {

            $model->where(function ($query) use ($q) {
                $query->where('title', 'LIKE', '%' . $q . '%');
            });
            $items = $model->get();

        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $items = $this->request->has('selected') ? [] : $model->orderBy('id', 'DESC')->take(20)->get();
        }

        return $this->setDataResponse($items)->setResponse(true);
    }


}
