<?php

namespace Modules\Entity\ViewModels\Entity\Front;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Category\EntityCategoryRelation;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\Models\Service\EntityServices;

class EntityViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity_view_model;
    private $entities_location = [];

    public function __construct()
    {
        $this->entity_view_model = new \Modules\Entity\ViewModels\Entity\EntityViewModel();
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {
            $access_assets[] = ['container' => 'theme_js', 'src' => ("assets/js/entity/entity.js"), 'name' => 'entity-js'];
        }

        return $access_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function showEntity()
    {
        $this->access_assets = true;
        $model = new Entity();
        $model = $model->active()
//            ->filterLanguage()
            ->createRelationsWithFilter(['entitySliderMedias'
                , 'entityCategory', 'entityEmployees.user'
                , 'entityCategories', 'entityContacts', 'entityAddress'])
            ->find($this->request->get('entity_id'));

        $serviceViewModel = new EntityServiceViewModel();

        if (!$model) {
            return $this->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }
        $services = $serviceViewModel->setRequest($this->request)->listServices(true, true);

        $model->entity_services = $services;
        $cart = BridgeHelper::getCartHelper()->getCurrentCart(false)->getCart();
        $model->user_cart = $cart;

        $this->setTitlePage(trans('entity::entity.show'));
        if (get_instance()->isAjax()) {
            $model = $this->decorateAttributes($model);
            $model['user_cart'] = $cart;
            return $this->setDataResponse($model)->setResponse(true);
        }

        if ($model) {
            $model->count_access_add_slider_images = $this->entity_view_model->getCountOfImages();
        }

        $this->setRelationalData("entity", $model);
        $this->setModelData($model);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.view", ['view_model' => $viewModel], "view");
    }

    /**
     * @param null $entity_id
     * @param bool $json
     * @return $this
     * @throws \Throwable
     */
    public function showServicesOfEntity($entity_id = null, $json = false)
    {
        $services = EntityServices::active()->with(['entityRelationServices' => function ($q) use ($entity_id) {
            $entity_id = $entity_id ?: $this->request->get('entity_id');
            $q->where('entity_id', $entity_id)->active()->with('entityMedias', 'bookingCalendar', 'entityEmployeeServices.entityEmployee.user');
        }])->has('entityRelationServices')->get();

        $this->setTitlePage(trans('entity::entity.show'));
        if (get_instance()->isAjax() || $json) {
            $services = $this->decorateList($services);
            return $this->setDataResponse($services)->setResponse(true);
        }
        $this->setModelData($services);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.view", ['view_model' => $viewModel], "view");
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function listEntities()
    {
        $this->layout = '1ColumnWithHead';
        $this->access_assets = true;
        $this->setPerPage($this->request->get('per_page', 10));
        $model = new Entity();
        $entities = $model->setRequestItems(request()->all())
            ->filterCurrentInstance()
//            ->filters()
            ->filterByCategories()
            ->filterEntity()
            ->filterByAddress()
            ->with(['entityAddress', 'entityCategories', 'entityImageMedia'])
            //->select('*', DB::raw('calc_distance_point(' . request()->get('latitude',35.73579000) . ',' . request()->get('longitude',51.41266000) . ',location,' . 0 . ',null,null) as dist'))
            ->simplePaginate($this->perPage);

        $entities = $entities->appends(request()->except("language_id"));
        $this->setTitlePage(trans('entity::entity.list'));
        if (get_instance()->isAjax()) {
            $entities = $this->decorateList($entities);
            return $this->setDataResponse($entities)->setResponse(true);
        }

        $entities = $this->decorateEntities($entities);

        $this->setModelData($entities);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.list", ['view_model' => $viewModel], "view");

    }

    /**
     * @param $list
     * @return mixed
     */
    public function decorateEntities($list)
    {
        $entities = $list->getCollection();

        $entities = collect($entities)->map(function ($row) {
            $dist = null;
            if (($latitude = request()->get('user_latitude')) && ($longitude = request()->get('user_longitude'))) {
                $dist = (DB::select("select calc_distance_point ( " . $latitude . "," . $longitude . ", " . DB::raw('(select location from entity_addresses where entity_id=' . $row->id . ' and is_default=1 and active=1 and deleted_at is null order by id desc limit 1)') . ", 0,null,null ) AS dist"));
                $dist = array_first($dist);
            }

            $row->dist = isset($dist->dist) ? $dist->dist : null;
            $row->unit = null;

            if ($row->entityAddress) {
                $row->entities_location = [
                    'latitude' => $row->entityAddress->latitude,
                    'longitude' => $row->entityAddress->longitude,
                ];
            }

            if ($row->dist) {
                if ($row->dist > 1000) {
                    $row->dist = number_format($row->dist / 1000, 2);
                    $row->unit = trans('entity::entity.address.units.km');
                } else {
                    $row->unit = trans('entity::entity.address.units.m');
                }
            }


            return $row;
        });

        $list->setCollection($entities);

        return $list;
    }

}
