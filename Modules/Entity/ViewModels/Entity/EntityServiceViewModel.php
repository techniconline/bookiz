<?php

namespace Modules\Entity\ViewModels\Entity;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Service\EntityServices;

class EntityServiceViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createService()
    {
        $this->access_assets = true;
        $model = new EntityServices();
        $model = $model->find($this->request->get('entity_service_id'));
        $this->setModelData($model);
        $viewModel =& $this;

        $this->setTitlePage(trans('entity::entity.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("entity::service.form_service", ['view_model' => $viewModel], "form");
    }

    /**
     * @return $this
     */
    protected function destroyService()
    {
        $entity_service_id = $this->request->get('entity_service_id');
        $entity_service = EntityServices::enable()->find($entity_service_id);

        if (!$entity_service) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        if ($entity_service->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveService()
    {
        $this->request = $this->requestValuesUpdate();
        $data = $this->request->all();
        $response = $this->serviceSaveEntityService($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('entity.service.edit', ['entity_service_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }


    /**
     * @param $data
     * @return array
     */
    public function serviceSaveEntityService($data)
    {
        try {
            $model = new EntityServices();
            $entity_service_id = isset($data['entity_service_id']) ? $data['entity_service_id'] : 0;
            if ($entity_service_id) {
                $model = $model->find($entity_service_id);
                if (!$model) {
                    return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
                }
            }

            if (!$data["slug"]) {
                $data['slug'] = create_alias($data["title"]);
            }

            $data['instance_id'] = get_instance()->getCurrentInstanceId();

            if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
                $model->fill($data);
                if ($model->save()) {
                    return ['action' => true, 'message' => trans("entity::messages.alert.save_success"), 'data' => $model];
                } else {
                    return ['action' => false, 'message' => trans("entity::messages.alert.save_un_success")];
                }
            }
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors' => $this->errors];
        } catch (\Exception $exception) {
            report($exception);
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data")];
        }
    }


}
