<?php

namespace Modules\Entity\ViewModels\Entity;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Code\Libraries\Uploader\UploaderLibrary;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Category\EntityCategoryRelation;
use Modules\Entity\Models\Entity;

class EntityMediaViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;

    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }


    /**
     * @return $this
     */
    protected function setAssetsCreateEntity()
    {
        $this->access_assets = true;
        return $this;
    }

    /**
     * @return $this
     */
    protected function uploadMedia()
    {
        $model_type = $this->request->get('model_type');
        $model_id = $this->request->get('model_id');
        $type = $this->request->get('type');
        $modelUsed = $this->getMediaUseModel($model_type, $model_id);

        if (!$modelUsed) {
            return $this->setResponse(false, trans("video::messages.alert.not_find_data"));
        }

        $method = 'upload';
        $method .= get_uppercase_by_underscores($type);
        $method .= 'Service';
        if (method_exists($this, $method)) {
            $response = $this->$method($model_type, $model_id);

            if ($response->response['action']) {
                $result = isset($response->response['result_items']) ? $response->response['result_items'] : null;
                if (!$result) {
                    return $this->setResponse(false, trans("video::messages.alert.not_find_data"));
                }

                if ($model_type == 'entity_relation_service') {
                    $entity_id = $modelUsed->entity_id;
                    $entity_relation_service_id = $model_id;
                } else {
                    $entity_id = $model_id;
                    $entity_relation_service_id = null;
                }

                $data = [
                    'entity_id' => $entity_id,
                    'entity_relation_service_id' => $entity_relation_service_id,
                    'type' => $type,
                    'active' => 1,
                    'src' => $result['src'],
                    'description' => $this->request->get('description'),
                    'sort' => $this->request->get('sort', 1)
                ];
                $modelEntityMedia = new Entity\EntityMedias();
                if ($modelEntityMedia->fill($data)->save()) {
                    return $response;
                } else {
                    return $this->setResponse(false, trans("video::messages.alert.not_find_data"));
                }

            }

        }
        return $this->setResponse(false, trans("video::messages.alert.not_find_data"));
    }

    /**
     * @param $model_type
     * @param $model_id
     * @return $this
     */
    private function uploadVideoService($model_type, $model_id)
    {
        $modelEntityMedia = new Entity\EntityMedias();
        $libUploader = new UploaderLibrary();
        $folderUpload = $modelEntityMedia->getTable() . DIRECTORY_SEPARATOR;
        $folderUpload .= $model_type . DIRECTORY_SEPARATOR . $model_id . DIRECTORY_SEPARATOR;
        $folderUpload .= "origin";

        $result = $libUploader->setStorage($libUploader::STORAGE_NFS_PUBLIC)->setFolderUploadName($folderUpload)
            ->setFileName(md5(time()))
            ->setTypeFile('video')
            ->setUrlUpload(null, true)->upload();

        $exist = false;
        if (isset($result['action']) && $result['action']) {
            if (isset($result['items']['extension']) && $result['items']['extension']) {
                //$video->extension = $result['items']['extension'];
            }

            $src = $result['items']['src'] ? $result['items']['src'] : null;
            //$srcDestination = $result['items']['destination'] ? $result['items']['destination'] : null;
            $exist = Storage::disk($libUploader::STORAGE_NFS_PUBLIC)->exists($src);

        }

        return $this->setDataResponse($result)->setResponse(boolval($exist), isset($result['message']) ? $result['message'] : null
            , isset($result['errors']) ? $result['errors'] : null, isset($result['items']) ? $result['items'] : null);
    }

    /**
     * @param $model_type
     * @param $model_id
     * @return $this
     */
    private function uploadImageService($model_type, $model_id)
    {
        $modelEntityMedia = new Entity\EntityMedias();
        $libUploader = new UploaderLibrary();
        $folderUpload = $modelEntityMedia->getTable() . DIRECTORY_SEPARATOR;
        $folderUpload .= $model_type . DIRECTORY_SEPARATOR . $model_id . DIRECTORY_SEPARATOR;
        $folderUpload .= "origin";
        $result = [];

        try{

            $result = $libUploader->setStorage($libUploader::STORAGE_NFS_PUBLIC)->setFolderUploadName($folderUpload)
                ->setFileName(md5(time()))
                ->setFileInputName('file')
                ->setTypeFile('image')
                ->setUrlUpload(null, true)
                ->setMaxImageSize(2048, 800)
                ->setWidth(1920)
                ->setHeight(600)
                ->setConvertFormat('jpg', 80)
                ->createThumbnail()
                ->resizeUpload();

        }catch (\Exception $exception){
            report($exception);
        }

        if (isset($result['action']) && $result['action']) {
//            $video->has_poster = 1;
//            $video->save();
        }

        return $this->setDataResponse($result)->setResponse(isset($result['action']) ? $result['action'] : null, isset($result['message']) ? $result['message'] : null
            , isset($result['errors']) ? $result['errors'] : null, isset($result['items']) ? $result['items'] : null);
    }

    /**
     * @param $model_type
     * @param $model_id
     * @return bool
     */
    public function getMediaUseModel($model_type, $model_id)
    {
        $model = get_uppercase_by_underscores($model_type);

        if ($model) {
            $modelEntityMedia = new Entity\EntityMedias();
            $model = 'get' . $model . 'Model';
            if (method_exists($modelEntityMedia, $model)) {
                $model = $modelEntityMedia->$model();
                return $model->enable()->find($model_id);
            }
        }
        return false;
    }

    /**
     * @return $this
     */
    protected function destroyEntityMedia()
    {
        $entity_media_id = $this->request->get('entity_media_id');
        $entity_media = Entity\EntityMedias::enable()->find($entity_media_id);

        if ($entity_media->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }


}
