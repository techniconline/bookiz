<?php

namespace Modules\Entity\ViewModels\Entity;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\Models\Service\EntityServices;

class EntityCalendarViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createCalendar()
    {
        $this->access_assets = true;
        $model = BridgeHelper::getBooking()->getBookingCalendarModel();
        $model = $model->find($this->request->get('booking_calendar_id'));

        if ($model) {
            $entityRelationService = EntityRelationServices::enable()->with('entity')->find($model->model_id);
            $model->entity_id = isset($entityRelationService->entity) && $entityRelationService->entity ? $entityRelationService->entity->id : null;
            $model->entity_relation_service_id = isset($entityRelationService->id) ? $entityRelationService->id : null;
        }

        $this->setModelData($model);
        $viewModel =& $this;

        $this->setTitlePage(trans('entity::entity.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("entity::calendar.form_calendar", ['view_model' => $viewModel], "form");
    }

    /**
     * @return $this
     */
    protected function destroyCalendar()
    {
        $vmmodel = BridgeHelper::getBooking()->getBookingCalendarViewModel();
        return $vmmodel->setRequest($this->request)->destroyCalendar(false);
    }

    /**
     * @return $this
     */
    protected function saveCalendar()
    {
        $this->request = $this->requestValuesUpdate();
        $entity_relation_service_id = $this->request->get("entity_relation_service_id");
        $entityRelationService = EntityRelationServices::enable()->find($entity_relation_service_id);

        if (!$entityRelationService) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        $this->request->offsetUnset('entity_relation_service_id');
        $this->request->offsetUnset('entity_id');

        $this->request->request->add(["model_id" => $entity_relation_service_id, 'model_type' => $entityRelationService->getTable()]);
        $data = $this->request->all();
        $vmmodel = BridgeHelper::getBooking()->getBookingCalendarViewModel();
        $response = $vmmodel->setRequest($this->request)->serviceSaveCalendar($data, false);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('entity.calendar.edit', ['booking_calendar_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

}
