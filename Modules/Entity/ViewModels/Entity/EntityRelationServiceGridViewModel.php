<?php

namespace Modules\Entity\ViewModels\Entity;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;

class EntityRelationServiceGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $filter_items = [];
    private $access_assets = false;

    public function __construct()
    {
        $this->useModel = new EntityRelationServices();
    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->filter_items = $params ? $params : request()->all();
        return $this;
    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->init();
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->filters()->enable()->with(['entity', 'entityService']);
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        //use bested assets
        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }


    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('entity::entity.services.title'), true);
        $this->addColumn('entity', trans('entity::entity.services.relation.title_entity'), false);
        $this->addColumn('service', trans('entity::entity.services.relation.title_service'), false);
        $this->addColumn('price', trans('entity::entity.services.relation.price'), true);
        $this->addColumn('discount', trans('entity::entity.services.relation.discount'), true);
        $this->addColumn('created_at', trans('entity::entity.created_at'), false);
        $this->addColumn('updated_at', trans('entity::entity.updated_at'), false);
        $this->addColumn('active', trans('entity::entity.status'), false);

        /*add action*/
        $add = array(
            'name' => 'entity.service.relation.create',
            'parameter' => ["entity_id" => ['value' => $this->request->get("entity_id")]]
        );
        $this->addButton('create_entity_service', $add, trans('entity::entity.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        /*add action*/
        $add = array(
            'name' => 'entity.index',
            'parameter' => null
        );
        $this->addButton('list_entity', $add, trans('entity::entity.list'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);


        $show = array(
            'name' => 'entity.service.relation.edit',
            'parameter' => ['id']
        );
        $this->addAction('content_edit', $show, trans('entity::entity.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('entity::entity.confirm_delete')];
        $delete = array(
            'name' => 'entity.service.relation.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('entity::entity.delete'), 'fa fa-trash', false, $options);

        /**/
        $this->action_text = false;

        /*filter*/
        $this->addFilter('title', 'text');
        $this->addFilter('service', 'text', ['relation' => 'entityService']);
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->active = trans('entity::entity.statuses.' . $row->active);
        $row->entity = $row->entity ? $row->entity->title : '-';
        $row->service = $row->entityService ? $row->entityService->title : '-';
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
//        if ($row->published) {
//            $action['publish']->icon = 'fa fa-eye';
//        } else {
//            $action['publish']->icon = 'fa fa-eye-slash';
//        }
        return $action;

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getRelationServiceListGrid()
    {
        $this->setTitlePage(trans('entity::entity.services.relation.list'));
        $this->generateGridList()->renderedView("entity::entity.index", ['view_model' => $this], "entity_relation_service_list");
        return $this;
    }


}
