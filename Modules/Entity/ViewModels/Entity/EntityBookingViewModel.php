<?php

namespace Modules\Entity\ViewModels\Entity;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Service\EntityServices;

class EntityBookingViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }


    /**
     * @return $this
     */
    protected function destroyBooking()
    {
        $model = BridgeHelper::getBooking()->getBookingModel();
        $booking_id = $this->request->get('booking_id');
        $booking = $model->enable()->find($booking_id);
        if (!$booking) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.not_find_data"));
        }

        if ($booking->delete()) {
            $vModel = BridgeHelper::getBooking()->getBookingViewModel();
            $bookingDetails = BridgeHelper::getBooking()->getBookingDetailsModel()->enable()->where("booking_id", $booking->id)->get();
            foreach ($bookingDetails as $bookingDetail) {
                $vModel->removeBookingCalendarDetails($bookingDetail);
                $bookingDetail->delete();
            }
            return $this->setResponse(true, trans("booking::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("booking::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function destroyBookingDetails()
    {
        $booking_details_id = $this->request->get('booking_details_id');
        $model = BridgeHelper::getBooking()->getBookingDetailsModel()->find($booking_details_id);
        $vModel = BridgeHelper::getBooking()->getBookingViewModel();
        if ($model) {
            if ($model->delete()){
                $vModel->removeBookingCalendarDetails($model);
                return $this->setResponse(true, trans("booking::messages.alert.del_success"));
            }
            return $this->setResponse(true, trans("booking::messages.alert.del_un_success"));
        }
        return $this->setResponse(false, trans("booking::messages.alert.not_find_data"));

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function showBookingDetails()
    {

        $booking_details_id = $this->request->get('booking_details_id');
        $this->modelData = BridgeHelper::getBooking()->getBookingDetailsModel()->find($booking_details_id);
        if ($booking_details_id && !$this->modelData) {
            return $this->redirectBack()->setResponse(false, trans('entity::message.alert.not_find_data'));
        }
        $BlockViewModel=&$this;
        return $this->setDataResponse(view('entity::booking.details',compact('BlockViewModel'))->render(), "html")->setResponse(true);
    }


}
