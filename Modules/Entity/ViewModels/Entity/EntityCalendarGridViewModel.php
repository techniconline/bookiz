<?php

namespace Modules\Entity\ViewModels\Entity;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;

class EntityCalendarGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $filter_items = [];
    private $access_assets = false;

    public function __construct()
    {
        $this->useModel = BridgeHelper::getBooking()->getBookingCalendarModel();
    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->filter_items = $params ? $params : request()->all();
        return $this;
    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->init();
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->filters()->enable()->with(['entityRelationService.entity']);
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        //use bested assets
        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }


    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title_entity', trans('entity::entity.calendars.title_entity'), false);
        $this->addColumn('title', trans('entity::entity.calendars.title_service'), false);
        $this->addColumn('for_gender', trans('entity::entity.calendars.for_gender'), true);
        $this->addColumn('start_date', trans('entity::entity.calendars.start_date'), true);
        $this->addColumn('end_date', trans('entity::entity.calendars.end_date'), true);
        $this->addColumn('days_active_reserve', trans('entity::entity.calendars.days_active_reserve'), true);
        $this->addColumn('count_reservation_in_time', trans('entity::entity.calendars.count_reservation_in_time'), true);
        $this->addColumn('period', trans('entity::entity.calendars.period'), true);
        $this->addColumn('created_at', trans('entity::entity.created_at'), false);
        $this->addColumn('updated_at', trans('entity::entity.updated_at'), false);
        $this->addColumn('active', trans('entity::entity.status'), false);

        /*add action*/
        $add = array(
            'name' => 'entity.calendar.create',
            'parameter' => ["entity_id" => ['value' => $this->request->get("entity_id")]]
        );
        $this->addButton('create_entity_service', $add, trans('entity::entity.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        /*add action*/
        $add = array(
            'name' => 'entity.index',
            'parameter' => null
        );
        $this->addButton('list_entity', $add, trans('entity::entity.list'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);


        $show = array(
            'name' => 'entity.calendar.edit',
            'parameter' => ['id']
        );
        $this->addAction('content_edit', $show, trans('entity::entity.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('entity::entity.confirm_delete')];
        $delete = array(
            'name' => 'entity.calendar.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('entity::entity.delete'), 'fa fa-trash', false, $options);

        /**/
        $this->action_text = false;

        /*filter*/
//        $this->addFilter('title', 'text', ['relation' => 'entityRelationService']);
//        $this->addFilter('title_entity', 'text', ['relation' => 'entity']);
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->active = trans('entity::entity.statuses.' . $row->active);
        $row->title_entity = isset($row->entityRelationService->entity->title) ? $row->entityRelationService->entity->title : '-';
        $row->title = $row->entityRelationService ? $row->entityRelationService->title : '-';
        $row->start_date = DateHelper::setDateTime($row->start_date)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->end_date = DateHelper::setDateTime($row->end_date)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
//        if ($row->published) {
//            $action['publish']->icon = 'fa fa-eye';
//        } else {
//            $action['publish']->icon = 'fa fa-eye-slash';
//        }
        return $action;

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getCalendarListGrid()
    {
        $this->setTitlePage(trans('entity::entity.calendars.list'));
        $this->generateGridList()->renderedView("entity::entity.index", ['view_model' => $this], "entity_calendar_list");
        return $this;
    }


}
