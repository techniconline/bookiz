<?php

namespace Modules\Entity\ViewModels\Entity\Owner;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Category\EntityCategoryRelation;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\Models\Service\EntityServices;

class EntityServiceViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function showServices()
    {
        $model = new Entity();
        $model = $model->enable()->isOwner()->find($this->request->get('entity_id'));

        if (!$model) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        $services = EntityServices::active()->with(['entityRelationServices'=>function($q)use($model){
            $q->where('entity_id',$model->id)->where(function ($q){
                if ($entity_relation_service_ids = $this->request->get("entity_relation_service_ids")){
                    $q->whereIn('id', $entity_relation_service_ids);
                }
            })->active()->with('bookingCalendar','entityEmployeeServices.entityEmployee.user');
        }])->has('entityRelationServices')->get();
//        $services = EntityRelationServices::active()->where('entity_id', $model->id)->with('entityService','bookingCalendar','entityEmployeeServices.entityEmployee.user')->get();
        $services = $this->decorateList($services);

        $services = $services->filter(function ($item){
            if ($item['entity_relation_services']->isNotEmpty()){
                return $item;
            }
        })->values();

        $this->setTitlePage(trans('entity::entity.services.show'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($services)->setResponse(true);
        }
        $this->setModelData($services);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.service.form_service", ['view_model' => $viewModel], "view");

    }

    /**
     * @return $this
     */
    protected function destroyService()
    {
        $entity_relation_service_id = $this->request->get('entity_relation_service_id');
        $entity_relation_service = EntityRelationServices::enable()->find($entity_relation_service_id);

        if (!$entity_relation_service) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        $entity = Entity::enable()->isOwner()->find($entity_relation_service->entity_id);

        if (!$entity) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        if ($entity_relation_service->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveService()
    {
        $data = $this->request->all();
        $response = $this->serviceSaveEntityService($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('entity.edit', ['entity_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }


    /**
     * @param $data
     * @return array
     */
    public function serviceSaveEntityService($data)
    {
        try{
            $model = new EntityRelationServices();
            $entity_relation_service_id = isset($data['entity_relation_service_id']) ? $data['entity_relation_service_id'] : 0;
            if ($entity_relation_service_id) {
                $model = $model->find($entity_relation_service_id);
                if (!$model) {
                    return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
                }
                $entity_id = $model->entity_id;
            } else {
                $entity_id = isset($data['entity_id']) ? $data['entity_id'] : 0;
            }

            $instance = get_instance()->getCurrentInstance();
            if (empty($data['currency_id']) && $instance){
                $data['currency_id'] = $instance->currency_id;
            }

            $entity = Entity::enable()->isOwner()->find($entity_id);
            if (!$entity) {
                return ['action' => false, 'message' => trans("entity::messages.alert.access_error")];
            }


            if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
                $model->fill($data);
                if ($model->save()) {
                    return ['action' => true, 'message' => trans("entity::messages.alert.save_success"), 'data' => $model];
                } else {
                    return ['action' => false, 'message' => trans("entity::messages.alert.save_un_success")];
                }
            }
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors' => $this->errors];
        }catch (\Exception $exception){
            report($exception);
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data")];
        }
    }

}
