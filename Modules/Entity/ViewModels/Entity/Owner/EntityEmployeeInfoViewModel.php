<?php

namespace Modules\Entity\ViewModels\Entity\Owner;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Category\EntityCategoryRelation;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\Models\Service\EntityServices;
use Modules\User\ViewModels\Manage\User\CreateViewModel;

class EntityEmployeeInfoViewModel extends BaseViewModel
{

    private $access_assets;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @param bool $check_access_owner
     * @return $this
     */
    public function destroyEmployeeInfo($check_access_owner = true)
    {
        $entity_employee_id = $this->request->get('entity_employee_id');
        $entity_employee = Entity\EntityEmployee::enable()->find($entity_employee_id);
        $employeeInfo = Entity\EntityEmployeeInfo::enable()->where('entity_employee_id', $entity_employee_id)->first();

        if (!$entity_employee || !$employeeInfo) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        if ($check_access_owner){
            $entity = Entity::enable()->isOwner()->find($entity_employee->entity_id);
            if (!$entity) {
                return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
            }
        }

        if ($employeeInfo->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }


    /**
     * @param bool $json
     * @param bool $check_access_owner
     * @return $this|array
     * @throws \Throwable
     */
    public function getEmployeeInfo($json = false, $check_access_owner = true)
    {
        $entity_employee_id = $this->request->get('entity_employee_id');
        $entity_employee = Entity\EntityEmployee::enable()->find($entity_employee_id);

        if (!$entity_employee) {
            if ($json){
                return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
            }
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        if ($check_access_owner){
            $entity = Entity::enable()->isOwner()->find($entity_employee->entity_id);
            if (!$entity) {
                if ($json){
                    return ['action' => false, 'message' => trans("entity::messages.alert.access_error")];
                }
                return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
            }
        }

        $employeeInfo = Entity\EntityEmployeeInfo::active()->with('entityEmployee.user')->where('entity_employee_id', $entity_employee_id)->first();

        if ($json){
            return ['action' => true, 'data' => $employeeInfo];
        }

        $employeeInfo = $this->decorateAttributes($employeeInfo);
        $this->setTitlePage(trans('entity::entity.employees.show_info'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($employeeInfo)->setResponse(true);
        }
        $this->setModelData($employeeInfo);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.employee.form_employee_info", ['view_model' => $viewModel], "view");

    }


    /**
     * @return $this
     */
    protected function saveEmployeeInfo()
    {
        $this->request = $this->requestValuesUpdate([
            'certificate_end_date' => 'Y-m-d', 'certificate_start_date' => 'Y-m-d',
            'driving_license_end_date' => 'Y-m-d', 'driving_license_start_date' => 'Y-m-d',
            'stay_end_date' => 'Y-m-d', 'stay_start_date' => 'Y-m-d',
            'passport_end_date' => 'Y-m-d', 'passport_start_date' => 'Y-m-d',
        ]);
        $data = $this->request->all();
        $response = $this->serviceSaveEmployeeInfo($data);
        if ($response['action']) {
            return $this->redirectBack()->setDataResponse($response['data'])->setResponse(true, trans("entity::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, $response['message'], isset($response['errors']) ? $response['errors'] : null);
    }

    /**
     * @param $data
     * @param bool $check_access_owner
     * @return array
     */
    public function serviceSaveEmployeeInfo($data, $check_access_owner = true)
    {
        $entity_employee_id = isset($data['entity_employee_id']) ? $data['entity_employee_id'] : 0;
        $entity_employee = Entity\EntityEmployee::enable()->find($entity_employee_id);

        if (!$entity_employee) {
            return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
        }

        if ($check_access_owner) {
            $entity = Entity::enable()->isOwner()->find($entity_employee->entity_id);
            if (!$entity) {
                return ['action' => false, 'message' => trans("entity::messages.alert.access_error")];
            }
        }

        $employeeInfo = Entity\EntityEmployeeInfo::enable()->where('entity_employee_id', $entity_employee_id)->first();
        if (!$employeeInfo) {
            $employeeInfo = new Entity\EntityEmployeeInfo();
        }

        $this->isValidRequest($data, $employeeInfo->rules);
        if (!$this->isValidRequest) {
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors' => $this->errors];
        }

        if ($employeeInfo->fill($data)) {
            $employeeInfo->save();
            return ['action' => true, 'message' => trans("entity::messages.alert.save_success"), 'data' => $employeeInfo];
        }
        return ['action' => false, 'message' => trans("entity::messages.alert.save_un_success")];

    }

    /**
     * @param bool $check_access_owner
     * @return $this
     */
    public function destroyEmployeeExperienceInfo($check_access_owner = true)
    {
        $entity_employee_experience_id = $this->request->get('entity_employee_experience_id');
        $employeeExperienceInfo = Entity\EntityEmployeeExperienceInfo::enable()->find($entity_employee_experience_id);

        if (!$employeeExperienceInfo) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        $entity_employee_id = $employeeExperienceInfo->entity_employee_id;
        $entity_employee = Entity\EntityEmployee::enable()->find($entity_employee_id);

        if (!$entity_employee) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        if ($check_access_owner){
            $entity = Entity::enable()->isOwner()->find($entity_employee->entity_id);
            if (!$entity) {
                return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
            }
        }


        if ($employeeExperienceInfo->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }


    /**
     * @param bool $json
     * @param bool $check_access_owner
     * @return $this|array
     * @throws \Throwable
     */
    public function getEmployeeExperienceInfo($json = false, $check_access_owner = true)
    {
        $entity_employee_id = $this->request->get('entity_employee_id');
        $entity_employee = Entity\EntityEmployee::enable()->find($entity_employee_id);

        if (!$entity_employee) {
            if ($json){
                return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
            }
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        if ($check_access_owner){
            $entity = Entity::enable()->isOwner()->find($entity_employee->entity_id);
            if (!$entity) {
                if ($json){
                    return ['action' => false, 'message' => trans("entity::messages.alert.access_error")];
                }
                return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
            }
        }

        $employeeExperienceInfo = Entity\EntityEmployeeExperienceInfo::active()->where('entity_employee_id', $entity_employee_id)->get();

        if ($json){
            return ['action' => true, 'data' => $employeeExperienceInfo];
        }

        $employeeExperienceInfo = $this->decorateList($employeeExperienceInfo);
        $this->setTitlePage(trans('entity::entity.employees.show_experience_info'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($employeeExperienceInfo)->setResponse(true);
        }

        $this->setModelData($employeeExperienceInfo);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.employee.form_employee_experience_info", ['view_model' => $viewModel], "view");

    }


    /**
     * @return $this
     */
    protected function saveEmployeeExperienceInfo()
    {

        $this->request = $this->requestValuesUpdate([
            'certificate_end_date' => 'Y-m-d', 'certificate_start_date' => 'Y-m-d',
        ]);

        $data = $this->request->all();
        $response = $this->serviceEmployeeExperienceInfo($data);
        if ($response['action']) {
            return $this->redirectBack()->setDataResponse($response['data'])->setResponse(true, trans("entity::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, $response['message'], isset($response['errors']) ? $response['errors'] : null);
    }

    /**
     * @param $data
     * @param bool $check_access_owner
     * @return array
     */
    public function serviceEmployeeExperienceInfo($data, $check_access_owner = true)
    {
        $entity_employee_id = isset($data['entity_employee_id']) ? $data['entity_employee_id'] : 0;
        $entity_employee = Entity\EntityEmployee::enable()->find($entity_employee_id);

        if (!$entity_employee) {
            return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
        }

        if ($check_access_owner) {
            $entity = Entity::enable()->isOwner()->find($entity_employee->entity_id);
            if (!$entity) {
                return ['action' => false, 'message' => trans("entity::messages.alert.access_error")];
            }
        }

        $employeeExperienceInfo = new Entity\EntityEmployeeExperienceInfo();
        $this->isValidRequest($data, $employeeExperienceInfo->rules);
        if (!$this->isValidRequest) {
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors'=>$this->errors];
        }

        if ($employeeExperienceInfo->fill($data)) {
            $employeeExperienceInfo->save();
            return ['action' => true, 'message' => trans("entity::messages.alert.save_success"), 'data'=>$employeeExperienceInfo];
        }
        return ['action' => false, 'message' => trans("entity::messages.alert.save_un_success")];
    }

}
