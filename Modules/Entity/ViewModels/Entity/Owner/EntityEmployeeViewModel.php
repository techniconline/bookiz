<?php

namespace Modules\Entity\ViewModels\Entity\Owner;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Category\EntityCategoryRelation;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\Models\Service\EntityServices;
use Modules\User\ViewModels\Manage\User\CreateViewModel;

class EntityEmployeeViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @param bool $json
     * @param null $entity_id
     * @param bool $by_service
     * @param array $entity_relation_service_ids
     * @return array|\Illuminate\Support\Collection|EntityEmployeeViewModel
     * @throws \Throwable
     */
    public function showEmployees($json = false, $entity_id = null, $by_service = false, $entity_relation_service_ids =[])
    {
        $model = new Entity();
        $model = $model->enable()->isOwner()->find($this->request->get('entity_id',$entity_id));

        if (!$model) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        $decorateByService = $this->request->get("by_service",$by_service);

        $employees = Entity\EntityEmployee::active()->with(['user', 'entityEmployeeServices.entityRelationService' => function ($q) use ($entity_relation_service_ids){
            if ($entity_relation_service_ids = $this->request->get("entity_relation_service_ids",$entity_relation_service_ids)) {
                $q->whereIn('entity_relation_services.id', $entity_relation_service_ids);
            }
        }])->where('entity_id', $model->id)->get();

        $this->setTitlePage(trans('entity::entity.employees.show'));
        if (get_instance()->isAjax() || $json) {
            $employees = $this->decorateList($employees);
            if ($decorateByService && $employees) {
                $employees = $this->decorateByService($employees);
            }
            if ($json){
                return $employees;
            }
            return $this->setDataResponse($employees)->setResponse(true);
        }
        $this->setModelData($employees);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.employee.form_employee", ['view_model' => $viewModel], "view");

    }

    /**
     * @param $employees
     * @return array
     */
    private function decorateByService($employees)
    {
        $newData = [];
        foreach ($employees as $employee) {
            $entityEmployeeServices = $employee['entity_employee_services'];
            if ($entityEmployeeServices->isNotEmpty()) {
                foreach ($entityEmployeeServices as $entityEmployeeService) {

                    if (!$entityEmployeeService['entity_relation_service']){
                        continue;
                    }

                    $newData[$entityEmployeeService['entity_relation_service_id']]["service"] = $entityEmployeeService['entity_relation_service'];
                    $newData[$entityEmployeeService['entity_relation_service_id']]["employees"][] = ["employee_id" => $employee['id'], "user_id" => $employee['user_id'], "user_name" => $employee['user']['full_name'], "small_avatar_url" => $employee['user']['small_avatar_url']];
                }
            } else {
                $newData["public"]["service"] = ["id" => 0, "title" => "public"];
                $newData["public"]["employees"][] = ["employee_id" => $employee['id'], "user_id" => $employee['user_id'], "user_name" => $employee['user']['full_name'], "small_avatar_url" => $employee['user']['small_avatar_url']];
            }
        }
        return $newData;
    }

    /**
     * @param bool $check_access_owner
     * @return $this
     */
    public function destroyEmployee($check_access_owner = true)
    {
        $entity_employee_id = $this->request->get('entity_employee_id');
        $entity_employee = Entity\EntityEmployee::enable()->find($entity_employee_id);

        if (!$entity_employee) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        if ($check_access_owner) {
            $entity = Entity::enable()->isOwner()->find($entity_employee->entity_id);

            if (!$entity) {
                return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
            }
        }


        if ($entity_employee->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveEmployee()
    {
        $data = $this->request->all();
        $response = $this->serviceSaveEntityEmployee($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('entity.edit', ['entity_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @param $data
     * @param bool $check_access_owner
     * @return array
     */
    public function serviceSaveEntityEmployee($data, $check_access_owner = true)
    {
        try {
            $model = new Entity\EntityEmployee();
            $entity_employee_id = isset($data['entity_employee_id']) ? $data['entity_employee_id'] : 0;
            if ($entity_employee_id) {
                $model = $model->find($entity_employee_id);
                if (!$model) {
                    return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
                }
                $entity_id = $model->entity_id;
            } else {
                $entity_id = isset($data['entity_id']) ? $data['entity_id'] : 0;
            }

            if ($check_access_owner) {
                $entity = Entity::enable()->isOwner()->find($entity_id);
                if (!$entity) {
                    return ['action' => false, 'message' => trans("entity::messages.alert.access_error")];
                }
            }

            if (!isset($data['user_id'])) {
                $this->request->offsetSet('confirm', 1);
                $createViewModel = BridgeHelper::getUser()->getCreateViewModel();
                $response = $createViewModel->setRequest($this->request)->saveUser()->getResponse();
                if (!$response['action']) {
                    return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors' => isset($response['errors']) ? $response['errors'] : []];
                }
                $data['user_id'] = isset($response['data']->id) ? $response['data']->id : null;
            }

            if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
                $model->fill($data);
                if ($model->save()) {
                    return ['action' => true, 'message' => trans("entity::messages.alert.save_success"), 'data' => $model];
                } else {
                    return ['action' => false, 'message' => trans("entity::messages.alert.save_un_success")];
                }
            }
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors' => $this->errors];
        } catch (\Exception $exception) {
            report($exception);
            return ['action' => false, 'message' => trans("entity::messages.alert.error")];
        }
    }

    /**
     * @return $this
     */
    public function addAvatar()
    {
        $entity_id = $this->request->get('entity_id');
        $entity = Entity::enable()->isOwner()->find($entity_id);
        if (!$entity) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        $user_id = $this->request->get('user_id');
        $model = new Entity\EntityEmployee();
        $entity_employee = $model->active()->where('entity_id', $entity_id)->where('user_id', $user_id)->first();
        if (!$entity_employee) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }


        $editViewModel = BridgeHelper::getUser()->getEditViewModel();
        $response = $editViewModel->saveUserAvatar($entity_employee->user_id);
        if (!$response) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.save_un_success"));
        }

        $userModel = BridgeHelper::getUser()->getUserModel();
        $user = $userModel->find($user_id);
        $user->avatar = $response;
        $user->save();

        return $this->redirectBack()->setResponse(true, trans("entity::messages.alert.save_success"));
    }

    /**
     * @param null $entity_relation_service_ids STRING | ARRAY
     * @param null $employee_id
     * @param null $for_gender
     * @param string $params is Array
     * @param bool $check_access_owner
     * @return $this
     */
    public function addServiceToEmployee($entity_relation_service_ids = null, $employee_id = null, $for_gender = null, $params = '', $check_access_owner = true)
    {
        $employee_id = $employee_id ?: $this->request->get("entity_employee_id");
        $entity_relation_service_ids = $entity_relation_service_ids ?: $this->request->get("entity_relation_service_ids");
        $for_gender = $for_gender ?: $this->request->get("for_gender");
        $params = $params ?: $this->request->get("params", "");

        try {
            $model = new Entity\EntityEmployee();
            $entity_employee = $model->enable()->find($employee_id);
            if (!$entity_employee) {
                return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
            }

            $existServices = Entity\EntityEmployeeServices::enable()->where("entity_employee_id", $employee_id)->get();
            $currentServiceIds = $existServices ? $existServices->pluck("entity_relation_service_id") : [];

            if (!is_array($entity_relation_service_ids)) {
                $entity_relation_service_ids = [$entity_relation_service_ids];
            }
            $entity_relation_service_ids = collect($entity_relation_service_ids)->diff($currentServiceIds)->values()->toArray();

            $model = new EntityRelationServices();
            $entity_services = $model->enable()->whereIn("id", $entity_relation_service_ids)->get();
            if (!$entity_services) {
                return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
            }

            if ($check_access_owner) {
                $entity_id = $entity_employee->entity_id;
                $entity = Entity::enable()->isOwner()->find($entity_id);
                if (!$entity) {
                    return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
                }
            }

            $res = [];
            foreach ($entity_services as $entity_service) {
                $data = [
                    'entity_employee_id' => $employee_id,
                    'entity_relation_service_id' => $entity_service->id,
                    'for_gender' => $for_gender,
                    'params' => $params ? json_encode($params) : null
                ];

                $model = new Entity\EntityEmployeeServices();
                $model->fill($data);
                $model->save();
                $res[] = $model;
            }

            return $this->redirectBack()->setDataResponse($res)->setResponse(boolval($res), boolval($res) ? trans("entity::messages.alert.save_success") : trans("entity::messages.alert.save_un_success"));
        } catch (\Exception $exception) {
            report($exception);
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.error"));
        }

    }

    /**
     * @param bool $check_access_owner
     * @return $this
     */
    public function destroyEmployeeService($check_access_owner = true)
    {
        $entity_employee_service_id = $this->request->get('entity_employee_service_id');
        $entity_employee_service = Entity\EntityEmployeeServices::enable()->with("entityEmployee.entity")->find($entity_employee_service_id);

        if (!$entity_employee_service) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        if ($check_access_owner) {
            $entity_id = isset($entity_employee_service->entityEmployee->entity->id) ? $entity_employee_service->entityEmployee->entity->id : 0;
            $entity = Entity::enable()->isOwner()->find($entity_id);
            if (!$entity) {
                return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
            }
        }

        if ($entity_employee_service->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    public function getUserForEmployeeByApi()
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }

        $model = BridgeHelper::getUser()->getUserRegisterKeyModel();
        $model = $model->enable()->with('user')->select('*');
        if ($q) {

            $model->where(function ($query) use ($q) {
                $query->where('value', '=', $q);
            });
            $items = $model->get();

        } elseif ($selected && count($selected)) {
            $model->whereIn('user_id', $selected);
            $items = $model->get();

        } else {
            $items = $this->request->has('selected') ? collect([]) : $model->orderBy('id', 'DESC')->take(20)->get();
        }

        $items = $items->map(function ($row) {
            if ($row->user) {
                $new = [
                    'user_id' => $row->user->id,
                    'full_name' => $row->user->fullname,
                    'avatar' => $row->user->avatar,
                    'small_avatar_url' => $row->user->small_avatar_url,
                    'confirm' => $row->user->confirm,
                    'national_code' => $row->user->national_code,
                ];
                return $new;
            }
        });

        return $this->setDataResponse($items)->setResponse(true);
    }


}
