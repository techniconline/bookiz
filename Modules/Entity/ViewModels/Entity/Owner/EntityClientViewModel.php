<?php

namespace Modules\Entity\ViewModels\Entity\Owner;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Category\EntityCategoryRelation;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\Models\Service\EntityServices;
use Modules\User\ViewModels\Manage\User\CreateViewModel;

class EntityClientViewModel extends BaseViewModel
{

    private $access_assets;

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @param bool $json
     * @param null $entity_id
     * @return array|\Illuminate\Support\Collection|EntityClientViewModel
     * @throws \Throwable
     */
    public function showClients($json = false, $entity_id = null)
    {
        $model = new Entity();
        $model = $model->enable()->isOwner()->find($this->request->get('entity_id', $entity_id));

        if (!$model) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        $clients = Entity\EntityClient::active()->with(['user'])
            ->whereHas('user', function ($q){
                if ($text = $this->request->get('text')){
                    $q->Where('username', 'LIKE', '%' . $text . '%');
                    $q->orWhere('last_name', 'LIKE', '%' . $text . '%');
                    $q->orWhere('first_name', 'LIKE', '%' . $text . '%');
                }
            })
            ->where('entity_id', $model->id)->paginate();

        $this->setTitlePage(trans('entity::entity.clients.show'));
        if (get_instance()->isAjax() || $json) {
            $clients = $this->decorateList($clients);
            if ($json) {
                return $clients;
            }
            return $this->setDataResponse($clients)->setResponse(true);
        }
        $this->setModelData($clients);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.client.form_client", ['view_model' => $viewModel], "view");

    }

    /**
     * @param bool $json
     * @param null $entity_client_id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection|mixed|EntityClientViewModel
     * @throws \Throwable
     */
    public function showInfoClient($json = false, $entity_client_id = null)
    {
        $entity_client_id = $entity_client_id?:$this->request->get('entity_client_id');
        $client = Entity\EntityClient::active()->with(['user'])->find($entity_client_id);

        if (!$client){
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        $model = new Entity();
        $model = $model->enable()->isOwner()->find($this->request->get('entity_id', $client->entity_id));

        if (!$model) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        $this->setTitlePage(trans('entity::entity.clients.show'));
        if (get_instance()->isAjax() || $json) {
            $client = $this->decorateAttributes($client);
            if ($json) {
                return $client;
            }
            return $this->setDataResponse($client)->setResponse(true);
        }

        $this->setModelData($client);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.client.form_client", ['view_model' => $viewModel], "view");

    }

    /**
     * @param bool $check_access_owner
     * @return $this
     */
    public function destroyClient($check_access_owner = true)
    {
        $entity_client_id = $this->request->get('entity_client_id');
        $entity_client = Entity\EntityClient::enable()->find($entity_client_id);

        if (!$entity_client) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        if ($check_access_owner) {
            $entity = Entity::enable()->isOwner()->find($entity_client->entity_id);

            if (!$entity) {
                return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
            }
        }


        if ($entity_client->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveClient()
    {
        $data = $this->request->all();
        $response = $this->serviceSaveEntityClient($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('entity.edit', ['entity_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @param $data
     * @param bool $check_access_owner
     * @return array
     */
    public function serviceSaveEntityClient($data, $check_access_owner = true)
    {
        try {
            $model = new Entity\EntityClient();
            $entity_client_id = isset($data['entity_client_id']) ? $data['entity_client_id'] : 0;
            if ($entity_client_id) {
                $model = $model->find($entity_client_id);
                if (!$model) {
                    return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
                }
                $entity_id = $model->entity_id;
            } else {
                $entity_id = isset($data['entity_id']) ? $data['entity_id'] : 0;
            }

            if ($check_access_owner) {
                $entity = Entity::enable()->isOwner()->find($entity_id);
                if (!$entity) {
                    return ['action' => false, 'message' => trans("entity::messages.alert.access_error")];
                }
            }

            $modelConnector = BridgeHelper::getUser()->getUserRegisterKeyModel();
            $userConnector = $modelConnector->active()->where('value',$this->request->get('connector'))->orderBy('id','DESC')->first();
            if ($userConnector){
                $data['user_id'] = $userConnector->user_id;
            }


            if (!isset($data['user_id'])) {
                $this->request->offsetSet('confirm', 1);
                $createViewModel = BridgeHelper::getUser()->getCreateViewModel();
                $response = $createViewModel->setRequest($this->request)->saveUser()->getResponse();
                if (!$response['action']) {
                    return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors' => isset($response['errors']) ? $response['errors'] : []];
                }
                $data['user_id'] = isset($response['data']->id) ? $response['data']->id : null;
            }

            if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
                $model->fill($data);
                if ($model->save()) {
                    $modelUser = BridgeHelper::getUser()->getUserModel();
                    $user = $modelUser->find($data['user_id']);
                    $model->user = $user;
                    return ['action' => true, 'message' => trans("entity::messages.alert.save_success"), 'data' => $model];
                } else {
                    return ['action' => false, 'message' => trans("entity::messages.alert.save_un_success")];
                }
            }
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors' => $this->errors];
        } catch (\Exception $exception) {
            report($exception);
            return ['action' => false, 'message' => trans("entity::messages.alert.error")];
        }
    }

    /**
     * @return $this
     */
    public function addAvatar()
    {
        $entity_id = $this->request->get('entity_id');
        $entity = Entity::enable()->isOwner()->find($entity_id);
        if (!$entity) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        $user_id = $this->request->get('user_id');
        $model = new Entity\EntityClient();
        $entity_client = $model->active()->where('entity_id', $entity_id)->where('user_id', $user_id)->first();
        if (!$entity_client) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }


        $editViewModel = BridgeHelper::getUser()->getEditViewModel();
        $response = $editViewModel->saveUserAvatar($entity_client->user_id);
        if (!$response) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.save_un_success"));
        }

        $userModel = BridgeHelper::getUser()->getUserModel();
        $user = $userModel->find($user_id);
        $user->avatar = $response;
        $user->save();

        return $this->redirectBack()->setResponse(true, trans("entity::messages.alert.save_success"));
    }

    /**
     * @return $this
     */
    public function getUserForClientByApi()
    {

        $entity_id = $this->request->get('entity_id');
        $entity = Entity::enable()->isOwner()->find($entity_id);
        if (!$entity) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }

        $model = BridgeHelper::getUser()->getUserRegisterKeyModel();
        $model = $model->enable()->select('*');
        if ($q) {

            $model->where(function ($query) use ($q) {
                $query->where('value', '=', $q);
            });
            $items = $model->get();

        } else {
//            $items = $this->request->has('selected') ? collect([]) : $model->orderBy('id', 'DESC')->take(20)->get();
            $items = collect([]);
        }

        if ($items && $entity) {

            $user_ids = $items->pluck('user_id');

            $items = Entity\EntityClient::active()->where('entity_id', $entity_id)->whereIn('user_id', $user_ids)->get();

            $items = $items->map(function ($row) {
                if ($row->user) {
                    $new = [
                        'entity_client_id' => $row->id,
                        'user_id' => $row->user->id,
                        'full_name' => $row->user->fullname,
                        'avatar' => $row->user->avatar,
                        'small_avatar_url' => $row->user->small_avatar_url,
                        'confirm' => $row->user->confirm,
                        'national_code' => $row->user->national_code,
                    ];
                    return $new;
                }
            });
        }


        return $this->setDataResponse($items)->setResponse(true);
    }


}
