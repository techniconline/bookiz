<?php

namespace Modules\Entity\ViewModels\Entity\Owner;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Entity;

class EntityAddressViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function showAddress()
    {
        $model = new Entity();
        $model = $model->enable()->isOwner()->find($this->request->get('entity_id'));

        if (!$model) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        $address = Entity\EntityAddress::active()->where('entity_id', $model->id)->get();
        $address = $this->decorateAttributes($address);

        $this->setTitlePage(trans('entity::entity.address.show'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($address)->setResponse(true);
        }
        $this->setModelData($model);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.address.form_address", ['view_model' => $viewModel], "view");

    }

    /**
     * @return $this
     */
    protected function destroyAddress()
    {
        $entity_address_id = $this->request->get('entity_address_id');
        $entity_address = Entity\EntityAddress::enable()->find($entity_address_id);

        if (!$entity_address) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        $entity = Entity::enable()->isOwner()->find($entity_address->entity_id);

        if (!$entity) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        if ($entity_address->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveAddress()
    {
        $data = $this->request->all();
        $response = $this->serviceSaveEntityAddress($data);
        return $this->redirectBack()->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @param $data
     * @param bool $check_access_owner
     * @return array
     */
    public function serviceSaveEntityAddress($data, $check_access_owner = true)
    {
        try {
            $model = new Entity\EntityAddress();
            $entity_address_id = isset($data['entity_address_id']) ? $data['entity_address_id'] : 0;
            if ($entity_address_id) {
                $model = $model->find($entity_address_id);
                if (!$model) {
                    return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
                }
                $entity_id = $model->entity_id;
                if (!isset($data["is_default"]) && $model->is_default) {
                    $model->is_default = 0;
                }
            } else {
                $entity_id = isset($data['entity_id']) ? $data['entity_id'] : 0;
            }

            if ($check_access_owner){
                $entity = Entity::enable()->isOwner()->find($entity_id);
                if (!$entity) {
                    return ['action' => false, 'message' => trans("entity::messages.alert.access_error")];
                }
            }

            if (isset($data["is_default"]) && $data["is_default"]) {
                Entity\EntityAddress::enable()->where('entity_id', $entity_id)
                    ->where(function ($q) use ($entity_address_id) {
                        if ($entity_address_id) {
                            $q->where('id', '!=', $entity_address_id);
                        }
                    })->where("is_default", 1)->update(["is_default" => 0]);
            }

            if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
                $model->fill($data);
                if ($model->save()) {
                    return ['action' => true, 'message' => trans("entity::messages.alert.save_success"), 'data' => $model];
                } else {
                    return ['action' => false, 'message' => trans("entity::messages.alert.save_un_success")];
                }
            }
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors' => $this->errors];
        } catch (\Exception $exception) {
            report($exception);
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data")];
        }
    }

    /**
     * @param int $lat
     * @param int $lon
     * @param int $near_dist
     * @return $this
     */
    public function getEntityAddressByApi($lat = 0, $lon = 0, $near_dist = 1000)
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }

        $lat = $this->request->get('lat', $lat);
        $lon = $this->request->get('lon', $lon);
        $near_dist = $this->request->get('near_dist', $near_dist);

        $model = Entity\EntityAddress::active()
            ->where(function ($q) use ($lat, $lon, $near_dist) {
                if ($lat && $lon) {
                    $q->searchDistance($lat, $lon, $near_dist);
                    $q->addSelect(DB::raw('calc_distance_point(' . $lat . ',' . $lon . ',location,' . 0 . ',null,null) as dist'));
                    $q->orderBy(DB::raw('calc_distance_point(' . $lat . ',' . $lon . ',location,' . 0 . ',null,null)'), 'Desc');
                }
            })
            ->select('*')
            ->distinct('entity_addresses.entity_id')
            //->orderBy(DB::raw('calc_distance_point(' . $lat . ',' . $lon . ',location,' . 0 . ',null,null)'), 'Desc')
            ->with('entity')
            ->whereHas('entity', function ($q) {
                $term = $this->request->get('title');
                $q->filterCurrentInstance();
                if ($term) {
                    $q->where('title', 'LIKE', '%' . $term . '%');
                }
            });

        if ($q) {

            $model->where(function ($query) use ($q) {
                $query->where('address', 'LIKE', '%' . $q . '%');
            });

            $items = $model->get();

        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();
        } else {
            $items = $this->request->has('selected') ? [] : $model->orderBy('id', 'DESC')->take(20)->get();
        }

        $items = $this->decorateList($items, null, ['dist']);
        if ($items->isNotEmpty()) {
            $items = $items->map(function ($item) {
                if ($item['dist'] > 1000) {
                    $item['dist'] = number_format($item['dist'] / 1000, 2);
                    $item['unit'] = trans('entity::entity.address.units.km');
                } else {
                    $item['unit'] = trans('entity::entity.address.units.m');
                }
                return $item;
            });
        }


        return $this->setDataResponse($items)->setResponse(true);
    }

}
