<?php

namespace Modules\Entity\ViewModels\Entity\Owner;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Category\EntityCategoryRelation;
use Modules\Entity\Models\Entity;

class EntityViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;
    private $entity_view_model;

    public function __construct()
    {
        $this->entity_view_model = new \Modules\Entity\ViewModels\Entity\EntityViewModel();
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function listEntity()
    {
        $model = new Entity();
        $model = $model->enable()
            ->isOwner()
            ->createRelationsWithFilter(['entitySliderMedias', 'entityCategory'])
            ->get();

        $model = $this->decorateList($model);


        $this->setTitlePage(trans('entity::entity.list'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($model)->setResponse(true);
        }
        $this->setModelData($model);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.list", ['view_model' => $viewModel], "view");
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function showEntity()
    {
        $model = new Entity();
        $model = $model->enable()
            ->isOwner()
            ->createRelationsWithFilter(['entitySliderMedias'
                , 'entityRelationServices.entityMedias'
                , 'entityCategory'
                , 'entityCategories', 'entityContacts', 'entityOwners.user', 'entityAddress', 'entityAddresses'])
            ->find($this->request->get('entity_id'));

        $model = $this->decorateAttributes($model);

        if ($model) {
            $model->count_access_add_slider_images = $this->entity_view_model->getCountOfImages();
        }

        $this->setTitlePage(trans('entity::entity.show'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($model)->setResponse(true);
        }
        $this->setModelData($model);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.view", ['view_model' => $viewModel], "view");

    }

    /**
     * @return $this
     */
    protected function destroyEntity()
    {
        $entity_id = $this->request->get('entity_id');
        $entity = Entity::enable()->isOwner()->find($entity_id);

        if ($entity->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveEntity()
    {
        $user = BridgeHelper::getAccess()->getUser();
        $data = $this->request->all();
        $data['user_id'] = $user ? $user->id : 0;
        $data['instance_id'] = app('getInstanceObject')->getCurrentInstanceId();
        $response = $this->serviceSaveEntity($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('entity.edit', ['entity_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @param $data
     * @return array
     */
    public function serviceSaveEntity($data)
    {
        $model = new Entity();

        $entity_id = isset($data['entity_id']) ? $data['entity_id'] : 0;
        if ($entity_id) {
            $model = $model->isOwner()->enable()->find($entity_id);
            if (!$model) {
                return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
            }
        }

        if (!isset($data["categories"])) {
            $data["categories"] = [];
        }

        if (isset($data["category_id"]) && $data["category_id"]){
            array_push($data["categories"], $data["category_id"]);
        }

        $data["active"] = 2;
        if (!isset($data["slug"]) && isset($data["title"]) && $data["title"]) {
            $data["slug"] = create_alias($data["title"]);
        }

        if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
            $model->fill($data);
            if ($model->save()) {

                if (isset($data['categories']) && $data['categories']) {
                    $this->serviceSaveCategories($model->id, $data['categories']);
                }

                if (!$entity_id) {
                    $this->serviceSaveOwners($model->id, [auth()->user()->id]);
                }


                return ['action' => true, 'message' => trans("entity::messages.alert.save_success"), 'data' => $model];
            } else {
                return ['action' => false, 'message' => trans("entity::messages.alert.save_un_success")];
            }
        }
        return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors' => $this->errors];
    }

    /**
     * @param $entity_id
     * @param $categories
     * @return bool
     */
    public function serviceSaveCategories($entity_id, $categories)
    {
        $model = new EntityCategoryRelation();
        $insertItems = [];
        $model->where('entity_id', $entity_id)->delete();

        foreach ($categories as $category) {
            $insertItems[] = ['entity_id' => $entity_id, 'category_id' => $category];
        }

        if ($insertItems) {
            return $model->insert($insertItems);
        }
        return false;
    }

    /**
     * @param $entity_id
     * @param $owners
     * @return bool
     */
    public function serviceSaveOwners($entity_id, $owners)
    {
        $model = new Entity\EntityOwner();
        $insertItems = [];
        $model->where('entity_id', $entity_id)->delete();

        foreach ($owners as $owner) {
            $insertItems[] = ['entity_id' => $entity_id, 'user_id' => $owner, 'active' => 1, 'created_at' => date("Y-m-d H:i:s")];
        }

        if ($insertItems) {
            return $model->insert($insertItems);
        }
        return false;
    }

}
