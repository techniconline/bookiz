<?php

namespace Modules\Entity\ViewModels\Entity\Owner;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;

class EntityProductViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getUnitsOfProduct()
    {
        return $this->setDataResponse(trans('entity::entity.products.units'))->setResponse(true);
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function showProducts()
    {
        $model = new Entity();
        $model = $model->enable()->isOwner()->find($this->request->get('entity_id'));

        if (!$model) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        $services = Entity\EntityProduct::active()->paginate();
        $services = $this->decorateList($services);

        $this->setTitlePage(trans('entity::entity.products.show'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($services)->setResponse(true);
        }
        $this->setModelData($model);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.product.form_product", ['view_model' => $viewModel], "view");

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function showProduct()
    {

        $entity_product_id = $this->request->get('entity_product_id');
        $entity_product = Entity\EntityProduct::enable()->find($entity_product_id);

        if (!$entity_product) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        $entity = Entity::enable()->isOwner()->find($entity_product->entity_id);

        if (!$entity) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        $this->setTitlePage(trans('entity::entity.products.show'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($entity_product)->setResponse(true);
        }
        $this->setModelData($entity_product);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.product.form_product", ['view_model' => $viewModel], "view");

    }

    /**
     * @return $this
     */
    protected function destroyProduct()
    {
        $entity_product_id = $this->request->get('entity_product_id');
        $entity_product = Entity\EntityProduct::enable()->find($entity_product_id);

        if (!$entity_product) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        $entity = Entity::enable()->isOwner()->find($entity_product->entity_id);

        if (!$entity) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        if ($entity_product->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveProduct()
    {
        $this->request = $this->requestValuesUpdate();
        $data = $this->request->all();
        $response = $this->serviceSaveEntityProduct($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('entity.edit', ['entity_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }


    /**
     * @param $data
     * @return array
     */
    public function serviceSaveEntityProduct($data)
    {
        try {
            $model = new Entity\EntityProduct();
            $entity_product_id = isset($data['entity_product_id']) ? $data['entity_product_id'] : 0;
            if ($entity_product_id) {
                $model = $model->find($entity_product_id);
                if (!$model) {
                    return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
                }
                $entity_id = $model->entity_id;
            } else {
                $entity_id = isset($data['entity_id']) ? $data['entity_id'] : 0;
            }

            $entity = Entity::enable()->isOwner()->find($entity_id);
            if (!$entity) {
                return ['action' => false, 'message' => trans("entity::messages.alert.access_error")];
            }


            if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
                $model->fill($data);
                if ($model->save()) {
                    return ['action' => true, 'message' => trans("entity::messages.alert.save_success"), 'data' => $model];
                } else {
                    return ['action' => false, 'message' => trans("entity::messages.alert.save_un_success")];
                }
            }
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors' => $this->errors];
        } catch (\Exception $exception) {
            report($exception);
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data")];
        }
    }

    public function assignServiceToProducts()
    {
        $entity_products = $this->request->get('entity_products');
        $entity_relation_service_id = $this->request->get('entity_relation_service_id');

        if (!$entity_products || !$entity_relation_service_id || !is_array($entity_products)) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.mis_data"));
        }

        $entity_relation_service = EntityRelationServices::enable()->with(["entity" => function ($q) {
            $q->isOwner();
        }])->find($entity_relation_service_id);

        if (!$entity_relation_service) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.mis_data"));
        }

        $res = [];
        foreach ($entity_products as $entity_product_id => $used) {
            $res[] = $this->assignServiceToProduct($entity_relation_service, $entity_product_id, $used);
        }

        return $this->redirectBack()->setDataResponse($res)->setResponse(true, trans("entity::messages.alert.save_success"));

    }

    /**
     * @param $entity_relation_service
     * @param $entity_product_id
     * @param int $used
     * @return array
     */
    public function assignServiceToProduct($entity_relation_service, $entity_product_id, $used = 0)
    {

        if (!$entity_product_id || !$entity_relation_service) {
            return ["action" => false, "message" => trans("entity::messages.alert.mis_data")];
        }

        $entity_product = Entity\EntityProduct::enable()->with(["entity" => function ($q)use($entity_relation_service) {
            $q->isOwner();
        }])->where("entity_id",$entity_relation_service->entity_id)->find($entity_product_id);

        if (!$entity_product) {
            return ["action" => false, "message" => trans("entity::messages.alert.mis_data")];
        }

        $model = Entity\EntityProductService::enable()->where("entity_relation_service_id", $entity_relation_service->id)->where("entity_product_id", $entity_product_id)->first();

        if (!$model) {
            $model = new Entity\EntityProductService();
        }else{
            $used = $used + $model->used;
        }

        $data = [
            'entity_relation_service_id' => $entity_relation_service->id,
            'entity_product_id' => $entity_product_id,
            'used' => $used,
            'active' => 1,
        ];

        if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
            $model->fill($data);
            if ($model->save()) {
                return ["action" => true, "data" => $model, "message" => trans("entity::messages.alert.save_success")];
            } else {
                return ["action" => false, "message" => trans("entity::messages.alert.save_un_success")];
            }
        }
        return ["action" => false, "message" => trans("entity::messages.alert.mis_data"), "errors" => $this->errors];
    }


    /**
     * @return EntityProductViewModel
     */
    protected function destroyProductService()
    {
        $entity_product_service_id = $this->request->get('entity_product_service_id');
        $entity_product_service = Entity\EntityProductService::enable()->with('entityProduct')->find($entity_product_service_id);

        if (!$entity_product_service) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        $entity = Entity::enable()->isOwner()->find($entity_product_service->entityProduct->entity_id);

        if (!$entity) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        if ($entity_product_service->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    public function getEntityProductByApi()
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }

        $model = new Entity\EntityProduct();
        $model = $model->active()->select('id', DB::raw('title as text'));
        if ($q) {

            $model->where(function ($query) use ($q) {
                $query->where('title', 'LIKE', '%' . $q . '%');
            });
            $items = $model->get();

        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $items = $this->request->has('selected') ? [] : $model->orderBy('id', 'DESC')->take(20)->get();
        }

        return $this->setDataResponse($items)->setResponse(true);
    }


}
