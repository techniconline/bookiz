<?php

namespace Modules\Entity\ViewModels\Entity\Owner;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Entity\EntityContact;

class EntityContactViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function showContacts()
    {
        $model = new Entity();
        $model = $model->enable()->isOwner()->find($this->request->get('entity_id'));

        if (!$model) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        $contact = EntityContact::active()->where('entity_id', $model->id)->get();
        $contact = $this->decorateAttributes($contact);

        $this->setTitlePage(trans('entity::entity.contact.show'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($contact)->setResponse(true);
        }
        $this->setModelData($model);
        $viewModel =& $this;
        return $this->renderedView("entity::entity.contact.form_contact", ['view_model' => $viewModel], "view");

    }

    /**
     * @return $this
     */
    protected function destroyContact()
    {
        $entity_contact_id = $this->request->get('entity_contact_id');
        $entity_contact = EntityContact::enable()->find($entity_contact_id);

        if (!$entity_contact) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        $entity = Entity::enable()->isOwner()->find($entity_contact->entity_id);

        if (!$entity) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.access_error"));
        }

        if ($entity_contact->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveContact()
    {
        $data = $this->request->all();
        $response = $this->serviceSaveEntityService($data);
        return $this->redirectBack()->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * $contacts array : [media_type => media_value ,...]
     * $default array single = [media_value => 1]
     * @return $this
     */
    protected function saveContacts()
    {
        $data = $this->request->all();

        $contacts = isset($data['contacts']) ? $data['contacts'] : null;
        $entity_id = isset($data['entity_id']) ? $data['entity_id'] : null;
        $default = isset($data['is_default']) ? $data['is_default'] : null;

        if (!$contacts) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.mis_data"));
        }

        $res = [];
        $counter = 0;
        foreach ($contacts as $media => $value) {
            $is_default = 0;
            if (!$default && !$counter) {
                $is_default = 1;
            } elseif ($default && isset($default[$value]) && $default[$value]) {
                $is_default = 1;
            }

            $new_data = [
                'entity_id' => $entity_id,
                'media_type' => $media,
                'media_value' => $value,
                'active' => 1,
                'is_default' => $is_default
            ];

            $res[] = $this->serviceSaveEntityService($new_data);

        }
        return $this->redirectBack()->setDataResponse($res)->setResponse(true, trans("entity::messages.alert.save_success"));
    }

    /**
     * @param $data
     * @return array
     */
    public function serviceSaveEntityService($data)
    {
        try{
            $model = new EntityContact();
            $entity_contact_id = isset($data['entity_contact_id']) ? $data['entity_contact_id'] : 0;
            if ($entity_contact_id) {
                $model = $model->find($entity_contact_id);
                if (!$model) {
                    return ['action' => false, 'message' => trans("entity::messages.alert.not_find_data")];
                }
                $entity_id = $model->entity_id;
            } else {
                $entity_id = isset($data['entity_id']) ? $data['entity_id'] : 0;
                $exist = EntityContact::enable()->where('entity_id', $entity_id)->where('media_type', $data['media_type'])->where('media_value', $data['media_value'])->first();
                if ($exist) {
                    $model = $exist;
                }
            }

            $entity = Entity::enable()->isOwner()->find($entity_id);
            if (!$entity) {
                return ['action' => false, 'message' => trans("entity::messages.alert.access_error")];
            }
            if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
                if (isset($data['is_default']) && $data['is_default']) {
                    EntityContact::enable()
                        ->where(function ($q)use($model){
                            if($model->id){
                                $q->where('id', '!=', $model->id);
                            }
                        })
                        ->where('entity_id', $entity_id)
                        ->update(['is_default' => 0]);
                }
                $model->fill($data);
                if ($model->save()) {
                    return ['action' => true, 'message' => trans("entity::messages.alert.save_success"), 'data' => $model];
                } else {
                    return ['action' => false, 'message' => trans("entity::messages.alert.save_un_success")];
                }
            }
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data"), 'errors' => $this->errors];
        }catch (\Exception $exception){
            return ['action' => false, 'message' => trans("entity::messages.alert.mis_data")];
            report($exception);
        }
    }

}
