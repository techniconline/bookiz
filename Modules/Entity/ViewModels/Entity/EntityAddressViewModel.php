<?php

namespace Modules\Entity\ViewModels\Entity;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;

class EntityAddressViewModel extends BaseViewModel
{

    private $access_assets;
    private $entity;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {
            $access_assets[] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/entity/address.js"), 'name' => 'entity-address'];
        }

        return $access_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createAddress()
    {
        $this->access_assets = true;
        $model = new Entity\EntityAddress();
        $model = $model->find($this->request->get('entity_address_id'));
        $this->setModelData($model);
        $viewModel =& $this;

        $this->setTitlePage(trans('entity::entity.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("entity::address.form_address", ['view_model' => $viewModel], "form");
    }

    /**
     * @return $this
     */
    protected function destroyAddress()
    {
        $entity_address_id = $this->request->get('entity_address_id');
        $entity_address = Entity\EntityAddress::enable()->find($entity_address_id);

        if (!$entity_address) {
            return $this->redirectBack()->setResponse(false, trans("entity::messages.alert.not_find_data"));
        }

        if ($entity_address->delete()) {
            return $this->setResponse(true, trans("entity::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("entity::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveAddress()
    {
        $this->request = $this->requestValuesUpdate();
        $data = $this->request->all();
        $viewModel = new \Modules\Entity\ViewModels\Entity\Owner\EntityAddressViewModel();
        $response = $viewModel->serviceSaveEntityAddress($data, false);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('entity.address.edit', ['entity_address_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @return $this
     */
    public function getEntityAddressByApi()
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }

        $model = new Entity\EntityAddress();
        $model = $model->active()->select('id', DB::raw('address as text'));
        if ($q) {

            $model->where(function ($query) use ($q) {
                $query->where('address', 'LIKE', '%' . $q . '%');
            });
            $items = $model->get();

        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $items = $this->request->has('selected') ? [] : $model->orderBy('id', 'DESC')->take(20)->get();
        }

        return $this->setDataResponse($items)->setResponse(true);
    }


}
