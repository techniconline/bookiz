<?php
namespace Modules\Entity\Bridge\Entity;

use Illuminate\Support\Facades\Request;
use Modules\Entity\Models\Entity\EntityClient;
use Modules\Entity\Models\Entity\EntityOwner;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\ViewModels\Entity\Owner\EntityEmployeeViewModel;

/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class Entity
{


    /**
     * @return \Modules\Entity\Models\Entity
     */
    public function getEntityModel()
    {
        return new \Modules\Entity\Models\Entity();
    }

    /**
     * @return EntityOwner
     */
    public function getEntityOwnerModel()
    {
        return new EntityOwner();
    }

    /**
     * @return string
     */
    public function getEntityClass()
    {
        return \Modules\Entity\Models\Entity::class;
    }

    /**
     * @return string
     */
    public function getEntityRelationServiceClass()
    {
        return EntityRelationServices::class;
    }

    /**
     * @return string
     */
    public function getEntityRelationServiceModel()
    {
        return new EntityRelationServices();
    }

    /**
     * @return EntityEmployeeViewModel
     */
    public function getEntityEmployeeViewModel()
    {
        return new EntityEmployeeViewModel();
    }

    /**
     * @return EntityClient
     */
    public function getEntityClientModel()
    {
        return new EntityClient();
    }


}