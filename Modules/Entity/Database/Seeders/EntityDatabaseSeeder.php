<?php

namespace Modules\Entity\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EntityDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Model::unguard();
        $this->call('Modules\Entity\Database\Seeders\BlockTableSeeder');
        $this->call('Modules\Entity\Database\Seeders\ModuleTableSeeder');
        // $this->call("OthersTableSeeder");
    }
}
