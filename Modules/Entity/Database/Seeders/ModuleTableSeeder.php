<?php

namespace Modules\Entity\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Module\Module;

class ModuleTableSeeder extends Seeder {

	public function run()
	{
		// CurrenciesSeeder
        $module_id = Module::where('name', 'entity')->first()->id;
        if ($module_id){
            return false;
        }
        $module=new Module();
        $module->fill(array(
				'name' =>'entity',
				'version' => '2019010101',
			))->save();

	}
}