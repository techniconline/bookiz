<?php

namespace Modules\Entity\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Block\Block;
use Modules\Core\Models\Module\Module;


class BlockTableSeeder extends Seeder
{

    public function run()
    {
        //DB::table('currencies')->delete();

        $module_id = Module::where('name', 'entity')->first()->id;

        if (!Block::active()->where('module_id', $module_id)->where('name', 'entities_block')->count()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'entities_block',
                'namespace' => 'Modules\Entity\ViewModels\Blocks\EntitiesBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::active()->where('module_id', $module_id)->where('name', 'entity_category_block')->count()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'entity_category_block',
                'namespace' => 'Modules\Entity\ViewModels\Blocks\EntityCategoryBlock',
                'active' => 1,
            ))->save();
        }

    }
}