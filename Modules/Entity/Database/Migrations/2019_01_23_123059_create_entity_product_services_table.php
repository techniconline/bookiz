<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityProductServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('entity_product_services', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_product_id');
            $table->unsignedInteger('entity_relation_service_id')->nullable();
            $table->float("used")->nullable();
            $table->text("params")->nullable();
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('entity_product_services', function (Blueprint $table) {
            $table->foreign('entity_product_id')->references('id')->on('entity_products')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('entity_relation_service_id')->references('id')->on('entity_relation_services')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_product_services', function (Blueprint $table) {
            $table->dropForeign('entity_product_services_entity_employee_id_foreign');
            $table->dropForeign('entity_product_services_entity_relation_service_id_foreign');
        });
        Schema::dropIfExists('entity_product_services');

    }
}
