<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityRelationServicePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('entity_relation_service_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_id');
            $table->unsignedInteger('entity_relation_service_id');
            $table->unsignedInteger('price')->nullable();
            $table->timestamps();
        });

        Schema::table('entity_relation_service_prices', function (Blueprint $table) {

            $table->foreign('entity_relation_service_id','ersp_entity_relation_service_id_foreign')->references('id')->on('entity_relation_services')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('entity_id','ersp_entity_id_foreign')->references('id')->on('entities')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_relation_service_prices', function (Blueprint $table) {
            $table->dropForeign('ersp_entity_relation_service_id_foreign');
            $table->dropForeign('ersp_entity_id_foreign');
        });

        Schema::dropIfExists('entity_relation_service_prices');
    }
}
