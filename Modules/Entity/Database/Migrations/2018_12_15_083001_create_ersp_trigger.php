<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErspTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::unprepared('
            CREATE TRIGGER `insert_after_insert_price_service_log`
            AFTER INSERT ON `entity_relation_services`
            FOR EACH ROW
            BEGIN
              INSERT INTO `entity_relation_service_prices`(`entity_id`, `entity_relation_service_id`, `price`, `created_at`, `updated_at`) 
              VALUES (new.entity_id,new.id,new.price,new.created_at,new.updated_at);
            END
           ');

        DB::unprepared('
            CREATE TRIGGER `insert_after_update_price_service_log`
            AFTER UPDATE ON `entity_relation_services`
            FOR EACH ROW
            BEGIN
              INSERT INTO `entity_relation_service_prices`(`entity_id`, `entity_relation_service_id`, `price`, `created_at`, `updated_at`) 
              VALUES (new.entity_id,new.id,new.price,new.created_at,new.updated_at);
            END
           ');


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS `insert_after_insert_price_service_log`');
        DB::unprepared('DROP TRIGGER IF EXISTS `insert_after_update_price_service_log`');

    }
}
