<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('instance_id');
            $table->unsignedInteger('language_id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->mediumText('short_description')->nullable();
            $table->longText('description')->nullable();
            $table->text('params')->nullable();
            $table->timestamp('active_date')->nullable();
            $table->timestamp('expire_date')->nullable();
            $table->unsignedInteger('user_id');
            $table->tinyInteger('active')->default('2')->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('entities', function (Blueprint $table) {
            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('parent_id')->references('id')->on('entities')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entities', function (Blueprint $table) {
            $table->dropForeign('entities_instance_id_foreign');
            $table->dropForeign('entities_language_id_foreign');
            $table->dropForeign('entities_user_id_foreign');
            $table->dropForeign('entities_parent_id_foreign');
        });

        Schema::dropIfExists('entities');
    }
}
