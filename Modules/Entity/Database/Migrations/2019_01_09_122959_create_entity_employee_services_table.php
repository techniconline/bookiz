<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityEmployeeServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('entity_employee_services', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_employee_id')->index();
            $table->unsignedInteger('entity_relation_service_id')->index()->nullable();
            $table->enum('for_gender', ['female', 'male', 'all'])->index()->default('all')->nullable();
            $table->mediumText('params')->nullable();
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('entity_employee_services', function (Blueprint $table) {
            $table->foreign('entity_employee_id')->references('id')->on('entity_employees')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('entity_relation_service_id')->references('id')->on('entity_relation_services')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_employee_services', function (Blueprint $table) {
            $table->dropForeign('entity_employee_services_entity_employee_id_foreign');
            $table->dropForeign('entity_employee_services_entity_relation_service_id_foreign');
        });
        Schema::dropIfExists('entity_employee_services');

    }
}
