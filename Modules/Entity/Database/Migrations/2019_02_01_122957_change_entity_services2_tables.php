<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEntityServices2Tables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('entity_services', function (Blueprint $table) {
            $table->unsignedInteger('parent_id')->after("language_id")->nullable()->index();
            $table->text('params')->after("icon")->nullable();
            $table->integer('sort')->after("params")->nullable()->index();
        });

        Schema::table('entity_services', function (Blueprint $table) {

            $table->foreign('parent_id')->references('id')->on('entity_services')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_services', function (Blueprint $table) {
            $table->dropForeign('entity_services_parent_id_foreign');
            $table->dropColumn(['parent_id','params','sort']);
        });
    }
}
