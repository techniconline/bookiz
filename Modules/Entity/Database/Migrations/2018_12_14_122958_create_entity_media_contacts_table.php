<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityMediaContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('entity_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_id')->index();
            $table->string('media_type')->index()->comment('example: mobile, tel, email, whatsapp, telegram, instagram and ...');
            $table->string('media_value')->index();
            $table->boolean('is_default')->default(0)->nullable();
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('entity_contacts', function (Blueprint $table) {
            $table->foreign('entity_id')->references('id')->on('entities')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_contacts', function (Blueprint $table) {
            $table->dropForeign('entity_contacts_entity_id_foreign');
        });

        Schema::dropIfExists('entity_contacts');
    }
}
