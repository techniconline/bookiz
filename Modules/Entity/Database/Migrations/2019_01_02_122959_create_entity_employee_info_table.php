<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityEmployeeInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('entity_employee_info', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_employee_id')->index();
            $table->boolean('married')->nullable();
            $table->smallInteger('count_childes')->nullable();
            $table->string('nationality')->nullable();
            $table->string('religion')->nullable();
            $table->string('blood_type',10)->nullable();
            $table->date('passport_start_date')->nullable();
            $table->date('passport_end_date')->nullable();
            $table->date('stay_start_date')->nullable()->comment('eghamat');
            $table->date('stay_end_date')->nullable();
            $table->date('driving_license_start_date')->nullable();
            $table->date('driving_license_end_date')->nullable();
            $table->date('certificate_start_date')->nullable();
            $table->date('certificate_end_date')->nullable();
            $table->smallInteger('weight')->nullable();
            $table->smallInteger('length')->nullable();
            $table->mediumText('history_disease')->nullable()->comment('bimari');
            $table->mediumText('drug_history')->nullable()->comment('daro');
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('entity_employee_info', function (Blueprint $table) {
            $table->foreign('entity_employee_id')->references('id')->on('entity_employees')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::create('entity_employee_experience_info', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_employee_id')->index();
            $table->string('experience');
            $table->date('certificate_start_date')->nullable();
            $table->date('certificate_end_date')->nullable();
            $table->integer('work_experience')->nullable()->comment('month');
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });


        Schema::table('entity_employee_experience_info', function (Blueprint $table) {
            $table->foreign('entity_employee_id')->references('id')->on('entity_employees')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_employee_info', function (Blueprint $table) {
            $table->dropForeign('entity_employee_info_entity_employee_id_foreign');
        });
        Schema::dropIfExists('entity_employee_info');

        Schema::table('entity_employee_experience_info', function (Blueprint $table) {
            $table->dropForeign('entity_employee_experience_info_entity_employee_id_foreign');
        });
        Schema::dropIfExists('entity_employee_experience_info');
    }
}
