<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('entity_medias', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_id');
            $table->unsignedInteger('entity_relation_service_id');
            $table->string('src')->nullable();
            $table->mediumText('description')->nullable();
            $table->tinyInteger('sort')->default(0)->nullable();
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('entity_medias', function (Blueprint $table) {

            $table->foreign('entity_relation_service_id')->references('id')->on('entity_relation_services')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('entity_id')->references('id')->on('entities')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_medias', function (Blueprint $table) {
            $table->dropForeign('entity_medias_entity_relation_service_id_foreign');
            $table->dropForeign('entity_medias_entity_id_foreign');
        });

        Schema::dropIfExists('entity_medias');
    }
}
