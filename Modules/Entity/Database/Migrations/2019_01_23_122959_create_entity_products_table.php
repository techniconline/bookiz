<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('entity_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_id');
            $table->string('title')->nullable();
            $table->string('unit',50)->nullable()->inedex();
            $table->float('quantity')->nullable();
            $table->float('order_point')->nullable();
            $table->boolean('notify')->nullable();
            $table->text('params')->nullable();
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('entity_products', function (Blueprint $table) {

            $table->foreign('entity_id')->references('id')->on('entities')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_products', function (Blueprint $table) {
            $table->dropForeign('entity_products_entity_id_foreign');
        });

        Schema::dropIfExists('entity_products');

    }


}
