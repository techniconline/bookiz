<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionSearchDistance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
                    
            CREATE FUNCTION `calc_distance_point`(`my_lat` DOUBLE, `my_lon` DOUBLE, `location` POINT, `x_convert` DOUBLE, `dis_lat` DOUBLE, `dis_lon` DOUBLE) RETURNS double
                NO SQL
                    BEGIN
                    DECLARE distance DOUBLE ;
                            
                    IF x_convert is null THEN
                    set x_convert = 1000;
                    END IF;
                    
                    IF dis_lat>0 and dis_lon>0 THEN
                    set location=(select ST_GeomFromText(ST_AsText(point(dis_lon, dis_lat)),4326));
                    END IF;
                    
                    IF srid(location)!=4326 THEN
                       set location=(select ST_GeomFromText(ST_AsText(location),4326));
                    END IF;
                    
                    set distance=(select ST_Distance_Sphere(ST_GeomFromText(ST_AsText(point(my_lon, my_lat)),4326), location));
                    
                    IF x_convert>0 THEN
                        set distance=distance/x_convert;
                    END IF;
                    
                    RETURN distance;
                    
                    END
            
           ');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP FUNCTION IF EXISTS `calc_distance_point`');
    }
}
