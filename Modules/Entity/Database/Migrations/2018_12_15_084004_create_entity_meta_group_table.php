<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityMetaGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_meta_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_id');
            $table->unsignedInteger('meta_group_id');
            $table->integer('position')->default(0);
        });

        Schema::table('entity_meta_groups', function (Blueprint $table) {
            $table->foreign('meta_group_id')->references('id')->on('meta_groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('entity_id')->references('id')->on('entities')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_meta_groups', function (Blueprint $table) {
            $table->dropForeign('entity_meta_groups_meta_group_id_foreign');
            $table->dropForeign('entity_meta_groups_entity_id_foreign');
        });

        Schema::dropIfExists('entity_meta_groups');
    }
}
