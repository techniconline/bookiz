<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->text('address');
            $table->unsignedInteger('entity_id')->index();
            $table->unsignedInteger('province_id')->nullable()->index();
            $table->unsignedInteger('city_id')->nullable()->index();
            $table->decimal('latitude', 12,8)->nullable()->index();
            $table->decimal('longitude', 12,8)->nullable()->index();
            $table->point('location')->nullable()->index();
            $table->boolean('is_default')->default(0)->nullable();
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('entity_addresses', function (Blueprint $table) {
            $table->foreign('entity_id')->references('id')->on('entities')
                ->onDelete('cascade')
                ->onUpdate('cascade');

             $table->foreign('province_id')->references('id')->on('locations')
                ->onDelete('cascade')
                ->onUpdate('cascade');

             $table->foreign('city_id')->references('id')->on('locations')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_addresses', function (Blueprint $table) {
            $table->dropForeign('entity_addresses_entity_id_foreign');
            $table->dropForeign('entity_addresses_city_id_foreign');
            $table->dropForeign('entity_addresses_province_id_foreign');
        });

        Schema::dropIfExists('entity_addresses');
    }
}
