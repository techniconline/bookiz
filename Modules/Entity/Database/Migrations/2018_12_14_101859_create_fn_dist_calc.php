<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use \Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateFnDistCalc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            CREATE FUNCTION `SPLIT_STRING`(`str` VARCHAR(255), `delim` VARCHAR(12), `pos` INT) RETURNS varchar(255) CHARSET utf8
            RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(str, delim, pos),
                   LENGTH(SUBSTRING_INDEX(str, delim, pos-1)) + 1),
                   delim, \'\');
      
           ');

        DB::unprepared('
        CREATE FUNCTION `calc_distance`(`my_lat` DOUBLE, `my_lng` DOUBLE, `db_lat` DOUBLE, `db_lng` DOUBLE, `unit` VARCHAR(20), `unitOut` INT) RETURNS double
        BEGIN
    
		DECLARE distance DOUBLE ;
		DECLARE t_distance INT;
		DECLARE out_distance INT DEFAULT 1000 ;
	       
    	DECLARE mLat DOUBLE;
		DECLARE dLat DOUBLE;
        DECLARE dLng DOUBLE;
        DECLARE msLat DOUBLE;
        DECLARE dsLat DOUBLE;
        DECLARE aCos DOUBLE;
        
		IF unit = \'miles\' THEN                   
           SET t_distance = 3959;          ELSE
           SET t_distance = 6371;        END IF;
              
		IF unitOut THEN
        SET out_distance = unitOut;
        END IF;
              
                SET mLat = COS(RADIANS(my_lat));
        SET dLat = COS(RADIANS(db_lat));

        SET dLng = COS(RADIANS(my_lng - db_lng));
        SET msLat = SIN(RADIANS(my_lat));
        SET dsLat = SIN(RADIANS(db_lat));
        
        SET aCos = ACOS(  mLat * dLat * dLng + msLat * dsLat  );
        SET distance = t_distance * ( aCos );
                
      RETURN distance*out_distance ;
      END
;
      
           ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP FUNCTION `SPLIT_STRING`');
        DB::unprepared('DROP FUNCTION `calc_distance`');
    }
}
