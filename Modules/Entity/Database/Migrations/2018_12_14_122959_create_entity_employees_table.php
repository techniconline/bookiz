<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('entity_employees', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('entity_employees', function (Blueprint $table) {
            $table->foreign('entity_id')->references('id')->on('entities')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_employees', function (Blueprint $table) {
            $table->dropForeign('entity_employees_entity_id_foreign');
        });

        Schema::dropIfExists('entity_employees');
    }
}
