<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEntityServicesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('entity_services', function (Blueprint $table) {
            $table->string("image")->nullable()->after("slug");
            $table->string("icon")->nullable()->after("image");
        });

        Schema::table('entity_relation_services', function (Blueprint $table) {
            $table->unsignedInteger('currency_id')->after("price")->nullable();
            $table->string("image")->nullable()->after("currency_id");
            $table->string("icon")->nullable()->after("image");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
