<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityCategoryRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_category_relations', function (Blueprint $table) {
            $table->unsignedInteger('entity_id')->index();
            $table->unsignedInteger('category_id')->index();
        });

        Schema::table('entity_category_relations', function (Blueprint $table) {
            $table->foreign('entity_id')->references('id')->on('entities')
                ->onDelete('cascade')
                ->onUpdate('cascade');

             $table->foreign('category_id')->references('id')->on('entity_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_category_relations', function (Blueprint $table) {
            $table->dropForeign('entity_category_relations_entity_id_foreign');
            $table->dropForeign('entity_category_relations_category_id_foreign');
        });

        Schema::dropIfExists('entity_category_relations');
    }
}
