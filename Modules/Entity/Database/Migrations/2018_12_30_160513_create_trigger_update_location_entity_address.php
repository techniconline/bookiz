<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerUpdateLocationEntityAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
                    
            CREATE TRIGGER `update_location_entity_address_before_insert` BEFORE INSERT ON `entity_addresses`
             FOR EACH ROW BEGIN
            
            IF new.latitude>0 and new.longitude>0 THEN
            
                SET new.location=ST_GeomFromText(ST_AsText(point(new.longitude, new.latitude)),4326);
            
            END IF;
            
            END
            
           ');

        DB::unprepared('
        
            CREATE TRIGGER `update_location_entity_address_before_update` BEFORE UPDATE ON `entity_addresses`
             FOR EACH ROW BEGIN
            
            IF new.latitude>0 and new.longitude>0 THEN
            
                SET new.location=ST_GeomFromText(ST_AsText(point(new.longitude, new.latitude)),4326);
            
            END IF;
            
            END
            
           ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS `update_location_entity_address_before_update`');
        DB::unprepared('DROP TRIGGER IF EXISTS `update_location_entity_address_before_insert`');
    }
}
