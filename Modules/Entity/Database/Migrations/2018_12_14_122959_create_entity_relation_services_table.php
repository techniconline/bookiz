<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityRelationServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('entity_relation_services', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_id');
            $table->unsignedInteger('entity_service_id');
            $table->string('title')->nullable();
            $table->unsignedInteger('price')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('entity_relation_services', function (Blueprint $table) {

            $table->foreign('entity_service_id')->references('id')->on('entity_services')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('entity_id')->references('id')->on('entities')
                ->onDelete('cascade')
                ->onUpdate('cascade');


            $table->foreign('currency_id')->references('id')->on('currencies')
                ->onDelete('cascade')
                ->onUpdate('cascade');



        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_relation_services', function (Blueprint $table) {
            $table->dropForeign('entity_relation_services_entity_service_id_foreign');
            $table->dropForeign('entity_relation_services_entity_id_foreign');
            $table->dropForeign('entity_relation_services_currency_id_foreign');
        });

        Schema::dropIfExists('entity_relation_services');

    }


}
