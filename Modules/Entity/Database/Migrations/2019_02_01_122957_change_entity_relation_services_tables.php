<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEntityRelationServicesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('entity_relation_services', function (Blueprint $table) {
            $table->text('params')->after("description")->nullable();
            $table->unsignedInteger('period')->after("params")->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_relation_services', function (Blueprint $table) {
            $table->dropColumn(['params','period']);
        });
    }
}
