<?php

namespace Modules\Video\Models\Video;

use Modules\Core\Models\BaseModel;
use Modules\Video\Traits\Video\VideoMetaGroup as TraitModel;

class VideoMetaGroup extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['video_id', 'meta_group_id', 'start_time', 'end_time'];
    public $messages = [];
    public $rules = [
        'video_id' => 'required',
        'meta_group_id' => 'required',
    ];

    public $timestamps = false;

}
