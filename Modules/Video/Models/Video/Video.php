<?php

namespace Modules\Video\Models\Video;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Video\Traits\Video\Video as TraitModel;
use Modules\Core\Models\BaseModel;

class Video extends BaseModel
{


    use TraitModel;
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['title', 'instance_id', 'language_id', 'hash_key', 'has_poster', 'show_poster', 'extension', 'info', 'status_convert', 'group','path_hash_key', 'bulk_source', 'video_time', 'user_id', 'active', 'deleted_at', 'created_at', 'updated_at'];

    const SYSTEM_NAME = 'video';
    public $messages = [];
    public $rules = [
        'title' => 'required',
        'language_id' => 'required',
        'instance_id' => 'required',
        'hash_key' => 'required',
    ];

    const STATUS_WAIT = "wait";
    const STATUS_NONE = "none";
    const STATUS_UPLOADED = "uploaded";
    const STATUS_ERROR_API = "error-api-call";
    const STATUS_ERROR_UPLOAD = "error-upload";
    const STATUS_NOT_FIND_SOURCE = "not-find-source-file";
    const STATUS_NOT_UPLOAD = "not-upload";
    const STATUS_NEW = "new";
    const STATUS_FINISHED = "finished";

    const STATUS_CONVERT_SYS_PROCESS_FAIL = "processingFail";
    const STATUS_CONVERT_SYS_PRE_PROCESS_FAIL = "preProcessingFail";
    const STATUS_CONVERT_SYS_NOT_FIND_SOURCE_FILE = "notFindSourceFile";

    protected $disk = 'public';
    protected $baseVideoUpload = 'uploads/video';
    protected $basePosterUpload = 'uploads/image';

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $appends = ['has_poster_text', 'show_poster_text'];

}
