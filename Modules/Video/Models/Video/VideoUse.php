<?php

namespace Modules\Video\Models\Video;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Video\Traits\Video\VideoUse as TraitModel;

class VideoUse extends BaseModel
{
    use TraitModel;
    use SoftDeletes;


    /**
     * @var array
     */
    protected $fillable = ['video_id', 'instance_id', 'hash_key', 'model', 'model_id','is_public', 'downloadable', 'poster', 'active', 'deleted_at', 'created_at', 'updated_at','options'];

    public $messages = [];
    public $rules = [
        'video_id' => 'required',
        'instance_id' => 'required',
        'hash_key' => 'required|unique:video_uses',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
