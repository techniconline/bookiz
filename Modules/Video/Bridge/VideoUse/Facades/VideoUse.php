<?php
namespace Modules\Video\Bridge\VideoUse\Facades;

use Illuminate\Support\Facades\Facade;

class VideoUse extends Facade
{

    /**
     * get sms service
     * @return string
     */
    protected static function getFacadeAccessor() { return 'VideoUse'; }

}