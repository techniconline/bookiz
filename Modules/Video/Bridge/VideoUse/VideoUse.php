<?php

namespace Modules\Video\Bridge\VideoUse;

use Modules\Video\Traits\Video\VideoPlayerTrait;
use Modules\Video\Traits\Video\VideoUseTrait;

/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class VideoUse
{
    use VideoUseTrait;
    use VideoPlayerTrait;

    /**
     * @return $this
     */
    public function getVideoUseObject()
    {
        return $this;
    }

    /**
     * @return string
     */
    public function getUrlVideoList()
    {
        return route('video.getVideoList');
    }


    public function getPlayer($json=false){
        return $this->getVideoPlayer($this->get(),$json);
    }

}