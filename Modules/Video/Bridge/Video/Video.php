<?php

namespace Modules\Video\Bridge\Video;

use Modules\Video\Traits\Video\VideoTrait;
use Modules\Video\ViewModels\VideoStream\VideoStreamViewModel;

/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class Video
{
    use VideoTrait;

    /**
     * @return $this
     */
    public function getVideoObject()
    {
        return $this;
    }

    /**
     * @return VideoStreamViewModel
     */
    public function getVideoStreamViewModel()
    {
        return new VideoStreamViewModel();
    }

}