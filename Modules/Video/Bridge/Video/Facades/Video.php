<?php
namespace Modules\Video\Bridge\Video\Facades;

use Illuminate\Support\Facades\Facade;

class Video extends Facade
{

    /**
     * get sms service
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Video'; }

}