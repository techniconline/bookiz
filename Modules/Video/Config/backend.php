<?php

return [
    'version' => '20180619001',
    'blocks' => [
        'left' => [],
        'top' => []
    ],
    'menus' => [
        'management_admin' => [
            [
                'alias' => '', //**
                'route' => 'video.config.video.index', //**
                'key_trans' => 'video::menu.configs_video', //**
                'order' =>50000, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => '', //fa-XXXXX
                'class' => '',
                'id' => '',
                'group_menu' => 'admin_manage_settings',
            ],
            [
                'alias' => '', //**
                'route' => 'video.index', //**
                'key_trans' => 'video::menu.video_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-list', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'video_admin_settings',
                ],
            [
                'alias' => '', //**
                'route' => 'video.videoUse.index', //**
                'key_trans' => 'video::menu.video_use_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-list', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'video_admin_settings',
                ],
        ],
    ],
    'group_menu' => [
        'management_admin' => [
            [   'alias' => 'video_admin_settings', //**
                'key_trans' => 'video::menu.group.videos', //**
                'icon_class' => 'fa fa-video-camera',
                'before' => '',
                'after' => '',
                'order' => 20,
                ],
        ]
    ],

];
