<?php

namespace Modules\Video\Traits\Video;

trait PlayerQuestionsTrait
{
    public function getVideoQuestions($video,$videoUse,$json=true)
    {
        return $this->getVideoQuestionsJson($videoUse->hash_key,$video->video_time,$json);
    }

    private function getVideoQuestionsJson($fileid,$filetime,$getJson=true){
        $overlay=array();
        //$rows=$DB->get_records('repository_nimble_question',array('file_id'=>$fileid,'deleted'=>0),'time');
        $rows=[];
        $overlay=array('schedule'=>array());
        $search=array();
        $replace=array();
        if(count($rows)) {
            foreach ($rows as $row) {
                $answers=array();
                $answers[]=array(
                    'height'=>'0.32',
                    'cols'=>array(array(
                        'width'=>'1',
                        'text'=>$row->title,
                        'style'=>array(
                            'background'=> 'rgba(0,0,0,0.5)',
                            'font-family' => 'IRANSans !important',
                            'border_radius' => '3px'
                        )
                    )),
                    'style'=>array('background'=>'transparent')
                );
                $height='0.15';
                if (strlen($row->answers)) {
                    $row->answers = @unserialize($row->answers);

                    if(count($row->answers)){
                        $height=round(0.6/count($row->answers),2);
                    }
                }

                foreach($row->answers as $key=>$answer){
                    if($getJson){
                        $run=':s_'.$row->id.'_'.$key.':';
                        $search[]='":s_'.$row->id.'_'.$key.':"';
                    }else{
                        $run=array('key'=>$key,'id'=>$row->id,'file_id'=>$row->file_id,'correct'=>$row->correct);
                    }

                    $answers[]=array(
                        'height'=>$height,
                        'cols'=>array(array(
                            'width'=>'1',
                            'text'=>$answer,
                            'isButton'=>true,
                            'run'=>$run,
                            'style'=>array(
                                'background'=> 'rgba(0,0,0,0.1)',
                                'transition'=> 'all ease-in-out 0.2s',
                                'borderRadius'=> '5px',
                                'font-family' => 'IRANSans !important',
                                'border'=> '1px solid rgba(255,255,255,0.5)','color'=> 'white'),
                            'styleHover'=>array(
                                'background'=> 'rgba(255,255,255,0.8)',
                                'transition'=> 'all ease-in-out 0.4s',
                                'borderRadius'=> '5px',
                                'color'=> 'black',
                                'textShadow'=>'none'
                            ),
                        )),
                        'style'=>array('background'=>'transparent')
                    );
                    if($row->correct!=$key){
                        $replace[]="function(player){videoAjaxApi(".$key.",".$row->id.",".$row->file_id."); setTimeout(function(){player.seek(".$row->rewindtime.");}, 10);return true;}";
                    }else{
                        $replace[]="function(player){videoAjaxApi(".$key.",".$row->id.",".$row->file_id."); return true;}";
                    }

                }
                $overlay['schedule'][]=array(
                    'start'=>$row->time,
                    'wait'=>$row->waittime,
                    //'limit'=>1,
                    'rewind'=>$row->rewindtime,
                    'rows'=>$answers,
                );
            }
        }
        if($filetime > 5){
            $answers=array();
            $answers[]=array(
                'height'=>'0.32',
                'cols'=>array(array(
                    'width'=>'1',
                    'text'=>trans('video::data.vote.question'),
                    'style'=>array(
                        'background'=> 'rgba(0,0,0,0.5)',
                        'font-family' => 'IRANSans !important',
                        'border_radius' => '3px',
                    )
                )),
                'style'=>array('background'=>'transparent')
            );
            $height='0.25';
            $votes=array(
                1=>array('title'=>trans('video::data.vote.answers.weak'),'style'=>'rgba(242,95,92,0.6)'),
                2=>array('title'=>trans('video::data.vote.answers.middle'),'style'=>'rgba(232,197,58,0.6)'),
                3=>array('title'=>trans('video::data.vote.answers.good'),'style'=>'rgba(0,192,160,0.6)'),
                4=>array('title'=>trans('video::data.vote.answers.excellent'),'style'=>'rgba(36,123,160,0.6)'),

            );
            if($getJson){
                $run=':s_0'.'_3:';
                $search[]='":s_0'.'_3:"';
                $run1=':s_0'.'_4:';
                $search[]='":s_0'.'_4:"';
            }else{
                $run=array('key'=>3,'id'=>0,'file_id'=>$fileid,'correct'=>0);
                $run1=array('key'=>4,'id'=>0,'file_id'=>$fileid,'correct'=>0);
            }
            $answers[]=array(
                'height'=>$height,
                'cols'=>array(array(
                    'width'=>'0.3',
                    'text'=>$votes[3]['title'],
                    'isButton'=>true,
                    'run'=>$run,
                    'style'=>array(
                        'background'=> $votes[3]['style'],
                        'transition'=> 'all ease-in-out 0.2s',
                        'borderRadius'=> '5px',
                        'font-family' => 'IRANSans !important',
                        'border'=> '1px solid rgba(255,255,255,0.5)',
                        'color'=> 'white'),
                    'styleHover'=>array(
                        'background'=> 'rgba(255,255,255,0.8)',
                        'transition'=> 'all ease-in-out 0.4s',
                        'borderRadius'=> '5px',
                        'color'=> 'black',
                        'textShadow'=>'none'
                    ),
                ),array(
                    'width'=>'0.3',
                    'text'=>$votes[4]['title'],
                    'isButton'=>true,
                    'run'=>$run1,
                    'style'=>array(
                        'background'=> $votes[4]['style'],
                        'transition'=> 'all ease-in-out 0.2s',
                        'borderRadius'=> '5px',
                        'font-family' => 'IRANSans !important',
                        'border'=> '1px solid rgba(255,255,255,0.5)',
                        'color'=> 'white'),
                    'styleHover'=>array(
                        'background'=> 'rgba(255,255,255,0.8)',
                        'transition'=> 'all ease-in-out 0.4s',
                        'borderRadius'=> '5px',
                        'color'=> 'black',
                        'textShadow'=>'none'
                    ),
                )),
                'style'=>array('background'=>'transparent')
            );

            $replace[]="function(player){videoAjaxApi(3,0,'".$fileid."');}";
            $replace[]="function(player){videoAjaxApi(4,0,'".$fileid."');}";


            if($getJson){
                $run=':s_0'.'_1:';
                $search[]='":s_0'.'_1:"';
                $run1=':s_0'.'_2:';
                $search[]='":s_0'.'_2:"';
            }else{
                $run=array('key'=>1,'id'=>0,'file_id'=>$fileid,'correct'=>0);
                $run1=array('key'=>2,'id'=>0,'file_id'=>$fileid,'correct'=>0);
            }
            $answers[]=array(
                'height'=>$height,
                'cols'=>array(array(
                    'width'=>'0.3',
                    'text'=>$votes[1]['title'],
                    'isButton'=>true,
                    'run'=>$run,
                    'style'=>array(
                        'background'=> $votes[1]['style'],
                        'transition'=> 'all ease-in-out 0.2s',
                        'borderRadius'=> '5px',
                        'border'=> '1px solid rgba(255,255,255,0.5)',
                        'font-family' => 'IRANSans !important',
                        'color'=> 'white'),
                    'styleHover'=>array(
                        'background'=> 'rgba(255,255,255,0.8)',
                        'transition'=> 'all ease-in-out 0.4s',
                        'borderRadius'=> '5px',
                        'color'=> 'black',
                        'textShadow'=>'none'
                    ),
                ),array(
                    'width'=>'0.3',
                    'text'=>$votes[2]['title'],
                    'isButton'=>true,
                    'run'=>$run1,
                    'style'=>array(
                        'background'=> $votes[2]['style'],
                        'transition'=> 'all ease-in-out 0.2s',
                        'borderRadius'=> '5px',
                        'font-family' => 'IRANSans !important',
                        'border'=> '1px solid rgba(255,255,255,0.5)',
                        'color'=> 'white'),
                    'styleHover'=>array(
                        'background'=> 'rgba(255,255,255,0.8)',
                        'transition'=> 'all ease-in-out 0.4s',
                        'borderRadius'=> '5px',
                        'color'=> 'black',
                        'textShadow'=>'none'
                    ),
                )),
                'style'=>array('background'=>'transparent')
            );

            $replace[]="function(player){videoAjaxApi(1,0,'".$fileid."');}";
            $replace[]="function(player){videoAjaxApi(2,0,'".$fileid."');}";
            $overlay['schedule'][]=array(
                'start'=>($filetime-3),
                'wait'=>30,
                'rows'=>$answers,
            );
        }

        if($getJson){
            $json=json_encode($overlay,1);
            $json=str_replace($search,$replace,$json);
            return $json;
        }else{
            return $overlay;
        }

    }

}
