<?php
namespace Modules\Video\Traits\Video;

use Illuminate\Support\Facades\Cache;
use Modules\Video\Models\Video\Video;

trait VideoPlayerTrait
{
    use PlayerQuestionsTrait;

    public function getPlayConfigName(){
        return 'farayad';
    }

    public function getVideoPlayer($video_use,$json=false){
        if(!$video_use){
            return $this->getVideoPlayerError($json);
        }
        $data=$this->getVideoPlayerData($video_use);
        if(!count($data)){
            return $this->getVideoPlayerError($json);
        }
        if($json){
            return $data;
        }
        app('getInstanceObject')->setActionAssets($this->getVideoPlayerAssets());
        return view('video::player.clapper.player',$data);
    }


    public function getVideoPlayerData($video_use){
        $video=$this->getVideoObject($video_use->video_id);
        if(!$video){
            return [];
        }
        $source=url('video/play/'.$this->getPlayConfigName().'/'.$video_use->video_id.'/'.$video_use->hash_key.'/master.m3u8');
        //$source='https://learn.farayad.org/hls/251aeb026980dc07ccbcbce0f3c264c8daa524470/playlist.m3u8';
        if($video_use->poster){
            $poster=url($video_use->poster).'?_v='.strtotime($video_use->updated_at?$video_use->updated_at:$video_use->created_at);
        }elseif($video->has_poster && strlen($video->poster_url)){
            $poster=url($video->poster_url).'?_v='.strtotime($video->updated_at?$video->updated_at:$video->created_at);
        }else{
            $poster='';
        }
        $ratio=0.66;
        if($video->status_convert=='finished' && isset($video->info)){
            $ratio=($video->info->info->height/$video->info->info->width);
        }
        $data=[
            'id'=>str_random(10),
            'host'=>$this->getPlayerHost($video_use->video_id),
            'hash'=>$video_use->hash_key,
            'video_use_id'=>$video_use->id,
            'video_id'=>$video_use->video_id,
            'source'=>$source,
            'poster'=>$poster,
            'language'=>trans('video::player.language'),
            'strings'=>[
                'live'=>trans('video::player.live'),
                'back_to_live'=>trans('video::player.back_to_live'),
                'playback_not_supported'=>trans('video::player.playback_not_supported'),
            ],
            'maxBufferLength'=>'1',
            'ratio'=>$ratio,
            'track'=>$this->canTrackVideo(),
        ];
        if($video->status_convert=='finished' && $video->info){
            $data['levelSelectorConfig']=$this->getVideoSelectorConfig($video->info);
            $data['levelSizeConfig']=$this->getVideoQualitiesSizeConfig($video->info);
        }
        if($video->status_convert=='finished' && $video->info && $video_use->downloadable){
            $data['qualityDownloaderConfig']=$this->getVideoDownloaderConfig($video->info,$video_use->video_id.'/'.$video_use->hash_key);
        }
        if($video->status_convert=='finished' && $video->video_time > 5){
            $data['overlayButtons']=$this->getVideoQuestions($video,$video_use);
        }
        return $this->setPlugins($data);
    }

    public function getVideoSelectorConfig($info){
        $selectors=[];
        if(isset($info->info->qualities)){
            $selectors=['title'=>'Quality','labels'=>[]];
            $qualities=(array)$info->info->qualities;
            asort($qualities);
            foreach ($qualities as $name=>$size){
                $selectors['labels'][]=$name.'p';
            }
        }
        return $selectors;
    }

    public function getVideoQualitiesSizeConfig($info){
        $downloader=[];
        if(isset($info->info->qualities)){
            $downloader=[];
            $qualities=(array)$info->info->qualities;
            asort($qualities);
            foreach ($qualities as $name=>$size){
                $name=$name.'p';
                $sizeExtra=0;
                if($size){
                    $sizeExtra=$size*10/100;
                }
                $size=$this->getVideoSizeString($size+$sizeExtra);
                $downloader[]=['name'=>$name,'size'=>$size];
            }
        }
        return $downloader;
    }

    public function getVideoDownloaderConfig($info,$hashkey){
        $downloader=[];
        if(isset($info->info->qualities)){
            $downloader=['title'=>' ','links'=>[]];
            $qualities=(array)$info->info->qualities;
            asort($qualities);
            foreach ($qualities as $name=>$size){
                $link=url('video/play/download/'.$this->getPlayConfigName().'/'.$hashkey.'/'.$name.'p.mp4');
                $size=$this->getVideoSizeString($size);
                $text=$name.'p'.' - '.$size ;
                $downloader['links'][]=array('label'=>$text,'url'=>$link,'size'=>$size,'quality'=>$name.'p');
            }
        }
        return $downloader;
    }

    public function setPlugins($data){
        $data['plugins']=[];
        if(isset($data['levelSelectorConfig']) && count($data['levelSelectorConfig'])){
            if(!isset($data['plugins']['core'])){
                $data['plugins']['core']=[];
            }
            $data['plugins']['core'][]='LevelSelector';
        }
        if(isset($data['qualityDownloaderConfig']) && count($data['qualityDownloaderConfig'])){
            if(!isset($data['plugins']['core'])){
                $data['plugins']['core']=[];
            }
            $data['plugins']['core'][]='QualityDownloader';
        }
        if(isset($data['overlayButtons']) && strlen($data['overlayButtons'])){
            if(!isset($data['plugins']['container'])){
                $data['plugins']['container']=[];
            }
            $data['plugins']['container'][]='OverlayButtons';
        }
        return $data;
    }

    public function getVideoSizeString($bytes){
        if ($bytes >= 1073741824){
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }elseif ($bytes >= 1048576){
            $bytes = number_format($bytes / 1048576, 0) .  ' MB';
        }elseif ($bytes >= 1024){
            $bytes = number_format($bytes / 1024, 0) .  ' KB';
        }else{
            $bytes = $bytes . ' b';
        }
        return $bytes;
    }

    public function getVideoObject($video_id){
        $key='video_videos_id_'.$video_id;
        $video=Cache::get($key);
        if(!$video){
            $video=Video::find($video_id);
            if($video && !empty($video->info)){
                $poster=$video->poster_url;
                $video=(object) $video->toArray();
                $video->poster_url=$poster;
                Cache::set($key,$video,14400);
            }
        }
        return $video;
    }


    public function getVideoPlayerError($json){
        if($json){
            return ['error'=>trans('video::video.video.not_find')];
        }
        return view('core::component.message',['error'=>trans('video::video.video.not_find')]);
    }

    public function getVideoPlayerAssets(){
        return [
            [ 'container'=>'up_js','src' => "/plugins/clapper/clappr.min.js", 'name' => 'clapper'],
            //[ 'container'=>'up_js','src' => "/plugins/clapper/video-func.js?timeupdate=213325", 'name' => 'video-func'],
        ];
    }

    public function getPlayerHost($video_id){
        return url('/video/play/farayad/'.$video_id.'/');
    }

    public function canTrackVideo(){
        $instance=app('getInstanceObject')->getCurrentInstance();
        if($instance->name=='www' || $instance->name=='kanoon'){
            return true;
        }
        return false;
    }
}
