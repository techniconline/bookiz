<?php

namespace Modules\Video\Traits\Video;


trait VideoUse
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [

        ];
        return $this;
    }

    /**
     * @param $value
     * @return string
     */
    public function getPosterAttribute($value)
    {
        return $value;
    }

    /**
     * @return string
     */
    public function getHashKey()
    {
        $strRand = str_random(16);
        $last_video = \Modules\Video\Models\Video\VideoUse::orderBy('id', 'DESC')->first();
        $hash_id = $last_video ? $last_video->id + 1 : 1;
        $hashKey = sha1($strRand.$hash_id);
        return $hashKey;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilter($query)
    {
        $query->where(function ($q){
            if ($video_id = request()->get('video_id')){
                $q->where('video_id', $video_id);
            }

            if ($hash_key = request()->get('hash_key')){
                $q->where('hash_key', $hash_key);
            }
        });
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsPublic($query)
    {
        $query = $query->where('is_public', '=', 1);
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeDownloadable($query)
    {
        $query = $query->where('downloadable', '=', 1);
        return $query;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function video()
    {
        return $this->belongsTo('Modules\Video\Models\Video\Video');
    }


    public function getOptionsAttribute()
    {
        if(!$this->attributes['options']){
            $new=new \stdClass();
            $new->vote=0;
            $new->random_questions=0;
            return $new;
        }
        return json_decode($this->attributes['options']);
    }

    public function getOptionsArrayAttribute()
    {
        if(!$this->attributes['options']){
            return [];
        }
        return json_decode($this->attributes['options'],true);
    }

    public function setOptionsAttribute( $val )
    {
        if(!is_array($val) && !is_object($val)){
            $this->attributes['options'] =  $val ;
        }else{
            $this->attributes['options'] = json_encode( $val );
        }
    }

}
