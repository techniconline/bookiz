<?php

namespace Modules\Video\Traits\Video;


trait VideoUseTrait
{
    public $videoUse=false;
    public $model = "none";
    public $model_id = 0;
    public $video_id = 0;
    public $instance_id = 0;
    public $is_public = 0;
    public $video_use_id = 0;
    public $insertActive = true;


    /**
     * @return $this
     */
    public function init()
    {
        $this->setVideoUseModel();

        return $this;
    }

    /**
     * @return $this
     */
    public function setVideoUseModel($videoUse=null)
    {
        if($videoUse){
            $this->videoUse=$videoUse;
        }else{
            $this->videoUse = new \Modules\Video\Models\Video\VideoUse();
        }
        return $this;
    }

    /**
     * @param $model
     * @param null $model_id
     * @return $this
     */
    public function setModel($model, $model_id = null)
    {
        $this->model = $model;
        $this->model_id = $model_id;
        return $this;
    }

    /**
     * @param $video_id
     * @return $this
     */
    public function setVideoId($video_id)
    {
        $this->video_id = $video_id;
        return $this;
    }

    /**
     * @param int $is_public
     * @return $this
     */
    public function setIsPublic($is_public = 1)
    {
        $this->is_public = $is_public;
        return $this;
    }


    /**
     * @param $instance_id
     * @return $this
     */
    public function setInstanceId($instance_id)
    {
        $this->instance_id = $instance_id;
        return $this;
    }

    /**
     * @param array $configs
     * @return $this
     */
    public function setConfigs(array $configs)
    {
        foreach ($configs as $key => $config) {
            $this->{$key} = $config;
        }
        return $this;
    }

    /**
     * @return array
     */
    public function save()
    {
        $params = [];
        if ($this->model && $this->model_id) {
            $this->videoUse = $this->videoUse->active()->where("model", $this->model)->where("model_id", $this->model_id)->first();
            if (!$this->videoUse) {
                $this->setVideoUseModel();
            } else {
                $video_use_id = $this->videoUse->id;
                $params[] = ["model" => $this->model, "model_id" => $this->model_id];
                $this->video_use_id = $video_use_id;
                $this->delete();
                $this->setVideoUseModel();
            }

        }

        $params = [
            "model" => $this->model
            , "model_id" => $this->model_id
            , "video_id" => $this->video_id
            , "instance_id" => $this->instance_id
            , "hash_key" => $this->videoUse->getHashKey()
        ];

        if (($this->videoUse->fill($params)->isValid()) && $this->insertActive) {
            $this->videoUse->save();
            return ["action" => true, "video_use" => $this->videoUse];
        }

        return ["action" => false, "errors" => $this->videoUse->errors];
    }

    /**
     * @return array
     */
    public function delete()
    {
        $videoUse = null;
        if ($this->video_use_id) {
            $videoUse = $this->videoUse->find($this->video_use_id);
        }elseif ($this->model && $this->model_id) {
            $videoUse = $this->videoUse->where("model", $this->model)->where("model_id", $this->model_id)->first();
        }

        if ($videoUse && $this->video_id != $videoUse->video_id) {
            $res = $videoUse->delete();
            return ["action" => boolval($res)];
        } else {
            $this->insertActive = false;
            return ["action" => false];
        }
    }

    /**
     * @return mixed
     */
    public function get()
    {
        if($this->videoUse){
            return $this->videoUse;
        }
        return $this->setVideoUseModel()->videoUse->active()->where("model", $this->model)->where("model_id", $this->model_id)->with("video")->first();
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function getViewVideoForm()
    {
        $videoUse = $this->get();
        return view("video::video_use.video_show", compact("videoUse"))->render();
    }




}
