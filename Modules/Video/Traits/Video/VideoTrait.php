<?php

namespace Modules\Video\Traits\Video;


trait VideoTrait
{

    public $video;

    public function init()
    {
        $this->setVideoModel();

        return $this;
    }

    /**
     * @return $this
     */
    public function setVideoModel()
    {
        $this->video = new \Modules\Video\Models\Video\Video();
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVideoModel()
    {
        if(!$this->video){
            $this->setVideoModel();
        }
        return $this->video;
    }

}
