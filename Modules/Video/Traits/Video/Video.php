<?php

namespace Modules\Video\Traits\Video;


use Illuminate\Support\Facades\Storage;

trait Video
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'title.required' => trans('video::errors.video.title_required'),
            'language_id.required' => trans('video::errors.video.language_id_required'),
            'instance_id.required' => trans('video::errors.video.instance_id_required'),
            'extension.required' => trans('video::errors.video.extension_required'),
            'hash_key.required' => trans('video::errors.video.hash_key_required'),

        ];
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getInfoArrayAttribute()
    {
        if(strlen($this->attributes['info'])){
            return json_decode($this->attributes['info'],true);
        }
        return null;
    }


    /**
     * @return mixed|null
     */
    public function getParamsJsonAttribute()
    {
        $json = [
            'access_rate'=>'all',
            'access_comment'=>'all',
        ];
        if($json){
            return json_decode(json_encode($json));
        }
        return null;
    }

    /**
     * @return mixed|null
     */
    public function getInfoAttribute()
    {
        if(strlen($this->attributes['info'])){
            return json_decode($this->attributes['info']);
        }
        return null;
    }

    /**
     * @return null
     */
    public function getInfoTextAttribute()
    {
        if(strlen($this->attributes['info'])){
            return $this->attributes['info'];
        }
        return null;
    }

    /**
     * @return float|int
     */
    public function getVideoTimeMinutesByFormatAttribute()
    {
        if($value = $this->video_time){
            $fmod = fmod($value , 60);
            $int = (int)($value/60);
            return $int.":".$fmod;
        }
        return "0:0";
    }

    /**
     * @return int
     */
    public function getVideoTimeMinutesAttribute()
    {
        if($value = $this->video_time){
            $int = (int)ceil($value/60);
            return $int;
        }
        return 0;
    }

    /**
     * @return int|string
     */
    public function getVideoTimeHoursByFormatAttribute()
    {
        if($value = $this->video_time){
            $fmod_m = fmod($value , 60);
            $int_m = (int)($value/60);
            $fmod = fmod($int_m , 60);
            $int = (int)($int_m/60);
            return $int.":".$fmod.":".$fmod_m;
        }
        return "0:0";
    }

    /**
     * @return string
     */
    public function getHashKey()
    {
        $strRand = str_random(16);
        $last_video = \Modules\Video\Models\Video\Video::orderBy('id', 'DESC')->first();
        $hash_id = $last_video ? $last_video->id + 1 : 1;
        $hashKey = sha1($strRand . $hash_id);
        return $hashKey;
    }

    /**
     * @return mixed|string
     */
    public function getVideoUrlAttribute()
    {
        $url = '';
        if (($hash_key = $this->hash_key) && (strlen($this->extension) > 1)) {
            $url = $this->baseVideoUpload . '/' . $hash_key . '/' . $hash_key . '.' . $this->extension;
            $url = asset("/storage/" . $url);
        }
        return $url;
    }

    /**
     * @return mixed|string
     */
    public function getPosterUrlAttribute()
    {
        $url = '';
        if (($hash_key = $this->hash_key) && (int)$this->has_poster) {
            $folderUpload = substr($hash_key, 0, 2) . DIRECTORY_SEPARATOR;
            $folderUpload .= substr($hash_key, 2, 2) . DIRECTORY_SEPARATOR;
            $folderUpload .= $hash_key . DIRECTORY_SEPARATOR;
            $folderUpload .= "vids" . DIRECTORY_SEPARATOR . "origin";
            $url =  $folderUpload . DIRECTORY_SEPARATOR . $hash_key . '.jpg';
            $url = ("/storage/vod/" . $url);
        }
        return $url;
    }

    /**
     * @param $value
     * @return string
     */
    public function getStatusConvertAttribute($value)
    {
        return $value?:'-';
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        $query = parent::scopeActive($query);
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeHasPoster($query)
    {
        $query = $query->where('has_poster', '=', 1);
        return $query;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getHasPosterTextAttribute()
    {
        return trans('video::video.has_poster_text.' . ((int)$this->has_poster));
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getShowPosterTextAttribute()
    {
        return trans('video::video.show_poster_text.' . ((int)$this->show_poster));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Models\User\User');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function metaGroups()
    {
        return $this->belongsToMany('Modules\Core\Models\Meta\MetaGroup', 'video_meta_groups');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function videoUses()
    {
        return $this->hasMany('Modules\Video\Models\Video\VideoUse');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function videoMetaGroups()
    {
        return $this->hasMany('Modules\Video\Models\Video\VideoMetaGroup');
    }

}
