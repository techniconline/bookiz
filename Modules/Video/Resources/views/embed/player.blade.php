<html>
<head>
    @php $player=BridgeHelper::getVideoUse()->setVideoUseModel($videoUse) @endphp
    @foreach($player->getVideoPlayerAssets() as $asset)
        @if(strpos($asset['src'],'.js')!==false)
            <script src="{!! url($asset['src']) !!}"></script>
        @else
            <link rel="stylesheet" href="{!! url($asset['src']) !!}">
        @endif
    @endforeach
    <style type="text/css">
        body,.display-block{
            direction: rtl;
            text-align: right;
            height: 100%;
            margin: 0px;
            padding: 0px;
            position: absolute;
            width: 100%;
        }
        svg.poster-icon {
            background: #000;
            border-radius: 30px;
            padding: 21px;
        }
    </style>
</head>
<body>
    {!! $player->getPlayer() !!}
</body>
</html>