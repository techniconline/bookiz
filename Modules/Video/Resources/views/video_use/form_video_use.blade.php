<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("video::video.video_use.".($videoUse?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">
                {!! FormHelper::open(['role'=>'form','url'=>(isset($videoUse) && $videoUse? route('video.videoUse.update', ['video_id'=>$videoUse->id]) : route('video.videoUse.save'))
                     ,'method'=>(isset($videoUse) && $videoUse?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                @if($videoUse)
                {!! FormHelper::input('text','hash_key',old('hash_key',$videoUse?$videoUse->hash_key:null)
                ,["readonly"=>"readonly",'title'=>trans("video::video.video_use.hash_key"),'helper'=>trans("video::video.video_use.hash_key")]) !!}

                {!! FormHelper::input('text','model',old('model',$videoUse?$videoUse->model:null)
                ,["readonly"=>"readonly",'title'=>trans("video::video.video_use.model"),'helper'=>trans("video::video.video_use.model")]) !!}

                {!! FormHelper::input('text','model_id',old('model_id',$videoUse?$videoUse->model_id:null)
                ,["readonly"=>"readonly",'title'=>trans("video::video.video_use.model_id"),'helper'=>trans("video::video.video_use.model_id")]) !!}

                {!! FormHelper::inputByButton('poster',old('poster',$videoUse?$videoUse->poster:null),['type'=>'image','title'=>trans('video::video.video_use.poster'),'helper'=>trans('video::video.video_use.poster')],trans('video::video.video_use.select_file'),[],true) !!}
                @endif

                {!! FormHelper::select('instance_id',$instances,$videoUse?$videoUse->instance_id:null
                ,['title'=>trans("video::video.video_use.select_instance"),'helper'=>trans("video::video.video_use.select_instance")
                ,'placeholder'=>trans("video::video.video_use.select_instance")]) !!}

                {!! FormHelper::selectTag('video_id',[],old('video_id',$videoUse?$videoUse->video_id:null),['data-ajax-url'=>route('video.getVideoList')
                     , 'title'=>trans("video::video.video_use.select_video"),'helper'=>trans("video::video.video_use.select_video")]) !!}

                {!! FormHelper::checkbox('is_public',1,old('is_public',$videoUse?$videoUse->is_public:null)
                ,['title'=>trans("video::video.video_use.is_public")]) !!}

                {!! FormHelper::checkbox('downloadable',1,old('downloadable',$videoUse?$videoUse->downloadable:null)
                ,['title'=>trans("video::video.video_use.downloadable")]) !!}

                {!! FormHelper::checkbox('options[vote]',1,old('options[vote]',(isset($videoUse->options->vote))?$videoUse->options->vote:null)
                ,['title'=>trans("video::video.video_use.vote")]) !!}

                {!! FormHelper::checkbox('options[comment]',1,old('options[comment]',(isset($videoUse->options->comment))?$videoUse->options->comment:null)
,['title'=>trans("video::video.video_use.comment")]) !!}


                {!! FormHelper::selectRange('options[random_questions]',0,20,old('options[random_questions]',(isset($videoUse->options->random_questions))?$videoUse->options->random_questions:null)
,['title'=>trans("video::video.video_use.random_questions")]) !!}

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("video::video.video_use.submit")], ['title'=>trans("video::video.video_use.cancel"), 'url'=>route('video.videoUse.index')]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}


            </div>


        </div>

    </div>

</div>

<div>

</div>
