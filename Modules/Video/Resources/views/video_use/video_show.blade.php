@if($videoUse && $videoUse->video)
    <video width="320" controls>
        <source src="{!! $videoUse->video->video_url !!}" type="video/mp4">
    </video>
@endif
