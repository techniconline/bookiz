<div class="col-md-12" id="user.instance.config">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cog"></i>
                {{ $BlockViewModel->getTitlePage() }}
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('video.config.video.save'),'method'=>'POST','class'=>'form-horizontal','id'=>'config_form']) !!}
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button> @lang('user::validation.form.error')
            </div>
            {!! FormHelper::legend(trans('video::video.config.video_convert_config')) !!}
            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp
            {!! FormHelper::inputByButton('default_poster',old('default_poster',$BlockViewModel->getSetting('default_poster')),['type'=>'image','title'=>trans('course::form.fields.default_poster'),'helper'=>trans('core::form.helper.image_url')],trans('course::form.fields.default_poster'),[],true) !!}

            {{--{!! FormHelper::input('text','url_server',old('url_server',$BlockViewModel->getSetting('url_server')) ,['required'=>'required','title'=>trans("video::video.config.title_url_server"),'helper'=>"http://www.farayad.org/hls_protected/"]) !!}--}}
            {{--{!! FormHelper::input('text','playlist_pattern',old('playlist_pattern',$BlockViewModel->getSetting('playlist_pattern')) ,['title'=>trans("video::video.config.pattern_playlist_1"),'helper'=>"vid/\${name}/smil:video.smil/playlist.\${type}"]) !!}--}}
            {{--{!! FormHelper::input('text','rate_pattern',old('rate_pattern',$BlockViewModel->getSetting('playlist_pattern')) ,['title'=>trans("video::video.config.pattern_rate"),'helper'=>"vids/\${name}-\${rate}p.mp4_chunk.\${type}?nimblesessionid=\${sessid}"]) !!}--}}
            {{--{!! FormHelper::input('text','chunk_pattern',old('chunk_pattern',$BlockViewModel->getSetting('chunk_pattern')) ,['title'=>trans("video::video.config.pattern_chunk"),'helper'=>"\${name}-\${rate}p.mp4-n_\${chunk}.ts?nimblesessionid=\${sessid}"]) !!}--}}
            {{--{!! FormHelper::input('text','key_pattern',old('key_pattern',$BlockViewModel->getSetting('key_pattern')) ,['title'=>trans("video::video.config.pattern_key"),'helper'=>"vids/hls.key?nimblesessionid=\${sessid}"]) !!}--}}
            {{--{!! FormHelper::input('text','download_pattern',old('download_pattern',$BlockViewModel->getSetting('download_pattern')) ,['title'=>trans("video::video.config.pattern_download"),'helper'=>"/hls_download/\${name}/vids/\${name}-\${rate}p.mp4"]) !!}--}}
            {{--{!! FormHelper::input('text','url_api_hls',old('url_api_hls',$BlockViewModel->getSetting('url_api_hls')) ,['title'=>trans("video::video.config.url_api_hls"),'helper'=>"http://nimble:10101/v1/video/add2queue"]) !!}--}}
            {{--{!! FormHelper::input('text','url_api_get_video_details',old('url_api_get_video_details',$BlockViewModel->getSetting('url_api_get_video_details')) ,['title'=>trans("video::video.config.url_api_get_video_details"),'helper'=>"http://nimble:10101/v1/video/info/"]) !!}--}}
            {{--{!! FormHelper::input('text','path_source_video_convert',old('path_source_video_convert',$BlockViewModel->getSetting('path_source_video_convert')) ,['required'=>'required','title'=>trans("video::video.config.path_source_video_convert"),'helper'=>"/mnt/nfs/vids/src/"]) !!}--}}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitOnly() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>