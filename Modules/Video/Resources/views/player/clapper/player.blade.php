<div class="display-block">
    <div class="player" id="player_{{$id}}" dir="ltr" style="direction: ltr"></div>
</div>
<div class="clearfix"></div>
<script type="text/javascript" charset="utf-8">
    var video_player_host='{!! $host !!}';
    var player{{$id}} = new Clappr.Player({
        baseUrl:'/plugins/clapper/',
        source: "{{ $source }}",
        poster: "{{ $poster }}",
        autoplay:true,
        @if(count($plugins))
        plugins: {
            @foreach($plugins as $name=>$allplugins)
            "{{$name}}":[{!!implode(',',$allplugins)!!}],
            @endforeach
        },
        @endif
        language: "{{$language}}",
        strings: {
            "{{$language}}": {
                @foreach($strings as $name=>$trans)
                "{{$name}}": "{{$trans}}",
                @endforeach
            }
        },
        hlsjsConfig: {
            maxMaxBufferLength:10,
        },
        maxBufferLength:{{$maxBufferLength}},
        @if(isset($levelSelectorConfig) && count($levelSelectorConfig))
        levelSelectorConfig:{!! json_encode($levelSelectorConfig) !!},
        @endif
        parentId: "#player_{{$id}}",
        @if(isset($qualityDownloaderConfig) && count($qualityDownloaderConfig))
        qualityDownloaderConfig:{!! json_encode($qualityDownloaderConfig) !!},
        @endif
        @if(isset($overlayButtons) && strlen($overlayButtons))
        OverlayButtons:{!! $overlayButtons !!},
        @endif
        @if(isset($track) && $track)
        events: {
            onPlay:  function(){ videoLogApi(this,"{!! $hash !!}","play");  },
            onPause: function(){ videoLogApi(this,"{!! $hash !!}","pause"); },
            onStop:  function(){ videoLogApi(this,"{!! $hash !!}","stop");  },
            onEnded: function(){ videoLogApi(this,"{!! $hash !!}","finish");   },
            onSeek:  function(){ videoLogApi(this,"{!! $hash !!}","seek");  },
            onTimeUpdate: function(){ videoLogApi(this,"{!! $hash !!}","timeupdate");  },
        }
        @endif
    });

    player{{$id}}.listenTo(player{{$id}}.core.getCurrentPlayback(), Clappr.Events.PLAYBACK_BITRATE, set_player_bitrate);

    function resizePlayer_{{$id}}(){
        var aspectRatio = '{{$ratio}}',
        newWidth = document.getElementById("player_{{$id}}").offsetWidth,
        newHeight = 2 * Math.round(newWidth * aspectRatio/2);
        player{{$id}}.resize({width: newWidth, height: newHeight});
    }

    resizePlayer_{{$id}}();
    addResizeEventsCrossBrowsers(resizePlayer_{{$id}});
</script>
