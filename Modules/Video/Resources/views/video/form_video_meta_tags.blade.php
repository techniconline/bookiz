<div class="row" id="form_feature">
    <div class="col-md-12">
        <a href="{!! route('video.index') !!}" class="btn btn-lg green circle-right margin-bottom-5"><i
                    class="fa fa-list"></i> @lang("video::video.list") </a>
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-video-camera"></i> <i class="fa fa-tags"></i> @lang("video::video.edit_meta_tags")
                    : {!! $video->title or null  !!}
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">
                {{--<form role="form"--}}
                {{--action="{!! isset($video) && $video? route('video.update', ['video_id'=>$video->id]) : route('video.save') !!}"--}}
                {{--method="POST">--}}
                {{--<input type="hidden" name="_method" value="{!! isset($video) && $video?'PUT':'POST' !!}">--}}
                {{--{!! csrf_field() !!}--}}

                <div class="form-body">
                    <div class="form-group">

                        @if(isset($metaGroups)&&$metaGroups)
                            <div class="row">

                                <div class="col-md-12">
                                    @foreach($metaGroups as $metaGroup)

                                        <div class="col-md-3">
                                            @php
                                              $checked = boolval(in_array($metaGroup->id,$metaGroupsActive));
                                            @endphp
                                            {!! FormHelper::checkbox('meta_group_ids[]',$metaGroup->id,$checked
                                            ,['title'=>$metaGroup->title,'disabled'=>'disabled']) !!}

                                        </div>

                                    @endforeach

                                </div>

                            </div>

                        @endif

                    </div>
                </div>
                {{--</form>--}}

            </div>


        </div>

        @if(isset($video) && $video && !empty($metaGroupsActive))

            @foreach($videoMetaGroups as $videoMetaGroup)

                <div class="portlet box blue-dark" style="margin: 5px">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-tags"></i> {!! $videoMetaGroup->group_name !!}
                        </div>
                        <div class="tools">
                            <a href="" class="expand"> </a>
                            {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}

                            <a href="{!! route('video.meta.copy',['video_id'=>$video->id, 'video_meta_group_id'=>$videoMetaGroup->id]) !!}"
                               class="_copy_video_meta_group"
                               title="@lang("video::video.copy_meta_group"): {!! $videoMetaGroup->group_name !!}"
                               data-confirm-message="@lang("video::video.copy_meta_group"): {!! $videoMetaGroup->group_name !!} ?">
                                <i class="fa fa-copy" style="color: white; font-size: 24px"></i> </a>

                            <a href="{!! route('video.meta.delete',['video_id'=>$video->id, 'video_meta_group_id'=>$videoMetaGroup->id]) !!}"
                               class="_del_video_meta_group"
                               title="@lang("video::video.del_meta_group"): {!! $videoMetaGroup->group_name !!}"
                               data-confirm-message="@lang("video::video.del_meta_group"): {!! $videoMetaGroup->group_name !!} ?">
                                <i class="fa fa-remove" style="color: orangered; font-size: 24px"></i> </a>
                        </div>
                    </div>

                    <div class="portlet-body form" style="display: none;">

                    {!! FormHelper::open(['role'=>'form','url'=>route('video.features.value.save', ['video_id'=>$video->id, 'video_meta_group_id'=>$videoMetaGroup->id])
                                ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                    @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp



                                <div class="col-md-6">

                                    {!! FormHelper::input('text','start_time',old('start_time',$videoMetaGroup?$videoMetaGroup->start_time:0)
                                            ,['title'=>trans("video::video.start_time"),'helper'=>trans("video::video.start_time")]) !!}

                                </div>

                                <div class="col-md-6">

                                    {!! FormHelper::input('text','end_time',old('end_time',$videoMetaGroup?$videoMetaGroup->end_time:0)
                                         ,['title'=>trans("video::video.end_time"),'helper'=>trans("video::video.end_time")]) !!}

                                </div>


                            @foreach($videoMetaGroup->metaGroup->featureGroups as $featureGroup)

                                @foreach($featureGroup->featureGroupRelations as $featureGroupRelation)

                                    @if($featureGroupRelation->feature)

                                        <?php

                                        $activeValue = null;
                                        if ($metaFeatureDataList && isset($metaFeatureDataList[$videoMetaGroup->id][$featureGroupRelation->feature->id])) {
                                            $activeValue = $metaFeatureDataList[$videoMetaGroup->id][$featureGroupRelation->feature->id];
                                        }

                                        ?>

                                        {!!
                                            $view_model->setFeatureGroup($featureGroup)
                                            ->setFeatureModel($featureGroupRelation->feature)
                                            ->setActiveValueFeature($activeValue)
                                            ->getHtml()
                                        !!}

                                    @endif

                                @endforeach

                            @endforeach


                        {!! FormHelper::openAction() !!}
                        {!! FormHelper::submitByCancel(['title'=>trans("video::video.submit"),'class'=>'btn btn green _save_meta'], ['title'=>trans("video::video.cancel"), 'url'=>'#']) !!}
                        {!! FormHelper::closeAction() !!}
                        {!! FormHelper::close() !!}


                    </div>
                </div>

            @endforeach

            <hr>

        @endif

    </div>

</div>

<div>

</div>
