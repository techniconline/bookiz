<div class="row" id="form_feature">
    <div class="col-md-12">
        <a href="{!! route("video.index") !!}" class="btn btn-success"> @lang("video::video.list") </a>

        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("video::video.add_videos")
                </div>
                <div class="tools">
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                </div>
            </div>

            <div class="portlet-body form">

                <div class="portlet box blue ">

                    <div class="portlet-body form">
                        <!-- Instantiating: -->
                        <div id="uploader_video">
                            {{--<p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>--}}
                        </div>

                        <script>
                            $('#uploader_video').plupload({
                                runtimes: 'html5,flash,silverlight,html4',
                                url: '{!! route('video.uploads') !!}',
                                filters: [
                                    {title: "Video files", extensions: "mp4,mov,avi,mpg,vob,mpeg,ts,mkv,wmv"},
                                ],
                                headers: {
                                    "Accept": "application/json",
                                    "X-CSRF-TOKEN": "{!! csrf_token() !!}"
                                },
                                views: {
                                    list: false,
                                    thumbs: true, // Show thumbs
                                    active: 'thumbs'
                                },
                                multipart_params: {"is_ajax": true},
                                chunk_size: "1000kb",
                                file_data_name: "file",
                                multipart: true,
                                multi_selection: true,
                                dragdrop: true,

                                // rename: true,
                                // sortable: true,
                                {{--flash_swf_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.swf") !!}',--}}
                                        {{--silverlight_xap_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.xap") !!}',--}}
                                init: {
                                    FilesAdded: function (up, files) {
                                        // console.log('FilesAdded');

                                        var max_files = 50;
                                        plupload.each(files, function (file) {
                                            if (up.files.length > max_files) {
                                                alert('{!! trans("core::messages.alert.maxFileUpload",['max_upload'=>50]) !!}');
                                                up.removeFile(file);
                                            }
                                        });
                                        if (up.files.length >= max_files) {
                                            $('#pickfiles').hide('slow');
                                        }


                                    },
                                    FileUploaded: function (up, file, response) {
                                        // console.log('FileUploaded');
                                    },
                                    UploadComplete: function (up, files) {
                                        // console.log('UploadComplete');
                                    },
                                    FilesRemoved: function (up, files) {
                                        // console.log('Destroy');
                                    },
                                    BeforeUpload: function (up, files) {
                                        // console.log('BeforeUpload',files);
                                    },
                                    Init: function ($img_object, file, response) {

                                    }
                                }
                            });
                        </script>
                    </div>

                </div>

            </div>


        </div>

    </div>

</div>

<div>

</div>
