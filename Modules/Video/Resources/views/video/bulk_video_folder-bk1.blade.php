@if(isset($folder) && $folder)

        <div class="col-md-12 margin-top-10 _folder" data-id="{!! md5($folder["src"]) !!}">
            <label class="mt-checkbox mt-checkbox-outline"> {!! $folder["src"] !!}
                ( @lang("video::video.bulk_count_videos"): {!! $folder["count_files"] !!} )
                <input data-index="{!! $index !!}" type="radio" value="{!! $folder["src"] !!}" name="folder"/>
                <a data-confirm-message="@lang("video::video.confirm_delete")" data-directory="{!! $folder["src"] !!}"
                   href="{!! route('video.deleteBulkDirectory') !!}"
                   class="btn btn-danger btn-xs _delete_directory">@lang("video::video.delete_directory")</a>
                <span></span>
            </label>
            <div class="_files">
                @if($folder['files'])
                    @foreach($folder['files'] as $file)
                        <input data-uploaded="0" type="hidden" class="_file" name="file[]" value="{!! $file !!}">
                    @endforeach
                @endif
            </div>
            <div class="_messages"></div>
            @if(isset($folder['childes']) && $folder['childes'])
            <div class="_childes" data-parent="{!! md5($folder["src"]) !!}">
                @foreach($folder['childes'] as $folder)
                    @include('video::video.bulk_video_folder')
                @endforeach
            </div>
            @endif
        </div>

@endif