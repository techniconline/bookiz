<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("video::video.bulk_folder_list")
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">
                <form role="form"
                      action="{!! route('video.copyBulkVideo') !!}"
                      method="POST">
                    <input type="hidden" name="_method" value="POST">

                    {!! csrf_field() !!}
                    <div class="form-body">
                        <div class="form-group">

                            <div class="row">

                                @if(isset($listFolders) && $listFolders)
                                    @foreach($listFolders as $index => $folder)

                                        @include('video::video.bulk_video_folder')

                                    @endforeach
                                @endif
                            </div>


                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-6 _language_list">
                                <a href="{!! route('video.index') !!}"
                                   class="btn default _cancel">@lang("video::video.cancel")</a>
                                <button type="submit"
                                        class="btn green _copy_files">@lang("video::video.copy")</button>
                            </div>
                        </div>


                    </div>
                </form>

            </div>


        </div>

    </div>


</div>

