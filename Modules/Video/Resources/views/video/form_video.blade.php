<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("video::video.".($video?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">
                {!! FormHelper::open(['role'=>'form','url'=>(isset($video) && $video? route('video.update', ['video_id'=>$video->id]) : route('video.save'))
                     ,'method'=>(isset($video) && $video?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                {!! FormHelper::input('text','title',old('title',$video?$video->title:null)
                ,['required'=>'required','title'=>trans("video::video.title"),'helper'=>trans("video::video.title")]) !!}

                {!! FormHelper::select('instance_id',$instances,$video?$video->instance_id:null
                ,['title'=>trans("core::feature.select_instance"),'helper'=>trans("core::feature.select_instance")
                ,'placeholder'=>trans("core::feature.select_instance")]) !!}

                {{--{!! FormHelper::checkbox('is_public',1,old('is_public',$video?$video->is_public:null)--}}
                {{--,['title'=>trans("video::video.is_public")]) !!}--}}

                {!! FormHelper::checkbox('show_poster',1,old('show_poster',$video?$video->show_poster:null)
                ,['title'=>trans("video::video.show_poster")]) !!}

                {{--{!! FormHelper::checkbox('downloadable',1,old('downloadable',$video?$video->downloadable:null)--}}
                {{--,['title'=>trans("video::video.downloadable")]) !!}--}}

                @if($video)
                {!! FormHelper::input('text','hash_key',old('hash_key',$video?$video->hash_key:null)
                ,['disabled'=>'disabled','title'=>trans("video::video.hash_key"),'helper'=>trans("video::video.hash_key")]) !!}

                {!! FormHelper::input('text','has_poster',old('has_poster',$video?$video->has_poster_text:null)
                ,['disabled'=>'disabled','title'=>trans("video::video.has_poster"),'helper'=>trans("video::video.has_poster")]) !!}

                {!! FormHelper::input('text','poster',old('poster',$video?$video->poster_url:null)
                ,['disabled'=>'disabled','title'=>trans("video::video.poster"),'helper'=>trans("video::video.poster")]) !!}

                {!! FormHelper::input('text','status_convert',old('status_convert',$video?$video->status_convert:'-')
                ,['disabled'=>'disabled','title'=>trans("video::video.status_convert"),'helper'=>trans("video::video.status_convert")]) !!}

                {!! FormHelper::input('text','video_time',old('video_time',$video?$video->video_time:'-')
                ,['disabled'=>'disabled','title'=>trans("video::video.video_time"),'helper'=>trans("video::video.video_time")]) !!}

                {!! FormHelper::textarea('info',old('description',$video?$video->info_text:null)
                ,['disabled'=>'disabled','title'=>trans("video::video.info"),'helper'=>trans("video::video.info"), "dir"=>"ltr"]) !!}

                @endif

                @if(isset($metaGroups)&&$metaGroups)
                    <hr>

                    {!! FormHelper::selectTag('meta_group_ids[]',$metaGroups,old('meta_group_ids',$metaGroupsActive?$metaGroupsActive:null),['multiple'=>'multiple'
                         , 'title'=>trans("video::video.meta_groups"),'helper'=>trans("video::video.meta_groups")]) !!}


                @endif

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancelWithLanguage(['title'=>trans("video::video.submit")], ['title'=>trans("video::video.cancel"), 'url'=>route('video.index')], ['selected'=>$video?$video->language_id:null]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}


            </div>


        </div>

        @if(isset($video) && $video)
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i> @lang("video::video.upload")
                    </div>
                    {{--<div class="tools">--}}
                    {{--<a href="" class="collapse"> </a>--}}
                    {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                    {{--<a href="" class="reload"> </a>--}}
                    {{--<a href="" class="remove"> </a>--}}
                    {{--</div>--}}
                </div>

                <div class="portlet-body form">
                    <!-- Instantiating: -->
                    <div id="uploader_video">
                        {{--<p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>--}}
                    </div>

                    <script>
                        $('#uploader_video').plupload({
                            runtimes: 'html5,flash,silverlight,html4',
                            url: '{!! route('video.upload', ['video_id'=>$video->id]) !!}',
                            filters: [
                                {title: "Video files", extensions: "mp4,mov,avi,mpg,vob,mpeg,ts,mkv,wmv"},
                            ],
                            headers: {
                                "Accept": "application/json",
                                "X-CSRF-TOKEN": "{!! csrf_token() !!}"
                            },
                            views: {
                                list: false,
                                thumbs: true, // Show thumbs
                                active: 'thumbs'
                            },
                            multipart_params: {"is_ajax": true},
                            chunk_size: "1000kb",
                            file_data_name: "file",
                            multipart: true,
                            multi_selection: true,
                            dragdrop: true,

                            // rename: true,
                            // sortable: true,
                            {{--flash_swf_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.swf") !!}',--}}
                                    {{--silverlight_xap_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.xap") !!}',--}}
                            init: {
                                FilesAdded: function (up, files) {
                                    // console.log('FilesAdded');

                                    var max_files = 1;
                                    plupload.each(files, function (file) {
                                        if (up.files.length > max_files) {
                                            alert('{!! trans("core::messages.alert.maxFileUpload",['max_upload'=>1]) !!}');
                                            up.removeFile(file);
                                        }
                                    });
                                    if (up.files.length >= max_files) {
                                        $('#pickfiles').hide('slow');
                                    }


                                },
                                FileUploaded: function (up, file, response) {
                                    // console.log('FileUploaded');
                                },
                                UploadComplete: function (up, files) {
                                    // console.log('UploadComplete');
                                },
                                FilesRemoved: function (up, files) {
                                    // console.log('Destroy');
                                },
                                BeforeUpload: function (up, files) {
                                    // console.log('BeforeUpload',files);
                                },
                                Init: function ($img_object, file, response) {

                                }
                            }
                        });
                    </script>
                </div>
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i> @lang("video::video.upload_poster")
                    </div>
                    {{--<div class="tools">--}}
                    {{--<a href="" class="collapse"> </a>--}}
                    {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                    {{--<a href="" class="reload"> </a>--}}
                    {{--<a href="" class="remove"> </a>--}}
                    {{--</div>--}}
                </div>

                <div class="portlet-body form">

                    <div id="uploader_image">
                        {{--<p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>--}}
                    </div>

                    <script>

                        $('#uploader_image').plupload({
                            runtimes: 'html5,flash,silverlight,html4',
                            url: '{!! route('video.poster.upload', ['video_id'=>$video->id]) !!}',
                            filters: [
                                {title: "Image files", extensions: "jpg,png"},
                            ],
                            headers: {
                                "Accept": "application/json",
                                "X-CSRF-TOKEN": "{!! csrf_token() !!}"
                            },
                            views: {
                                list: false,
                                thumbs: true, // Show thumbs
                                active: 'thumbs'
                            },
                            multipart_params: {"is_ajax": true},
                            // chunk_size: "500kb",
                            file_data_name: "file_image",
                            multipart: true,
                            multi_selection: true,
                            dragdrop: true,

                            // rename: true,
                            // sortable: true,
                            {{--flash_swf_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.swf") !!}',--}}
                                    {{--silverlight_xap_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.xap") !!}',--}}
                            init: {
                                FilesAdded: function (up, files) {
                                    // console.log('FilesAdded',files.length);
                                    var max_files = 1;
                                    plupload.each(files, function (file) {
                                        if (up.files.length > max_files) {
                                            alert('{!! trans("core::messages.alert.maxFileUpload",['max_upload'=>1]) !!}');
                                            up.removeFile(file);
                                        }
                                    });
                                    if (up.files.length >= max_files) {
                                        $('#pickfiles').hide('slow');
                                    }

                                },
                                FileUploaded: function (up, file, response) {
                                    // console.log('FileUploaded');
                                },
                                UploadComplete: function (up, files) {
                                    // console.log('UploadComplete');
                                },
                                FilesRemoved: function (up, files) {
                                    // console.log('Destroy');
                                },
                                BeforeUpload: function (up, files) {
                                    // console.log('BeforeUpload');
                                },
                                Init: function ($img_object, file, response) {

                                }
                            }
                        });
                        // Invoking methods:
                        // $('#uploader').plupload(options);

                        // Display welcome message in the notification area
                        // $('#uploader_image').plupload('notify', 'info', "This might be obvious, but you need to click 'Add Files' to add some files.");

                        // Subscribing to the events...
                        // ... on initialization:
                    </script>
                </div>


            </div>
        @endif
    </div>

</div>

<div>

</div>
