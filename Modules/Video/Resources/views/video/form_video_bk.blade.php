<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("video::video.".($video?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">
                @if(isset($video) && $video)
                    <form role="form"
                          action="{!! route('video.update', ['video_id'=>$video->id]) !!}"
                          method="POST">
                        <input type="hidden" name="_method" value="PUT">
                        @else
                            <form role="form" action="{!! route('video.save') !!}" method="POST">
                                <input type="hidden" name="_method" value="POST">
                                @endif

                                {!! csrf_field() !!}
                                <div class="form-body">
                                    <div class="form-group">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-icon right">
                                                    <i class="fa fa-check"></i>
                                                    <input name="title" type="text" class="form-control"
                                                           value="{!! $video->title or null  !!}"
                                                           placeholder="@lang("video::video.title")">
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <label class="mt-checkbox mt-checkbox-outline"> @lang("video::video.is_public")
                                                    <input type="checkbox"
                                                           {!! $video && $video->is_public?'checked':'' !!} value="1"
                                                           name="is_public"/>
                                                    <span></span>
                                                </label>
                                            </div>

                                            <div class="col-md-4 ">
                                                <select name="instance_id" class="form-control input-medium">
                                                    <option value="0"> ... @lang("video::video.select_instance")...
                                                    </option>
                                                    @if(isset($instances) && $instances)
                                                        @foreach($instances as $instance)
                                                            <option {!! $video&&$video->instance_id==$instance->id? 'selected' : null  !!} value="{!! $instance->id !!}">{!! $instance->name !!}
                                                                - {!! $instance->language->name !!}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>

                                            <div class="col-md-12 margin-top-10">
                                                <label for="">@lang("video::video.info")</label>
                                                <textarea name="info" class="form-control" rows="5"></textarea>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-6 _language_list">
                                            <select name="language_id" class="btn form-control input-small">
                                                <option> ... @lang("video::video.select_language") ...</option>
                                                @if(isset($languages) && $languages)
                                                    @foreach($languages as $language)
                                                        <option {!! $video&&$video->language_id==$language->id? 'selected' : null  !!} value="{!! $language->id !!}">{!! $language->name !!}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <a href="{!! route('video.index') !!}"
                                               class="btn default _cancel">@lang("video::video.cancel")</a>
                                            <button type="submit"
                                                    class="btn green _save">@lang("video::video.submit")</button>
                                        </div>
                                    </div>


                                </div>
                            </form>

                            <div class="row">
                                <div class="col-md-12">
                                    {{--<div id="-container">--}}
                                        {{--<button type="button" id="-browse-button" class="btn btn-primary">Browse...--}}
                                        {{--</button>--}}
                                        {{--<button type="button" id="-start-upload" class="btn btn-success">Upload</button>--}}
                                    {{--</div>--}}

                                    {{--<div id="my_uploader_id">--}}

                                    {{--</div>--}}

                                </div>

                            </div>
            </div>

            <!-- Instantiating: -->
            <div id="uploader">
                {{--<p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>--}}
            </div>

            <script>
                $('#uploader').plupload({
                    runtimes : 'html5,flash,silverlight,html4',
                    url : 'http://admin.farayad.local/video/1/upload',
                    filters : [
                        {title : "Video files", extensions : "mp4"},
                        {title : "Image files", extensions : "jpg,png"}
                    ],
                    headers: {
                        // "Accept": "application\/json",
                        "X-CSRF-TOKEN": "{!! csrf_token() !!}"
                    },
                    views: {
                        list: false,
                        thumbs: true, // Show thumbs
                        active: 'thumbs'
                    },
                    multipart_params: {"is_ajax": true},
                    chunk_size: "500kb",
                    multipart : false,
                    multi_selection : false,
                    dragdrop : true,

                    // rename: true,
                    // sortable: true,
                    flash_swf_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.swf") !!}',
                    silverlight_xap_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.xap") !!}',
                    init: {
                        FilesAdded: function (up, files) {
                            console.log('FilesAdded');
                        },
                        FileUploaded: function (up, file, response) {
                            console.log('FileUploaded');
                        },
                        UploadComplete: function (up, files) {
                            console.log('UploadComplete');
                        },
                        FilesRemoved: function (up, files) {
                            console.log('Destroy');
                        },
                        BeforeUpload: function (up, files) {

                            console.log('BeforeUpload',files);
                        },
                        Init: function ($imgobjec, file, response) {
                            var $files = {};
                            $files = [{url:'test', name:'sadsad'}/*,{url:'test2', name:'sadsad2'},{url:'test3', name:'sadsad3'}*/];
                            var $list =  $('ul#uploader_filelist');

                            $.each($files, function ($i, $file) {
                                console.log($file);

                                var $rowHtml = '<li class="plupload_file ui-state-default plupload_delete plupload_thumb_embedded"\n' +
                                    '                id="o_'+$i+'" style="width:100px;">\n' +
                                    '                <div class="plupload_file_thumb" style="width: 100px; height: 60px;">\n' +
                                    '                    <div class="plupload_file_dummy ui-widget-content" style="line-height: 60px;"><span\n' +
                                    '                                class="ui-state-disabled">png </span></div>\n' +
                                    '                    <canvas width="100" height="29" id="uid_'+$i+'_canvas"></canvas>\n' +
                                    '                </div>\n' +
                                    '                <div class="plupload_file_status">\n' +
                                    '                    <div class="plupload_file_progress ui-widget-header" style="width: 0%"></div>\n' +
                                    '                    <span class="plupload_file_percent"> </span></div>\n' +
                                    '                <div class="plupload_file_name" title="logo_topArtboard-1@3x.png"><span\n' +
                                    '                            class="plupload_file_name_wrapper">logo_topArtboard-1@3x.png </span>\n' +
                                    '                </div>\n' +
                                    '                <div class="plupload_file_action">\n' +
                                    '                    <div class="plupload_action_icon ui-icon ui-icon-circle-minus"></div>\n' +
                                    '                </div>\n' +
                                    '                <div class="plupload_file_size">5 kb</div>\n' +
                                    '                <div class="plupload_file_fields">\n' +
                                    '                </div>\n' +
                                    '            </li>';

                                $list.append($rowHtml);

                            })
                        }
                }
                });

                // Invoking methods:
                // $('#uploader').plupload(options);

                // Display welcome message in the notification area
                // $('#uploader').plupload('notify', 'info', "This might be obvious, but you need to click 'Add Files' to add some files.");

                // Subscribing to the events...
                // ... on initialization:
                $('#uploader').plupload({

                    viewchanged : function(event, args) {

                    // stuff ...
                    }
                });
                // ... or after initialization
                $('#uploader').on("viewchanged", function(event, args) {
                    // stuff ...
                });
            </script>



            @if(isset($video1) && $video)
                <div class="row fix-margin margin-top-10">
                    <label for="">@lang("video::video.upload")</label>

                    <div class="col-md-12">
                        {{--{!!--}}
                        {{--Plupload::make([--}}
                        {{--'url' => route('video.upload', ['video_id'=>$video->id]),--}}
                        {{--'filters'=>[--}}
                        {{--'mime_types'=>[['title' => "Video files", 'extensions' => 'mp4,mov']]--}}
                        {{--],--}}
                        {{--'multipart_params'=>['is_ajax'=>true],--}}
                        {{--//'headers'=>['X-Requested-With'=>'XMLHttpRequest'],--}}
                        {{--'chunk_size' => '500kb',--}}
                        {{--])--}}
                        {{--!!}--}}

                        {!!

                         Plupload::init([
                                'url' => route('video.upload', ['video_id'=>$video->id]),
                                'chunk_size' => '100kb',
                            ])->withPrefix('current')->createHtml();

                         !!}
                    </div>
                </div>


            @endif
            @if(isset($video1) && $video)
                <div class="row fix-margin margin-top-10">
                    {{--<label for="">@lang("video::video.upload")</label>--}}

                    {{--<div class="col-md-12">--}}
                    {{--<form id="videoupload" action="{!! route('video.upload', ['video_id'=>$video->id]) !!}" method="POST" enctype="multipart/form-data">--}}
                    {{--{!! csrf_field() !!}--}}
                    {{--<input type="hidden" name="video_id" value="{!! $video->id !!}">--}}
                    {{--<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->--}}
                    {{--<div class="row fileupload-buttonbar">--}}
                    {{--<div class="col-lg-7">--}}
                    {{--<!-- The fileinput-button span is used to style the file input field as button -->--}}
                    {{--<span class="btn green fileinput-button">--}}
                    {{--<i class="fa fa-plus"></i>--}}
                    {{--<span> Add files... </span>--}}
                    {{--<input type="file" name="files[]" multiple=""> </span>--}}
                    {{--<button type="submit" class="btn blue start">--}}
                    {{--<i class="fa fa-upload"></i>--}}
                    {{--<span> Start upload </span>--}}
                    {{--</button>--}}
                    {{--<button type="reset" class="btn warning cancel">--}}
                    {{--<i class="fa fa-ban-circle"></i>--}}
                    {{--<span> Cancel upload </span>--}}
                    {{--</button>--}}
                    {{--<button type="button" class="btn red delete">--}}
                    {{--<i class="fa fa-trash"></i>--}}
                    {{--<span> Delete </span>--}}
                    {{--</button>--}}
                    {{--<input type="checkbox" class="toggle">--}}
                    {{--<!-- The global file processing state -->--}}
                    {{--<span class="fileupload-process"> </span>--}}
                    {{--</div>--}}
                    {{--<!-- The global progress information -->--}}
                    {{--<div class="col-lg-5 fileupload-progress fade">--}}
                    {{--<!-- The global progress bar -->--}}
                    {{--<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">--}}
                    {{--<div class="progress-bar progress-bar-success" style="width:0%;"> </div>--}}
                    {{--</div>--}}
                    {{--<!-- The extended global progress information -->--}}
                    {{--<div class="progress-extended"> &nbsp; </div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- The table listing the files available for upload/download -->--}}
                    {{--<table role="presentation" class="table table-striped clearfix">--}}
                    {{--<tbody class="files"> </tbody>--}}
                    {{--</table>--}}
                    {{--</form>--}}
                    {{--</div>--}}
                    <label for="">@lang("video::video.upload_poster")</label>

                    <div class="col-md-12">
                        <form id="imageoupload" action="{!! route('video.poster.upload', ['video_id'=>$video->id]) !!}"
                              method="POST" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        {{--<input type="hidden" name="video_id" value="{!! $video->id !!}">--}}
                        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                            <div class="row fileupload-buttonbar">
                                <div class="col-lg-7">
                                    <!-- The fileinput-button span is used to style the file input field as button -->
                                    <span class="btn green fileinput-button">
                                        <i class="fa fa-plus"></i>
                                        <span> Add files... </span>
                                        {{--<input type="file" name="files[]" multiple=""> </span>--}}
                                        <input type="file" name="file_image"> </span>
                                {{--<button type="submit" class="btn blue start">--}}
                                {{--<i class="fa fa-upload"></i>--}}
                                {{--<span> Start upload </span>--}}
                                {{--</button>--}}
                                {{--<button type="reset" class="btn warning cancel">--}}
                                {{--<i class="fa fa-ban-circle"></i>--}}
                                {{--<span> Cancel upload </span>--}}
                                {{--</button>--}}
                                {{--<button type="button" class="btn red delete">--}}
                                {{--<i class="fa fa-trash"></i>--}}
                                {{--<span> Delete </span>--}}
                                {{--</button>--}}
                                {{--<input type="checkbox" class="toggle">--}}
                                <!-- The global file processing state -->
                                    <span class="fileupload-process"> </span>
                                </div>
                                <!-- The global progress information -->
                                <div class="col-lg-5 fileupload-progress fade">
                                    <!-- The global progress bar -->
                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0"
                                         aria-valuemax="100">
                                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                    </div>
                                    <!-- The extended global progress information -->
                                    <div class="progress-extended"> &nbsp;</div>
                                </div>
                            </div>
                            <!-- The table listing the files available for upload/download -->
                            <table role="presentation" class="table table-striped clearfix">
                                <tbody class="files"></tbody>
                            </table>
                        </form>
                    </div>

                </div>

                <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                    <div class="slides"></div>
                    <h3 class="title"></h3>
                    <a class="prev"> ‹ </a>
                    <a class="next"> › </a>
                    <a class="close white"> </a>
                    <a class="play-pause"> </a>
                    <ol class="indicator"></ol>
                </div>
                <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
                <script id="template-upload" type="text/x-tmpl"> {% for (var i=0, file; file=o.files[i]; i++) { %}
                        <tr class="template-upload fade">
                            <td>
                                <span class="preview"></span>
                            </td>
                            <td>
                                <p class="name">{%=file.name%}</p>
                                <strong class="error text-danger label label-danger"></strong>
                            </td>
                            <td>
                                <p class="size">Processing...</p>
                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                </div>
                            </td>
                            <td> {% if (!i && !o.options.autoUpload) { %}
                                <button class="btn blue start" disabled>
                                    <i class="fa fa-upload"></i>
                                    <span>Start</span>
                                </button> {% } %} {% if (!i) { %}
                                <button class="btn red cancel">
                                    <i class="fa fa-ban"></i>
                                    <span>Cancel</span>
                                </button> {% } %} </td>
                        </tr> {% } %}


                </script>
                <!-- The template to display files available for download -->
                <script id="template-download" type="text/x-tmpl"> {% for (var i=0, file; file=o.files[i]; i++) { %}
                        <tr class="template-download fade">
                            <td>
                                <span class="preview"> {% if (file.thumbnailUrl) { %}
                                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery>
                                        <img src="{%=file.thumbnailUrl%}">
                                    </a> {% } %} </span>
                            </td>
                            <td>
                                <p class="name"> {% if (file.url) { %}
                                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl? 'data-gallery': ''%}>{%=file.name%}</a> {% } else { %}
                                    <span>{%=file.name%}</span> {% } %} </p> {% if (file.error) { %}
                                <div>
                                    <span class="label label-danger">Error</span> {%=file.error%}</div> {% } %} </td>
                            <td>
                                <span class="size">{%=o.formatFileSize(file.size)%}</span>
                            </td>
                            <td> {% if (file.deleteUrl) { %}
                                <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}" {% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}' {% } %}>
                                    <i class="fa fa-trash-o"></i>
                                    <span>Delete</span>
                                </button>
                                <input type="checkbox" name="delete" value="1" class="toggle"> {% } else { %}
                                <button class="btn yellow cancel btn-sm">
                                    <i class="fa fa-ban"></i>
                                    <span>Cancel</span>
                                </button> {% } %} </td>
                        </tr> {% } %}


                </script>
            @endif
        </div>
    </div>
</div>
</div>

<div>

</div>
