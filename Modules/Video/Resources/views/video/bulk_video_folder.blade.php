@if(isset($folder) && $folder)

        <li class="dd-item" data-id="{!! md5($folder["src"]) !!}">
            <div class="dd-handle1 _folder" style="background-color: whitesmoke; border: 1px solid darkgrey; padding: 2px; margin: 2px">
                <input class="" data-index="{!! $index !!}" type="radio" value="{!! $folder["src"] !!}" name="folder"/>
                {!! $folder["src"] !!}
                ( @lang("video::video.bulk_count_videos"): {!! $folder["count_files"] !!} )
                <a data-confirm-message="@lang("video::video.confirm_delete")" data-directory="{!! $folder["src"] !!}"
                   href="{!! route('video.deleteBulkDirectory') !!}"
                   class="btn btn-danger btn-xs _delete_directory">@lang("video::video.delete")</a>
                <span></span>
                @php $group = rand(111111111,999999999) @endphp
                <div class="_files" data-group="{!! $group !!}">
                    @if($folder['files'])
                    @php $count_files = count($folder['files']) @endphp
                    @foreach($folder['files'] as $index => $file)
                        <?php
                        $last = false;
                        if ($count_files==($index+1)){
                            $last = true;
                        }
                        ?>
                            <input data-uploaded="0" data-last-file="{!! $last?1:0 !!}" type="hidden" class="_file" name="file[]" value="{!! $file !!}">
                        @endforeach
                    @endif
                </div>
                <div class="_messages"></div>
            </div>

            @if(isset($folder['childes']) && $folder['childes'])
                <ol class="dd-list _childes" data-parent="{!! md5($folder["src"]) !!}">
                    @foreach($folder['childes'] as $folder)
                        @include('video::video.bulk_video_folder')
                    @endforeach
                </ol>
            @endif


        </li>

@endif