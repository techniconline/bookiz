<?php

return [
    'group' => [
        'videos'=>'مدیریت ویدیوها',
        'settings'=>'مدیریت تنظیمات',
    ],
    'video_list'=>'لیست ویدیوها',
    'video_use_list'=>'لیست ویدیوها مورد استفاده',
    'configs_video'=>'مدیریت تنظیمات ویدیو',


];