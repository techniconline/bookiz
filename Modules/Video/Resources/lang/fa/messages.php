<?php

return [
    'alert' => [
        'save_success'=>'عملیات ذخیره سازی با موفقیت انجام شد.',
        'update_success'=>'عملیات بروزرسانی سازی با موفقیت انجام شد.',
        'del_success'=>'عملیات حذف با موفقیت انجام شد.',
        'save_un_success'=>'عملیات ذخیره سازی با موفقیت انجام نشد.',
        'update_un_success'=>'عملیات بروزرسانی سازی با موفقیت انجام نشد.',
        'del_un_success'=>'عملیات حذف با موفقیت انجام نشد.',
        'mis_data' => 'اطلاعات ناقص است!',
        'not_find_data' => 'اطلاعات یافت نشد!',
        'find_data' => 'اطلاعات یافت نشد!',
        'info' => 'پیام سیستم',
        'error' => 'خطا در سیستم!',
        'send_request_convert_video' => 'درخواست کانورت ارسال شد!',
        'not_send_request_convert_video' => 'درخواست کانورت ارسال نشد!',
        'process_error' => 'خطا در پردازش رخ داده!',

        'uploadFileIsNotValid'=>'فایل ارسالی معتبر نیست!',
        'uploadFileNotFind'=>'فایل یافت نشد!',
        'uploadFileUnSuccess'=>'خطا در ذخیره سازی!',
        'uploadFileSuccess'=>'فایل با موفقیت ذخیره شد.',
        'maxFileUpload'=>'حداکثر فایل قابل ارسال :max_upload فایل می باشد.',

    ],
];