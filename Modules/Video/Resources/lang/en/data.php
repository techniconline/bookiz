<?php

return [
    'vote' => [
        'question' => 'به این ویدیو چه امتیازی می دهید؟',
        'answers' => [
            'bad' => 'بد',
            'weak' => 'ضعیف',
            'middle' => 'متوسط',
            'good' => 'خوب',
            'excellent' => 'عالی',
        ],
    ],
];