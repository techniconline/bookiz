<?php

return [

    'video' => [
        'title_required'=>'نام را مشخص کنید!',
        'instance_id_required'=>'سامانه را مشخص کنید!',
        'language_id_required'=>'زبان نامشخص است!',
        'extension_required'=>'مشخصه فیل نامشخص است!',
        'hash_key_required'=>'کد فیلم نامشخص است!',
    ],

];