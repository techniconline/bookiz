<?php

Route::group(['middleware' =>['web'] , 'prefix' => 'video', 'as' => 'video.', 'namespace' => 'Modules\Video\Http\Controllers'], function () {
    /*for access check*/
    Route::group(['prefix' => 'access' ,'as'=> 'access.', 'namespace' => '\Access'], function () {
        Route::post('/api', 'AccessController@api')->name('api');
    });


    Route::group(['prefix' => 'embed' ,'as'=> 'embed.', 'namespace' => '\Embed'], function () {
        Route::get('/{key}', 'EmbedController@embed')->name('player');
    });

    //call back after converted
    Route::post('/converted/{hash_file}', 'VideoController@converted')->name('converted');

});


Route::group(['middleware' => ['web','auth','admin'], 'prefix' => 'video', 'as' => 'video.', 'namespace' => 'Modules\Video\Http\Controllers'], function () {
    Route::get('/', 'VideoController@index')->name('index');
    Route::get('/create', 'VideoController@create')->name('create');
    Route::get('/{video_id}/edit', 'VideoController@edit')->name('edit');
    Route::get('/get/list', 'VideoController@getVideoList')->name('getVideoList');
    Route::get('/bulk/list', 'VideoController@showBulkVideosFolder')->name('showBulkVideosFolder');
    Route::get('/upload/videos', 'VideoController@createVideos')->name('createVideos');
    Route::post('/bulk/copies', 'VideoController@copyBulkVideos')->name('copyBulkVideos');
    Route::post('/bulk/copy', 'VideoController@copyBulkVideo')->name('copyBulkVideo');
    Route::delete('/bulk/directory/delete', 'VideoController@deleteBulkDirectory')->name('deleteBulkDirectory');

    Route::post('/save', 'VideoController@store')->name('save');
    Route::put('/{video_id}/update', 'VideoController@update')->name('update');
    Route::post('/{video_id}/addManualConvertVideo', 'VideoController@addManualForConvertVideo')->name('addManualConvertVideo');
    Route::delete('/{video_id}/delete', 'VideoController@destroy')->name('delete');

    Route::post('/videos/upload', 'VideoController@uploadVideos')->name('uploads');
    Route::post('/{video_id}/upload', 'VideoController@uploadVideo')->name('upload');
    Route::post('/{video_id}/poster/upload', 'VideoController@uploadPoster')->name('poster.upload');


    Route::get('/{video_id}/meta/tags/edit', 'VideoController@editMetaTags')->name('editMetaTags');
    Route::post('/{video_id}/{meta_group_id}/features/value/save', 'VideoController@saveFeaturesValue')->name('features.value.save');
    Route::post('/{video_id}/{video_meta_group_id}/meta/copy', 'VideoController@copyVideoMetaGroup')->name('meta.copy');
    Route::delete('/{video_id}/{video_meta_group_id}/meta/delete', 'VideoController@deleteVideoMetaGroup')->name('meta.delete');

    //for videoUse
    Route::group(['prefix' => 'videoUse', 'as' => 'videoUse.', 'namespace' => '\Video'], function () {

        Route::get('/', 'VideoUseController@index')->name('index');
        Route::get('/create', 'VideoUseController@create')->name('create');
        Route::get('/{video_use_id}/edit', 'VideoUseController@edit')->name('edit');

        Route::get('/{video_use_id}/getEmbedCode', 'VideoUseController@getEmbedCode')->name('getEmbedCode');

        Route::post('/save', 'VideoUseController@store')->name('save');
        Route::put('/{video_use_id}/update', 'VideoUseController@update')->name('update');
        Route::get('/{video_use_id}/show', 'VideoUseController@show')->name('show');
        Route::delete('/{video_use_id}/delete', 'VideoUseController@destroy')->name('delete');

    });

    //for Configs
    Route::group(['prefix' => 'config', 'as' => 'config.', 'namespace' => '\Config'], function () {

        Route::group(['prefix' => 'video', 'as' => 'video.'], function () {

            Route::get('/', 'VideoController@index')->name('index');
            Route::post('/save', 'VideoController@store')->name('save');

        });

    });

});