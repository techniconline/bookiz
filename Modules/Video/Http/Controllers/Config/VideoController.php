<?php

namespace Modules\Video\Http\Controllers\Config;

use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class VideoController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('config.video')->setActionMethod("getConfigForm")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('config.video')->setActionMethod("save")->response();
    }


}
