<?php
namespace Modules\Video\Http\Controllers\Embed;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class EmbedController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function embed($key,Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['key'=>$key],$request)->setViewModel('embed.player')->setActionMethod("getVideoPlayer")->response();
    }
}
