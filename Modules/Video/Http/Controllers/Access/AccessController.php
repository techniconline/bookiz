<?php

namespace Modules\Video\Http\Controllers\Access;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class AccessController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function api(Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['is_ajax'=>1],$request)->setViewModel('access.api')->setActionMethod("getVideoAccess")->response();
    }
}
