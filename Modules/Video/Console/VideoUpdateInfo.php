<?php

namespace Modules\Video\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Modules\Code\Libraries\Uploader\UploaderLibrary;
use Modules\Video\Models\Video\Video;
use Modules\Video\ViewModels\Video\VideoViewModel;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\Log;

class VideoUpdateInfo extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'video:update-info';

    protected $signature = 'video:update-info {--hash_key=} {--start_date=} {--end_date=} {--status_convert=} {--with_call_manual_convert=}';

    private $hash_key;
    private $status_convert;
    private $end_date;
    private $start_date;
    private $with_call_manual_convert;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update info Actions Video to server {--hash_key=} {--start_date=} {--end_date=} {--status_convert=}  {--with_call_manual_convert=}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(" Start Update Video Info. ");
        $this->end_date = $this->option('end_date');
        $this->start_date = $this->option('start_date');
        $this->status_convert = $this->option('status_convert');
        $this->with_call_manual_convert = $this->option('with_call_manual_convert');
        $this->hash_key = $this->option('hash_key');

        if (!$this->status_convert) {
            $this->status_convert = [Video::STATUS_WAIT, Video::STATUS_NEW, Video::STATUS_UPLOADED];
        } else {
            $this->status_convert[] = $this->status_convert;
        }

        if($this->with_call_manual_convert){
            $this->serviceManualApi();
        }else{
            $this->serviceUpdate();
        }



        return true;

    }

    private function serviceManualApi()
    {
        $videos = Video::enable()
            ->whereIn('status_convert', $this->status_convert)
            ->where(function ($q) {
//                $start_date = date("Y-m-d H:i:s", strtotime('-10 hours'));
//                $this->start_date = $this->start_date ?: $start_date;
                if ($this->start_date) {
                    $q->where('updated_at', '>=', $this->start_date);
                }

                if ($this->end_date) {
                    $q->where('updated_at', '<=', $this->end_date);
                }

                if ($this->hash_key) {
                    $q->where('hash_key', $this->hash_key);
                }
            })
            ->get();

        if (!$videos) {
            $this->error("Not Find Video Records for this group!");
            return false;
        }
        $max_count = count($videos);
        $this->output->progressStart($max_count);
        $counter = 0;
        $videoViewModel = new VideoViewModel();
        foreach ($videos as $video) {

            try {
                $result = $videoViewModel->addManualForConvertVideo($video->id,'all', 1)->response;
                $this->info($result);
            } catch (\Exception $exception) {
                report($exception);
            }

            $counter++;
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();


    }


    /**
     * @return bool
     */
    private function serviceUpdate()
    {
        $videos = Video::enable()
            ->whereIn('status_convert', $this->status_convert)
            ->where(function ($q) {
                $start_date = date("Y-m-d H:i:s", strtotime('-10 hours'));
                $this->start_date = $this->start_date ?: $start_date;
                if ($this->start_date) {
                    $q->where('updated_at', '>=', $this->start_date);
                }

                if ($this->end_date) {
                    $q->where('updated_at', '<=', $this->end_date);
                }

                if ($this->hash_key) {
                    $q->where('hash_key', $this->hash_key);
                }
            })
            ->get();

        if (!$videos) {
            $this->error("Not Find Video Records for this group!");
            return false;
        }
        $max_count = count($videos);
        $this->output->progressStart($max_count);
        $counter = 0;
        foreach ($videos as $video) {

            try {

                $res = $this->actionUpdate($video->hash_key);
                if (!$res && $this->status_fail) {
                    $video->status_convert = $this->status_fail;
                    $video->active = 2;
                    $video->save();
                }

            } catch (\Exception $exception) {
                report($exception);
            }

            $counter++;
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();


    }

    private $status_fail;

    /**
     * @param $hash_key
     * @return bool
     */
    private function actionUpdate($hash_key)
    {
        $this->status_fail = null;
        $viewModel = new VideoViewModel();
        $result = $this->callApiStatus($hash_key);
        $this->info($result);
        $result = json_decode($result, true);
        if ($result["action"]) {
            $data = $result['data'];
            if ($data["status"] == Video::STATUS_FINISHED) {
                $viewModel->converted($hash_key, $data);
                $this->info("updated, hash_key: " . $hash_key);
                return true;
            } else {

                if ((isset($data["status"]))
                    && (in_array($data["status"], [Video::STATUS_CONVERT_SYS_NOT_FIND_SOURCE_FILE, Video::STATUS_CONVERT_SYS_PRE_PROCESS_FAIL, Video::STATUS_CONVERT_SYS_PROCESS_FAIL]))) {
                    $this->status_fail = $data["status"];
                }

                $this->warn('this video:' . $hash_key . ', status is ' . $data["status"]);
                return false;
            }

        }
        $this->error("not updated, hash_key: " . $hash_key . ', result ' . json_encode($result));
        return false;
    }

    /**
     * @param $hash_file
     * @return mixed
     */
    protected function callApiStatus($hash_file)
    {
        $configVideo = config("convert.video");
        $url = isset($configVideo["url_get_video_properties"]) ? $configVideo["url_get_video_properties"] : null;
//        $this->info($url);
        $params = [
            "hash_file" => $hash_file,
            "_method" => 'GET',
        ];

        $fields_string = http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['hash_key', InputArgument::REQUIRED, 'An hash_key argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

}
