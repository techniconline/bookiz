<?php

namespace Modules\Video\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Modules\Code\Libraries\Uploader\UploaderLibrary;
use Modules\Video\Models\Video\Video;
use Modules\Video\ViewModels\Video\VideoViewModel;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\Log;

class VideoUploadBulk extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'video:upload-bulk';

    protected $signature = 'video:upload-bulk {--hash_key=} {--group=} {--quality=} {--with_pre_roll=} {--with_post_roll=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy Video by hash_key to server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $hash_key = $this->argument('hash_key');
        $hash_key = $this->option('hash_key');
        $group = $this->option('group');

        if ($group) {
            $videos = Video::enable()->where("group", $group)
                ->whereNotIn('status_convert', [Video::STATUS_UPLOADED,Video::STATUS_NEW,Video::STATUS_FINISHED,Video::STATUS_WAIT])
                ->get();

            if ($videos->isEmpty()) {
                $this->error("Not Find Video Records for this group!");
                return false;
            }
            $max_count = $videos->count();
            $this->output->progressStart($max_count);
            $counter = 0;
            foreach ($videos as $video) {
                $this->actionFile($video->hash_key);
                $counter++;
                $this->info(" hash_key: ".$video->hash_key.", video:".$video->id.", counter:".$counter." from: ".count($videos));
                $this->output->progressAdvance();
            }
            $this->output->progressFinish();
        } elseif ($hash_key) {
            //$this->actionFile($hash_key);
            $this->info(" hash_key: " . $hash_key);
        } else {
            $this->error("Miss Data!");
        }
        return true;

    }

    private function actionFile($hash_key)
    {
        $with_post_roll = $this->option('with_post_roll');
        $with_pre_roll = $this->option('with_pre_roll');
        $quality = $this->option('quality');
        $options = [
            'with_post_roll' => $with_post_roll ?: 1,
            'with_pre_roll' => $with_pre_roll ?: 1,
        ];

        $video = Video::enable()->where('hash_key', $hash_key)->first();

        if (!$video) {
            $this->error("Not Find Video Record!");
        } else {
            $file = $video->bulk_source;
            $arrFolder = explode("/", $file);
            $fileName = end($arrFolder);
            unset($arrFolder[last(array_keys($arrFolder))]);
            $arrFolder = array_values($arrFolder);
            $directory = implode("/", $arrFolder);
            $newSrc = implode("/", $arrFolder);
            $fileNameArr = explode(".", $fileName);
            $extension = last($fileNameArr);
            unset($arrFolder[0]);

            $folderUpload = substr($video->hash_key, 0, 2) . DIRECTORY_SEPARATOR;
            $folderUpload .= substr($video->hash_key, 2, 2) . DIRECTORY_SEPARATOR;
            $folderUpload .= $video->hash_key . DIRECTORY_SEPARATOR;
            $folderUpload .= "vids" . DIRECTORY_SEPARATOR . "origin" . DIRECTORY_SEPARATOR;
            $folderUpload .= $video->hash_key . '.' . $extension;

            if (file_exists(storage_path("app/public/" . $file))) {
                try {

                    //$resultUpload = Storage::disk(UploaderLibrary::STORAGE_VOD)->put($folderUpload, $content);
                    $resultUpload = Storage::disk(UploaderLibrary::STORAGE_NFS_VOD)->put($folderUpload, fopen(storage_path("app/public/" . $file), 'r+'));

                    if ($resultUpload) {

                        $video->status_convert = Video::STATUS_UPLOADED;
                        $video->active = 1;
                        $video->save();

                        $videoViewModel = new VideoViewModel();
                        $resApi = $videoViewModel->addForConvertVideo($video->hash_key, $extension, $quality ?: "all", 0, $options)->getJsonData();



                        $resultDelete = null;
                        if (isset($resApi["action"]) && $resApi["action"]) {
                            //delete source
                            Storage::disk(UploaderLibrary::STORAGE_PUBLIC)->delete($file);
                            $resultDelete = $videoViewModel->deleteDirectory($directory);
//                            $video->status_convert = Video::STATUS_NEW;
//                            $video->active = 1;
//                            $video->save();
                            //$resultDelete=true;
                        }elseif (isset($resApi["action"]) && !$resApi["action"]) {
                            $video->status_convert = Video::STATUS_ERROR_API;
                            $video->active = 2;
                            $video->save();
                            $this->info("Error:" . Video::STATUS_ERROR_API);
                            $this->saveLog(["Error, this hash:" . $hash_key, "api call:", $resApi, "error:", Video::STATUS_ERROR_API], $hash_key);
                            return false;
                        }else{
                            $video->status_convert = Video::STATUS_ERROR_UPLOAD;
                            $video->active = 2;
                            $video->save();
                        }

                        $this->info("Upload Success fully, this hash:" . $hash_key);
                        $this->saveLog(["Upload Success fully, this hash:" . $hash_key, "api call:", $resApi, "delete:", $resultDelete], $hash_key);
                        //dd("api call:", $resApi, "delete:", $resultDelete);
                    }else{
                        $video->status_convert = Video::STATUS_NOT_UPLOAD;
                        $video->active = 2;
                        $video->save();
                    }


                } catch (\Exception $exception) {
                    report($exception);
                    $video->status_convert = Video::STATUS_ERROR_UPLOAD;
                    $video->active = 2;
                    $video->save();
                    $this->info("Upload unSuccessfully, this hash:" . $hash_key . ", message: " . $exception->getMessage());
                    $this->saveLog(["Upload unSuccessfully, this hash:" . $hash_key, " message: " . $exception->getMessage()], $hash_key);
                }
            } else {
                $this->info("Not Find Source File, :" . $file);
                $this->saveLog(["Not Find Source File, :" . $file], $hash_key);

                if(Storage::disk(UploaderLibrary::STORAGE_NFS_VOD)->exists($folderUpload)){
                    $video->status_convert = Video::STATUS_UPLOADED;
                    $video->active = 1;
                }else{
                    $video->status_convert = Video::STATUS_NOT_FIND_SOURCE;
                    $video->active = 2;
                }

                $video->save();
            }
        }
    }

    /**
     * @param $logData
     * @param null $file_name
     * @param null $tag
     */
    public function saveLog($logData, $file_name = null, $tag = null)
    {
        $file_name = $file_name ?: "command";
        try {
            $file_name .= "_" . date("His");
            Log::useDailyFiles(storage_path('/logs/command/' . date("Y-m-d") . '/log_' . $file_name . '.log'));
            Log::info('START ------------------' . ($tag ?: null));
            Log::info('log', [$logData]);
            Log::info('END ------------------' . ($tag ?: null));
        } catch (\Exception $exception) {
            //
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['hash_key', InputArgument::REQUIRED, 'An hash_key argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
