<?php

namespace Modules\Video\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Modules\Code\Libraries\Uploader\UploaderLibrary;
use Modules\Video\Models\Video\Video;
use Modules\Video\ViewModels\Video\VideoViewModel;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\Log;

class VideoRefreshBulk extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'video:refresh-bulk';

    protected $signature = 'video:refresh-bulk {--hash_key=} {--group=} {--quality=} {--with_pre_roll=} {--with_post_roll=}';

    private $source_local_directory;
    private $destination_file_upload;
    private $extension;
    private $bulk_file;
    private $quality;
    private $options;
    private $hash_key;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh Upload Actions Video to server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(" Start Refresh Videos. ");
        $with_post_roll = $this->option('with_post_roll');
        $with_pre_roll = $this->option('with_pre_roll');
        $this->quality = $this->option('quality');
        $this->options = [
            'with_post_roll' => $with_post_roll ?: 1,
            'with_pre_roll' => $with_pre_roll ?: 1,
        ];

        $resApi = $this->serviceCallApi();

        $resUpload = $this->serviceCallUploadFile();

        return true;

    }

    /**
     * @return bool
     */
    public function serviceCallApi()
    {
        $videos = Video::enable()->where('status_convert', Video::STATUS_ERROR_API)->get();
        if ($videos->isEmpty()) {
            $this->info(" Not find Video by status: " . Video::STATUS_ERROR_API);
            return false;
        }
        $this->info(" Start Refresh 'Api Failed' Videos. ");
        $max_count = $videos->count();
        $this->output->progressStart($max_count);
        foreach ($videos as $video) {
            $this->actionCallApi($video);
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();
        return true;
    }

    /**
     * @param $video
     * @return bool
     */
    private function actionCallApi($video)
    {
        $this->boot($video);
        if (Storage::disk(UploaderLibrary::STORAGE_NFS_VOD)->exists($this->destination_file_upload)) {
            $videoViewModel = new VideoViewModel();
            $resApi = $videoViewModel->addForConvertVideo($video->hash_key, $this->extension, $this->quality ?: "all", 0, $this->options)->getJsonData();
            if (isset($resApi["action"]) && $resApi["action"]) {
                //delete source
                Storage::disk(UploaderLibrary::STORAGE_PUBLIC)->delete($this->bulk_file);
                $resultDelete = $videoViewModel->deleteDirectory($this->source_local_directory);
                //$resultDelete=true;
            } else {
                $video->status_convert = Video::STATUS_ERROR_API;
                $video->active = 2;
                $video->save();
                $this->info("Error:" . Video::STATUS_ERROR_API);
                $this->saveLog(["Error, this hash:" . $this->hash_key, "api call:", $resApi, "error:", Video::STATUS_ERROR_API], $this->hash_key);
                return false;
            }
        } else {
            $video->status_convert = Video::STATUS_NOT_FIND_SOURCE;
            $video->active = 2;
            $video->save();
        }
    }

    /**
     * @return bool
     */
    public function serviceCallUploadFile()
    {
        $videos = Video::enable()->whereIn('status_convert', [Video::STATUS_ERROR_UPLOAD, Video::STATUS_NOT_UPLOAD, Video::STATUS_NOT_FIND_SOURCE])->get();
        if ($videos->isEmpty()) {
            $this->info(" Not find Video by status: " . implode(",", [Video::STATUS_ERROR_UPLOAD, Video::STATUS_NOT_UPLOAD]));
            return false;
        }

        $this->info(" Start Refresh 'Not Uploaded' Videos. ");
        $max_count = $videos->count();
        $this->output->progressStart($max_count);
        foreach ($videos as $video) {
            $this->actionCallUploadFile($video);
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();
        return true;
    }

    /**
     * @param $video
     * @return bool
     */
    private function actionCallUploadFile($video)
    {
        $this->boot($video);
        if (file_exists(storage_path("app/public/" . $this->bulk_file))) {
            try {

                $resultUpload = Storage::disk(UploaderLibrary::STORAGE_NFS_VOD)->put($this->destination_file_upload, fopen(storage_path("app/public/" . $this->bulk_file), 'r+'));

                if ($resultUpload) {
                    $video->status_convert = Video::STATUS_UPLOADED;
                    $video->active = 1;
                    $video->save();
                    $this->actionCallApi($video);
                    return true;
                } else {
                    $video->status_convert = Video::STATUS_NOT_UPLOAD;
                    $video->active = 2;
                    $video->save();
                    $this->info("Upload unSuccessfully, this hash:" . $this->hash_key);
                    $this->saveLog(["Upload unSuccessfully, status: " . Video::STATUS_NOT_UPLOAD . ", this hash:" . $this->hash_key], $this->hash_key);
                    return false;
                }
            } catch (\Exception $exception) {
                report($exception);
                $video->status_convert = Video::STATUS_ERROR_UPLOAD;
                $video->active = 2;
                $video->save();
                $this->info("Upload unSuccessfully, this hash:" . $this->hash_key . ", message: " . $exception->getMessage());
                $this->saveLog(["Upload unSuccessfully, this hash:" . $this->hash_key, " message: " . $exception->getMessage()], $this->hash_key);
                return false;
            }

        } else {
            $this->info("Not Find Source File, :" . $this->bulk_file);
            $this->saveLog(["Not Find Source File, :" . $this->bulk_file], $this->hash_key);

            if (Storage::disk(UploaderLibrary::STORAGE_NFS_VOD)->exists($this->destination_file_upload)) {
                $video->status_convert = Video::STATUS_UPLOADED;
                $video->active = 1;
            } else {
                $video->status_convert = Video::STATUS_NOT_FIND_SOURCE;
                $video->active = 3;
            }

            $video->save();
            return false;
        }
    }


    /**
     * @param $video
     * @return $this
     */
    private function boot($video)
    {

        $this->bulk_file = $file = $video->bulk_source;
        $arrFolder = explode("/", $file);
        $fileName = end($arrFolder);
        unset($arrFolder[last(array_keys($arrFolder))]);
        $arrFolder = array_values($arrFolder);
        $this->source_local_directory = $directory = implode("/", $arrFolder);
        $newSrc = implode("/", $arrFolder);
        $fileNameArr = explode(".", $fileName);
        $this->extension = $extension = last($fileNameArr);
        unset($arrFolder[0]);

        $folderUpload = substr($video->hash_key, 0, 2) . DIRECTORY_SEPARATOR;
        $folderUpload .= substr($video->hash_key, 2, 2) . DIRECTORY_SEPARATOR;
        $folderUpload .= $video->hash_key . DIRECTORY_SEPARATOR;
        $folderUpload .= "vids" . DIRECTORY_SEPARATOR . "origin" . DIRECTORY_SEPARATOR;
        $folderUpload .= $video->hash_key . '.' . $extension;

        $this->hash_key = $video->hash_key;
        $this->destination_file_upload = $folderUpload;
        return $this;
    }

    /**
     * @param $logData
     * @param null $file_name
     * @param null $tag
     */
    private function saveLog($logData, $file_name = null, $tag = null)
    {
        $file_name = $file_name ?: "command";
        try {
            $file_name .= "_" . date("His");
            Log::useDailyFiles(storage_path('/logs/command/' . date("Y-m-d") . '/log_' . $file_name . '.log'));
            Log::info('START ------------------' . ($tag ?: null));
            Log::info('log', [$logData]);
            Log::info('END ------------------' . ($tag ?: null));
        } catch (\Exception $exception) {
            //
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['hash_key', InputArgument::REQUIRED, 'An hash_key argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
