<?php

namespace Modules\Video\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class VideoDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('Modules\Video\Database\Seeders\ModuleTableSeeder');
        $this->command->info('Module table seeded!');

        // $this->call("OthersTableSeeder");
    }
}
