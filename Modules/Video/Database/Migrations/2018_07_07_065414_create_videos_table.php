<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //this table can not relations with another tables -
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->unsignedInteger('instance_id')->nullable();
            $table->unsignedInteger('language_id')->nullable();
            $table->string('hash_key')->unique();
            $table->char('extension')->nullable();
            $table->text('info')->nullable();
            $table->unsignedInteger('video_time')->default(0);
            $table->unsignedInteger('user_id')->nullable();
            $table->boolean('is_public')->default(0);
            $table->boolean('downloadable')->default(0);
            $table->tinyInteger('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
