<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeVideoUseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_uses', function (Blueprint $table) {
            $table->renameColumn("module", "model");
            $table->renameColumn('module_id', "model_id");
            $table->dropColumn(['active']);
        });

        Schema::table('video_uses', function (Blueprint $table) {
            $table->unsignedBigInteger('model_id')->nullable()->change();
            $table->tinyInteger('active')->default(1)->after("model_id");
            $table->boolean('is_public')->nullable()->after('model_id');
            $table->boolean('downloadable')->nullable()->after('model_id');
            $table->string('poster')->nullable()->after('model_id');
        });

        Schema::table('videos', function (Blueprint $table) {
            $table->dropColumn(['is_public', 'downloadable']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('videos', function (Blueprint $table) {
            $table->boolean('is_public')->nullable()->after('video_time');
            $table->boolean('downloadable')->nullable()->after('video_time');
        });

        Schema::table('video_uses', function (Blueprint $table) {
            $table->renameColumn("model", "module");
            $table->renameColumn('model_id', "module_id");
        });

        Schema::table('video_uses', function (Blueprint $table) {
            $table->dropColumn(["poster","downloadable","is_public"]);
        });
    }
}
