<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoMetaGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_meta_groups', function (Blueprint $table) {

            $table->unsignedInteger('video_id');
            $table->unsignedInteger('meta_group_id');

        });

        Schema::table('video_meta_groups', function (Blueprint $table) {
            $table->foreign('meta_group_id')->references('id')->on('meta_groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('video_id')->references('id')->on('videos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_meta_groups', function (Blueprint $table) {
            $table->dropForeign('video_meta_groups_meta_group_id_foreign');
            $table->dropForeign('video_meta_groups_video_id_foreign');
        });

        Schema::dropIfExists('video_meta_groups');
    }
}
