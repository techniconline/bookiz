<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoUsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_uses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('video_id');
            $table->string('hash_key')->unique();
            $table->unsignedInteger('instance_id');
            $table->string('module')->nullable();
            $table->unsignedBigInteger('module_id');
            $table->tinyInteger('active');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('video_uses', function (Blueprint $table) {
            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('video_id')->references('id')->on('videos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_uses', function (Blueprint $table) {
            $table->dropForeign('video_uses_instance_id_foreign');
            $table->dropForeign('video_uses_video_id_foreign');
        });

        Schema::dropIfExists('video_uses');
    }
}
