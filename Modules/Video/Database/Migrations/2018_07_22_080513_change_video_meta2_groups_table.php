<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeVideoMeta2GroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_meta_groups', function (Blueprint $table) {
            $table->unsignedInteger('start_time')->after('meta_group_id')->comment('minutes');
            $table->unsignedInteger('end_time')->after('start_time')->comment('minutes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_meta_groups', function (Blueprint $table) {
            $table->dropColumn(['start_time','end_time']);
        });
    }
}
