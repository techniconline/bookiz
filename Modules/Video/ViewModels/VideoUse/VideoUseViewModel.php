<?php

namespace Modules\Video\ViewModels\VideoUse;

use Modules\Core\Models\Instance\Instance;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Video\Models\Video\VideoUse;


class VideoUseViewModel extends BaseViewModel
{


    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createVideoUse()
    {
        $instances = Instance::active()->get();
        $instances = $instances ? $instances->pluck('name', 'id') : [];

        $videoUse = new VideoUse();
        $videoUse = $videoUse->active()->with(['instance', 'video'])->find($this->request->get('video_use_id'));
        

        $this->setTitlePage(trans('video::video.video_use.' . ($videoUse ? 'edit' : 'add')));
        return $this->renderedView("video::video_use.form_video_use", ['videoUse' => $videoUse, 'instances' => $instances], "form");
    }

    /**
     * @return VideoUseViewModel
     * @throws \Throwable
     */
    protected function editVideoUse()
    {
        return $this->createVideoUse();
    }

    /**
     * @return $this
     */
    protected function destroyVideoUse()
    {
        $video_use_id = $this->request->get('video_use_id');
        $videoUse = VideoUse::active()->find($video_use_id);

        if ($videoUse->delete()) {
            return $this->setResponse(true, trans("video::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("video::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    public function saveVideoUse()
    {
        $videoUse = new VideoUse();
        if ($video_use_id = $this->request->get("video_use_id")) {
            $videoUse = $videoUse->enable()->find($video_use_id);
            if (!$videoUse) {
                return $this->setResponse(false, trans("video::messages.alert.not_find_data"));
            }

        }

        if(!$video_use_id){
            $this->request->offsetSet('hash_key', $videoUse->getHashKey());
        }
        if (($videoUse->fill($this->request->all())->isValid()) || ($video_use_id && $videoUse)) {
            $videoUse->save();
            $videoUse->updated = $video_use_id ? true : false;
            return $this->redirect(route("video.videoUse.edit",["video_use_id"=>$videoUse->id]))->setDataResponse($videoUse)->setResponse(true, trans("video::messages.alert.save_success"));
        }

        return $this->redirectBack()->setResponse(false, trans("video::messages.alert.mis_data"), $videoUse->errors);
    }

    /**
     * @return $this
     */
    protected function getVideoUse()
    {
        $videoUse = VideoUse::active()
            ->with(['video','instance'])
            ->filterInstance()
            ->filter()->first();
        if($videoUse){
            return $this->setDataResponse($videoUse)->setResponse(true);
        }
        return $this->setResponse(false, trans("video::messages.alert.not_find_data"));

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getEmbedCode()
    {
        $video_use_id = $this->request->get('video_use_id');
        $videoUse = VideoUse::active()->find($video_use_id);
        if($videoUse){
            $key=$videoUse->hash_key;
            $random_id=str_random(10);
            $instanceBase=app('getInstanceObject')->getInstance();
            $instance=config('app.instance');
            $host=url('/');
            $host=str_replace($instanceBase->name.'.',$instance.'.',$host);
            $embedCode = view("video::video_use.embed_code_video_use",['key'=>$key,'host'=>$host,'random_id'=>$random_id])->render();
            if($embedCode){
                return $this->setDataResponse($embedCode, "html")->setResponse(true);
            }
        }

        return $this->setResponse(false, trans("video::messages.alert.not_find_data"));

    }


}
