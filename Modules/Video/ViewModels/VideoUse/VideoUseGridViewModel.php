<?php

namespace Modules\Video\ViewModels\VideoUse;


use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Video\Models\Video\VideoUse;


class VideoUseGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $video_assets;
    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = VideoUse::enable()->filterCurrentInstance()->with("video");
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $video_assets = [];

        //use bested assets
        if ($this->video_assets) {

        }

        return $video_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('video::video.video_use.title'), true)
            ->addColumn('video_id', trans('video::video.video_use.video_id'), false)
            ->addColumn('is_public', trans('video::video.video_use.is_public'), false)
            ->addColumn('poster', trans('video::video.video_use.has_poster'), false)
            ->addColumn('hash_key', trans('video::video.video_use.hash_key'), false)
            ->addColumn('active', trans('video::video.video_use.active'), false)
            ->addColumn('created_at', trans('video::video.created_at'), false);

        /*add action*/
        $add = array(
            'name' => 'video.videoUse.create',
            'parameter' => null
        );
        $this->addButton('create_video_use', $add, trans('video::video.video_use.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        $show = array(
            'name' => 'video.videoUse.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('video::video.video_use.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'video.videoUse.getEmbedCode',
            'parameter' => ['id']
        );
        $this->addAction('embed_code', $show, trans('video::video.video_use.getEmbedCode'), 'fa fa-code', false, ['target' => '', 'class' => 'btn btn-sm btn-primary ajax']);


        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('video::video.video_use.confirm_delete')];
        $delete = array(
            'name' => 'video.videoUse.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('video::video.video_use.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $configHash = ['title' => trans('video::video.video_use.hash_key')];
        $this->addFilter('hash_key', 'text', $configHash);
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->title = $row->video ? $row->video->title : "-";
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->has_poster = $row->poster ? trans("video::video.video_use.has_poster_text.1") : trans("video::video.video_use.has_poster_text.0");
        $row->is_public = $row->is_public ? trans("video::video.video_use.status_public.1") : trans("video::video.video_use.status_public.0");
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
        if ($row->model && !$row->is_public) {
            unset($action['embed_code'],$action['delete']);
        }
        return $action;

    }

    /**
     * @return $this
     */
    protected function setAssetsEditVideo()
    {
        $this->video_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('video::video.video_use.list'));
        $this->generateGridList()->renderedView("video::video_use.index", ['view_model' => $this], "video_list");
        return $this;
    }


}
