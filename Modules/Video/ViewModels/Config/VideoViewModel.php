<?php

namespace Modules\Video\ViewModels\Config;

use Modules\Core\ViewModels\Config\SettingViewModel;
use Modules\User\Models\Connector;

class VideoViewModel extends SettingViewModel
{

    public $setting_name = 'video_configs';
    public $module_name = 'video';

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getConfigForm()
    {
        $BlockViewModel =& $this;
        $this->setTitlePage(trans('video::video.config.video_config'));
        return $this->renderedView('video::config.video', compact('BlockViewModel'));
    }


    /**
     * @return $this
     */
    public function save()
    {
       /* $this->request->validate([
            'url_server' => 'required|string',
            'path_source_video_convert' => 'required|string',
        ]);*/
        $result = $this->saveConfig($this->request->all());
        if ($result) {
            return $this->redirectBack()->setResponse($result, trans('video::messages.alert.save_success'));
        }
        return $this->redirectBack()->setResponse($result, trans('video::messages.alert.save_un_success'));

    }
}
