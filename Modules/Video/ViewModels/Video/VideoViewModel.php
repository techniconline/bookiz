<?php

namespace Modules\Video\ViewModels\Video;


use Illuminate\Support\Facades\Storage;
use Modules\Code\Libraries\Uploader\UploaderLibrary;
use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureValueRelation;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\Traits\Feature\FormFeatureBuilder;
use Modules\Core\Models\Meta\MetaGroup;
use Modules\Video\Models\Video\Video;
use Modules\Core\Models\Language\Language;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Video\Models\Video\VideoMetaGroup;
use Modules\Video\ViewModels\VideoUse\VideoUseViewModel;
use function RFM\mime_type_by_extension;


class VideoViewModel extends BaseViewModel
{

    use GridViewModel;
    use FormFeatureBuilder;

    private $video_assets;
    private $bulk_video_assets;
    private $video_meta_assets;
    private $nestable_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = Video::enable()->filterCurrentInstance();
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $video_assets = [];

        //use bested assets
        if ($this->video_assets) {
            $video_assets [] = ['container' => 'up_theme_js_2', 'src' => ("assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"), 'name' => 'ui.widget'];

            $video_assets [] = ['container' => 'plugin_general_2', 'src' => ("jquery-ui/jquery-ui.min.js"), 'name' => 'jquery-ui'];
            $video_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/plupload.full.min.js"), 'name' => 'plupload.full'];
            $video_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/jquery.ui.plupload.js"), 'name' => 'jquery.ui.plupload'];
            $video_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/i18n/fa.js"), 'name' => 'fa.js'];

            $video_assets [] = ['container' => 'general_style', 'src' => ("jquery-ui/jquery-ui.min.css"), 'name' => 'jquery-ui'];
            $video_assets [] = ['container' => 'general_style', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css"), 'name' => 'jquery.ui.plupload.css'];

        }

        if ($this->video_assets || $this->video_meta_assets) {
            $video_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/video.js"), 'name' => 'video-backend'];
        }

        if ($this->bulk_video_assets) {
            $video_assets [] = ['container' => 'theme_style', 'src' => ("assets/global/plugins/jquery-nestable/jquery.nestable.css"), 'name' => 'jquery.nestable'];
            $video_assets [] = ['container' => 'theme_js', 'src' => ("assets/global/plugins/jquery-nestable/jquery.nestable-rtl.js"), 'name' => 'jquery.nestable'];
            $video_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/video/bulk_video.js"), 'name' => 'category-backend'];
        }


        return $video_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('video::video.title'), true)
            ->addColumn('extension', trans('video::video.extension'), false)
            ->addColumn('show_poster', trans('video::video.show_poster'), false)
            ->addColumn('has_poster', trans('video::video.has_poster'), false)
            ->addColumn('status_convert', trans('video::video.status_convert'), false)
            ->addColumn('video_time', trans('video::video.video_time'), false)
            ->addColumn('hash_key', trans('video::video.hash_key'), false)
            ->addColumn('group', trans('video::video.group'), false)
            ->addColumn('active', trans('video::video.active'), false)
            ->addColumn('created_at', trans('video::video.created_at'), false)
            ->addColumn('updated_at', trans('video::video.updated_at'), false);

        /*add action*/
        $add = array(
            'name' => 'video.create',
            'parameter' => null
        );
        $this->addButton('create_video', $add, trans('video::video.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        $add = array(
            'name' => 'video.showBulkVideosFolder',
            'parameter' => null
        );
        $this->addButton('create_video_bulk', $add, trans('video::video.add_bulk'), 'fa fa-cloud-upload', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);

        $add = array(
            'name' => 'video.createVideos',
            'parameter' => null
        );
        $this->addButton('create_videos', $add, trans('video::video.add_videos'), 'fa fa-cloud-upload', false, 'before', ['target' => '', 'class' => 'btn btn-success']);

        $show = array(
            'name' => 'video.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('video::video.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'video.editMetaTags',
            'parameter' => ['id']
        );
        $this->addAction('edit_meta_tags', $show, trans('video::video.edit_meta_tags'), 'fa fa-tags', false, ['target' => '', 'class' => 'btn btn-sm btn-warning']);

        $show = array(
            'name' => 'video.addManualConvertVideo',
            'parameter' => ['id']
        );
        $this->addAction('addManualConvertVideo', $show, trans('video::video.add_manual_convert_video'), 'fa fa-cloud-upload'
            , false, ['target' => '', 'class' => 'btn btn-sm btn-default _ajax_confirm', 'data-method' => "POST", 'data-message-confirm' => trans('video::video.confirm')]);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('video::video.confirm_delete')];
        $delete = array(
            'name' => 'video.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('video::video.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
//        $this->addFilter('id', 'text', ['op' => '=']);
        $this->addFilter('title', 'text');
        $this->addFilter('group', 'text');
        $this->addFilter('status_convert', 'select', ['options' => trans("video::video.video_statuses")]);
        $configHash = ['title' => trans('video::video.hash_key')];
        $this->addFilter('hash_key', 'text', $configHash);
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->has_poster = $row->has_poster_text;
        $row->show_poster = $row->show_poster_text;
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
        if ($row->custom) {
            unset($action['add_options']);
        }
        return $action;

    }

    /**
     * @return $this
     */
    protected function setAssetsEditVideo()
    {
        $this->video_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('video::video.list'));
        $this->generateGridList()->renderedView("video::video.index", ['view_model' => $this], "video_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createVideo()
    {
        $instances = Instance::active()->get();
        $instances = $instances ? $instances->pluck('name', 'id') : [];

        $video = new Video();
        $video = $video->with(['language'
            , 'instance'
            , 'metaGroups.featureGroups.featureGroupRelations.feature.featureValues'
        ])->find($this->request->get('video_id'));

        $metaGroups = null;
        $metaGroupsActive = [];
        if ($video) {
            $metaGroups = new MetaGroup();
            $metaGroups = $metaGroups->active()->filterLanguage()->filterSystem(Video::SYSTEM_NAME)->get();
            $metaGroups = $metaGroups ? $metaGroups->pluck('title', 'id') : [];

            $videoMetaGroups = $video->videoMetaGroups()->get();

            if ($videoMetaGroups) {
                $metaGroupsActive = $videoMetaGroups->pluck('meta_group_id')->toArray();

            }


        }

        $this->setTitlePage(trans('video::video.' . ($video ? 'edit' : 'add')));
        return $this->renderedView("video::video.form_video", ['video' => $video, 'instances' => $instances
            , 'metaGroups' => $metaGroups, 'metaGroupsActive' => $metaGroupsActive], "form");
    }

    /**
     * @return VideoViewModel
     * @throws \Throwable
     */
    protected function editVideo()
    {
        return $this->createVideo();
    }

    /**
     * @return $this
     */
    protected function setAssetsEditMetaTags()
    {
        $this->video_meta_assets = true;
        return $this;
    }

    /**
     *
     */
    protected function editMetaTags()
    {
        $video = new Video();
        $video = $video->active()->with(['language'
            , 'instance'
            , 'metaGroups.featureGroups.featureGroupRelations.feature.featureValues'
        ])->find($this->request->get('video_id'));

        if (!$video) {
            return $this->redirect(route('video.editMetaTags', ['video_id' => $this->request->get('video_id')]))->setResponse(false, trans("video::messages.alert.not_find_data"));
        }

        $videoMetaGroups = $metaGroups = null;
        $metaFeatureDataList = $metaGroupsActive = [];
        $metaGroups = MetaGroup::active()->filterLanguage()->filterSystem(Video::SYSTEM_NAME)->get();

        $videoMetaGroups = $video->videoMetaGroups()->get();

        if ($videoMetaGroups) {
            $metaGroupsActive = $videoMetaGroups->pluck('meta_group_id')->toArray();

            $listMetaGroup = $metaGroups ? $metaGroups->pluck('title', 'id') : null;

            $videoMetaGroups = ($videoMetaGroups)->map(function ($item) use ($listMetaGroup) {
                $item['group_name'] = isset($listMetaGroup[$item->meta_group_id]) ? $listMetaGroup[$item->meta_group_id] : null;
                return $item;
            });

            $videoMetaGroupList = $videoMetaGroups->pluck('meta_group_id', 'id');
            $metaFeatureData = FeatureValueRelation::active()->where('system', 'video_meta_groups')->whereIn('item_id', ($videoMetaGroupList->keys()))->get();

            foreach ($metaFeatureData as $item) {
                $metaFeatureDataList[$item->item_id][$item->feature_id][] = $item->feature_value_id ? $item->feature_value_id : $item->feature_value;
            }

        }

        $this->setTitlePage(trans('video::video.edit_meta_tags') . ':' . $video->title);

        return $this->renderedView("video::video.form_video_meta_tags", ['video' => $video, 'metaGroups' => $metaGroups
            , 'metaGroupsActive' => $metaGroupsActive, 'videoMetaGroups' => $videoMetaGroups
            , 'metaFeatureDataList' => $metaFeatureDataList, 'view_model' => $this], "form");


    }

    /**
     * @return $this
     */
    protected function destroyVideo()
    {
        $video_id = $this->request->get('video_id');
        $video = Video::active()->find($video_id);

        if ($video->delete()) {
            return $this->setResponse(true, trans("video::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("video::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveVideo()
    {
        $response = $this->saveVideoService();
        $video = $response['video'];
        if ($response['action']) {
            return $this->redirect(route('video.edit', ['video_id' => $video->id]))->setDataResponse($video)->setResponse(true, trans("video::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, trans("video::messages.alert.mis_data"), $video->errors);
    }

    /**
     * @return $this|array
     */
    private function saveVideoService()
    {
        $video = new Video();
        if ($video_id = $this->request->get("video_id")) {
            $video = $video->active()->find($video_id);
            if (!$video) {
                return ['action' => false, "video" => null, "video_id" => $this->request->get('video_id')];
            }

            $this->saveMetaGroups($video_id, $this->request->get('meta_group_ids'));
        }

        if (!$video_id) {
            $this->request->request->set('hash_key', $video->getHashKey());
            $this->request->request->set('status_convert', '-');
            $this->request->request->set('extension', $this->request->get('extension', '-'));
        }

        $this->request->request->set('show_poster', $this->request->get('show_poster', 0));

        if (($video->fill($this->request->all())->isValid()) || ($video_id && $video)) {
            $video->save();
            $video->updated = $video_id ? true : false;
            if (!$video_id) {
                $resVideoUse = $this->saveVideoUse($video->id);
            }
            $response = ['action' => true, 'video' => $video];
        } else {
            $response = ['action' => false, 'video' => $video];
        }
        return $response;
    }

    /**
     * @param $video_id
     * @return $this
     */
    private function saveVideoUse($video_id)
    {
        $videoUserViewModel = new VideoUseViewModel();
        $this->request->offsetSet("video_id", $video_id);
        $this->request->offsetSet("is_public", 1);
        $this->request->offsetSet("active", 1);
        $res = $videoUserViewModel->setRequest($this->request)->saveVideoUse();
        return $res;
    }

    /**
     * @param $video_id
     * @param $meta_group_ids
     * @return bool
     */
    protected function saveMetaGroups($video_id, $meta_group_ids)
    {

        $vmgModel = new VideoMetaGroup();
        $system = $vmgModel->getTable();
        $vmgIds = $vmgModel->where('video_id', $video_id)->pluck('id');
        if ($vmgIds) {
            FeatureValueRelation::where('system', $system)->whereIn('item_id', $vmgIds)->delete();
            VideoMetaGroup::whereIn('id', $vmgIds)->delete();
        }

        if (empty($meta_group_ids) || !$video_id) {
            return false;
        }

        $insertItems = [];
        foreach ($meta_group_ids as $meta_group_id) {
            $insertItems[] = ['meta_group_id' => $meta_group_id, 'video_id' => $video_id, 'start_time' => 0, 'end_time' => 0];
        }

        $result = VideoMetaGroup::insert($insertItems);
        return $result;
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createVideos()
    {
        $this->video_assets = true;
        $this->setTitlePage(trans('video::video.add_videos'));
        return $this->renderedView("video::video.form_videos_upload", [], "form");
    }

    protected function uploadVideos()
    {
        if ($name = $this->request->get("name")) {
            $libUploader = new UploaderLibrary();
            $folderUpload = isset($this->config['config']['manual_videos']) ? $this->config['config']['manual_videos'] : "videos_manual_uploaded";
            $folderUpload .= DIRECTORY_SEPARATOR . (date("Y-m-d"));
            $result = $libUploader->setStorage($libUploader::STORAGE_PUBLIC)
                ->setFolderUploadName($folderUpload)
                ->setTypeFile('video')
                ->setUrlUpload(null, false)->upload();

            $exist = false;
            if (isset($result['action']) && $result['action']) {

                $src = $result['items']['src'] ? $result['items']['src'] : null;
                //$srcDestination = $result['items']['destination'] ? $result['items']['destination'] : null;
                $exist = Storage::disk($libUploader::STORAGE_PUBLIC)->exists($src);
            }
            return $this->setDataResponse($result)->setResponse(boolval($exist), $exist ? trans("video::messages.alert.uploadFileSuccess") : trans("video::messages.alert.not_find_data"));
        }
        return $this->redirectBack()->setResponse(false, trans("video::messages.alert.mis_data"));
    }


    /**
     * @return $this
     * @throws \getid3_exception
     */
    protected function uploadVideo()
    {
        $video = Video::find($this->request->get("video_id"));
        if (!$video) {
            return $this->setResponse(false, trans("video::messages.alert.not_find_data"));
        }

        $result = $this->uploadVideoService($video);

        return $this->setResponse(isset($result['action']) ? $result['action'] : null, isset($result['message']) ? $result['message'] : null
            , isset($result['errors']) ? $result['errors'] : null, isset($result['items']) ? $result['items'] : null);

    }


    /**
     * @param Video $video
     * @return array
     * @throws \getid3_exception
     */
    private function uploadVideoService(Video $video = null)
    {
        $libUploader = new UploaderLibrary();
        $folderUpload = substr($video->hash_key, 0, 2) . DIRECTORY_SEPARATOR;
        $folderUpload .= substr($video->hash_key, 2, 2) . DIRECTORY_SEPARATOR;
        $folderUpload .= $video->hash_key . DIRECTORY_SEPARATOR;
        $folderUpload .= "vids" . DIRECTORY_SEPARATOR . "origin";
        $result = $libUploader->setStorage($libUploader::STORAGE_NFS_VOD)->setFolderUploadName($folderUpload)
            ->setFileName($video->hash_key)
            ->setTypeFile('video')
            ->setUrlUpload(null, false)->upload();

        if (isset($result['action']) && $result['action']) {
            if (isset($result['items']['extension']) && $result['items']['extension']) {
                $video->extension = $result['items']['extension'];
            }

            $src = $result['items']['src'] ? $result['items']['src'] : null;
            //$srcDestination = $result['items']['destination'] ? $result['items']['destination'] : null;
            $exist = Storage::disk($libUploader::STORAGE_NFS_VOD)->exists($src);

            if ($exist) {
                $src = config('convert.video.source_video_files_local') . $src;
                $duration = $this->getDuration($src);
                $video->video_time = $duration ? $duration : 0;
            }

            $video->status_convert = Video::STATUS_UPLOADED;
            $video->save();

            if ($exist) {
                $responseConvert = $this->addForConvertVideo($video->hash_key, $video->extension, "all", 0);
            }

        }

        return $result;
    }

    /**
     * @param $full_video_path
     * @return false|string
     * @throws \getid3_exception
     */
    public function getDuration($full_video_path)
    {
        $playtime_seconds = 0;
        try {
            $getID3 = new \getID3();
            $file = $getID3->analyze($full_video_path);
            $playtime_seconds = $file['playtime_seconds'];
        } catch (\Exception $exception) {
            report($exception);
        }
        //$duration = date('H:i:s.v', $playtime_seconds);
        return $playtime_seconds;
    }

    /**
     * @return $this
     */
    protected function uploadPoster()
    {
        $video = Video::active()->find($this->request->get("video_id"));
        if (!$video) {
            return $this->setResponse(false, trans("video::messages.alert.not_find_data"));
        }
        $libUploader = new UploaderLibrary();
        $folderUpload = 'vod' . DIRECTORY_SEPARATOR;
        $folderUpload .= substr($video->hash_key, 0, 2) . DIRECTORY_SEPARATOR;
        $folderUpload .= substr($video->hash_key, 2, 2) . DIRECTORY_SEPARATOR;
        $folderUpload .= $video->hash_key . DIRECTORY_SEPARATOR;
        $folderUpload .= "vids" . DIRECTORY_SEPARATOR . "origin";

        $result = $libUploader->setStorage($libUploader::STORAGE_NFS_PUBLIC)->setFolderUploadName($folderUpload)
            ->setFileName($video->hash_key)
            ->setFileInputName('file_image')
            ->setTypeFile('image')
            ->setUrlUpload(null, false)
            ->setMaxImageSize(800, 800)
            ->setConvertFormat('jpg', 75)
            //->createThumbnail()
            ->resizeUpload();

        if (isset($result['action']) && $result['action']) {
            $video->has_poster = 1;
            $video->save();
        }

        return $this->setResponse(isset($result['action']) ? $result['action'] : null, isset($result['message']) ? $result['message'] : null
            , isset($result['errors']) ? $result['errors'] : null, isset($result['items']) ? $result['items'] : null);
    }


    /**
     * @return $this
     */
    protected function setAssetsShowBulkVideosFolder()
    {
        $this->bulk_video_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function showBulkVideosFolder()
    {
        $srcFolders = isset($this->config['config']['bulk_src_videos']) ? $this->config['config']['bulk_src_videos'] : "bulk_videos";
        $listFolders = $this->getDirectoryData($srcFolders);

        $srcFolders = isset($this->config['config']['manual_videos']) ? $this->config['config']['manual_videos'] : "videos_manual_uploaded";
        $listManualVideos = $this->getDirectoryData($srcFolders);
        $listFolders = $listFolders->merge($listManualVideos);

        $this->setTitlePage(trans('video::video.bulk_folder_list'));
        return $this->renderedView("video::video.bulk_video_folders", ['listFolders' => $listFolders], "bulk_video_folders");

    }

    /**
     * @param $directory
     * @return static
     */
    public function getDirectoryData($directory)
    {
        $valid_extensions = ['mp4', 'avi', 'mkv', 'mpeg', 'mpg', 'vob', 'ts'];
        $first_char = substr($directory, 0, 1);
        if ($first_char == "$" && $first_char == ".") {
            return null;
        }
        $listFolders = collect(Storage::disk('public')->directories($directory))->map(function ($directory) use ($valid_extensions) {
            $files = $childes = collect();
            $dirArr = explode(DIRECTORY_SEPARATOR, $directory);
            $first_char = substr(last($dirArr), 0, 1);
            try {
                if ($first_char != "$" && $first_char != ".") {
                    $files = Storage::disk('public')->allFiles($directory);
                    $files = collect($files)->filter(function ($file) use ($valid_extensions) {
                        try {
                            if (in_array(strtolower(pathinfo($file, PATHINFO_EXTENSION)), $valid_extensions)) {
                                return $file;
                            }
                        } catch (\Exception $e) {
                            report($e);
                        }
                    });

                    $subDirectories = Storage::disk('public')->directories($directory);
                    $childes = null;
                    if (count($subDirectories)) {
                        $childes = $this->getDirectoryData($directory);
                    }
                }
            } catch (\Exception $e) {
                report($e);
            }
            return ["src" => $directory, 'count_files' => $files->count(), 'files' => $files, "childes" => $childes];
        })->filter(function ($row) {
            if ($row["count_files"]) {
                return $row;
            }
        });

        return $listFolders;
    }

    /**
     * @return $this
     */
    protected function copyBulkVideos()
    {

        $listFiles = [];
        if ($folder = $this->request->get("folder")) {
            $listFiles = collect(Storage::disk('public')->allFiles($folder))->map(function ($file) {
                return ['file' => $file, "result" => $this->copyBulkVideoService($file)];
            });
        }

        return $this->redirect(route('video.showBulkVideosFolder'))->setDataResponse($listFiles)->setResponse(true, trans("video::messages.alert.uploadFileSuccess"));
    }

    /**
     * @return $this
     */
    protected function copyBulkVideo()
    {
        $file = $this->request->get('file');
        $group = $this->request->get('group');
        $last_file = $this->request->get('last_file');
        $check_has_file = (int)$this->request->get('check_has_file', 1);

        $responseMove = $this->copyBulkVideoService($file, $group, $last_file, $check_has_file);

        return $this->setDataResponse($responseMove)->setResponse($responseMove['action'] ? true : false, trans("video::messages.alert." . ($responseMove['action'] ? 'uploadFileSuccess' : 'uploadFileUnSuccess')));

    }

    /**
     * @param $file
     * @param null $group
     * @param int $last_file
     * @param bool $check_has_file
     * @return null
     */
    private function copyBulkVideoService($file, $group = null, $last_file = 0, $check_has_file = true)
    {
        $arrFolder = explode("/", $file);
        $fileName = end($arrFolder);
        unset($arrFolder[last(array_keys($arrFolder))]);
        $arrFolder = array_values($arrFolder);
        $directory = implode("/", $arrFolder);
        $newSrc = implode("/", $arrFolder);
        $fileNameArr = explode(".", $fileName);
        $extension = last($fileNameArr);
        unset($arrFolder[0]);
        $requestObject = \request();
        $items = [
            'title' => $fileName,
            'instance_id' => app('getInstanceObject')->getCurrentInstanceId(),
            'language_id' => app('getInstanceObject')->getLanguageId(),
            'bulk_source' => $file,
            'extension' => $extension,
            'group' => $group,
            'path_hash_key' => md5($file),
            'active' => 1,
        ];


        $resultUpload = null;
        $checkVideo = Video::where("path_hash_key", md5($file))->first();
        if ($checkVideo && $check_has_file) {
            return ['action' => false, 'message' => "this file exist!"];
        }

        $requestObject->request->add($items);
        $responseVideo = $this->setRequest($requestObject)->saveVideoService();
        $video = $responseVideo['video'];
        if ($last_file) {
            try {

                $artisan = rtrim(base_path("/"), "/") . "/artisan";
                $shell_string = "php " . $artisan . " video:upload-bulk";
                if ($group) {
                    $shell_string .= " --group=" . $group;
                } elseif ($video->hash_key) {
                    $shell_string .= " --hash_key=" . $group;
                } else {
                    return ['action' => false, 'video' => $video, 'directory' => $directory, 'message' => "not run command!"];
                }
                $shell_string .= " > /dev/null 2>&1 &";
                $res = exec($shell_string);

                //            $content = Storage::disk(UploaderLibrary::STORAGE_PUBLIC)->get($file);
                /*
                                $folderUpload = substr($video->hash_key, 0, 2) . DIRECTORY_SEPARATOR;
                                $folderUpload .= substr($video->hash_key, 2, 2) . DIRECTORY_SEPARATOR;
                                $folderUpload .= $video->hash_key . DIRECTORY_SEPARATOR;
                                $folderUpload .= "vids" . DIRECTORY_SEPARATOR . "origin" . DIRECTORY_SEPARATOR;
                                $folderUpload .= $video->hash_key . '.' . $extension;
                                //$resultUpload = Storage::disk(UploaderLibrary::STORAGE_VOD)->put($folderUpload, $content);
                                $resultUpload = Storage::disk(UploaderLibrary::STORAGE_VOD)->put($folderUpload, fopen(storage_path("app/public/" . $file), 'r+'));

                                //add for convert
                                if ($resultUpload) {
                                    $responseConvert = $this->addForConvertVideo($video->hash_key, $extension, "all", 0);
                                }
                */


            } catch (\Exception $exception) {
                report($exception);
            }
        }

        /*        $resultDelete = false;
                if ($resultUpload) {
                    Storage::disk(UploaderLibrary::STORAGE_PUBLIC)->delete($file);
                    $resultDelete = $this->deleteDirectory($directory);
                }*/

        return ['action' => $responseVideo['action'] ?: ($last_file ? true : false), 'video' => $video, 'directory' => $directory];
    }

    /**
     * @param $hash_file
     * @param string $extension
     * @param string $qualities
     * @param int $rewrite
     * @param array $options
     * @return $this
     */
    public function addForConvertVideo($hash_file, $extension = "mp4", $qualities = "all", $rewrite = 0, $options = [/*"with_pre_roll"=>1, "with_post_roll"=>1*/])
    {
        $file_name = $hash_file . "." . $extension;
        if ($file_name) {
            $resultCurl = $this->callApiConvert($file_name, $hash_file, $extension, $qualities, $rewrite, $options);
            return $this->setDataResponse($resultCurl)
                ->setResponse(isset($resultCurl["action"]) ? $resultCurl["action"] : false, trans("video::messages.alert." . (isset($resultCurl["action"]) && $resultCurl["action"] ? "send_request_convert_video" : "not_send_request_convert_video")));
        }
        return $this->setResponse(false);
    }

    /**
     * @return mixed
     */
    public function getJsonData()
    {
        return $this->response;
    }

    /**
     * @param null $video_id
     * @param null $qualities
     * @return $this
     */
    public function addManualForConvertVideo($video_id = null, $qualities = null, $rewrite = null)
    {
        $video_id = $video_id ?: $this->request->get("video_id");
        $qualities = $qualities ?: $this->request->get("qualities", "all");
        $rewrite = $rewrite ?: $this->request->get("rewrite", 1);
        $video = Video::enable()->find($video_id);
        if (!$video) {
            return $this->setResponse(false, trans("video::messages.alert.not_find_data"));
        }
        return $this->addForConvertVideo($video->hash_key, $video->extension, $qualities, $rewrite);
    }


    /**
     * @param $file
     * @param $hash_file
     * @param $extension
     * @return bool
     */
    private function moveVideoFile($file, $hash_file, $extension)
    {
        return true;

        $configVideoConvert = config("convert.video");
        $source_video_files = isset($configVideoConvert["source_video_files"]) ? $configVideoConvert["source_video_files"] : null;
        $source_video_files .= DIRECTORY_SEPARATOR . $hash_file . "." . $extension;
        try {
            if (file_exists($file)) {
                //chmod($file, 0777);
            }

            set_time_limit(0); //Unlimited max execution time
            $file = fopen($file, "rb");
            $create = null;
            if ($file) {
                $create = fopen($source_video_files, "wb");
                if ($create)
                    while (!feof($file)) {
                        fwrite($create, fread($file, 1024 * 8), 1024 * 8);
                    }
            }

            if ($file) {
                fclose($file);
            }

            $resultUpload = null;
            if ($create) {
                $resultUpload = fclose($create);
            }

        } catch (\Exception $exception) {
            $resultUpload = null;
        }

        return $resultUpload;
    }


    /**
     * @param $file_name
     * @param $hash_file
     * @param string $extension
     * @param string $qualities => [270,360,480,...]
     * @param int $rewrite
     * @param array $options
     * @return mixed
     */
    protected function callApiConvert($file_name, $hash_file, $extension = "mp4", $qualities = "all", $rewrite = 0, $options = [])
    {
        $configVideo = config("convert.video");
        $url = isset($configVideo["url_server_convert"]) ? $configVideo["url_server_convert"] : null;
        $url_call_back = isset($configVideo["url_call_back_convert"]) ? $configVideo["url_call_back_convert"] : null;
        $params = [
            "file_name" => $file_name,
            "hash_file" => $hash_file,
            "rewrite" => $rewrite,
            "call_back_url" => $url_call_back . $hash_file,
            "extension" => $extension,
            "qualities" => $qualities,
        ];

        if ($options) {
            $params = array_merge($params, $options);
        }

        $fields_string = http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        if ($result["action"]) {
            $video = Video::where("hash_key", $hash_file)->first();
            $video->info = null;
            $video->status_convert = Video::STATUS_WAIT;
            $video->active = 1;
            $video->save();
        }

        curl_close($ch);
        return $result;
    }


    /**
     * @param $directory
     * @param bool $force
     * @return array
     */
    public function deleteDirectory($directory, $force = false)
    {
        $files = Storage::disk(UploaderLibrary::STORAGE_PUBLIC)->allFiles($directory);
        $result = null;
        if (!count($files) || $force) {
            try {
                $resDeleteFiles = Storage::disk(UploaderLibrary::STORAGE_PUBLIC)->delete($files);
                $result = Storage::disk(UploaderLibrary::STORAGE_PUBLIC)->deleteDirectory($directory);
            } catch (\Exception $exception) {
                return ['action' => false, 'count_files' => count($files)];
            }
        }
        return ['action' => $result ? true : false, 'count_files' => count($files)];
    }

    /**
     * @return $this
     */
    protected function deleteBulkDirectory()
    {
        $directory = $this->request->get('directory');
        $force_delete = $this->request->get('force', false);
        $resultDelete = $this->deleteDirectory($directory, $force_delete);
        return $this->setDataResponse($resultDelete)->setResponse($resultDelete['action'] ? true : false, trans("video::messages.alert." . ($resultDelete['action'] ? 'del_success' : 'del_un_success')));

    }

    /**
     * @return $this
     */
    protected function copyVideoMetaGroup()
    {
        $video_meta_group_id = $this->request->get('video_meta_group_id');
        $video_id = $this->request->get('video_id');
        $videoMetaGroup = VideoMetaGroup::find($video_meta_group_id);

        $resultCopy = null;
        if ($videoMetaGroup) {
            $newVideoMetaGroup = $videoMetaGroup->replicate();
            $resultCopy = $newVideoMetaGroup->save();
            if ($resultCopy) {
                $system = 'video_meta_groups';
                $this->copyFeatureValueRelation($video_meta_group_id, $system, $newVideoMetaGroup->id);
            }

        }
        return $this->redirectBack()->setResponse($resultCopy ? true : false, trans("video::messages.alert." . ($resultCopy ? 'save_success' : 'save_un_success')));

    }

    /**
     * @param $item_id
     * @param $system
     * @param $new_item_id
     * @return bool
     */
    protected function copyFeatureValueRelation($item_id, $system, $new_item_id)
    {
        $featureValueRelation = new FeatureValueRelation();
        $fvrs = $featureValueRelation->active()->where('system', $system)->where('item_id', $item_id)->get();

        if (!$fvrs) {
            return false;
        }

        $insertItem = [];
        foreach ($fvrs as $fvr) {
            $insertItem[] = ['language_id' => $fvr->language_id, 'system' => $fvr->system, 'item_id' => $new_item_id
                , 'feature_id' => $fvr->feature_id, 'feature_value_id' => $fvr->feature_value_id
                , 'feature_value' => $fvr->feature_value, 'active' => $fvr->active];
        }

        $featureValueRelation->insert($insertItem);
        return true;
    }

    /**
     * @return $this
     */
    protected function deleteVideoMetaGroup()
    {
        $video_meta_group_id = $this->request->get('video_meta_group_id');
        $video_id = $this->request->get('video_id');
        $videoMetaGroup = VideoMetaGroup::find($video_meta_group_id);
        $resultDelete = null;
        if ($videoMetaGroup) {
            $resultDelete = $videoMetaGroup->delete();
            if ($resultDelete) {
                $system = 'video_meta_groups';
                $this->deleteFeatureValueRelation($video_meta_group_id, $system);
            }

        }
        return $this->redirectBack()->setResponse($resultDelete ? true : false, trans("video::messages.alert." . ($resultDelete ? 'del_success' : 'del_un_success')));
    }

    /**
     * @param $item_id
     * @param $system
     * @return bool
     */
    protected function deleteFeatureValueRelation($item_id, $system)
    {
        $featureValueRelation = new FeatureValueRelation();
        $fvrs = $featureValueRelation->active()->where('system', $system)->where('item_id', $item_id)->delete();
        if (!$fvrs) {
            return false;
        }
        return true;
    }

    /**
     * @return $this
     */
    protected function saveFeaturesValue()
    {
        $video_meta_group_id = $this->request->get('video_meta_group_id');
        $video_id = $this->request->get('video_id');
        $videoMetaGroup = VideoMetaGroup::find($video_meta_group_id);

        if (!$videoMetaGroup) {
            return $this->redirectBack()->setResponse(false, trans("video::messages.alert.not_find_data"));
        }

        //save time
        $videoMetaGroup->start_time = $this->request->get('start_time');
        $videoMetaGroup->end_time = $this->request->get('end_time');
        $videoMetaGroup->save();
        //save time

        //insert feature values
        $features = $this->request->get('feature');

        if (!$features) {
            return $this->redirectBack()->setResponse(false);
        }

        $feature_ids = array_keys($features);

        $featureModel = new Feature();
        $useFeatures = $featureModel->active()->whereIn('id', $feature_ids)->get();

        if (!$useFeatures) {
            return $this->redirectBack()->setResponse(false);
        }

        $featuresType = $useFeatures->pluck('type', 'id')->toArray();

        $customTypes = $featureModel->customTypes;

        $insertItems = [];
        $language_id = app('getInstanceObject')->getLanguageId();
        $item_id = $videoMetaGroup->id;
        $system = 'video_meta_groups';
        foreach ($features as $f_id => $value) {
            $type = isset($featuresType[$f_id]) ? $featuresType[$f_id] : null;

            if (!$type || !$value)
                continue;

            if (in_array($type, $customTypes)) {
                //insert value
                $insertItems[] = ['language_id' => $language_id, 'item_id' => $item_id, 'system' => $system, 'feature_id' => $f_id, 'feature_value' => $value, 'feature_value_id' => null, 'created_at' => date("Y-m-d H:i:s")];
            } else {
                //insert value_id
                if (is_array($value)) {

                    foreach ($value as $item) {
                        $insertItems[] = ['language_id' => $language_id, 'item_id' => $item_id, 'system' => $system, 'feature_id' => $f_id, 'feature_value' => null, 'feature_value_id' => $item, 'created_at' => date("Y-m-d H:i:s")];
                    }

                } else {
                    $insertItems[] = ['language_id' => $language_id, 'item_id' => $item_id, 'system' => $system, 'feature_id' => $f_id, 'feature_value' => null, 'feature_value_id' => $value, 'created_at' => date("Y-m-d H:i:s")];
                }
            }


        }

        $featureValueRelation = new FeatureValueRelation();
        $resultDel = $featureValueRelation->active()->where('system', $system)->where('item_id', $item_id)->delete();

        if (!$insertItems) {
            return $this->redirectBack()->setResponse(false);
        }

        $result = $featureValueRelation->insert($insertItems);
        //insert feature values

        if ($result) {
            return $this->redirect(route('video.editMetaTags', ['video_id' => $video_id]))->setResponse(true, trans("video::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, trans("video::messages.alert.save_un_success"));

    }

    /**
     * @param null $hash_key
     * @param null $data
     * @return $this
     */
    public function converted($hash_key = null, $data = null)
    {
        $hash_file = $hash_key ?: $this->request->get("hash_file");
        $video = Video::where("hash_key", $hash_file)->first();
        $dataInfo = $data ?: $this->request->get("data");
        if ($video && $dataInfo) {
            $video->info = json_encode($dataInfo);
            $video->video_time = isset($dataInfo["info"]["duration"]) ? $dataInfo["info"]["duration"] : "-1";
            $video->status_convert = isset($dataInfo["status"]) ? $dataInfo["status"] : Video::STATUS_NONE;
            $video->save();
        }
        return $this->setResponse(true);
    }

    /**
     * @return $this
     */
    public function getVideoList()
    {
        $q = false;
        $optionAttr = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }
        $model = new Video();
        $model = $model->active()->select('id', 'title as text');
        if ($q) {

            $model->where(function ($query) use ($q) {

                $query->where('title', 'LIKE', '%' . $q . '%');

            });
            $items = $model->get();
            $optionAttr = true;

        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();
            $optionAttr = true;
        } else {
            $items = $this->request->has('selected') ? [] : $model->orderBy('id', 'DESC')->take(20)->get();
        }

        return $this->setDataResponse($items)->setResponse(true);
    }

    /**
     * @param $video_ids
     * @return array
     */
    private function getOpttionAttrVideos($video_ids)
    {
        $video = new Video();
        $videos = $video->active()->whereIn("id", $video_ids)->get();

        $optionAttributes = [];
        if ($videos) {
            $optionAttributes = collect($videos)->mapWithKeys(function ($video) {
                $video_url = $video->video_url;
                $poster_url = $video->poster_url;
                return [$video->id => ['data-video' => $video_url, 'data-poster' => $poster_url]];
            })->toArray();
        }

        return $optionAttributes;

    }
}
