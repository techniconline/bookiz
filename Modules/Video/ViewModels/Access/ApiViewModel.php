<?php

namespace Modules\Video\ViewModels\Access;

use BridgeHelper;
use Illuminate\Support\Facades\Cache;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Video\Models\Video\VideoUse;

class ApiViewModel extends BaseViewModel
{
    public function getVideoAccess(){
        $video_id=$this->request->get('video_id');
        $hash=$this->request->get('hash');
        $data=false;
        if($video_id && $hash){
            $videoUse=$this->getVideo($hash,$video_id);
            if($videoUse && isset($videoUse->video) && $videoUse->video){
                $user_id=0;
                if(BridgeHelper::getAccess()->islogin()){
                    $user_id=BridgeHelper::getAccess()->getUser()->id;
                }
                $info=[
                    'hash'=>$videoUse->video->hash_key,
                    'info'=>$videoUse->video->info_array,
                    'is_public'=>$videoUse->is_public,
                    'extension'=>$videoUse->video->extension,
                    'downloadable'=>$videoUse->downloadable,
                ];
                $data=[
                    'info'=>$info,
                    'access'=>true,
                    'user_id'=>$user_id,
                ];
            }
        }
        return $this->setDataResponse($data,'video')->setResponse(true);
    }


    public function getVideo($hash,$video_id){
        $key='all_video_'.$hash.'_'.$video_id;
        if(!($videoUse=Cache::get($key))){
            $videoUse=VideoUse::with(['video'])->where('hash_key',$hash)->where('video_id',$video_id)->first();
            Cache::put($key,$videoUse,3600);
        }
        return $videoUse;
    }
}
