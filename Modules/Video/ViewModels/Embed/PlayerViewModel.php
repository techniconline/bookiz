<?php

namespace Modules\Video\ViewModels\Embed;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\Video\Models\Video\VideoUse;

class PlayerViewModel extends BaseViewModel
{

    public function getVideoPlayer(){
        $this->layout='None';
        $key= $this->request->get('key');
        $videoUse = VideoUse::active()->where('hash_key',$key)->first();
        if($videoUse && $videoUse->is_public ){
            return $this->renderedView('video::embed.player',['videoUse'=>$videoUse]);
        }
        return $this->setResponse(false, trans("video::messages.alert.not_find_data"));
    }

}
