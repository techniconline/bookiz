<?php

namespace Modules\Video\ViewModels\VideoStream;

use Illuminate\Support\Facades\Cache;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Course\Libraries\Activity\Exam\ExamActivity;
use Modules\Exam\Libraries\ExamQuestion\MasterExamQuestion;
use Modules\Exam\Models\ExamAnswerUser;
use Modules\Exam\Models\ExamAttempt;
use Modules\Exam\Models\ExamQuestion;
use Modules\Exam\Models\ExamAnswer;
use Modules\Exam\Models\ExamQuestionUser;

class VideoStreamViewModel extends BaseViewModel
{

    private $access_assets;
    private $course;
    private $activity;


    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "None";
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {
//            $access_assets [] = ['container' => 'theme_js', 'src' => ("assets/js/modules/exam/exam.js"), 'name' => 'exam-modules'];
        }

        return $access_assets;
    }


    /**
     * @return $this
     */
    protected function setAssetsRenderUserExam()
    {
        $this->access_assets = true;
        return $this;
    }


    /**
     * @param $view
     * @param null $title
     * @return $this
     */
    public function renderVideoStream($view, $title = null)
    {
        $viewModel =& $this;

        $this->setTitlePage($title);
        $this->response["content"] = $view;
        return $this->setResponse(true);
    }

}
