<?php

namespace Modules\User\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Modules\Core\ViewModels\MasterViewModel;

/**
 * Class LoginController
 * @package Modules\User\Http\Controllers\Auth
 */
class LoginJWTController extends Controller
{


    public function login(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('auth.login')->setActionMethod("loginJWT")->response();
    }

    /**
     * @param $hash_code
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function loginByHash($hash_code, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['hash_code'=>$hash_code], $request)->setViewModel('auth.login')->setActionMethod("loginByHash")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('auth.login')->setActionMethod("logoutJWT")->response();
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }
}
