<?php

namespace Modules\User\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Core\ViewModels\MasterViewModel;

class ForgotPasswordJWTController extends Controller
{


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function checkUser(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('auth.forgot')->setActionMethod("getCheckUserJWT")->response();
    }

    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePassword($id, Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['reset_id' => $id], $request)->setViewModel('auth.forgot')->setActionMethod("changePasswordJWT")->response();
    }


}
