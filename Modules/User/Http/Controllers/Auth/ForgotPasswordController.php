<?php

namespace Modules\User\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Core\ViewModels\MasterViewModel;

class ForgotPasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('auth.forgot')->setActionMethod("getForgotForm")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function checkUser(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('auth.forgot')->setActionMethod("getCheckUser")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function reset($id,Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['reset_id'=>$id],$request)->setViewModel('auth.forgot')->setActionMethod("getConfirmPasswordForm")->response();
    }

    public function resetByHash($hash,Request $request,MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['hash'=>$hash],$request)->setViewModel('auth.forgot')->setActionMethod("getConfirmByHash")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePassword($id,Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['reset_id'=>$id],$request)->setViewModel('auth.forgot')->setActionMethod("changePassword")->response();
    }


}
