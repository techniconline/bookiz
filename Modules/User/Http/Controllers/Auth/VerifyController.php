<?php

namespace Modules\User\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

/**
 * Class LoginController
 * @package Modules\User\Http\Controllers\Auth
 */
class VerifyController extends Controller
{
    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('auth.verify')->setActionMethod("getVerifyForm")->response();
    }

    public function verify(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('auth.verify')->setActionMethod("verify")->response();
    }


    public function changeForm(MasterViewModel $masterViewModel){
        return $masterViewModel->setViewModel('auth.verify')->setActionMethod("getChangeForm")->response();
    }

    public function changeSave(MasterViewModel $masterViewModel){
        return $masterViewModel->setViewModel('auth.verify')->setActionMethod("saveChange")->response();
    }


    public function resend(MasterViewModel $masterViewModel){
        return $masterViewModel->setViewModel('auth.verify')->setActionMethod("resend")->response();
    }


    public function verifyByHash($hash,Request $request,MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['hash'=>$hash],$request)->setViewModel('auth.verify')->setActionMethod("verifyByHash")->response();
    }

}
