<?php

namespace Modules\User\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Core\ViewModels\MasterViewModel;

class RegisterJWTController extends Controller
{

    /**
     * Handle a registration request for the application.
     *
     * @param MasterViewModel $masterViewModel
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('auth.register')->setActionMethod("registerJWT")->response();
    }

}
