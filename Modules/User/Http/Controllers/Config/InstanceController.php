<?php

namespace Modules\User\Http\Controllers\Config;

use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class InstanceController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('config.instance')->setActionMethod("getConfigForm")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('config.instance')->setActionMethod("save")->response();
    }


}
