<?php

namespace Modules\User\Http\Controllers\Meta;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class MetaController extends Controller
{

    /**
     * @param $user_id
     * @param $user_meta_group_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveFeaturesValue($user_id, $user_meta_group_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['user_id' => $user_id, 'user_meta_group_id' => $user_meta_group_id], $request)
            ->setViewModel('meta')
            ->setActionMethod("saveFeaturesValue")->response();
    }

    /**
     * @param $user_id
     * @param $user_meta_group_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function copyUserMetaGroup($user_id, $user_meta_group_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['user_id' => $user_id, 'user_meta_group_id' => $user_meta_group_id], $request)
            ->setViewModel('meta')
            ->setActionMethod("copyUserMetaGroup")->response();
    }

    /**
     * @param $user_id
     * @param $user_meta_group_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteUserMetaGroup($user_id, $user_meta_group_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['user_id' => $user_id, 'user_meta_group_id' => $user_meta_group_id], $request)
            ->setViewModel('meta')
            ->setActionMethod("deleteUserMetaGroup")->response();
    }

    /**
     * Show the specified resource.
     * @param $user_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editMetaTags($user_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['user_id' => $user_id], $request)->setViewModel('meta')
            ->setActionMethod("editMetaTags")->response();
    }

}
