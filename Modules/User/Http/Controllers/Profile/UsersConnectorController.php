<?php

namespace Modules\User\Http\Controllers\Profile;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

/**
 * Class LoginController
 * @package Modules\User\Http\Controllers\Auth
 */
class UsersConnectorController extends Controller
{
    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function connectorList(Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setRequest($request)->setViewModel('profile.connector.list')->setActionMethod("getUserConnectorList")->response();
    }

//    /**
//     * @param $id
//     * @param Request $request
//     * @param MasterViewModel $masterViewModel
//     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
//     */
//    public function connectorEdit(Request $request, MasterViewModel $masterViewModel){
//        return $masterViewModel->setRequest($request)->setViewModel('profile.connector.edit')->setActionMethod("getUserConnectorEditForm")->response();
//    }
//
//    /**
//     * @param $id
//     * @param Request $request
//     * @param MasterViewModel $masterViewModel
//     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
//     */
//    public function connectorSave(Request $request, MasterViewModel $masterViewModel){
//        return $masterViewModel->setRequest($request)->setViewModel('profile.connector.edit')->setActionMethod("saveUserConnector")->response();
//    }
//
//    /**
//     * @param $id
//     * @param Request $request
//     * @param MasterViewModel $masterViewModel
//     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
//     */
//    public function connectorCreate(Request $request, MasterViewModel $masterViewModel){
//        return $masterViewModel->setRequest($request)->setViewModel('profile.connector.create')->setActionMethod("getUserConnectorCreateForm")->response();
//    }
//
//    /**
//     * @param $id
//     * @param Request $request
//     * @param MasterViewModel $masterViewModel
//     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
//     */
//    public function connectorCreateSave(Request $request, MasterViewModel $masterViewModel){
//        return $masterViewModel->setRequest($request)->setViewModel('profile.connector.create')->setActionMethod("saveUserConnector")->response();
//    }

}
