<?php

namespace Modules\User\Http\Controllers\Profile;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\MasterViewModel;

/**
 * Class LoginController
 * @package Modules\User\Http\Controllers\Auth
 */
class UsersController extends Controller
{

    /**
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('profile.user.edit')->setActionMethod("getUserEditForm")->response();
    }

    /**
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getQrCodeLogin(Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('profile.user.edit')->setActionMethod("getQrCodeLogin")->response();
    }

    /**
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveEdit(Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setRequest($request)->setViewModel('profile.user.edit')->setActionMethod("saveUser")->response();
    }

    /**
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePassword(Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setRequest($request)->setViewModel('profile.user.password')->setActionMethod("getUserPasswordForm")->response();
    }

    /**
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function savePassword(Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setRequest($request)->setViewModel('profile.user.password')->setActionMethod("saveUserPassword")->response();
    }


}
