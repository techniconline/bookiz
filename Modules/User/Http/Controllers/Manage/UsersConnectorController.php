<?php

namespace Modules\User\Http\Controllers\Manage;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

/**
 * Class LoginController
 * @package Modules\User\Http\Controllers\Auth
 */
class UsersConnectorController extends Controller
{
    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function connectorList($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.connector.list')->setActionMethod("getUserConnectorList")->response();
    }



    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function connectorEdit($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.connector.edit')->setActionMethod("getUserConnectorEditForm")->response();
    }

    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function connectorSave($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.connector.edit')->setActionMethod("saveUserConnector")->response();
    }



    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function connectorCreate($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.connector.create')->setActionMethod("getUserConnectorCreateForm")->response();
    }

    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function connectorCreateSave($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.connector.create')->setActionMethod("saveUserConnector")->response();
    }

}
