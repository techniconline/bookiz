<?php

namespace Modules\User\Http\Controllers\Manage;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

/**
 * Class LoginController
 * @package Modules\User\Http\Controllers\Auth
 */
class UsersController extends Controller
{
    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel){
        return $masterViewModel->setViewModel('manage.user.list')->setActionMethod("getAllUsersList")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function creditList($id, Request $request,MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['user_id'=>$id],$request)->setViewModel('manage.user.credit_grid')->setActionMethod("getAllUserCreditList")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(MasterViewModel $masterViewModel){
        return $masterViewModel->setViewModel('manage.user.create')->setActionMethod("getUserCreateForm")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveCreate(MasterViewModel $masterViewModel){
        return $masterViewModel->setViewModel('manage.user.create')->setActionMethod("saveUser")->response();
    }

    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.user.edit')->setActionMethod("getUserEditForm")->response();
    }

    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveEdit($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.user.edit')->setActionMethod("saveUser")->response();
    }


    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function password($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.user.password')->setActionMethod("getUserPasswordForm")->response();
    }

    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function savePassword($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.user.password')->setActionMethod("saveUserPassword")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function searchUser(MasterViewModel $masterViewModel){
        return $masterViewModel->setViewModel('manage.user.list')->setActionMethod("searchUser")->response();
    }

}
