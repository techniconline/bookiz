<?php

namespace Modules\User\Http\Controllers\Manage\Role;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

/**
 * Class PermissionController
 * @package Modules\User\Http\Controllers\Auth
 */
class RoleController extends Controller
{
    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel){
        return $masterViewModel->setViewModel('manage.role.list')->setActionMethod("getAllRoleList")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id,Request $request,MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.role.edit')->setActionMethod("getForm")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save($id,Request $request,MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.role.edit')->setActionMethod("save")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function permission($id,Request $request,MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.role.permission')->setActionMethod("getForm")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function savePermission($id,Request $request,MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.role.permission')->setActionMethod("save")->response();
    }

}
