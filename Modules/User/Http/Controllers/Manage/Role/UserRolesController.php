<?php

namespace Modules\User\Http\Controllers\Manage\Role;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

/**
 * Class LoginController
 * @package Modules\User\Http\Controllers\Auth
 */
class UserRolesController extends Controller
{

    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function roles($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.user.role.list')->setActionMethod("getUserRoles")->response();
    }


    /**
     * @param $user_id
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function roleEdit($user_id, $id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id,'user_id'=>$user_id],$request)->setViewModel('manage.user.role.edit')->setActionMethod("getForm")->response();
    }

    /**
     * @param $user_id
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveRole($user_id, $id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id,'user_id'=>$user_id],$request)->setViewModel('manage.user.role.edit')->setActionMethod("save")->response();
    }


    /**
     * @param $user_id
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function roleDelete($user_id, $id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id,'user_id'=>$user_id],$request)->setViewModel('manage.user.role.edit')->setActionMethod("delete")->response();
    }

    /**
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function json(Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['is_ajax'=>1],$request)->setViewModel('manage.user.role.json')->setActionMethod("getJson")->response();
    }


}
