<?php

namespace Modules\User\Http\Controllers\Manage\Permission;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

/**
 * Class PermissionController
 * @package Modules\User\Http\Controllers\Auth
 */
class PermissionController extends Controller
{
    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel){
        return $masterViewModel->setViewModel('manage.permission.list')->setActionMethod("getAllPermissionList")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(MasterViewModel $masterViewModel){
        return $masterViewModel->setViewModel('manage.permission.create')->setActionMethod("getCreateForm")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save(MasterViewModel $masterViewModel){
        return $masterViewModel->setViewModel('manage.permission.create')->setActionMethod("save")->response();
    }

}
