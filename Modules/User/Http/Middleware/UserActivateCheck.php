<?php

namespace Modules\User\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Modules\Core\Traits\Config\Settings;

class UserActivateCheck
{
    use Settings;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check()){
            $exceptions=array('user.verify','user.verify.resend','user.verify.change');
            if(in_array(Route::current()->getName(),$exceptions)){
                return $next($request);
            }
            if($this->isActiveUser()){
                return $next($request);
            }else{
                return redirect()->route('user.verify');
            }
        }else{
            $exceptions=array('login','reset','forgot','register','forgot.reset','forgot.hash','verify.hash');
            if(in_array(Route::current()->getName(),$exceptions)){
                return $next($request);
            }
            $instance=app('getInstanceObject')->getCurrentInstance();
            if($instance && $instance->name==config('app.admin')){
                return redirect()->route('login');
            }
        }
        return $next($request);
    }


    public function isActiveUser(){
        $user=Auth::user();
        if($user->confirm){
            return true;
        }
        $verify_type=$this->setSettings('user','instance')->getSetting('verify_type');
        if(is_null($verify_type)){
            $verify_type='singup';
        }
        $result=false;
        switch ($verify_type){
            case 'month':
                $expireTime=strtotime($user->created_at)+2592000;
                if($expireTime >= time()){
                    $result=true;
                }
                break;
        }
        return $result;
    }
}
