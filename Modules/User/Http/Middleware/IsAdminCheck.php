<?php

namespace Modules\User\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use BridgeHelper;
use Illuminate\Support\Facades\Route;
use Modules\Core\Traits\Config\Settings;

class IsAdminCheck
{
    use Settings;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $instance=app('getInstanceObject')->getInstance();
         if($instance->name==config('app.admin')) {
             if(BridgeHelper::getAccess()->canAccessAdminInstance()){
                 $role=BridgeHelper::getAccess()->getUserRole();
                 $instance = app('getInstanceObject')->getInstance();
                 if($role->instance_id !=$instance->id){
                     $current_instance = session()->get('current_instance_id');
                     if(!$current_instance){
                         app('getInstanceObject')->setCurrentInstanceById($role->instance_id);
                     }
                 }
                 $route=Route::currentRouteName();
                 if($route){
                     BridgeHelper::getAccess()->canAccessRoute($route,true);
                 }
                 return $next($request);
             }
         }
         $url=url('/');
         return redirect(str_replace('admin.',config('app.instance').'.',$url))->with('error',trans('user::message.action.access_error'));
    }


}
