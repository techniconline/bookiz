<?php

namespace Modules\User\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Contracts\Auth\Factory as AuthFactory;

class Auth
{
    /**
     * The authentication factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(AuthFactory $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $instance = app('getInstanceObject')->getInstance();
        if ($instance && $instance->name == config('app.api')) {
            $middleware = new AuthJWT();
        }else{
            $middleware = new Authenticate($this->auth);
        }

        return $middleware->handle($request, $next);

    }
}
