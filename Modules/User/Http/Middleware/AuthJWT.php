<?php

namespace Modules\User\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use JWTAuth;
use Exception;

class AuthJWT
{

    private $exception;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $instance = app('getInstanceObject')->getInstance();
        if ($instance && $instance->name == config('app.api')) {

//          $user = JWTAuth::toUser($request->input('token'));
            if (!$this->auth()) {
                if (!$this->exception) {
                    return response()->json(['action' => false, 'need_login' => true, 'message' => trans('user::message.jwt.not_find_user'), 'code' => 404]);
                } else {
                    if ($this->exception instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {

                        return response()->json(['action' => false, 'need_login' => true, 'message' => trans('user::message.jwt.not_find_token')]);

                    } else if ($this->exception instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {

                        return response()->json(['action' => false, 'need_login' => true, 'message' => trans('user::message.jwt.token_is_expire')]);

                    } else {
                        return response()->json(['action' => false, 'need_login' => true, 'message' => trans('user::message.jwt.err_system'), 'error' => trans('user::message.jwt.error_system', ['message' => $this->exception->getMessage(), 'code' => $this->exception->getCode()])]);

                    }
                }
            }

        }

        return $next($request);
    }

    /**
     * @return bool
     */
    public function auth()
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if ($user) {
                $res = auth()->loginUsingId($user->id);
                return true;
            } else {
                return false;
            }
        } catch (Exception $exception) {
            $this->exception = $exception;
            return false;
        }
    }
}
