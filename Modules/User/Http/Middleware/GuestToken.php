<?php

namespace Modules\User\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class GuestToken
{

    private $request;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(app('getInstanceObject')->isApi()){
            $jwt = new AuthJWT();
            $jwt->auth();
            $quest_token = $request->get("guest_token");
            if (!$quest_token) {
                $value = sha1(rand(1000, 10000000) . time() . rand(1000, 10000000));
            }else{
                $value = $quest_token;
            }
        }else{
            if ($value = Cookie::get('guest_token')) {
                Cookie::queue('guest_token', $value, 133920);
            } else {
                $value = sha1(rand(1000, 10000000) . time() . rand(1000, 10000000));
                Cookie::queue('guest_token', $value, 133920);
            }
        }
        app('getInstanceObject')->setUserToken($value);
        return $next($request);
    }

}
