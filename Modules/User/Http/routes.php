<?php


Route::group(['middleware' => 'web', 'prefix' => 'user', 'namespace' => 'Modules\User\Http\Controllers'], function () {

    Route::group(['namespace' => '\Auth'], function () {

        Route::group(['prefix' => 'jwt', 'as' => 'user.jwt.'], function () {

            Route::post('/register', 'RegisterJWTController@register')->name('register');
            Route::post('/login', 'LoginJWTController@login')->name('login');
            Route::post('/login/{hash_code}', 'LoginJWTController@loginByHash')->name('login_by_hash');
            Route::post('/logout', 'LoginJWTController@logout')->name('logout');

            Route::post('/forgot', 'ForgotPasswordJWTController@checkUser')->name('forgot');
            Route::post('/forgot/reset/{id}', 'ForgotPasswordJWTController@changePassword')->name('forgot.reset');

        });

        Route::get('/login', 'LoginController@index')->name('login');
        Route::post('/login', 'LoginController@login')->name('login');
        Route::post('/logout', 'LoginController@logout')->name('logout');
        Route::get('/register', 'RegisterController@showRegistrationForm')->name('register');
        Route::post('/register', 'RegisterController@register')->name('register');

        Route::get('/forgot', 'ForgotPasswordController@index')->name('forgot');
        Route::post('/forgot', 'ForgotPasswordController@checkUser')->name('forgot');
        Route::get('/forgot/reset/{id}', 'ForgotPasswordController@reset')->name('forgot.reset');
        Route::post('/forgot/reset/{id}', 'ForgotPasswordController@changePassword')->name('forgot.reset');

        Route::get('/forgot/hash/{hash}', 'ForgotPasswordController@resetByHash')->name('forgot.hash');
        Route::get('/verify/hash/{hash}', 'VerifyController@verifyByHash')->name('verify.hash');
    });

});


Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'user', 'as' => 'user.', 'namespace' => 'Modules\User\Http\Controllers'], function () {

    Route::group(['namespace' => '\Auth'], function () {
        Route::get('/verify', 'VerifyController@index')->name('verify');
        Route::post('/verify', 'VerifyController@verify')->name('verify');
        Route::get('/verify/change', 'VerifyController@changeForm')->name("verify.change");
        Route::post('/verify/change', 'VerifyController@changeSave')->name("verify.change");

        Route::get('/verify/resend', 'VerifyController@resend')->name('verify.resend');
    });

    Route::group(['prefix' => 'profile', 'as' => 'profile.','namespace' => '\Profile'], function () {
        Route::get('/edit', 'UsersController@edit')->name('edit');
        Route::get('/qrcode_login', 'UsersController@getQrCodeLogin')->name('qrcode_login');
        Route::post('/save', 'UsersController@saveEdit')->name("edit.save");

        Route::get('/changePassword', 'UsersController@changePassword')->name('password.form');
        Route::post('/savePassword', 'UsersController@savePassword')->name("password.save");

        Route::get('/connectors', 'UsersConnectorController@connectorList')->name('connectors');

    });

});


Route::group(['middleware' => ['web', 'auth', 'admin'], 'prefix' => 'user', 'as' => 'user.', 'namespace' => 'Modules\User\Http\Controllers'], function () {

    Route::group(['prefix' => 'config', 'as' => 'config.', 'namespace' => '\Config'], function () {
        Route::get('/instance/index', 'InstanceController@index')->name('instance.index');
        Route::post('/instance/save', 'InstanceController@save')->name('instance.save');
    });

    Route::group(['prefix' => 'meta', 'as' => 'meta.', 'namespace' => '\Meta'], function () {

        Route::post('/{user_id}/{meta_group_id}/features/value/save', 'MetaController@saveFeaturesValue')->name('features.value.save');
        Route::get('/{user_id}/tags/edit', 'MetaController@editMetaTags')->name('editMetaTags');
        Route::post('/{user_id}/{user_meta_group_id}/copy', 'MetaController@copyUserMetaGroup')->name('copy');
        Route::delete('/{user_id}/{user_meta_group_id}/delete', 'MetaController@deleteUserMetaGroup')->name('delete');

    });

    Route::group(['prefix' => 'manage', 'as' => 'manage.', 'namespace' => '\Manage'], function () {
        Route::get('/', 'UsersController@index')->name('index');

        Route::get('/create', 'UsersController@create')->name('create');
        Route::get('/searchUser', 'UsersController@searchUser')->name('search_user');
        Route::post('/create', 'UsersController@saveCreate')->name('create');

        Route::get('/edit/{id}', 'UsersController@edit')->name('edit');
        Route::post('/edit/{id}', 'UsersController@saveEdit')->name('edit');

        Route::get('/credit_list/{id}', 'UsersController@creditList')->name('credit_list');

        Route::get('/password/{id}', 'UsersController@password')->name('password');
        Route::post('/password/{id}', 'UsersController@savePassword')->name('password');

        /*connector*/
        Route::get('/connector/{id}', 'UsersConnectorController@connectorList')->name('connector');
        Route::get('/connector/edit/{id}', 'UsersConnectorController@connectorEdit')->name('connector.edit');
        Route::post('/connector/edit/{id}', 'UsersConnectorController@connectorSave')->name('connector.edit');
        Route::get('/connector/create/{id}', 'UsersConnectorController@connectorCreate')->name('connector.create');
        Route::post('/connector/create/{id}', 'UsersConnectorController@connectorCreateSave')->name('connector.create');


        /*roles*/
        Route::group(['prefix' => 'user','as'=>'user.','namespace' => '\Role'], function()
        {
            Route::get('/roles/json/', 'UserRolesController@json')->name('roles.json');
            Route::get('/roles/{id}', 'UserRolesController@roles')->name('roles');
            Route::get('/roles/edit/{user_id}/{id}', 'UserRolesController@roleEdit')->name('roles.edit');
            Route::post('/roles/edit/{user_id}/{id}', 'UserRolesController@saveRole')->name('roles.edit');
            Route::get('/roles/delete/{user_id}/{id}', 'UserRolesController@roleDelete')->name('roles.delete');

        });


        /*roles*/
        Route::group(['prefix' => 'role','as'=>'role.','namespace' => '\Role'], function()
        {
            Route::get('/', 'RoleController@index')->name('index');
            Route::get('/edit/{id}', 'RoleController@edit')->name('edit');
            Route::post('/edit/{id}', 'RoleController@save')->name('edit');
            Route::get('/permission/{id}', 'RoleController@permission')->name('permission');
            Route::post('/permission/{id}', 'RoleController@savePermission')->name('permission');


        });
        /*permissions*/
        Route::group(['prefix' => 'permission','as'=>'permission.','namespace' => '\Permission'], function()
        {
            Route::get('/', 'PermissionController@index')->name('index');
            Route::get('/create', 'PermissionController@create')->name('create');
            Route::post('/create', 'PermissionController@save')->name('create');

        });
    });


});


