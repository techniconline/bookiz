<?php

namespace Modules\User\ViewModels\Meta;


use Illuminate\Http\Request;
use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureValueRelation;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Meta\MetaGroup;
use Modules\Core\Traits\Feature\FormFeatureBuilder;
use Modules\User\Models\User;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\User\Models\Meta\UserMetaGroup;


class MetaViewModel extends BaseViewModel
{

    use GridViewModel;
    use FormFeatureBuilder;

    private $user_meta_assets;

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $user_assets = [];

        //use bested assets
        if ($this->user_meta_assets) {
            $user_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/user/user.js"), 'name' => 'user-backend'];
        }
        return $user_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return $this
     */
    protected function saveFeaturesValue()
    {
        $user_meta_group_id = $this->request->get('user_meta_group_id');
        $user_id = $this->request->get('user_id');
        $userMetaGroup = UserMetaGroup::find($user_meta_group_id);

        if (!$userMetaGroup) {
            return $this->redirectBack()->setResponse(false, trans("user::messages.alert.not_find_data"));
        }
        $userMetaGroup->save();
        //save time

        //insert feature values
        $features = $this->request->get('feature');

        if (!$features) {
            return $this->redirectBack()->setResponse(false);
        }

        $feature_ids = array_keys($features);

        $featureModel = new Feature();
        $useFeatures = $featureModel->active()->whereIn('id', $feature_ids)->get();

        if (!$useFeatures) {
            return $this->redirectBack()->setResponse(false);
        }

        $featuresType = $useFeatures->pluck('type', 'id')->toArray();

        $customTypes = $featureModel->customTypes;

        $insertItems = [];
        $language_id = app('getInstanceObject')->getLanguageId();
        $item_id = $userMetaGroup->id;
        $system = 'user_meta_groups';
        foreach ($features as $f_id => $value) {
            $type = isset($featuresType[$f_id]) ? $featuresType[$f_id] : null;

            if (!$type || !$value)
                continue;

            if (in_array($type, $customTypes)) {
                //insert value
                $insertItems[] = ['language_id' => $language_id, 'item_id' => $item_id, 'system' => $system, 'feature_id' => $f_id, 'feature_value' => $value, 'feature_value_id' => null, 'created_at' => date("Y-m-d H:i:s")];
            } else {
                //insert value_id
                if (is_array($value)) {

                    foreach ($value as $item) {
                        $insertItems[] = ['language_id' => $language_id, 'item_id' => $item_id, 'system' => $system, 'feature_id' => $f_id, 'feature_value' => null, 'feature_value_id' => $item, 'created_at' => date("Y-m-d H:i:s")];
                    }

                } else {
                    $insertItems[] = ['language_id' => $language_id, 'item_id' => $item_id, 'system' => $system, 'feature_id' => $f_id, 'feature_value' => null, 'feature_value_id' => $value, 'created_at' => date("Y-m-d H:i:s")];
                }
            }


        }

        $featureValueRelation = new FeatureValueRelation();
        $resultDel = $featureValueRelation->active()->where('system', $system)->where('item_id', $item_id)->delete();

        if (!$insertItems) {
            return $this->redirectBack()->setResponse(false);
        }

        $result = $featureValueRelation->insert($insertItems);
        //insert feature values

        if ($result) {
            return $this->redirect(route('user.meta.editMetaTags', ['user_id' => $user_id]))->setResponse(true, trans("user::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, trans("user::messages.alert.save_un_success"));

    }


    /**
     * @return $this
     */
    protected function setAssetsEditMetaTags()
    {
        $this->user_meta_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function editMetaTags()
    {
        $user = new User();
        $user = $user->enable()->with(['metaGroups.featureGroups.featureGroupRelations.feature.featureValues'])->find($this->request->get('user_id'));

        if (!$user) {
            return $this->redirect(route('user.meta.editMetaTags', ['user_id' => $this->request->get('user_id')]))->setResponse(false, trans("user::messages.alert.not_find_data"));
        }

        $user->addDefaultMeta();
        $userMetaGroups = $metaGroups = null;
        $metaFeatureDataList = $metaGroupsActive = [];
        $metaGroups = MetaGroup::active()->filterLanguage()->filterSystem(User::SYSTEM_NAME)->get();

        $userMetaGroups = $user->userMetaGroups()->get();

        if ($userMetaGroups) {
            $metaGroupsActive = $userMetaGroups->pluck('meta_group_id')->toArray();

            $listMetaGroup = $metaGroups ? $metaGroups->pluck('title', 'id') : null;

            $userMetaGroups = ($userMetaGroups)->map(function ($item) use ($listMetaGroup) {
                $item['group_name'] = isset($listMetaGroup[$item->meta_group_id]) ? $listMetaGroup[$item->meta_group_id] : null;
                return $item;
            });

            $userMetaGroupList = $userMetaGroups->pluck('meta_group_id', 'id');
            $metaFeatureData = FeatureValueRelation::active()->where('system', 'user_meta_groups')->whereIn('item_id', ($userMetaGroupList->keys()))->get();

            foreach ($metaFeatureData as $item) {
                $metaFeatureDataList[$item->item_id][$item->feature_id][] = $item->feature_value_id ? $item->feature_value_id : $item->feature_value;
            }

        }

        $this->setTitlePage(trans('user::user.edit_meta_tags') . ':' . $user->title);

        return $this->renderedView("user::user.form_user_meta_tags", ['user' => $user, 'metaGroups' => $metaGroups
            , 'metaGroupsActive' => $metaGroupsActive, 'userMetaGroups' => $userMetaGroups
            , 'metaFeatureDataList' => $metaFeatureDataList, 'view_model' => $this], "form");


    }


    /**
     * @return $this
     */
    protected function copyUserMetaGroup()
    {
        $user_meta_group_id = $this->request->get('user_meta_group_id');
        $user_id = $this->request->get('user_id');
        $userMetaGroup = UserMetaGroup::find($user_meta_group_id);

        $resultCopy = null;
        if ($userMetaGroup) {
            $newUserMetaGroup = $userMetaGroup->replicate();
            $resultCopy = $newUserMetaGroup->save();
            if ($resultCopy) {
                $system = 'user_meta_groups';
                $this->copyFeatureValueRelation($user_meta_group_id, $system, $newUserMetaGroup->id);
            }

        }
        return $this->redirectBack()->setResponse($resultCopy ? true : false, trans("user::messages.alert." . ($resultCopy ? 'save_success' : 'save_un_success')));

    }

    /**
     * @param $item_id
     * @param $system
     * @param $new_item_id
     * @return bool
     */
    protected function copyFeatureValueRelation($item_id, $system, $new_item_id)
    {
        $featureValueRelation = new FeatureValueRelation();
        $fvrs = $featureValueRelation->active()->where('system', $system)->where('item_id', $item_id)->get();

        if (!$fvrs) {
            return false;
        }

        $insertItem = [];
        foreach ($fvrs as $fvr) {
            $insertItem[] = ['language_id' => $fvr->language_id, 'system' => $fvr->system, 'item_id' => $new_item_id
                , 'feature_id' => $fvr->feature_id, 'feature_value_id' => $fvr->feature_value_id
                , 'feature_value' => $fvr->feature_value, 'active' => $fvr->active];
        }

        $featureValueRelation->insert($insertItem);
        return true;
    }

    /**
     * @return $this
     */
    protected function deleteUserMetaGroup()
    {
        $user_meta_group_id = $this->request->get('user_meta_group_id');
        $user_id = $this->request->get('user_id');
        $userMetaGroup = UserMetaGroup::find($user_meta_group_id);
        $resultDelete = null;
        if ($userMetaGroup) {
            $resultDelete = $userMetaGroup->delete();
            if ($resultDelete) {
                $system = 'user_meta_groups';
                $this->deleteFeatureValueRelation($user_meta_group_id, $system);
            }

        }
        return $this->redirectBack()->setResponse($resultDelete ? true : false, trans("user::messages.alert." . ($resultDelete ? 'del_success' : 'del_un_success')));
    }

    /**
     * @param $item_id
     * @param $system
     * @return bool
     */
    protected function deleteFeatureValueRelation($item_id, $system)
    {
        $featureValueRelation = new FeatureValueRelation();
        $fvrs = $featureValueRelation->active()->where('system', $system)->where('item_id', $item_id)->delete();
        if (!$fvrs) {
            return false;
        }
        return true;
    }

}
