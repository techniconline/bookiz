<?php

namespace Modules\User\ViewModels\Blocks\UserBlock;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Traits\Block\MasterBlock;


class UserBlock
{

    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        if(BridgeHelper::getAccess()->isLogin()){
            $user=BridgeHelper::getAccess()->getUser();
            return view('user::widgets.user.info',compact('user'))->render();
        }else{
            return '';
        }
    }

}
