<?php

namespace Modules\User\ViewModels\Blocks\UserBlock;


use Modules\Core\Traits\Block\MasterBlockConfig;

class UserBlockConfigs
{
    use MasterBlockConfig;

    public $baseClass='UserBlock';

    public function __construct(){
        $this->addTab('user',trans('user::block.user_block'));
    }


}