<?php

namespace Modules\User\ViewModels\Blocks\UserPopupBlock;


use BridgeHelper;
use Modules\Core\Traits\Block\MasterBlock;
use Modules\User\ViewModels\Auth\LoginViewModel;
use Modules\User\ViewModels\Auth\RegisterViewModel;

class UserPopupBlock
{
    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel=& $this;
        if(BridgeHelper::getAccess()->isLogin()){
            $user=BridgeHelper::getAccess()->getUser();
            return view('user::widgets.user.info',['user'=>$user,'blockViewModel'=>$blockViewModel])->render();
        }else{
            return view('user::widgets.popup.index',compact('blockViewModel'))->render();
        }
    }

    public function getReferer(){
        $url=url()->previous();
        if($url != route('login')){
            return $url;
        }
        return '';
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function getLoginForm(){
        $loginView=new LoginViewModel();
        return $loginView->getLoginForm()->view;
    }

    /**
     * @return mixed
     */
    public function getRegisterForm(){
        $loginView=new RegisterViewModel();
        return $loginView->getRegisterForm()->view;
    }
}
