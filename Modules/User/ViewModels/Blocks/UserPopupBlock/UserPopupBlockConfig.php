<?php

namespace Modules\User\ViewModels\Blocks\UserPopupBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;

class UserPopupBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='UserPopupBlock';

    public function __construct(){
        $this->setDefaultName(trans('user::block.user_popup'))
            ->addTab('user_popup',trans('user::block.user_popup'),'user::widgets.popup.config.form');
    }

}
