<?php

namespace Modules\User\ViewModels\Profile\Connector;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\User\Models\Register\UserRegisterKey;
use DateHelper;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;


    public function setGridModel()
    {
        $user = BridgeHelper::getAccess()->getUser();
        $this->Model = UserRegisterKey::with(['user','connector'])->where('user_id',$user->id);
    }

    /**
     * @return $this
     */
    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('user::form.fields.id'), true)
             ->addColumn('connector', trans('user::form.fields.connector'), false)
             ->addColumn('value', trans('user::form.fields.value'), true)
             ->addColumn('status', trans('user::form.fields.status'), true)
             ->addColumn('date', trans('user::form.fields.date'), false);

        $show = array(
            'name' => 'user.verify',
//            'parameter' => ['id']
        );
        $this->addAction('user_verify', $show, trans('user::form.fields.verify_user'), 'fa fa-unlock', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);


        $this->setPaginate(false);
        return $this;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
        if ($row->status!='wait') {
            unset($action['user_verify']);
        }
        return $action;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->full_name=$row->user->full_name;
        $row->status=view('user::component.connector_status',['status'=>$row->status]);
        $row->connector=trans('user::data.connectors.'.$row->connector->name);
        $row->date=DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getUserConnectorList()
    {
        $this->setTitlePage(trans('user::routes.user.manage_user_connectors'));
        $this->generateGridList();
        return $this->renderedView("user::manage.connector.list", ['view_model' => $this], trans('user::routes.user.manage_user_connectors'));
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function getUserConnectors()
    {
        $this->setTitlePage(trans('user::routes.user.manage_user_connectors'));
        $this->generateGridList();
        return view("user::manage.connector.list", ['view_model' => $this])->render();
    }


}
