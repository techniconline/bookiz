<?php

namespace Modules\User\ViewModels\Profile\User;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\User\Models\User;
use Modules\User\Traits\User\UserViewModelTrait;
use Modules\User\ViewModels\Profile\Connector\ListViewModel;

class EditViewModel extends BaseViewModel
{
    use UserViewModelTrait;

    private $user = null;
    public $connectorViewModel;

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getUserEditForm()
    {
//        $this->layout = 'user.layout';
        $BlockViewModel =& $this;
        $this->getConnectorListViewModel();
        $this->user = BridgeHelper::getAccess()->getUser();
        if ($this->user){
            $connectors = $this->user->userConnectors;
            $this->user->userConnectors = $connectors;
        }
        if (app('getInstanceObject')->isApi()) {
            $this->user = $this->decorateAttributes($this->user);
            return $this->setDataResponse($this->user)->setResponse($this->user ? true : false, $this->user ? null : trans('user::message.action.user_not_find'));
        }

        if (!$this->user) {
            return $this->redirectBack()->setResponse(false, trans('user::message.action.user_not_find'));
        }
        $this->setTitlePage(trans('user::routes.user.manage_edit_user', ['full_name' => $this->user->full_name]));
        return $this->renderedView('user::profile.user.edit', compact('BlockViewModel'));
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getQrCodeLogin()
    {
        $this->layout = 'user.layout';
        $BlockViewModel =& $this;
        $this->getConnectorListViewModel();
        $this->user = BridgeHelper::getAccess()->getUser();

        if (!$this->user) {
            return $this->redirectBack()->setResponse(false, trans('user::message.action.user_not_find'));
        }

        $data = $this->setQrCodeLogin($this->user);
        $hash_code = $data ? $data->hash : null;
        $url_app = url(env('DOWNLOAD_APP_PAGE','/downloads/FarayadAndroid.apk')).'?hash_code='.$hash_code;
        $hash_code = $url_app;
        $date_time_available = DateHelper::setDateTime(date("Y-m-d H:i:s",$data->available_time))->getLocaleFormat(trans('core::date.datetime.medium'));
        $date_time_now = DateHelper::setDateTime(date("Y-m-d H:i:s"))->getLocaleFormat(trans('core::date.datetime.medium'));
        $this->setTitlePage(trans('user::routes.user.create_qr_code_login', ['full_name' => $this->user->full_name]));
        return $this->renderedView('user::profile.user.qrcode_form', compact('BlockViewModel', 'hash_code', 'date_time_available','date_time_now'));
    }

    /**
     * @param $user
     * @return bool|User\PasswordReset
     */
    public function setQrCodeLogin($user)
    {
        if (!$user) {
            return false;
        }

        $resetModel = new User\PasswordReset();
        $row = $resetModel->where('user_id', $user->id)->whereNull('token')->first();
        $available_time = $row ? $row->available_time : null;
        if ($available_time > time()) {
            return $row;
        }

        if ($row) {
            $row->delete();
        }

        $data = [
            'user_id' => $user->id,
            'hash' => $resetModel->generateHashKey(),
            'available_time' => $resetModel->createAvailableTime(),
        ];

        if ($resetModel->fill($data)) {
            $resetModel->save();
            return $resetModel;
        }
        return false;
    }


    /**
     * @return $this
     */
    private function getConnectorListViewModel()
    {
        $this->connectorViewModel = new ListViewModel();
        return $this;
    }

    /**
     * @return $this
     */
    public function saveUser()
    {
        $user = BridgeHelper::getAccess()->getUser();
        $this->user = User::find($user->id);
        $this->request->offsetUnset('username');

        $this->requestValuesUpdate(['birthday' => 'Y-m-d'])->validate($this->getUserEditRules());
        try {
            $data = $this->request->all();
            if ($this->request->hasFile('avatar')) {
                $data['avatar'] = $this->saveUserAvatar($this->user->id);
            }

            $result = $this->user->fill($data)->save();
            $message = trans('user::message.action.success');
        } catch (\Exception $e) {
            report($e);
            $result = false;
            $message = trans('user::message.action.error');
        }
        if (app('getInstanceObject')->isApi() && $result) {
            $this->setDataResponse($this->user);
        }
        return $this->redirectBack()->setResponse(boolval($result), $message);
    }

    /**
     * @return array
     */
    private function getUserSaveRules()
    {
        $canValidate = ['father_name', 'national_code', 'date', 'country_id', 'state_id', 'city_id', 'gender', 'marital', 'avatar_confirm', 'confirm', 'avatar'];
        $rules = [
            'first_name' => trans('user::rules.first_name'),
            'last_name' => trans('user::rules.last_name'),
        ];
        if ($this->request->get('username') != $this->getUserData('username')) {
            $rules['username'] = trans('user::rules.username');
        }
        foreach ($canValidate as $key) {
            if ($this->request->has($key) && !empty($this->request->get($key))) {
                $rules[$key] = trans('user::rules.' . $key);
            }
        }
        return $rules;
    }

    /**
     * @return array
     */
    private function getUserEditRules()
    {
        $canValidate = ['father_name', 'first_name', 'last_name', 'national_code', 'date', 'country_id', 'state_id', 'city_id', 'gender', 'marital', 'avatar_confirm', 'confirm', 'avatar'];
        $rules = [];
        $user_name = $this->request->get('username');
        if ($user_name && $user_name != $this->getUserData('username')) {
            $rules['username'] = trans('user::rules.username');
        }
        foreach ($canValidate as $key) {
            if ($this->request->has($key) && !empty($this->request->get($key))) {
                $rules[$key] = trans('user::rules.' . $key);
            }
        }
        return $rules;
    }

    /**
     * @param null $name
     * @param null $default
     * @return null
     */
    public function getUserData($name = null, $default = null)
    {
        if (is_null($name)) {
            return $this->user;
        }
        if (isset($this->user->{$name})) {
            return $this->user->{$name};
        }
        return $default;
    }


}
