<?php

namespace Modules\User\ViewModels\Profile\User;

use Illuminate\Support\Facades\Hash;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\User\Models\User;
use Modules\User\Traits\User\UserViewModelTrait;

class PasswordViewModel extends BaseViewModel
{
    use UserViewModelTrait;

    private $user = null;


    public function getUserPasswordForm()
    {
        $this->layout='user.layout';
        $BlockViewModel =& $this;
        $user = BridgeHelper::getAccess()->getUser();
        $this->user = User::find($user->id);
        if (!$this->user) {
            return $this->redirectBack()->setResponse(false, trans('user::message.action.user_not_find'));
        }
        $this->setTitlePage(trans('user::routes.user.manage_change_password', ['full_name' => $this->user->full_name]));
        return $this->renderedView('user::profile.user.change_password', compact('BlockViewModel'));
    }

    /**
     * @return $this
     */
    public function saveUserPassword()
    {
        if (!$this->setModel(new User())->isValidRequest($this->request->all(), $this->getRules(),[],'POST')->isValidRequest) {
            return $this->redirectBack()->setResponse(false, trans('user::message.action.save_un_success'));
        }

        $user = BridgeHelper::getAccess()->getUser();
        $this->user = $this->useModel->find($user->id);

        if(!Hash::check($this->request->get("current_password"), $this->user->password)){
            return $this->redirectBack()->setResponse(false, trans('user::message.action.current_password_is_not_valid'));
        }

        try {
            $this->user->password = bcrypt($this->request->get('password'));
            $result = $this->user->save();
            $message = trans('user::message.action.success');
        } catch (\Exception $e) {
            report($e);
            $result = false;
            $message = trans('user::message.action.error');
        }
        return $this->redirectBack()->setResponse(boolval($result), $message);
    }

    public function getRules()
    {
        return [
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ];
    }

    public function getUserData($name = null)
    {
        if (is_null($name)) {
            return $this->user;
        }
        if (isset($this->user->{$name})) {
            return $this->user->{$name};
        }
        return null;
    }

}
