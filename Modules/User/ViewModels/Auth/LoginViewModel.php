<?php

namespace Modules\User\ViewModels\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Modules\User\Models\Device;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\User\Models\User;
use Modules\User\Rules\ConnectorRule;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginViewModel extends AuthViewModel
{

    use AuthenticatesUsers;

    protected $redirectTo = 'login';
    protected $redirectRoute = 'login';

    public function __construct()
    {
        $this->setTitlePage(trans('user::auth.login'));
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    public function getLoginForm()
    {
        $viewModel =& $this;
        $this->layout = 'auth.login';
        return $this->renderedView("user::auth.login.form", compact('viewModel'));
    }

    /**
     * @return $this|void
     */
    public function login()
    {
        try {
            $message = trans('user::validation.auth.failed');
            $this->firstStepLogin();

            if ($this->hasTooManyLoginAttempts($this->request)) {
                $this->fireLockoutEvent($this->request);
                return $this->sendLockoutResponse($this->request);
            }
            if ($this->attemptLogin($this->request)) {
                $result = true;
                $this->sendLoginResponse($this->request);
                return $this->redirect($this->getUserRedirectUrl())->setResponse($result);
            }
        } catch (ValidationException $e) {
            $message = $e->validator->getMessageBag()->first();
        } catch (Exception $e) {
            $message = $e->getMessage();
        }


        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($this->request);
        $result = false;
        // $this->sendFailedLogin();
        return $this->redirect(route('login'))->setResponse($result, $message);
    }

    /**
     *
     */
    protected function firstStepLogin()
    {
        // Check validation
        $connector = $this->setConnectorByRequest()->getCurrentConnector();
        $messages = $connector->getMessage();
        $connecterRule = $connector->getRule();
        $rules = [
            'password' => ['required', 'string', 'min:6', 'max:50'],
            'connector' => $connecterRule,
        ];
        $this->request->validate($rules, $messages);
        $this->setUsernameFromConnector($connector);
    }

    /**
     * @param $connector
     * @return bool
     */
    public function setUsernameFromConnector($connector)
    {
        $user = UserRegisterKey::with('user')->where('connector_id', $connector->getId())->where('value', $this->request->get('connector'))->first();
        if ($user) {
            $this->request->merge(['username' => $user->user->username]);
            return true;
        }
        throw ValidationException::withMessages([
            'connector' => [trans('user::validation.auth.failed')],
        ]);
    }


    /**
     *
     */
    public function sendFailedLogin()
    {
        throw ValidationException::withMessages([
            'connector' => [trans('user::validation.auth.failed')],
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     *
     */
    protected function loginJWT()
    {
        $user_id = $this->request->get('user_id');
        $user = new User();
        $user->token = null;
        $action = false;
        if ($user_id) {
            $user = $user->find($user_id);
            $token = $this->loginByUserObject($user);
            $user->token = $token;
            $action = $token ? true : false;
        } else {
            $this->firstStepLogin();
            $inputs = $this->request->only('username', 'password');
            $token = JWTAuth::attempt($inputs);
            if ($token) {
                $user = JWTAuth::toUser($token);
                $user->token = $token;
                $action = $token ? true : false;
            }
        }

        if($action){
            $data = $this->request->all();
            $data['user_id'] = $user->id;
            $this->registerDevicePushNotification($data);
        }
        $message = $action ? null : trans("user::message.action.user_not_find");
        return $this->setDataResponse($user)->setResponse($action, $message);
    }

    /**
     * @param $data
     * @return bool
     */
    public function registerDevicePushNotification($data)
    {
        if (isset($data['user_id']) && $data['user_id'] && isset($data['push_id']) && $data['push_id']) {

            $data['push_token'] = isset($data['push_token']) ? $data['push_token'] : '';
            $data['push_user_id'] = $data['push_id'];
            $data['os'] = isset($data['os']) ? $data['os'] : '-';
            $data['app'] = isset($data['app']) ? $data['app'] : config('app.instance', 'www');
            $model = new Device();

            $exist = Device::enable()->where('user_id', $data['user_id'])->where(function ($q) use ($data) {
                $q = $q->where('push_user_id', $data['push_id'])
                    ->where('os', $data['os'])->where('app', $data['app']);
                if ($data['push_token']) {
                    $q = $q->where('push_token', $data['push_token']);
                }
                return $q;
            })->first();

            if ($exist) {
                return false;
            }

            if ($model->fill($data)->save()) {
                return true;
            }

            return false;
        }
    }

    /**
     * @param null $hash
     * @return $this
     */
    protected function loginByHash($hash = null)
    {
        $hash = $hash ?: $this->request->get('hash_code');
        $resetModel = new User\PasswordReset();
        $row = $resetModel->where('hash', $hash)->whereNull('token')->first();

        if (!$row) {
            return $this->setDataResponse(null)->setResponse(false, trans("user::message.action.verify_token_wrong"));
        }

        $available_time = $row ? $row->available_time : null;
        if ($available_time < time()) {
            $row->delete();
            return $this->setDataResponse(null)->setResponse(false, trans("user::message.action.token_expired"));
        }

        $user = new User();
        $user = $user->find($row->user_id);
        $token = $this->loginByUserObject($user);
        if ($token) {
            $row->delete();
        }
        $user->token = $token;
        $connector = $this->decorateAttributes($user->userConnectors()->active()->first());
        $user->connector = $connector;
        $action = $token ? true : false;
        $message = $action ? null : trans("user::message.action.user_not_find");
        return $this->setDataResponse($user)->setResponse($action, $message);

    }

    /**
     * @param User $user
     * @return mixed
     */
    public function loginByUserObject(User $user)
    {
        return JWTAuth::fromUser($user);
    }


    /**
     * @return $this
     */
    protected function logoutJWT()
    {
        if ($token = $this->getTokenJwt()) {

            try {
                $result = JWTAuth::setToken($token)->invalidate();
                return $this->setResponse($result, trans('user::message.action.' . ($result ? 'success_in_logout' : 'error_in_logout')));
            } catch (\Exception $exception) {
                return $this->setResponse(false, trans('user::message.action.before_logout'));
            }

        }
        return $this->setResponse(false, trans('user::message.action.user_not_find'));
    }

    /**
     * @return mixed
     */
    protected function getTokenJwt()
    {
        return JWTAuth::getToken();
    }


}
