<?php

namespace Modules\User\ViewModels\Auth;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\User\Traits\User\UserViewModelTrait;
use BridgeHelper;

class AuthViewModel extends BaseViewModel
{
    use UserViewModelTrait;


    /**
     * @return mixed|null|string
     */
    public function getUserRedirectUrl()
    {
        $route = BridgeHelper::getConfig()->getSettings('home', 'instance', 'core');
        if (!$route) {
            $route = 'core.index';
        }
        $referer = $this->request->has('referer') ? $this->request->get('referer') : null;
        if (!$referer) {
            $referer = route($route);
        }
        return $referer;
    }


    /**
     * @return array|mixed|string
     */
    public function getReferer()
    {
        if (isset($this->request) && $this->request->has('referer')) {
            return $this->request->get('referer');
        }
        $url = request()->header('referer');
        if ($url != route('login')) {
            return $url;
        }
        return '';
    }


}
