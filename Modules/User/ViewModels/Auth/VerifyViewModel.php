<?php

namespace Modules\User\ViewModels\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Modules\User\Models\Register;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\User\Notifications\UserVerify;
use Modules\User\Rules\ConnectorRule;
use function PHPSTORM_META\type;

class VerifyViewModel extends AuthViewModel
{

    public function __construct()
    {
        parent::__construct();
        $this->setTitlePage(trans('user::auth.verify'));
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    public function getVerifyForm()
    {
        $this->layout = 'auth.register';
        $viewModel =& $this;
        $connector = $this->canVerify();
        if (!$connector) {
            return $this->redirect('/')->setResponse(true, trans('user::message.action.verify_closed'));
        }
        $this->sendVerifyToken($connector);
        if ($this->is_api) {
            return $this->setDataResponse($connector)->setResponse(true);
        }
        return $this->renderedView("user::auth.verify.form", compact('viewModel', 'connector'));
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    public function getChangeForm()
    {
        $viewModel =& $this;
        $this->layout ='auth.register';
        $connector = $this->canVerify();
        if (!$connector) {
            return $this->redirect('/')->setResponse(true, trans('user::message.action.verify_closed'));
        }
        return $this->renderedView("user::auth.verify.change", compact('viewModel', 'connector'));
    }


    /**
     * @return $this
     */
    public function resend()
    {
        $connector = $this->canVerify();
        if (!$connector) {
            return $this->redirect('/')->setResponse(true, trans('user::message.action.verify_closed'));
        }
        $this->sendVerifyToken($connector, true);
        return $this->redirect(route('user.verify'))->setResponse(true, trans('user::message.action.verify_resend'));
    }

    /**
     * @return $this
     */
    public function saveChange()
    {
        $connector = $this->canVerify();
        if (!$connector) {
            return $this->redirect('/')->setResponse(true, trans('user::message.action.verify_closed'));
        }
        $newConnector = $this->setConnectorByRequest()->getCurrentConnector();
        $messages = $newConnector->getMessage();
        $connecterRule = $newConnector->getRule();
        $connecterRule[] = new ConnectorRule($newConnector->getId());
        $rules = [
            'connector' => $connecterRule,
        ];
        $this->request->validate($rules, $messages);
        $connector->connector_id = $newConnector->getId();
        $connector->token = $newConnector->getToken();
        $connector->hash = $newConnector->getHash($newConnector->getId(), $connector->token);
        $connector->value = $this->request->get('connector');
        $connector->status = 'deactive';
        try {
            $connector->save();
            return $this->redirect(route('user.verify'))->setResponse(true, trans('user::message.action.verify_change', ['text' => $this->getConnectorLabel()]));
        } catch (\Exception $e) {
            return $this->redirect(route('user.verify.change'))->setResponse(false, trans('user::message.action.verify_change_fail'));
        }

    }

    /**
     * @return $this
     */
    public function verify()
    {
        $connector = $this->canVerify();
        if (!$connector) {
            return $this->redirect('/')->setResponse(true, trans('user::message.action.verify_closed'));
        }
        $this->request->validate(['token' => ['required']]);
        if ($this->request->get('token') == $connector->token) {
            $register = new Register();
            $result = $register->activeUser($connector);
            if ($result) {
                return $this->redirect(url("/"))->setResponse(true, trans('user::message.action.verify_token_success'));
            } else {
                return $this->redirect(route('user.verify'))->setResponse(false, trans('user::message.action.verify_token_fail'));
            }
        } else {
            return $this->redirect(route('user.verify'))->setResponse(false, trans('user::message.action.verify_token_wrong'));
        }
    }


    /**
     * @return $this
     */
    public function verifyByHash()
    {
        $UserRegisterKey = UserRegisterKey::with('user')->where('hash', $this->request->get('hash'));
        if (Auth::check()) {
            $user = Auth::user();
            $UserRegisterKey->where('user_id', $user->id);
        }
        $userConnector = $UserRegisterKey->first();
        if ($userConnector) {
            $register = new Register();
            $result = $register->activeUser($userConnector, $userConnector->user);
            if ($result) {
                if (!isset($user)) {
                    Auth::loginUsingId($userConnector->user->id);
                }
                return $this->redirect('/')->setResponse(true, trans('user::message.action.verify_token_success'));
            } else {
                return $this->redirect(route('user.verify'))->setResponse(false, trans('user::message.action.verify_token_fail'));
            }
        }
        return $this->redirect('/')->setResponse(false, trans('user::message.action.verify_token_wrong'));
    }


    /**
     * @param $userConnector
     * @param bool $force
     * @return bool
     */
    public function sendVerifyToken($userConnector, $force = false)
    {
        if ($userConnector->status == 'deactive' || $force) {

            if(is_null($userConnector->token)){
                $this->request->offsetSet('connector',$userConnector->value);
                $newConnector = $this->setConnectorByRequest()->getCurrentConnector();
                $userConnector->token = $newConnector->getToken();
                $userConnector->hash =$newConnector->getHash($userConnector->token, $userConnector->token);
            }

            $user = Auth::user();
            $userConnector->notifyNow(new UserVerify($user, $userConnector));
            $userConnector->status = 'wait';
            $userConnector->save();
            return true;
        }
        return false;
    }


    /**
     * @return mixed
     */
    public function canVerify()
    {
        $user = Auth::user();
        $userRegisterKey = new UserRegisterKey();
        return $userRegisterKey->where('user_id', $user->id)/*->whereIN('status', ['deactive', 'wait'])*/->first();
    }

}
