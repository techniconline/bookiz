<?php

namespace Modules\User\ViewModels\Auth;

use BridgeHelper;
use Modules\User\Rules\ConnectorRule;
use Modules\User\Models\Register;
use Illuminate\Support\Facades\Auth;

class RegisterViewModel extends AuthViewModel
{

    public function __construct()
    {
        $this->setTitlePage(trans('user::auth.register'));
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return [];
    }

    public function getRegisterForm()
    {
        if (BridgeHelper::getConfig()->getSettings('register_hide', 'instance', 'core')) {
            return $this->redirect(route('login'))->setResponse(false, trans('user::message.action.can_not_register'));
        }
        $this->layout = 'auth.register';
        $viewModel =& $this;
        return $this->renderedView("user::auth.register.form", compact('viewModel'));
    }

    /**
     * @return $this
     */
    public function register()
    {
        if (BridgeHelper::getConfig()->getSettings('register_hide', 'instance', 'core')) {
            return $this->redirect(route('login'))->setResponse(false, trans('user::message.action.can_not_register'));
        }
        $result = $this->onlyRegister();

        if ($result) {
            $user = Auth::loginUsingId($result->id);
            if ($user) {
                return $this->redirect($this->getUserRedirectUrl())->setResponse(true);
            } else {
                return $this->redirectBack()->setResponse(false, trans('user::message.action.login_error'));
            }
        }
        return $this->redirectBack()->setResponse($result, trans('user::message.action.register_error'));

    }

    /**
     * @return $this
     */
    protected function registerJWT()
    {
        $result = $this->onlyRegister();
        if ($result) {
            $user = $result;
            if ($user) {
                $loginViewModel = new LoginViewModel();
                $token = $loginViewModel->loginByUserObject($user);
                $user->token = $token;
                if ($token) {
                    $viewModelLogin = new LoginViewModel();
                    $data = $this->request->all();
                    $data['user_id'] = $user->id;
                    $viewModelLogin->registerDevicePushNotification($data);
                }
                return $this->setDataResponse($user)->setResponse(true);
            } else {
                return $this->setResponse(false, trans('user::message.action.login_error'));
            }
        }
        return $this->setResponse($result, trans('user::message.action.register_error'), $this->errors);

    }

    /**
     * @return bool|\Modules\User\Models\User
     */
    protected function onlyRegister()
    {
        $connector = $this->setConnectorByRequest()->getCurrentConnector();
        $messages = $connector->getMessage();
        $connecterRule = $connector->getRuleRegister();
        $connecterRule[] = new ConnectorRule($connector->getId());
        $rules = [
            'first_name' => ['required', 'string', 'max:50'],
            'last_name' => ['required', 'string', 'max:50'],
            'password' => ['required', 'string', 'min:6', 'max:50'],
            'connector' => $connecterRule,
        ];

        if (get_instance()->isApi()) {
            $this->isValidRequest($this->request->all(), $rules, $messages, "POST", new \stdClass());
            if (!$this->isValidRequest) {
                return false;
            }
        } else {
            $this->request->validate($rules, $messages);
        }
        $register = new Register();
        $result = $register->registerUser($this->request, $connector);
        return $result;
    }


    public function getReferer()
    {
        if (isset($this->request) && $this->request->has('referer')) {
            return $this->request->get('referer');
        }
        $url = url()->previous();
        if ($url != route('register')) {
            return $url;
        }
        return '';
    }

}
