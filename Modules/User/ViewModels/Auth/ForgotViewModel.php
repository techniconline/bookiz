<?php

namespace Modules\User\ViewModels\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Modules\User\Models\Connector;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\User\Models\PasswordReset;
use Modules\User\Models\User;
use Modules\User\Notifications\PasswordReset AS PasswordResetNotify;

class ForgotViewModel extends AuthViewModel
{
    private $passwordReset = null;
    private $reset_id = null;

    public function __construct()
    {
        $this->setTitlePage(trans('user::auth.forgot'));
    }


    public function getForgotForm()
    {
        $this->layout = 'auth.register';
        $viewModel =& $this;
        return $this->renderedView("user::auth.forgot.form", compact('viewModel'));
    }


    /**
     * @return $this
     */
    public function getCheckUser()
    {
        $resetId = $this->onlyCheckuser()['reset_id'];
        return $this->redirect(route('forgot.reset', ['id' => $resetId]))->setResponse(true, trans('user::message.action.forgot_send'));
    }

    /**
     * @return $this
     */
    public function getCheckUserJWT()
    {
        $reset = $this->onlyCheckuser();
        $resetId = $reset['reset_id'];
        return $this->setDataResponse($reset)->setResponse($resetId ? true : false, trans('user::message.action.' . ($resetId ? 'forgot_send' : 'user_not_find')));
    }

    /**
     * @return int|mixed
     */
    protected function onlyCheckUser()
    {
        $connector = $this->setConnectorByRequest()->getCurrentConnector();
        $messages = $connector->getMessage();
        $connecterRule = $connector->getRule();
        $rules = [
            'connector' => $connecterRule,
        ];
        $this->request->validate($rules, $messages);
        $userConnector = $this->getUserByConnector($connector, $this->request->get('connector'));
        $resetId = 0;
        $hashKey = null;
        if ($userConnector) {
            session(['forgot' => ['user_id' => $userConnector->user_id, 'connector_id' => $userConnector->connector_id]]);
            $PasswordReset = $this->createToken($userConnector, $connector);
            $userConnector->notifyNow(new PasswordResetNotify($userConnector->user, $userConnector, $PasswordReset));
            $resetId = $PasswordReset->id;
            $hashKey = $PasswordReset->hash;
        }
        return ['reset_id' => $resetId, 'hash' => $hashKey];
    }

    /**
     * @param $connector
     * @param $connectorValue
     * @return \Illuminate\Database\Eloquent\Model|mixed|null|object|static
     */
    public function getUserByConnector($connector, $connectorValue)
    {
        $userConnector = UserRegisterKey::with('user')->where('connector_id', $connector->getId())->where('value', $connectorValue)->first();
        return $userConnector;
    }


    public function createToken($userConnector, $connector)
    {
        $PasswordReset = PasswordReset::where('connector_id', $userConnector->connector->id)->where('user_id', $userConnector->user->id)->first();
        if (!$PasswordReset) {
            $PasswordReset = new PasswordReset;
            $PasswordReset->connector_id = $connector->getId();
            $PasswordReset->user_id = $userConnector->user->id;
        }
        $PasswordReset->token = $connector->getToken();
        $PasswordReset->hash = $connector->getHash();
        $PasswordReset->available_time = time() + $connector->reset_time;
        $PasswordReset->save();
        return $PasswordReset;
    }


    public function getConfirmPasswordForm($reset_id = null)
    {
        $viewModel =& $this;
        if (is_null($reset_id)) {
            $reset_id = $this->request->get('reset_id');
        }
        $this->reset_id = $reset_id;
        $this->layout = 'auth.register';
        return $this->renderedView("user::auth.forgot.confirm_password", compact('viewModel'));
    }

    public function getConfirmByHash()
    {
        $passwordReset = PasswordReset::where('hash', $this->request->get('hash'))->first();
        if ($passwordReset) {
            $this->passwordReset = $passwordReset;
            return $this->getConfirmPasswordForm($this->passwordReset->id);
        }
        return $this->redirect('/')->setResponse(false, trans('user::message.action.verify_token_wrong'));
    }


    /**
     * @return $this
     */
    public function changePassword()
    {
        $user = $this->onlyChangePassword();
        $user = Auth::loginUsingId($user->id);
        if ($user) {
            return $this->redirect('/')->setResponse(true);
        } else {
            return $this->redirectBack()->setResponse(false, trans('user::message.action.login_error'));
        }
    }

    /**
     * @return $this
     */
    public function changePasswordJWT()
    {
        $user = $this->onlyChangePassword();
        $loginViewModel = new LoginViewModel();
        $token = $loginViewModel->loginByUserObject($user);
        if ($token) {
            $user->token = $token;
            return $this->setDataResponse($user)->setResponse(true, trans('user::message.action.password_changed'));
        } else {
            return $this->setResponse(false, trans('user::message.action.login_error'));
        }
    }

    /**
     * @return mixed
     */
    protected function onlyChangePassword()
    {
        $rules = ['password' => 'required|confirmed|min:6', 'token' => 'required|min:5'];
        if ($useHash = $this->request->get('hash')) {
            unset($rules['token']);
        }
        $this->request->validate($rules);
        if ($useHash) {
            $passwordReset = PasswordReset::where('hash', $useHash)->first();
        } else {
            $passwordReset = PasswordReset::find($this->request->get('reset_id'));
        }
        if (!$passwordReset || (!$useHash && $passwordReset->token != $this->request->get('token'))) {
            throw ValidationException::withMessages([
                'token' => [trans('user::message.action.verify_token_wrong')],
            ]);
        }
        $user = User::find($passwordReset->user_id);
        $connector=UserRegisterKey::where('user_id',$passwordReset->user_id)->where('connector_id',$passwordReset->connector_id)->first();
        if($connector && $connector->status!='active'){
            $connector->status='active';
            $connector->save();
        }
        $user->password = Hash::make($this->request->get('password'));
        if($user->confirm){
            $user->confirm=1;
        }
        $user->setRememberToken(Str::random(60));
        $user->save();
        $passwordReset->delete();
        return $user;
    }

    public function getPasswordResetHash()
    {
        if ($this->passwordReset) {
            return $this->passwordReset->hash;
        }
        return 0;
    }

    public function getResetId()
    {
        return $this->reset_id;
    }
}
