<?php

namespace Modules\User\ViewModels\Config;

use Modules\Core\ViewModels\Config\SettingViewModel;
use Modules\User\Models\Connector;

class InstanceViewModel extends SettingViewModel
{

    private $myAssets=false;
    public $setting_name='instance';
    public $module_name='user';

    public function getConfigForm(){
        $BlockViewModel=&$this;
        $this->setTitlePage(trans('user::routes.config.instance'));
        return $this->renderedView('user::config.instance',compact('BlockViewModel'));
    }


    public function save(){
        $this->request->validate([
            'connector'=>'required|array|min:1',
            'verify_type'=>'required|string',
        ]);
        $result=$this->saveConfig($this->request->all());
        if($result){
            return $this->redirectBack()->setResponse($result,trans('user::message.action.success'));
        }
        return $this->redirectBack()->setResponse($result,trans('user::message.action.error'));

    }

    public function getConnectorList(){
        $items=Connector::all();
        $list=[];
        foreach($items as $item){
            $list[$item->code]=trans('user::values.'.$item->name);
        }
        return $list;
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $assets = [];

        //use bested assets
        if ($this->myAssets) {
            $assets [] = ['container' => 'plugin_style', 'src' => asset("themes/admin/assets/global/plugins/select2/css/select2.min.css"), 'name' => 'select2'];
            $assets [] = ['container' => 'plugin_general_2', 'src' => asset("themes/admin/assets/global/plugins/select2/js/select2.full.min.js"), 'name' => 'select2'];
            $assets [] = ['container' => 'plugin_general_2', 'src' => asset("themes/admin/assets/modules/user/config/instance.js"), 'name' => 'config.instance'];
        }

        return $assets;
    }


    /**
     * @return $this
     */
    protected function setAssetsGetConfigForm()
    {
        $this->myAssets = true;
        return $this;
    }


    public function getFieldsList(){
        $fieldsList=[
            'username'=>trans('user::form.fields.username'),
            'father_name'=>trans('user::form.fields.father_name'),
            'national_code'=>trans('user::form.fields.national_code'),
            'birthday'=>trans('user::form.fields.birthday'),
            'country'=>trans('user::form.fields.country'),
            'state_id'=>trans('user::form.fields.state_id'),
            'city_id'=>trans('user::form.fields.city_id'),
            'gender'=>trans('user::form.fields.gender'),
            'marital'=>trans('user::form.fields.marital'),
        ];
        return $fieldsList;
    }

}
