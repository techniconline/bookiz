<?php

namespace Modules\User\ViewModels\Manage\Permission;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\User\Models\Permission;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;


    public function setGridModel()
    {
        $this->Model =Permission::class;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        $this->per_page=50;
        /*add columns to grid*/
        $this->addColumn('id', trans('user::form.fields.id'), true)
             ->addColumn('module', trans('user::form.fields.module'), true)
             ->addColumn('permission', trans('user::form.fields.permission'), true)
             ->addColumn('description', trans('user::form.fields.description'), true);

        /*filters*/
        $this->addFilter('module','text',['title'=>trans('user::form.fields.module')]);
        $this->addFilter('permission','text',['title'=>trans('user::form.fields.permission')]);
        $this->addFilter('description','text',['title'=>trans('user::form.fields.description')]);

        /*buttons*/
        $routeAdd=route('user.manage.permission.create');
        $options=['class'=>"btn btn-primary",'target'=>"_blank"];
        $this->addButton('add',$routeAdd,trans('user::form.titles.create_permission'),'fa fa-plus',false,'before',$options);

        //$this->export=true;
        //$this->export_file_name='';
        return $this;
    }

    public function getRowsUpdate($row)
    {
        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getAllPermissionList()
    {
        $this->setTitlePage(trans('user::menu.manage.permissions'));
        $this->generateGridList()->renderedView("user::manage.permission.list", ['view_model' => $this], trans('user::menu.manage.permissions'));
        return $this;
    }
}
