<?php

namespace Modules\User\ViewModels\Manage\Permission;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\User\Models\Permission;
use Nwidart\Modules\Facades\Module;
use PHPUnit\Util\Printer;

class CreateViewModel extends BaseViewModel
{
    public function getCreateForm(){
        $BlockViewModel=&$this;
        $this->setTitlePage(trans('user::form.titles.create_permission'));
        return $this->renderedView('user::manage.permission.create',compact('BlockViewModel'));
    }


    public function save(){
        $rules=[
            'module'=>['required','string','max:50'],
            'permission'=>['required','string','min:3','max:50'],
            'description'=>['required','string','min:3','max:50'],
        ];
        $this->requestValuesUpdate()->validate($rules);
        $data=$this->request->all();
        $permission=new Permission;
        $permission->module=$data['module'];
        $permission->permission=$data['permission'];
        $permission->description=$data['description'];
        $result=$permission->save();
        if($result){
           return $this->redirect(route('user.manage.permission.index'))->setResponse(true);
        }
        return $this->redirectBack()->setResponse($result,trans('user::message.action.register_error'));
    }


    public function getModulesList(){
        $all=Module::all();
        $list=[];
        foreach ($all as $key=>$value){
            $list[$key]=$value->name;
        }
        return $list;
    }

}
