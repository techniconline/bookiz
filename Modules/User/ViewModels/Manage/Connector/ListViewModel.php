<?php

namespace Modules\User\ViewModels\Manage\Connector;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\User\Models\Register\UserRegisterKey;
use DateHelper;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;


    public function setGridModel()
    {
        $id=$this->request->get('id');
        $this->Model = UserRegisterKey::with(['user','connector'])->where('user_id',$id);
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('user::form.fields.id'), true)
             ->addColumn('full_name', trans('user::form.fields.full_name'), false)
             ->addColumn('connector', trans('user::form.fields.connector'), false)
             ->addColumn('value', trans('user::form.fields.value'), true)
             ->addColumn('status', trans('user::form.fields.status'), true)
             ->addColumn('date', trans('user::form.fields.date'), false);


        $edit = array(
            'name' => 'user.manage.connector.edit',
            'parameter' => ['id']
        );
        $this->addAction('edit', $edit, trans('user::routes.user.manage_connector_edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $this->action_text = false;

        $routeAdd=route('user.manage.connector.create',['id'=>$this->request->get('id')]);
        $options=['class'=>"btn btn-primary pull-right",'target'=>"_blank"];
        $this->addButton('add',$routeAdd,trans('user::form.titles.create_connector'),'fa fa-plus',false,'before',$options);


        $routeAdd=route('user.manage.index');
        $options=['class'=>"btn btn-default pull-right",'target'=>"_blank"];
        $this->addButton('back',$routeAdd,trans('user::form.titles.back'),'fa fa-arrow-circle-right',false,'before',$options);

        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->full_name=$row->user->full_name;
        $row->status=view('user::component.connector_status',['status'=>$row->status]);
        $row->connector=$row->connector->name;
        $row->date=DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getUserConnectorList()
    {
        $this->setTitlePage(trans('user::routes.user.manage_user_connectors'));
        $this->generateGridList()->renderedView("user::manage.connector.list", ['view_model' => $this], trans('user::routes.user.manage_user_connectors'));
        return $this;
    }


}
