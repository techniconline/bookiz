<?php

namespace Modules\User\ViewModels\Manage\Connector;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\User\Models\User;
use Modules\User\Models\Connector;
use Modules\User\Traits\User\UserViewModelTrait;
use Modules\User\Rules\ConnectorRule;


class CreateViewModel extends BaseViewModel
{
    use UserViewModelTrait;

    private $user=null;


    public function getUserConnectorCreateForm(){
        $BlockViewModel=&$this;
        $this->user=User::find($this->request->get('id'));
        if(!$this->user){
            return $this->redirectBack()->setResponse(false,trans('user::message.action.not_find'));
        }
        $this->setTitlePage(trans('user::routes.user.manage_create_connector',['full_name'=>$this->user->full_name]));
        return $this->renderedView('user::manage.connector.create',compact('BlockViewModel'));
    }


    public function saveUserConnector(){
        $this->user=User::find($this->request->get('id'));
        if(!$this->user){
            return $this->redirectBack()->setResponse(false,trans('user::message.action.user_not_find'));
        }
        $this->requestValuesUpdate();
        $connector=$this->setConnectorByRequest($this->user->instance_id)->getCurrentConnector();
        $messages=$connector->getMessage();
        $connecterRule=$connector->getRule();
        $connecterRule[]=new ConnectorRule($connector->getId());
        $rules=[
            'status'=>['required','string'],
            'connector'=>$connecterRule,
        ];
        $this->request->validate($rules);
        try{
            $data=$this->request->all();
            $userConnector=new UserRegisterKey;
            $userConnector->user_id=$this->user->id;
            $userConnector->connector_id=$connector->getId();
            $userConnector->value=$data['connector'];
            $userConnector->status=$data['status'];
            $userConnector->token=$connector->getToken();
            $userConnector->hash=$connector->getHash($connector->getId(),$this->user->id);
            $result=$userConnector->save();
            $message=trans('user::message.action.success');
            return $this->redirect(route('user.manage.connector',['id'=>$this->user->id]))->setResponse($result,$message);
        }catch(\Exception $e){
            report($e);
            $result=false;
            $message=trans('user::message.action.error');
            return $this->redirectBack()->setResponse($result,$message);
        }
    }

    public function getUserData($name=null){
        if(is_null($name)){
            return $this->user;
        }
        if(isset($this->user->{$name})){
            return $this->user->{$name};
        }
        return null;
    }


    public function getConnectorList(){
        $all=Connector::all();
        return $all->pluck('name','id');
    }



}
