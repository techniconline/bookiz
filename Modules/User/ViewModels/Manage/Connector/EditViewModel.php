<?php

namespace Modules\User\ViewModels\Manage\Connector;

use Modules\Core\Traits\Config\Settings;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\User\Models\Connector;
use Modules\User\Rules\ConnectorRule;
use Modules\User\Traits\User\UserViewModelTrait;

class EditViewModel extends BaseViewModel
{
    use UserViewModelTrait;

    private $userConnector=null;


    public function getUserConnectorEditForm(){
        $BlockViewModel=&$this;
        $this->userConnector=UserRegisterKey::with(['user'])->find($this->request->get('id'));
        if(!$this->userConnector){
            return $this->redirectBack()->setResponse(false,trans('user::message.action.not_find'));
        }
        $this->setTitlePage(trans('user::routes.user.manage_edit_connector',['full_name'=>$this->userConnector->user->full_name]));
        return $this->renderedView('user::manage.connector.edit',compact('BlockViewModel'));
    }


    public function saveUserConnector(){
        $this->userConnector=UserRegisterKey::with(['user'])->find($this->request->get('id'));
        if(!$this->userConnector){
            return $this->redirectBack()->setResponse(false,trans('user::message.action.user_not_find'));
        }
        $this->requestValuesUpdate();
        $connector=$this->setConnectorByRequest($this->userConnector->user->instance_id)->getCurrentConnector();
        $messages=$connector->getMessage();
        $connecterRule=$connector->getRule();
        if($this->userConnector->value!=$this->request->get('connector')){
            $connecterRule[]=new ConnectorRule($connector->getId());
        }
        $rules=[
            'status'=>['required','string'],
            'connector'=>$connecterRule,
        ];
        $this->request->validate($rules);
        try{
            $data=$this->request->all();
            $this->userConnector->value=$data['connector'];
            $this->userConnector->status=$data['status'];
            $result=$this->userConnector->save();
            $message=trans('user::message.action.success');
        }catch(\Exception $e){
            report($e);
            $result=false;
            $message=trans('user::message.action.error');
        }
        return $this->redirectBack()->setResponse($result,$message);
    }

    public function getUserData($name=null){
        if(is_null($name)){
            return $this->userConnector;
        }
        if(isset($this->userConnector->{$name})){
            return $this->userConnector->{$name};
        }
        return null;
    }


    public function getConnectorList(){
        $all=Connector::all();
        return $all->pluck('name','id');
    }



}
