<?php

namespace Modules\User\ViewModels\Manage\User;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\User\Models\User;
use Modules\User\Traits\User\UserViewModelTrait;

class PasswordViewModel extends BaseViewModel
{
    use UserViewModelTrait;

    private $user=null;


    public function getUserPasswordForm(){
        $BlockViewModel=&$this;
        $this->user=User::find($this->request->get('id'));
        if(!$this->user){
            return $this->redirectBack()->setResponse(false,trans('user::message.action.user_not_find'));
        }
        $this->setTitlePage(trans('user::routes.user.manage_change_password',['full_name'=>$this->user->full_name]));
        return $this->renderedView('user::manage.user.password',compact('BlockViewModel'));
    }


    public function saveUserPassword(){
        $this->user=User::find($this->request->get('id'));
        if(!$this->user){
            return $this->redirectBack()->setResponse(false,trans('user::message.action.user_not_find'));
        }
        $this->requestValuesUpdate()->validate([
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ]);
        try{
            $this->user->password=bcrypt($this->request->get('password'));
            $result=$this->user->save();
            $message=trans('user::message.action.success');
            return $this->redirect(route('user.manage.index'))->setResponse($result);
        }catch(\Exception $e){
            report($e);
            $result=false;
            $message=trans('user::message.action.error');
        }
        return $this->redirectBack()->setResponse($result,$message);
    }

    public function getUserData($name=null){
        if(is_null($name)){
            return $this->user;
        }
        if(isset($this->user->{$name})){
            return $this->user->{$name};
        }
        return null;
    }

}
