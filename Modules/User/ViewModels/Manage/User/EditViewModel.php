<?php

namespace Modules\User\ViewModels\Manage\User;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\User\Models\User;
use Modules\User\Traits\User\UserViewModelTrait;

class EditViewModel extends BaseViewModel
{
    use UserViewModelTrait;

    private $user=null;


    public function getUserEditForm(){
        $BlockViewModel=&$this;
        $this->user=User::find($this->request->get('id'));
        if(!$this->user){
            return $this->redirectBack()->setResponse(false,trans('user::message.action.user_not_find'));
        }
        $this->setTitlePage(trans('user::routes.user.manage_edit_user',['full_name'=>$this->user->full_name]));
        return $this->renderedView('user::manage.user.edit',compact('BlockViewModel'));
    }


    public function saveUser(){
        $this->user=User::find($this->request->get('id'));
        if(!$this->user){
            return $this->redirectBack()->setResponse(false,trans('user::message.action.user_not_find'));
        }
        $this->requestValuesUpdate(['birthday'=>'Y-m-d'])->validate($this->getUserSaveRules());
        try{
            $data=$this->request->all();
            if($this->request->hasFile('avatar')){

                $data['avatar']=$this->saveUserAvatar($this->user->id);
            }

            $this->user->addDefaultMeta();

            $result=$this->user->fill($data)->save();
            $message=trans('user::message.action.success');
        }catch(\Exception $e){
            report($e);
            $result=false;
            $message=trans('user::message.action.error');
        }
        return $this->redirectBack()->setResponse($result,$message);
    }

    private function getUserSaveRules(){
        $canValidate=['father_name','national_code','date','country_id','state_id','city_id','gender','marital','avatar_confirm','confirm','avatar'];
        $rules=[
            'first_name'=>trans('user::rules.first_name'),
            'last_name'=>trans('user::rules.last_name'),
        ];
        if($this->request->get('username')!=$this->getUserData('username')){
            $rules['username']=trans('user::rules.username');
        }
        foreach($canValidate as $key){
            if($this->request->has($key) && !empty($this->request->get($key))){
                $rules[$key]=trans('user::rules.'.$key);
            }
        }
        return $rules;
    }

    /**
     * @param null $name
     * @param null $default
     * @return null
     */
    public function getUserData($name=null, $default = null){
        if(is_null($name)){
            return $this->user;
        }
        if(isset($this->user->{$name})){
            return $this->user->{$name};
        }
        return $default;
    }




}
