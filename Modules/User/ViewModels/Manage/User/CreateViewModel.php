<?php

namespace Modules\User\ViewModels\Manage\User;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\User\Models\Register;
use Modules\User\Models\User;
use Modules\User\Rules\ConnectorRule;
use Modules\User\Traits\User\UserViewModelTrait;

class CreateViewModel extends BaseViewModel
{
    use UserViewModelTrait;

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getUserCreateForm()
    {
        $BlockViewModel =& $this;
        $this->setTitlePage(trans('user::routes.user.manage_create_user'));
        return $this->renderedView('user::manage.user.create', compact('BlockViewModel'));
    }


    /**
     * @return $this
     */
    public function saveUser()
    {
        $connector = $this->setConnectorByRequest()->getCurrentConnector();
        $messages = $connector->getMessage();
        $connecterRule = $connector->getRuleRegister();
        $connecterRule[] = new ConnectorRule($connector->getId());
        $rules = [
            'first_name' => ['required', 'string', 'max:50'],
            'last_name' => ['required', 'string', 'max:50'],
            'password' => ['required', 'string', 'min:6', 'max:50'],
            'connector' => $connecterRule,
        ];
        $this->isValidRequest($this->request->all(), $rules, $messages,"POST", new \stdClass());

        $result = null;
        if ($this->isValidRequest) {
            $register = new Register();
            $result = $register->registerUser($this->request, $connector);
            if ($result) {
                return $this->redirect(route('user.manage.index'))->setDataResponse($result)->setResponse(true);
            }
        }
        return $this->redirectBack()->setResponse(boolval($result), trans('user::message.action.register_error'), $this->errors);
    }


}
