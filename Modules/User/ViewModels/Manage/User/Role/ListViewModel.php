<?php

namespace Modules\User\ViewModels\Manage\User\Role;

use DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\User\Models\Connector;
use Modules\User\Models\User\UserRole;
use Modules\User\Models\User;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;
    private $user=false;

    public function setGridModel()
    {
        $this->Model = UserRole::with(['role','instance'])->where('user_id',$this->user->id);
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('user::form.fields.id'), true)
             ->addColumn('full_name', trans('user::form.fields.full_name'), false)
             ->addColumn('instance_id', trans('user::form.fields.instance'), true)
             ->addColumn('role_id', trans('user::form.fields.role'), true)
             ->addColumn('default', trans('user::form.fields.default'), true)
             ->addColumn('date', trans('user::form.fields.date'), false);

        /*role*/
        $edit = array(
            'name' => 'user.manage.user.roles.edit',
            'parameter' => ['user_id','id']
        );
        $this->addAction('edit', $edit, trans('user::form.titles.edit'), 'fa fa-pencil', false, [ 'class' => 'btn btn-sm btn-primary']);

        $delete = array(
            'name' => 'user.manage.user.roles.delete',
            'parameter' => ['user_id','id']
        );
        $this->addAction('delete', $delete, trans('user::form.titles.delete'), 'fa fa-times', false, [ 'class' => 'btn btn-sm btn-danger']);


        $routeBack=route('user.manage.index');
        $options=['class'=>"btn btn-default"];
        $this->addButton('aback',$routeBack,trans('user::form.titles.back'),'fa fa-arrow-right',false,'before',$options);


        $routeAdd=route('user.manage.user.roles.edit',['user_id'=>$this->user->id,'id'=>0]);
        $options=['class'=>"btn btn-primary"];
        $this->addButton('add',$routeAdd,trans('user::routes.user.add_role',['full_name'=>$this->user->full_name]),'fa fa-plus',false,'before',$options);




        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->instance_id=$row->instance->name;
        $row->role_id=$row->role->name;
        $row->full_name=$this->user->full_name;
        $row->default=$this->getComponent('yesno',['value'=>$row->default]);
        $row->date=DateHelper::setDateTime()->getLocaleFormat();
        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getUserRoles()
    {
        $this->user=User::find($this->request->get('id'));
        if(!$this->user){
            return $this->redirectBack()->setResponse(false,trans('user::message.action.not_find'));
        }
        $this->modelData=new \stdClass();
        $this->modelData->user_id=$this->user->id;
        $this->setTitlePage(trans('user::routes.user.roles'));
        $this->generateGridList()->renderedView("user::manage.user.role.list", ['view_model' => $this], trans('user::routes.user.roles'));
        return $this;
    }

}
