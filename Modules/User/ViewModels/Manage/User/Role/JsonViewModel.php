<?php

namespace Modules\User\ViewModels\Manage\User\Role;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\User\Models\Role;

class JsonViewModel extends BaseViewModel
{
    public function getJson(){
        $instance_id=false;
        if($this->request->has('instance_id')){
            $instance_id=$this->request->get('instance_id');
        }
        $model=Role::select('id','name','title')->where('type','site');
        if($instance_id){
            $model->where(function ($query) use($instance_id) {
                $query->whereNull('instance_id')
                    ->orWhere('instance_id', '=', $instance_id);
            });
        }else{
            $model->whereNull('instance_id');
        }
        $items=$model->get();
        $items=$items->map(function ($item) {
            $item->title=$item->name;
            $item->value=$item->id;
            return $item;
        });
        return $this->setDataResponse($items)->setResponse(true) ;
    }

}
