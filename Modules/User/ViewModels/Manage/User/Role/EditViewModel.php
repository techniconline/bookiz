<?php

namespace Modules\User\ViewModels\Manage\User\Role;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\User\Models\User\UserRole;

class EditViewModel extends BaseViewModel
{
    public function boot()
    {
        $id=$this->requestValuesUpdate()->get('id');
        $user_id=$this->requestValuesUpdate()->get('user_id');
        if($id){
            $this->modelData=UserRole::find($id);
            if($this->modelData->user_id!=$user_id){
                $this->modelData=false;
            }
        }else{
            $this->modelData=new \stdClass();
            $this->modelData->user_id=$user_id;
        }
    }

    public function getForm(){
        $this->boot();
        $BlockViewModel=&$this;
        $this->setTitlePage(trans('user::routes.user.manage_role'));
        return $this->renderedView('user::manage.user.role.edit',compact('BlockViewModel'));
    }


    public function save(){
        $this->boot();
        $rules=[
            'instance_id'=>['required','integer'],
            'role_id'=>['required','integer'],
        ];
        $this->request->validate($rules);
        $data=$this->request->all();
        if(!($this->modelData instanceof UserRole)){
            $this->modelData=new UserRole;
        }
        $this->modelData->instance_id=$data['instance_id'];
        $this->modelData->role_id=$data['role_id'];
        $this->modelData->user_id=$data['user_id'];
        $result=$this->modelData->save();
        if($result){
            return $this->redirect(route('user.manage.user.roles',['id'=>$data['user_id']]))->setResponse(true,trans('user::message.action.success'));
        }
        return $this->redirectBack()->setResponse($result,trans('user::message.action.register_error'));
    }


    public function delete(){
        $this->boot();
        if(!$this->modelData || !($this->modelData instanceof UserRole)){
            return $this->redirectBack()->setResponse(false,trans('user::message.action.not_find'));
        }
        $data=$this->request->all();
        $result=$this->modelData->delete();
        if($result){
            return $this->redirect(route('user.manage.user.roles',['id'=>$data['user_id']]))->setResponse(true,trans('user::message.action.success'));
        }
        return $this->redirectBack()->setResponse($result,trans('user::message.action.register_error'));
    }

}
