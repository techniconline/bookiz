<?php

namespace Modules\User\ViewModels\Manage\User;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\User\Models\Connector;
use Modules\User\Models\User;
use Modules\User\Providers\UserServiceProvider;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;


    public function setGridModel()
    {
        $mobile = $this->getFilterValueFromRequest('mobile');
        $email = $this->getFilterValueFromRequest('email');
        $params = [];
        if ($mobile) {
            $connector = $this->getConnectorByName('mobile');
            $params['mobile'] = $mobile;
            $params['mobile_connector_id'] = $connector->id;
        }
        if ($email) {
            $connector = $this->getConnectorByName('email');
            $params['email'] = $email;
            $params['email_connector_id'] = $connector->id;

        }
        $this->Model = User::with(['userConnectors', 'userCredit'])->enable()->filterByValue($params)->filterCurrentInstance();
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('image', trans('user::form.fields.avatar'), true)
            ->addColumn('first_name', trans('user::form.fields.first_name'), true)
            ->addColumn('last_name', trans('user::form.fields.last_name'), true)
            ->addColumn('credit', trans('user::form.fields.user_credit'), false)
            ->addColumn('confirm', trans('user::form.fields.confirm'), true)
            ->addColumn('connector', trans('user::form.fields.connector'), false);

        $edit = array(
            'name' => 'user.manage.edit',
            'parameter' => ['id']
        );
        $this->addAction('edit', $edit, trans('user::routes.user.manage_edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $connector = array(
            'name' => 'user.manage.connector',
            'parameter' => ['id']
        );
        $this->addAction('connector', $connector, trans('user::routes.user.manage_connector'), 'fa  fa-connectdevelop', false, ['target' => '', 'class' => 'btn btn-sm btn-danger']);

        $password = array(
            'name' => 'user.manage.credit_list',
            'parameter' => ['id']
        );

        $this->addAction('user_credit', $password, trans('user::routes.user.user_credits'), 'fa  fa-credit-card', false, ['target' => '', 'class' => 'btn btn-sm btn-default yellow']);


        $password = array(
            'name' => 'user.manage.password',
            'parameter' => ['id']
        );
        $this->addAction('password', $password, trans('user::routes.user.change_password'), 'fa  fa-key', false, ['target' => '', 'class' => 'btn btn-sm btn-info']);


        $role = array(
            'name' => 'user.manage.user.roles',
            'parameter' => ['id']
        );
        $this->addAction('role', $role, trans('user::routes.user.roles'), 'fa fa-hand-paper-o', false, ['target' => '', 'class' => 'btn btn-sm btn-danger']);

        $show = array(
            'name' => 'user.meta.editMetaTags',
            'parameter' => ['id']
        );
        $this->addAction('edit_meta_tags', $show, trans('user::routes.user.edit_meta_tags'), 'fa fa-tags', false, ['target' => '', 'class' => 'btn btn-sm btn-warning']);


        $this->action_text = false;

        $routeAdd = route('user.manage.create');
        $options = ['class' => "btn btn-primary", 'target' => "_blank"];
        $this->addButton('add', $routeAdd, trans('user::form.titles.create_user'), 'fa fa-plus', false, 'before', $options);

        /*filters*/
        $this->addFilter('first_name', 'text');
        $this->addFilter('last_name', 'text');
        $this->addFilter('mobile', 'text', ['process' => false]);
        $this->addFilter('email', 'text', ['process' => false]);
        //$this->export=true;
        //$this->export_file_name='';
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->image = view('core::component.avatar', ['class' => 'avatar_small', 'avatar' => $row->small_avatar_url, 'url' => $row->avatar_url]);
        $row->confirm = view('core::component.yesno', ['value' => $row->confirm]);
        $row->connector = view('user::component.connectors', ['userConnectors' => $row->userConnectors]);
        $row->credit = $row->userCredit ? $row->userCredit->current_credit : 0;
        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getAllUsersList()
    {
        $this->setTitlePage(trans('user::menu.manage.users'));
        $this->generateGridList()->renderedView("user::manage.user.list", ['view_model' => $this], trans('user::menu.manage.users'));
        return $this;
    }


    public function getConnectorByName($name)
    {
        return Connector::where('name', $name)->first();
    }

    /**
     * @return $this
     */
    public function searchUser()
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }
        $model = new User();
        $model = $model->setRequestItems($this->request->all())->enable()->select('id', 'last_name', 'first_name', 'username');
        if ($q) {

            $items = $model->filterUser()->get();

        } elseif ($selected && count($selected)) {

            $model->whereIn('id', $selected);
            $items = $model->filterUser()->get();

        } else {
            $items = $this->request->has('selected') ? [] : $model->filterUser()->active()->select('id', 'last_name', 'first_name', 'username')->take(20)->orderBy('id', 'DESC')->get();
        }
        $items = $items->map(function ($item) {
            $item->text = $item->full_name . '(' . $item->username . ')';
            return $item;
        });
        return $this->setDataResponse($items)->setResponse(true);
    }


}
