<?php

namespace Modules\User\ViewModels\Manage\User;

use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\User\Models\Connector;
use Modules\User\Models\Credit;
use Modules\User\Models\User;

class CreditGridViewModel extends BaseViewModel
{
    use GridViewModel;

    public function setGridModel()
    {
        $this->Model = Credit::where('user_id', $this->request->get('user_id'));
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('model_type', trans('user::form.fields.model_type'), true)
            ->addColumn('model_id', trans('user::form.fields.model_id'), true)
            ->addColumn('type', trans('user::form.fields.action_credit_type'), true)
            ->addColumn('amount', trans('user::form.fields.amount'), true)
            ->addColumn('current_credit', trans('user::form.fields.current_credit'), false)
            ->addColumn('created_at', trans('user::form.fields.created_at'), true);


        $this->action_text = false;

        $routeAdd = route('user.manage.index');
        $options = ['class' => "btn btn-primary", 'target' => ""];
        $this->addButton('add', $routeAdd, trans('user::form.titles.list_user'), 'fa fa-list', false, 'before', $options);

        /*filters*/
        $this->addFilter('model_type', 'text', ['title' => trans('user::form.fields.model_type')]);
        $this->addFilter('model_id', 'text', ['title' => trans('user::form.fields.model_id')]);
//        $this->addFilter('mobile','text',['process'=>false]);
//        $this->addFilter('email','text',['process'=>false]);
        //$this->export=true;
        //$this->export_file_name='';
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->type = '<span class="text-'.($row->type=='plus'?'success':'danger').'"> '.($row->type=='plus'?'<i class="fa fa-caret-up"></i>':'<i class="fa fa-caret-down"></i>').' '.trans('user::form.fields.action_credit_types.' . $row->type).'</span>';
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));

        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getAllUserCreditList()
    {
        $this->setTitlePage(trans('user::routes.user.user_credits'));
        $this->generateGridList()->renderedView("user::manage.user.list", ['view_model' => $this], trans('user::menu.manage.users'));
        return $this;
    }
}
