<?php

namespace Modules\User\ViewModels\Manage\Role;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\User\Models\Role;

class EditViewModel extends BaseViewModel
{
    public function boot()
    {
        $id=$this->requestValuesUpdate()->get('id');
        if($id){
            $this->modelData=Role::find($id);
        }
    }

    public function getForm(){
        $this->boot();
        $BlockViewModel=&$this;
        $this->setTitlePage(trans('user::routes.user.manage_role'));
        return $this->renderedView('user::manage.role.edit',compact('BlockViewModel'));
    }


    public function save(){
        $this->boot();
        $rules=[
            'instance_id'=>['nullable','required','integer'],
            'name'=>['required','alpha','min:3','max:50'],
            'title'=>['required','string','min:2','max:150'],
            'type'=>['required','string'],
        ];
        $this->request->validate($rules);
        $data=$this->request->all();
        if(!$this->modelData){
            $this->modelData=new Role;
            $this->modelData->instance_id=$data['instance_id'];
        }
        $this->modelData->name=$data['name'];
        $this->modelData->title=$data['title'];
        $this->modelData->type=$data['type'];
        if($data['type']=='course'){
            $this->modelData->is_admin=0;
        }else{
            $this->modelData->is_admin=isset($data['is_admin'])?$data['is_admin']:0;;
        }
        $this->modelData->default=isset($data['default'])?$data['default']:0;
        $result=$this->modelData->save();
        if($result){
            return $this->redirect(route('user.manage.role.index'))->setResponse(true,trans('user::message.action.success'));
        }
        return $this->redirectBack()->setResponse($result,trans('user::message.action.register_error'));
    }



}
