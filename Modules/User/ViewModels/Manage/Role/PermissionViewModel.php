<?php

namespace Modules\User\ViewModels\Manage\Role;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\User\Models\Role;
use Modules\User\Models\Role\RolePermission;
use Modules\User\Models\Permission;
use Nwidart\Modules\Facades\Module;

class PermissionViewModel extends BaseViewModel
{
    private $permissions=false;
    private $allPermissions=false;

    public function boot()
    {
        $id=$this->requestValuesUpdate()->get('id');
        if($id){
            $this->modelData=Role::find($id);
        }
    }

    public function getForm(){
        $this->boot();
        if(!$this->modelData){
            return $this->redirectBack()->setResponse(false,trans('user::message.action.not_find'));
        }
        $this->assets[] = ['container' => 'theme_js', 'src' => "assets/js/modules/user/role/permission.js", 'name' => 'user-permission'];
        $BlockViewModel=&$this;
        $this->setTitlePage(trans('manage_role_permissions_by_name',['name'=>$this->modelData->name]));
        return $this->renderedView('user::manage.role.permission',compact('BlockViewModel'));
    }


    public function save(){
        $this->boot();
        if(!$this->modelData){
            return $this->redirectBack()->setResponse(false,trans('user::message.action.not_find'));
        }
        $RolePermissions=RolePermission::where('role_id',$this->modelData->id)->first();
        if(!$RolePermissions){
            $RolePermissions=new RolePermission;
            $RolePermissions->role_id=$this->modelData->id;
        }
        $RolePermissions->permissions=$this->request->get('permissions');
        $result=$RolePermissions->save();
        if($result){
            return $this->redirect(route('user.manage.role.index'))->setResponse(true,trans('user::message.action.success'));
        }
        return $this->redirectBack()->setResponse($result,trans('user::message.action.error'));
    }


    public function hasPermission($permission_id,$module){
        if($this->permissions==false){
            $this->permissions=$this->getRolePermission();
        }
        if(isset($this->permissions->{$module}->{$permission_id})){
            return $this->permissions->{$module}->{$permission_id};
        }
        return false;
    }


    public function getRolePermission(){
      $RolePermission=RolePermission::where('role_id',$this->modelData->id)->first();
      if($RolePermission){
          return $RolePermission->permissions;
      }
      return [];
    }


    public function getAllPermissions(){
        if($this->allPermissions){
            return $this->allPermissions;
        }
        $all=Module::all();
        $this->allPermissions=[];
        foreach($all as $key=>$value){
            $allAccess=Permission::where('module',$key)->get();
            if($allAccess->count()){
                $this->allPermissions[$value->name]=$allAccess;
            }
        }
        return $this->allPermissions;
    }


}
