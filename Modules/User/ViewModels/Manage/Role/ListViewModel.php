<?php

namespace Modules\User\ViewModels\Manage\Role;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\User\Models\Role;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;


    public function setGridModel()
    {
        $this->Model =Role::with('instance');
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        $this->per_page=50;
        /*add columns to grid*/
        $this->addColumn('id', trans('user::form.fields.id'), true)
             ->addColumn('instance', trans('user::form.fields.instance'), false)
             ->addColumn('title', trans('user::form.fields.title'), false)
             ->addColumn('name', trans('user::form.fields.name'), true)
             ->addColumn('type', trans('user::form.fields.type'), true)
             ->addColumn('is_admin', trans('user::form.fields.is_admin'), true)
             ->addColumn('default', trans('user::form.fields.default'), true);

        $edit = array(
            'name' => 'user.manage.role.edit',
            'parameter' => ['id']
        );
        $this->addAction('edit', $edit, trans('user::routes.user.manage_role_edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $permission = array(
            'name' => 'user.manage.role.permission',
            'parameter' => ['id']
        );
        $this->addAction('permission', $permission, trans('user::routes.user.manage_role_permissions'), 'fa fa-bars', false, ['target' => '', 'class' => 'btn btn-sm btn-danger']);


        $this->action_text = false;

        /*buttons*/
        $routeAdd=route('user.manage.role.edit',['id'=>0]);
        $options=['class'=>"btn btn-primary",'target'=>"_blank"];
        $this->addButton('add',$routeAdd,trans('user::routes.user.manage_role_create'),'fa fa-plus',false,'before',$options);


        /*filters*/

        //$this->export=true;
        //$this->export_file_name='';
        return $this;
    }

    public function getRowsUpdate($row)
    {
        if(is_null($row->instance)){
            $row->instance=trans('user::form.fields.all');

        }else{
            $row->instance=$row->instance->name;
        }
        $row->default=$this->getComponent('yesno',['value'=>$row->default]);
        $row->is_admin=$this->getComponent('yesno',['value'=>$row->is_admin]);
        return $row;
    }

    public function getActionsUpdate($row)
    {
        $actions=$this->actions;
        if(is_null($row->instance) && !BridgeHelper::getAccess()->isSuperAdmin()){
           $actions=[];
        }
        return $actions;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getAllRoleList()
    {
        $this->setTitlePage(trans('user::menu.manage.roles'));
        $this->generateGridList()->renderedView("user::manage.role.list", ['view_model' => $this], trans('user::menu.manage.roles'));
        return $this;
    }
}
