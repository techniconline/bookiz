<?php
namespace Modules\User\Bridge\Access;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\Traits\ConfigThemeTrait;
use Modules\User\Models\Role\RolePermission;
use Modules\User\Models\User\UserRole;
use Modules\User\Models\Role;


/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class Access
{

    use ConfigThemeTrait;
    /**
     * @var string
     */
    private $operator='|';
    private $is_super_admin=null;

    public function canAccessRoute($route,$require=true){
        if($this->isSuperAdmin()){
            return true;
        }
        $permission=$this->getRoutePermission($route);
        if(is_array($permission) && isset($permission['access'])){
            if(isset($permission['type']) && !empty($permission['type'])){
                $this->setOperator($permission['type']);
            }
            return $this->canAccess($permission['access'],$require);
        }
        if($permission==true){
            return true;
        }
        if($require){
            return redirect()->route('core.error.role')->send();
        }
        return false;
    }
    /**
     * @param $permission
     * @param bool $require
     * @param bool $user
     * @return Access
     * @throws \Throwable
     */
    public function canAccess($permission, $require=false, $user=false){
        if(is_array($permission)){
            return $this->canAccessByGroups($permission, $require=false, $user=false);
        }
        $role=$this->getUserRole($user);
        if(!$role){
            return $this->getRequireAccess($require,false);
        }

        $access=$this->hasAccess($role,$permission);
        return $this->getRequireAccess($require,$access);
    }


    /**
     * @param $permissions
     * @param bool $require
     * @param bool $user
     * @return Access
     * @throws \Throwable
     */
    public function canAccessByGroups($permissions, $require=false, $user=false){
        $byOr=($this->operator==='|')?true:false;
        if($byOr){
            $result=false;
            foreach($permissions as $permission){
                $result=$this->canAccess($permission,false,$user);
                if($result){
                    return $this->getRequireAccess($require,$result);
                }
            }
            return $this->getRequireAccess($require,$result);
        }else{
            $result=true;
            foreach($permissions as $permission){
                $result=$result & $this->canAccess($permission,false,$user);
            }
            return $this->getRequireAccess($require,$result);
        }
    }


    /**
     * @param $role
     * @param $permission
     * @return bool
     * @throws \Exception
     */
    public function hasAccess($role, $permission){
        $role_permissions=RolePermission::getAllPermissionsByRole($role->role_id);
        if(is_string($permission)){
            $permission=$role_permissions->where('permission',$permission)->first();
        }else{
            $permission=$role_permissions->where('permission_id',$permission)->first();
        }
        if($permission){
            return true;
        }
        return false;
    }

    public function canAccessAdminInstance($user=false){
        if($this->isSuperAdmin() || $this->isAdmin()){
            return true;
        }
        return false;
    }

    /**
     * @param $require
     * @param $access
     * @return $this
     * @throws \Throwable
     */
    public function getRequireAccess($require, $access){
        if($require && !$access){
            if(Request::ajax()){
                $responce=['error'=>1,'html'=>''];
                $responce['html']=view('layouts.error',['error'=>trans('error.can_not_access')])->render();
                return response()->json($responce, 200)->send();
            }else{
                return redirect()->route('core.error.access')->send();
            }

        }
        return $access;
    }


    /**
     * @param $user
     * @return bool
     */
    public function getUserRole($user=false){
        if($user===false){
            $user=Auth::user();
            if(!$user){
                return false;
            }
        }
        return  $user->getCurrentRole();
    }

    /**
     * @param $operator
     * @return $this
     */
    public function setOperator($operator){
        $this->operator=$operator;
        return $this;
    }


    /**
     * @param bool $require
     * @param bool $user
     * @return Access
     * @throws \Throwable
     */
    public function isAdmin($require=false, $user=false){
        return $this->canAccess('admin',$require,$user);
    }

    /**
     * @param bool $user
     * @return bool
     */
    public function isOwnerEntity($user){
        $model = BridgeHelper::getEntity()->getEntityOwnerModel();
        if (!$user){
            return false;
        }
        $row = $model->active()->where('user_id', $user->id)->first();

        if ($row){
            return true;
        }
        return false;

    }

    /**
     * @param bool $require
     * @param bool $user
     * @return Access
     * @throws \Throwable
     */
    public function isSuperAdmin($require=false, $user=false){
        if(is_null($this->is_super_admin)){
            $this->is_super_admin=$this->canAccess('core/is.super.admin',$require,$user);
        }
        return $this->is_super_admin;
    }

    /**
     * @param bool $user
     * @return bool
     */
    public function hasMultiInstance($user=false){
        if($user===false){
            $user=$this->getUser();
        }
        if(!$user){
            return false;
        }
        $roles=UserRole::getInstanceFromUserRoles($user);
        if($roles->count()>1){
           return $roles;
        }
        return false;
    }


    /**
     * @param bool $user
     * @return bool
     */
    public function hasMultiRole($user=false){
        if($user===false){
            $user=$this->getCurrentUser();
        }
        if(!$user){
            return false;
        }
        $roles=UserRole::getRolesByUser($user);
        if($roles->count()>1){
            return true;
        }
        return false;
    }


    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function getUser(){
        if($this->isLogin()){
            return Auth::user();
        }
        return null;
    }

    public function isLogin(){
        return Auth::check();
    }

    public function loginById($user_id)
    {
        return auth()->loginUsingId($user_id);
    }


    public function hasAccessModelOnInstanceAndLanguage($object,$check_instance=true,$check_language=true){
        if(!$object){
            return true;
        }
        if($check_language){
            $language_id=app('getInstanceObject')->getLanguage()->id;
            if($object->language_id!=$language_id){
                return $this->getRequireAccess(true,false);
            }
        }
        if($check_instance){
            $instance_id=app('getInstanceObject')->getCurrentInstance()->id;
            if($object->instance_id!=$instance_id){
                return $this->getRequireAccess(true,false);
            }
        }
        return true;
    }



    public function getRoles($name='site',$instance_id=null,$forSelect=false){
        if(is_null($instance_id)){
            $instance_id=app('getInstanceObject')->getCurrentInstance()->id;
        }
        $roles=Role::getInstanceRoles($name,$instance_id);
        if(!$forSelect){
            return $roles;
        }
        $lists=[];
        foreach ($roles as $role){
            $lists[$role->id]=$role->title;
        }
        return $lists;
    }

    public function getAdminRoles($name='site',$instance_id=null,$forSelect=false){
        if(is_null($instance_id)){
            $instance_id=app('getInstanceObject')->getCurrentInstance()->id;
        }
        $roles=Role::getInstanceAdminRoles($name,$instance_id);
        if(!$forSelect){
            return $roles;
        }
        $lists=[];
        foreach ($roles as $role){
            $lists[$role->id]=$role->title;
        }
        return $lists;
    }

}