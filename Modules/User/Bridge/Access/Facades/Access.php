<?php
namespace Modules\User\Bridge\Access\Facades;

use Illuminate\Support\Facades\Facade;

class Access extends Facade
{

    /**
     * get sms service
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Access'; }

}