<?php
namespace Modules\User\Bridge\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Modules\Core\ViewModels\Traits\ConfigThemeTrait;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\User\Models\Role\RolePermission;
use Modules\User\Models\User\UserRole;
use Modules\User\Models\Role;
use Modules\User\ViewModels\Manage\User\CreateViewModel;
use Modules\User\ViewModels\Manage\User\EditViewModel;


/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class User
{

    /**
     * @return string
     */
    public function getUrlSearchUser()
    {
        return route("user.manage.search_user");
    }


    /**
     * @return \Modules\User\Models\User
     */
    public function getUserModel()
    {
        return new \Modules\User\Models\User();
    }

    /**
     * @return string
     */
    public function getUserClass()
    {
        return \Modules\User\Models\User::class;
    }

    /**
     * @return UserRegisterKey
     */
    public function getUserRegisterKeyModel()
    {
        return new UserRegisterKey();
    }

    /**
     * @return CreateViewModel
     */
    public function getCreateViewModel()
    {
        return new CreateViewModel();
    }

    /**
     * @return EditViewModel
     */
    public function getEditViewModel()
    {
        return new EditViewModel();
    }

}