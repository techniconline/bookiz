<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 7/21/18
 * Time: 1:19 PM
 */

namespace Modules\User\Libraries\Connector\Master;

use Illuminate\Http\Request;

abstract class Connector
{

    const CONNECTOR_DEACTIVE='deactive';
    const CONNECTOR_ACTIVE='active';
    const CONNECTOR_WAIT='wait';

    private $info;
    private $def_length=6;

    abstract public function useThisConnector(Request $request);
    abstract public function getRule();
    abstract public function getMessage();

    public function setInfo($info){
        $this->info=$info;
        return $this;
    }

    public function getToken($length=null){
        if(is_null($length)){
            $length=$this->__get('token_length');
            if(!$length){
                $length=$this->def_length;
            }
        }
        $result='';
        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }
        return $result;
    }


    public function getHash($value1=null,$value2=null){
        return sha1($value1.rand(1000,100000).time().rand(1000,100000).$value2);
    }

    public function __get($name) {
        $name=strtolower($name);
        if(isset($this->info->{$name})){
            return $this->info->{$name};
        }
        return null;
    }

    public function __call($method, $parameters)
    {
     if(method_exists($this,$method)){
         return $this->{$method}($parameters);
     }elseif(strpos($method,'get')!==false){
         $method=str_replace('get','',$method);
         return $this->__get($method);
     }
    }

}