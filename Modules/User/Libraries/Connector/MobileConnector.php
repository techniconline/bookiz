<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 7/21/18
 * Time: 1:18 PM
 */

namespace Modules\User\Libraries\Connector;

use Modules\User\Libraries\Connector\Master\Connector;
use Illuminate\Http\Request;

class MobileConnector extends Connector
{

    public function useThisConnector(Request $request){
        if(preg_match('/^[0-9]+$/',$request->get('connector'))){
            $mobile = $request->input('connector');
            $request->merge(['connector'=>ltrim($mobile,'0')]);
            return true;
        }
        return false;
    }

    public function getRule(){
        return ['regex:/^([1-9]){3}+([0-9]){9}$/'];
    }

    public function getRuleRegister(){
//        return ['regex:/^(09|9)+([0-9]){9}$/'];
        //            'mobile' => 'required|regex:/(09)[0-9]{9}/|unique:users|size:11',

        return ['required','regex:/^([1-9]){3}+([0-9]){9}$/','unique:user_register_key,value'];
    }

    public function getMessage(){
        return ['regex'=>trans('user::validation.fields.mobile'),'unique'=>trans('user::validation.fields.duplicate')];
    }

    public function getName(){
        return 'mobile';
    }




}