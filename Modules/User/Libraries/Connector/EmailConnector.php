<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 7/21/18
 * Time: 1:18 PM
 */

namespace Modules\User\Libraries\Connector;

use Modules\User\Libraries\Connector\Master\Connector;
use Illuminate\Http\Request;


class EmailConnector extends Connector
{

    public function useThisConnector(Request $request){
        if(preg_match('/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/',$request->get('connector'))){
            return true;
        }
        return false;
    }


    public function getRule(){
        return ['email'];

    }

    public function getRuleRegister(){
        return ['required','email','unique:user_register_key,value'];

    }

    public function getMessage(){
        return ['email'=>trans('user::validation.fields.email')];
    }
    public function getName(){
        return 'email';
    }

}