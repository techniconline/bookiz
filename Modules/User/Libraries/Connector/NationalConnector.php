<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 7/21/18
 * Time: 1:18 PM
 */

namespace Modules\User\Libraries\Connector;

use Modules\User\Libraries\Connector\Master\Connector;
use Illuminate\Http\Request;

class NationalConnector extends Connector
{
    public function useThisConnector(Request $request)
    {
        if ($this->isValidIranianNationalCode($request->get('connector'))) {
            $mobile = $request->input('connector');
            $request->merge(['connector' => ltrim($mobile, '0')]);
            return true;
        }
        return false;
    }

    public function getRule()
    {
        return ['regex:/^[0-9]{10}$/'];
    }

    public function getMessage()
    {
        return ['regex' => trans('user::validation.fields.national')];
    }

    public function getName()
    {
        return 'national';
    }


    public function isValidIranianNationalCode($input)
    {
        # check if input has 10 digits that all of them are not equal
        if (!preg_match("/^\d{10}$/", $input)) {
            return false;
        }

        $check = (int)$input[9];
        $sum = array_sum(array_map(function ($x) use ($input) {
                return ((int)$input[$x]) * (10 - $x);
            }, range(0, 8))) % 11;

        return ($sum < 2 && $check == $sum) || ($sum >= 2 && $check + $sum == 11);
    }
}