<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 7/21/18
 * Time: 1:18 PM
 */

namespace Modules\User\Libraries\Connector;

use Modules\User\Libraries\Connector\Master\Connector;
use Illuminate\Http\Request;

class MeduConnector extends Connector
{
    public function useThisConnector(Request $request){
        if(preg_match('/^[0-9]+$/',$request->get('connector'))){
            $mobile = $request->input('connector');
            $request->merge(['connector'=>ltrim($mobile,'0')]);
            return true;
        }
        return false;
    }

    public function getRule(){
        return ['regex:/^[0-9]+$/'];
    }

    public function getMessage(){
        return ['regex'=>trans('user::validation.fields.medu')];
    }

    public function getName(){
        return 'medu';
    }

}