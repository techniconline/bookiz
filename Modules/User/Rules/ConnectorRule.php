<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 7/21/18
 * Time: 3:45 PM
 */
namespace Modules\User\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\User\Models\Register\UserRegisterKey;


class ConnectorRule implements Rule
{
    private $connectorId=false;

    public function __construct($connectorId)
    {
        $this->connectorId=$connectorId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $connectorId=$this->connectorId;
        $user=UserRegisterKey::where('connector_id',$connectorId)->where('value',$value)->first();
        if($user){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('user::validation.form.user_exists');
    }

}