<div class="col-xl-12 col-lg-12 col-md-12  floatright   dash6  ">
    <div class="dash7">
        <h2>
            @lang('user::routes.user.manage_change_password',['full_name'=>$BlockViewModel->getUserData('full_name')])
        </h2>
    </div>
    <div class="dash9">
        {!! FormHelper::open(['role'=>'form','url'=>route('user.profile.password.save'),'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

        @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

        {!! FormHelper::input('password','current_password',null,['required'=>'required','title'=>trans('user::form.fields.current_password'),'helper'=>trans('user::form.fields.current_password')]) !!}

        {!! FormHelper::input('password','password',null,['autocomplete'=>"off",'required'=>'required','title'=>trans('user::form.fields.password')]) !!}

        {!! FormHelper::input('password','password_confirmation',null,['autocomplete'=>"off",'required'=>'required','title'=>trans('user::form.fields.password_confirmation')]) !!}

        {!! FormHelper::openAction() !!}
        {!! FormHelper::submitOnly() !!}
        {!! FormHelper::closeAction() !!}
        {!! FormHelper::close() !!}
    </div>
</div>
