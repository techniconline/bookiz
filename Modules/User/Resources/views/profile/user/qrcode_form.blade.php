<div class="col-xl-12 col-lg-12 col-md-12  floatright   dash6  ">
    <div class="dash7">
        <h2>
            @lang('user::routes.user.create_qr_code_login',['full_name'=>$BlockViewModel->getUserData('full_name')])
        </h2>
    </div>
    <div class="dash9">

        {!!QrCode::size(300)->generate($hash_code)!!}

    </div>
    <div class="dash7">
        @lang('user::routes.user.available_time_qr_code_login'): {!! $date_time_available !!}
    </div>

    <div class="dash7">
        @lang('user::routes.user.now_time_qr_code_login'): {!! $date_time_now !!}
    </div>
</div>
