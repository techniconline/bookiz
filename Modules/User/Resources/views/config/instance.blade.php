<div class="col-md-12" id="user.instance.config">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cog"></i>
                {{ $BlockViewModel->getTitlePage() }}
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('user.config.instance.save'),'method'=>'POST','class'=>'form-horizontal','id'=>'config_form']) !!}
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button> @lang('user::validation.form.error')
            </div>
            {!! FormHelper::legend(trans('user::menu.manage.user_config')) !!}
            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp
            {!! FormHelper::selectTag('connector[]',$BlockViewModel->getConnectorList(),old('connector',$BlockViewModel->getSetting('connector')),['title'=>trans('user::form.fields.connector'),'helper'=>trans('user::form.helper.connector'),'multiple'=>'multiple','required'=>'required']) !!}
            {!! FormHelper::checkbox('usernamehide',1,old('usernamehide',$BlockViewModel->getSetting('usernamehide')),['title'=>trans('user::form.fields.usernamehide'),'label'=>trans('user::form.helper.usernamehide')]) !!}
            {!! FormHelper::checkbox('register',1,old('register',$BlockViewModel->getSetting('register')),['title'=>trans('user::form.fields.register_show'),'label'=>trans('user::form.helper.register_show')]) !!}
            {!! FormHelper::select('verify_type',['singup'=>trans('user::form.helper.verify_type_singup'),'month'=>trans("user::form.helper.verify_type_month")],old('verify_type',$BlockViewModel->getSetting('verify_type')),['title'=>trans('user::form.fields.verify_type'),'required'=>'required']) !!}
            {!! FormHelper::checkbox('complete_active',1,old('complete_active',$BlockViewModel->getSetting('complete_active')),['title'=>trans('user::form.fields.complete_active'),'label'=>trans('user::form.helper.complete_active')]) !!}
            {!! FormHelper::checkbox('complete_required',1,old('complete_required',$BlockViewModel->getSetting('complete_required')),['title'=>trans('user::form.fields.complete_required'),'label'=>trans('user::form.helper.complete_required')]) !!}
            {!! FormHelper::selectTag('complete_fields[]',$BlockViewModel->getFieldsList(),old('complete_fields',$BlockViewModel->getSetting('complete_fields')),['title'=>trans('user::form.fields.complete_fields'),'helper'=>trans('user::form.helper.complete_fields'),'multiple'=>'multiple']) !!}
            {!! FormHelper::selectTag('complete_required_fields[]',$BlockViewModel->getFieldsList(),old('complete_required_fields',$BlockViewModel->getSetting('complete_required_fields')),['title'=>trans('user::form.fields.complete_required_fields'),'helper'=>trans('user::form.helper.complete_required_fields'),'multiple'=>'multiple']) !!}
            {!! FormHelper::select('verify_type',['singup'=>trans('user::form.helper.verify_type_singup'),'month'=>trans("user::form.helper.verify_type_month")],old('verify_type',$BlockViewModel->getSetting('verify_type')),['title'=>trans('user::form.fields.verify_type'),'required'=>'required']) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitOnly() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>