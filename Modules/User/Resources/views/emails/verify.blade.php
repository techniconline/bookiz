@component('mail::message',['subject'=>$subject,'token'=>$token,'url'=>$url])

    <b><p class="meeting">@lang('user::message.email.hello_user',['first_name'=>$user->full_name])</p></b>
    <div class="token_box">
        <p class="token_title">@lang('user::message.email.verify_token')</p>
        <div class="token_numbers">
            {{ $token }}
        </div>
        <hr>
        <div class="token_button">
            <a href="{{$url}}" class="btn">
                @lang('user::message.email.verify_user_account')
            </a>
        </div>
    </div>
@endcomponent