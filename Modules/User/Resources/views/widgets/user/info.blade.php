﻿{!! $blockViewModel->getConfig('before_html') !!}

<div class=" col-md-6 col-lg-6 col-xl-6 col-12 userbox floatright">


    <div class="top-menu">


        <div class="dropdown">


            <div class="dropdown">
                <button class="dropbtn">
                    <img alt="{{ $user->full_name }}" class="img-circle userPopimg"
                         src="{!! $user->small_avatar_url !!}"/>
                    <span class="span_name_login_user">  {{ $user->full_name }} </span>
                    <i class="fas fa-sort-down"></i></button>
                <div class="dropdown-content">
                    <a href="{!! route("course.user.my.courses") !!}"> <i
                                class="fas fa-window-restore"></i>@lang("course::course.my_courses")</a>
                    <a href="{!! route("sale.order.user") !!}"> <i
                                class="fas fa-shopping-cart"></i> @lang("sale::sale.my_orders") </a>
                    <a href="{!! route("user.profile.edit") !!}"><i class="fa fa-user-edit"></i>@lang("user::form.titles.profile")</a>

                    <a href="{!! route("user.profile.qrcode_login") !!}"><i class="fa fa-qrcode"></i>@lang("user::form.titles.qrcode_login")</a>

                    <a href="{!! route("user.profile.password.form") !!}"><i
                                class="fa fa-lock"></i>@lang("user::form.titles.change_password")</a>
                    <a href="{!! route('logout') !!}"
                       onclick="event.preventDefault(); document.getElementById('logout-form-menu').submit();"><i
                                class="fas fa-sign-out-alt"></i> {{ trans('user::form.titles.logout') }}</a>
                    <form id="logout-form-menu" action="{!! route('logout') !!}" method="POST"
                          style="display: none;">
                        {!! csrf_field() !!}
                    </form>
                </div>
            </div>


        </div>

    </div>
</div>
<form id="logout-form" action="{!! route('logout') !!}" method="POST"
      style="display: none;">
    {!! Form::token() !!}
</form>
<ul class="nav navbar-nav pull-right">
    <!-- BEGIN USER LOGIN DROPDOWN -->
    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->


    <!-- END USER LOGIN DROPDOWN -->
    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
    <li class="dropdown dropdown-quick-sidebar-toggler Nodisp">
        <a href="{!! route('logout') !!}"
           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
           class="dropdown-toggle">
            <i class="icon-logout"></i>
        </a>
    </li>
    <li>

    </li>
    <!-- END QUICK SIDEBAR TOGGLER -->
</ul>


<div class=" col-md-6 col-lg-6 col-xl-6 col-12 userbox floatright phone_ _phone2">
    <div class="_phone4">
        <i class="fas fa-phone fa-flip-horizontal"></i>
        <span class="_user">
						  <span>
                    با ما تماس بگیرید
                   </span>  <br/>
                  
                        021-88176609

                   
					   </span>
    </div>
</div>

				  