{!! FormHelper::input('text','title',old('title',$blockViewModel->getData('title')),['title'=>trans('core::form.fields.title'),'helper'=>trans('core::form.helper.title')]) !!}
{!! FormHelper::checkbox('show_login_link',1,old('show_login_link',$blockViewModel->getData('show_login_link')),['title'=>trans('user::form.fields.show_login_link')]) !!}
{!! FormHelper::checkbox('show_register_link',1,old('show_register_link',$blockViewModel->getData('show_register_link')),['title'=>trans('user::form.fields.show_register_link')]) !!}
