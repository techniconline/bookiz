@foreach($userConnectors as $userConnector)
    <span class="label label-{!! ($userConnector->status=='active')?'success':'default' !!}">
        @lang('user::form.fields.'.$userConnector->connector->name) :
        <b>{{ $userConnector->value }}</b>
        <span> | @lang('user::values.'.$userConnector->status)</span>
    </span>
@endforeach