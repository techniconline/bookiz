@if($status=='active')
    <span class="text-success">@lang('user::data.connector_status.'.$status)</span>
@elseif($status=='deactive')
    <span class="text-danger">@lang('user::data.connector_status.'.$status)</span>
@else
    <span class="text-danger">@lang('user::data.connector_status.'.$status)</span>
@endif