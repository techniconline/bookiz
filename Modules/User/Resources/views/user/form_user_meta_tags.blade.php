<div class="row" id="form_feature">
    <div class="col-md-12">
        <a href="{!! route('user.manage.index') !!}" class="btn btn-lg green circle-right margin-bottom-5"><i
                    class="fa fa-list"></i> @lang("user::user.list") </a>
        <div class="portlet box blue " style="display: none">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-video-camera"></i> <i class="fa fa-tags"></i> @lang("user::routes.user.edit_meta_tags")
                    : {!! $user->full_name or null  !!}
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">


                <div class="form-body">
                    <div class="form-group">

                        @if(isset($metaGroups)&&$metaGroups)
                            <div class="row">

                                <div class="col-md-12">
                                    @foreach($metaGroups as $metaGroup)

                                        <div class="col-md-3">
                                            @php
                                              $checked = boolval(in_array($metaGroup->id,$metaGroupsActive));
                                            @endphp
                                            {!! FormHelper::checkbox('meta_group_ids[]',$metaGroup->id,$checked
                                            ,['title'=>$metaGroup->title,'disabled'=>'disabled']) !!}

                                        </div>

                                    @endforeach

                                </div>

                            </div>

                        @endif

                    </div>
                </div>

            </div>


        </div>

        {!! \Modules\Core\Providers\Helpers\Form\Facades\FormHelper::legend($user->full_name) !!}

        @if(isset($user) && $user && !empty($metaGroupsActive))

            @foreach($userMetaGroups as $userMetaGroup)

                <div class="portlet box blue-dark" style="margin: 5px">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-tags"></i> {!! $userMetaGroup->group_name !!}
                        </div>
                        <div class="tools">
                            <a href="" class="expand"> </a>
                            {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}

                            {{--<a href="{!! route('user.meta.copy',['user_id'=>$user->id, 'user_meta_group_id'=>$userMetaGroup->id]) !!}"--}}
                               {{--class="_copy_user_meta_group"--}}
                               {{--title="@lang("user::routes.user.copy_meta_group"): {!! $userMetaGroup->group_name !!}"--}}
                               {{--data-confirm-message="@lang("user::user.copy_meta_group"): {!! $userMetaGroup->group_name !!} ?">--}}
                                {{--<i class="fa fa-copy" style="color: white; font-size: 24px"></i> </a>--}}

                            {{--<a href="{!! route('user.meta.delete',['user_id'=>$user->id, 'user_meta_group_id'=>$userMetaGroup->id]) !!}"--}}
                               {{--class="_del_user_meta_group"--}}
                               {{--title="@lang("user::user.del_meta_group"): {!! $userMetaGroup->group_name !!}"--}}
                               {{--data-confirm-message="@lang("user::user.del_meta_group"): {!! $userMetaGroup->group_name !!} ?">--}}
                                {{--<i class="fa fa-remove" style="color: orangered; font-size: 24px"></i> </a>--}}
                        </div>
                    </div>

                    <div class="portlet-body form" style="display: none;">

                    {!! FormHelper::open(['role'=>'form','url'=>route('user.meta.features.value.save', ['user_id'=>$user->id, 'user_meta_group_id'=>$userMetaGroup->id])
                                ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                    @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                            @foreach($userMetaGroup->metaGroup->featureGroups as $featureGroup)

                                @foreach($featureGroup->featureGroupRelations as $featureGroupRelation)

                                    @if($featureGroupRelation->feature)

                                        <?php

                                        $activeValue = null;
                                        if ($metaFeatureDataList && isset($metaFeatureDataList[$userMetaGroup->id][$featureGroupRelation->feature->id])) {
                                            $activeValue = $metaFeatureDataList[$userMetaGroup->id][$featureGroupRelation->feature->id];
                                        }

                                        ?>

                                        {!!
                                            $view_model->setFeatureGroup($featureGroup)
                                            ->setFeatureModel($featureGroupRelation->feature)
                                            ->setActiveValueFeature($activeValue)
                                            ->getHtml()
                                        !!}

                                    @endif

                                @endforeach

                            @endforeach


                        {!! FormHelper::openAction() !!}
                        {!! FormHelper::submitByCancel(['title'=>trans("user::user.submit"),'class'=>'btn btn green _save_meta'], ['title'=>trans("user::user.cancel"), 'url'=>'#']) !!}
                        {!! FormHelper::closeAction() !!}
                        {!! FormHelper::close() !!}


                    </div>
                </div>

            @endforeach

            <hr>

        @endif

    </div>

</div>

<div>

</div>
