﻿﻿
<div class="modal-header      ">
    <br/><br/>
    <form id="signup" action="{{ route('login') }}" class="form-signup" method="post">
        {{ csrf_field() }}
        {!! FormHelper::input('hidden','referer',old('referer',$viewModel->getReferer())) !!}

        <h4 class="modal-title">@lang('user::form.helper.login')</h4>
        <br/><br/>
        <img src="/themes/default/assets/images/logo.png" class="Login_logo visible-xs"/>
            <label for="connector" class="col-md-4 control-label DisplayNone "
                   required>{{ $viewModel->getConnectorLabel() }} </label>


            <div class="modal-body">
                <div class="form-group{{ $errors->has('connector') ? ' has-error' : '' }}">
                <p><input id="Text3" type="text" class="form-control txtSize13" id="connector" name="connector"
                          value="{{ old('connector') }}"
                          placeholder="{{ $viewModel->getConnectorLabel() }}" required/>
                    @if ($errors->has('connector'))
                        <span class="help-block">
                            <strong>{{ $errors->first('connector') }}</strong>
                        </span>
                    @endif
                </p>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <p>
                    <label for="password" class="col-md-4 control-label DisplayNone"
                           required>@lang('user::form.fields.password') </label>

                    <input class="form-control txtSize13" type="password" id="inputPassword" name="password"
                           class="form-control txtSize13" placeholder="@lang('user::form.fields.password')" required>
                </p>
                </div>

                <input type="checkbox" id="remember" name="remember" class="pull-right mrg_left_10 "
                       value="1" {!! old('remember') ? 'checked="checked"':'' !!}>
                <label for="remember"
                       class="control-label pull-right   txtSize13">@lang('user::form.fields.remember') </label>


                </p>

                <button class="form-control btn btn-success" type="submit">@lang('user::form.fields.login')</button>


                <span class="_linkup">

					   
                          <a class="forgot" href="{{ route('forgot') }}"> @lang('user::form.titles.forgotToken') </a>
                    @if(!BridgeHelper::getConfig()->getSettings('register_hide','instance','core'))
                        <br/>
                        <a href="{{ route('register') }}">@lang('user::form.titles.register')  </a>
                    @endif
                    </span>

            </div>
    </form>

</div>
