<div class="modal-header   mrg_top_30Precent   ">
    <form id="signup" action="{{ route('register') }}" class="form-signup " method="post">
        {{ csrf_field() }}
        {!! FormHelper::input('hidden','referer',old('referer',$viewModel->getReferer())) !!}

        <h4 class="modal-title">@lang('user::form.helper.register')</h4>
        <br/><br/>
        <img src="../themes/default/assets/images/logo.png" class="Login_logo visible-xs"/>

        <div class=" {{ $errors->has('first_name') ? ' has-error' : '' }}">
        <!-- <label for="first_name" class="col-md-4 control-label" required>@lang('user::form.fields.first_name')</label> -->

            <div class="col-md-12 mrg_btn_10">
                <input type="text" id="first_name" name="first_name" value="{{ old('first_name') }}"
                       class="form-control txtSize13" placeholder="@lang('user::form.fields.first_name')" required
                       autofocus>
                @if ($errors->has('first_name'))
                    <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
        <!--    <label for="last_name" class="col-md-4 control-label" required>@lang('user::form.fields.last_name') </label> -->

            <div class="col-md-12 mrg_btn_10">
                <input type="text" id="last_name" name="last_name" value="{{ old('last_name') }}"
                       class="form-control txtSize13" placeholder="@lang('user::form.fields.last_name')" required>

                @if ($errors->has('last_name'))
                    <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('connector') ? ' has-error' : '' }}">
        <!-- <label for="connector" class="col-md-4 control-label" required>{{ $viewModel->getConnectorLabel() }} </label> -->

            <div class="col-md-12 mrg_btn_10">
                <input type="text" id="connector" name="connector" value="{{ old('connector') }}"
                       class="form-control txtSize13" placeholder="{{ $viewModel->getConnectorLabel() }}" required>
                @if ($errors->has('connector'))
                    <span class="help-block">
                            <strong>{{ $errors->first('connector') }}</strong>
                        </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <!--  <label for="password" class="col-md-4 control-label" required>@lang('user::form.fields.password') </label> -->

            <div class="col-md-12 mrg_btn_10">
                <input type="password" id="inputPassword" name="password" class="form-control txtSize13"
                       placeholder="@lang('user::form.fields.password')" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif
            </div>
        </div>
        <div class="col-md-12 mrg_btn_10">
            <button class="form-control btn btn-success" type="submit">@lang('user::form.fields.signup')</button>


            <span class="_linkup">


                          <a class="forgot" href="{{ route('forgot') }}"> @lang('user::form.titles.forgotToken') </a>
                         <br/>
                        <a href="{{ route('login') }}">@lang('user::form.titles.login')  </a>
                    </span>


        </div>
    </form>
</div>