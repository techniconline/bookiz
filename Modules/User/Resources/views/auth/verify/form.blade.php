﻿﻿ 
          <!--   <img id="profile-img" class="profile-img-card" src="{!! asset('themes/default/assets/img/avatar.png') !!}" />  
        <p id="profile-name" class="profile-name-card"></p>-->

		   <div class="modal-header txtCener mrg_top_90   ">

        <form id="signup" action="{{ route('user.verify') }}" class="form-verify" method="post">
            {{ csrf_field() }}
           


			  <h4 class="modal-title">@lang('user::form.helper.confirm')</h4>
			<br /><br />


            @include('core::component.error')


            <div class="form-group ">
                  <!--  <label class="col-md-6 control-label fltr mrg_top_10">{{ $viewModel->getConnectorLabel() }}</label> -->

                <div class="col-md-6  fltr mrg_btn_10">
                    <input type="text" value="{{ $connector->value }}" class="form-control txtSize13" readonly>
                </div>
				 <div class="col-md-6 fltr">
                    <input type="text" id="token" name="token" value="{{ old('token') }}"   class="form-control txtSize13" placeholder="@lang('user::form.fields.token')" required>

                    @if ($errors->has('token'))
                        <span class="help-block">
                            <strong>{{ $errors->first('token') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('token') ? ' has-error' : '' }}">
             <!--   <label for="token" class="col-md-4 control-label" required>@lang('user::form.helper.token') </label> -->

               
            </div>
		
	   <div class="row4 ">

	   <button class="form-control btn btn-success mrg_top_10" type="submit">@lang('user::form.fields.SubmitVerfyCode')</button>
	   </div>

	   	<div class="   mrg_top_10   " >
		    <div class="col-md-6      ">
               
			  <button id="some_div" type="button" class="form-control btn mysome_div" onclick="return clickit();" >   </button>


			</div>
			     <div class="col-md-6     ">
                   <a class="form-control  btn btn-info       " href="{{ route('user.verify.change')  }}">@lang('user::form.fields.change',['text'=>$viewModel->getConnectorLabel()]) </a>
			     </div>
            
       </div>

	   </form>


	   <script>
var timeLeft = 2;
var elem = document.getElementById('some_div');
var timerId = setInterval(countdown, 1000);
function clickit(){
 window.location = "{{ route('user.verify.resend')  }}";
}
function countdown() {
    if (timeLeft == -1) 
	{
         clearTimeout(timerId);
		 EnableButtn();
    } else {
          elem.innerHTML = timeLeft + ' ثانیه تا مکان ارسال مجدد';
 		  elem.disabled = true;
		   timeLeft--;
    }
}

function EnableButtn() {
     elem.disabled = false;
	 elem.innerHTML ="ارسال مجدد کد";
     elem.classList.add("btn-success");
     elem.classList.remove("mysome_div");
	 } 
	
	
	</script>
   </div>