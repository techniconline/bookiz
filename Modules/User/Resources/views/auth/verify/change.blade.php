
        <img id="profile-img" class="profile-img-card" src="{!! asset('themes/default/assets/img/avatar.png') !!}" />
        <p id="profile-name" class="profile-name-card"></p>
        <form id="change" action="{{ route('user.verify.change') }}" class="form-verify" method="post">
            {{ csrf_field() }}
            <h1>@lang('user::form.fields.change',['text'=>$viewModel->getConnectorLabel()])</h1>
            <div class="form-group{{ $errors->has('connector') ? ' has-error' : '' }}">
                <label for="connector" class="col-md-4 control-label" required>@lang('user::form.fields.new',['text'=>$viewModel->getConnectorLabel()]) </label>
                <div class="col-md-6">
                    <input type="text" id="connector" name="connector" value="{{ old('connector',$connector->value) }}" class="form-control" placeholder="{{ $viewModel->getConnectorLabel() }}" required>
                    @if ($errors->has('connector'))
                        <span class="help-block">
                            <strong>{{ $errors->first('connector') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <a href="{{ route('user.verify') }}">@lang('user::form.fields.back_to_verify') </a>
            <button class="btn btn-lg btn-primary btn-block btn-signup" type="submit">@lang('user::form.fields.signup')</button>
        </form>
