<div class="container">
    <div class="card card-container">
        <p id="profile-name" class="profile-name-card"></p>
        <form id="change" action="{{ route('forgot.reset',['id'=>$viewModel->getResetId()]) }}" class="form-verify" method="post">
            {{ csrf_field() }}
            <h1>@lang('user::form.titles.reset_password')</h1>
            <input type="hidden" name="hash" value="{{ old('hash',$viewModel->getPasswordResetHash()) }}">
            @if(!$viewModel->getPasswordResetHash())
                <div class="form-group{{ $errors->has('token') ? ' has-error' : '' }}">
                    <label for="token" class="col-md-4 control-label">@lang('user::form.fields.token')</label>

                    <div class="col-md-6">
                        <input id="token" type="token" class="form-control" name="token" required>

                        @if ($errors->has('token'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('token') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
            @endif
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">@lang('user::form.fields.password')</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label for="password-confirm" class="col-md-4 control-label">@lang('user::form.fields.password_confirmation')</label>
                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <button class="btn btn-lg btn-primary btn-block btn-signup" type="submit">@lang('user::form.fields.password_change')</button>
        </form>
    </div><!-- /card-container -->

</div><!-- /container -