  <div class=" mrg_top_30Precent"> 
      <!--  <img id="profile-img" class="profile-img-card" src="{!! asset('themes/default/assets/img/avatar.png') !!}" /> 
        <p id="profile-name" class="profile-name-card"></p>-->
        <form id="change" action="{{ route('forgot') }}" class="form-verify" method="post">
            {{ csrf_field() }}
             <h4 class="modal-title mrg_btn_10" >@lang('user::form.titles.forgotToken')</h4>	<br /><br />
            <div class="form-group ">
                  <!-- <label for="connector" class="col-md-4 control-label" required>{{ $viewModel->getConnectorLabel() }}</label> -->
                <div class="col-md-12 mrg_btn_10 pad_btn_10">
                    <input type="text" id="connector" name="connector" value="{{ old('connector') }}" class="form-control txtSize13" placeholder="{{ $viewModel->getConnectorLabel() }}" required>
                    @if ($errors->has('connector'))
                        <span class="help-block">
                            <strong>{{ $errors->first('connector') }}</strong>
                        </span>
                    @endif
                

            

			</div>
            </div>
			  <div class="col-md-12 mrg_btn_10">
			  
			 
			<button class="form-control btn btn-success" type="submit">@lang('user::form.fields.forgot')</button>
			
			
			
				 <span class="_linkup">


                     @if(!BridgeHelper::getConfig()->getSettings('register_hide','instance','core'))
                         <a class="forgot" href="{{ route('register') }}"> @lang('user::form.titles.register') </a>
                     @endif
                     <br />
                     <a  href="{{ route('login') }}">@lang('user::form.titles.login')  </a>
                    </span>
                      
					  
					  
					  </div>
        </form>
    </div>