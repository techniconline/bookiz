<div class="col-md-12">
    @php $userConnector= $BlockViewModel->getUserData() @endphp
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i> @lang('user::routes.user.manage_edit_connector',['full_name'=>$userConnector->user->full_name])
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('user.manage.connector.edit',['id'=>$BlockViewModel->getUserData('id')]),'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            {!! FormHelper::select('connector_id',$BlockViewModel->getConnectorList(),old('connector_id',$BlockViewModel->getUserData('connector_id')),['title'=>trans('user::form.fields.connector'),'helper'=>trans('user::form.helper.connector'),'placeholder'=>trans('user::form.placeholder.please_select'),'disabled'=>'disabled']) !!}

            {!! FormHelper::input('text','connector',old('connector',$BlockViewModel->getUserData('value')),['required'=>'required','title'=>trans('user::form.fields.value'),'helper'=>trans('user::form.helper.value')]) !!}

            {!! FormHelper::select('status',trans('user::data.connector_status'),old('status',$BlockViewModel->getUserData('status')),['title'=>trans('user::form.fields.status'),'helper'=>trans('user::form.helper.status'),'placeholder'=>trans('user::form.placeholder.please_select')]) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>
