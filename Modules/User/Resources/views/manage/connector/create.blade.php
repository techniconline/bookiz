<div class="col-md-12">
    @php $user= $BlockViewModel->getUserData() @endphp
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i> @lang('user::routes.user.manage_create_connector',['full_name'=>$user->full_name])
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('user.manage.connector.create',['id'=>$BlockViewModel->getUserData('id')]),'method'=>'POST','class'=>'form-horizontal']) !!}

            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            {!! FormHelper::input('text','connector',old('connector'),['required'=>'required','title'=>$BlockViewModel->getConnectorLabel(false,$user->instance_id)]) !!}

            {!! FormHelper::select('status',trans('user::data.connector_status'),old('status'),['title'=>trans('user::form.fields.status'),'helper'=>trans('user::form.helper.status'),'placeholder'=>trans('user::form.placeholder.please_select')]) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>
