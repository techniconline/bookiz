<div class="col-md-12">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i> @lang('user::routes.user.manage_edit_user',['full_name'=>$BlockViewModel->getUserData('full_name')])
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('user.manage.edit',['id'=>$BlockViewModel->getUserData('id')]),'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            {!! FormHelper::input('text','username',old('username',$BlockViewModel->getUserData('username')),['required'=>'required','title'=>trans('user::form.fields.username'),'helper'=>trans('user::form.helper.username')]) !!}

            {!! FormHelper::input('text','first_name',old('first_name',$BlockViewModel->getUserData('first_name')),['required'=>'required','title'=>trans('user::form.fields.first_name'),'helper'=>trans('user::form.helper.first_name')]) !!}

            {!! FormHelper::input('text','last_name',old('last_name',$BlockViewModel->getUserData('last_name')),['required'=>'required','title'=>trans('user::form.fields.last_name'),'helper'=>trans('user::form.helper.last_name')]) !!}

            {!! FormHelper::input('text','father_name',old('father_name',$BlockViewModel->getUserData('father_name')),['title'=>trans('user::form.fields.father_name'),'helper'=>trans('user::form.helper.father_name')]) !!}

            {!! FormHelper::input('text','national_code',old('national_code',$BlockViewModel->getUserData('national_code')),['title'=>trans('user::form.fields.national_code'),'helper'=>trans('user::form.helper.national_code')]) !!}

            {!! FormHelper::date('birthday',old('birthday',$BlockViewModel->getUserData('birthday')),['title'=>trans('user::form.fields.birthday'),'helper'=>trans('user::form.helper.birthday'),'date-year-current'=>0,'date-year-before'=>100,'set-default'=>0]) !!}


            {!! FormHelper::select('country_id',[],old('country_id',$BlockViewModel->getUserData('country_id')),['data-init'=>1,'data-fill-url'=>route('core.location.json'),'data-change-trigger'=>'state_id','title'=>trans('user::form.fields.country'),'helper'=>trans('user::form.helper.country')]) !!}

            {!! FormHelper::select('state_id',[],old('state_id',$BlockViewModel->getUserData('state_id')),['data-fill-url'=>route('core.location.json'),'data-change-trigger'=>'city_id','title'=>trans('user::form.fields.state_id'),'helper'=>trans('user::form.helper.state_id')]) !!}

            {!! FormHelper::select('city_id',[],old('city_id',$BlockViewModel->getUserData('city_id')),['data-fill-url'=>route('core.location.json'),'title'=>trans('user::form.fields.city_id'),'helper'=>trans('user::form.helper.city_id')]) !!}

            {!! FormHelper::select('gender',trans('core::data.gender'),old('gender',$BlockViewModel->getUserData('gender')),['title'=>trans('user::form.fields.gender'),'helper'=>trans('user::form.helper.gender'),'placeholder'=>trans('user::form.placeholder.please_select')]) !!}

            {!! FormHelper::select('marital',trans('core::data.marital'),old('marital',$BlockViewModel->getUserData('marital')),['title'=>trans('user::form.fields.marital'),'helper'=>trans('user::form.helper.marital'),'placeholder'=>trans('user::form.placeholder.please_select')]) !!}

            {!! FormHelper::avatar('avatar',$BlockViewModel->getUserData('small_avatar_url'),['title'=>trans('user::form.fields.avatar'),'helper'=>trans('user::form.helper.avatar')]) !!}

            {!! FormHelper::select('avatar_confirm',trans('core::data.confirm'),old('avatar_confirm',$BlockViewModel->getUserData('avatar_confirm')),['title'=>trans('user::form.fields.avatar_confirm'),'helper'=>trans('user::form.helper.avatar_confirm'),'placeholder'=>trans('user::form.placeholder.please_select')]) !!}

            {!! FormHelper::select('confirm',trans('core::data.confirm'),old('confirm',$BlockViewModel->getUserData('confirm')),['title'=>trans('user::form.fields.confirm'),'helper'=>trans('user::form.helper.confirm'),'placeholder'=>trans('user::form.placeholder.please_select')]) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>

