<div class="col-md-12">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i> @lang('user::routes.user.manage_create_user')
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('user.manage.create'),'method'=>'POST','class'=>'form-horizontal']) !!}

            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            {!! FormHelper::input('text','first_name',old('first_name'),['required'=>'required','title'=>trans('user::form.fields.first_name'),'helper'=>trans('user::form.helper.first_name')]) !!}

            {!! FormHelper::input('text','last_name',old('last_name'),['required'=>'required','title'=>trans('user::form.fields.last_name'),'helper'=>trans('user::form.helper.last_name')]) !!}

            {!! FormHelper::input('text','connector',old('connector'),['autocomplete'=>"off",'required'=>'required','title'=>$BlockViewModel->getConnectorLabel()]) !!}

            {!! FormHelper::input('password','password',old('password'),['autocomplete'=>"off",'required'=>'required','title'=>trans('user::form.fields.password')]) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>
