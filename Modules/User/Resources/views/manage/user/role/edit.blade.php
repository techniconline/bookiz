<div class="col-md-12">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i> @lang('user::routes.user.manage_role')
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('user.manage.user.roles.edit',['user_id'=>$BlockViewModel->getModelData('user_id',0),'id'=>$BlockViewModel->getModelData('id',0)]),'method'=>'POST','class'=>'form-horizontal']) !!}

            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            {!! FormHelper::instances('instance_id',old('instance_id',$BlockViewModel->getModelData('instance_id')),['required'=>'required','title'=>trans('user::form.fields.instance'),'helper'=>trans('user::form.helper.instance')]) !!}

            {!! FormHelper::select('role_id',[],old('role_id',$BlockViewModel->getModelData('role_id')),['data-init'=>1,'data-fill-url'=>route('user.manage.user.roles.json'),'data-parent'=>'instance_id','title'=>trans('user::form.fields.role')]) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>

