<div class="col-md-12">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i> @lang('user::routes.user.manage_change_password',['full_name'=>$BlockViewModel->getUserData('full_name')])
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('user.manage.password',['id'=>$BlockViewModel->getUserData('id')]),'method'=>'POST','class'=>'form-horizontal']) !!}

            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            {!! FormHelper::input('password','password',null,['autocomplete'=>"off",'required'=>'required','title'=>trans('user::form.fields.password')]) !!}
            {!! FormHelper::input('password','password_confirmation',null,['autocomplete'=>"off",'required'=>'required','title'=>trans('user::form.fields.password_confirmation')]) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>
