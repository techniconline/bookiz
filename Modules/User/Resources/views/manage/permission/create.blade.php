<div class="col-md-12">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i> @lang('user::routes.user.create_permission')
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('user.manage.permission.create'),'method'=>'POST','class'=>'form-horizontal']) !!}

            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            {!! FormHelper::select('module',$BlockViewModel->getModulesList(),old('module'),['required'=>'required','title'=>trans('user::form.fields.module'),'helper'=>trans('user::form.helper.module')]) !!}

            {!! FormHelper::input('text','permission',old('permission'),['required'=>'required','title'=>trans('user::form.fields.permission'),'helper'=>trans('user::form.helper.permission')]) !!}

            {!! FormHelper::input('text','description',old('description'),['required'=>'required','title'=>trans('user::form.fields.description'),'helper'=>trans('user::form.helper.description')]) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>
