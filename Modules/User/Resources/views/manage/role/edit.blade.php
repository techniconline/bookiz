<div class="col-md-12">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i> @lang('user::routes.user.manage_role')
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('user.manage.role.edit',['id'=>$BlockViewModel->getModelData('id',0)]),'method'=>'POST','class'=>'form-horizontal']) !!}

            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            {!! FormHelper::instances('instance_id',old('instance_id',$BlockViewModel->getModelData('instance_id')),['required'=>'required','title'=>trans('user::form.fields.instance'),'helper'=>trans('user::form.helper.instance')]) !!}

            {!! FormHelper::input('name','name',old('name',$BlockViewModel->getModelData('name')),['required'=>'required','title'=>trans('user::form.fields.name'),'helper'=>trans('user::form.helper.name')]) !!}

            {!! FormHelper::input('title','title',old('title',$BlockViewModel->getModelData('title')),['required'=>'required','title'=>trans('user::form.fields.title'),'helper'=>trans('user::form.helper.title')]) !!}

            {!! FormHelper::select('type',trans('user::data.role_type'),old('type',$BlockViewModel->getModelData('type')),['title'=>trans('user::form.fields.type'),'helper'=>trans('user::form.helper.type')]) !!}

            {!! FormHelper::checkbox('is_admin',1,old('is_admin',$BlockViewModel->getModelData('is_admin')),['title'=>trans('user::form.fields.is_admin'),'helper'=>trans('user::form.helper.is_admin')]) !!}

            {!! FormHelper::checkbox('default',1,old('default',$BlockViewModel->getModelData('default')),['title'=>trans('user::form.fields.default'),'helper'=>trans('user::form.helper.default')]) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>

