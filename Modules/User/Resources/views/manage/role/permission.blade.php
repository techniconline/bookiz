<div class="col-md-12">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i> @lang('user::routes.user.manage_role_permissions_by_name',['name'=>$BlockViewModel->getModelData('name')])
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('user.manage.role.permission',['id'=>$BlockViewModel->getModelData('id',0)]),'method'=>'POST','class'=>'form-horizontal']) !!}

            @foreach($BlockViewModel->getAllPermissions() as $key=>$permissions)
                @php $name=strtolower($key) @endphp
                {!! FormHelper::legend(trans('user::form.titles.permissions_by_module',['module'=>$key])) !!}
                <table class="table table-striped table-responsive">
                    <thead>
                        <tr>
                            <th>{!! FormHelper::checkbox('all',null,old('all'),['id'=>$name]) !!}</th>
                            <th>@lang('user::form.fields.name')</th>
                            <th>@lang('user::form.fields.description')</th>
                            <th>@lang('user::form.fields.module')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($permissions as $permission)
                            <tr>
                                <td>{!! FormHelper::checkbox('permissions['.$name.']['.$permission->id.']',$permission->permission,$BlockViewModel->hasPermission($permission->id,$name),['id'=>$name.'_id_'.$permission->id]) !!}</td>
                                <td>{!! $permission->permission !!}</td>
                                <td>{!! $permission->description !!}</td>
                                <td>{!! $key !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="clearfix"></div>
            @endforeach
            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>

