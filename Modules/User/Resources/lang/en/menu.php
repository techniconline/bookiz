<?php
return [
    'manage'=>[
        'users'=>'مدیریت کاربران',
        'user'=>'مدیریت کاربری',
        'user_config'=>'تنظیمات بخش کاربری',
        'permissions'=>'دسترسی ها',
        'roles'=>'مدیریت نقش ها',
    ],
    'config'=>[
        'instance'=>'تنظیمات کاربری',
    ]
];