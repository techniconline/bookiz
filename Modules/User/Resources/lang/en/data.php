<?php
return [
    'connectors' =>[
        'email'=>'ایمیل',
        'mobile'=>'شماره همراه',
        'national_code'=>'کد ملی',
    ],
    'connector_status' => [
        'active'=>'فعال شده',
        'deactive'=>'غیر فعال',
        'wait'=>'در انتظار تایید',
    ],
    'role_type' => [
        'site'=>'وبسایت',
        'course'=>'درس',
    ],
    'gender'=>[
        1=>'زن',
        2=>'مرد',
    ],
    'gender_key'=>[
        1=>'female',
        2=>'male',
    ],
    'marital'=>[
        1=>'مجرد',
        2=>'متاهل',
    ],
];