<?php
return [
    'username'=>'required|alpha_dash|unique:users,username',
    'first_name'=>'required|min:2',
    'last_name'=>'required|min:2',
    'father_name'=>'required|min:2',
    'national_code'=>'required|digits:10',
    'date'=>'required|date',
    'country_id'=>'required|exists:locations,id',
    'state_id'=>'required|exists:locations,id',
    'city_id'=>'required|exists:locations,id',
    'gender'=>'required|integer|between:1,2',
    'marital'=>'required|integer|between:1,2',
    'avatar_confirm'=>'required|integer|between:0,1',
    'confirm'=>'required|integer|between:0,1',
    'avatar'=>'required|mimes:jpeg,jpg,png|max:1024',
];