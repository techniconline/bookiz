<?php
return [
    'action' => [
        'success'=>'ذخیره داده ها با موفقیت انجام شد.',
        'error'=>'متاسفانه ذخیره داده ها ممکن نیست.',
        'verify_closed'=>'حساب کاربری شما قبلا تایید شده است.',
        'verify_change'=>':text شما تغییر پیدا کرد.',
        'verify_change_fail'=>'متاسفانه عملیات مورد نظر به دلایل فنی قابل اجرا نمی باشد.',
        'verify_resend'=>'کد فعالسازی مجددا ارسال شد.',
        'verify_token_success'=>'حساب کاربری شما با موفقیت تایید شد.',
        'verify_token_wrong'=>'کد فعالسازی وارد شده اشتباه است.',
        'token_expired'=>'کد فعالسازی منقضی شده است.',
        'forgot_send'=>'در صورتی که عضو بوده باشید کلید فعالسازی برایتان ارسال می شود.',
        'access_error'=>'متاسفانه دسترسی شما به این بخش ممکن نیست.',
        'user_not_find'=>'متاسفانه کاربر مورد نظر وجود ندارد.',
        'not_find'=>'متاسفانه مورد نظر وجود ندارد.',
        'success_in_logout'=>'خروج شما با موفقیت انجام شد.',
        'before_logout'=>'خروج شما قبلا انجام شده!',
        'error_in_logout'=>'خطا در خروج از سیستم!',
        'current_password_is_not_valid'=>'کلمه عبور قبلی را درست وارد نکرده اید!',

        'update_success'=>'عملیات بروزرسانی سازی با موفقیت انجام شد.',
        'del_success'=>'عملیات حذف با موفقیت انجام شد.',
        'save_un_success'=>'عملیات ذخیره سازی با موفقیت انجام نشد.',
        'update_un_success'=>'عملیات بروزرسانی سازی با موفقیت انجام نشد.',
        'del_un_success'=>'عملیات حذف با موفقیت انجام نشد.',
        'mis_data' => 'اطلاعات ناقص است!',
        'can_not_register'=>'امکان ثبت نام در این سامانه وجود ندارد.',
      ],
    'sms'=>[
        'verify'=>'کاربر عزیز،
:token2
ثبت نام شما با موفقیت انجام شد. کد تایید شما:
:token1
',
       'forgot'=>'کاربر عزیز،
:token2
 کد تغییر رمز عبور شما:
:token1
',
    ],
    "jwt"=>[
        "error_system"=>"system has error, code: :code , message: :message .",
        "err_system"=>"system has error.",
    ],
    'email'=>[
        'hello_user'=>'سلام :first_name،',
        'verify_token'=>'کد فعالسازی حساب کاربری',
        'regards'=>'موفق و پیروز باشید',
        'verify_user_account'=>'فعالسازی حساب کاربری',
        'reset_password_verify'=>'کد فراموشی رمز عبور',
        'reset_password'=>'تغییر رمز عبور',
    ],
    'user'=>[
      'verify_message'=>'کاربر گرامی، لطفا حساب کاربری خود را فعال نمایید.',
      'verify_account'=>'فعالسازی حساب کاربری',
    ],
];