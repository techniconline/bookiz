<?php
return [
    'config' => [
        'instance'=>'تنظیمات کاربری وب سایت',
    ],
    'user' => [
        'manage_edit'=>'ویرایش کاربر',
        'manage_edit_user'=>'ویرایش کاربر (:full_name)',
        'create_qr_code_login'=>'کد QR برای ورود در اپلیکیشن برای کاربر (:full_name)',
        'available_time_qr_code_login'=>'زمان معتبر بودن کد',
        'now_time_qr_code_login'=>'زمان اکنون',
        'manage_edit_connector'=>'ویرایش دسترسی ارتباطی (:full_name)',
        'manage_create_connector'=>'اضافه کردن دسترسی ارتباطی (:full_name)',
        'manage_user_connectors'=>'مدیریت دسترسی ارتباطی ',
        'manage_create_user'=>'اضافه کردن کاربر جدید',
        'change_password'=>'تغییر رمز عبور',
        'manage_change_password'=>'تغییر رمز عبور (:full_name)',
        'manage_role_edit'=>'ویرایش نقش',
        'manage_role_permissions'=>'دسترسی های نقش',
        'manage_role_permissions_by_name'=>'تعیین دسترسی های نقش (:name)',
        'manage_role_create'=>'ایجاد نقش جدید',
        'roles'=>'نقش های کاربری',
        'add_role'=>'اضافه کردن نقش کاربری (:full_name)',

        'favorites' => 'محبوب ها',
        'edit_meta_tags' => 'مدیریت متاها',
        'copy_meta_group' => 'کپی از متا',
        'del_meta_group' => 'حذف متا',
        'manage_connector'=>'مدیریت راه های دسترسی کاربر',
        'user_courses'=>'درس های کاربر',
        'user_credits'=>'لیست تراکنش های اعتبار کاربر',

    ],
];