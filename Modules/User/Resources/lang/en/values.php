<?php
return [
    'email'=>'ایمیل',
    'mobile'=>'شماره همراه',
    'men'=>'مرد',
    'women'=>'زن',
    'single'=>'مجرد',
    'married'=>'متاهل',
    'active'=>'فعال شده',
    'deactive'=>'غیر فعال',
    'wait'=>'در انتظار',
];