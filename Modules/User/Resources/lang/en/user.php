<?php

return [

    'list'=>'لیست کاربران',
    'meta'=>'متا',
    'meta_groups' => 'متاها',
    'edit_meta_tags' => 'مدیریت متاها',
    'copy_meta_group' => 'کپی از متا',
    'del_meta_group' => 'حذف متا',
    'cancel' => 'انصراف',
    'submit' => 'ثبت',
    'status' => 'وضعیت',
];