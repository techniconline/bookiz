<?php

namespace Modules\User\Models\UserContact;


use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\User\Traits\UserContact\UserContact as TraitModel;

class UserContact extends BaseModel
{

    use TraitModel;
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'contact', 'value', 'active', 'deleted_at', 'created_at', 'updated_at'];
    public $messages = [];
    public $rules = [
        'user_id' => 'required',
        'contact' => 'required',
        'value' => 'required',
    ];

    public $contacts = ['twitter' => 'twitter', 'instagram' => 'instagram', 'telegram' => 'telegram'
        , 'soroush' => 'soroush', 'whatsapp'=>'whatsapp', 'email'=>'email'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
