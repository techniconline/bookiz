<?php

namespace Modules\User\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\User\Traits\User\UserRoleTrait;

class PasswordReset extends BaseModel
{

    public $timestamps = true;
    protected $fillable = ['user_id', 'connector_id', 'token', 'hash', 'available_time', 'created_at', 'updated_at'];

    /**
     * @return string
     */
    public function generateHashKey()
    {
        return sha1(str_random(16));
    }

    /**
     * @param int $min
     * @return false|string
     */
    public function createAvailableTime($min = 10)
    {
        $now = strtotime(date("Y-m-d H:i:s"));
//        $expireTime = date("Y-m-d H:i:s", strtotime('+'.$min.' minutes', $now));
        $expireTime = (strtotime('+'.$min.' minutes', $now));
        return $expireTime;
    }

}