<?php

namespace Modules\User\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Traits\User\UserRoleTrait;

class UserRole extends Model 
{

    protected $table = 'user_roles';
    public $timestamps = true;

    use SoftDeletes;
    use UserRoleTrait;

    protected $dates = ['deleted_at'];
    protected $fillable = array('user_id', 'role_id');


}