<?php

namespace Modules\User\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\User\Traits\User\UserTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Modules\User\Models\Register\UserRegisterKey;

class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    use Notifiable;
    use SoftDeletes;
    use UserTrait;

    protected $table = 'users';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $fillable = array('username', 'password', 'instance_id', 'first_name', 'last_name', 'father_name', 'national_code', 'birthday', 'country_id', 'state_id', 'city_id', 'gender', 'marital', 'avatar_confirm', 'confirm', 'avatar','deleted_at');

    public $api_fields = ['id', 'username', 'password', 'instance_id', 'first_name', 'last_name', 'father_name', 'national_code', 'birthday', 'country_id', 'state_id', 'city_id', 'gender', 'marital', 'avatar_confirm', 'confirm'];
    public $list_fields = ['id', 'username', 'password', 'instance_id', 'first_name', 'last_name', 'father_name', 'national_code', 'birthday', 'country_id', 'state_id', 'city_id', 'gender', 'marital', 'avatar_confirm', 'confirm'];
    public $api_append_fields = ['full_name', 'birthday_by_format', 'small_avatar_url'];
    public $list_append_fields = ['full_name', 'small_avatar_url'];

    const SYSTEM_NAME = "user";

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['avatar', 'small_avatar_url'];


    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForMail($notification=null)
    {
        $userConnector=UserRegisterKey::where('user_id',$this->id)->where('connector_id',1)->first();
        if($userConnector){
            return $userConnector->value;
        }
        return null;
    }
}