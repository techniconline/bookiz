<?php

namespace Modules\User\Models\Meta;

use Modules\Core\Models\BaseModel;
use Modules\User\Traits\Meta\UserMetaGroup as TraitModel;

class UserMetaGroup extends BaseModel
{
    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'meta_group_id'];
    public $messages = [];
    public $rules = [
        'course_id' => 'required',
        'meta_group_id' => 'required',
    ];

    public $timestamps = false;

}
