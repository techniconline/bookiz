<?php

namespace Modules\User\Models\Register;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Illuminate\Notifications\Notifiable;

class UserRegisterKey extends BaseModel
{

    protected $table = 'user_register_key';
    public $timestamps = true;

    use SoftDeletes;
    use Notifiable;

    protected $with = ['connector'];
    protected $dates = ['deleted_at'];
    protected $fillable = array('user_id', 'connector_id', 'value', 'status', 'token', 'hash');
    public $api_fields = array('id', 'user_id', 'connector_id', 'value', 'status');
    public $list_fields = ['id', 'user_id', 'connector_id', 'value', 'status'];
    public $api_append_fields = [];
    public $list_append_fields = [];

    protected $hidden = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Models\User');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function connector()
    {
        return $this->belongsTo('Modules\User\Models\Connector');
    }


    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification $notification
     * @return string
     */
    public function routeNotificationForMail($notification = null)
    {
        return $this->value;

    }

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification $notification
     * @return string
     */
    public function routeNotificationForSms($notification = null)
    {
        return $this->value;
    }
}