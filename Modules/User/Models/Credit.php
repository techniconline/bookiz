<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\User\Traits\Credit as TraitModel;

class Credit extends BaseModel
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'user_id', 'model_id', 'model_type', 'type', 'amount', 'current_credit', 'created_at', 'updated_at'];

    const TYPE_MINUS = 'minus';
    const TYPE_PLUS = 'plus';

    use TraitModel;
    use SoftDeletes;

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
