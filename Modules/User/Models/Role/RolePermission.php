<?php

namespace Modules\User\Models\Role;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Traits\Role\RolePermissionTrait;

class RolePermission extends Model 
{

    protected $table = 'role_permissions';
    public $timestamps = true;

    use SoftDeletes;
    use RolePermissionTrait;

    protected $dates = ['deleted_at'];
    protected $fillable = array('role_id','permissions');


}