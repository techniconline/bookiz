<?php

namespace Modules\User\Models\Connector;

use Illuminate\Database\Eloquent\Model;

class InstanceConnector extends Model 
{

    protected $table = 'instance_connectors';
    public $timestamps = true;

}