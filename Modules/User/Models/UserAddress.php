<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\User\Traits\Address\UserAddress as TraitModel;

class UserAddress extends BaseModel
{
    use SoftDeletes;
    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'province_id', 'city_id', 'address', 'latitude', 'longitude', 'location', 'is_default', 'active', 'deleted_at', 'created_at', 'updated_at'];
    public $api_fields = [];
    public $list_fields = [];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
