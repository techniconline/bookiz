<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use \Modules\User\Traits\Permission\PermissionTrait;

class Permission extends BaseModel
{
    use PermissionTrait;

    protected $table = 'permissions';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('module', 'permission', 'description');

}