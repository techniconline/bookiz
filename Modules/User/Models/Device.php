<?php

namespace Modules\User\Models;

use Modules\Core\Models\BaseModel;
use Modules\User\Traits\Device as TraitModel;

class Device extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'os', 'app', 'push_user_id', 'push_token', 'active'];

    public $rules = [
        'os' => 'required',
        'push_token' => 'required',
    ];

    public $messages = [
        'os.required' => 'لطفا سیستم عامل را مشخص کنید',
        'app.required' => 'لطفا سامانه را مشخص کنید',
        'push_user_id.required' => 'لطفا userID را مشخص کنید',
        'push_token.required' => 'لطفا pushToken را مشخص کنید',
    ];

}
