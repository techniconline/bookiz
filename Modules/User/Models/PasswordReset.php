<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model 
{

    protected $table = 'password_resets';
    public $timestamps = true;
    protected $fillable = array('token', 'available_time');

}