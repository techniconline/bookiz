<?php
namespace Modules\User\Models;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\User\Libraries\Connector\Master\Connector;

class Register
{

    public function registerUser(Request $request,Connector $connector,$confirm=0){
        DB::beginTransaction();
        try{
            /*add user*/
            $instance_id=app('getInstanceObject')->getCurrentInstanceId();
            $user = new User;
            $user->instance_id=$instance_id;
            $user->username=$this->getUsername($connector,$request->get('connector'));
            $user->password=bcrypt($request->get('password'));
            $user->first_name=$request->get('first_name');
            $user->last_name=$request->get('last_name');
            $user->confirm=$confirm;
            if($request->has('national_code')){
                $user->national_code=$request->get('national_code');
            }
            $user->save();

            $user->addDefaultMeta();

            /*add connector*/
            $userConnector=new UserRegisterKey;
            $userConnector->user_id=$user->id;
            $userConnector->connector_id=$connector->getId();
            $userConnector->value=$request->get('connector');
            if($confirm){
                $userConnector->status=Connector::CONNECTOR_ACTIVE;
                $userConnector->token=null;
                $userConnector->hash=null;
            }else{
                $userConnector->status=Connector::CONNECTOR_DEACTIVE;
                $userConnector->token=$connector->getToken();
                $userConnector->hash=$connector->getHash($connector->getId(),$user->id);
            }

            $userConnector->save();
            /*add default Role*/

            DB::commit();
        }catch (Exception $e){
            DB::rollBack();
            return false;
        }
        return $user;
    }


    public function getUsername($connector,$value){
        return $connector->getName().'_'.$value.'_'.rand(1000,100000);
    }


    public function activeUser($userConnector,$user=null){
        if(is_null($user)){
            $user=Auth::user();
        }
        DB::beginTransaction();
        try{
            /*add user*/
            $user->confirm=1;
            $user->save();

            /*add connector*/
            $userConnector->status=Connector::CONNECTOR_ACTIVE;
            $userConnector->token=null;
            $userConnector->hash=null;
            $userConnector->save();
            /*add default Role*/

            DB::commit();
        }catch (Exception $e){
            DB::rollBack();
            return false;
        }
        return true;
    }

}