<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Models\BaseModel;

class Connector extends BaseModel
{


    protected $table = 'connector';
    public $timestamps = false;
    protected $fillable = array('name', 'code', 'reset_time', 'message');

    public $api_fields = array('id', 'name', 'code');
    public $list_fields = ['id', 'name', 'code'];
    public $api_append_fields = [];
    public $list_append_fields = [];

}