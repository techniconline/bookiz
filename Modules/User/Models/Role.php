<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\User\Traits\Role\RoleTrait;

class Role extends BaseModel
{

    use RoleTrait;
    protected $table = 'roles';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('instance_id', 'name', 'type', 'self_active', 'default');


    /*relation*/
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userRole()
    {
        return $this->hasMany('Modules\User\Models\User\UserRole');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseUserEnrollments()
    {
        return $this->hasMany('Modules\Course\Models\Enrollment\CourseUserEnrollment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseEnrollments()
    {
        return $this->hasMany('Modules\Course\Models\Enrollment\CourseEnrollment');
    }

}