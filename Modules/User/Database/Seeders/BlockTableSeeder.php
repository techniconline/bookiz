<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Block\Block;
use Modules\Core\Models\Module\Module;


class BlockTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('currencies')->delete();

        $module_id=Module::where('name','user')->first()->id;

        $block = new Block();
        $block->fill(array(
            'module_id'=>$module_id,
            'name' => 'user_popup_block',
            'namespace' => 'Modules\User\ViewModels\Blocks\UserPopupBlock',
            'active' => 1,
        ))->save();
	}
}