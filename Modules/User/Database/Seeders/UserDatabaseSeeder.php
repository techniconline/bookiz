<?php
namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserDatabaseSeeder extends Seeder {

	public function run()
	{
		//Model::unguard();
        $this->call('Modules\User\Database\Seeders\ModuleTableSeeder');
        $this->command->info('Module table seeded!');

        $this->call('Modules\User\Database\Seeders\BlockTableSeeder');
        $this->command->info('Block table seeded!');

		$this->call('Modules\User\Database\Seeders\ConnectorTableSeeder');
		$this->command->info('Connector table seeded!');

        $this->call('Modules\User\Database\Seeders\PermissionTableSeeder');
        $this->command->info('Permission table seeded!');

        $this->call('Modules\User\Database\Seeders\RoleTableSeeder');
        $this->command->info('Role table seeded!');

        $this->call('Modules\User\Database\Seeders\RolePermissionsTableSeeder');
        $this->command->info('Role Permission table seeded!');

        $this->call('Modules\User\Database\Seeders\ConfigTableSeeder');
        $this->command->info('Config Permission table seeded!');

        $this->call('Modules\User\Database\Seeders\UserTableSeeder');
        $this->command->info('User table seeded!');

        $this->call('Modules\User\Database\Seeders\SocialsUserSeeder');
        $this->command->info('Social items seeded!');


    }
}