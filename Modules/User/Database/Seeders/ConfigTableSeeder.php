<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;

use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Instance\InstanceConfigs;
use Modules\Core\Models\Module\Module;


class ConfigTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('currencies')->delete();

        $instance=Instance::where('name','admin')->first();
        $module_id=Module::where('name','user')->first()->id;

        $InstanceConfigs=new InstanceConfigs();
        $InstanceConfigs->fill(array(
            'name'=>'instance',
            'instance_id'=>$instance->id,
            'language_id'=>$instance->language_id,
            'module_id'=>$module_id,
            'configs'=>array (
                'connector' =>
                    array (
                        0 => 'email',
                        1 => 'mobile',
                    ),
                'usernamehide' => '0',
                'verify_type' => 'month',
            ),
        ))->save();


        $instance=Instance::where('name','www')->first();
        $InstanceConfigs=new InstanceConfigs();
        $InstanceConfigs->fill(array(
            'name'=>'instance',
            'instance_id'=>$instance->id,
            'language_id'=>$instance->language_id,
            'module_id'=>$module_id,
            'configs'=>array (
                'connector' =>
                    array (
                        0 => 'email',
                        1 => 'mobile',
                    ),
                'usernamehide' => '0',
                'verify_type' => 'month',
            ),
        ))->save();

	}
}