<?php
namespace Modules\User\Database\Seeders;
use Illuminate\Database\Seeder;
use Modules\User\Models\Role;
use Modules\Core\Models\Instance\Instance;

class RoleTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('connector')->delete();

        $roles=[
            [NULL,'user','کاربر','site',0,0,1],
            [NULL,'student','دانش آموز','course',0,0,0],
            [NULL,'teacher','معلم','course',0,0,0],
            [NULL,'admin','مدیر','site',0,1,0],
            [NULL,'superadmin','مدیر کل','site',0,1,0],
        ];

        foreach ($roles as $role) {
            $saver=new Role();
            $saver->fill(array(
                'instance_id'=>$role[0],
                'name'=>$role[1],
                'title'=>$role[2],
                'type'=>$role[3],
                'self_active'=>$role[4],
                'is_admin'=>$role[5],
                'default'=>$role[6]
            ))->save();
        }
	}
}