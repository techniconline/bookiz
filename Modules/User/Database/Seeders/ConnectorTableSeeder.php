<?php
namespace Modules\User\Database\Seeders;
use Illuminate\Database\Seeder;
use Modules\User\Models\Connector;

class ConnectorTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('connector')->delete();

		// ConnectorSeeder
        $Connector=new Connector();
		$Connector->fill(array(
				'name' => 'email',
				'code' => 'email',
				'token_length' => 8,
				'reset_time' => 86400,
				'message' => 1
			))->save();

		// ConnectorSeeder
        $Connector=new Connector();
        $Connector->fill(array(
				'name' => 'mobile',
				'code' => 'mobile',
				'token_length' => 6,
				'reset_time' => 1800,
				'message' => 1
			))->save();

        // ConnectorSeeder
        $Connector=new Connector();
        $Connector->fill(array(
            'name' => 'national',
            'code' => 'national',
            'token_length' => 6,
            'reset_time' => 1800,
            'message' => 0
        ))->save();

        //
        $Connector=new Connector();
        $Connector->fill(array(
            'name' => 'medu',
            'code' => 'medu',
            'token_length' => 6,
            'reset_time' => 1800,
            'message' => 0
        ))->save();

	}
}