<?php
namespace Modules\User\Database\Seeders;
use Illuminate\Database\Seeder;

use Modules\User\Models\Role;
use Modules\User\Models\Role\RolePermission;
use Modules\User\Models\Permission;

class RolePermissionsTableSeeder extends Seeder {

	public function run()
	{
	    $role=Role::where('name','superadmin')->first();
	    $all=Permission::all();
	    $permissions=[];
	    foreach($all as $permission){
	        if(!isset($permissions[$permission->module])){
                $permissions[$permission->module]=[];
            }
            $permissions[$permission->module][$permission->id]=$permission->permission;
        }

        $rolePermissions=new RolePermission();
        $rolePermissions->fill(array(
            'role_id' => $role->id,
            'permissions' => $permissions,
        ))->save();

    }
}