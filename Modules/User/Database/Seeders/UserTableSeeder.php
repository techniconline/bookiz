<?php
namespace Modules\User\Database\Seeders;
use Illuminate\Database\Seeder;

use Modules\Core\Models\Instance\Instance;
use Modules\User\Models\Role;
use Modules\User\Models\User;
use Modules\User\Models\Connector;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\User\Models\User\UserRole;

class UserTableSeeder extends Seeder {

	public function run()
	{
        $instance=Instance::where('name','admin')->first();
        $role=Role::where('name','superadmin')->first();

        $user=new User();
        $user->fill(array(
            'instance_id'=>$instance->id,
            'username'=>'admin',
            'password'=>bcrypt('farayad@123'),
            'first_name'=>'admin',
            'last_name'=>'user',
            'confirm'=>1,
        ))->save();

        $connector=Connector::where('code','email')->first();

        $userRegisterKey=new UserRegisterKey();
        $userRegisterKey->fill(array(
            'user_id'=>$user->id,
            'connector_id'=>$connector->id,
            'value'=>'admin@farayad.org',
            'status'=>'active',
            'token'=>'0',
        ))->save();


        $userRole=new UserRole();
        $userRole->fill(array(
            'instance_id'=>$instance->id,
            'user_id'=>$user->id,
            'role_id'=>$role->id
        ))->save();

    }
}