<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('os')->default('android')->index(); // enum [ android , ios , web ]
            $table->string('app')->default('farayad');
            $table->string('push_user_id')->index()->nullable();
            $table->string('push_token')->index()->nullable();
            $table->tinyInteger('active')->default(1)->index();
            $table->timestamps();
        });

        Schema::table('devices', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('devices', function(Blueprint $table) {
            $table->dropForeign('devices_user_id_foreign');
        });
        Schema::dropIfExists('devices');
    }
}
