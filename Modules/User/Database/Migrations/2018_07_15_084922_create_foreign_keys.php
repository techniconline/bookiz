<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
        Schema::table('form_records', function(Blueprint $table) {
        $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });

		Schema::table('user_register_key', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('user_register_key', function(Blueprint $table) {
			$table->foreign('connector_id')->references('id')->on('connector')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('role_permissions', function(Blueprint $table) {
			$table->foreign('role_id')->references('id')->on('roles')
						->onDelete('cascade')
						->onUpdate('cascade');
		});

		Schema::table('user_roles', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('user_roles', function(Blueprint $table) {
			$table->foreign('role_id')->references('id')->on('roles')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('password_resets', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('password_resets', function(Blueprint $table) {
			$table->foreign('connector_id')->references('id')->on('connector')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{

        Schema::table('form_records', function (Blueprint $table) {
            $table->dropForeign('form_feature_value_relations_user_id_foreign');
        });

		Schema::table('user_register_key', function(Blueprint $table) {
			$table->dropForeign('user_register_key_user_id_foreign');
		});
		Schema::table('user_register_key', function(Blueprint $table) {
			$table->dropForeign('user_register_key_connector_id_foreign');
		});
		Schema::table('role_permissions', function(Blueprint $table) {
			$table->dropForeign('role_permissions_role_id_foreign');
		});
		Schema::table('role_permissions', function(Blueprint $table) {
			$table->dropForeign('role_permissions_permission_id_foreign');
		});
		Schema::table('user_roles', function(Blueprint $table) {
			$table->dropForeign('user_roles_user_id_foreign');
		});
		Schema::table('user_roles', function(Blueprint $table) {
			$table->dropForeign('user_roles_role_id_foreign');
		});
		Schema::table('password_resets', function(Blueprint $table) {
			$table->dropForeign('password_resets_user_id_foreign');
		});
		Schema::table('password_resets', function(Blueprint $table) {
			$table->dropForeign('password_resets_connector_id_foreign');
		});
	}
}