<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePasswordResetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('password_resets', function(Blueprint $table) {
            $table->dropForeign('password_resets_connector_id_foreign');
        });

        Schema::table('password_resets', function (Blueprint $table) {
            $table->unsignedInteger("connector_id")->nullable()->change();
            $table->string("token",50)->nullable()->change();
        });

        Schema::table('password_resets', function(Blueprint $table) {
            $table->foreign('connector_id')->references('id')->on('connector')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
