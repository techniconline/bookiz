<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConnectorTable extends Migration {

	public function up()
	{
		Schema::create('connector', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 150);
			$table->string('code', 20)->unique()->nullable();
			$table->smallInteger('token_length')->default('6');
			$table->integer('reset_time')->index()->default('3600');
			$table->boolean('message')->nullable()->index()->default(0);
		});
	}

	public function down()
	{
		Schema::drop('connector');
	}
}