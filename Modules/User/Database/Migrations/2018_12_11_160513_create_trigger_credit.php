<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerCredit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        
            CREATE TRIGGER `update_credit_before_insert`
            BEFORE INSERT ON `credits`
            FOR EACH ROW
            BEGIN
            
                DECLARE _user_id INT;
                DECLARE _current_credit DOUBLE;
                DECLARE _type varchar(50);
                
                SET _type = new.type;
                
                select user_id, current_credit into _user_id, _current_credit from credits where user_id=new.user_id order by id desc limit 1;
                
                IF (_type = \'plus\') THEN
                    SET new.current_credit = _current_credit + new.amount;
                ELSE
                    SET new.current_credit = _current_credit - new.amount;
                END IF;
            
            END
            
           ');

        DB::unprepared('
        
            CREATE TRIGGER `update_current_credit_before_update`
            BEFORE UPDATE ON `credits`
            FOR EACH ROW
            BEGIN
            
                DECLARE _amount DOUBLE;
                DECLARE _current_credit DOUBLE;
                DECLARE _type varchar(50);
                
                DECLARE _old_amount DOUBLE;
                DECLARE _old_current_credit DOUBLE;
                DECLARE _old_type varchar(50);
                
                SET _type = new.type;
                SET _current_credit = new.current_credit;
                SET _amount = new.amount;
                
                SET _old_type = old.type;
                SET _old_current_credit = old.current_credit;
                SET _old_amount = old.amount;
                
                IF (_old_amount > _amount) THEN
                    set new.current_credit = (_old_current_credit - (_old_amount - _amount));
                ELSE
                    set new.current_credit = (_old_current_credit + ( _amount - _old_amount ));
                END IF;
                
                IF (_old_type != _type AND _type=\'plus\') THEN
                    set new.current_credit = (_old_current_credit + (_amount * 2));
                END IF;
                
                IF ( _old_type != _type AND _type=\'minus\') THEN
                    set new.current_credit = (_old_current_credit - (_amount * 2));
                END IF;
            
            END
            
           ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS `update_current_credit_before_update`');
        DB::unprepared('DROP TRIGGER IF EXISTS `update_credit_before_insert`');
    }
}
