<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMetaGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_meta_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('meta_group_id');

        });

        Schema::table('user_meta_groups', function (Blueprint $table) {
            $table->foreign('meta_group_id')->references('id')->on('meta_groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_meta_groups', function (Blueprint $table) {
            $table->dropForeign('user_meta_groups_meta_group_id_foreign');
            $table->dropForeign('user_meta_groups_user_id_foreign');
        });

        Schema::dropIfExists('user_meta_groups');
    }
}
