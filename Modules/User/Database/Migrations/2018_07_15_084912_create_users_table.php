<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('instance_id')->unsigned()->index();
			$table->string('username', 100)->unique();
			$table->string('password', 200);
			$table->string('first_name', 100)->index();
			$table->string('last_name', 100)->index();
			$table->string('father_name', 100)->nullable();
			$table->string('national_code', 25)->nullable();
			$table->bigInteger('birthday')->nullable();
			$table->integer('country_id')->nullable()->index();
			$table->integer('state_id')->unsigned()->nullable()->index();
			$table->integer('city_id')->unsigned()->nullable()->index();
			$table->boolean('gender')->nullable();
			$table->boolean('marital')->nullable();
			$table->string('avatar',250)->nullable();
			$table->boolean('avatar_confirm')->default(0);
			$table->boolean('confirm')->default(0);
            $table->rememberToken();
            $table->timestamps();
			$table->softDeletes();
        });
	}

	public function down()
	{
		Schema::drop('users');
	}
}