<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRolesTable extends Migration {

	public function up()
	{
		Schema::create('roles', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('instance_id')->unsigned()->nullable();
			$table->string('name', 150)->unique();
			$table->string('title', 150);
			$table->enum('type', array('site', 'course'))->index();
			$table->boolean('is_admin')->default(0)->index();
			$table->boolean('self_active')->index()->default(0);
			$table->boolean('default')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('roles');
	}
}