<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserRolesTable extends Migration {

	public function up()
	{
		Schema::create('user_roles', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('instance_id')->unsigned()->index();
			$table->integer('user_id')->unsigned();
			$table->integer('role_id')->unsigned();
			$table->boolean('default')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('user_roles');
	}
}