<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInstanceConnectorsTable extends Migration {

	public function up()
	{
		Schema::create('instance_connectors', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('instance_id')->unsigned()->index();
			$table->integer('connector_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('instance_connectors');
	}
}