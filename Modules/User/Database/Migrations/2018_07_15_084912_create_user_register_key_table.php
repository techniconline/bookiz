<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserRegisterKeyTable extends Migration {

	public function up()
	{
		Schema::create('user_register_key', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned()->nullable();
			$table->integer('connector_id')->unsigned()->index();
			$table->string('value', 100)->index();
			$table->enum('status', array('deactive', 'active', 'wait'))->nullable()->index();
			$table->string('token', 50)->nullable()->index();
            $table->string('hash', 100)->unique()->nullable();
            $table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('user_register_key');
	}
}