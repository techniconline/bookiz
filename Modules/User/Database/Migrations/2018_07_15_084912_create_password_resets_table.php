<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePasswordResetsTable extends Migration {

	public function up()
	{
		Schema::create('password_resets', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('connector_id')->unsigned();
			$table->string('token', 50)->index();
			$table->string('hash', 100)->unique();
			$table->integer('available_time');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('password_resets');
	}
}