<?php
return [
    'version' => '20180619001',
    'blocks' => [
        /*'right_top' => [
            ['alias' => 'user_info_block',
                'view_model' => \Modules\User\ViewModels\Blocks\UserInfoBlock\UserInfoBlock::class,
                'order' => '0',
                'position' => 'top',
                'before' => '',
                'after' => '',
                'configs' => []
            ],
        ],*/
    ],
    'menus' => [
        'management_admin' => [
            [
                'alias' => 'user.manage', //**
                'route' => 'user.manage.index', //**
                'key_trans' => 'user::menu.manage.users', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-users', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_manage_user',
            ],
            [
                'alias' => 'user.manage.role', //**
                'route' => 'user.manage.role.index', //**
                'key_trans' => 'user::menu.manage.roles', //**
                'order' =>2, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-users', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_manage_user',
            ],
            [
                'alias' => 'user.manage.permission', //**
                'route' => 'user.manage.permission.index', //**
                'key_trans' => 'user::menu.manage.permissions', //**
                'order' => 3, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-users', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_manage_user',
            ],
            ['alias' => 'user.config.instance.index', //**
                'route' => 'user.config.instance.index', //**
                'key_trans' => 'user::menu.config.instance', //**
                'order' => null, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-cog', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_manage_settings',
            ],
        ],
    ],
    'group_menu' => [
        'management_admin' => [
            ['alias' => 'admin_manage_user', //**
                'key_trans' => 'user::menu.manage.user', //**
                'icon_class' => 'fa fa-users',
                'before' => 'admin_features',
                'after' => '',
                'order' => 30,
            ],
            ['alias' => 'admin_manage_settings', //**
                'key_trans' => 'core::menu.group.settings', //**w
                'icon_class' => 'fa fa-cog',
                'before' => '',
                'after' => '',
                'order' => 10000000,
            ],
        ]
    ],
];
