<?php

namespace Modules\User\Notifications;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\User\Models\User;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\Core\Notifications\Channels\SmsChannel;
use Modules\Core\Notifications\Channels\Sms\SmsMessage;
use Illuminate\Notifications\Channels\MailChannel;
use Modules\User\Models\PasswordReset as PasswordResetModel;


/**
 * Class PasswordReset
 * @package Modules\User\Notifications
 */
class PasswordReset extends Notification
{
    use Queueable;

    private $user=null;
    private $registerConnector=null;
    private $token=null;

    /**
     * Create a new notification instance.
     *
     * UserVerify constructor.
     * @param User $user
     * @param UserRegisterKey $registerConnector
     * @param PasswordReset $passwordReset
     */
    public function __construct(User $user,UserRegisterKey $registerConnector,PasswordResetModel $passwordReset)
    {
        $this->user=$user;
        $this->registerConnector=$registerConnector;
        $this->passwordReset=$passwordReset;
    }


    /**
     * @param $notifiable
     * @return array
     * @throws Exception
     */
    public function via($notifiable)
    {
        if($this->registerConnector->connector->name=='email'){
            return ['mail'];
        }elseif($this->registerConnector->connector->name=='mobile'){
            return [SmsChannel::class];
        }else{
            throw new Exception(trans('user::message.action.forgot'),404);
        }

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $url=route('forgot.hash',['hash'=>$this->passwordReset->hash]);
        return (new MailMessage)
        ->subject(trans('user::form.titles.forgotToken'))
        ->markdown('user::emails.reset', ['subject'=>trans('user::form.titles.forgotToken'),'user' => $this->user,'token'=>$this->passwordReset->token,'registerConnector'=>$this->registerConnector,'url'=>$url]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toSms($notifiable)
    {
        $message=new SmsMessage();
        $values=['token2'=>$this->user->full_name,'token1'=>$this->passwordReset->token,'template'=>'password'];
        $content=trans('user::message.sms.forgot',$values);
        return $message->setContent($content)->setTo($this->registerConnector->value)->setType('forgot')->setCustomFields($values)->getMessage();
    }


    public function saveLog($message){
        $address=storage_path('logs/email.log');
        file_put_contents($address,$message."\r\n",FILE_APPEND);
    }

}
