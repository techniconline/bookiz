<?php

namespace Modules\User\Notifications;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\User\Models\User;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\Core\Notifications\Channels\SmsChannel;
use Modules\Core\Notifications\Channels\Sms\SmsMessage;

class UserVerify extends Notification
{
    use Queueable;

    private $user = null;
    private $registerConnector = null;

    /**
     * Create a new notification instance.
     *
     * UserVerify constructor.
     * @param User $user
     * @param UserRegisterKey $registerConnector
     */
    public function __construct(User $user, UserRegisterKey $registerConnector)
    {
        $this->user = $user;
        $this->registerConnector = $registerConnector;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     * @throws Exception
     */
    public function via($notifiable)
    {
        if ($this->registerConnector->connector->name == 'email') {
            return ['mail'];
        } elseif ($this->registerConnector->connector->name == 'mobile') {
            return [SmsChannel::class];
        } else {
            throw new Exception(trans('user::message.action.verify'), 404);
        }

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = route('verify.hash', ['hash' => $this->registerConnector->hash]);
        $message = (new MailMessage)
            ->subject(trans('user::form.titles.verifyToken'))
            ->markdown('user::emails.verify', ['subject' => trans('user::form.titles.verifyToken'), 'token' => $this->registerConnector->token, 'user' => $this->user, 'registerConnector' => $this->registerConnector, 'url' => $url]);
        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toSms($notifiable)
    {
        $message = new SmsMessage();
        $values = ['token1' => $this->registerConnector->token, 'token2' => $this->user->full_name, 'template' => 'verify'];
        $content = trans('user::message.sms.verify', $values);
        return $message->setContent($content)->setTo($this->registerConnector->value)->setType('verify')->setCustomFields($values)->getMessage();
    }


    public function saveLog($message)
    {
        $address = storage_path('logs/email.log');
        file_put_contents($address, $message . "\r\n", FILE_APPEND);
    }


}
