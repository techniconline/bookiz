<?php
namespace Modules\User\Traits\User;


/**
 * Trait UserTrait
 * @package Modules\User\Traits\User
 */
Trait UserRoleTrait
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('Modules\User\Models\Role');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance');
    }


    public static function getInstanceFromUserRoles($user){
       return self::whereHas('role', function ($query) {
            $query->where('is_admin', '=', 1)->where('type','site');
        })->where('user_id',$user->id)->groupBy('instance_id')->select('instance_id')->get();
    }

    public static function getRolesByUser($user){
        $instance_id=app('getInstanceObject')->getCurrentInstance()->id;
        return self::whereHas('role', function ($query) use ($instance_id) {
            $query->where('type','site')->where('instance_id',$instance_id);
        })->where('user_id',$user->id)->get();
    }
}