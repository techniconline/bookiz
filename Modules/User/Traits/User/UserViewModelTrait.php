<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 7/25/18
 * Time: 3:40 PM
 */

namespace Modules\User\Traits\User;

use Modules\Code\Libraries\Uploader\UploaderLibrary;
use Modules\Core\Traits\Config\Settings;
use Modules\User\Models\Connector;
use Illuminate\Support\Facades\Cache;

/**
 * Trait UserViewModelTrait
 * @package Modules\User\Traits\User
 */
Trait UserViewModelTrait
{

    use Settings;
    private $connector;

    /**
     * @param $id
     * @param string $inputName
     * @return array
     */
    public function saveUserAvatar($id, $inputName = 'avatar')
    {
        $avatarPath = '/user/';
        $hash = $this->getUserHash($id);
        $userPath = $this->getUserFolder($hash);
        $libUploader = new UploaderLibrary($inputName, $avatarPath, $userPath, 'image');
        $result = $libUploader->setFileName($hash . '_avatar')
            ->setStorage($libUploader::STORAGE_NFS_PUBLIC)
            ->setMaxImageSize(800, 800)
            ->setThumbNailImageSize(200, NULL)
            ->setConvertFormat('jpg', 75)
            ->createThumbnail(true)
            ->resizeUpload();
        if ($result['action']) {
            return $result['items']['src'].'?v='.time();
        }
        return false;
    }

    /**
     * @param $id
     * @return string
     */
    public function getUserHash($id)
    {
        return md5('user_' . $id);
    }

    /**
     * @param $hash
     * @return string
     */
    public function getUserFolder($hash)
    {
        $f1 = substr($hash, 0, 2);
        $f2 = substr($hash, 2, 2);
        $path = $f1 . DIRECTORY_SEPARATOR . $f2 . DIRECTORY_SEPARATOR . $hash;
        return $path;
    }


    public function getConnectorLabel($onLogin = false, $instance_id = null)
    {
        $connectors = $this->getConnectors($instance_id);
        $label = '';
        foreach ($connectors as $index => $name) {
            $or = '';
            if ($index > 0) {
                $or = trans('user::form.fields.or');
            }
            $label .= $or . trans('user::form.fields.' . $name);
        }
        return $label;
    }


    public function setConnectorByRequest($instance_id = null)
    {
        $default = false;
        $connectors = $this->getConnectors($instance_id);
        foreach ($connectors as $cName) {
            $connector = "\\Modules\\User\\Libraries\\Connector\\" . ucfirst($cName) . "Connector";
            $connector = new $connector();
            if ($connector->useThisConnector($this->request)) {
                $this->connector = $connector;
                break;
            }
        }
        if (!$this->connector) {
            $default = $this->getDefaultConnector();
            $connector = "\\Modules\\User\\Libraries\\Connector\\" . ucfirst($default) . "Connector";
            $this->connector = new $connector();
        }
        $this->connector->setInfo($this->getConnectorFromDb());
        return $this;
    }


    public function getConnectorFromDb($name = null)
    {
        if (is_null($name)) {
            $name = $this->connector->getName();
        }
        $key = "connector_list_name_" . $name;
        $connector = false;
        if (!($connector = Cache::get($key))) {
            $connector = Connector::where('name', $name)->first();
            if ($connector) {
                $connector = (object)$connector->toArray();
                Cache::forever($key, $connector);
            }
        }
        return $connector;
    }


    public function getConnectors($instance_id)
    {
        $connectors = $this->setInstanceId($instance_id)->setSettings('user', 'instance')->getSetting('connector');
        if (empty($connectors)) {
            $connectors = [$this->getDefaultConnector()];
        }
        return $connectors;
    }


    public function getDefaultConnector()
    {
        return 'email';
    }

    public function getCurrentConnector()
    {
        return $this->connector;
    }


}