<?php

namespace Modules\User\Traits\User;

use DateTime;
use Modules\Core\Models\Location\Location;
use Modules\Core\Models\Meta\MetaGroup;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Course\Models\Enrollment\CourseUserEnrollment;
use Modules\User\Models\Credit;
use Modules\User\Models\Meta\UserMetaGroup;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\User\Models\Role;
use Illuminate\Support\Facades\Cache;
use Modules\User\Models\User;
use Modules\User\Models\User\UserRole;

/**
 * Trait UserTrait
 * @package Modules\User\Traits\User
 */
Trait UserTrait
{

    /**
     * @var array
     */
    private $filters = [];
    /**
     * @var string
     */
    private $baseUserPath = '/storage';
    private $userCurrentRole = false;

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsConfirm($query)
    {
        $query = $query->where('confirm', 1);
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterUser($query)
    {
        $q = isset($this->requestItems['q']) ? $this->requestItems['q'] : null;
        $query = $query->where(function ($query) use ($q) {

            if ($q) {
                $query->orWhere('username', 'LIKE', '%' . $q . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $q . '%');
                $query->orWhere('first_name', 'LIKE', '%' . $q . '%');
            }

        })
            ->active();
//            ->filterCurrentInstance();

        return $query;
    }


    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getSmallAvatarUrlAttribute()
    {
        if (empty($this->avatar)) {
            return asset('/storage/user/avatars/avatar.png');
        }
        $last = '_thumbnail' . substr($this->avatar, strrpos($this->avatar, '.'));
        $avatar = substr($this->avatar, 0, strrpos($this->avatar, '.')) . $last;
        return $avatar;
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getAvatarUrlAttribute()
    {
        if (empty($this->avatar)) {
            return asset('/storage/user/avatars/avatar.png');
        }
        return url($this->baseUserPath . $this->avatar);
    }

    /**
     * @param $value
     * @return \Illuminate\Contracts\Routing\UrlGenerator|mixed|string
     */
    public function getAvatarAttribute($value)
    {
        if (empty($value)) {
            return asset('/storage/user/avatars/avatar.png');
        }
        return asset($this->baseUserPath . $value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getBirthdayByFormatAttribute()
    {
        $value = $this->birthday;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.date_medium'));
        }
        return $value;
    }

    /**
     * @return null
     */
    public function getStateAttribute()
    {
        $value = $this->state_id;
        if ($value) {
            $value = Location::find($value);
            $value = $value ? $value->name : null;
        }
        return $value;
    }

    /**
     * @return null
     */
    public function getCityAttribute()
    {
        $value = $this->city_id;
        if ($value) {
            $value = Location::find($value);
            $value = $value ? $value->name : null;
        }
        return $value;
    }

    /**
     * @return null
     */
    public function getCountryAttribute()
    {
        $value = $this->country_id;
        if ($value) {
            $value = Location::find($value);
            $value = $value ? $value->name : null;
        }
        return $value;
    }

    /**
     * add default meta to user , for example : social meta
     * @return bool
     */
    public function addDefaultMeta()
    {
        $user_id = $this->id;
        $defaultMetaGroup = MetaGroup::where("default", 1)->where("system", User::SYSTEM_NAME)->get();
        $defaultMetaGroup = $defaultMetaGroup ? $defaultMetaGroup->pluck("id")->toArray() : [];
        //$requestMetaGroup = array_map('intval', $this->request->get('meta_group_ids', []));
        $metaGroupIds = array_unique($defaultMetaGroup);
        return $this->saveMetaGroups($user_id, $metaGroupIds);
    }

    /**
     * @param $user_id
     * @param $meta_group_ids
     * @return bool
     */
    protected function saveMetaGroups($user_id, $meta_group_ids)
    {
        $cmgModel = new UserMetaGroup();
        $mgIds = $cmgModel->where('user_id', $user_id)->pluck('meta_group_id')->toArray();
        $cmgIdsDelete = array_diff($meta_group_ids, $mgIds);

        if ($cmgIdsDelete) {
            UserMetaGroup::whereIn('id', $cmgIdsDelete)->where('user_id', $user_id)->delete();
        }

        if (empty($meta_group_ids) || !$user_id || ($mgIds == $meta_group_ids)) {
            return false;
        }

        $insertItems = [];
        foreach ($meta_group_ids as $meta_group_id) {
            $insertItems[] = ['meta_group_id' => $meta_group_id, 'user_id' => $user_id];
        }

        $result = UserMetaGroup::insert($insertItems);
        return $result;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getMaritalTextAttribute()
    {
        return $this->gender ? trans('user::data.marital.' . $this->marital) : null;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getGenderTextAttribute()
    {
        return $this->gender ? trans('user::data.gender.' . $this->gender) : null;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getGenderKeyAttribute()
    {
        return $this->gender ? trans('user::data.gender_key.' . $this->gender) : null;
    }

    /**
     * @return int|null
     */
    public function getAgeAttribute()
    {

        $date = $this->birthday;
        if ($date){
            $dob = new DateTime($date);
            $now = new DateTime();
            $difference = $now->diff($dob);
            $age = $difference->y;
            return $age;
        }
        return null;
    }


    /*relation*/
    /**
     * @return mixed
     */
    public function userConnectors()
    {
        return $this->hasMany('Modules\User\Models\Register\UserRegisterKey');
    }

    /**
     * @return mixed
     */
    public function roles()
    {
        return $this->hasMany('Modules\User\Models\User\UserRole');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contacts()
    {
        return $this->hasMany('Modules\User\Models\UserContact\UserContact')->active();
    }


    /**
     * @param $query
     * @param $params
     * @return mixed
     */
    public function scopeFilterByValue($query, $params)
    {
        if (isset($params['mobile'], $params['mobile_connector_id'])) {
            $query = $query->whereHas('userConnectors', function ($q) use ($params) {
                $q->where('value', 'LIKE', '%' . $params['mobile'] . '%')->where('connector_id', $params['mobile_connector_id']);
            });
        }
        if (isset($params['email'], $params['email_connector_id'])) {
            $query = $query->whereHas('userConnectors', function ($q) use ($params) {
                $q->where('value', 'LIKE', '%' . $params['email'] . '%')->where('connector_id', $params['email_connector_id']);
            });
        }
        return $query;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseUserEnrollments()
    {
        return $this->hasMany('Modules\Course\Models\Enrollment\CourseUserEnrollment')->where('type', CourseUserEnrollment::TYPE_ENROLLMENT)->active();
    }

    /**
     * @return mixed
     */
    public function courseUserActivities()
    {
        return $this->hasMany('Modules\Course\Models\Enrollment\CourseUserEnrollment')->where('type', CourseUserEnrollment::TYPE_ACTIVITY)->active();
    }

    /**
     * @return bool|UserRole|null
     */
    public function getCurrentRole()
    {
        if (!$this->id) {
            return null;
        }
        if ($this->userCurrentRole) {
            return $this->userCurrentRole;
        }
        $instance = app('getInstanceObject')->getInstance();
        $userCurrentRole = $this->getUserInstanceRoles($instance->id);
        if ($instance->name == config('app.admin')) {
            $roles = $userCurrentRole->where('is_admin', 1);
            $this->userCurrentRole = $this->getCurrentAdminRole($roles);
        } else {
            $roles = $userCurrentRole->where('is_admin', 0);
            $this->userCurrentRole = $this->getCurrentUserRole($roles, $instance);
        }
        return $this->userCurrentRole;
    }

    /**
     * @param $instance_id
     * @return string
     */
    private function getUserRoleInstanceCacheKey($instance_id)
    {
        return 'Instance_User_' . $this->id . '_Roles_' . (int)$this->instance_id . '_' . $instance_id;
    }

    /**
     * @param $instance_id
     * @return mixed
     */
    public function getUserInstanceRoles($instance_id)
    {
        $key = $this->getUserRoleInstanceCacheKey($instance_id);
        if (true || !($userCurrentRole = Cache::get($key))) {
            $adminRoles = $this->getAdminRole();
            $userCurrentRole = $this->roles()->get();
            foreach ($userCurrentRole as $userRole) {
                if (isset($adminRoles[$userRole->role_id])) {
                    $userRole->is_admin = 1;
                } else {
                    $userRole->is_admin = 0;
                }
            }
            Cache::forever($key, $userCurrentRole);
        }
        return $userCurrentRole;
    }


    /**
     * @return array
     */
    public function getAdminRole()
    {
        $user_id = $this->id;
        $key = 'Instance_Admin_Roles_' . $user_id;
        if (!($list = Cache::get($key))) {
            $roles = Role::/*whereHas('userRole', function ($q) use ($user_id) {
                $q->where('user_id', $user_id);
            })->*/
            where('is_admin', 1)->get();
            $list = [];
            foreach ($roles as $role) {
                $list[$role->id] = $role->name;
            }
            Cache::forever($key, $list);
        }
        return $list;
    }


    /**
     * @param $roles
     * @return bool
     */
    public function getCurrentAdminRole($roles)
    {
        if (!count($roles)) {
            return false;
        }
        return $roles->first();
    }

    /**
     * @param $roles
     * @param $instance
     * @return UserRole
     */
    public function getCurrentUserRole($roles, $instance)
    {
        if (!count($roles)) {
            $role = $this->getInstanceDefaultRole($instance->id);
            $userRole = new UserRole;
            $userRole->instance_id = $instance->id;
            $userRole->role_id = $role->id;
            $userRole->user_id = $this->id;
            $userRole->save();
            Cache::forget($this->getUserRoleInstanceCacheKey($instance->id));
            return $userRole;
        }
        return $roles->first();
    }


    /**
     * @param $instance_id
     * @return object
     */
    private function getInstanceDefaultRole($instance_id)
    {

        $key = 'Instance_User_Default_Roles_' . $instance_id;
        if (!($role = Cache::get($key))) {
            $role = Role::where(function ($query) use ($instance_id) {
                $query->whereNull('instance_id')
                    ->orWhere('instance_id', '=', $instance_id);
            })->where('type', 'site')->where('is_admin', 0)->where('default', 1)->first();
            $role = (object)$role->toArray();
            Cache::forever($key, $role);
        }
        return $role;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function metaGroups()
    {
        return $this->belongsToMany('Modules\Core\Models\Meta\MetaGroup', 'user_meta_groups');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userMetaGroups()
    {
        return $this->hasMany('Modules\User\Models\Meta\UserMetaGroup');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formRecords()
    {
        return $this->hasMany('Modules\Core\Models\Form\FormRecord');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kanoonCenterUsers()
    {
        return $this->hasMany('Modules\Kanoon\Models\Center\KanoonCenterUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function passwordResets()
    {
        return $this->hasMany('Modules\User\Models\PasswordReset');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function teachers()
    {
        return $this->hasMany('Modules\Course\Models\Teacher\Teacher');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userContacts()
    {
        return $this->hasMany('Modules\User\Models\UserContact\UserContact');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userCredits()
    {
        return $this->hasMany(Credit::class)->orderBy('id', 'DESC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userCredit()
    {
        return $this->hasOne(Credit::class)->orderBy('id', 'DESC');

    }


}