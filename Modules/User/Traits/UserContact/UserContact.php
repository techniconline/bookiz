<?php

namespace Modules\User\Traits\UserContact;


trait UserContact
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Models\User');
    }
}
