<?php

namespace Modules\User\Traits\Meta;

trait UserMetaGroup
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [

        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function metaGroup()
    {
        return $this->belongsTo('Modules\Core\Models\Meta\MetaGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Models\User');
    }

    /** //TODO test
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function featureValueRelations()
    {
        return $this->hasMany('Modules\Core\Models\Feature\FeatureValueRelation', 'item_id')
            ->where('system', $this->getTable())->active()->filterLanguage();
    }

}
