<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 7/25/18
 * Time: 3:40 PM
 */
namespace Modules\User\Traits\Role;

/**
 * Trait RolePermissionTrait
 * @package Modules\User\Traits\Role
 */
Trait RoleTrait
{
    public static function getInstanceRoles($name,$instance_id){
        return self::select('id','name','title')->where('type',$name)->where(function ($query) use($instance_id) {
                    $query->whereNull('instance_id')->orWhere('instance_id', '=', $instance_id);
                })->get();
    }

    public static function getInstanceAdminRoles($name,$instance_id){
        return self::select('id','name','title')->where('type',$name)->where('is_admin',1)->where(function ($query) use($instance_id) {
            $query->Where('instance_id', '=', $instance_id);
        })->get();
    }

}