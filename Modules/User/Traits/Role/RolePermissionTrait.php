<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 7/25/18
 * Time: 3:40 PM
 */
namespace Modules\User\Traits\Role;

/**
 * Trait RolePermissionTrait
 * @package Modules\User\Traits\Role
 */
Trait RolePermissionTrait
{

    public function permission()
    {
        return $this->belongsTo('Modules\User\Models\Permission');
    }

    public function role()
    {
        return $this->belongsTo('Modules\User\Models\Role');
    }


    /**
     * @return mixed
     */
    public function getPermissionsAttribute()
    {
        return json_decode($this->attributes['permissions']);
    }

    /**
     * @param $val
     */
    public function setPermissionsAttribute($val )
    {
        if(!is_array($val) && !is_object($val)){
            $val=explode( ', ',  $val);
        }
        $this->attributes['permissions'] = json_encode( $val );
    }


    /**
     * @param $role_id
     * @return array|\Illuminate\Cache\CacheManager|mixed
     * @throws \Exception
     */
    public static function getAllPermissionsByRole($role_id){
        $key='AllPermissionsByRole_role_id_'.$role_id;
        $items=cache($key);
        if(!$items){
            $rolePermission=Self::with('role')->Where('role_id',$role_id)->first();
            if($rolePermission){
                $list=[];
                if($rolePermission->role && $rolePermission->role->is_admin){
                    $list[0]=(object)['permission_id'=>0,'role_id'=>$role_id,'permission'=>'admin'];
                }
                $permissions=collect($rolePermission->permissions);
                foreach ($permissions as $key=>$permission) {
                    foreach ($permission as $permission_id=>$value){
                        $permission_key=$key.'/'.$value;
                        $list[$permission_id]=(object)['permission_id'=>$permission_id,'role_id'=>$role_id,'permission'=>$permission_key];
                    };
                }
                $items=collect($list);
            }else{
                $items=collect([]);

            }
            cache([$key=>$items],1440);
        }
        return $items;
    }

}