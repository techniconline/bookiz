<?php

namespace Modules\User\Traits\Credit;

use Modules\User\Models\Credit;

trait CreditTrait
{

    private $user_id;
    private $price;
    private $error;
    private $credit;
    private $model_id;
    private $model_type;
    private $instance_id;
    private $current_credit;
    private $type_credit;
    public $is_valid = true;

    public function initCredit($price, $user_id, $model_type, $model_id, $type_credit = Credit::TYPE_PLUS, $instance_id = null)
    {

        if ($price && $user_id && $model_id && $model_type) {
            $this->user_id = $user_id;
            $this->price = $price;
            $this->model_id = $model_id;
            $this->model_type = $model_type;
            $this->type_credit = $type_credit;
            $this->instance_id = $instance_id ?: app('getInstanceObject')->getCurrentInstanceId();
            $this->setCredit()->addCreditToUser();
        } else {
            $this->is_valid = false;
        }
        return $this;
    }

    /**
     * @return $this
     */
    private function setCredit()
    {
//        $this->credit = Credit::active()->filterCurrentInstance($this->instance_id)->where('user_id', $this->user_id)->orderBy('id', 'DESC')->first();
//        $this->current_credit = $this->credit ? $this->credit->current_credit : 0;
        return $this;
    }

    /**
     * @return $this
     */
    private function addCreditToUser()
    {
        $model = new Credit();

//        if ($this->type_credit == Credit::TYPE_PLUS) {
//            $this->current_credit = $this->current_credit + $this->price;
//        } else {
//            $this->current_credit = $this->current_credit - $this->price;
//        }

        $data = [
            'user_id' => $this->user_id,
            'instance_id' => $this->instance_id,
            'model_id' => $this->model_id,
            'model_type' => $this->model_type,
            'type' => $this->type_credit,
            'amount' => $this->price,
            'current_credit' => $this->current_credit,
        ];

        if ($model->fill($data)->save()) {
            return $this;
        }
        $this->is_valid = false;
        return $this;
    }

}
