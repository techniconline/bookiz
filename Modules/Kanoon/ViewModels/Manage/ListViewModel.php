<?php

namespace Modules\Kanoon\ViewModels\Manage;

use Illuminate\Support\Facades\DB;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Kanoon\Models\Center\KanoonCenter;
use Modules\Kanoon\Models\Center\KanoonCenterUser;
use Modules\Kanoon\Traits\Manager\CenterManager;
use Modules\User\Models\Connector;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;
    use CenterManager;

    private $all_centers=[];

    public function setGridModel()
    {
        $mobile=$this->getFilterValueFromRequest('mobile');
        $email=$this->getFilterValueFromRequest('email');
        $first_name=$this->getFilterValueFromRequest('first_name');
        $last_name=$this->getFilterValueFromRequest('last_name');
        $params=[];
        if($mobile){
            $connector=$this->getConnectorByName('mobile');
            $params['mobile']=$mobile;
            $params['mobile_connector_id']=$connector->id;
        }
        if($email){
            $connector=$this->getConnectorByName('email');
            $params['email']=$email;
            $params['email_connector_id']=$connector->id;
        }

        if($first_name){
            $params['first_name']=$first_name;
        }

        if($last_name){
            $params['last_name']=$last_name;
        }


        if($centerManager=$this->isCenterManager()){
            $this->Model = KanoonCenterUser::with('user','userConnectors')->where('center_id',$centerManager->center_id)->filterByValue($params);
        }else{
            $this->Model = KanoonCenterUser::with('user','userConnectors')->filterByValue($params);
        }

    }

    private function generateGridList()
    {
        $this->all_centers=KanoonCenter::getAllCenters();
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('image', trans('user::form.fields.avatar'), true)
             ->addColumn('first_name', trans('user::form.fields.first_name'), false)
             ->addColumn('last_name', trans('user::form.fields.last_name'), false)
             ->addColumn('national_code', trans('user::form.fields.national_code'), false)
             ->addColumn('confirm', trans('user::form.fields.confirm'), false)
             ->addColumn('connector', trans('user::form.fields.connector'), false)
             ->addColumn('center_id', trans('kanoon::form.fields.center'), true);

        /**/
        $edit = array(
            'name' => 'user.manage.edit',
            'parameter' => ['user_id']
        );
        $this->addAction('edit', $edit, trans('user::routes.user.manage_edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $connector = array(
            'name' => 'user.manage.connector',
            'parameter' => ['user_id']
        );
        $this->addAction('connector', $connector, trans('user::routes.user.manage_connector'), 'fa  fa-connectdevelop', false, ['target' => '', 'class' => 'btn btn-sm btn-danger']);

        $password = array(
            'name' => 'user.manage.password',
            'parameter' => ['user_id']
        );
        $this->addAction('password', $password, trans('user::routes.user.change_password'), 'fa  fa-credit-card', false, ['target' => '', 'class' => 'btn btn-sm btn-info']);

        $course = array(
            'name' => 'kanoon.user.courses',
            'parameter' => ['id']
        );
        $this->addAction('course', $course, trans('user::routes.user.user_courses'), 'fa fa-file', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);


        $this->action_text = false;

        /*filters*/
        $this->addFilter('first_name','text',['process'=>false]);
        $this->addFilter('last_name','text',['process'=>false]);
        $this->addFilter('mobile','text',['process'=>false]);
        $this->addFilter('email','text',['process'=>false]);
        if(!$this->isCenterManager()){
            $this->addFilter('center','select',['title'=>trans('kanoon::form.fields.center'),'options'=>$this->all_centers,'field'=>'center_id']);
        }

        $this->can_export=1;
        $this->setExportFileName('users_center');

        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->image=view('core::component.avatar',['class'=>'avatar_small','avatar'=>$row->user->small_avatar_url,'url'=>$row->user->avatar_url]);
        $row->first_name=$row->user->first_name;
        $row->last_name=$row->user->last_name;
        $row->national_code=$row->user->national_code;
        $row->confirm=view('core::component.yesno',['value'=>$row->user->confirm]);
        $row->connector=view('user::component.connectors',['userConnectors'=>$row->userConnectors]);
        $row->center_id=isset($this->all_centers[$row->center_id])?$this->all_centers[$row->center_id]:'-';
        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getList()
    {
        $this->setTitlePage(trans('user::menu.manage.users'));
        $this->generateGridList()->renderedView("kanoon::manage.user.list", ['view_model' => $this], trans('user::menu.manage.users'));
        return $this;
    }


    public function getConnectorByName($name){
        return Connector::where('name',$name)->first();
    }


    public function getExportHeader(){
        $header=[
            'id'=>"شناسه",
            'national_code'=>"کد ملی",
            'first_name'=>"نام",
            'last_name'=>"نام خانوادگی",
            'center_id'=>"شناسه آموزشگاه",
            'center'=>"آموزشگاه",
            'courses'=>"رشته های ثبت نامی",
        ];
        return $header;
    }

    public function getExportRow($row)
    {
        $courses=DB::select("SELECT GROUP_CONCAT(courses.title SEPARATOR ',') as `courses_title` FROM `course_user_enrollments` inner join courses on courses.id=course_user_enrollments.course_id where course_user_enrollments.user_id='".$row->user->id."' group by course_user_enrollments.user_id");
        $cNames='-';
        if(count($courses)){
            $cNames=$courses[0]->courses_title;
        }
        $center_name=isset($this->all_centers[$row->center_id])?$this->all_centers[$row->center_id]:'-';
        $csvRow = [
            'id'=>$row->id,
            'national_code'=>(empty($row->user->national_code)?'-':$row->user->national_code),
            'first_name'=>$row->user->first_name,
            'last_name'=>$row->user->last_name,
            'center_id'=>$row->center_id,
            'center'=>$center_name,
            'courses'=> $cNames,
        ];
        return $csvRow;
    }


}
