<?php
namespace Modules\Kanoon\ViewModels\Manage\Center;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Kanoon\Models\Center\KanoonCenter;

class CenterViewModel extends BaseViewModel
{
    public function getForm(){
        $this->boot();
        $BlockViewModel=&$this;
        return $this->renderedView('kanoon::manage.center.form',compact('BlockViewModel'));
    }


    public function saveForm(){
        $this->boot();
        $rules=[
            'title'=>['required','string','min:2','max:150'],
            'country_id'=>['required','integer','exists:locations,id'],
            'state_id'=>['required','integer','exists:locations,id'],
            'city_id'=>['required','integer','exists:locations,id'],
        ];
        $this->request->validate($rules);
        $data=$this->request->all();
        if(!$this->modelData){
            $this->modelData=new KanoonCenter;
        }
        $this->modelData->title=$data['title'];
        if(!$this->modelData->instance_id){
            $currentInstance=app('getInstanceObject')->getCurrentInstance();
            $this->modelData->instance_id =$currentInstance->id;
        }

        $this->modelData->country_id=$data['country_id'];
        $this->modelData->state_id=$data['state_id'];
        $this->modelData->city_id=$data['city_id'];
        $this->modelData->active=$data['active'];
        $result=$this->modelData->save();
        if($result){
            return $this->redirect(route('kanoon.manage.center'))->setResponse(true,trans('kanoon::messages.save_success'));
        }
        return $this->redirectBack()->setResponse($result,trans('kanoon::messages.save_error'));
    }

    public function boot(){
        if($this->request->has('id')){
            $id=$this->request->get('id');
            if($id){
                $this->modelData=KanoonCenter::find($id);
                if(!$this->modelData){
                    abort(404,'Not Find Page!');
                }
                $this->setTitlePage(trans('kanoon::menu.titles.manage_edit_center'));
            }
        }else{
            $this->setTitlePage(trans('kanoon::menu.titles.manage_create_center'));
        }
    }

}
