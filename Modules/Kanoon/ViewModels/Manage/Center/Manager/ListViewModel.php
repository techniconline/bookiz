<?php

namespace Modules\Kanoon\ViewModels\Manage\Center\Manager;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Kanoon\Models\Center\KanoonCenter;
use Modules\Kanoon\Models\Center\KanoonCenterManager;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;

    private $all_centers=[];
    private $center_id=[];


    public function setGridModel()
    {
        $mobile=$this->getFilterValueFromRequest('mobile');
        $email=$this->getFilterValueFromRequest('email');
        $first_name=$this->getFilterValueFromRequest('first_name');
        $last_name=$this->getFilterValueFromRequest('last_name');
        $params=[];
        if($mobile){
            $connector=$this->getConnectorByName('mobile');
            $params['mobile']=$mobile;
            $params['mobile_connector_id']=$connector->id;
        }
        if($email){
            $connector=$this->getConnectorByName('email');
            $params['email']=$email;
            $params['email_connector_id']=$connector->id;
        }

        if($first_name){
            $params['first_name']=$first_name;
        }

        if($last_name){
            $params['last_name']=$last_name;
        }

        $this->Model = KanoonCenterManager::with('user','userConnectors','role')->where('center_id',$this->center_id)->filterByValue($params);
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('image', trans('user::form.fields.avatar'), true)
            ->addColumn('first_name', trans('user::form.fields.first_name'), false)
            ->addColumn('last_name', trans('user::form.fields.last_name'), false)
            ->addColumn('national_code', trans('user::form.fields.national_code'), false)
            ->addColumn('active', trans('core::form.fields.active'), false)
            ->addColumn('role', trans('user::form.fields.role'), false)
            ->addColumn('connector', trans('user::form.fields.connector'), false);

        /**/
        $editManger = array(
            'name' => 'kanoon.manage.center.manager.edit',
            'parameter' => ['id','center_id']
        );
        $this->addAction('editManger', $editManger, trans('kanoon::menu.titles.manage_center_edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);
        $deleteManger = array(
            'name' => 'kanoon.manage.center.manager.delete',
            'parameter' => ['id','center_id']
        );
        $this->addAction('deleteManger', $deleteManger, trans('kanoon::menu.titles.manage_center_delete'), 'fa fa-times', false, ['target' => '', 'class' => 'btn btn-sm btn-danger']);


        $edit = array(
            'name' => 'user.manage.edit',
            'parameter' => ['user_id']
        );
        $this->addAction('edit', $edit, trans('user::routes.user.manage_edit'), 'fa fa-user', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $connector = array(
            'name' => 'user.manage.connector',
            'parameter' => ['user_id']
        );
        $this->addAction('connector', $connector, trans('user::routes.user.manage_connector'), 'fa  fa-connectdevelop', false, ['target' => '', 'class' => 'btn btn-sm btn-danger']);

        $password = array(
            'name' => 'user.manage.password',
            'parameter' => ['user_id']
        );
        $this->addAction('password', $password, trans('user::routes.user.change_password'), 'fa  fa-credit-card', false, ['target' => '', 'class' => 'btn btn-sm btn-info']);


        $routeAdd=route('kanoon.manage.center.manager.edit',['center_id'=>$this->center_id,'id'=>0]);
        $options=['class'=>"btn btn-primary"];
        $this->addButton('add',$routeAdd,trans('kanoon::menu.titles.manage_center_add'),'fa fa-plus',false,'before',$options);



        $this->action_text = false;

        /*filters*/
        $this->addFilter('first_name','text',['process'=>false]);
        $this->addFilter('last_name','text',['process'=>false]);
        $this->addFilter('mobile','text',['process'=>false]);
        $this->addFilter('email','text',['process'=>false]);

        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->image=view('core::component.avatar',['class'=>'avatar_small','avatar'=>$row->user->small_avatar_url,'url'=>$row->user->avatar_url]);
        $row->first_name=$row->user->first_name;
        $row->last_name=$row->user->last_name;
        $row->national_code=$row->user->national_code;
        $row->role=$row->role->title;
        $row->active=view('core::component.yesno',['value'=>$row->active]);
        $row->connector=view('user::component.connectors',['userConnectors'=>$row->userConnectors]);
        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getList()
    {
        $this->all_centers=KanoonCenter::getAllCenters();
        $center_id=$this->center_id=$this->request->get('center_id');
        if(!$center_id){
           return $this->redirectBack()->setResponse(false,trans('Not_Find_Page'));
        }
        $centerName=isset($this->all_centers[$center_id])?$this->all_centers[$center_id]:'-';
        $this->setTitlePage(trans('kanoon::menu.titles.manage_edit_center_managers_name',['name'=>$centerName]));
        $this->generateGridList()->renderedView("kanoon::manage.center.manager.list", ['view_model' => $this], trans('kanoon::menu.titles.manage_edit_center_managers_name',['name'=>$centerName]));
        return $this;
    }



}
