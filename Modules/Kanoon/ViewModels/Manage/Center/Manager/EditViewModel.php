<?php

namespace Modules\Kanoon\ViewModels\Manage\Center\Manager;

use Illuminate\Support\Facades\DB;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Kanoon\Models\Center\KanoonCenterManager;
use Modules\Kanoon\Models\Center\KanoonCenter;
use Modules\Kanoon\Traits\Manager\CenterManager;
use Modules\User\Models\User\UserRole;

class EditViewModel extends BaseViewModel
{
    use CenterManager;
    private $center=false;

    public function boot()
    {
        $id=$this->requestValuesUpdate()->get('id');
        if($id){
            $this->modelData=KanoonCenterManager::find($id);
        }
        $center_id=$this->center_id=$this->request->get('center_id');
        if(!$center_id){
            abort(404,'NOt Find Page');
        }
        $this->center=KanoonCenter::find($center_id);
    }

    public function getFromRequest($name){
        if($this->request->has($name)){
            return $this->request->get($name);
        }
        return null;
    }

    public function getUrlSearchUser(){
        return route("user.manage.search_user");
    }
    public function getForm(){
        $this->boot();
        $BlockViewModel=&$this;
        if($this->modelData) {
            $this->setTitlePage(trans('kanoon::menu.titles.manage_center_edit'));
        }else{
            $this->setTitlePage(trans('kanoon::menu.titles.manage_center_add'));
        }
        return $this->renderedView('kanoon::manage.center.manager.form',compact('BlockViewModel'));
    }


    public function save(){
        $this->boot();
        $rules=[
            'user_id'=>['required'],
            'active'=>['required'],
            'center_id'=>['required'],
            'role_id'=>['required'],
        ];
        $this->request->validate($rules);
        $data=$this->request->all();
        if($this->isCenterManager($data['user_id'])){
            return $this->redirectBack()->setResponse(false,trans('kanoon::messages.user_manage_error'));
        }
        $instance=app('getInstanceObject')->getCurrentInstance();
        DB::beginTransaction();
        try{
            if(!($this->modelData instanceof KanoonCenterManager)){
                $this->modelData=new KanoonCenterManager;
            }
            $this->modelData->center_id=$data['center_id'];
            $this->modelData->user_id=$data['user_id'];
            $this->modelData->role_id=$data['role_id'];
            $this->modelData->active=$data['active'];
            $result=$this->modelData->save();

            $userRole=new UserRole;
            $userRole->instance_id=$instance->id;
            $userRole->role_id=$data['role_id'];
            $userRole->user_id=$this->modelData->user_id;
            $result=$userRole->save();
            DB::commit();
        }catch (Exception $e){
            DB::rollBack();
            $result=false;
        }
        if($result){
            return $this->redirect(route('kanoon.manage.center.managers',["center_id"=>$this->getFromRequest('center_id')]))->setResponse(true,trans('kanoon::messages.save_success'));
        }
        return $this->redirectBack()->setResponse($result,trans('kanoon::messages.save_error'));
    }

    public function getCenter($name=null){
        if(!$name){
            return $this->center;
        }
        if(isset($this->center->{$name})){
            return $this->center->{$name};
        }
        return null;
    }


    public function delete(){
        $this->boot();
        if($this->modelData){
            $result=$this->modelData->forceDelete();
        }
        if($result){
            return $this->redirect(route('kanoon.manage.center.managers',["center_id"=>$this->getFromRequest('center_id')]))->setResponse(true,trans('kanoon::messages.save_success'));
        }
        return $this->redirectBack()->setResponse($result,trans('kanoon::messages.save_error'));
    }

}
