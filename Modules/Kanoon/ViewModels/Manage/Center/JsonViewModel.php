<?php
namespace Modules\Kanoon\ViewModels\Manage\Center;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Kanoon\Models\Center\KanoonCenter;

class JsonViewModel extends BaseViewModel
{

    public function getCenterByApi(){
        $q=false;
        $selected=false;
        if($this->request->has('q')){
            $q=$this->request->get('q');
        }elseif($this->request->has('selected')){
            $inputs=explode(',',$this->request->get('selected'));
            $selected=[];
            foreach($inputs as $select){
                $select=trim($select);
                if(strlen($select)){
                    $selected[]=$select;
                }
            }
        }
        $model=KanoonCenter::select('id','title');
        if($q){
            $model->where('title', 'like', '%'.$q.'%');
            $items=$model->get();
        }elseif($selected && count($selected)){
            $model->whereIN('id', $selected);
            $items=$model->get();

        }else{
            $model=KanoonCenter::select('id','title')->orderBy('id','DESC')->take(10);
            $items=$model->get();
        }
        $items=$items->map(function ($item) {
            $item->text=$item->title;
            return $item;
        });
        return $this->setDataResponse($items)->setResponse(true) ;
    }


}
