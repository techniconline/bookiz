<?php

namespace Modules\Kanoon\ViewModels\Manage\Center\Course;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Kanoon\Models\Center\KanoonCenter;
use Modules\Kanoon\Models\Center\KanoonCenterCourse;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;

    private $all_centers=[];
    private $center_id=[];


    public function setGridModel()
    {
        $name=$this->getFilterValueFromRequest('name');
        $params=[];
        if($name){
            $params['name']=$name;
        }

        $this->Model = KanoonCenterCourse::with('course')->where('center_id',$this->center_id)->filterByValue($params);
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('core::form.fields.id'), true)
            ->addColumn('title', trans('core::form.fields.title'), false)
            ->addColumn('code', trans('core::form.fields.code'), false)
            ->addColumn('active', trans('core::form.fields.active'), false);

        /**/
        $editManger = array(
            'name' => 'kanoon.manage.center.courses.edit',
            'parameter' => ['id','center_id']
        );
        $this->addAction('editManger', $editManger, trans('kanoon::menu.titles.manage_center_course_edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);



        $routeAdd=route('kanoon.manage.center.courses.edit',['center_id'=>$this->center_id,'id'=>0]);
        $options=['class'=>"btn btn-primary"];
        $this->addButton('add',$routeAdd,trans('kanoon::menu.titles.manage_center_course_add'),'fa fa-plus',false,'before',$options);



        $this->action_text = false;

        /*filters*/
        $this->addFilter('name','text',['process'=>false]);

        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->code=$row->course->code;
        $row->title=$row->course->title;
        $row->active=view('core::component.yesno',['value'=>$row->active]);
        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getList()
    {
        $this->all_centers=KanoonCenter::getAllCenters();
        $center_id=$this->center_id=$this->request->get('center_id');
        if(!$center_id){
           return $this->redirectBack()->setResponse(false,trans('Not_Find_Page'));
        }
        $centerName=isset($this->all_centers[$center_id])?$this->all_centers[$center_id]:'-';
        $this->setTitlePage(trans('kanoon::menu.titles.manage_edit_center_managers_name',['name'=>$centerName]));
        $this->generateGridList()->renderedView("kanoon::manage.center.manager.list", ['view_model' => $this], trans('kanoon::menu.titles.manage_edit_center_managers_name',['name'=>$centerName]));
        return $this;
    }



}
