<?php

namespace Modules\Kanoon\ViewModels\Manage\Center\Course;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\Kanoon\Models\Center\KanoonCenterCourse;
use Modules\Kanoon\Models\Center\KanoonCenter;

class EditViewModel extends BaseViewModel
{
    private $center=false;

    public function boot()
    {
        $id=$this->requestValuesUpdate()->get('id');
        if($id){
            $this->modelData=KanoonCenterCourse::find($id);
        }
        $center_id=$this->center_id=$this->request->get('center_id');
        if(!$center_id){
            abort(404,'NOt Find Page');
        }
        $this->center=KanoonCenter::find($center_id);
    }

    public function getFromRequest($name){
        if($this->request->has($name)){
            return $this->request->get($name);
        }
        return null;
    }

    public function getUrlSearchCourse(){
        return route("course.json");
    }
    public function getForm(){
        $this->boot();
        $BlockViewModel=&$this;
        if($this->modelData) {
            $this->setTitlePage(trans('kanoon::menu.titles.manage_center_course_edit'));
        }else{
            $this->setTitlePage(trans('kanoon::menu.titles.manage_center_course_add'));
        }
        return $this->renderedView('kanoon::manage.center.course.form',compact('BlockViewModel'));
    }


    public function save(){
        $this->boot();
        $rules=[
            'course_id'=>['required'],
            'active'=>['required'],
            'center_id'=>['required'],
        ];
        $this->request->validate($rules);
        $data=$this->request->all();
        if(!$this->modelData){
            $this->modelData=new KanoonCenterCourse;
        }
        $this->modelData->center_id=$data['center_id'];
        $this->modelData->course_id=$data['course_id'];
        $this->modelData->active=$data['active'];
        $result=$this->modelData->save();
        if($result){
            return $this->redirect(route('kanoon.manage.center.courses',["center_id"=>$this->getFromRequest('center_id')]))->setResponse(true,trans('kanoon::messages.save_success'));
        }
        return $this->redirectBack()->setResponse($result,trans('kanoon::messages.save_error'));
    }

    public function getCenter($name=null){
        if(!$name){
            return $this->center;
        }
        if(isset($this->center->{$name})){
            return $this->center->{$name};
        }
        return null;
    }


}
