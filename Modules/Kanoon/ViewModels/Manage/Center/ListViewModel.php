<?php

namespace Modules\Kanoon\ViewModels\Manage\Center;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Kanoon\Models\Center\KanoonCenter;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;

    private $all_centers=[];

    public function setGridModel()
    {
        $this->Model = KanoonCenter::with('instance');
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('kanoon::form.fields.id'), true)
             ->addColumn('instance_id', trans('kanoon::form.fields.instance'), true)
             ->addColumn('title', trans('kanoon::form.fields.title'), true)
             ->addColumn('country_id', trans('kanoon::form.fields.country_id'), true)
             ->addColumn('state_id', trans('kanoon::form.fields.state_id'), true)
             ->addColumn('city_id', trans('kanoon::form.fields.city_id'), true)
             ->addColumn('active', trans('kanoon::form.fields.active'), true);


        $edit = array(
            'name' => 'kanoon.manage.center.edit',
            'parameter' => ['id']
        );
        $this->addAction('edit', $edit, trans('kanoon::menu.titles.manage_edit_center'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $managers = array(
            'name' => 'kanoon.manage.center.managers',
            'parameter' => ['id']
        );
        $this->addAction('managers',$managers, trans('kanoon::menu.titles.manage_edit_center_managers'), 'fa fa-users', false, ['target' => '', 'class' => 'btn btn-sm btn-info']);


        $course = array(
            'name' => 'kanoon.manage.center.courses',
            'parameter' => ['id']
        );
        $this->addAction('course',$course, trans('kanoon::menu.titles.manage_edit_center_courses'), 'fa fa-file', false, ['target' => '', 'class' => 'btn btn-sm btn-warning']);



        $routeAdd=route('kanoon.manage.center.create');
        $options=['class'=>"btn btn-primary",'target'=>"_blank"];
        $this->addButton('add',$routeAdd,trans('kanoon::form.fields.create_center'),'fa fa-plus',false,'before',$options);


        /*filters*/
        $this->addFilter('title','text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->active=view('core::component.yesno',['value'=>$row->active]);
        $row->country_id=BridgeHelper::getLocationHelper()->getLocationName($row->country_id);
        $row->state_id=BridgeHelper::getLocationHelper()->getLocationName($row->state_id);
        $row->city_id=BridgeHelper::getLocationHelper()->getLocationName($row->city_id);
        $row->active=view('core::component.yesno',['value'=>$row->active]);
        $row->instance_id=$row->instance->name;
        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getList()
    {
        $this->setTitlePage(trans('kanoon::menu.titles.manage_centers'));
        $this->generateGridList()->renderedView("kanoon::manage.center.list", ['view_model' => $this], trans('kanoon::menu.titles.manage_centers'));
        return $this;
    }



}
