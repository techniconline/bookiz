<?php
namespace Modules\Kanoon\ViewModels\Manage;

use BridgeHelper;
use DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Kanoon\Traits\Manager\CenterManager;
use Modules\User\Models\Register;
use Modules\User\Rules\ConnectorRule;
use Modules\User\Traits\User\UserViewModelTrait;
use Modules\User\Models\User;
use Modules\Course\Models\Enrollment\CourseUserEnrollment;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Enrollment;
use Modules\Kanoon\Models\Center\KanoonCenterUser;

class AddUserViewModel extends BaseViewModel
{
    use UserViewModelTrait;
    use CenterManager;

    private $messages=[];

    public function getForm(){
        $BlockViewModel =& $this;
        $this->setTitlePage(trans('kanoon::form.titles.add_user_to_course'));
        return $this->renderedView('kanoon::manage.add_user_to_course',compact('BlockViewModel'));
    }


    public function getAjaxUserData(){
        return route('kanoon.manage.user.info');
    }

    public function saveUser(){
        $rules=[
            'center_id'=>['required','integer','exists:kanoon_centers,id'],
            'first_name'=>['required','string','max:50'],
            'last_name'=>['required','string','max:50'],
            'role_id'=>['required','integer'],
            'national_code'=>['required','string','min:6','max:50'],
            'courses'=>['required','array'],
            "courses.*"  => "required",
        ];
        $this->request->validate($rules);
        $user_id=$this->request->get('user_id');
        if($user_id){
            $user=User::find($user_id);
        }else{
            $connector=$this->setConnectorByRequest()->getCurrentConnector();
            $messages=$connector->getMessage();
            $connecterRule=$connector->getRule();
            $connecterRule[]=new ConnectorRule($connector->getId());
            $rules=[
                'first_name'=>['required','string','max:50'],
                'last_name'=>['required','string','max:50'],
                'password'=>['required','string','min:6','max:50'],
                'connector'=>$connecterRule,
            ];
            $this->request->validate($rules,$messages);
            $register=new Register();
            $user=$register->registerUser($this->request,$connector,1);
        }
        if(!$user){
            return $this->redirectBack()->setResponse(false,trans('user::message.action.register_error'));
        }
        $center_id=$this->request->get('center_id',0);
        if($centerManager=$this->isCenterManager()){
            $center_id=$centerManager->center_id;
        }
        $center_user=KanoonCenterUser::where('user_id',$user->id)->where('center_id',$center_id)->first();
        if(!$center_user){
            $center_user=new KanoonCenterUser;
            $center_user->user_id=$user->id;
            $center_user->center_id=$center_id;
            $center_user->active=1;
            $center_user->save();
        }
        if(!$center_user || !$center_user->id){
            return $this->redirectBack()->setResponse(false,trans('kanoon::message.register_center_error'));
        }
        $instance_id=app('getInstanceObject')->getCurrentInstanceId();
        $courses=$this->request->get('courses');
        $messages=[];
        foreach($courses as $course_id){
            $course=Course::where('id',$course_id)->where('instance_id',$instance_id)->first();
            if($course){
                $userEnrollment=CourseUserEnrollment::where('user_id',$user->id)->where('course_id',$course->id)->first();
                if($userEnrollment){
                    $messages[]=trans('kanoon::messages.is_enrolled',['username'=>$user->full_name,'title'=>$course->title]);
                }else{
                    $result=$this->enrollUserOnCourse($user->id,$course->id,$this->request->get('role_id'));
                    if($result){
                        $messages[]=trans('kanoon::messages.is_enroll_success',['username'=>$user->full_name,'title'=>$course->title]);
                    }else{
                        $messages[]=trans('kanoon::messages.is_enroll_error',['username'=>$user->full_name,'title'=>$course->title]);
                    }
                }
            }else{
                $messages[]=trans('course::messages.course_not_find',['id'=>$course_id]);
            }
        }
        $this->messages=$messages;
        $data=[];
        foreach ($this->request->all() as $name=>$value){
            $data[$name]=null;
        }
        $this->request->merge($data);
        return $this->getForm();
    }

    public function getMessages(){
        return $this->messages;
    }

    public function getUser(){
        $data=$this->request->get('data');
        $national_code=false;
        if(!empty($data)){
            $data=json_decode($data);
            if(isset($data->national_code)){
                $national_code=$data->national_code;
            }
        }
        //$instance_id=app('getInstanceObject')->getCurrentInstanceId();
        $data=[];
        if($national_code){
            $user=User::/*where('instance_id',$instance_id)->*/where('national_code',$national_code)->select(['id','first_name','last_name'])->first();
            if($user){
                $data=$user->toArray();
            }
        }
        return $this->setDataResponse($data)->setResponse(true);
    }


    public function enrollUserOnCourse($user_id,  $course_id,$role_id)
    {
        $enrollment=Enrollment::where('alias','manual')->first();
        $time = time() + 25920000;
        $expireDate = DateHelper::setDateTime(date(trans('core::date.database.datetime'), $time), null, trans('core::date.database.datetime'))->getDateTime(trans('core::date.database.datetime'));
        $userEnroll = new CourseUserEnrollment;
        $userEnroll->user_id = $user_id;
        $userEnroll->course_id = $course_id;
        $userEnroll->enrollment_id = $enrollment->id;
        $userEnroll->role_id = $role_id;
        $userEnroll->active_date = date(trans('core::date.database.datetime'), time());
        $userEnroll->expire_date = $expireDate;
        $userEnroll->active = 1;
        return $userEnroll->save();
    }
}
