<?php

namespace Modules\Kanoon\ViewModels\User;

use DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Models\Enrollment\CourseUserEnrollment;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;
    private $user_id=null;


    public function setGridModel()
    {
        $this->Model=CourseUserEnrollment::where('user_id', $this->user_id)->active()->with(['course','user']);
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('course::form.fields.id'), true)
             ->addColumn('full_name', trans('user::form.fields.full_name'), false)
             ->addColumn('course', trans('course::course.course'), false)
             ->addColumn('active_date', trans('course::enrollment.active_date'), false)
             ->addColumn('expire_date', trans('course::enrollment.expire_date'), false);

        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('video::video.confirm_delete')];
        $delete = array(
            'name' => 'kanoon.user.courses.delete',
            'parameter' => ['id']
        );
        $this->addAction('delete', $delete, trans('course::enrollment.delete_enroll'), 'fa fa-trash', false, $options);

        $back=route('kanoon.manage');
        $options=['class'=>"btn btn-default"];
        $this->addButton('back',$back,trans('course::enrollment.back'),'fa fa-angle-right',false,'before',$options);

        $this->action_text = false;

        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->full_name=$row->user->full_name;
        $row->course=$row->course->title;
        $row->active_date=DateHelper::setDateTime($row->active_date)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->expire_date=DateHelper::setDateTime($row->expire_date)->getLocaleFormat(trans('core::date.datetime.medium'));

        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getList()
    {
        $this->user_id=$this->request->get('id');
        if(!$this->user_id){
            return $this->redirectBack()->setResponse(false,trans('course::messages.alert.not_find_data'));
        }
        $this->setTitlePage(trans('course::course.user_courses'));
        $this->generateGridList()->renderedView("kanoon::user.list", ['view_model' => $this], trans('course::course.user_courses'));
        return $this;
    }



    protected function destroyUserCourseEnrollment()
    {
        $course_enrollment_id = $this->request->get('user_course_enrollment_id');
        $courseEnrollment = CourseUserEnrollment::enable()->find($course_enrollment_id);
        if ($courseEnrollment && $courseEnrollment->delete()) {
            return $this->setResponse(true, trans("course::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.del_un_success"));
    }


    }
