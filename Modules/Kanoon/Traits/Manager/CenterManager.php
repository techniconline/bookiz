<?php
namespace Modules\Kanoon\Traits\Manager;

use BridgeHelper;
use Modules\Kanoon\Models\Center\KanoonCenterManager;

trait CenterManager{

    private $isCenterManager=false;

    public function isCenterManager($user_id=null){
        if($this->isCenterManager!==false){
            return $this->isCenterManager;
        }
        if(is_null($user_id)){
            $user=BridgeHelper::getAccess()->getUser();
            $user_id=$user->id;
        }
        $key=$this->getCenterManagerCacheKey($user_id);
        $centerUser=cache($key);
        if(!$centerUser){
            $centerUser=KanoonCenterManager::where('user_id',$user_id)->first();
            if($centerUser){
                $centerUser=(object)$centerUser->toArray();
            }else{
                $centerUser=0;
            }
            cache($key,$centerUser,3600);
        }
        return $centerUser;
    }

    public function getCenterManagerCacheKey($user_id){
        return 'kanoon_center_manager_user_'.$user_id;
    }
}