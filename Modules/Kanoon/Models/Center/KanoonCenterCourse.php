<?php

namespace Modules\Kanoon\Models\Center;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;

class KanoonCenterCourse extends BaseModel
{
    use SoftDeletes;

    protected $table='kanoon_center_courses';


    /**
     * @var array
     */
    protected $fillable = ['center_id', 'course_id','active', 'deleted_at', 'created_at', 'updated_at'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function scopeFilterByValue($query, $params)
    {

        if (isset($params['name'])) {
            $query = $query->whereHas('course', function ($q) use ($params) {
                $q->where('title', 'LIKE', '%' . $params['name'] . '%');
            });
        }

        return $query;
    }


    public function course()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course');
    }

    public function center()
    {
        return $this->belongsTo('Modules\Kanoon\Models\Center\KanoonCenter');
    }

}
