<?php

namespace Modules\Kanoon\Models\Center;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;

class KanoonCenterUser extends BaseModel
{
    use SoftDeletes;

    protected $table='kanoon_center_users';


    /**
     * @var array
     */
    protected $fillable = ['center_id', 'user_id','active', 'deleted_at', 'created_at', 'updated_at'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];


    /**
     * @param $query
     * @param $params
     * @return mixed
     */
    public function scopeFilterByValue($query, $params)
    {
        if (isset($params['mobile'], $params['mobile_connector_id'])) {
            $query = $query->whereHas('userConnectors', function ($q) use ($params) {
                $q->where('value', 'LIKE', '%' . $params['mobile'] . '%')->where('connector_id', $params['mobile_connector_id']);
            });
        }
        if (isset($params['email'], $params['email_connector_id'])) {
            $query = $query->whereHas('userConnectors', function ($q) use ($params) {
                $q->where('value', 'LIKE', '%' . $params['email'] . '%')->where('connector_id', $params['email_connector_id']);
            });
        }

        if (isset($params['first_name'], $params['first_name'])) {
            $query = $query->whereHas('user', function ($q) use ($params) {
                $q->where('first_name', 'LIKE', '%' . $params['first_name'] . '%');
            });
        }

        if (isset($params['last_name'], $params['last_name'])) {
            $query = $query->whereHas('user', function ($q) use ($params) {
                $q->where('last_name', 'LIKE', '%' . $params['last_name'] . '%');
            });
        }

        return $query;
    }


    public function user()
    {
        return $this->belongsTo('Modules\User\Models\User');
    }

    public function center()
    {
        return $this->belongsTo('Modules\Kanoon\Models\Center\KanoonCenter');
    }

    /**
     * @return mixed
     */
    public function userConnectors()
    {
        return $this->hasMany('Modules\User\Models\Register\UserRegisterKey','user_id','user_id');
    }

}
