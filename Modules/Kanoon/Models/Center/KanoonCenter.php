<?php

namespace Modules\Kanoon\Models\Center;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;

class KanoonCenter extends BaseModel
{
    use SoftDeletes;

    protected $table = 'kanoon_centers';
    /**
     * @var array
     */
    protected $fillable = ['title', 'instance_id', 'description', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];


    public static function getAllCenters($id = null)
    {
        $key = 'all_kanoon_centers';
        $all = cache($key);
        if (!$all) {
            $all = [];
            foreach (Self::all() as $item) {
                $all[$item->id] = $item->title;
            }
            cache($key, $all, 3600);
        }
        if ($id) {
            if (isset($all[$id])) {
                return $all[$id];
            }
            return null;
        }
        return $all;
    }


    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\instance');
    }
}
