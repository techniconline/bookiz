<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKanoonCenterManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kanoon_center_managers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('center_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('role_id');
            $table->tinyInteger('active')->default('2')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('kanoon_center_managers', function (Blueprint $table) {
            $table->foreign('center_id')->references('id')->on('kanoon_centers')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('role_id')->references('id')->on('roles')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kanoon_center_managers', function (Blueprint $table) {
            $table->dropForeign('kanoon_center_managers_center_id_foreign');
            $table->dropForeign('kanoon_center_managers_user_id_foreign');
            $table->dropForeign('kanoon_center_managers_role_id_foreign');
        });
        Schema::dropIfExists('kanoon_center_managers');
    }
}
