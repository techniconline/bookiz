<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKanoonCenterCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kanoon_center_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('center_id');
            $table->unsignedInteger('course_id');
            $table->tinyInteger('active')->default('2')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('kanoon_center_courses', function (Blueprint $table) {
            $table->foreign('center_id')->references('id')->on('kanoon_centers')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kanoon_center_courses', function (Blueprint $table) {
            $table->dropForeign('kanoon_center_courses_center_id_foreign');
            $table->dropForeign('kanoon_center_courses_course_id_foreign');
        });
        Schema::dropIfExists('kanoon_center_courses');
    }
}
