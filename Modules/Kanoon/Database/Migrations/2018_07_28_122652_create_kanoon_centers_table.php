<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKanoonCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kanoon_centers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instance_id')->unsigned()->index();
            $table->string('title');
            $table->mediumText('description')->nullable();
            $table->integer('country_id')->nullable()->index();
            $table->integer('state_id')->unsigned()->nullable()->index();
            $table->integer('city_id')->unsigned()->nullable()->index();
            $table->tinyInteger('active')->default('2')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

//        Schema::table('kanoon_centers', function (Blueprint $table) {
//            $table->foreign('country_id')->references('id')->on('locations')
//                ->onDelete('set null')
//                ->onUpdate('cascade');
//
//            $table->foreign('state_id')->references('id')->on('locations')
//                ->onDelete('set null')
//                ->onUpdate('cascade');
//
//            $table->foreign('city_id')->references('id')->on('locations')
//                ->onDelete('set null')
//                ->onUpdate('cascade');
//
//        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('kanoon_centers', function (Blueprint $table) {
//            $table->dropForeign('kanoon_centers_country_id_foreign');
//            $table->dropForeign('kanoon_centers_state_id_foreign');
//            $table->dropForeign('kanoon_centers_city_id_foreign');
//        });
        Schema::dropIfExists('kanoon_centers');
    }
}
