<?php
namespace Modules\Kanoon\Database\Seeders;
use Illuminate\Database\Seeder;
use Modules\User\Models\Permission;

class PermissionTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('connector')->delete();

        $permissions=[
            ['kanoon','manage.center','مدیریت آموزشگاهها'],
            ['kanoon','manage.center.add','اضافه کردن آموزشگاهها'],
            ['kanoon','manage.center.edit','ویرایش آموزشگاهها'],
            ['kanoon','manage.center.users','مدیریت کاربران آموزشگاهها'],
            ['kanoon','manage.center.user.add','اضافه کردن کاربر جدید'],
        ];

        foreach ($permissions as $permission) {
            $saver=new Permission();
            $saver->fill(array(
                'module' => $permission[0],
                'permission' => $permission[1],
                'description' => $permission[2],
            ))->save();
        }

	}
}