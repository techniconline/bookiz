<?php

namespace Modules\Kanoon\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class KanoonDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");
        $this->call('Modules\Kanoon\Database\Seeders\ModuleTableSeeder');
        $this->call('Modules\Kanoon\Database\Seeders\PermissionTableSeeder');

    }
}
