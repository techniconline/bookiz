<?php

Route::group(['middleware' => ['web','auth','admin'], 'prefix' => 'kanoon','as'=>'kanoon.', 'namespace' => 'Modules\Kanoon\Http\Controllers\Manage'], function()
{
    Route::get('/manage', 'KanoonController@index')->name('manage');
    Route::get('/manage/add/user', 'KanoonController@addUserToCourse')->name('manage.add.user');
    Route::post('/manage/add/user', 'KanoonController@saveUserToCourse')->name('manage.add.user.save');
    Route::post('/manage/info/user', 'KanoonController@getUserInfo')->name('manage.user.info');
    Route::get('/manage/user/course/{id}', 'KanoonController@getUserCourse')->name('manage.center.user.course');

    Route::get('/manage/center', 'Center\CenterController@index')->name('manage.center');
    Route::get('/manage/center/json', 'Center\CenterController@json')->name('manage.center.json');

    Route::get('/manage/center/edit/{id}', 'Center\CenterController@edit')->name('manage.center.edit');
    Route::get('/manage/center/create/0', 'Center\CenterController@create')->name('manage.center.create');
    Route::post('/manage/center/save/{id}', 'Center\CenterController@save')->name('manage.center.save');

    Route::get('/manage/center/managers/{center_id}', 'Center\CenterManagerController@index')->name('manage.center.managers');
    Route::get('/manage/center/manager/edit/{center_id}/{id}', 'Center\CenterManagerController@edit')->name('manage.center.manager.edit');
    Route::post('/manage/center/manager/edit/{center_id}/{id}', 'Center\CenterManagerController@save')->name('manage.center.manager.save');
    Route::get('/manage/center/manager/delete/{center_id}/{id}', 'Center\CenterManagerController@delete')->name('manage.center.manager.delete');


    Route::get('/manage/center/courses/{center_id}', 'Center\CenterCourseController@index')->name('manage.center.courses');
    Route::get('/manage/center/courses/edit/{center_id}/{id}', 'Center\CenterCourseController@edit')->name('manage.center.courses.edit');
    Route::post('/manage/center/courses/edit/{center_id}/{id}', 'Center\CenterCourseController@save')->name('manage.center.courses.save');


    Route::group(['prefix' => 'user', 'as' => 'user.','namespace' => '\Course'], function () {
        Route::get('/{id}', 'UserController@userCourses')->name('courses');
        Route::delete('/delete/{id}', 'UserController@deleteCourse')->name('courses.delete');
    });

});
