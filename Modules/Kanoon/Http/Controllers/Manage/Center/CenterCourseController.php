<?php

namespace Modules\Kanoon\Http\Controllers\Manage\Center;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class CenterCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($center_id,MasterViewModel $masterViewModel,Request $request)
    {
        return $masterViewModel->setItemsRequest(['center_id'=>$center_id],$request)->setViewModel('manage.center.course.list')->setActionMethod("getList")->response();

    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function edit($center_id,$id,MasterViewModel $masterViewModel,Request $request)
    {
        return $masterViewModel->setItemsRequest(['center_id'=>$center_id,'id'=>$id],$request)->setViewModel('manage.center.course.edit')->setActionMethod("getForm")->response();

    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function save($center_id,$id,MasterViewModel $masterViewModel,Request $request)
    {
        return $masterViewModel->setItemsRequest(['center_id'=>$center_id,'id'=>$id],$request)->setViewModel('manage.center.course.edit')->setActionMethod("save")->response();

    }
}
