<?php

namespace Modules\Kanoon\Http\Controllers\Manage\Center;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class CenterController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('manage.center.list')->setActionMethod("getList")->response();

    }


    public function edit($id,Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.center.center')->setActionMethod("getForm")->response();
    }

    public function create(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('manage.center.center')->setActionMethod("getForm")->response();
    }


    public function save($id,Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.center.center')->setActionMethod("saveForm")->response();
    }

    public function json( Request $request,MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['is_ajax'=>1],$request)->setViewModel('manage.center.json')->setActionMethod("getCenterByApi")->response();
    }




}
