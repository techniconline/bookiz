<?php

namespace Modules\Kanoon\Http\Controllers\Manage\Course;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class UserController extends Controller
{

    /**
     * Show the specified resource.
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function userCourses($id,Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('user.list')->setActionMethod("getList")->response();
    }


   public function deleteCourse($id,Request $request,MasterViewModel $masterViewModel){
       return $masterViewModel->setItemsRequest(['user_course_enrollment_id' => $id], $request)
           ->setViewModel('user.list')
           ->setActionMethod("destroyUserCourseEnrollment")->response();
   }

}
