<?php

namespace Modules\Kanoon\Http\Controllers\Manage;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class KanoonController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('manage.list')->setActionMethod("getList")->response();
    }

    public function addUserToCourse(MasterViewModel $masterViewModel){
        return $masterViewModel->setViewModel('manage.add_user')->setActionMethod("getForm")->response();
    }

    public function saveUserToCourse(Request $request,MasterViewModel $masterViewModel){
        return $masterViewModel->setRequest($request)->setViewModel('manage.add_user')->setActionMethod("saveUser")->response();
    }


    public function getUserInfo(Request $request,MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['is_ajax'=>1],$request)->setViewModel('manage.add_user')->setActionMethod("getUser")->response();
    }


    public function getUserCourse($id,Request $request,MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('manage.user.list_course')->setActionMethod("getList")->response();
    }


}
