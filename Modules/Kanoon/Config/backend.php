<?php

return [
    'version' => '20180619001',
    'blocks' => [
        'left' => [],
        'top' => []
    ],
    'menus' => [
        'management_admin' => [
            [
                'alias' => '', //**
                'route' => 'kanoon.manage', //**
                'key_trans' => 'kanoon::menu.titles.manage_users', //**
                'order' =>50000, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => '', //fa-XXXXX
                'class' => '',
                'id' => '',
                'group_menu' => 'kanoon_admin',
            ],
            [
                'alias' => '', //**
                'route' => 'kanoon.manage.center', //**
                'key_trans' => 'kanoon::menu.titles.manage_centers', //**
                'order' =>50000, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => '', //fa-XXXXX
                'class' => '',
                'id' => '',
                'group_menu' => 'kanoon_admin',
            ],
            [
                'alias' => '', //**
                'route' => 'kanoon.manage.add.user', //**
                'key_trans' => 'kanoon::menu.titles.manage_add_user', //**
                'order' =>1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => '', //fa-XXXXX
                'class' => '',
                'id' => '',
                'group_menu' => 'kanoon_admin',
            ],
        ],
    ],
    'group_menu' => [
        'management_admin' => [
            [   'alias' => 'kanoon_admin', //**
                'key_trans' => 'kanoon::menu.titles.admin_menu', //**
                'icon_class' => 'fa fa-users',
                'before' => '',
                'after' => '',
                'order' => 10,
                ],
        ]
    ],

];
