<?php

return [
    'name' => 'Kanoon',
    'permissions'=>[
         'kanoon.manage'=>['type' => '|', 'access' => 'kanoon/manage.center.users'],
         'kanoon.manage.add.user'=>['type' => '|', 'access' => 'kanoon/manage.center.user.add'],
         'manage.add.user.save'=>['type' => '|', 'access' => 'kanoon/manage.center.user.add'],
         'kanoon.manage.user.info'=>['type' => '|', 'access' => 'kanoon/manage.center.user.add'],
         'kanoon.manage.center'=>['type' => '|', 'access' => 'kanoon/manage.center'],
         'kanoon.manage.center.json'=>['type' => '|', 'access' => 'kanoon/manage.center.edit'],
         'kanoon.manage.center.edit'=>['type' => '|', 'access' => 'kanoon/manage.center.edit'],
         'kanoon.manage.center.create'=>['type' => '|', 'access' => 'kanoon/manage.center.add'],
         'kanoon.manage.center.save'=>['type' => '|', 'access' => 'kanoon/manage.center.edit'],
         'kanoon.manage.center.managers'=>['type' => '|', 'access' => 'kanoon/manage.center'],
         'kanoon.manage.center.manager.edit'=>['type' => '|', 'access' => 'kanoon/manage.center.edit'],
         'kanoon.manage.center.manager.save'=>['type' => '|', 'access' => 'kanoon/manage.center.edit'],
         'kanoon.manage.center.manager.delete'=>['type' => '|', 'access' => 'kanoon/manage.center.edit'],
         'kanoon.manage.center.courses'=>['type' => '|', 'access' => 'kanoon/manage.center'],
         'kanoon.manage.center.courses.edit'=>['type' => '|', 'access' => 'kanoon/manage.center.edit'],
         'kanoon.manage.center.courses.save'=>['type' => '|', 'access' => 'kanoon/manage.center.edit'],
     ],
];
