<div class="row margin-top-10">
    <div class="col-md-12">

        <div class="portlet box green _list_course_activity_items">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bell-o"></i>
                    @if($BlockViewModel->getModelData('id'))
                    @lang('kanoon::menu.titles.manage_center_edit')
                    @else
                    @lang('kanoon::menu.titles.manage_center_add')
                    @endif
                    ({{ $BlockViewModel->getCenter('title') }})
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title=""
                       title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block">

                <div class="row">

                    <div class="col-md-12">

                        {!! FormHelper::open(['role'=>'form','url'=>route('kanoon.manage.center.manager.save',['center_id'=>$BlockViewModel->getFromRequest('center_id'),'id'=>$BlockViewModel->getFromRequest('id')]) ,'method'=>'POST','class'=>'form-horizontal']) !!}

                        @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                        {!! FormHelper::selectTag('user_id',[],old('user_id',$BlockViewModel->getModelData('user_id')),['data-ajax-url'=>$BlockViewModel->getUrlSearchUser() , 'title'=>trans("course::enrollment.manuals.user"),'helper'=>trans("course::enrollment.manuals.select_user")]) !!}
                        {!! FormHelper::select('active',trans('core::data.yes_no'),old('active',$BlockViewModel->getModelData('active')),['title'=>trans('core::form.fields.active'),'helper'=>trans('core::form.helper.active')]) !!}

                        {!! FormHelper::select('role_id',BridgeHelper::getAccess()->getAdminRoles('site',null,true),old('role_id',$BlockViewModel->getModelData('role_id'))
,['title'=>trans("course::enrollment.role"),'helper'=>trans("course::enrollment.select_role")
,'placeholder'=>trans("course::enrollment.select_role")]) !!}

                        {!! FormHelper::openAction() !!}
                        {!! FormHelper::submitByCancel(['title'=>trans("kanoon::menu.titles.submit")], ['title'=>trans("kanoon::menu.titles.cancel"), 'url'=>route('kanoon.manage.center.managers',["center_id"=>$BlockViewModel->getFromRequest('center_id')])]) !!}
                        {!! FormHelper::closeAction() !!}
                        {!! FormHelper::close() !!}


                    </div>

                </div>

            </div>
        </div>

    </div>
</div>


