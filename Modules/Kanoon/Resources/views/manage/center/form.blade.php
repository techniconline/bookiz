<div class="col-md-12">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i> @lang($BlockViewModel->getModelData('id',0)?'kanoon::menu.titles.manage_edit_center':'kanoon::menu.titles.manage_create_center')
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('kanoon.manage.center.save',['id'=>$BlockViewModel->getModelData('id',0)]),'method'=>'POST','class'=>'form-horizontal']) !!}

            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            {!! FormHelper::input('title','title',old('title',$BlockViewModel->getModelData('title')),['required'=>'required','title'=>trans('user::form.fields.title'),'helper'=>trans('user::form.helper.title')]) !!}

            {!! FormHelper::textarea('description',old('description',$BlockViewModel->getModelData('description'))
,['title'=>trans("kanoon::form.fields.description"),'helper'=>trans("kanoon::form.fields.description")]) !!}

            {!! FormHelper::select('country_id',[],old('country_id',$BlockViewModel->getModelData('country_id')),['required'=>'required','data-init'=>1,'data-fill-url'=>route('core.location.json'),'data-change-trigger'=>'state_id','title'=>trans('user::form.fields.country'),'helper'=>trans('user::form.helper.country')]) !!}

            {!! FormHelper::select('state_id',[],old('state_id',$BlockViewModel->getModelData('state_id')),['required'=>'required','data-fill-url'=>route('core.location.json'),'data-change-trigger'=>'city_id','title'=>trans('user::form.fields.state_id'),'helper'=>trans('user::form.helper.state_id')]) !!}

            {!! FormHelper::select('city_id',[],old('city_id',$BlockViewModel->getModelData('city_id')),['required'=>'required','data-fill-url'=>route('core.location.json'),'title'=>trans('user::form.fields.city_id'),'helper'=>trans('user::form.helper.city_id')]) !!}

            {!! FormHelper::select('active',trans('core::data.active'),old('active',$BlockViewModel->getModelData('active')),['title'=>trans('kanoon::form.fields.status'),'helper'=>trans('kanoon::form.fields.active'),'placeholder'=>trans('user::form.placeholder.please_select')]) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>

