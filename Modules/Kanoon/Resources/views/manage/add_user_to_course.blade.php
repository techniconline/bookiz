<div class="col-md-12">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i> @lang('kanoon::form.titles.add_user_to_course')
            </div>
        </div>

        <div class="portlet-body form">
            @if(count($BlockViewModel->getMessages()))
                <div class="alert alert-info">
                    @foreach($BlockViewModel->getMessages() as $message)
                        <p><strong>{{ $loop->iteration }}-{{ $message }}</strong></p>
                    @endforeach
                        <a href="javascript:void(0);" title="بستن" class="close " ><i class="fa fa-times"></i></a>
                </div>
            @endif

            {!! FormHelper::open(['role'=>'form','url'=>route('kanoon.manage.add.user'),'method'=>'POST','class'=>'form-horizontal']) !!}
                @if($centerManager=$BlockViewModel->isCenterManager())
                    {!! FormHelper::input('hidden','center_id',$centerManager->center_id,['id'=>'center_id']) !!}
                @endif
            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp
                @if(!$BlockViewModel->isCenterManager())
                    {!! FormHelper::selectTag('center_id',[],old('center_id',null),['required'=>'required','data-ajax-url'=> route('kanoon.manage.center.json'), 'title'=>trans("kanoon::form.fields.center"),'helper'=>trans("kanoon::form.helper.center")]) !!}
                @endif

                {!! FormHelper::input('text','national_code',old('national_code'),['required'=>'required','title'=>trans('user::form.fields.national_code'),'pattern'=>".{10}",'helper'=>trans('user::form.helper.national_code')]) !!}
            {!! FormHelper::input('hidden','user_id',old('user_id'),['id'=>'user_id']) !!}
            {!! FormHelper::input('text','first_name',old('first_name'),['required'=>'required','title'=>trans('user::form.fields.first_name'),'helper'=>trans('user::form.helper.first_name')]) !!}
            {!! FormHelper::input('text','last_name',old('last_name'),['required'=>'required','title'=>trans('user::form.fields.last_name'),'helper'=>trans('user::form.helper.last_name')]) !!}
            <div class="user-info">
                {!! FormHelper::input('text','connector',old('connector',''),['autocomplete'=>"new-password",'required'=>'required','title'=>$BlockViewModel->getConnectorLabel()]) !!}
                {!! FormHelper::input('password','password',old('password',''),['autocomplete'=>"new-password",'required'=>'required','title'=>trans('user::form.fields.password')]) !!}
            </div>
            {!! FormHelper::selectTag('courses[]',[],old('courses[]',null),['required'=>'required','multiple'=>'multiple' ,'data-ajax-url'=> route('course.json'), 'title'=>trans("course::course.courses"),'helper'=>trans("course::course.courses")]) !!}
            {!! FormHelper::select('role_id',BridgeHelper::getAccess()->getRoles('course',null,true),old('role_id')
,['title'=>trans("course::enrollment.role"),'helper'=>trans("course::enrollment.select_role")
,'placeholder'=>trans("course::enrollment.select_role")]) !!}
            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {

        function removeReadOnly(){
            $("#first_name").removeAttr('readonly');
            $("#last_name").removeAttr('readonly');
            $("#user_id").val('');
            $("#connector").removeAttr('readonly');
            $("#password").removeAttr('readonly');
        }

        $("#national_code").keyup(function (){
            removeReadOnly();
            var $val=$(this).val();
            if($val && $val.length>9){
                $("#user_id").val('');
                $data = {};
                $data.national_code = $val;
                getUserInfo($data);
            }
        });

        $("#national_code").blur(function () {
            removeReadOnly();
            var $val=$(this).val();
            if($val && $val.length>9){
                $("#user_id").val('');
                $data = {};
                $data.national_code = $val;
                getUserInfo($data);
            }
        });

    var getUserInfo = function ($data) {
        var url='{{ $BlockViewModel->getAjaxUserData() }}';
        removeReadOnly();
        $("#first_name","#last_name","#connector","#user_id").val('');
        jQuery.ajax({
            method: "POST",
            dataType: 'json',
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
            },
            url: url,
            data: "data=" + JSON.stringify($data),
            processData: false,
            success: function (result) {
                if (result && result.data) {
                    $("#first_name").val(result.data.first_name).attr('readonly','readonly');
                    $("#last_name").val(result.data.last_name).attr('readonly','readonly');
                    $("#user_id").val(result.data.id);
                    $("#connector").attr('readonly','readonly');
                    $("#password").attr('readonly','readonly');
                }
            },
            error: function (e) {
                removeReadOnly();
            }
        });
    };

        $("#national_code").trigger('keyup');

    });
</script>
