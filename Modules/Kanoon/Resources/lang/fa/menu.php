<?php

return [
    'titles' => [
        'admin_menu' => 'مدیریت آموزشگاه ها',
        'manage_users'=>' مدیریت کاربران آموزشگاهها',
        'manage_centers'=>'مدیریت آموزشگاهها',
        'manage_add_user'=>'اضافه کردن کاربر جدید',
        'manage_edit_center'=>'ویرایش آموزشگاه',
        'manage_create_center'=>'اضافه کردن آموزشگاه',
        'manage_edit_center_managers'=>'مدیران آموزشگاه',
        'manage_edit_center_managers_name'=>':name مدیریت مدیران آموزشگاه',
        'manage_center_add'=>'اضافه کردن مدیر آموزشگاه',
        'manage_center_edit'=>'ویرایش مدیر آموزشگاه',
        'manage_center_delete'=>'حذف مدیر آموزشگاه',
        'manage_edit_center_courses'=>'دوره های آموزشگاه',
        'manage_edit_center_courses_name'=>':name مدیریت دوره های آموزشگاه',
        'manage_center_course_add'=>'اضافه کردن دوره به آموزشگاه',
        'manage_center_course_edit'=>'ویرایش دوره آموزشگاه',
        'submit'=>'ذخیره داده ها',
        'cancel'=>'برگشت به بخش قبلی',
    ],
];