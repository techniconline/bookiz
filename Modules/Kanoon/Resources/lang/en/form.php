<?php

return [
    'titles' => [
        'add_user_to_course' => 'اضافه کردن کاربر به درس',
    ],
    'fields' => [
        'center'=>'آموزشگاه',
        'country_id'=>'کشور',
        'state_id'=>'استان',
        'city_id'=>'شهر',
        'id'=>'شناسه',
        'title'=>'عنوان',
        'active'=>'فعال بودن',
        'create_center'=>'ایجاد آموزشگاه جدید',
        'status'=>'وضعیت',
        'description'=>'توضیح',
        'instance'=>'سامانه',
    ],
    'helper' => [
        'center'=>'',
        'country_id'=>'',
        'state_id'=>'',
        'city_id'=>'',
        'id'=>'',
        'title'=>'',
        'active'=>'',
        'description'=>'',
    ],
];