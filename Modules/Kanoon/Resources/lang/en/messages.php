<?php

return [
    'is_enrolled'=>'کاربر (:username) در درس (:title) قبلا عضو بوده است.',
    'enroll_success'=>'کاربر (:username) در درس (:title) با موفقیت عضو شد.',
    'is_enroll_success'=>'کاربر (:username) در درس (:title) با موفقیت عضو شد.',
    'is_enroll_error'=>'امکان ثبت نام کاربر (:username) در درس (:title) وجود ندارد.',
    'register_center_error'=>'اضافه کردن کاربر به آموزشگاه مورد نظر ممکن نیست.',
    'save_success'=>'داده ها با موفقیت ذخیره شدند.',
    'save_error'=>'امکان ذخیره داده ها وجود ندارد.',
    'user_manage_error'=>'متاسفانه کاربر مورد نظر در آموزشگاه دیگری مدیر است.',
];