<?php

namespace Modules\Exam\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Exam\Traits\ExamAnswer as TraitModel;

class ExamAnswer extends BaseModel
{

    /**
     * @var array
     */
    protected $fillable = ['exam_question_id', 'type', 'answer_number', 'relations', 'answer', 'is_true', 'options', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $types = ['normal', 'all_answers', 'relational'];

    public $api_fields = ['id', 'exam_question_id', 'answer', 'options', 'active'];
    public $list_fields = ['id', 'exam_question_id', 'answer', 'options', 'active'];
    public $api_append_fields = [];
    public $list_append_fields = [];

    use TraitModel;
    use SoftDeletes;

    public $timestamps = true;
    protected $dates = ['deleted_at'];


    /**
     * @param int $index
     * @return int|mixed
     */
    public function getRowAlpha($index = 0)
    {
        $alpha = ['الف', 'ب', 'ج', 'د'];

        if (isset($alpha[$index])) {
            return $alpha[$index];
        }
        return $index;
    }

}
