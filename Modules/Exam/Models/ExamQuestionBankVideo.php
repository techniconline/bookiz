<?php

namespace Modules\Exam\Models;

use Modules\Core\Models\BaseModel;
use Modules\Exam\Traits\ExamQuestionBankVideo as TraitModel;

class ExamQuestionBankVideo extends BaseModel
{

    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['exam_question_bank_id', 'video_id'];

}
