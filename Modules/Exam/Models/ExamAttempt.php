<?php

namespace Modules\Exam\Models;


use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Exam\Traits\ExamAttempt as TraitModel;

class ExamAttempt extends BaseModel
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'course_activity_id', 'score', 'relative_score', 'start_time', 'end_time', 'status', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use TraitModel;
    use SoftDeletes;

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public $statuses =  ['new', 'open', 'close', 'expire'];

    const STATUS_NEW = 'new';
    const STATUS_OPEN = 'open';
    const STATUS_CLOSE = 'close';
    const STATUS_EXPIRE_TIME = 'expire';


}
