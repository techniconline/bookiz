<?php

namespace Modules\Exam\Models;

use Modules\Core\Models\BaseModel;
use Modules\Exam\Traits\ExamQuestionBankCourse as TraitModel;

class ExamQuestionBankCourse extends BaseModel
{
    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['exam_question_bank_id', 'course_id'];

}
