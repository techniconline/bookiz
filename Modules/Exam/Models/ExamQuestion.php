<?php

namespace Modules\Exam\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Exam\Traits\ExamQuestion as TraitModel;

class ExamQuestion extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['exam_question_bank_id', 'question', 'type', 'description', 'score', 'time', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use TraitModel;
    use SoftDeletes;

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public $types = ['four_options','descriptive'];

    const QUESTION_TYPE_FOUR_OPTIONS = 'four_options';
    const QUESTION_TYPE_DESCRIPTIVE = 'descriptive';

}
