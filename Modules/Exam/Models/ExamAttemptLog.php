<?php

namespace Modules\Exam\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\User\Models\User;

class ExamAttemptLog extends BaseModel
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'exam_attempt_id', 'data', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function examAttempt()
    {
        return $this->belongsTo('Modules\Exam\Models\ExamAttempt');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return mixed
     */
    public function getDataJsonAttribute()
    {
        if ($this->data) {
            return json_decode($this->data);
        }
        return $this->data;
    }
}
