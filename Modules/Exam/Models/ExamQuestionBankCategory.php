<?php

namespace Modules\Exam\Models;


use Modules\Core\Models\BaseModel;
use Modules\Exam\Traits\ExamQuestionBankCategory as TraitModel;

class ExamQuestionBankCategory extends BaseModel
{
    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['exam_question_bank_id', 'category_id'];


}
