<?php
namespace Modules\Exam\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Exam\Traits\ExamAnswerUser as TraitModel;

class ExamAnswerUser extends BaseModel
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'exam_question_id', 'exam_answer_id', 'correct_exam_answer_id', 'exam_attempt_id', 'exam_answer', 'correct_exam_answer', 'score', 'final_score', 'start_time', 'end_time', 'status', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use TraitModel;
    use SoftDeletes;

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public $statuses = ['new', 'answered', 'calculated'];

    const STATUS_NEW = 'new';
    const STATUS_ANSWERED = 'answered';
    const STATUS_CALCULATED = 'calculated';

    const DEFAULT_SCORE = 20;
    const DEFAULT_TOLERANCE_TIME = 3;

}
