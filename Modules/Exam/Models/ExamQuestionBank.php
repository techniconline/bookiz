<?php

namespace Modules\Exam\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Exam\Traits\ExamQuestionBank as TraitModel;

class ExamQuestionBank extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'language_id', 'title', 'type', 'description', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use TraitModel;
    use SoftDeletes;

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public $types = ['four_options','descriptive'];

}
