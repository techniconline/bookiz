<?php

namespace Modules\Exam\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Exam\Traits\ExamQuestionUser as TraitModel;

class ExamQuestionUser extends BaseModel
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'questions', 'exam_attempt_id', 'status', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use TraitModel;
    use SoftDeletes;

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public $statuses = ['new', 'answered'];

    const STATUS_NEW = 'new';
    const STATUS_ANSWERED = 'answered';

}
