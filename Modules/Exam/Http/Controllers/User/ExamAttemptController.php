<?php

namespace Modules\Exam\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class ExamAttemptController extends Controller
{

    /**
     * @param $activity_id
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function list($activity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_activity_id' => $activity_id], $request)
            ->setViewModel('user.examAttemptGrid')->setActionMethod("getListAttemptGrid")->response();
    }

    /**
     * @param $attempt_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($attempt_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_attempt_id' => $attempt_id], $request)->setViewModel('user.examAttempt')
            ->setActionMethod("editAttempt")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $attempt_id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($attempt_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_attempt_id' => $attempt_id], $request)->setViewModel('user.examAttempt')
            ->setActionMethod("saveAttempt")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $attempt_id
     * @param $status
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatus($attempt_id, $status, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_attempt_id' => $attempt_id, 'status' => $status], $request)->setViewModel('user.examAttempt')
            ->setActionMethod("changeStatus")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $attempt_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($attempt_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_attempt_id' => $attempt_id], $request)->setViewModel('user.examAttempt')
            ->setActionMethod("destroyAttempt")->response();
    }


}
