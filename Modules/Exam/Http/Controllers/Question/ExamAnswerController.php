<?php

namespace Modules\Exam\Http\Controllers\Question;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class ExamAnswerController extends Controller
{

    /**
     * @param $exam_question_bank_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($exam_question_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_question_id' => $exam_question_id], $request)->setViewModel('question.examAnswerGrid')->setActionMethod("getListGrid")->response();
    }


    /**
     * @param $exam_question_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create($exam_question_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_question_id' => $exam_question_id], $request)->setViewModel('question.examAnswer')
            ->setActionMethod("createAnswer")->response();
    }

    /**
     * @param $exam_question_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function sectionCreateAnswer($exam_question_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_question_id' => $exam_question_id], $request)->setViewModel('question.examAnswer')
            ->setActionMethod("sectionCreateAnswer")->response();
    }

    /**
     * @param $exam_question_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($exam_question_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_question_id' => $exam_question_id], $request)->setViewModel('question.examAnswer')
            ->setActionMethod("saveAnswer")->response();
    }

    /**
     * @param $exam_question_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveAnswers($exam_question_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_question_id' => $exam_question_id], $request)->setViewModel('question.examAnswer')
            ->setActionMethod("saveAnswers")->response();
    }

    /**
     * Show the specified resource.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_answer_id' => $id], $request)->setViewModel('question.examAnswer')
            ->setActionMethod("editAnswer")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_answer_id' => $id], $request)->setViewModel('question.examAnswer')
            ->setActionMethod("saveAnswer")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_answer_id' => $id], $request)->setViewModel('question.examAnswer')
            ->setActionMethod("destroyAnswer")->response();
    }

}
