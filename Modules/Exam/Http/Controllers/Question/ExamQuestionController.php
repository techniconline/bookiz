<?php

namespace Modules\Exam\Http\Controllers\Question;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class ExamQuestionController extends Controller
{

    /**
     * @param $exam_question_bank_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($exam_question_bank_id, MasterViewModel $masterViewModel , Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_question_bank_id' => $exam_question_bank_id], $request)->setViewModel('question.examQuestionGrid')->setActionMethod("getListGrid")->response();
    }


    /**
     * @param $exam_question_bank_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create($exam_question_bank_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_question_bank_id' => $exam_question_bank_id], $request)->setViewModel('question.examQuestion')
            ->setActionMethod("createQuestion")->response();
    }

    /**
     * @param $exam_question_bank_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($exam_question_bank_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_question_bank_id' => $exam_question_bank_id], $request)->setViewModel('question.examQuestion')
            ->setActionMethod("saveQuestion")->response();
    }

    /**
     * Show the specified resource.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_question_id' => $id], $request)->setViewModel('question.examQuestion')
            ->setActionMethod("editQuestion")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_question_id' => $id], $request)->setViewModel('question.examQuestion')
            ->setActionMethod("saveQuestion")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['exam_question_id' => $id], $request)->setViewModel('question.examQuestion')
            ->setActionMethod("destroyQuestion")->response();
    }
    
}
