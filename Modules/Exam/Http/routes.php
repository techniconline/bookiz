<?php

Route::group(['middleware' => ['web'], 'prefix' => 'exam', 'as' => 'exam.', 'namespace' => 'Modules\Exam\Http\Controllers'], function () {

    Route::group(['middleware' => ['auth'], 'prefix' => 'user', 'as' => 'user.'], function () {
        Route::post('/{attempt_id}/save', 'ExamController@store')->name('save');
    });

});

Route::group(['middleware' => ['web', 'auth', 'admin'], 'prefix' => 'exam', 'as' => 'exam.', 'namespace' => 'Modules\Exam\Http\Controllers'], function () {

    Route::group(['prefix' => 'user', 'as' => 'user.', 'namespace' => '\User'], function () {

        Route::get('/', 'ExamController@index')->name('index');

        Route::group(['prefix' => 'attempt', 'as' => 'attempt.'], function () {
            Route::get('/{activity_id}/list', 'ExamAttemptController@list')->name('list');
            Route::get('/{attempt_id}/edit', 'ExamAttemptController@edit')->name('edit');
            Route::put('/{attempt_id}/update', 'ExamAttemptController@update')->name('update');
            Route::post('/{attempt_id}/{status}/changeStatus', 'ExamAttemptController@changeStatus')->name('change_status');
            Route::delete('/{attempt_id}/delete', 'ExamAttemptController@destroy')->name('delete');
        });

    });

    Route::group(['prefix' => 'question', 'as' => 'question.', 'namespace' => '\Question'], function () {


        Route::group(['prefix' => 'bank', 'as' => 'bank.'], function () {

            Route::get('/', 'ExamQuestionBankController@index')->name('index');
            Route::get('/search', 'ExamQuestionBankController@getExamQuestionBankByApi')->name('get_exam_question_bank_by_api');
            Route::get('/create', 'ExamQuestionBankController@create')->name('create');
            Route::post('/save', 'ExamQuestionBankController@store')->name('save');
            Route::put('/{exam_question_bank_id}/update', 'ExamQuestionBankController@update')->name('update');
            Route::get('/{exam_question_bank_id}/edit', 'ExamQuestionBankController@edit')->name('edit');
            Route::get('/{exam_question_bank_id}/show', 'ExamQuestionBankController@show')->name('show');
            Route::delete('/{exam_question_bank_id}/delete', 'ExamQuestionBankController@destroy')->name('delete');

        });

        Route::group(['prefix' => 'list', 'as' => 'list.'], function () {

            Route::get('/{exam_question_bank_id}/', 'ExamQuestionController@index')->name('index');
            Route::get('/{exam_question_bank_id}/create', 'ExamQuestionController@create')->name('create');
            Route::post('/{exam_question_bank_id}/save', 'ExamQuestionController@store')->name('save');
            Route::put('/{exam_question_id}/update', 'ExamQuestionController@update')->name('update');
            Route::get('/{exam_question_id}/edit', 'ExamQuestionController@edit')->name('edit');
            Route::get('/{exam_question_id}/show', 'ExamQuestionController@show')->name('show');
            Route::delete('/{exam_question_id}/delete', 'ExamQuestionController@destroy')->name('delete');

        });

        Route::group(['prefix' => 'answer', 'as' => 'answer.'], function () {

            Route::get('/{exam_question_id}/', 'ExamAnswerController@index')->name('index');
            Route::get('/{exam_question_id}/create', 'ExamAnswerController@create')->name('create');
            Route::get('/{exam_question_id}/section/create', 'ExamAnswerController@sectionCreateAnswer')->name('section_create_answer');
            Route::post('/{exam_question_id}/save', 'ExamAnswerController@store')->name('save');
            Route::post('/{exam_question_id}/save_answers', 'ExamAnswerController@saveAnswers')->name('save_answers');
            Route::put('/{exam_answer_id}/update', 'ExamAnswerController@update')->name('update');
            Route::get('/{exam_answer_id}/edit', 'ExamAnswerController@edit')->name('edit');
            Route::get('/{exam_answer_id}/show', 'ExamAnswerController@show')->name('show');
            Route::delete('/{exam_answer_id}/delete', 'ExamAnswerController@destroy')->name('delete');

        });

    });

});
