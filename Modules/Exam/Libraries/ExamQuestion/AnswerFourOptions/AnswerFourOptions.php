<?php

namespace Modules\Exam\Libraries\ExamQuestion\AnswerFourOptions;

use Illuminate\Support\Facades\Cache;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Exam\Libraries\ExamQuestion\ExamAnswerBase;
use Modules\Exam\Models\ExamAnswer;
use Modules\Exam\Models\ExamQuestionUser;

class AnswerFourOptions extends ExamAnswerBase
{

    protected $name = 'answer_four_options';
    protected $cache_key_name = 'answer_four_options';
    protected $answerModel;

    public function __construct()
    {
        $this->answerModel = new ExamAnswer();
    }

    /**
     * @param $question
     * @return $this
     */
    public function setQuestion($question)
    {
        $this->setUser();
        $this->question = $question;
        $attempt = $this->getData('attempt');
        $attempt_id = $attempt ? $attempt->id : 0;
        $this->setAttemptId($attempt_id);
        if (isset($question['exam_answers'])) {
            $this->setAnswers($question['exam_answers']);
        }
        return $this;
    }

    /**
     * @return null
     */
    public function getAnswers()
    {
        if ($this->answers) {
            return $this->getRandomAnswers();
        }
        return null;
    }

    /**
     * @return int
     */
    public function getCountAppendAnswers()
    {
        return 10;
    }

    /**
     * @return array
     */
    public function getRandomAnswers()
    {
        $this->cache_key_name .= 'attempt_' . $this->exam_attempt_id . '_question_' . $this->question['id'];
        $activity = $this->getData('activity');
        $params = $activity ? $activity->params : null;
        $is_random = $params && isset($params->exam_answer_is_random) ? $params->exam_answer_is_random : 0;
        if (Cache::has($this->cache_key_name)) {
            return Cache::get($this->cache_key_name);
        }

        $answerTrue = collect($this->answers)->where('is_true', 1)->first();
        $answers = collect($this->answers)->where('is_true', 0);
        if (boolval($is_random)) {
            $answers = $answers->shuffle();
        }

        $answers = $answers->whereIn('type', ['normal','all_answers','relational'])->take(3)->values();
        if (boolval($is_random)) {
            $answers = $answers->shuffle();
        }

        $is_relation = false;
        $hasAllAnswers = 0;
        $hasRelational = ($answers)->where('type', 'relational')->count();
        $hasRelational = $hasRelational ?: ($answerTrue['type'] == 'relational' ? true : false);


        if (!$hasRelational) {
            $hasAllAnswers = ($answers)->where('type', 'all_answers')->count();
            $hasAllAnswers = $hasAllAnswers ?: ($answerTrue['type'] == 'all_answers' ? true : false);
            if ($hasAllAnswers) {
                $answers = $this->renderAllAnswersTrue($answers, $answerTrue);
                $is_relation = true;
            } else {
                $answers = $answers->put(3, $answerTrue);
            }
        } else {
            $answers = $this->renderRelationalAnswers($answers, $answerTrue);
            $is_relation = true;
        }

        $answers = $answers->map(function ($item) {
            unset($item['is_true'], $item['relations']);
            return $item;
        });

        if ($is_relation) {
            $answers = $answers->all();
        } else {
            $answers = $answers->shuffle()->all();
        }

        $exp_date = isset($this->question['exp_time']) && $this->question['exp_time'] ? $this->question['exp_time'] : null;
        $duration = $exp_date ? (int)((strtotime($exp_date) - time()) / 60) : 60;

        //save log in server
        $this->saveExamQuestionUser($answers, $this->question['id']);
        Cache::put($this->cache_key_name, $answers, $duration);
        return $answers;
    }

    /**
     * @param $answers
     * @param $answerTrue
     * @return mixed
     */
    private function renderRelationalAnswers($answers, $answerTrue)
    {
        $answerRelation = $answerTrue['type'] == 'relational' ? $answerTrue : $answers->where('type', 'relational')->first();
//        dd($answers,$answerRelation,$answerTrue);
        if($answerTrue['type'] != 'relational'){
            $keyRelation = $answers->where('type', 'relational')->keys()->first();
            $answers = $answers->put($keyRelation, $answerTrue);
        }

        if ($answerRelation) {
            $relations = $answerRelation['relations'] ? explode(",", $answerRelation['relations']) : [];
            $relationAnswers = $answers->whereIn('answer_number', $relations);
            $rows = $relationAnswers->keys()->values()->all();
            $answerT = [];
            foreach ($rows as $row) {
                $answerT[] = $this->getRowAlpha($row);
            }
            $answerText = implode(trans('exam::exam.answer.separator_relation_answer_text'), $answerT);
            $answerRelation['answer'] = $answerText;
        }

        $answers = $answers->put(3, $answerRelation);

        return $answers;
    }

    /**
     * @param $answers
     * @param $answerTrue
     * @return mixed
     */
    private function renderAllAnswersTrue($answers, $answerTrue)
    {
        if ($answerTrue['type'] == 'all_answers') {
            $answers = $answers->put(3, $answerTrue);
        } else {
            $answers = $answers->put(3, $answerTrue);
            $key = $answers->where('type', 'all_answers')->keys()->first();
            $answerPull = $answers->pull($key);
            $answers = $answers->values()->put(3, $answerPull);;
        }
        return $answers;
    }

    /**
     * @param $answers
     * @param $question_id
     * @return bool
     */
    private function saveExamQuestionUser($answers, $question_id)
    {
        $short_answer = collect($answers)->map(function ($row) {
            return collect($row)
                ->only(['id', 'answer_number', 'type'])
                ->all();
        });
        $questionUser = $this->getExamQuestionUser(true);
        if (!$questionUser) {
            return false;
        }
        $questions = $questionUser ? $questionUser->questions : [];
        $questions = json_decode(json_encode($questions), true);
        $questions['list_answers'][$question_id] = $short_answer;

        $questionUser->questions = json_encode($questions);
        return $questionUser->save();
    }

    /**
     * @param bool $forget
     * @return mixed
     */
    private function getExamQuestionUser($forget = false)
    {
        $model = new ExamQuestionUser();
        $key = $model->getTable() . '_user_' . $this->user->id . '_attempt_' . $this->exam_attempt_id;
        if (Cache::has($key)) {
            $userQuestions = Cache::get($key);
        } else {
            $exp_date = isset($this->question['exp_time']) && $this->question['exp_time'] ? $this->question['exp_time'] : null;
            $duration = $exp_date ? (int)((strtotime($exp_date) - time()) / 60) : 60;
            $userQuestions = $model->active()->where('exam_attempt_id', $this->exam_attempt_id)->where('user_id', $this->user->id)->where('status', $model::STATUS_NEW)->orderBy('id', 'DESC')->first();
            Cache::put($key, $userQuestions, $duration);
        }

        if ($forget) {
            Cache::forget($key);
        }

        return $userQuestions;
    }

    /**
     * @return array
     */
    public function getRowData()
    {
        $data = [
            'item_type' => 'exam',
            'item_id' => $this->exam_attempt_id,
            'options' => [
                'question_id' => isset($this->question['id']) ? $this->question['id'] : null
            ],
            'question_text' => isset($this->question['question']) ? $this->question['question'] : null,
            'answers' => $this->getAnswers(),
            'answer_user' => null,
        ];
        return $data;
    }

    /**
     * @return null
     */
    public function getAllAnswers()
    {
        return $this->answers ? $this->answers : null;
    }

    public function getGenerateAnswerView()
    {

    }

    /**
     * @param int $index
     * @return int|mixed
     */
    public function getRowAlpha($index = 0)
    {
        return $this->answerModel->getRowAlpha($index);
    }

}