<?php

namespace Modules\Exam\Libraries\ExamQuestion;

class MasterExamQuestion
{

    /**
     * @param $question
     * @param null $data
     * @return string
     */
    public function getQuestion($question, $data = null)
    {
        if (is_null($data)) {
            $data = [];
        }
        try {
            $questionClass = "\\Modules\\Exam\\Libraries\\ExamQuestion";
            $type = get_uppercase_by_underscores($question['type']);
            $questionClass .= "\\Answer" . $type . "\\Answer" . $type ;
            $questionClass = new $questionClass();
            $questionClass->setDataQuestion($data)->setQuestion($question);
        } catch (\Exception $exception) {
            //
        }
        return $questionClass;
    }

    /**
     * @param $type
     * @param array $answers
     * @param array $data
     * @return string
     */
    public function getAnswer($type, $answers = [], $data = [])
    {
        try {
            $questionClass = "\\Modules\\Exam\\Libraries\\ExamQuestion";
            $type = get_uppercase_by_underscores($type);
            $questionClass .= "\\Answer" . $type . "\\Answer" . $type ;
            $questionClass = new $questionClass();
            $questionClass->setDataQuestion($data)->setAnswers($answers);
        } catch (\Exception $exception) {
            report($exception);
        }
        return $questionClass;
    }


}