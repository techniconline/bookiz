<?php

namespace Modules\Exam\Libraries\ExamQuestion;

use BridgeHelper;

abstract Class ExamAnswerBase
{
    protected $name = '';
    protected $data = [];
    protected $viewTypes = ['content', 'list', 'widget'];
    protected $course_id = null;
    protected $question = null;
    protected $answers = null;
    protected $user_answer = null;
    protected $exam_attempt_id = null;
    protected $user = null;

    /**
     * @param null $user
     * @return $this
     */
    public function setUser($user = null)
    {
        $this->user = $user?:BridgeHelper::getAccess()->getUser();
        return $this;
    }

    /**
     * @param $course_id
     * @return $this
     */
    public function setCourseId($course_id)
    {
        $this->course_id = $course_id;
        return $this;
    }

    /**
     * @param $attempt_id
     * @return $this
     */
    public function setAttemptId($attempt_id)
    {
        $this->exam_attempt_id = $attempt_id;
        return $this;
    }

    /**
     * @param $question
     * @return $this
     */
    public function setQuestion($question)
    {
        $this->question = $question;
        return $this;
    }

    /**
     * @return null
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param $answers
     * @return $this
     */
    public function setAnswers($answers)
    {
        if($this->answers && !$answers){
            return $this;
        }
        $this->answers = $answers;
        return $this;
    }

    /**
     * @return null
     */
    public function getAnswers()
    {
        return $this->answers;

    }

    /**
     * @param $user_answer
     * @return $this
     */
    public function setUserAnswer($user_answer)
    {
        $this->user_answer = $user_answer;
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setDataQuestion($data)
    {
        if (is_array($data)) {
            $data = (object)$data;
        }
        $this->data = $data;
        return $this;
    }

    /**
     * @param null $name
     * @param null $default
     * @return array|null
     */
    public function getData($name = null, $default = null)
    {
        if (is_null($name)) {
            return $this->data;
        }
        if (isset($this->data->{$name})) {
            return $this->data->{$name};
        }
        return $default;
    }


    /**
     * @param null $view
     * @return string
     * @throws \Throwable
     */
    public function getForm($view = null)
    {
        $blockViewModel =& $this;
        if (is_null($view)) {
            $view = 'exam::question.answer_types.' . $this->name . '.form';
        }
        return view($view, compact('blockViewModel'))->render();
    }


    /**
     * @param string $viewType
     * @param null $view
     * @param bool $returnJson
     * @return array|string
     * @throws \Throwable
     */
    public function getRenderView($viewType = 'content', $view = null, $returnJson = false)
    {
        $blockViewModel =& $this;
        if ($returnJson) {
            return $this->getDataJson();
        }

        if (is_null($view)) {
            $view = 'exam::question.answer_types.' . $this->name . '.' . $viewType;
        }
        return view($view, compact('blockViewModel'))->render();
    }

    /**
     * @return array
     */
    public function getDataJson()
    {
        $rowData = $this->getRowData();
        return $rowData;
    }

    /**
     * @return array
     */
    public function getRowData()
    {
        $data = [
            'item_type' => 'exam',
            'item_id' => $this->exam_attempt_id,
            'options' => [
                'question_id' => isset($this->question['exam_question']['id'])?$this->question['exam_question']['id']:null
            ],
            'question_text' => isset($this->question['exam_question']['question'])?$this->question['exam_question']['question']:null,
            'answers' => $this->getAnswers(),
            'answer_user' => null,
        ];
        return $data;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


}