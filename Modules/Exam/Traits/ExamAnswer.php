<?php

namespace Modules\Exam\Traits;


trait ExamAnswer
{

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsTrue($query)
    {
        return $query->where('is_true', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function examQuestion()
    {
        return $this->belongsTo('Modules\Exam\Models\ExamQuestion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function correctExamAnswerUsers()
    {
        return $this->hasMany('Modules\Exam\Models\ExamAnswerUser', 'correct_exam_answer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function examAnswerUsers()
    {
        return $this->hasMany('Modules\Exam\Models\ExamAnswerUser');
    }
}
