<?php

namespace Modules\Exam\Traits;


trait ExamQuestion
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function examQuestionBank()
    {
        return $this->belongsTo('Modules\Exam\Models\ExamQuestionBank');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function examAnswerUsers()
    {
        return $this->hasMany('Modules\Exam\Models\ExamAnswerUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function examAnswers()
    {
        return $this->hasMany(\Modules\Exam\Models\ExamAnswer::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function examAnswerTrue()
    {
        return $this->hasOne(\Modules\Exam\Models\ExamAnswer::class)->enable()->isTrue()->orderBy('id','DESC');
    }

}
