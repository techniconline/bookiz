<?php

namespace Modules\Exam\Traits;

use Modules\User\Models\User;

trait ExamQuestionUser
{

    /**
     * @param $value
     * @return mixed
     */
    public function getQuestionsAttribute($value)
    {
        if ($value) {
            $value = json_decode($value);
        }
        return $value;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
