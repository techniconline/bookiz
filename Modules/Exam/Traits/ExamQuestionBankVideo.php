<?php

namespace Modules\Exam\Traits;

use Modules\Video\Models\Video\Video;

trait ExamQuestionBankVideo
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function examQuestionBank()
    {
        return $this->belongsTo('Modules\Exam\Models\ExamQuestionBank');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function video()
    {
        return $this->belongsTo(Video::class);
    }

}
