<?php

namespace Modules\Exam\Traits;

use Modules\Course\Models\Course\CourseActivity;
use Modules\User\Models\User;

trait ExamAttempt
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseActivity()
    {
        return $this->belongsTo(CourseActivity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
