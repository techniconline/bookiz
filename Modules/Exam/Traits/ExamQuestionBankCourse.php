<?php

namespace Modules\Exam\Traits;

use Modules\Course\Models\Course\Course;

trait ExamQuestionBankCourse
{


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function examQuestionBank()
    {
        return $this->belongsTo('Modules\Exam\Models\ExamQuestionBank');
    }

}
