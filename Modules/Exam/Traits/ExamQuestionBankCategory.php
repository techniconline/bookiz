<?php

namespace Modules\Exam\Traits;


use Modules\Course\Models\Course\CourseCategory;

trait ExamQuestionBankCategory
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseCategory()
    {
        return $this->belongsTo(CourseCategory::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function examQuestionBank()
    {
        return $this->belongsTo('Modules\Exam\Models\ExamQuestionBank');
    }
}
