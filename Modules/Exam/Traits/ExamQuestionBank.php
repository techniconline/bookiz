<?php

namespace Modules\Exam\Traits;


use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Course\CourseCategory;
use Modules\Video\Models\Video\Video;

trait ExamQuestionBank
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo(Instance::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courseCategories()
    {
        return $this->belongsToMany(CourseCategory::class, 'exam_question_bank_categories', null, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courses()
    {
        return $this->belongsToMany(Course::class, 'exam_question_bank_courses');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function videos()
    {
        return $this->belongsToMany(Video::class, 'exam_question_bank_videos');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function examQuestions()
    {
        return $this->hasMany('Modules\Exam\Models\ExamQuestion');
    }
}
