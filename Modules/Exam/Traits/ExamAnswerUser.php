<?php

namespace Modules\Exam\Traits;


use Modules\User\Models\User;

trait ExamAnswerUser
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function correctExamAnswer()
    {
        return $this->belongsTo('Modules\Exam\Models\ExamAnswer', 'correct_exam_answer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function examAnswer()
    {
        return $this->belongsTo('Modules\Exam\Models\ExamAnswer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function examQuestion()
    {
        return $this->belongsTo('Modules\Exam\Models\ExamQuestion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
