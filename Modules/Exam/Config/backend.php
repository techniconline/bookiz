<?php

return [
    'version' => '20180619001',
    'blocks' => [
        'left' => [],
        'top' => []
    ],
    'menus' => [
        'management' => [],
        'management_admin' => [
            [
                'alias' => '', //**
                'route' => 'exam.question.bank.index', //**
                'key_trans' => 'exam::menu.exam_question_bank_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-question-circle', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_courses',
            ],

            [
                'alias' => '', //**
                'route' => 'exam.user.index', //**
                'key_trans' => 'exam::menu.exam_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-list', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_courses',
            ],


        ],
    ],
    'group_menu' => [
        'management' => [],
        'management_admin' => [
            ['alias' => 'admin_courses', //**
                'key_trans' => 'exam::menu.group.courses', //**
                'icon_class' => 'fa fa-graduation-cap',
                'before' => '',
                'after' => '',
                'order' => 10,
            ],
        ]
    ],

];
