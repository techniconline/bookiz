<?php

return [
    'name' => 'Exam',
    'permissions' => [
        'exam.question.answer.delete' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.answer.edit' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.answer.show' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.answer.update' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.answer.index' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.answer.create' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.answer.save' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.answer.save_answers' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.answer.section_create_answer' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.bank.index' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.bank.create' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.bank.save' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.bank.get_exam_question_bank_by_api' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.bank.delete' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.bank.edit' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.bank.show' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.bank.update' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.list.index' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.list.create' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.list.save' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.list.delete' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.list.edit' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.list.show' => ['type' => '|', 'access' => ['exam/manage.question.full']],
        'exam.question.list.update' => ['type' => '|', 'access' => ['exam/manage.question.full']],

        'exam.user.index' => ['type' => '|', 'access' => ['exam/manage.question.full', 'exam/manage.question.access.attempt']],
        'exam.user.attempt.list' => ['type' => '|', 'access' => ['exam/manage.question.full', 'exam/manage.question.access.attempt']],
        'exam.user.attempt.delete' => ['type' => '|', 'access' => ['exam/manage.question.full', 'exam/manage.question.access.attempt']],
        'exam.user.attempt.edit' => ['type' => '|', 'access' => ['exam/manage.question.full', 'exam/manage.question.access.attempt']],
        'exam.user.attempt.update' => ['type' => '|', 'access' => ['exam/manage.question.full', 'exam/manage.question.access.attempt']],
        'exam.user.attempt.change_status' => ['type' => '|', 'access' => ['exam/manage.question.full', 'exam/manage.question.access.attempt']],
    ]
];
