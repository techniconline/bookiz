<?php

namespace Modules\Exam\Bridge\Exam;
use Modules\Exam\Models\ExamAnswer;
use Modules\Exam\Models\ExamQuestion;
use Modules\Exam\Models\ExamQuestionBank;
use Modules\Exam\ViewModels\User\UserExamViewModel;


class Exam
{
    /**
     * @return string
     */
    public function getExamQuestionBankModel()
    {
        return new ExamQuestionBank();
    }
    /**
     * @return string
     */
    public function getExamQuestionModel()
    {
        return new ExamQuestion();
    }
    /**
     * @return string
     */
    public function getExamAnswerModel()
    {
        return new ExamAnswer();
    }

    /**
     * @return UserExamViewModel
     */
    public function getUserExamViewModel()
    {
        return new UserExamViewModel();
    }

    /**
     * @return string
     */
    public function getUrlSearchQuestionBank()
    {
        return route('exam.question.bank.get_exam_question_bank_by_api');
    }

    /**
     * @param $id
     * @return string
     */
    public function getUrlSingleQuestionBank($id = null)
    {
        if($id){
            return route('exam.question.bank.show', ['exam_question_bank_id'=>$id]);
        }
        return route('exam.question.bank.index');
    }

}