<?php

namespace Modules\Exam\ViewModels\User;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Course\Libraries\Activity\Exam\ExamActivity;
use Modules\Exam\Libraries\ExamQuestion\MasterExamQuestion;
use Modules\Exam\Models\ExamAnswerUser;
use Modules\Exam\Models\ExamAttempt;
use Modules\Exam\Models\ExamQuestion;
use Modules\Exam\Models\ExamAnswer;
use Modules\Exam\Models\ExamQuestionUser;

class UserExamViewModel extends BaseViewModel
{

    private $access_assets;
    private $exam_question_id;
    private $exam_question;
    private $exam_questions;
    private $data_exam;
    private $user_answers;
    private $view_user_answers;
    private $course;
    private $activity;
    private $exam_attempt;
    private $title_exam;
    private $filter_items = [];

    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {
            $access_assets [] = ['container' => 'theme_js', 'src' => ("assets/js/modules/exam/exam.js"), 'name' => 'exam-modules'];
        }

        return $access_assets;
    }


    /**
     * @return $this
     */
    protected function setAssetsRenderUserExam()
    {
        $this->access_assets = true;
        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->filter_items = $params ? $params : request()->except(array_keys($this->filter_items));
        $this->exam_question_id = isset($this->filter_items['exam_question_id']) ? $this->filter_items['exam_question_id'] : 0;
        return $this;
    }

    /**
     * @param $data
     * @return $this
     * @throws \Throwable
     */
    public function renderUserExam($data)
    {
        $this->access_assets = true;
        $this->data_exam = $data;
        $this->setExamQuestions($data);
        $viewModel =& $this;
        $user_name = null;
        if (isset($data['user']) && $data['user']) {
            $user_name = $data['user']->full_name;
        }

        $user_answers = null;

        $this->activity = $activity = isset($data['activity']) ? $data['activity'] : null;
        $this->course = $activity ? $activity->course : '';
        if (isset($data['data']) && $data['data']) {
            $this->exam_attempt = $data['data'];
            $exam_title = $activity ? $activity->title : '-';
            if (!$data['action']) {
                $user_answers = $this->renderUserAnswers($data['data']);
            }
        }

        $exam_title = $activity ? $activity->title : null;

        $this->setTitlePage(trans('exam::exam.exam_running_for_user', ['exam_title' => $exam_title, 'course_name' => $this->course->title]));
        $this->title_exam = trans('exam::exam.exam_running_for_user', ['exam_title' => $exam_title, 'course_name' => $this->course->title]);
        if (app("getInstanceObject")->isApi()) {
            $data = $this->getAnswers($data);
            $data['user_answers'] = $user_answers;
            return $data;
        }

        return $this->renderedView("exam::course_activity.exam.output", ['data' => $data, 'view_model' => $viewModel], "form");
    }


    private function getAnswers($data)
    {
        if (isset($data['exam_data']['data']['exam']) && $data['exam_data']['data']['exam']) {
            foreach ($data['exam_data']['data']['exam'] as &$datum) {
                $datum = $this->getRowQuestion($datum);
            }
        }
        return $data;
    }

    /**
     * @return mixed
     */
    public function getTitleExam()
    {
        return $this->title_exam;
    }

    /**
     * @param null $field
     * @return mixed
     */
    public function getActivity($field = null)
    {
        if ($field) {
            if (isset($this->activity->$field)) {
                return $this->activity->$field;
            } else {
                return null;
            }
        }
        return $this->activity;
    }

    /**
     * @param null $key
     * @return array
     */
    public function getParamsActivity($key = null)
    {

        if (!$this->activity) {
            return null;
        }
        if ($key) {
            if (isset($this->activity->params->$key)) {
                return $this->activity->params->$key;
            } else {
                return null;
            }
        }
        return $this->activity->params;

    }

    /**
     * @param null $key
     * @return array
     */
    public function getDataExam($key = null)
    {

        if (!$this->data_exam) {
            return null;
        }
        if ($key) {
            if (isset($this->data_exam[$key])) {
                return $this->data_exam[$key];
            } else {
                return null;
            }
        }
        return $this->data_exam;

    }

    /**
     * @return mixed
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getExamTime()
    {
        $exp_seconds = $seconds = $minutes = 0;
        if (isset($this->data_exam['exam_data']['data']['exp_time']) && $this->data_exam['exam_data']['data']['exp_time']) {

            $end_time = strtotime($this->data_exam['exam_data']['data']['exp_time']);
            $now_time = strtotime($this->data_exam['exam_data']['data']['now_time']);

            if ($end_time >= $now_time) {
                $exp_seconds = $end_time - $now_time;
                $minutes = (int)($exp_seconds / 60);
                $seconds = fmod($exp_seconds, 60);
            }

        }

        return ['text' => trans('exam::exam.exam_time', ['minutes' => $minutes, 'seconds' => $seconds]), 'seconds' => $seconds
            , 'minutes' => $minutes, 'total_seconds' => $exp_seconds, 'total_minutes' => ($exp_seconds / 60), 'origin_minutes' => $this->getParamsActivity('exam_total_time')];
    }

    /**
     * @param bool $json
     * @return mixed
     */
    public function getUserAnswers($json = false)
    {
        if ($json) {
            return $this->user_answers;
        }
        return $this->view_user_answers;
    }


    /**
     * @param $attempt
     * @return array|bool|string
     * @throws \Throwable
     */
    public function renderUserAnswers($attempt)
    {
        if (!$attempt) {
            return false;
        }
        $view_model =& $this;
        $model = new ExamAnswerUser();
        $answers = $model->enable()->where('exam_attempt_id', $attempt->id)->with(['examQuestion', 'examAnswer', 'correctExamAnswer'])->get();

        $this->user_answers = $answers;
        if (app("getInstanceObject")->isApi()) {
            $answersList = $this->userAnswers($answers);
            return $answersList;
        }
        $answersList = $this->decorateAnswer($answers);
        $this->view_user_answers = $view = view("exam::course_activity.exam.user_answers", compact('answers', 'answersList', 'view_model'))->render();

        return $view;
    }


    private $uAnswer;
    private $cAnswer;

    /**
     * @param $answers
     * @return array
     */
    private function userAnswers($answers)
    {

        $data = [];
        if ($this->getParamsActivity('show_final_score') && $this->getParamsActivity('show_final_score') == 'only_score_with_answer') {
            $data = $this->decorateAnswer($answers);
            $data['scores'] = ['exam_total_score' => $this->getParamsActivity('exam_total_score'), 'exam_passing_score' => $this->getParamsActivity('exam_passing_score'), 'user_score' => $this->getAttempt('score')];
            $data['passed'] = false;
            $data['message'] = trans('exam::exam.user_exam_fail');
            if ($this->getAttempt('score') >= $this->getParamsActivity('exam_passing_score')) {
                $data['passed'] = true;
                $data['message'] = trans('exam::exam.user_exam_passed');
            }
        } elseif ($this->getParamsActivity('show_final_score') && $this->getParamsActivity('show_final_score') == 'only_passed_exam') {

            $data['passed'] = false;
            $data['message'] = trans('exam::exam.user_exam_fail');
            if ($this->getAttempt('score') >= $this->getParamsActivity('exam_passing_score')) {
                $data['passed'] = true;
                $data['message'] = trans('exam::exam.user_exam_passed');
            }

        } elseif ($this->getParamsActivity('show_final_score') && $this->getParamsActivity('show_final_score') == 'only_score') {
            $data['scores'] = ['exam_total_score' => $this->getParamsActivity('exam_total_score'), 'exam_passing_score' => $this->getParamsActivity('exam_passing_score'), 'user_score' => $this->getAttempt('score')];
            $data['passed'] = false;
            $data['message'] = trans('exam::exam.user_exam_fail');
            if ($this->getAttempt('score') >= $this->getParamsActivity('exam_passing_score')) {
                $data['passed'] = true;
                $data['message'] = trans('exam::exam.user_exam_passed');
            }
        } else {
            $data['message'] = trans('exam::exam.result_not_show_user');
        }

        return $data;
    }

    /**
     * @param $answers
     * @return array
     */
    private function decorateAnswer($answers)
    {
        $questionUser = $this->getExamQuestionUser();
        $list_answers = isset($questionUser->questions->list_answers) ? $questionUser->questions->list_answers : [];
        $list_answers = collect($list_answers);
        $data = [];
        $answerModel = new ExamAnswer();
        foreach ($answers as $index => $answer) {
            if ($answer->examQuestion->type == 'four_options') {
                $this->cAnswer = null;
                $this->uAnswer = null;
                $list_answers->each(function ($row, $key) use ($answer, $answerModel) {
                    if ($answer->exam_question_id == $key) {
                        $uAnswer = collect($row)->where('id', $answer->exam_answer_id)->keys()->first();
                        $cAnswer = collect($row)->where('id', $answer->correct_exam_answer_id)->keys()->first();
                        $uAnswer = $answerModel->getRowAlpha($uAnswer);
                        $cAnswer = $answerModel->getRowAlpha($cAnswer);
                        $this->uAnswer = $uAnswer;
                        $this->cAnswer = $cAnswer;
                    }
                });
                $data['answers'][$index]['answer'] = $this->uAnswer;
                $data['answers'][$index]['correct_answer'] = $this->cAnswer;
                $data['answers'][$index]['exam_question_id'] = $answer->exam_question_id;
                $data['answers'][$index]['correct_exam_answer_id'] = $answer->correct_exam_answer_id;
                $data['answers'][$index]['exam_answer_id'] = $answer->exam_answer_id;
                $data['answers'][$index]['is_accept'] = boolval($answer->exam_answer_id == $answer->correct_exam_answer_id);

            } else {
                $data['answers'][$index]['answer'] = $answer->examAnswer ? $answer->examAnswer->answer : '-';
                $data['answers'][$index]['correct_answer'] = ($answer->exam_answer_id != $answer->correct_exam_answer_id) ? $answer->correctExamAnswer->answer : null;
            }
            $data['answers'][$index]['question'] = $answer->examQuestion->question;
        }
        return $data;
    }

    private function getExamQuestionUser()
    {
        $model = new ExamQuestionUser();
        $attempt_id = $this->data_exam['data']->id;
        $user_id = $this->data_exam['data']->user_id;
        $userQuestions = $model->active()->where('exam_attempt_id', $attempt_id)->where('user_id', $user_id)->orderBy('id', 'DESC')->first();
        return $userQuestions;
    }

    /**
     * @param null $field
     * @return mixed
     */
    public function getAttempt($field = null)
    {
        if ($field) {
            if (isset($this->exam_attempt->$field)) {
                return $this->exam_attempt->$field;
            } else {
                return null;
            }
        }
        return $this->exam_attempt;
    }


    /**
     * @param $data
     * @return $this
     */
    public function setExamQuestions($data)
    {
        if (isset($data['exam_data']['data']['exam']) && $data['exam_data']['data']['exam']) {
            $this->exam_questions = $data['exam_data']['data']['exam'];
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExamQuestions()
    {
        return $this->exam_questions;
    }

    /**
     * @param $question
     * @param int $row_number
     * @return null
     */
    public function getRowQuestion($question, $row_number = 1)
    {
        $masterExamQuestion = new MasterExamQuestion();
        if (isset($question['question'])) {
            $question['exp_time'] = $this->data_exam['exam_data']['data']['exp_time'];
            $data['activity'] = $this->activity;
            $data['attempt'] = $this->exam_attempt;
            $data['row_number'] = $row_number;
            return $masterExamQuestion->getQuestion($question, $data)->getRenderView('content', null, app("getInstanceObject")->isApi());
        }
        return null;
    }

    /**
     * @param $attempt
     * @param $exam_tolerance_time
     * @return bool
     */
    private function checkAttempt($attempt, $exam_tolerance_time)
    {
        $end_date = strtotime($attempt->end_time);
        $now_time = time();
        $exam_tolerance_time_sec = $exam_tolerance_time * 60;

        $now_time += $exam_tolerance_time_sec;
        if ($end_date >= $now_time) {
            return true;
        }
        return false;
    }

    public function saveAnswer($user = null, $attempt_id = null)
    {
        $single = $this->request?$this->request->get('single'):null;
        if ($single) {
            $result = $this->saveSingleAnswerUser();
            return $this->setResponse($result['action'], $result['message']);
        }

        $user = $user ? $user : BridgeHelper::getAccess()->getUser();
        $answers = $this->request ? $this->request->get('answers') : null;
        $attempt_id = $attempt_id ? $attempt_id : $this->request->get('attempt_id');
        $result = $this->getExamQuestion($attempt_id, $user->id);

        $modelExamAnswerUser = new ExamAnswerUser();

        $questions = $result['questions'];
        $attempt = $result['attempt'];
        $this->activity = $activity = $attempt ? $attempt->courseActivity : null;
        $course = $activity ? $activity->course : null;
        $paramsActivity = $activity ? $activity->params : null;

        $exam_tolerance_time = isset($paramsActivity->exam_tolerance_time) ? $paramsActivity->exam_tolerance_time : $modelExamAnswerUser::DEFAULT_TOLERANCE_TIME;
        $exam_total_score = isset($paramsActivity->exam_total_score) ? $paramsActivity->exam_total_score : $modelExamAnswerUser::DEFAULT_SCORE;

        //
        $attempt_status = $this->checkAttempt($attempt, $exam_tolerance_time);
        $questionIds = array_keys($questions);

        $insertItem = [];
        $resInsert = false;
        $total_question_score = $total_user_score = 0;
        $examAnswerUserLog = $this->getExamAnswerUserLog($user->id, $attempt);

        //get answer user in another process
        if (empty($answers)) {
            $answers = $this->generateUserAnswers($examAnswerUserLog);
        }

        foreach ($questionIds as $questionId) {
            $answer_id = $answer = $correct_answer_text = $correct_answer_id = null;
            $question = $questions[$questionId];

            if (!$question) {
                continue;
            }

            $total_question_score += $question->score ?: 0;
            $resAnswer = $this->saveSingleAnswerUserCompleted($examAnswerUserLog, $answers, $question, $questionId, $attempt_id, $attempt, $user, $activity, $attempt_status);
            if ($resAnswer['action']) {
                $answer = $resAnswer['data'];
                $resInsert = true;
                $total_user_score += $answer ? $answer->score : 0;
            }


        }

        if ($resInsert) {
            $final_score = $this->calcFinalScore($exam_total_score, $total_question_score, $total_user_score);
            $status = ExamAttempt::STATUS_CLOSE;
            $active = 2;
            if (!$attempt_status) {
                $status = ExamAttempt::STATUS_EXPIRE_TIME;
            }
            $this->updateAttempt($attempt, $final_score, $total_question_score, $active, $status);
        } else {
            $this->updateAttempt($attempt, 0, 0, 0);
        }

        return $this->redirect(route('course.activity', ['course_id' => $course ? $course->id : 0, 'activity_id' => $activity ? $activity->id : 0]))
            ->setResponse($resInsert, trans("exam::messages.alert." . ($resInsert ? 'success_answer_exam' : 'un_success_answer_exam')));
    }

    /**
     * @param array $answers
     * @param null $question
     * @param null $question_id
     * @param null $attempt_id
     * @param null $attempt
     * @param null $user
     * @param null $activity
     * @return array
     */
    protected function saveSingleAnswerUser($answers = [], $question = null, $question_id = null, $attempt_id = null, $attempt = null, $user = null, $activity = null)
    {
        $user = $user ?: BridgeHelper::getAccess()->getUser();
        $answers = $answers ?: $this->request->get('answers');
        $question_id = $question_id ?: key($answers);
        $attempt_id = $attempt_id ?: $this->request->get('attempt_id');

        if (!$user || !$answers || !$question_id || !$attempt_id) {
            return ['action' => false, 'message' => trans("exam::messages.alert.miss_date")];
        }

        $user_answer = isset($answers[$question_id]) ? $answers[$question_id] : null;

        $user_answer_id = array_first($user_answer);

        $data = [
            'user_id' => $user->id,
            'exam_question_id' => $question_id,
            'exam_answer_id' => $user_answer_id,
            'exam_attempt_id' => $attempt_id,
            'active' => 2,
            'status' => "'" . ExamAnswerUser::STATUS_ANSWERED . "'",
            'created_at' => "'" . date("Y-m-d H:i:s") . "'",
        ];

        $result = DB::statement(" REPLACE INTO exam_answer_users_log (" . implode(",", array_keys($data)) . ") VALUES(" . implode(",", array_values($data)) . ");");

        if ($result) {
            return ['action' => true, 'data' => $result, 'message' => trans("exam::messages.alert.save_success")];
        }
        return ['action' => false, 'message' => trans("exam::messages.alert.save_un_success")];

    }

    /**
     * @param array $examAnswerUserLog
     * @param array $answers
     * @param null $question
     * @param null $question_id
     * @param null $attempt_id
     * @param null $attempt
     * @param null $user
     * @param null $activity
     * @param bool $attempt_status
     * @return array
     */
    protected function saveSingleAnswerUserCompleted($examAnswerUserLog = [], $answers = [], $question = null, $question_id = null, $attempt_id = null, $attempt = null, $user = null, $activity = null, $attempt_status = true)
    {
        $user = $user ?: BridgeHelper::getAccess()->getUser();
        $answers = $answers ?: ($this->request?$this->request->get('answers'):[]);
        $question_id = $question_id ?: key($answers);
        $attempt_id = $attempt_id ?: ($this->request?$this->request->get('attempt_id'):null);

        $modelExamAnswerUser = new ExamAnswerUser();
        $modelExamQuestion = new ExamQuestion();
        $attempt = $attempt ?: ExamAttempt::find($attempt_id);
        $question = $question ?: $modelExamQuestion->find($question_id);

        if (!$question) {
            return ['action' => false, 'message' => trans("exam::messages.alert.miss_date")];
        }

        $userAnswer = $modelExamAnswerUser->enable()->where('user_id', $user->id)->where('exam_attempt_id', $attempt_id)->where('exam_question_id', $question_id)->first();
        $correct_answer = $question->examAnswerTrue;
        $correct_answer_id = $correct_answer ? $correct_answer->id : null;
        $user_answer = isset($answers[$question_id]) ? $answers[$question_id] : [];
        $answer = $correct_answer_text = $user_answer_id = null;

        $by_calc = true;
        if (!$attempt_status) {
            if (isset($examAnswerUserLog[$question_id])) {
                $user_answer_log = $examAnswerUserLog[$question_id];
                $user_answer_id = $user_answer_log->exam_answer_id;
                if ($question->type == $modelExamQuestion::QUESTION_TYPE_FOUR_OPTIONS) {
                    $user_answer = [$examAnswerUserLog[$question_id]->exam_answer_id];
                } else {
                    $user_answer = [$examAnswerUserLog[$question_id]->exam_answer];
                }
            } else {
                $by_calc = false;
            }
        }

        if ($question->type == $modelExamQuestion::QUESTION_TYPE_FOUR_OPTIONS) {
            $correct_answer_id = $correct_answer ? $correct_answer->id : null;
            $user_answer_id = array_first($user_answer);
        } else {
            $correct_answer_text = $correct_answer ? $correct_answer->id : null;
            $answer = array_first($user_answer);
        }


        if (!$activity) {
            $this->activity = $activity = $attempt ? $attempt->courseActivity : null;
        }
        $paramsActivity = $activity ? $activity->params : null;

        $exam_total_score = isset($paramsActivity->exam_total_score) ? $paramsActivity->exam_total_score : $modelExamAnswerUser::DEFAULT_SCORE;

        $score = $user_answer && $by_calc ? $this->getScore($exam_total_score, $user_answer, $question) : 0;

        $data = [
            'user_id' => $user->id,
            'exam_question_id' => $question_id,
            'exam_answer_id' => $user_answer_id,
            'correct_exam_answer_id' => $correct_answer_id,
            'exam_attempt_id' => $attempt_id,
            'exam_answer' => $answer,
            'correct_exam_answer' => $correct_answer_text,
            'score' => $score,
            'final_score' => $score,
            'start_time' => $attempt->start_time,
            'end_time' => $attempt->end_time,
            'active' => $attempt_status ? 1 : 3,
            'status' => $modelExamAnswerUser::STATUS_CALCULATED
        ];

        if (!$userAnswer) {
            $userAnswer = new ExamAnswerUser();
        }

        $userAnswer->exam_answer_id = $user_answer_id;
        $userAnswer->exam_answer = $answer;


        if ($userAnswer->fill($data)->save()) {
            return ['action' => true, 'data' => $userAnswer, 'message' => trans("exam::messages.alert.save_success")];
        }
        return ['action' => false, 'message' => trans("exam::messages.alert.save_un_success")];

    }

    /**
     * @param $examAnswerUserLog
     * @return array
     */
    private function generateUserAnswers($examAnswerUserLog)
    {
        $answers = [];
        foreach ($examAnswerUserLog as $question_id => $item) {
            $answers[$question_id][] = $item->exam_answer_id;
        }
        return $answers;
    }

    /**
     * @param $user_id
     * @param $attempt
     * @return mixed
     */
    private function getExamAnswerUserLog($user_id, $attempt)
    {
        $result = DB::select('select * from exam_answer_users_log where user_id=' . $user_id . ' and exam_attempt_id=' . $attempt->id . ' and created_at between "' . $attempt->start_time . '" AND "' . $attempt->end_time . '"');

        if (!$result) {
            return $result;
        }

        $newData = [];
        foreach ($result as $item) {
            $newData[$item->exam_question_id] = $item;
        }
        return $newData;
    }

    private function closeExamAnswerUserLog($user_id, $attempt)
    {
        $result = DB::statement('update exam_answer_users_log set active=3, status="' . ExamAnswerUser::STATUS_CALCULATED . '" where user_id=' . $user_id . ' and exam_attempt_id=' . $attempt->id);
        return $result;
    }

    /**
     * @param $exam_total_score
     * @param $total_question_score
     * @param $total_user_score
     * @return float|int
     */
    private function calcFinalScore($exam_total_score, $total_question_score, $total_user_score)
    {
        $score = $exam_total_score * $total_user_score;
        $score = $score / $total_question_score;
        $score = (float)number_format($score, 2);
        return $score;
    }

    /**
     * @param $exam_total_score
     * @param $user_answer
     * @param $question
     * @return int
     */
    private function getScore($exam_total_score, $user_answer, $question)
    {
        $modelExamQuestion = new ExamQuestion();
        $correct_answer = $question->examAnswerTrue;

        if ($question->type == $modelExamQuestion::QUESTION_TYPE_FOUR_OPTIONS) {
            $correct_answer_id = $correct_answer ? $correct_answer->id : null;
            $user_answer = is_array($user_answer) ? array_first($user_answer) : $user_answer;
            if ($user_answer == $correct_answer_id) {
                $score = $question->score;
                return $score;
            }
            $mines_score = (int)$this->getParamsActivity('mines_score');
            if ($mines_score && $question->score) {
                $score = -($question->score / $mines_score);
                return $score;
            }
            return 0;
        } else {
            return 0;
        }
    }

    /**
     * @param $attempt_id
     * @param $user_id
     * @return array
     */
    private function getExamQuestion($attempt_id, $user_id)
    {
        $attempt = ExamAttempt::active()->with('courseActivity')->find($attempt_id);

        $examQuestionUser = new ExamQuestionUser();
        $userQuestions = $examQuestionUser->active()->where('user_id', $user_id)->where('exam_attempt_id', $attempt_id)->orderBy('id', 'DESC')->first();
//            ->with(['examQuestion.examAnswerTrue'])->get();

        $question_ids = isset($userQuestions->questions->list_questions) ? $userQuestions->questions->list_questions : [];

        if (!$question_ids) {
            return ['questions' => [], 'attempt' => []];
        }

        $list = ExamQuestion::active()->whereIn('id', $question_ids)->with(['examAnswerTrue'])->get();

        $questions = [];
        foreach ($list as $item) {
            $questions[$item->id] = $item;

        }

        return ['questions' => $questions, 'attempt' => $attempt];

    }

    /**
     * @param $attempt
     * @param $final_score
     * @param $question_score
     * @param int $active
     * @param string $status
     * @return bool
     */
    private function updateAttempt($attempt, $final_score, $question_score, $active = 1, $status = ExamAttempt::STATUS_CLOSE)
    {
        if (!$attempt) {
            return false;
        }

        $attempt->score = $final_score;
        $attempt->relative_score = $question_score;
        $attempt->active = $active;
        $attempt->status = $status;

        $user_id = $attempt->user_id;
        $attempt_id = $attempt->id;

        $examQuestionsCacheKey = 'exam_question_user_' . $user_id . '_attempt_' . $attempt_id;
        $keyCacheExpTime = $examQuestionsCacheKey . '_time';
        if (Cache::has($examQuestionsCacheKey)) {
            Cache::forget($keyCacheExpTime);
            Cache::forget($examQuestionsCacheKey);
        }

        return $attempt->save();
    }


}
