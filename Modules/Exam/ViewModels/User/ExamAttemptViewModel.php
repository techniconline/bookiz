<?php

namespace Modules\Exam\ViewModels\User;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Exam\Models\ExamAttempt;
use Modules\Exam\Models\ExamAttemptLog;

class ExamAttemptViewModel extends BaseViewModel
{

    private $access_assets;
    private $exam_question_bank_id;
    private $exam_question_bank;
    private $filter_items = [];

    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {
            $access_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/exam/exam.js"), 'name' => 'exam-backend'];
        }

        return $access_assets;
    }


    /**
     * @return $this
     */
    protected function setAssetsCreateAttemptBank()
    {
        $this->access_assets = true;
        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->filter_items = $params ? $params : request()->except(array_keys($this->filter_items));
        $this->exam_question_bank_id = isset($this->filter_items['exam_question_bank_id']) ? $this->filter_items['exam_question_bank_id'] : 0;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createAttempt()
    {
        $this->init();
        $model = new ExamAttempt();
        $model = $model->find($this->request->get('exam_attempt_id'));
        $viewModel =& $this;

        $this->setTitlePage(trans('exam::exam.attempt.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("exam::user.attempt.form_attempt", ['attempt' => $model, 'view_model' => $viewModel], "form");
    }

    /**
     * @return ExamAttemptViewModel
     * @throws \Throwable
     */
    protected function editAttempt()
    {
        return $this->createAttempt();
    }

    /**
     * @return $this
     */
    protected function destroyAttempt()
    {
        $exam_attempt_id = $this->request->get('exam_attempt_id');
        $question = ExamAttempt::enable()->find($exam_attempt_id);

        if ($question->delete()) {
            return $this->setResponse(true, trans("exam::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("exam::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveAttempt()
    {

        $data = $this->request->all();
        $response = $this->serviceSaveAttemptLog($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('exam.user.attempt.edit', ['exam_attempt_id' => $response['data']->exam_attempt_id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }


    /**
     * @param $data
     * @return array
     */
    public function serviceSaveAttemptLog($data)
    {
        $examAttempt = new ExamAttempt();

        $exam_attempt_id = isset($data['exam_attempt_id']) ? $data['exam_attempt_id'] : 0;
        if ($exam_attempt_id) {
            $examAttempt = $examAttempt->enable()->find($exam_attempt_id);
            if (!$examAttempt) {
                return ['action' => false, 'message' => trans("exam::messages.alert.not_find_data")];
            }
        }else{
            return ['action' => false, 'message' => trans("exam::messages.alert.mis_data"), 'errors' => []];
        }

        //save attempt
        $examAttempt->active = $data['active'];
        $examAttempt->relative_score = $data['relative_score'];
        $examAttempt->save();


        //save log
        $data_json = [
            'employee' => BridgeHelper::getAccess()->getUser()->id,
            'employee_name' => BridgeHelper::getAccess()->getUser()->full_name,
            'description' => isset($data['description']) ? $data['description'] : null,
            'score' => $examAttempt->score,
            'relative_score' => isset($data['relative_score']) ? $data['relative_score'] : null,
        ];

        $model = new ExamAttemptLog();
        $data['user_id'] = $examAttempt->user_id;
        $data['active'] = 1;
        $data['data'] = json_encode($data_json);

        if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
            $model->fill($data);
            if ($model->save()) {
                return ['action' => true, 'message' => trans("exam::messages.alert.save_success"), 'data' => $model];
            } else {
                return ['action' => false, 'message' => trans("exam::messages.alert.save_un_success")];
            }
        }
        return ['action' => false, 'message' => trans("exam::messages.alert.mis_data"), 'errors' => $this->errors];
    }

    /**
     * @param $state_publish
     * @param $attempt_id
     * @return array
     */
    public function serviceChangePublish($state_publish, $attempt_id)
    {
        $state = $state_publish;
        $state_publish = $state == '2' ? 1 : 2;
        $attempt = new ExamAttempt();
        $model = $attempt->find($attempt_id);
        $model->active = $state_publish;

        if ($model->save()) {
            return ['action' => true, 'message' => trans("feedback::messages.alert.save_success"), 'data' => $model];
        }
        return ['action' => false, 'message' => trans("feedback::messages.alert.save_un_success")];
    }

    /**
     * @return $this
     */
    protected function changeStatus()
    {
        $exam_attempt_id = $this->request->get('exam_attempt_id');
        $state = $this->request->get('status');
        $response = $this->serviceChangePublish($state, $exam_attempt_id);
        return $this->setResponse($response['action'], $response['message']);
    }

}
