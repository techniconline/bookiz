<?php

namespace Modules\Exam\ViewModels\User;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Exam\Models\ExamAttempt;

class ExamAttemptGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $filter_items = [];
    private $course_activity_id;
    private $attempt_grid;
    private $activity_grid;

    public function __construct()
    {
    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->useModel = new ExamAttempt();
        $this->filter_items = $params ? $params : request()->except(array_keys($this->filter_items));
        $this->course_activity_id = $this->filter_items['course_activity_id'];
        return $this;
    }

    /**
     *
     */
    public function setGridAttemptModel()
    {
        $this->init();
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->filters()->enable()->with(['user', 'courseActivity']);
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        //use bested assets
        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }


    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridListAttempt()
    {
        $this->setGridAttemptModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('user', trans('exam::exam.attempt.user'), true);
        $this->addColumn('score', trans('exam::exam.attempt.score'), true);
        $this->addColumn('relative_score', trans('exam::exam.attempt.relative_score'), true);

        $this->addColumn('created_at', trans('exam::exam.created_at'), true);
        $this->addColumn('updated_at', trans('exam::exam.updated_at'), false);

        /*add action*/
        $add = array(
            'name' => 'exam.user.index',
            'parameter' => null
        );
        $this->addButton('list_activity', $add, trans('exam::exam.attempt.list_activity'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);

        $show = array(
            'name' => 'exam.user.attempt.edit',
            'parameter' => ['id']
        );
        $this->addAction('content_edit', $show, trans('exam::exam.attempt.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'exam.user.attempt.change_status',
            'parameter' => ['id', 'active']
        );
        $this->addAction('publish', $show, trans('exam::exam.attempt.change_status'), 'fa fa-eye-slash', false
            , ['target' => '', 'class' => 'btn btn-sm btn-default _ajax_confirm', 'data-method' => "POST", 'data-message-confirm' => trans('exam::exam.confirm')]);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('exam::exam.confirm_delete')];
        $delete = array(
            'name' => 'exam.user.attempt.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('exam::exam.delete'), 'fa fa-trash', false, $options);

        /**/
        $this->action_text = false;

        /*filter*/
        $this->addFilter('last_name', 'text', ['relation'=>'user']);
        $this->addFilter('first_name', 'text', ['relation'=>'user']);
        return $this;
    }


    /**
     * @param array $params
     * @return $this
     */
    public function initActivity($params = [])
    {
        $this->useModel = BridgeHelper::getCourse()->getCourseActivityModel();
        $this->filter_items = $params ? $params : request()->except(array_keys($this->filter_items));
        $this->course_activity_id = isset($this->filter_items['course_activity_id']) ? $this->filter_items['course_activity_id'] : 0;
        return $this;
    }

    /**
     *
     */
    public function setGridActivityModel()
    {
        $this->initActivity();
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->where('type', 'exam')->filters()->enable();
    }

    private function generateGridListActivity()
    {
        $this->setGridActivityModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('course::course.activity.title'), false);
        $this->addColumn('sort', trans('course::course.activity.sort'), true);
        $this->addColumn('type', trans('course::course.activity.type'), true);
        $this->addColumn('show_type_text', trans('course::course.activity.show_type'), false);
        $this->addColumn('active_date', trans('course::course.activity.active_date'), false);
        $this->addColumn('expire_date', trans('course::course.activity.expire_date'), false);
        $this->addColumn('is_public_text', trans('course::course.activity.is_public'), false);
        $this->addColumn('active', trans('course::course.activity.status'), false);


        $show = array(
            'name' => 'exam.user.attempt.list',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('exam::exam.attempt.list'), 'fa fa-list', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
//        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('course::course.confirm_delete')];
//        $delete = array(
//            'name' => 'course.activity.delete',
//            'parameter' => 'id',
//        );
//        $this->addAction('delete', $delete, trans('course::course.activity.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = true;

        /*filter*/
        $this->addFilter('title', 'text');
        $this->addFilter('active_date', 'text');
        $this->addFilter('expire_date', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        if ($this->attempt_grid) {
            $row->user = $row->user ? $row->user->full_name : $row->user_id;
        }

        if ($this->activity_grid) {
            $row->active = trans("course::course.statuses." . $row->active);
        }

        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
        if ($this->attempt_grid) {
            if ($row->active == 1) {
                $action['publish']->icon = 'fa fa-eye';
            } else {
                $action['publish']->icon = 'fa fa-eye-slash';
            }
        }

        return $action;

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getListAttemptGrid()
    {
        $this->attempt_grid = true;
        $this->setTitlePage(trans('exam::exam.attempt.list'));
        $this->generateGridListAttempt()->renderedView("exam::user.attempt.index", ['view_model' => $this], "attempt_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getListActivityGrid()
    {
        $this->activity_grid = true;
        $this->setTitlePage(trans('exam::exam.activity.list'));
        $this->generateGridListActivity()->renderedView("exam::user.activity.index", ['view_model' => $this], "activity_list");
        return $this;
    }


}
