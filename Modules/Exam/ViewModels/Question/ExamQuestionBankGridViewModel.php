<?php

namespace Modules\Exam\ViewModels\Question;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Exam\Models\ExamAnswer;
use Modules\Exam\Models\ExamQuestion;
use Modules\Exam\Models\ExamQuestionBank;

class ExamQuestionBankGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $filter_items = [];

    public function __construct()
    {
        $this->useModel = new ExamQuestionBank();
    }

//    public function convert()
//    {
//        $answers = ['answer_a' => 'الف', 'answer_c' => 'ج', 'answer_b' => 'ب', 'answer_d' => 'د'];
//        $res = DB::table('exam_mafahim')->where('level', 'آسان')->select('*')->get();
//
//        $data = [];
//        foreach ($res as $index => $re) {
//            $data['questions'][$index]['answer_a'] = $re->answer_a;
//            $data['questions'][$index]['answer_b'] = $re->answer_b;
//            $data['questions'][$index]['answer_c'] = $re->answer_c;
//            $data['questions'][$index]['answer_d'] = $re->answer_d;
//
//            $model = new ExamQuestion();
//            $data_q = [
//                'question' => trim($re->question),
//                'type' => 'four_options',
//                'exam_question_bank_id' => 7,
//                'time' => 0,
//                'score' => 1,
//                'active' => 3,
//            ];
//
//            if ($model->fill($data_q)->save())
//            {
//                $qid = $model->id;
//                //answers
//                foreach ($data['questions'][$index] as $key => $answer) {
//                    $model = new ExamAnswer();
//                    $data_a = [
//                        'exam_question_id' => $qid
//                        , 'answer' => trim($answer)
//                        , 'is_true' => ($answers[$key] == $re->answer_true) ? 1 : 0
//                    ];
//                    $a = $model->fill($data_a)->save();
//                }
//            }
//
//        }
//
//
//    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->filter_items = $params ? $params : request()->all();
        return $this;
    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->init();
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->enable()
            ->filterCurrentInstance()
            ->filterLanguage()
            ->with('instance');
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        //use bested assets
        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }


    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('exam::exam.question_bank.title'), true);
        $this->addColumn('type', trans('exam::exam.question_bank.type'), true);
        $this->addColumn('created_at', trans('exam::exam.created_at'), false);
        $this->addColumn('updated_at', trans('exam::exam.updated_at'), false);
        $this->addColumn('active', trans('exam::exam.active'), false);

        /*add action*/
        $add = array(
            'name' => 'exam.question.bank.create',
            'parameter' => null
        );
        $this->addButton('create_question', $add, trans('exam::exam.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        $show = array(
            'name' => 'exam.question.bank.edit',
            'parameter' => ['id']
        );
        $this->addAction('content_edit', $show, trans('exam::exam.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'exam.question.list.index',
            'parameter' => ['id']
        );
        $this->addAction('list_question', $show, trans('exam::exam.question.list'), 'fa fa-list-ol', false, ['target' => '', 'class' => 'btn btn-sm btn-primary']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('exam::exam.confirm_delete')];
        $delete = array(
            'name' => 'exam.question.bank.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('exam::exam.delete'), 'fa fa-trash', false, $options);

        /**/
        $this->action_text = false;

        /*filter*/
        $this->addFilter('title', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->active = trans('exam::exam.statuses.' . $row->active);
        $row->type = $row->type ? trans('exam::exam.question_bank.types.' . $row->type) : '-';
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
//        if ($row->published) {
//            $action['publish']->icon = 'fa fa-eye';
//        } else {
//            $action['publish']->icon = 'fa fa-eye-slash';
//        }
        return $action;

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getListGrid()
    {
        $this->setTitlePage(trans('exam::exam.question_bank.list'));
        $this->generateGridList()->renderedView("exam::question.bank.index", ['view_model' => $this], "question_bank_list");
        return $this;
    }


}
