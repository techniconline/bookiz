<?php

namespace Modules\Exam\ViewModels\Question;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Exam\Models\ExamQuestionBank;
use Modules\Exam\Models\ExamQuestionBankCategory;
use Modules\Exam\Models\ExamQuestionBankCourse;
use Modules\Exam\Models\ExamQuestionBankVideo;
use Modules\Feedback\Models\Qtoa\QtoaQuestion;
use Modules\Feedback\ViewModels\Qtoa\Answer\AnswerGridViewModel;

class ExamQuestionBankViewModel extends BaseViewModel
{

    private $access_assets;

    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {
            $access_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/exam/exam.js"), 'name' => 'exam-backend'];
        }

        return $access_assets;
    }
    

    /**
     * @return $this
     */
    protected function setAssetsCreateQuestionBank()
    {
        $this->access_assets = true;
        return $this;
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createQuestionBank()
    {
        $model = new ExamQuestionBank();
        $model = $model->find($this->request->get('exam_question_bank_id'));
        $viewModel =& $this;

        $this->setTitlePage(trans('exam::exam.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("exam::question.bank.form_question_bank", ['question' => $model, 'view_model' => $viewModel], "form");
    }

    /**
     * @param $examQuestionBank
     * @return array|mixed
     */
    public function getActiveCategories($examQuestionBank)
    {
        $activeCategories = $this->getCategories($examQuestionBank->id);
        $activeCategories = $activeCategories?$activeCategories->pluck('category_id'):[];
        return $activeCategories;
    }

    /**
     * @param $examQuestionBank
     * @return array|mixed
     */
    public function getActiveCourses($examQuestionBank)
    {
        $activeCourses = $this->getCourses($examQuestionBank->id);
        $activeCourses = $activeCourses?$activeCourses->pluck('course_id'):[];
        return $activeCourses;
    }

    /**
     * @param $examQuestionBank
     * @return array|mixed
     */
    public function getActiveVideos($examQuestionBank)
    {
        $activeVideos = $this->getVideos($examQuestionBank->id);
        $activeVideos = $activeVideos?$activeVideos->pluck('video_id'):[];
        return $activeVideos;
    }


    /**
     * @param $exam_question_bank_id
     * @return mixed
     */
    public function getCategories($exam_question_bank_id)
    {
        $model = new ExamQuestionBankCategory();
        $data = $model->where('exam_question_bank_id', $exam_question_bank_id)->get();
        return $data;
    }

    /**
     * @param $exam_question_bank_id
     * @return mixed
     */
    public function getCourses($exam_question_bank_id)
    {
        $model = new ExamQuestionBankCourse();
        $data = $model->where('exam_question_bank_id', $exam_question_bank_id)->get();
        return $data;
    }
    /**
     * @param $exam_question_bank_id
     * @return mixed
     */
    public function getVideos($exam_question_bank_id)
    {
        $model = new ExamQuestionBankVideo();
        $data = $model->where('exam_question_bank_id', $exam_question_bank_id)->get();
        return $data;
    }


    /**
     * @return ExamQuestionBankViewModel
     * @throws \Throwable
     */
    protected function editQuestionBank()
    {
        return $this->createQuestionBank();
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function showQuestionBank()
    {
        $model = new ExamQuestionBank();
        $model = $model->withCount('examQuestions')->with('examQuestions.examAnswers')->find($this->request->get('exam_question_bank_id'));
        if($this->isAjaxRequest()){
            return $this->setDataResponse($model)->setResponse(true);
        }
        $viewModel =& $this;
        $this->setTitlePage(trans('exam::exam.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("exam::question.bank.form_question_bank", ['question' => $model, 'view_model' => $viewModel], "form");

    }

    /**
     * @return $this
     */
    protected function destroyQuestionBank()
    {
        $exam_question_bank_id = $this->request->get('exam_question_bank_id');
        $question = ExamQuestionBank::enable()->find($exam_question_bank_id);

        if ($question->delete()) {
            return $this->setResponse(true, trans("exam::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("exam::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveQuestionBank()
    {

        $data = $this->request->all();
        $data['instance_id'] = app('getInstanceObject')->getCurrentInstanceId();
        $response = $this->serviceSaveQuestionBank($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('exam.question.bank.edit', ['exam_question_bank_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @param $model_type
     * @param $model_id
     * @param $data
     * @return array
     */
    public function serviceSaveQuestionBank($data)
    {
        $question = new ExamQuestionBank();

        $exam_question_bank_id = isset($data['exam_question_bank_id']) ? $data['exam_question_bank_id'] : 0;
        if ($exam_question_bank_id) {
            $question = $question->enable()->find($exam_question_bank_id);
            if (!$question) {
                return ['action' => false, 'message' => trans("exam::messages.alert.not_find_data")];
            }
        }

        if ($this->isValidRequest($data, $question->rules, $question->messages, "POST")->isValidRequest) {
            $question->fill($data);
            if ($question->save()) {


                if(isset($data['categories']) && $data['categories']){
                    $this->serviceSaveCategories($question->id, $data['categories']);
                }

                if(isset($data['courses']) && $data['courses']){
                    $this->serviceSaveCourses($question->id, $data['courses']);
                }

                if(isset($data['videos']) && $data['videos']){
                    $this->serviceSaveVideos($question->id, $data['videos']);
                }

                return ['action' => true, 'message' => trans("exam::messages.alert.save_success"), 'data' => $question];
            } else {
                return ['action' => false, 'message' => trans("exam::messages.alert.save_un_success")];
            }
        }
        return ['action' => false, 'message' => trans("exam::messages.alert.mis_data"), 'errors' => $this->errors];
    }

    /**
     * @param $exam_question_bank_id
     * @param $videos
     * @return bool
     */
    public function serviceSaveVideos($exam_question_bank_id, $videos)
    {
        $model = new ExamQuestionBankVideo();
        $insertItems = [];
        $model->where('exam_question_bank_id', $exam_question_bank_id)->delete();

        foreach ($videos as $video) {
            $insertItems[] = ['exam_question_bank_id'=>$exam_question_bank_id, 'video_id'=>$video];
        }

        if($insertItems){
            return $model->insert($insertItems);
        }
        return false;
    }

    /**
     * @param $exam_question_bank_id
     * @param $courses
     * @return bool
     */
    public function serviceSaveCourses($exam_question_bank_id, $courses)
    {
        $model = new ExamQuestionBankCourse();
        $insertItems = [];
        $model->where('exam_question_bank_id', $exam_question_bank_id)->delete();

        foreach ($courses as $cours) {
            $insertItems[] = ['exam_question_bank_id'=>$exam_question_bank_id, 'course_id'=>$cours];
        }

        if($insertItems){
            return $model->insert($insertItems);
        }
        return false;
    }

    /**
     * @param $exam_question_bank_id
     * @param $categories
     * @return bool
     */
    public function serviceSaveCategories($exam_question_bank_id, $categories)
    {
        $model = new ExamQuestionBankCategory();
        $insertItems = [];
        $model->where('exam_question_bank_id', $exam_question_bank_id)->delete();

        foreach ($categories as $category) {
            $insertItems[] = ['exam_question_bank_id'=>$exam_question_bank_id, 'category_id'=>$category];
        }

        if($insertItems){
            return $model->insert($insertItems);
        }
        return false;
    }

    /**
     * @return $this
     */
    public function getExamQuestionBankByApi()
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }

        $model = new ExamQuestionBank();
        $model = $model->enable()->filterCurrentInstance()->select('id', DB::raw('title as text'));
        if ($q) {

            $model->where(function ($query) use ($q) {
                $query->where('title', 'LIKE', '%' . $q . '%');
            });
            $items = $model->get();

        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $items = $this->request->has('selected')?[]:$model->orderBy('id','DESC')->take(20)->get();
        }

        return $this->setDataResponse($items)->setResponse(true);
    }


}
