<?php

namespace Modules\Exam\ViewModels\Question;

use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Exam\Models\ExamQuestion;
use Modules\Exam\Models\ExamQuestionBank;

class ExamQuestionGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $filter_items = [];
    private $exam_question_bank_id;

    public function __construct()
    {
        $this->useModel = new ExamQuestion();
    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->filter_items = $params ? $params : request()->except(array_keys($this->filter_items));
        $this->exam_question_bank_id = $this->filter_items['exam_question_bank_id'];
        return $this;
    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->init();
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->filters()->enable()->with('examQuestionBank')->withCount('examAnswers');
    }

    /**
     * @param $id
     * @return $this
     */
    public function setExamQuestionBankId($id)
    {
        $this->exam_question_bank_id = $this->filter_items['exam_question_bank_id'] = $id;
        return $this;
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        //use bested assets
        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }


    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('question', trans('exam::exam.question.title'), true);
        $this->addColumn('type', trans('exam::exam.question.type'), true);
        $this->addColumn('exam_question_bank_id', trans('exam::exam.question.title_bank'), false);
        $this->addColumn('exam_answers_count', trans('exam::exam.question.exam_answers_count'), false);
        $this->addColumn('score', trans('exam::exam.question.score'), false);
        $this->addColumn('time', trans('exam::exam.question.time'), false);
        $this->addColumn('created_at', trans('exam::exam.created_at'), false);
        $this->addColumn('updated_at', trans('exam::exam.updated_at'), false);
        $this->addColumn('active', trans('exam::exam.active'), false);

        /*add action*/
        $add = array(
            'name' => 'exam.question.list.create',
            'parameter' => ["exam_question_bank_id" => ['value' => $this->exam_question_bank_id]]
        );
        $this->addButton('create_question', $add, trans('exam::exam.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        /*add action*/
        $add = array(
            'name' => 'exam.question.bank.index',
            'parameter' => null
        );
        $this->addButton('list_question_bank', $add, trans('exam::exam.question_bank.list'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);

        $show = array(
            'name' => 'exam.question.list.edit',
            'parameter' => ['id']
        );
        $this->addAction('content_edit', $show, trans('exam::exam.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'exam.question.answer.index',
            'parameter' => ['id']
        );

        $this->addAction('list_answers', $show, trans('exam::exam.answer.list'), 'fa fa-list-ol', false, ['target' => '', 'class' => 'btn btn-sm btn-primary']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('exam::exam.confirm_delete')];
        $delete = array(
            'name' => 'exam.question.list.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('exam::exam.delete'), 'fa fa-trash', false, $options);

        /**/
        $this->action_text = false;

        /*filter*/
        $this->addFilter('title', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->active = trans('exam::exam.statuses.' . $row->active);
        $row->exam_answers_count = $row->type=='four_options' && $row->exam_answers_count<4 ? '<a class="btn btn-warning btn-sm">'.$row->exam_answers_count .'</a>' : $row->exam_answers_count;
        $row->type = trans('exam::exam.question.types.' . $row->type);
        $row->question = $row->question ? str_limit(strip_tags($row->question), 60) : '-';
        $row->exam_question_bank_id = $row->examQuestionBank ? $row->examQuestionBank->title : '-';
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
//        if ($row->published) {
//            $action['publish']->icon = 'fa fa-eye';
//        } else {
//            $action['publish']->icon = 'fa fa-eye-slash';
//        }
        return $action;

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getListGrid()
    {
        $this->setTitlePage(trans('exam::exam.question.list'));
        $this->generateGridList()->renderedView("exam::question.list.index", ['view_model' => $this], "question_list");
        return $this;
    }


}
