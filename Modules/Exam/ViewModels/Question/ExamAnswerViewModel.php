<?php

namespace Modules\Exam\ViewModels\Question;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Exam\Libraries\ExamQuestion\MasterExamQuestion;
use Modules\Exam\Models\ExamQuestion;
use Modules\Exam\Models\ExamAnswer;

class ExamAnswerViewModel extends BaseViewModel
{

    private $access_assets;
    private $exam_question_id;
    private $exam_question;
    private $exam_answers;
    private $filter_items = [];

    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {
            $access_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/exam/exam.js"), 'name' => 'exam-backend'];
        }

        return $access_assets;
    }


    /**
     * @return $this
     */
    protected function setAssetsCreateAnswerBank()
    {
        $this->access_assets = true;
        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->filter_items = $params ? $params : request()->except(array_keys($this->filter_items));
        $this->exam_question_id = isset($this->filter_items['exam_question_id']) ? $this->filter_items['exam_question_id'] : 0;
        $this->exam_question = $this->getExamQuestion();
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExamQuestion()
    {
        $data = ExamQuestion::enable()->find($this->exam_question_id);
        return $data;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setExamQuestionId($id)
    {
        $this->exam_question_id = $this->filter_items['exam_question_id'] = $id;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createAnswer()
    {
        $this->init();
        $model = new ExamAnswer();
        $model = $model->find($this->request->get('exam_answer_id'));
        $viewModel =& $this;

        if ($model) {
            $this->setExamQuestionId($model->exam_question_id);
        }

        $this->setTitlePage(trans('exam::exam.answer.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("exam::question.answer.form_answer", ['answer' => $model, 'view_model' => $viewModel], "form");
    }

    /**
     * @return ExamAnswerViewModel
     * @throws \Throwable
     */
    protected function editAnswer()
    {
        return $this->createAnswer();
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function sectionCreateAnswer()
    {
        $viewModel =& $this;
        $this->init()->setAnswers();
        $data['request'] = $this->request->all();
        $type = $this->exam_question ? $this->exam_question->type : $this->request->get('type');
        if (!$type) {
            return $this->redirectBack()->setResponse(false, trans("exam::messages.alert.not_find_data"));
        }
        return $this->renderedView("exam::question.answer.form_section_answer", ['view_model' => $viewModel, 'data' => $data, 'type' => $type], "form");
    }

    /**
     * @return $this
     */
    protected function setAnswers()
    {
        if ($this->exam_question_id) {
            $this->exam_answers = ExamAnswer::enable()->where('exam_question_id', $this->exam_question_id)->get();
            return $this;
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAnswers()
    {
        return $this->exam_answers;
    }

    /**
     * @return mixed
     */
    public function getQuestion()
    {
        return $this->exam_question;
    }

    /**
     * @return MasterExamQuestion
     */
    public function getMasterExamQuestion()
    {
        $masterExamQuestion = new MasterExamQuestion();
        return $masterExamQuestion;
    }

    /**
     * @return $this
     */
    protected function destroyAnswer()
    {
        $exam_answer_id = $this->request->get('exam_answer_id');
        $question = ExamAnswer::enable()->find($exam_answer_id);

        if ($question->delete()) {
            return $this->setResponse(true, trans("exam::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("exam::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveAnswer()
    {

        $data = $this->request->all();
        $response = $this->serviceSaveAnswer($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('exam.question.answer.edit', ['exam_answer_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @return $this
     */
    protected function saveAnswers()
    {
        $data = $this->request->all();
        $answers = $this->request->get('answer');
        $relations = $this->request->get('relations');
        $answer_types = $this->request->get('answer_types');
        $answer_number = $this->request->get('answer_number');
        $is_trues = $this->request->get('is_true');
        $is_update = $this->request->get('is_update');
        $exam_question_id = $this->request->get('exam_question_id');
        $res = [];
        $action = false;
        foreach ($answers as $answer_id => $answer) {
            $data = [
                'exam_question_id' => $exam_question_id,
                'answer' => $answer,
                'active' => 1,
                'is_true' => isset($is_trues[$answer_id]) && $is_trues[$answer_id] ? $is_trues[$answer_id] : 0,
                'relations' => isset($relations[$answer_id]) && $relations[$answer_id] ? $relations[$answer_id] : '',
                'type' => isset($answer_types[$answer_id]) && $answer_types[$answer_id] ? $answer_types[$answer_id] : 0,
                'answer_number' => isset($answer_number[$answer_id]) && $answer_number[$answer_id] ? $answer_number[$answer_id] : 0,
            ];

            if($is_update){
                $data['exam_answer_id'] = $answer_id;
            }
            $res[] = $r = $this->serviceSaveAnswer($data);
            if($r['action']){
                $action = true;
            }
        }

        // append answers
        $answers = $this->request->get('answer_append');
        $is_trues = $this->request->get('is_true_append');
        if($answers){
            foreach ($answers as $index => $answer) {
                if(!strip_tags($answer)){
                    continue;
                }

                $data = [
                    'exam_question_id' => $exam_question_id,
                    'answer' => $answer,
                    'active' => 1,
                    'is_true' => isset($is_trues[$index]) && $is_trues[$index] ? $is_trues[$index] : 0,
                ];
                $res[] = $r = $this->serviceSaveAnswer($data);
            }
        }


        return $this->redirectBack()->setResponse($action, $action?trans("exam::messages.alert.save_success"):trans("exam::messages.alert.save_un_success"));
    }


    /**
     * @return null
     */
    public function getTitleQuestion()
    {
        $data = $this->exam_question;
        if (!$this->exam_question) {
            $data = $this->getExamQuestion();
        }
        return $data ? $data->question : null;
    }

    /**
     * @return mixed
     */
    public function getExamQuestionId()
    {
        return $this->exam_question_id;
    }

    /**
     * @param $data
     * @return array
     */
    public function serviceSaveAnswer($data)
    {
        $answer = new ExamAnswer();

        $exam_answer_id = isset($data['exam_answer_id']) ? $data['exam_answer_id'] : 0;
        if ($exam_answer_id) {
            $answer = $answer->enable()->find($exam_answer_id);
            if (!$answer) {
                return ['action' => false, 'message' => trans("exam::messages.alert.not_find_data")];
            }
        }

        $is_true = isset($data['is_true']) ? $data['is_true'] : 0;
        $relations = isset($data['relations']) ? $data['relations'] : null;

        if ($this->isValidRequest($data, $answer->rules, $answer->messages, "POST")->isValidRequest) {
            $answer->fill($data);
            $answer->is_true = $is_true;
            $answer->relations = $relations;
            if ($answer->save()) {
                return ['action' => true, 'message' => trans("exam::messages.alert.save_success"), 'data' => $answer];
            } else {
                return ['action' => false, 'message' => trans("exam::messages.alert.save_un_success")];
            }
        }
        return ['action' => false, 'message' => trans("exam::messages.alert.mis_data"), 'errors' => $this->errors];
    }
}
