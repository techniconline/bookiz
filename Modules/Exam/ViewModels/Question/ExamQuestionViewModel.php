<?php

namespace Modules\Exam\ViewModels\Question;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Exam\Models\ExamQuestion;
use Modules\Exam\Models\ExamQuestionBank;

class ExamQuestionViewModel extends BaseViewModel
{

    private $access_assets;
    private $exam_question_bank_id;
    private $exam_question_bank;
    private $filter_items = [];

    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {
            $access_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/exam/exam.js"), 'name' => 'exam-backend'];
        }

        return $access_assets;
    }


    /**
     * @return $this
     */
    protected function setAssetsCreateQuestionBank()
    {
        $this->access_assets = true;
        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->filter_items = $params ? $params : request()->except(array_keys($this->filter_items));
        $this->exam_question_bank_id = isset($this->filter_items['exam_question_bank_id']) ? $this->filter_items['exam_question_bank_id'] : 0;
        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setExamQuestionBankId($id)
    {
        $this->exam_question_bank_id = $this->filter_items['exam_question_bank_id'] = $id;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createQuestion()
    {
        $this->init();
        $model = new ExamQuestion();
        $model = $model->find($this->request->get('exam_question_id'));
        $viewModel =& $this;

        if ($model) {
            $this->setExamQuestionBankId($model->exam_question_bank_id);
        }

        $this->setTitlePage(trans('exam::exam.question.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("exam::question.list.form_question", ['question' => $model, 'view_model' => $viewModel], "form");
    }

    /**
     * @return ExamQuestionViewModel
     * @throws \Throwable
     */
    protected function editQuestion()
    {
        return $this->createQuestion();
    }

    /**
     * @return $this
     */
    protected function destroyQuestion()
    {
        $exam_question_id = $this->request->get('exam_question_id');
        $question = ExamQuestion::enable()->find($exam_question_id);

        if ($question->delete()) {
            return $this->setResponse(true, trans("exam::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("exam::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveQuestion()
    {

        $data = $this->request->all();
        $response = $this->serviceSaveQuestion($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('exam.question.answer.section_create_answer', ['exam_question_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @return mixed
     */
    public function getExamQuestionBank()
    {
        if (!$this->exam_question_bank) {
            $data = ExamQuestionBank::enable()->find($this->exam_question_bank_id);
            $this->exam_question_bank = $data;
        }

        return $this->exam_question_bank;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getExamQuestionTypes()
    {
        $data = $this->getExamQuestionBank();
        if ($data && $data->type) {
            $data = [$data->type => array_get(trans('exam::exam.question.types'), $data->type)];
        } else {
            $data = trans('exam::exam.question.types');
        }
        return $data;
    }

    public function getExamQuestionType()
    {
        $data = $this->getExamQuestionBank();
        if ($data && $data->type) {
            $data = $data->type;
        } else {
            $data = null;
        }
        return $data;
    }

    /**
     * @return null
     */
    public function getTitleBank()
    {
        $data = $this->getExamQuestionBank();
        return $data ? $data->title : null;
    }

    /**
     * @return mixed
     */
    public function getExamQuestionBankId()
    {
        return $this->exam_question_bank_id;
    }

    /**
     * @param $data
     * @return array
     */
    public function serviceSaveQuestion($data)
    {
        $question = new ExamQuestion();

        $exam_question_id = isset($data['exam_question_id']) ? $data['exam_question_id'] : 0;
        if ($exam_question_id) {
            $question = $question->enable()->find($exam_question_id);
            if (!$question) {
                return ['action' => false, 'message' => trans("exam::messages.alert.not_find_data")];
            }
        }

        if ($this->isValidRequest($data, $question->rules, $question->messages, "POST")->isValidRequest) {
            $question->fill($data);
            if ($question->save()) {
                return ['action' => true, 'message' => trans("exam::messages.alert.save_success"), 'data' => $question];
            } else {
                return ['action' => false, 'message' => trans("exam::messages.alert.save_un_success")];
            }
        }
        return ['action' => false, 'message' => trans("exam::messages.alert.mis_data"), 'errors' => $this->errors];
    }
}
