<?php

namespace Modules\Exam\ViewModels\Question;

use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Exam\Models\ExamQuestion;
use Modules\Exam\Models\ExamAnswer;

class ExamAnswerGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $filter_items = [];
    private $exam_question_id;
    private $exam_question_bank_id;
    private $exam_question;

    public function __construct()
    {
        $this->useModel = new ExamAnswer();
    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->filter_items = $params ? $params : request()->except(array_keys($this->filter_items));
        $this->exam_question_id = $this->filter_items['exam_question_id'];
        if ($this->exam_question_id) {
            $this->exam_question = ExamQuestion::enable()->find($this->exam_question_id);
            $this->exam_question_bank_id = $this->exam_question ? $this->exam_question->exam_question_bank_id : 0;
        }
        return $this;
    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->init();
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->filters()->enable();
    }

    /**
     * @param $id
     * @return $this
     */
    public function setExamQuestionId($id)
    {
        $this->exam_question_id = $this->filter_items['exam_question_id'] = $id;
        return $this;
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        //use bested assets
        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }


    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('answer', trans('exam::exam.answer.title'), true);
        $this->addColumn('is_true', trans('exam::exam.answer.is_true'), true);
        $this->addColumn('exam_question_id', trans('exam::exam.question.title'), false);
        $this->addColumn('created_at', trans('exam::exam.created_at'), false);
        $this->addColumn('updated_at', trans('exam::exam.updated_at'), false);
        $this->addColumn('active', trans('exam::exam.active'), false);
        /*add action*/
        $add = array(
            'name' => 'exam.question.answer.create',
            'parameter' => ["exam_question_id" => ['value' => $this->exam_question_id]]
        );
        $this->addButton('create_answer', $add, trans('exam::exam.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        /*add action*/
        $add = array(
            'name' => 'exam.question.list.index',
            'parameter' => ["exam_question_bank_id" => ['value' => $this->exam_question_bank_id]]
        );
        $this->addButton('list_question', $add, trans('exam::exam.question.list'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);

        $show = array(
            'name' => 'exam.question.answer.edit',
            'parameter' => ['id']
        );
        $this->addAction('content_edit', $show, trans('exam::exam.answer.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);


        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('exam::exam.confirm_delete')];
        $delete = array(
            'name' => 'exam.question.answer.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('exam::exam.delete'), 'fa fa-trash', false, $options);

        /**/
        $this->action_text = false;

        /*filter*/
        $this->addFilter('title', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->active = trans('exam::exam.statuses.' . $row->active);
        $row->is_true = '<a class="btn '.($row->is_true?'btn-success':'btn-danger').' btn-sm">'.trans('exam::exam.answer.is_true_states.' . $row->is_true).'</a>' ;
        $row->answer = $row->answer ? str_limit(strip_tags($row->answer), 60) : '-';
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
//        if ($row->published) {
//            $action['publish']->icon = 'fa fa-eye';
//        } else {
//            $action['publish']->icon = 'fa fa-eye-slash';
//        }
        return $action;

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getListGrid()
    {
        $this->setTitlePage(trans('exam::exam.answer.list'));
        $this->generateGridList()->renderedView("exam::question.answer.index", ['view_model' => $this], "question_list");
        return $this;
    }


}
