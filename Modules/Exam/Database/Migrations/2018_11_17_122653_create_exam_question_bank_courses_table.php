<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamQuestionBankCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_question_bank_courses', function (Blueprint $table) {
            $table->unsignedInteger('exam_question_bank_id');
            $table->unsignedInteger('course_id');
        });

        Schema::table('exam_question_bank_courses', function (Blueprint $table) {
            $table->foreign('exam_question_bank_id')->references('id')->on('exam_question_banks')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_question_bank_courses', function (Blueprint $table) {
            $table->dropForeign('exam_question_bank_courses_exam_question_bank_id_foreign');
            $table->dropForeign('exam_question_bank_courses_course_id_foreign');
        });

        Schema::dropIfExists('exam_question_bank_courses');
    }
}
