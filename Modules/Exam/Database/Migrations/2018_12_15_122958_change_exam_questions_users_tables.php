<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeExamQuestionsUsersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('exam_question_users', function (Blueprint $table) {
            $table->dropForeign('exam_question_users_exam_question_id_foreign');
            $table->dropColumn(['exam_question_id']);
        });

        Schema::table('exam_question_users', function (Blueprint $table) {
            $table->longText('questions')->after("exam_attempt_id")->nullable()->comment('json of questions');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_question_users', function (Blueprint $table) {

        });

    }
}
