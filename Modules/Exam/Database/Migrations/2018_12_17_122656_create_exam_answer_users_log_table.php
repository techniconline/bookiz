<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamAnswerUsersLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_answer_users_log', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->index();
            $table->unsignedBigInteger('exam_attempt_id')->index();
            $table->unsignedInteger('exam_question_id')->index();
            $table->unsignedInteger('exam_answer_id')->nullable();
            $table->longText('exam_answer')->nullable();
            $table->enum('status', ['new', 'answered', 'calculated'])->default('new')->index();
            $table->tinyInteger('active')->default('2')->index()->comment('1: active, 0: delete, 2:disable, 3 : expire time');
            $table->timestamp('created_at')->nullable();
            $table->primary(['user_id','exam_attempt_id','exam_question_id'], 'primary_exam_answer_users_log');

        });

        Schema::table('exam_answer_users_log', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('exam_attempt_id')->references('id')->on('exam_attempts')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('exam_question_id')->references('id')->on('exam_questions')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('exam_answer_id')->references('id')->on('exam_answers')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_answer_users_log', function (Blueprint $table) {
            $table->dropForeign('exam_answer_users_log_user_id_foreign');
            $table->dropForeign('exam_answer_users_log_exam_attempt_id_foreign');
            $table->dropForeign('exam_answer_users_log_exam_question_id_foreign');
            $table->dropForeign('exam_answer_users_log_exam_answer_id_foreign');
        });

        Schema::dropIfExists('exam_answer_users_log');
    }
}
