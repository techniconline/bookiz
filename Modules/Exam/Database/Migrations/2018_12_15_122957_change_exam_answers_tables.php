<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeExamAnswersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('exam_answers', function (Blueprint $table) {
            $table->enum("type", ['normal', 'all_answers', 'relational'])->after("exam_question_id")->default('normal')->nullable()->inedx();
            $table->string('relations')->after("exam_question_id")->nullable();
            $table->unsignedTinyInteger('answer_number')->after("exam_question_id")->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_answers', function (Blueprint $table) {
            $table->dropColumn(['answer_number', 'type', 'relations']);
        });

    }
}
