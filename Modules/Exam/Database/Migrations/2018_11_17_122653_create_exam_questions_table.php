<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('exam_question_bank_id');
            $table->longText('question');
            $table->enum('type',['four_options','descriptive'])->nullable()->index();
            $table->longText('description')->nullable();
            $table->unsignedInteger('score')->nullable()->default(1);
            $table->unsignedInteger('time')->nullable()->default(0);
            $table->tinyInteger('active')->default('2')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('exam_questions', function (Blueprint $table) {
            $table->foreign('exam_question_bank_id')->references('id')->on('exam_question_banks')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_questions', function (Blueprint $table) {
            $table->dropForeign('exam_questions_exam_question_bank_id_foreign');
        });

        Schema::dropIfExists('exam_questions');
    }
}
