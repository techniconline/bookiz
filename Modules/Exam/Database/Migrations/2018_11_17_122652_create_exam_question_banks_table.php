<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamQuestionBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_question_banks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('instance_id');
            $table->unsignedInteger('language_id');
            $table->string('title');
            $table->enum('type',['four_options','descriptive'])->nullable()->index();
            $table->longText('description')->nullable();
            $table->tinyInteger('active')->default('2')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('exam_question_banks', function (Blueprint $table) {
            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_question_banks', function (Blueprint $table) {
            $table->dropForeign('exam_question_banks_instance_id_foreign');
            $table->dropForeign('exam_question_banks_language_id_foreign');
        });

        Schema::dropIfExists('exam_question_banks');
    }
}
