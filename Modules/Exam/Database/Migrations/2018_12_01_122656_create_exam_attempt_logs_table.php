<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamAttemptLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_attempt_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->unsignedBigInteger('exam_attempt_id');
            $table->longText('data')->nullable()->comment('is json');
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('exam_attempt_logs', function (Blueprint $table) {

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('exam_attempt_id')->references('id')->on('exam_attempts')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_attempt_logs', function (Blueprint $table) {
            $table->dropForeign('exam_attempt_logs_user_id_foreign');
            $table->dropForeign('exam_attempt_logs_exam_attempt_id_foreign');
        });

        Schema::dropIfExists('exam_attempt_logs');
    }
}
