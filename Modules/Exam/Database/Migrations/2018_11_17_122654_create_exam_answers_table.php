<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('exam_question_id');
            $table->longText('answer');
            $table->boolean('is_true')->nullable()->default(0);
            $table->longText('options')->nullable();
            $table->tinyInteger('active')->nullable()->default('2')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('exam_answers', function (Blueprint $table) {
            $table->foreign('exam_question_id')->references('id')->on('exam_questions')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_answers', function (Blueprint $table) {
            $table->dropForeign('exam_answers_exam_question_id_foreign');
        });

        Schema::dropIfExists('exam_answers');
    }
}
