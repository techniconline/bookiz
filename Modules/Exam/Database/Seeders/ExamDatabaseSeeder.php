<?php

namespace Modules\Exam\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ExamDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('Modules\Exam\Database\Seeders\PermissionTableSeeder');
        $this->command->info('Permission table seeded!');
        // $this->call("OthersTableSeeder");
    }
}
