<?php
namespace Modules\Exam\Database\Seeders;
use Illuminate\Database\Seeder;
use Modules\User\Models\Permission;

class PermissionTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('connector')->delete();

        $permissions=[
            ['exam','manage.question.full','دسترسی کامل آزمون'],
            ['exam','manage.question.access.attempt','دسترسی بخش لیست آزمونها'],
        ];

        foreach ($permissions as $permission) {
            $exist = Permission::where('module',$permission[0])->where('permission',$permission[0])->where('description',$permission[0])->count();
            if($exist){
                continue;
            }
            $saver=new Permission();
            $saver->fill(array(
                'module' => $permission[0],
                'permission' => $permission[1],
                'description' => $permission[2],
            ))->save();
        }

	}
}