<?php

return [
    'group' => [
        'courses' => 'مدیریت دوره ها',
    ],
    'config'=>[
      'exam'=>'تنظیمات آزمون',
    ],
    'exam_question_bank_list' => 'بانک سوالات',
    'exam_list' => 'آزمونها',

];