<?php

return [
    'tabs'=>[
        'public'=> 'عمومی',
    ],
    'exam_running_for_user'=> ' آزمون <span class="under_3">:exam_title</span> برای دوره <span class="under_3">:course_name</span>.',
    'exam_cannot_running'=>'آزمون برای کاربری شما آماده برگزاری نمی باشد!',

    'exam_time'=>'زمان باقی منده شما در آزمون <span class="btn btn-primary btn-circle">:minutes</span> دقیقه و <span class="btn btn-warning btn-circle">:seconds</span> ثانیه می باشد.',
    'origin_exam_time'=>'زمان شما در آزمون <span class="btn btn-danger btn-circle bold font-lg">:minutes</span> دقیقه می باشد.',
    'valid_exam_time'=>'تاریخ شروع آزمون <span class="under_1">:from</span> الی <span class="under_1">:to</span> می باشد.',
    'exam_text'=>'نمره این آزمون از <span class="under_1">:exam_total_score</span> می باشد و حداقل نمره قبولی آن <span class="under_1">:exam_passing_score</span> می باشد.',
    'user_final_score'=>'نمره این آزمون از <span class="under_2">:exam_total_score</span> می باشد و حداقل نمره قبولی آن <span class="under_2">:exam_passing_score</span> می باشد. نمره شما در این آزمون <span class="btn btn-default btn-circle green-dark bold">:user_score</span> می باشد.',
    'user_question_answered'=>'کاربر گرامی شما از <span class="under_2 _from">:from</span> سوال به <span class="under_2 _to">:to</span> آن پاسخ داده اید. در هنگام <span class="under_2">ثبت نهایی</span> این موضوع را مورد توجه قرار دهید!',

    'reset_answers' => 'حذف پاسخ',
    'exam_total_score' => 'نمره نهایی',
    'exam_passing_score' => 'نمره قبولی',

    'exam_total_score_title' => 'نمره آزمون',
    'exam_passing_score_title' => 'حداقل نمره قبولی آزمون',

    'answered_question' => ' سوالات پاسخ داده شده',
    'not_answered_question' => 'سوالات پاسخ داده نشده',
    'start_date' => 'تاریخ شروع آزمون',
    'end_date' => 'تاریخ پایان آزمون',
    'exam_total_time' => 'مدت زمان آزمون (دقیقه)',
    'exam_repeat' => 'حد مجاز تکرار آزمون',
    'count_exam' => 'تعداد شرکت شما در آزمون',
    'exam_question_count' => 'تعداد سوالات این آزمون',
    'mines_score' => 'هر :mines_score سوال غلط معادل یک نمره منفی می باشد.',
    'next_questions' => 'سوالات بعدی',
    'next_question' => 'سوال بعدی',
    'prv_questions' => 'سوال قبلی',
    'back_to_course' => 'بازگشت به دوره',
    'start_exam' => 'شروع آزمون',
    'repeat_exam' => 'آزمون مجدد',
    'help' => 'راهنما',
    'number_of_questions' => 'شماره سوالات',
    'your_answer' => 'پاسخ کاربر',
    'correct_answer' => 'پاسخ صحیح',
    'user_has_not_answer' => 'کاربر هیچ پاسخی ثبت شده ای ندارد!',
    'user_exam_passed' => 'تبریک ... شما در این امتحان نمره قبولی را کسب کردید.',
    'user_exam_fail' => 'متاسفانه ... شما در این امتحان نمره قبولی را کسب نکردید.',
    'result_of_user' => 'آخرین پاسخ نامه کاربر',
    'result_not_show_user' => 'نمره و پاسخنامه شما قابل رویت نمی باشد!',

    'question_bank' => [
        'list' => 'لیست بانک سوالات',
        'title' => 'عنوان',
        'type' => 'نوع سوالات',
        'description' => 'توضیح',
        'delete' => 'حذف بانک سوالات',
        'add' => 'افزودن بانک سوالات',
        'edit' => 'ویرایش بانک سوالات',
        'types' => [
            'four_options' => 'چهارگزینه ی',
            'descriptive' => 'تشریحی',
        ]
    ]
    , 'question' => [
        'list' => 'لیست سوالات',
        'row'=> 'شماره سوال',
        'title' => 'سوال',
        'question_count' => 'تعداد سوالات',
        'title_bank' => 'عنوان بانک',
        'score' => 'نمره',
        'time' => 'زمان',
        'exam_answers_count' => 'تعداد پاسخ',
        'type' => 'نوع سوال',
        'description' => 'توضیح',
        'delete' => 'حذف سوال',
        'add' => 'افزودن سوال',
        'edit' => 'ویرایش سوال',
        'types' => [
            'four_options' => 'چهارگزینه ی',
            'descriptive' => 'تشریحی',
        ]
    ]
    , 'answer' => [
        'list' => 'لیست پاسخ ها',
        'title' => 'پاسخ',
        'separator_relation_answer_text' => ' و ',
        'title_bank' => 'سوال',
        'is_true' => 'پاسخ صحیح',
        'is_true_states' => [
            '0'=>'خیر',
            '1'=>'بله',
        ],

        'types'=>[
            'normal'=>'عادی',
            'relational'=>'مرتبط با',
            'all_answers'=>'همه موارد',
        ],
        'text_types' => 'نوع پاسخ',
        'relations' => 'شماره ردیف پاسخ ها',
        'answer_number' => 'شماره ردیف',

        'delete' => 'حذف پاسخ',
        'add' => 'افزودن پاسخ',
        'edit' => 'ویرایش پاسخ',

        'status'=>[
            'new' => 'جدید',
            'answered' => 'پاسخ داده',
            'calculated' => 'محاسبه شده'
        ],
    ]
    , 'attempt' => [
        'list'=>'لیست افراد آزمون داده',
        'list_activity'=>'لیست آزمون',
        'user' => 'کاربر',
        'edit' => 'ویرایش آزمون',
        'change_status' => 'تغییر وضعیت آزمون',
        'score' => 'نمره',
        'relative_score' => 'نمره موثر ',

        'status'=>[
            'new' => 'جدید',
            'open' => 'باز',
            'close' => 'بسته'
        ],
    ]
    ,
    'instance_id' => 'سامانه',
    'language_id' => 'زبان',
    'active' => 'وضعیت',
    'deleted_at' => 'زمان حذف',
    'created_at' => 'زمان ایجاد',
    'updated_at' => 'آخرین بروزرسانی',
    'instances' => 'سامانه های سیستم',
    'languages' => 'زبانهای سیستم',
    'select_language' => 'انتخاب زبان',
    'select_category' => 'انتخاب دسته بندی',
    'categories' => 'دسته بندی ها',
    'select_course' => 'انتخاب دوره',
    'courses' => 'دوره ها',
    'select_video' => 'انتخاب ویدیو',
    'videos' => 'ویدیو ها',
    'select_instance' => 'انتخاب سامانه',
    'select_status' => 'انتخاب وضعیت',
    'confirm_delete' => 'آیا از حذف اطمینان دارید؟',
    'confirm' => 'آیا از عملیات اطمینان دارید؟',
    'action' => 'عملیات',
    'delete' => 'حذف',
    'add' => 'افزودن',
    'edit' => 'ویرایش',
    'sort' => 'مرتب سازی (الویت)',
    'instance' => 'مشخصه سامانه (instance)',
    'cancel' => 'انصراف',
    'submit' => 'ثبت',
    'next_answer' => 'ادامه',
    'back_question' => 'برگشت به سوال',
    'status' => 'وضعیت',
    'statuses' => [
        "0" => "حذف شده",
        "1" => "فعال",
        "2" => "غیرفعال",
    ],

];