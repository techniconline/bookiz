<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("exam::exam.answer.".($answer?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>
            {{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>(isset($answer) && $answer? route('exam.question.answer.update', ['exam_answer_id'=>$answer->id]) : route('exam.question.answer.save', ['exam_question_id'=>$view_model->getExamQuestionId()]))
                        ,'method'=>(isset($answer) && $answer?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("exam::exam.tabs.public") </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::editor('question', $view_model->getTitleQuestion() ,['disabled'=>'disabled','title'=>trans("exam::exam.question.title"),'helper'=>trans("exam::exam.question.title")]) !!}

                                    {!! FormHelper::editor('answer',old('answer',$answer?$answer->answer:null) ,['required'=>'required','title'=>trans("exam::exam.answer.title"),'helper'=>trans("exam::exam.answer.title")]) !!}

                                    {!! FormHelper::checkbox('is_true',1,old('is_true',$answer?$answer->is_true:null) ,['title'=>trans("exam::exam.answer.is_true")]) !!}

                                    {!! FormHelper::select('active',trans("exam::exam.statuses"),old('active',$answer?$answer->active:2)
                                    ,['title'=>trans("exam::exam.status"),'helper'=>trans("exam::exam.select_status")
                                    ,'placeholder'=>trans("exam::exam.select_status")]) !!}

                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("exam::exam.submit")], ['title'=>trans("exam::exam.cancel"), 'url'=>route('exam.question.answer.index',['exam_question_id'=>$view_model->getExamQuestionId()])]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>
