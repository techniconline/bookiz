<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("exam::exam.question.".($question?'edit':'add')) (@lang("exam::exam.question.title_bank"): {!! $view_model->getTitleBank() !!})
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>
            {{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">

                {!! FormHelper::open(['role'=>'form','url'=>(isset($question) && $question? route('exam.question.list.update', ['exam_question_id'=>$question->id]) : route('exam.question.list.save', ['exam_question_bank_id'=>$view_model->getExamQuestionBankId()]))
                        ,'method'=>(isset($question) && $question?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("exam::exam.tabs.public") </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::editor('question',old('question',$question?$question->question:null) ,['required'=>'required','title'=>trans("exam::exam.question.title"),'helper'=>trans("exam::exam.question.title")]) !!}

                                    {!! FormHelper::select('type',$view_model->getExamQuestionTypes(),old('type',$question?$question->type:$view_model->getExamQuestionType())
                                    ,['title'=>trans("exam::exam.question.type"),'helper'=>trans("exam::exam.question.type")
                                    ,'placeholder'=>trans("exam::exam.question.type")]) !!}

                                    {!! FormHelper::selectTag('exam_question_bank_id',[],old('exam_question_bank_id',$view_model->getExamQuestionBankId()),['data-ajax-url'=>route('exam.question.bank.get_exam_question_bank_by_api')
                                         , 'title'=>trans("exam::exam.question.title_bank"),'helper'=>trans("exam::exam.question.title_bank")]) !!}

                                    {!! FormHelper::input('text','score',old('score',$question?$question->score:1) ,['required'=>'required','title'=>trans("exam::exam.question.score"),'helper'=>trans("exam::exam.question.score")]) !!}

                                    {!! FormHelper::input('text','time',old('score',$question?$question->time:0) ,['required'=>'required','title'=>trans("exam::exam.question.time"),'helper'=>trans("exam::exam.question.time")]) !!}

                                    {!! FormHelper::select('active',trans("exam::exam.statuses"),old('active',$question?$question->active:2)
                                    ,['title'=>trans("exam::exam.status"),'helper'=>trans("exam::exam.select_status")
                                    ,'placeholder'=>trans("exam::exam.select_status")]) !!}


                                    {!! FormHelper::editor('description',old('description',$question?$question->description:null)
                                    ,['title'=>trans("exam::exam.question.description"),'helper'=>trans("exam::exam.question.description")]) !!}

                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancelWithLanguage(['title'=>trans("exam::exam.next_answer")], ['title'=>trans("exam::exam.cancel"), 'url'=>route('exam.question.list.index',['exam_question_bank_id'=>$view_model->getExamQuestionBankId()])], ['selected'=>$question?$question->language_id:null]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>
