@php $question = $blockViewModel->getQuestion() @endphp
<div class="Question" id="group_id_{!! $question['id'] !!}">
   <h6 class="box_course28">
      <div class="question_no"> {!! $blockViewModel->getData('row_number') !!} </div> {!! $question['question'] !!}
   </h6>
   <div class="answerBox">
      <div class="control-group">
         @if($answers = $blockViewModel->getAnswers())
         @foreach($answers as $index => $answer)
         @php   $checked = null;    @endphp
         <label class="control control-radio">
            {!! $answer['answer'] !!}
            <input data-alpha="{!! $blockViewModel->getRowAlpha($index) !!}" id="checkbox_{!! $answer['id'] !!}" class="_exam_answer"
                   data-question="{!! $answer['exam_question_id'] !!}" name="answers[{!! $answer['exam_question_id'] !!}][]" type="radio" value="{!! $answer['id'] !!}" />
            <div class="control_indicator">{!! $blockViewModel->getRowAlpha($index) !!}</div>
         </label>
         @endforeach
      </div>
      <a class="btn btn-warning btn-sm _reset_answer">@lang('exam::exam.reset_answers')</a>
   </div>
</div>
@endif