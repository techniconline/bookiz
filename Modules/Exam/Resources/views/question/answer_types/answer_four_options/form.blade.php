@php
    $answers = $blockViewModel->getAllAnswers();
    $question = $blockViewModel->getQuestion();
    $append_answers = $blockViewModel->getCountAppendAnswers();
@endphp

<div class="row margin-top-10">
    <div class="col-md-12">

        <div class="portlet box green _list_answers">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bell-o"></i>@lang("exam::exam.answer.".($answers?'edit':'add'))
                </div>
                <div class="tools">
                    <a class="_add_answer" title="@lang("exam::exam.answer.add")"> <i class="fa fa-plus-circle"
                                                                                      style="font-size: 2em; color: white;"></i>
                    </a>
                    <a href="javascript:;" class="collapse" data-original-title=""
                       title=""> </a>
                </div>
            </div>

            <div class="portlet-body" style="display: block">
                <div class="row" style="padding: 5px">
                    {!! $question?$question->question:'-' !!}
                </div>
            </div>
            <div class="portlet-body" style="display: block">

                {!! FormHelper::open(['role'=>'form','url'=>route('exam.question.answer.save_answers', ['exam_question_id'=>$question?$question->id:0])
                         ,'method'=>(isset($answer) && $answer?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp


                <div class="_answers">
                    @php
                        $append_now = $answers->count()?$answers->count():4;
                    @endphp
                    @for($i=$append_now; $i<$append_answers + $append_now; $i++)
                        <span class="_append_answer _row_answer" style="display:none;">
                            <hr>

                            {!! FormHelper::select('answer_types_append['.$i.']',trans('exam::exam.answer.types'),old('answer_types_append['.$i.']','normal')
                             ,['title'=>trans('exam::exam.answer.text_types'),'helper'=>trans('exam::exam.answer.text_types'), 'class'=>'_type_answer']) !!}

                            <span style="display: none;" class="_relations">
                              {!! FormHelper::input('text','relations_append['.$i.']',old('relations_append['.$i.']',null),['title'=>trans('exam::exam.answer.relations'),'helper'=>trans('exam::exam.answer.relations')]) !!}
                            </span>

                            {!! FormHelper::input('text','answer_number_append['.$i.']',old('answer_number_append['.$i.']',$i),['required'=>'required' ,'readonly'=>'readonly'
                                       ,'title'=>trans('exam::exam.answer.answer_number'),'helper'=>trans('exam::exam.answer.answer_number')]) !!}

                            {!! FormHelper::editor('answer_append['.$i.']',old('answer_append['.$i.']',null) ,['id'=>'answer_append'.$i,'title'=>trans("exam::exam.answer.title"),'helper'=>trans("exam::exam.answer.title")]) !!}

                            {!! FormHelper::checkbox('is_true_append['.$i.']',1,old('is_true_append['.$i.']',null) ,['id'=>'is_true_append'.$i,'title'=>trans("exam::exam.answer.is_true")]) !!}
                            <a class="_answer_trash"
                               title="{!! trans("course::course.activity.exam_activity.remove_bank") !!}"
                               data-index="{!! $i !!}"><i style="color: red; font-size: 1.5em"
                                                          class="fa fa-trash"></i></a>
                    </span>
                    @endfor
                </div>


                @if($answers->count())

                    <input type="hidden" name="is_update" value="1">

                    @foreach($answers as $index => $answer)
                        <hr>
                        <span class="_row_answer">
                        {!! FormHelper::select('answer_types['.$answer->id.']',trans('exam::exam.answer.types'),old('answer_types['.$answer->id.']',$answer->type?$answer->type:'normal')
                             ,['title'=>trans('exam::exam.answer.text_types'),'helper'=>trans('exam::exam.answer.text_types'), 'class'=>'_type_answer']) !!}

                            <span style="display: {!! $answer->type=='relational'?'block':'none' !!};" class="_relations">
                        {!! FormHelper::input('text','relations['.$answer->id.']',old('relations['.$answer->id.']',$answer->relations?$answer->relations:null),['title'=>trans('exam::exam.answer.relations'),'helper'=>trans('exam::exam.answer.relations')]) !!}
                            </span>
                        {!! FormHelper::input('text','answer_number['.$answer->id.']',old('answer_number['.$answer->id.']',$answer->answer_number?$answer->answer_number:$index),['required'=>'required','readonly'=>'readonly'
                                   ,'title'=>trans('exam::exam.answer.answer_number'),'helper'=>trans('exam::exam.answer.answer_number')]) !!}

                        {!! FormHelper::editor('answer['.$answer->id.']',old('answer['.$answer->id.']',$answer?$answer->answer:null) ,['required'=>'required','title'=>trans("exam::exam.answer.title"),'helper'=>trans("exam::exam.answer.title")]) !!}

                        {!! FormHelper::checkbox('is_true['.$answer->id.']',1,old('is_true['.$answer->id.']',$answer?$answer->is_true:null) ,['title'=>trans("exam::exam.answer.is_true")]) !!}
                        </span>

                    @endforeach

                @else

                    @for($i=0; $i<4; $i++)
                        <hr>
                        <span class="_row_answer">
                        {!! FormHelper::select('answer_types['.$i.']',trans('exam::exam.answer.types'),old('answer_types['.$i.']','normal')
                            ,['title'=>trans('exam::exam.answer.text_types'),'helper'=>trans('exam::exam.answer.text_types'), 'class'=>'_type_answer']) !!}

                            <span style="display: none;" class="_relations">
                                              {!! FormHelper::input('text','relations['.$i.']',old('relations['.$i.']',null),['title'=>trans('exam::exam.answer.relations'),'helper'=>trans('exam::exam.answer.relations')]) !!}
                            </span>

                            {!! FormHelper::input('text','answer_number['.$i.']',old('answer_number['.$i.']',$i),['required'=>'required','readonly'=>'readonly'
                                       ,'title'=>trans('exam::exam.answer.answer_number'),'helper'=>trans('exam::exam.answer.answer_number')]) !!}

                            {!! FormHelper::editor('answer['.$i.']',old('answer['.$i.']',null) ,['required'=>'required','id'=>'answer_'.$i,'title'=>trans("exam::exam.answer.title"),'helper'=>trans("exam::exam.answer.title")]) !!}

                            {!! FormHelper::checkbox('is_true['.$i.']',1,old('is_true['.$i.']',null) ,['id'=>'is_true_'.$i,'title'=>trans("exam::exam.answer.is_true")]) !!}
                        </span>
                    @endfor

                @endif


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("exam::exam.submit")], ['title'=>trans("exam::exam.back_question"), 'url'=>route('exam.question.list.edit',['exam_question_id'=>$question?$question->id:0])]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>
        </div>

    </div>
</div>


<script>

    jQuery(document).ready(function () {

        $('div._list_answers').on('click', 'a._add_answer', function (event) {
            event.preventDefault();
            var $clicked = $(this);
            var $list = $('div._answers');

            // var $source = $list.find('div._question_bank_select_box[data-index="'+$counter+'"]').parents('._row').first();
            var $source = $list.find('span._append_answer:hidden:first').first();
            console.log($source);
            $source.fadeIn();
        });

        $('div._list_answers').on('click', 'a._answer_trash', function (event) {
            event.preventDefault();
            var $clicked = $(this);

            var $source = $clicked.parents('._append_answer').first();
            $source.fadeOut();
        });

        $('div._list_answers').on('change', 'select._type_answer', function (event) {
            event.preventDefault();
            var $clicked = $(this);
            var $source = $clicked.parents('span._row_answer').first().find('span._relations');
            if($clicked.find('option:selected').val() == 'relational'){
                $source.fadeIn();
            }else{
                $source.fadeOut();
            }

        });



    });

</script>