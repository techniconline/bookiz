<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("exam::exam.question_bank.".($question?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>
            {{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>(isset($question) && $question? route('exam.question.bank.update', ['exam_question_bank_id'=>$question->id]) : route('exam.question.bank.save'))
                        ,'method'=>(isset($question) && $question?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("exam::exam.tabs.public") </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::input('text','title',old('title',$question?$question->title:null),['required'=>'required','title'=>trans("exam::exam.question_bank.title"),'helper'=>trans("exam::exam.question_bank.title")]) !!}

                                    {!! FormHelper::select('type',trans('exam::exam.question_bank.types'),old('type',$question?$question->type:null)
                                    ,['title'=>trans("exam::exam.question_bank.type"),'helper'=>trans("exam::exam.question_bank.type")
                                    ,'placeholder'=>trans("exam::exam.question_bank.type")]) !!}

                                    {!! FormHelper::selectTag('categories[]',[],old('categories',$question?$view_model->getActiveCategories($question):null),['multiple'=>'multiple', 'data-ajax-url'=>BridgeHelper::getCourse()->getUrlCourseCategoryList()
                                         , 'title'=>trans("exam::exam.categories"),'helper'=>trans("exam::exam.select_category")]) !!}

                                    {!! FormHelper::selectTag('courses[]',[],old('courses',$question?$view_model->getActiveCourses($question):null),['multiple'=>'multiple', 'data-ajax-url'=>BridgeHelper::getCourse()->getUrlCourseList()
                                         , 'title'=>trans("exam::exam.courses"),'helper'=>trans("exam::exam.select_course")]) !!}

                                    {!! FormHelper::selectTag('videos[]',[],old('videos',$question?$view_model->getActiveVideos($question):null),['multiple'=>'multiple', 'data-ajax-url'=>BridgeHelper::getVideoUse()->getUrlVideoList()
                                         , 'title'=>trans("exam::exam.videos"),'helper'=>trans("exam::exam.select_video")]) !!}

                                    {!! FormHelper::select('active',trans("exam::exam.statuses"),old('active',$question?$question->active:2)
                                    ,['title'=>trans("exam::exam.status"),'helper'=>trans("exam::exam.select_status")
                                    ,'placeholder'=>trans("exam::exam.select_status")]) !!}


                                    {!! FormHelper::editor('description',old('description',$question?$question->description:null)
                                    ,['title'=>trans("exam::exam.question_bank.description"),'helper'=>trans("exam::exam.question_bank.description")]) !!}

                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancelWithLanguage(['title'=>trans("exam::exam.submit")], ['title'=>trans("exam::exam.cancel"), 'url'=>route('exam.question.bank.index')], ['selected'=>$question?$question->language_id:null]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>
