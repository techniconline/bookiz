@if($view_model->getParamsActivity('show_final_score') && $view_model->getParamsActivity('show_final_score') != 'not_show')
    <div class="row" style="direction: rtl">
        <div class="col-md-12 ">

            <table class="table table-striped col-lg-12" style="text-align: right">

                <tr>
                    <td>
                        <h3 style="color: orangered;" class="bold">@lang('exam::exam.result_of_user')</h3>
                    </td>
                </tr>

            </table>

            <table class="table table-striped col-lg-12 _list_question">
                @if($view_model->getParamsActivity('show_final_score') && $view_model->getParamsActivity('show_final_score') == 'only_score_with_answer')
                    <thead style="text-align: right">
                    <tr>
                        <th width="50px">@lang('exam::exam.question.row')</th>
                        <th width="45%">@lang('exam::exam.question.title')</th>
                        <th width="50%">@lang('exam::exam.answer.title')</th>
                        <th></th>
                    </tr>
                    </thead>

                @endif

                <tbody>
                @if($answers && isset($answersList['answers']) && $answersList['answers'])
                    @if($view_model->getParamsActivity('show_final_score') && $view_model->getParamsActivity('show_final_score') == 'only_score_with_answer')

                        @foreach($answersList['answers'] as $answer)
                            <tr dq="{!! $answer['exam_question_id'] !!}" da="{!! $answer['exam_answer_id'] !!}"
                                dca="{!! $answer['correct_exam_answer_id'] !!}">
                                <td style="direction: rtl; text-align: center">
                                    <h3>{{ $loop->iteration }}</h3>
                                </td>
                                <td style="direction: rtl; text-align: right">{!! $answer['question'] !!}</td>
                                <td style="direction: rtl; text-align: right">
                                    <h4 style="color: orangered">@lang('exam::exam.your_answer'):</h4>
                                    {!! $answer['answer'] !!}


                                    @if(!$answer['is_accept'])
                                        <div>
                                            <h4 style="color: forestgreen">@lang('exam::exam.correct_answer'):</h4>
                                            {!! $answer['correct_answer'] !!}
                                        </div>
                                    @endif

                                </td>
                                <td>
                                <span class="btn {!! $answer['is_accept']?'btn-success':'btn-danger' !!}">



                                        {!! $answer['is_accept']?'<i class="far fa-check-circle"></i>':'<i class="far fa-times-circle"></i>' !!}
                                </span>
                                </td>
                            </tr>
                        @endforeach

                        <tr>
                            <td style="text-align: center; color: red"
                                colspan="3">
                                {!!  trans('exam::exam.user_final_score',['exam_total_score' => $view_model->getParamsActivity('exam_total_score') ,'exam_passing_score' => $view_model->getParamsActivity('exam_passing_score'), 'user_score'=>$view_model->getAttempt('score')]) !!}
                            </td>
                        </tr>

                    @endif

                    @if($view_model->getParamsActivity('show_final_score') && $view_model->getParamsActivity('show_final_score') == 'only_score')
                        <tr>
                            <td style="text-align: center; color: red"
                                colspan="3">
                                {!! trans('exam::exam.user_final_score',['exam_total_score' => $view_model->getParamsActivity('exam_total_score') ,'exam_passing_score' => $view_model->getParamsActivity('exam_passing_score'), 'user_score'=>$view_model->getAttempt('score')]) !!}
                            </td>
                        </tr>
                    @elseif($view_model->getParamsActivity('show_final_score') && $view_model->getParamsActivity('show_final_score') == 'only_passed_exam')

                        <tr>
                            <td style="text-align: center;"
                                colspan="3">
                                @if ($view_model->getAttempt('score') >= $view_model->getParamsActivity('exam_passing_score'))
                                    <h3 style="color: green">@lang('exam::exam.user_exam_passed')</h3>
                                @else
                                    <h3 style="color: red">@lang('exam::exam.user_exam_fail')</h3>
                                @endif
                            </td>
                        </tr>

                    @endif
                @else
                    <tr>
                        <td style="text-align: center; color: red"
                            colspan="3">@lang('exam::exam.user_has_not_answer')</td>
                    </tr>
                @endif
                </tbody>
            </table>

        </div>
    </div>

@else

    <table class="table table-striped col-lg-12" style="text-align: right">

        <tr>
            <td>
                <h3 style="color: red; text-align: center" class="bold">@lang('exam::exam.result_not_show_user')</h3>
            </td>
        </tr>

    </table>

@endif
