<div class="FullWidth  margtop20 " data-date="{!! date("Y-m-d H:i:s") !!}">
    @if(isset($data['messages']) && $data['messages'])
        @foreach($data['messages'] as $message)

            <div class="container   ">
                <div class="alert alert-danger  errobx5  ">
                    <strong><i class="fas fa-exclamation-triangle"></i>{!! $message !!} </strong>
                </div>

            </div>


        @endforeach
    @endif
</div>

<div class="FullWidth _course1 mrg0">
    <div class="container  ">
        <h1>
            {!! $view_model->getTitleExam() !!}

        </h1>

        @if($view_model->getActivity() && $view_model->getExamQuestions())

            {{--<h1>{!! $view_model->getTitleExam() !!}</h1>--}}
        @endif

        @if($view_model->getActivity() && !$view_model->getExamQuestions())
            <h1>
                {{--                {!! trans('exam::exam.origin_exam_time',['minutes' => $view_model->getExamTime()['origin_minutes'] ]) !!}--}}
            </h1>
        @endif

    </div>
</div>


<div class="container  ">

    @if($view_model->getActivity())
        @if($view_model->getActivity('active_date'))

            {{--@lang('exam::exam.valid_exam_time',['from'=>$view_model->getActivity('active_date_by_mini_format'), 'to'=>$view_model->getActivity('expire_date_by_mini_format')])--}}

        @endif

        {{--                    {!! trans('exam::exam.exam_text',['exam_total_score' => $view_model->getParamsActivity('exam_total_score') ,'exam_passing_score' => $view_model->getParamsActivity('exam_passing_score') ]) !!}--}}

    @endif

    @php $course = $view_model->getCourse(); @endphp
    @php $attempt = $view_model->getAttempt(); @endphp
    @php $activity = $view_model->getActivity(); @endphp
    @if(($data['action']))

        {!! FormHelper::open(['role'=>'form','url'=>route('exam.user.save',['attempt_id'=>$attempt?$attempt->id:0]),'method'=>'POST','id'=>'form_exam','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
    @endif

    {{--{!! dd($view_model->getDataExam()) !!}--}}
    <div class="floatright {!! ($view_model->getExamQuestions())?'col-xl-9':'col-xl-12' !!}   box_course4 _list_question">

        @if((!$data['action']))
            <table class="table table-hover mttbl1 col-xl-12 col-lg-12 col-md-12  floatright " style="display: ">

                <tr>
                    <td class="gray"> @lang('exam::exam.start_date'):</td>
                    <td>{!! $view_model->getDataExam('start_date') !!}</td>
                </tr>
                <tr>
                    <td class="gray"> @lang('exam::exam.end_date'):</td>
                    <td>{!! $view_model->getDataExam('end_date') !!}</td>
                </tr>

                <tr>
                    <td class="gray"> @lang('exam::exam.exam_total_time'):</td>
                    <td>  {!! $view_model->getParamsActivity('exam_total_time') !!}</td>
                </tr>
                <tr>
                    <td class="gray"> @lang('exam::exam.exam_repeat'):</td>
                    <td> {!! $view_model->getParamsActivity('exam_repeat') !!}</td>
                </tr>

                <tr>
                    <td class="gray"> @lang('exam::exam.count_exam'):</td>
                    <td> {!! $view_model->getDataExam('count_exam') !!}</td>
                </tr>


                <tr>
                    <td class="gray"> @lang('exam::exam.exam_question_count'):</td>
                    <td> {!! $view_model->getDataExam('exam_question_count') !!}</td>
                </tr>


                <tr style="color: green">
                    <td class="gray"> @lang('exam::exam.exam_total_score_title'):</td>
                    <td><h4>{!! $view_model->getParamsActivity('exam_total_score') !!}</h4></td>
                </tr>

                <tr style="color: red">
                    <td class="gray"> @lang('exam::exam.exam_passing_score_title'):</td>
                    <td><h4>{!! $view_model->getParamsActivity('exam_passing_score') !!}</h4></td>
                </tr>

                @if($view_model->getParamsActivity('mines_score'))
                    <tr style="background-color: lightgray">
                        <td colspan="2">
                            <h4>@lang('exam::exam.mines_score',['mines_score'=>$view_model->getParamsActivity('mines_score')])</h4>
                        </td>
                    </tr>
                @endif

                <tr>
                    <td class="text-right">
                        <a href="{!! route('course.view', ['course_id' => $course ? $course->id : 0]) !!}"
                           class="btn btn-default green-dark">@lang('exam::exam.back_to_course')</a>
                    </td>
                    <td class="text-left">
                        @if(isset($data['action']) && !$data['action'])
                            @if($view_model->getDataExam('exam_start'))
                                <a href="{!! $view_model->getDataExam('exam_start_link') !!}"
                                   class="btn btn-primary">@lang('exam::exam.start_exam')</a>
                            @elseif($view_model->getDataExam('exam_repeat'))
                                <a href="{!! $view_model->getDataExam('exam_repeat_link') !!}"
                                   class="btn btn-primary">@lang('exam::exam.repeat_exam')</a>
                            @endif
                        @endif

                    </td>

                </tr>

                <tr>
                    <td colspan="2">
                        {!! $view_model->getUserAnswers() !!}
                    </td>
                </tr>

            </table>
        @endif

        @if(($data['action']))
            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            @if(isset($data['exam_data']['messages']) && $data['exam_data']['messages'])
                @foreach($data['exam_data']['messages'] as $message)
                    <div style="display: none" class="alert alert-warrning">
                        <strong> {!! $message !!} </strong>
                    </div>
                @endforeach
            @endif

            <div class=" col-xl-12  col-lg-12  col-md-12   col-sm-12 ">
                @if($questions = $view_model->getExamQuestions())
                    @foreach($questions as $question)
                        <div class="_QuestionDiv qlist" data-question="{!! $question['id'] !!}"
                             id="_question_{{ $loop->iteration }}">

                            <div>

                                {!! $view_model->getRowQuestion($question, $loop->iteration) !!}  </div>

                            <div style="direction: rtl; text-align: center">
                                {{--<span class="btn btn-warning btn-circle">{{ isset($question['exam_question']['score'])?$question['exam_question']['score']:'' }}</span>--}}
                            </div>
                        </div>


                    @endforeach
                @endif
                <div class="col-xl-12 col-lg-12 col-md-12  floatright loadmoreDiV ">
                    <a id="loadMore" class="btn btn-success"> @lang('exam::exam.next_questions') </a>

                </div>
                <div class="col-xl-12 col-lg-12 col-md-12  floatright next_pre_box ">
                    <a class="btn btn-success NextQ"> @lang('exam::exam.next_question') <i
                                class="fas fa-angle-left"></i> </a>
                    <a class="btn btn-primary PreQ"> @lang('exam::exam.prv_questions') <i
                                class="fas fa-angle-right"></i> </a>
                </div>
            </div>


            {!! FormHelper::openAction() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::submitByCancel(['title'=>trans("exam::exam.submit")], ['title'=>trans("exam::exam.cancel"), 'url'=>route('course.view',['course_id'=>$course?$course->id:0])]) !!}


        @else
            @if(isset($data['messages']) && $data['messages'])
                @foreach($data['messages'] as $message)
                    <div class="alert alert-danger  errobx5">
                        <strong><i class="fas fa-exclamation-triangle"></i>{!! $message !!} </strong>
                    </div>
                @endforeach
            @endif
        @endif


    </div>


    @if($view_model->getExamQuestions())
        <div class="floatright col-xl-3  box_course4 box_course29" id="fixedbox">
            <span class="timer" data-minutes-left="{!! $view_model->getExamTime()['total_minutes'] !!}"></span>
            {{-- 					 {!! $view_model->getExamTime()['text'] !!}--}}
            <h6>
                <a href="#">
                    <i class="fas fa-question-circle"></i>

                    @lang('exam::exam.help')
                </a>
            </h6>

            <h3>
                @lang('exam::exam.number_of_questions')
            </h3>
            <div class="box_course30">
                <div class="QuestionDiv smooth_link_speedy" id="list_box_questions">
                    @foreach($view_model->getExamQuestions() as $question)
                        <a class="Noasnwered _question_{{ $question['id'] }}"
                           href="#_question_{{ $loop->iteration }}">{{ $loop->iteration }}<span> - </span></a>
                    @endforeach
                </div>
            </div>

            <div class="Underbx74">

                <span data-class="" class="Underbx75"></span>
                @lang('exam::exam.answered_question')
                <br>
                {{--<span data-class="Markasnwered"  class="Underbx76"></span> سوالات نشان شده--}}
                {{--<br>--}}
                <span data-class="Noasnwered" class="Underbx77"></span>
                @lang('exam::exam.not_answered_question')
                <br>
                {!! FormHelper::submitByCancel(['title'=>trans("exam::exam.submit")], ['title'=>trans("exam::exam.cancel"), 'url'=>route('course.view',['course_id'=>$course?$course->id:0])]) !!}


            </div>

        </div>
    @endif
    {!! FormHelper::close() !!}
</div>
</div>
<script src="/themes/default/assets/js/jquery.simple.timer.js"></script>
<script src="/themes/default/assets/js/dojo.js"></script>
<script>

    window.onscroll = function () {
        myFunction()
    };

    var header = document.getElementById("fixedbox");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky2");
        } else {
            header.classList.remove("sticky2");
        }
    }
</script>


@php $params = $view_model->getActivity('params'); @endphp
<script>

    $(function () {
        var $perPage = {{isset($params->exam_per_page)?$params->exam_per_page:10000}}
        console.log($perPage);

        $("._QuestionDiv").slice(0, $perPage).show();
        $("#loadMore").on('click', function (e) {
            e.preventDefault();
            $("._QuestionDiv:hidden").slice(0, $perPage).slideToggle();


            if ($("._QuestionDiv:hidden").length == 0) {
                $("#loadMore").fadeOut('slow');
            }

        });


    });

</script>


<script>


    $(function () {
        // $("._QuestionDiv").hide();
        $(".PreQ").hide();
        $("#_question_1").addClass("DivShow");

    });

    $(".NextQ").click(function () {

        var Oldid = $("div .DivShow .question_no").html();
        var LASTID = $("._QuestionDiv").length;
        $("div").find(".DivShow").removeClass("DivShow");
        Oldid++;
        if (Oldid > 1) {
            $(".PreQ").show();
        }
        $("#_question_" + Oldid).addClass("DivShow");
        if (Oldid == LASTID) {
            $(".NextQ").hide();
        }


    });

    $(".PreQ").click(function () {
        var Oldid = $("div .DivShow .question_no").html();
        var LASTID = $("._QuestionDiv").length;
        $("div").find(".DivShow").removeClass("DivShow");
        Oldid--;
        if (LASTID > Oldid && Oldid > 0) {
            $("#_question_" + Oldid).addClass("DivShow");
        }
        if (Oldid < LASTID) {
            $(".NextQ").show();
        }
        if (Oldid == 1) {
            $(".PreQ").hide();
        }
    });


    var $perPage =
    {{isset($params->exam_per_page)?$params->exam_per_page:10000}}
    if ($perPage == 1) {
        $(".loadmoreDiV").hide();
        $(".next_pre_box").show();
    }
    else {
        var win = $(this); //this = window
        if (win.width() >= 768) {
            $(".next_pre_box").hide();
            $(".loadmoreDiV").show();
        }
        if (win.width() < 768) {
            $(".loadmoreDiV").hide();
            $(".next_pre_box").show();
        }

    }


</script>

<script>
    // $(function()
    // {

    // var hieghtThreshold = $("._QuestionDiv").offset().top;
    // var contents = $('.exam5');
    // var contents2 = $('.Foter-V2');


    // $(window).scroll(function()
    // {
    // var scroll = $(window).scrollTop();
    // if (scroll >=  hieghtThreshold )  {
    // contents2.addClass('makeabsol');


    // }
    // else
    // {    contents2.removeClass('makeabsol');}
    // });

    // });
</script>

<script>
    jQuery(document).ready(function () {

        $('body').on('click', 'input._exam_answer', function (event) {
            var $clicked = $(this);
            var $question_id = $clicked.attr('data-question');
            var $rowQuestion = $('div#list_box_questions').find('a._question_' + $question_id);
            $rowQuestion.removeClass('Noasnwered').find('span').html($clicked.attr('data-alpha'));
        });

        $('body').on('click', 'a._reset_answer', function (event) {
            var $clicked = $(this);
            $clicked.parents('div').first().find('input').prop('checked', false);
            var $question_id = $clicked.parents('div._QuestionDiv').first().attr('data-question');
            console.log($question_id);
            $('body').find('a._question_' + $question_id).addClass('Noasnwered').find('span').html('-');
        });


        var exp_sec = {!! $view_model->getExamTime()['total_seconds'] !!};
        exp_sec = exp_sec * 1000;
        console.log(exp_sec);

        @if($view_model->getExamQuestions())
        setInterval(submit_me, exp_sec); // (1000 * seconds)
        @endif

        function submit_me() {
            $('#form_exam').submit();
        }

    });
</script>