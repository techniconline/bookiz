<div class="row" id="form_attempt">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("exam::exam.attempt.".($attempt?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>
            {{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">

                {!! FormHelper::open(['role'=>'form','url'=>(isset($attempt) && $attempt? route('exam.user.attempt.update', ['exam_attempt_id'=>$attempt->id]) :null)
                        ,'method'=>(isset($attempt) && $attempt?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("exam::exam.tabs.public") </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::input('text','score',old('score',$attempt?$attempt->score:1) ,['required'=>'required','disabled'=>'disabled','title'=>trans("exam::exam.attempt.score"),'helper'=>trans("exam::exam.attempt.score")]) !!}
                                    {!! FormHelper::input('text','relative_score',old('relative_score',$attempt?$attempt->relative_score:null) ,['required'=>'required','title'=>trans("exam::exam.attempt.relative_score"),'helper'=>trans("exam::exam.attempt.relative_score")]) !!}

                                    {!! FormHelper::select('active',trans("exam::exam.statuses"),old('active',$attempt?$attempt->active:2)
                                    ,['title'=>trans("exam::exam.status"),'helper'=>trans("exam::exam.select_status")
                                    ,'placeholder'=>trans("exam::exam.select_status")]) !!}

                                    {!! FormHelper::editor('description',old('description',null)
                                    ,['title'=>trans("exam::exam.question.description"),'helper'=>trans("exam::exam.question.description")]) !!}

                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("exam::exam.submit")], ['title'=>trans("exam::exam.cancel"), 'url'=>route('exam.user.attempt.list',['course_activity_id'=>$attempt->course_activity_id])]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>
