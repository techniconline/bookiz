<?php

namespace Modules\Log\Http\Controllers\Logger;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;
use Modules\Log\Events\EventLogger;
use Modules\Log\Models\LogRoute\LogRoute;

class LoggerController extends Controller
{

    /** //Sample Event Log
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        $logRouteModel = new LogRoute();

        $params = [
            'description' => 'description 1'
            , 'user_type' => 'user_type 1'
            , 'user_id' => 'user_id 1'
            , 'route' => "route 1"
            , 'ip_address' => 'ip_address 1'
            , 'user_agent' => 'user_agent 1'
            , 'locale' => 'locale 1'
            , 'referer' => 'referer 1'
            , 'method_type' => 'method_type 1'
            , 'data' => 'data 1'
            , 'created_at' => date("Y-m-d H:i:s")
        ];
        dd(event(new EventLogger($logRouteModel->fill($params))));
//        return $masterViewModel->setRequest($request)->setViewModel('logRoute')
//            ->setActionMethod("saveLog")->response();
    }

    public function getList(Request $request, LogRoute $logRoute)
    {
        $result = $logRoute->search()
            ->match('ip_address',"127.0.0.1")
            ->paginate(10);

        dd($result->toArray());
    }

}
