<?php

Route::group(['middleware' => 'web', 'prefix' => 'log', 'as' => 'log.', 'namespace' => 'Modules\Log\Http\Controllers'], function () {

    Route::group(['prefix' => 'route', 'as' => 'route.', 'namespace' => '\Logger'], function () {

        Route::get('/save', 'LoggerController@store')->name('save');
        Route::get('/getList', 'LoggerController@getList')->name('getList');


    });


});
