<?php

namespace Modules\Log\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Log\Jobs\LoggerJob;
use Modules\Log\Models\LogRoute\LogRoute;
use Modules\Log\Models\LogRoute\LogRoutes;
use Sinergi\BrowserDetector\Browser;
use Sinergi\BrowserDetector\Os;

class Logger
{
    private $after_time;
    private $params;
    private $response;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * @param $request
     * @return mixed
     */
    private function createLog($request)
    {
        $this->after_time = microtime(true);
        $language = get_instance()->getLanguage();
        $instance = get_instance()->getCurrentInstance();
        $user = BridgeHelper::getAccess()->getUser();
        $this->params = [
            'description' => json_encode($request->header())
            , 'user_type' => $user ? "user" : "guest"
            , 'user_id' => $user ? $user->id : 0
            , 'route' => $request->method()
            , 'url' => $request->url()
            , 'ip_address' => $request->ip()
            , 'user_agent' => $request->header("user-agent")
            , 'locale' => $language ? $language->code : $request->getLocale()
            , 'referer' => $request->server->get("HTTP_REFERER")
            , 'method_type' => $request->method()
            , 'data' => json_encode($request->except('password'))
            , 'created_at' => date("Y-m-d H:i:s")
            , 'module' => get_instance()->getModuleNameFromUrl()
            , 'host' => $request->getHost()
            , 'instance' => $instance ? $instance->name : "wwww"
        ];
        //event(new EventLogger($logRouteModel->fill($params)));
        return $request;
    }

    /**
     * @param $request
     * @param $response
     */
    public function terminate($request, $response)
    {
        if (env("ELASTIC_LOGGER", false) || env("MYSQL_LOGGER", false)) {
            $this->createLog($request);

            $this->response = 201;
            if ($response instanceof Response) {
                $this->response = $response->status();
            }

            $browser = new Browser();
            $os = new Os();
            $details = [
                "version_os" => $os->getVersion(),
                "version_browser" => $browser->getVersion(),
            ];
            $response_time = microtime(true) - $this->after_time;
            $newParams = [
                'response_time' => $response_time
                , "is_mobile" => $os->isMobile()
                , 'is_robot' => $browser->isRobot()
                , 'response_code' => $this->response
                , 'response_message' => ''
                , 'os' => $os->getName()
                , 'browser' => $browser->getName()
                , 'details' => json_encode($details)
            ];


            $this->params = array_merge($this->params, $newParams);
//            Log::info([$this->params]);
            if (env("ELASTIC_LOGGER", false)) {
                $logRouteModel = new LogRoute();
                dispatch(new LoggerJob($logRouteModel->fill($this->params)))->onQueue('high');
            }
            if (env("MYSQL_LOGGER", false)) {
                $mysqlRouteModel = new LogRoutes();
                dispatch(new LoggerJob($mysqlRouteModel->fill($this->params), true))->onQueue('high');
            }
        }
    }

}
