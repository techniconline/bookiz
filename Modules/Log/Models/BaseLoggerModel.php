<?php

namespace Modules\Log\Models;

use Sleimanx2\Plastic\Facades\Plastic;
use Sleimanx2\Plastic\Searchable;
use Illuminate\Support\Facades\Log;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class BaseLoggerModel extends Eloquent
{

    use Searchable;
    public $incrementing =false;
    public $documentIndex = 'farayad';

    protected $primaryKey = 'id';

    protected $client;
    protected $exists_index;

    public function __construct()
    {
        $this->documentIndex = env("INDEX_ELASTIC_LOGGER", $this->documentIndex);
        $this->client = Plastic::getClient();
        $this->checkExistsIndex()->createIndex();
        $this->exists = true;
//        dd($this->client->indices()->delete(['index'=> $this->documentIndex]));
    }

    /**
     * @param $index
     * @return $this
     */
    private function checkExistsIndex($index = null)
    {
        $this->exists_index = false;
        $this->documentIndex = $index ?: $this->documentIndex;
        if ($this->client->indices()->exists(['index' => $this->documentIndex])) {
            $this->exists_index = true;
        }
        return $this;
    }

    /**
     * @return $this
     */
    private function createIndex()
    {
        if (!$this->exists_index && $this->documentIndex) {
            $this->client->indices()->create(['index' => $this->documentIndex]);
        }
        return $this;
    }

    /**
     * @param $index
     * @return $this
     */
    public function setIndex($index)
    {
        return $this->checkExistsIndex($index)->createIndex();
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function fill(array $attributes)
    {
        $attributes[$this->primaryKey] = isset($attributes[$this->primaryKey]) ? $attributes[$this->primaryKey] : $this->getId();
        parent::fill($attributes);
        return $this;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private $in_elastic = false;
    public function saveLog()
    {
        try{
            $resultSaveElastic = ["action"=>false];
            if (env('ONLY_LOG_ELASTIC',false)){
                $resultSaveElastic = $this->document()->save();
                $this->in_elastic = true;
            }else{
                $this->in_elastic = false;
            }

        }catch (\Exception $exception){
            report($exception);
            $this->in_elastic = false;
            $resultSaveElastic = ["action"=>false, 'msg'=>$exception->getMessage()];
        }

        $resultMongo = $this->saveMongodb();
//        $resultSaveFile = $this->saveLogFile();
        $result = ['resultElastic' => $resultSaveElastic, 'resultMongo' => $resultMongo];
        return $result;
    }

    /**
     * @return mixed
     */
    public function saveMongodb()
    {
        try{
            $this->attributes['in_elastic'] = $this->in_elastic;
            $resultMongo = $this->setConnection('mongodb')->insert($this->attributes);
        }catch (\Exception $exception){
            report($exception);
            $resultMongo = ["action"=>false, 'msg'=>$exception->getMessage()];
        }
        return $resultMongo;
    }

    /**
     * @param $collection
     * @return array
     */
    public function saveBulkLog($collection)
    {
        $resultSaveElastic = $this->document()->bulkSave($collection);

        $result = ['resultElastic' => $resultSaveElastic];
        return $result;
    }

    /**
     *
     */
    private function saveLogFile()
    {
        if (env('LOG_SYSTEM_FILE', false)) {
            Log::useDailyFiles(storage_path('/logs/' . $this->getDocumentIndex() . '/' . $this->getDocumentType() . '/logger.log'));
//            Log::info('START -------------------- %' . date('Y-m-d H:i:s') . '%');
            Log::info($this->toJson());
//            Log::info('END -------------------- %' . date('Y-m-d H:i:s') . '%');
        }
    }

    /**
     * @return string
     */
    private function getId()
    {
        return sha1($this->getDocumentIndex().'_'.$this->getDocumentType().'_'.str_random(16));
    }

}
