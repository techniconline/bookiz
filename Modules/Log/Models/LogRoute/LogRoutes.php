<?php

namespace Modules\Log\Models\LogRoute;

use Modules\Core\Models\BaseModel;

class LogRoutes extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['id', 'description', 'user_type', 'user_id', 'url', 'ip_address', 'user_agent', 'locale'
        , 'referer', 'method_type', 'data', 'in_elastic', 'host', 'is_mobile', 'is_robot', 'instance', 'os', 'browser'
        , 'module', 'response_time', 'response_code', 'response_message', 'details'
        , 'created_at', 'updated_at', 'deleted_at'];

}
