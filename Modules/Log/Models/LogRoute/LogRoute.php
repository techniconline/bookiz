<?php

namespace Modules\Log\Models\LogRoute;

use Modules\Log\Models\BaseLoggerModel;
use Modules\Log\Traits\LogRoute\LogRoute as TraitModel;

class LogRoute extends BaseLoggerModel
{

    use TraitModel;
    public $documentIndex = 'farayad';

    protected $primaryKey = 'id';

    /**
     *  $params = [
     *   'description'=>'test 1'
     * , 'user_type'=>'test 1'
     * , 'user_id'=>'test 1'
     * , 'url'=>'test 1'
     * , 'ip_address'=>'test 1'
     * , 'user_agent'=>'test 1'
     * , 'locale'=>'test 1'
     * , 'referer'=>'test 1'
     * , 'method_type'=>'test 1'
     * , 'data'=>'test 1'
     * ];
     *
     * @var array
     */
    protected $fillable = [
        'id', 'description'
        , 'user_type', 'user_id'
        , 'url', 'ip_address'
        , 'user_agent', 'locale'
        , 'referer', 'method_type'
        , 'data', 'created_at'
        , 'updated_at', 'deleted_at', 'in_elastic', 'is_robot', 'is_mobile'
        , 'module', 'host', 'instance', 'os', 'browser', 'response_time'
        , 'response_code', 'response_message', 'details'
    ];


    public $rules = [
        'user_type' => 'required',
        'url' => 'required',
        'ip_address' => 'required',
        'user_agent' => 'required',
        'method_type' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    public $searchable = ['id', 'description'
        , 'user_type', 'user_id'
        , 'url', 'ip_address'
        , 'user_agent', 'locale'
        , 'referer', 'method_type'
        , 'data', 'created_at'
        , 'updated_at', 'deleted_at', 'in_elastic', 'is_robot', 'is_mobile'
        , 'module', 'host', 'instance', 'os', 'browser', 'response_time'
        , 'response_code', 'response_message', 'details'
    ];


}
