<?php

namespace Modules\Log\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class Logger implements ShouldQueue
{

    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {

    }

    /**
     *
     * Handle the event.
     *
     * @param \Modules\Log\Events\EventLogger $event
     * @return void
     */
    public function handle(\Modules\Log\Events\EventLogger $event)
    {
        $model = new $event->loggerModel();
        $model->fill($event->loggerModelData);
        $model->saveLog();
    }
}
