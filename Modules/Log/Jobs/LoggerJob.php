<?php

namespace Modules\Log\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Database\Eloquent\Model;

class LoggerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $loggerModel;
    public $loggerModelData;
    public $mySql;

    /**
     * Create a new job instance.
     *
     * @param Model $model
     * @param bool $mysql
     */
    public function __construct(Model $model, $mysql = false)
    {
        $this->mySql = $mysql;
        $this->loggerModelData = $model->toArray();
        $this->loggerModel = get_class($model);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $model = new $this->loggerModel();
            $model->fill($this->loggerModelData);
            if ($this->mySql){
                $result = $model->save();
            }else{
                $result = $model->saveLog();
            }
            //var_dump($result);
        }catch (\Exception $exception){
            report($exception);
        }
    }
}
