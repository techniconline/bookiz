<?php

namespace Modules\Log\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;


class EventLogger
{
    use SerializesModels;
    public $loggerModel;
    public $loggerModelData;

    /**
     * Create a new event instance.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->loggerModelData = $model->toArray();
        $this->loggerModel = get_class($model);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

}
