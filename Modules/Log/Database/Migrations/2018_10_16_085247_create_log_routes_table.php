<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Jenssegers\Mongodb\Schema\Blueprint;

class CreateLogRoutesTable extends Migration
{
    /**
     * The name of the database connection to use.
     *
     * @var string
     */
    protected $connection = 'mongodb';

    public function up()
    {
        Schema::connection($this->connection)->create('log_routes', function (Blueprint $table) {
            $table->string('id')->index();
            $table->longText('description');
            $table->string('user_type');
            $table->unsignedBigInteger('user_id');
            $table->string('url');
            $table->string('ip_address');
            $table->string('user_agent');
            $table->string('locale');
            $table->string('referer');
            $table->string('method_type');
            $table->longText('data');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->drop('log_routes');
    }
}