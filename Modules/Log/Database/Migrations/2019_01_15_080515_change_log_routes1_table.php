<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Jenssegers\Mongodb\Schema\Blueprint;

class ChangeLogRoutes1Table extends Migration
{
    protected $connection = 'mongodb';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->table('log_routes', function (Blueprint $table) {
            $table->string('host')->nullable();
            $table->boolean('is_mobile')->nullable();
            $table->boolean('is_robot')->nullable();
            $table->string('instance')->nullable();
            $table->string('os')->nullable();
            $table->string('browser')->nullable();
            $table->string('module')->nullable();
            $table->double('response_time',10,5)->nullable();
            $table->smallInteger('response_code')->nullable();
            $table->text('response_message')->nullable();
            $table->text('details')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
