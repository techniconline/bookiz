<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMysqlLogRoutesTable extends Migration
{
    /**
     * The name of the database connection to use.
     *
     * @var string
     */
    public function up()
    {
        Schema::create('log_routes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('description')->nullable();
            $table->string('user_type')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('url')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('user_agent')->nullable();
            $table->string('locale')->nullable();
            $table->string('referer')->nullable();
            $table->string('method_type')->nullable();
            $table->longText('data')->nullable();
            $table->boolean('in_elastic')->default(false);
            $table->string('host')->nullable();
            $table->boolean('is_mobile')->nullable();
            $table->boolean('is_robot')->nullable();
            $table->string('instance')->nullable();
            $table->string('os')->nullable();
            $table->string('browser')->nullable();
            $table->string('module')->nullable();
            $table->double('response_time',10,5)->nullable();
            $table->smallInteger('response_code')->nullable();
            $table->text('response_message')->nullable();
            $table->text('details')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_routes');
    }
}