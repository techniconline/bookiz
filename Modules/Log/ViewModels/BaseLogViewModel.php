<?php

namespace Modules\Log\ViewModels;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\ViewModels\BaseViewModel;
use Sleimanx2\Plastic\Facades\Plastic;

class BaseLogViewModel extends BaseViewModel
{

    protected $index;
    protected $client;
    protected $exists_index;
    protected $params;
    private $elasticModel;
    protected $valid;
    private $document;

    public function __construct()
    {
        $this->index = env("INDEX_ELASTIC_LOGGER", "farayad");
        $this->client = Plastic::getClient();
        $this->checkExistsIndex()->createIndex();
    }

    /**
     * @param $index
     * @return $this
     */
    private function checkExistsIndex($index = null)
    {
        $this->exists_index = false;
        $this->index = $index ?: $this->index;
        if ($this->client->indices()->exists(['index' => $this->index])) {
            $this->exists_index = true;
        }
        return $this;
    }

    /**
     * @return $this
     */
    private function createIndex()
    {
        if (!$this->exists_index && $this->index) {
            $this->client->indices()->create(['index' => $this->index]);
        }
        return $this;
    }

    /**
     * @param $index
     * @return $this
     */
    protected function setIndex($index)
    {
        return $this->checkExistsIndex($index)->createIndex();
    }

    /**
     * @param Model $model
     * @return $this
     */
    protected function setElasticModel(Model $model)
    {
        try {
            $this->elasticModel = $model;
            $this->valid = true;
        } catch (\Exception $exception) {
            $this->valid = false;
        }
        return $this;
    }

    /**
     * @param $fills
     * @return $this
     */
    protected function setFill($fills)
    {
        if ($this->valid) {
            $this->elasticModel->fill($fills);
        }
        return $this;
    }

    /**
     * @return $this
     */
    protected function isValid()
    {
        if ($this->valid) {
            $result = $this->elasticModel->isValid();
            if (!$result) {
                $this->valid = $result;
                $this->errors = isset($this->elasticModel->errors) ? $this->elasticModel->errors : [];
            } else {
                $this->elasticModel->exists = $result;
            }
        }
        return $this;
    }

    /**
     * @return $this
     */
    protected function setDocument()
    {
        if ($this->valid) {
            try {
                $this->document = $this->elasticModel->document();
            } catch (\Exception $exception) {
                $this->valid = false;
            }
        }
        return $this;
    }

    /**
     * @return array
     */
    protected function save()
    {
        if ($this->valid) {
            $result = $this->document->save();
            return ['action' => true, 'result' => $result];
        }
        return ['action' => false, 'errors' => $this->errors];
    }

    /**
     * @return array
     */
    protected function delete()
    {
        if ($this->valid) {
            $result = $this->document->delete();
            return ['action' => true, 'result' => $result];
        }
        return ['action' => false, 'errors' => $this->errors];
    }

    /**
     * @return array
     */
    protected function update()
    {
        if ($this->valid) {
            $result = $this->document->update();
            return ['action' => true, 'result' => $result];
        }
        return ['action' => false, 'errors' => $this->errors];
    }

    /**
     * @return mixed
     */
    protected function getDocumentObject()
    {
        return $this->document;
    }

    /**
     * @return $this
     */
    private function changeIndex()
    {
        if ($this->index && $this->params && isset($this->params['index'])) {
            $this->params['index'] = $this->index;
        }
        return $this;
    }

    /**
     * $params = [
     * 'index' => 'my_index',
     * 'type' => 'my_type',
     * 'id' => 'my_id',
     * 'body' => ['testField' => 'abc']
     * ];
     * @param array $params
     * @return array
     */
    protected function createDocument(array $params = null)
    {
        $this->params = $params ?: $this->params;
        $this->changeIndex();
        try {
            $res = $this->client->index($this->params);
            return ['action' => true, 'document' => $res];
        } catch (\Exception $exception) {
            return ['action' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * $params = [
     * 'index' => 'my_index',
     * 'type' => 'my_type',
     * 'id' => 'my_id',
     * 'body' => [
     * 'doc' => [
     * 'new_field' => 'abc'
     * ]
     * ]
     * ];
     * @param array $params
     * @return array
     */
    protected function updateDocument(array $params = null)
    {
        $this->params = $params ?: $this->params;
        $this->changeIndex();
        try {
            $res = $this->client->update($this->params);
            return ['action' => true, 'document' => $res];
        } catch (\Exception $exception) {
            return ['action' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * $params = [
     * 'index' => 'my_index',
     * 'type' => 'my_type',
     * 'id' => 'my_id'
     * ];
     * @param array $params
     * @return array
     */
    protected function getDocument(array $params = null)
    {
        $this->params = $params ?: $this->params;
        $this->changeIndex();
        try {
            $res = $this->client->get($this->params);
            return ['action' => true, 'document' => $res];
        } catch (\Exception $exception) {
            return ['action' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * $params = [
     * 'index' => 'my_index',
     * 'type' => 'my_type',
     * 'id' => 'my_id'
     * ];
     * @param array $params
     * @return array
     */
    protected function deleteDocument(array $params = null)
    {
        $this->params = $params ?: $this->params;
        $this->changeIndex();
        try {
            $res = $this->client->delete($this->params);
            return ['action' => true, 'document' => $res];
        } catch (\Exception $exception) {
            return ['action' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * $params = [
     * 'index' => 'my_index',
     * 'type' => 'my_type',
     * 'id' => 'my_id'
     * ];
     * @param array $params
     * @return array
     */
    protected function getSourceDocument(array $params = null)
    {
        $this->params = $params ?: $this->params;
        $this->changeIndex();
        try {
            $res = $this->client->getSource($this->params);
            return ['action' => true, 'document' => $res];
        } catch (\Exception $exception) {
            return ['action' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * $params = [
     * 'index' => 'my_index',
     * 'type' => 'my_type',
     * 'body' => [
     * 'query' => [
     * 'match' => [
     * 'testField' => 'abc'
     * ]
     * ]
     * ]
     * ];
     * @param array $params
     * @return array
     */
    protected function searchDocument(array $params = null)
    {
        $this->params = $params ?: $this->params;
        $this->changeIndex();
        try {
            $res = $this->client->search($this->params);
            return ['action' => true, 'document' => $res];
        } catch (\Exception $exception) {
            return ['action' => false, 'message' => $exception->getMessage()];
        }
    }


}
