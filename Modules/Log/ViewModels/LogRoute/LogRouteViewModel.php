<?php

namespace Modules\Log\ViewModels\LogRoute;


use Modules\Log\Models\LogRoute\LogRoute;
use Modules\Log\ViewModels\BaseLogViewModel;

class LogRouteViewModel extends BaseLogViewModel
{

    /**
     * @return $this
     */
    protected function saveLogRoute()
    {
        $logRouteModel = new LogRoute();
        $params = $this->request->all();
        $result = $this->setElasticModel($logRouteModel)->setFill($params)->isValid()->setDocument()->save();
        return $this->setResponse($result);
    }



}
