<?php

namespace Modules\Core\Libraries;

use App\Http\Services\BaseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Modules\Core\Models\Instance;
use Modules\Core\Models\Language;

class InstanceLibraryBk
{
    private $language;
    private $instance;
    private $instance_model;
    private $request;

    public function __construct() {
        $this->instance_model = new Instance();
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @param $language
     * @return $this
     */
    public function setLanguage($language = null) {
        $this->language = $language?:$this->getLanguage();
        return $this;
    }

    /**
     * @param $instance
     * @return $this
     */
    public function setInstance($instance = null) {
        $this->instance = $instance?:$this->getInstance();
        return $this;
    }

    /**
     * @return \stdClass
     */
    private function getObject() {
        $stdClass = new \stdClass();
        $stdClass->instance = $this->instance;
        $stdClass->language = $this->language;
        $stdClass->request = $this->request;
        return $stdClass;
    }

    public function get()
    {
        return $this->setInstance()->setLanguage()->getObject();
    }

    /**
     * @return mixed
     */
    public function getInstanceDefault() {
        return config('app.instance');
    }

    private function getInstance() {
        $instance = null;
        $hostArray = explode(".", $this->request->getHost());
        $instanceModel = new Instance();
        $language = $this->getLanguage();
        $lang_id = $language?$language->id:null;
        $instances = $instanceModel->get()->where('language_id', $lang_id)->keyBy('name')->all();

        if(count($hostArray)==3){
            //has sub domain
            list($subdomain, $domain, $ext) = $hostArray;
            $lang = $this->request->segment(1);
            $instance = $this->request->segment(2);

            if (strlen($lang)>2){
                $instance = $lang;
            }

            $instance = isset($instances[$instance])?$instances[$instance]:null;
            $instance = $instance ?: (isset($instances[$subdomain])?$instances[$subdomain]:$this->getInstanceDefault());

        }elseif(count($hostArray)<3){
            // has not sub domain


        }else{
            // !!
        }
        return $instance;
    }

    private function getLanguage() {
        $lang = $this->request->segment(1);
        $language = new Language();
        $languages = $language->get()->where('active',1)->keyBy('code')->all();
        $defaultLang = Config::get('app.locale');

        if(strlen($lang)!=2){
            $language = isset($languages[$defaultLang])?$languages[$defaultLang]:null;
            return $language;
        }

        if (!isset($languages[$lang])){
            $language = isset($languages[$defaultLang])?$languages[$defaultLang]:null;
        }else{
            $language = $languages[$lang];
        }
        return $language;
    }



}
