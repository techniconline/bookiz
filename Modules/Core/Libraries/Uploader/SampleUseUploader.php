<?php
class UploaderServices extends ApiServices
{

    public $moduleUploaderServices;
    public $moduleUploaderMediaServices;

    public function __construct()
    {
        $this->moduleUploaderServices = new ModuleUploaderService();
        $this->moduleUploaderMediaServices = new MediaServices();
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function fileUpload(Request $request)
    {
        $result = $this->moduleUploaderServices->setUser($this->user)
            ->setRequest([], $request)
            ->setFileName('origin')->setTypeFile('file')->setUrlUpload()->upload();
        return $this->setResponse($result);
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function docUpload(Request $request)
    {
        $result = $this->moduleUploaderServices->setUser($this->user)
            ->setRequest([], $request)
            ->setFileName('origin')->setTypeFile('doc')->setUrlUpload()->upload();
        return $this->setResponse($result);
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function galleryImageUpload(Request $request)
    {
        $result = $this->moduleUploaderServices
            ->setUser($this->user)
            ->setRequest([], $request)
            ->setFileName('gallery_image')
            ->setTypeFile('galleryImage')
            ->setWidth(600)->setHeight(400)
//            ->setFileInputName('image')
            ->setUrlUpload()
            ->createThumbnail()
            ->resizeUpload();
        return $this->setResponse($result);
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function imageUpload(Request $request)
    {
        $result = $this->moduleUploaderServices->setUser($this->user)
            ->setRequest([], $request)
            ->setFileName('origin')->setTypeFile('image')->setUrlUpload()
            ->createThumbnail()->upload();
        return $this->setResponse($result);
    }


    /**
     * @param Request $request
     * @return $this
     */
    public function logoUpload(Request $request)
    {
        $result = $this->moduleUploaderServices->setUser($this->user)
            ->setRequest([], $request)
            ->setFileName('origin')->setTypeFile('logo')->setUrlUpload()
            ->setThumbNailImageSize(150,150)
            ->createThumbnail()->upload();
        return $this->setResponse($result);
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function videoUpload(Request $request)
    {
        $result = $this->moduleUploaderServices->setUser($this->user)
            ->setRequest([], $request)
            ->setFileName('origin')->setTypeFile('video')->setUrlUpload()->upload();
        return $this->setResponse($result);
    }

    /**
     * @param $media_id
     * @param Request $request
     * @return $this
     */
    public function deleteMedia($media_id, Request $request)
    {
        $result = $this->moduleUploaderMediaServices->setUser($this->user)
            ->setRequest([], $request)
            ->deleteMedia($media_id,$request);
        return $this->setResponse($result);
    }


}
