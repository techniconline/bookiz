<?php

namespace Modules\Code\Libraries\Uploader;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use JildertMiedema\LaravelPlupload\Facades\Plupload;
use Intervention\Image\Facades\Image;


class UploaderLibrary
{

    private $media;
    private $file;
    private $fileName;
    private $fileNameThumbnail;
    private $fileInputName;
    private $typeFile;
    private $extensionFile;
    private $sizeFile;
    private $urlUpload;
    private $folderUploadName;
    private $isValid;
    private $src;
    private $srcThumbnail;
    private $extensionValid;

    private $imageResizeValid;
    private $imageSize = [];
    private $titleImage;
    private $widthImage;
    private $wThumbNailImage = 300;
    private $heightImage;
    private $hThumbNailImage = 200;
    private $save_in_database;
    private $items = [];
    private $resultUploaded;
    private $quality;
    private $format_convert_to;
    private $log_file = false;
    private $max_width;
    private $max_height;
    private $storage;

    const STORAGE_PUBLIC = "public";
    const STORAGE_PUBLIC_MINIO = "minio";
    const STORAGE_VOD = "vod";
    const STORAGE_NFS_VOD = "nfs_vod";
    const STORAGE_NFS_PUBLIC = "nfs_public";

    public function __construct($inputName = 'file', $path = 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . '', $folderUploadName = null, $typeFile = 'doc')
    {
        $this->fileInputName = $inputName;
        if (is_null($folderUploadName)) {
            $this->folderUploadName = md5(time());
        } else {
            $this->folderUploadName = $folderUploadName;
        }
        $this->typeFile = $typeFile;
        $this->urlUpload = $path;
        $this->isValid = false;
        $this->setRequestInItems();
        $this->setUrlUpload($path, false);
        $this->setStorage();
    }

    public function setFolderUploadName($folder_name = null)
    {
        $this->folderUploadName = $folder_name ?: $this->folderUploadName;
        return $this;
    }

    /**
     * @param string $storage
     * @return $this
     */
    public function setStorage($storage = self::STORAGE_NFS_PUBLIC)
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * @return $this
     */
    private function setRequestInItems()
    {
        $this->items = request()->all();
        return $this;
    }

    /**
     * @param $mediaModel
     * @return $this
     */
    public function setModelMedia($mediaModel)
    {
        $this->media = $mediaModel;
        return $this;
    }

    /**
     * @return array
     */
    private function getDocExtensions()
    {
        $this->extensionValid = ['zip', 'doc', 'docx', 'xls', 'xlsx', 'pdf', 'csv'];
        return $this->extensionValid;
    }

    /**
     * @return array
     */
    private function getFileExtensions()
    {
        $this->extensionValid = ['zip', 'rar'];
        return $this->extensionValid;
    }

    /**
     * @return array
     */
    private function getImageExtensions()
    {
        $this->extensionValid = ['png', 'jpg', 'jpeg'];
        return $this->extensionValid;
    }

    /**
     * @return array
     */
    private function getImageMimiType()
    {
        $this->extensionValid = ['image/png', 'image/jpg', 'image/jpeg'];
        return $this->extensionValid;
    }

    /**
     * @return array
     */
    private function getLogoMimiType()
    {
        $this->extensionValid = ['image/png', 'image/jpg', 'image/jpeg'];
        return $this->extensionValid;
    }

    /**
     * @return array
     */
    private function getGalleryImageExtensions()
    {
        $this->extensionValid = ['jpg', 'jpeg'];
        return $this->extensionValid;
    }

    /**
     * @return array
     */
    private function getGalleryImageMimiType()
    {
        $this->extensionValid = ['image/jpg', 'image/jpeg'];
        return $this->extensionValid;
    }

    /**
     * @return array
     */
    private function getLogoExtensions()
    {
        $this->extensionValid = ['png', 'jpg', 'jpeg'];
        return $this->extensionValid;
    }

    /**
     * @return array
     */
    private function getVideoExtensions()
    {
        return ['mov', 'mp4', 'mpeg', 'mpg', 'vob', 'avi', 'mkv', 'ts', "wmv"];
    }

    /**
     * @return array
     */
    private function getVideoMimiType()
    {
        return ['video/mov', 'video/mp4', 'video/mpeg', 'video/dvd', 'video/mpegts', 'video/x-msvideo', "video/quicktime", "video/x-ms-wmv"];
    }

    /**
     * @param $name
     * @return $this
     */
    public function setFileInputName($name)
    {
        $this->fileInputName = $name;
        return $this;
    }

    /**
     * @param $typeFile
     * @return $this
     */
    public function setTypeFile($typeFile)
    {
        $this->typeFile = $typeFile;
        return $this;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitleImage($title)
    {
        $this->titleImage = $title;
        return $this;
    }

    /**
     * @param $width
     * @return $this
     */
    public function setWidth($width)
    {
        $this->widthImage = $width;
        return $this;
    }

    /**
     * @param $height
     * @return $this
     */
    public function setHeight($height)
    {
        $this->heightImage = $height;
        return $this;
    }

    /**
     * @return $this
     */
    public function setImageResize()
    {
        if ($this->isValidResizeImage()) {
            $this->imageSize[] = [
                'title' => $this->titleImage ?: 'origin',
                'width' => $this->widthImage,
                'height' => $this->heightImage
            ];
        }
        return $this;
    }

    /**
     * @param $w
     * @param $h
     * @return $this
     */
    public function setThumbNailImageSize($w, $h)
    {
        $this->wThumbNailImage = $w;
        $this->hThumbNailImage = $h;
        return $this;
    }

    /**
     * @return bool
     */
    private function isValidResizeImage()
    {
        if (!$this->widthImage || !$this->heightImage) {
            return $this->imageResizeValid = true;
        }
        return $this->imageResizeValid = false;
    }

    /**
     * @param $extension
     * @return $this
     */
    public function setExtension($extension)
    {
        $this->extensionFile = $extension;
        $this->setItems('extension', $this->extensionFile);
        return $this;
    }

    /**
     * @param $size
     * @return $this
     */
    public function setSize($size)
    {
        $this->sizeFile = $size;
        return $this;
    }

    /**
     * @param string $urlUpload
     * @param bool $bytype
     * @return $this
     */
    public function setUrlUpload($urlUpload = 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . '', $bytype = true)
    {
        $this->urlUpload = null;
        if ($bytype) {
            $this->urlUpload = $this->urlUpload . ($urlUpload ?: '') . DIRECTORY_SEPARATOR . $this->typeFile . DIRECTORY_SEPARATOR . $this->folderUploadName;
        } else {
            $this->urlUpload = $this->urlUpload . ($urlUpload ?: '') . DIRECTORY_SEPARATOR . $this->folderUploadName;
        }
        $this->urlUpload = str_replace("//", DIRECTORY_SEPARATOR, $this->urlUpload);
        return $this;
    }

    /**
     * @param $fileName
     * @return $this
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @param $file
     * @return $this
     */
    private function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @param string $format_convert_to
     * @param int $quality
     * @return $this
     */
    public function setConvertFormat($format_convert_to = 'jpg', $quality = 80)
    {
        $this->format_convert_to = $format_convert_to;
        $this->quality = $quality;
        return $this;

    }

    /**
     * @return mixed
     */
    private function getSrc()
    {
        $this->src = $this->urlUpload . DIRECTORY_SEPARATOR . $this->fileName;
        $this->src = str_replace("app" . DIRECTORY_SEPARATOR . "public", "", $this->src);
        $this->src = str_replace("//", DIRECTORY_SEPARATOR, $this->src);
        return $this->src;
    }

    /**
     * @return mixed
     */
    private function getSrcDestination()
    {
        $destination = $this->urlUpload . DIRECTORY_SEPARATOR . $this->fileName;
        $destination = str_replace("//", DIRECTORY_SEPARATOR, $destination);
        return $destination;
    }

    /**
     * @return mixed
     */
    private function getSrcThumbnail()
    {
        $this->srcThumbnail = $this->urlUpload . DIRECTORY_SEPARATOR . $this->fileNameThumbnail;
        $this->srcThumbnail = str_replace("app" . DIRECTORY_SEPARATOR . "public", "", $this->srcThumbnail);
        $this->srcThumbnail = str_replace("//", DIRECTORY_SEPARATOR, $this->srcThumbnail);
        return $this->srcThumbnail;
    }

    /**
     * @return bool
     */
    private function isValidMimiTypeFile()
    {
        $fileType = $this->file->getMimeType();

        if (method_exists($this, 'get' . ucfirst($this->typeFile) . 'MimiType')) {
            if (in_array($fileType, $this->{'get' . ucfirst($this->typeFile) . 'MimiType'}())) {
                return true;
            }
        } else {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function isValidFile()
    {
        if ($this->isValid) {
            return $this->isValid;
        }

        if ($this->file) {

            try {
                $fileExtension = $this->file->clientExtension();
            } catch (\Exception $exception) {
                $fileExtension = null;
            }


            if (!$fileExtension && $this->isValidMimiTypeFile()) {
                $fileExtension = $this->file->getClientOriginalExtension();
            } else {
                $this->isValid = true;
            }

            if ($this->format_convert_to) {
                $fileExtension = $this->format_convert_to;
            }

            $this->setExtension($fileExtension);

            $this->fileName = $this->fileName ? $this->fileName . '.' . $fileExtension : $this->file->getClientOriginalName();
            $this->fileNameThumbnail = substr($this->fileName, 0, strrpos($this->fileName, '.')) . '_thumbnail.' . $fileExtension;

            if (in_array(strtolower($fileExtension), $this->{'get' . ucfirst($this->typeFile) . 'Extensions'}())) {
                $this->isValid = true;
            }

        } else {
            $this->isValid = false;
        }

        return $this->isValid;
    }

    /**
     * @param $src
     * @param bool $storage
     * @return mixed
     */
    private function checkFileExist($src, $storage = true)
    {
        $exists = Storage::disk($this->storage)->exists($src); // $this->getSrc()
        return $exists;
    }

    /**
     * @param $src
     * @param bool $storage
     * @return bool
     */
    private function renameFile($src, $storage = true)
    {
        if ($this->checkFileExist($src, $storage)) {
            $srcArr = explode(DIRECTORY_SEPARATOR, $src);
            $file_name = end($srcArr);
            unset($srcArr[count($srcArr) - 1]);
            $newUrl = implode(DIRECTORY_SEPARATOR, $srcArr);
            $newUrl .= '/rename_' . time() . '_' . $file_name;
            return Storage::disk($this->storage)->move($src, $newUrl);
        }
        return false;
    }

    /**
     * @return array
     */
    public function upload()
    {
        ini_set('max_execution_time', 600);
//        ini_set('memory_limit', '2048M');
        Plupload::receive($this->fileInputName, function ($file) {
            if ($this->setFile($file)->isValidFile()) {
                $this->setItems('src', $this->getSrc())
                    ->setItems('destination', $this->getSrcDestination())
                    ->setItems('size', $this->file->getSize())
                    ->setItems('type', $this->typeFile);

                if ($this->log_file) {
                    $renameFile = $this->renameFile($this->getSrc());
                    $this->setItems('rename_file', $renameFile);
                }

//                $resultUpload = Storage::disk($this->storage)->put($this->src, File::get($file));
                $resultUpload = Storage::disk($this->storage)->put($this->src, fopen($file, 'r+'));

                if ($resultUpload) {
                    $this->saveMedia();
                }

            } else {
                $this->resultUploaded = ['action' => $this->isValid, 'message' => trans('core::messages.uploadFileIsNotValid'),
                    'request' => ['type' => $this->typeFile, 'src' => $this->getSrc(), 'destination' => $this->getSrcDestination(), 'extensionValid' => $this->extensionValid]];
            }
        });
        if (!$this->resultUploaded) {
            $this->resultUploaded = ['action' => false, 'message' => trans('core::messages.uploadFileNotFind')];
        }
        return $this->resultUploaded;
    }


    /**
     * @param bool $fit
     * @return $this
     */
    public function createThumbnail($fit = false)
    {
        $file = request()->file($this->fileInputName);
        if ($this->setFile($file)->isValidFile()) {
            $image = Image::make($file);
            if ($fit) {
                $image = $image->fit($this->wThumbNailImage, $this->hThumbNailImage, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                $image = $image->resize($this->wThumbNailImage, $this->hThumbNailImage, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $resultUpload = Storage::disk($this->storage)->put($this->getSrcThumbnail(), (string)$image->encode($this->format_convert_to, $this->quality));
        }
        return $this;
    }

    /**
     * @return array
     */
    public function resizeUpload()
    {
        $file = request()->file($this->fileInputName);
        if ($this->setFile($file)->isValidFile()) {
            $this->setItems('src', $this->getSrc())
                ->setItems('type', $this->typeFile);

            if ($this->log_file) {
                $renameFile = $this->renameFile($this->getSrc());
                $this->setItems('rename_file', $renameFile);
            }


            $image = Image::make($file)->encode($this->format_convert_to, $this->quality);

            //set max width or height if set maximum value
            $this->setSizeImage($image);

            $image = $image->resize($this->widthImage, $this->heightImage, function ($constraint) {
                $constraint->aspectRatio();
            });

            $resultUpload = Storage::disk($this->storage)->put($this->getSrc(), (string)$image->encode($this->format_convert_to, $this->quality));
            if ($resultUpload) {
                $this->saveMedia();
            }

        } else {
            $this->resultUploaded = ['action' => $this->isValid, 'message' => trans('core::messages.uploadFileIsNotValid'),
                'request' => ['type' => $this->typeFile, 'src' => $this->getSrc(), 'extensionValid' => $this->extensionValid]];
        }
        if (!$this->resultUploaded) {
            $this->resultUploaded = ['action' => false, 'message' => trans('core::messages.uploadFileNotFind')];
        }
        return $this->resultUploaded;
    }

    /**
     * @param $image
     * @return $this
     */
    private function setSizeImage($image)
    {
        $width = $image->width();
        $height = $image->height();

        if ($width >= $height) {
            if ($this->max_width >= $width) {
                $this->widthImage = $width;
            } elseif ($this->max_width) {
                $this->widthImage = $this->max_width;
            }
        } else {
            if ($this->max_height >= $height) {
                $this->heightImage = $height;
            } elseif ($this->max_height) {
                $this->heightImage = $this->max_height;
            }
        }


        return $this;
    }

    /**
     * @param int $max_width
     * @param int $max_height
     * @return $this
     */
    public function setMaxImageSize($max_width = 0, $max_height = 0)
    {
        $this->max_width = $max_width;
        $this->max_height = $max_height;
        return $this;
    }

    /**
     * @param bool $log
     * @return $this
     */
    public function setLogFile($log = true)
    {
        $this->log_file = $log;
        return $this;
    }


    /**
     * @param bool $save_id_db
     * @return $this
     */
    public function setSaveMediaInDatabase($save_id_db = false)
    {
        $this->save_in_database = $save_id_db;
        return $this;
    }

    /**
     * @return $this
     */
    private function saveMedia()
    {
        if (!$this->save_in_database) {
            $this->resultUploaded = ['action' => true, 'items' => $this->items];
        } else {
            if ($this->media->fill($this->items)) {
                $this->media->save();
                $this->resultUploaded = ['action' => $this->isValid, 'message' => trans('core::messages.uploadFileSuccess'), 'data' => $this->media, 'items' => $this->items];
            } else {
                $this->resultUploaded = ['action' => false, 'message' => trans('core::messages.uploadFileUnSuccess'), 'errors' => $this->media->errors];
            }
        }
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setItems($key, $value)
    {
        $this->items[$key] = $value;
        return $this;

    }

    /**
     * @return bool
     */
    public function hasChunks()
    {
        return (bool)request()->get('chunks', false);
    }


    public function setUploadPath($path)
    {
        $this->urlUpload = $path;
        return $this;
    }

}
