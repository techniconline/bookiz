<?php namespace Modules\Core\Libraries\Theme;

use \Facuz\Theme\Theme as ThemeOrigin;

class Theme extends ThemeOrigin {

    /**
     * add by Ahmad Khorshidi
     * @param $key
     * @return bool
     */
    public function hasArgument($key)
    {
        $value = $this->getContentArgument($key);
        return (bool) $value;
    }


}