<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 10/22/18
 * Time: 12:58 PM
 */

namespace Modules\Core\Libraries\Filemanager;

use RFM\Repository\S3\Storage as S3Storage;

class Storage extends S3Storage
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * Initiate uploader instance and handle uploads.
     *
     * @param ItemModel $model
     * @return UploadHandler
     */
    public function initUploader($model)
    {
        return new UploadHandler([
            'model' => $model,
            'storage' => $this,
        ]);
    }

}