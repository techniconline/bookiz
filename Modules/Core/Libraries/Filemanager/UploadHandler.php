<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 10/22/18
 * Time: 12:58 PM
 */

namespace Modules\Core\Libraries\Filemanager;

use RFM\Repository\S3\UploadHandler as S3UploadHandler;

class UploadHandler extends S3UploadHandler
{
    public function __construct($options = null, $initialize = false, $error_messages = null)
    {
        parent::__construct($options, $initialize, $error_messages);
        $this->options['image_versions'] = array(
            '' => array(
                'auto_orient' => $this->storage->config('images.main.autoOrient'),
                'max_width' => $this->storage->config('images.main.maxWidth'),
                'max_height' => $this->storage->config('images.main.maxHeight'),
                'jpeg_quality' => $this->storage->config('images.main.jpeg_quality'),
            ),
        );
    }
}