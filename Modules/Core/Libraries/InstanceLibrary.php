<?php

namespace Modules\Core\Libraries;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Instance\TokenInstance;
use Modules\Core\Models\Language\Language;
use Modules\Core\Models\Template\Template;
use Illuminate\Support\Facades\Cache;
use Nwidart\Modules\Facades\Module;
use Illuminate\Routing\Router;

class InstanceLibrary
{
    private $language;
    private $instance;
    private $dupRequest;
    private $request;
    private $host;
    private $method_name;
    private $module_name;
    private $module_name_from_url;
    private $is_subdomain = false;
    private $is_ajax = false;
    private $current_instance_api;
    private $current_instance_user_admin;
    private $assets = [];
    private $userToken = null;
    private $sub_domain;
    private $theme = false;
    private $router;
    private $relationalData = [];


    public function init(Request $request)
    {
        $this->request = $request;
        $this->host = $request->getScheme() . '://' . $request->getHost() . '/';

        $subdomain = $this->getSubDomain($request->getHost());

        $this->sub_domain = $subdomain;
        $this->dupRequest = $request->duplicate();
        $step = 1;
        $segments = [];
        $segment = $this->getSegment($this->dupRequest, $step);
        $this->language = $this->languageDetector($segment);
        $lang_code = config('app.locale');
        if ($this->language) {
            $lang_code = $this->language->code;
            $segments[] = $segment;
            $step = 2;
        }

        if ($this->isDefaultInstance($subdomain)) {
            $segment = $this->getSegment($this->dupRequest, $step);
            $this->instance = $this->InstanceDetector($segment);
            if ($this->instance) {
                $segments[] = $segment;
            }
        }

        if (!$this->instance) {
            $this->is_subdomain = true;
            $this->instance = $this->InstanceDetector($subdomain);
        }
        if (!$this->instance) {
            $this->instance = $this->InstanceDetector('www');
        }

        return $this->generateRequest();
    }

    /**
     * @param Router $router
     * @param Request $request
     */
    public function setMultiLangRoutes(Router $router, Request $request)
    {
        $locale = $request->segment(1);
        if (strlen($locale) != 2) {
            $locale = config('app.local');
        }
        \app()->setLocale($locale);
        $router->group(['prefix' => $locale], function ($router) {
            $modules = Module::all();
            foreach ($modules as $module) {
                require $module->getPath() . '/Http/routes.php';
            }

        });
    }

    /**
     * @param $url
     * @return mixed
     */
    public function getCurrentUrl($url)
    {
        $this->instance;
        $this->getCurrentInstance();
        $this->getCurrentSubDomain();

        $subDomain = $subDomainOrg = $this->getCurrentSubDomain();
        $currentInstance = $this->getCurrentInstance();

        if ($subDomainOrg != $currentInstance->name) {
            $subDomain = $currentInstance->name;
        }

        $url = str_replace($subDomainOrg . ".", $subDomain . ".", $url);
        return $url;
    }

    /**
     * @return mixed
     */
    public function getCurrentSubDomain()
    {
        return $this->sub_domain;
    }

    /**
     * @param $instance
     * @return $this
     */
    public function setInstance($instance)
    {
        $this->instance = $instance;
        return $this;
    }

    /**
     * @param $language
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModuleName()
    {
        return $this->module_name;
    }

    /**
     * @return mixed
     */
    public function getModuleNameFromUrl()
    {
        $segments = \request()->segments();
        foreach ($segments as $segment) {
            if (strlen($segment) > 2) {
                return $this->module_name_from_url = $segment;
            }
        }
        if (!$this->module_name_from_url) {
            return "core";
        }
        return $this->module_name_from_url;
    }

    /**
     * @return mixed
     */
    public function setModuleName($module_name)
    {
        $this->module_name = $module_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMethodName()
    {
        return $this->method_name;
    }

    /**
     * @return $this
     */
    private function generateRequest()
    {
        $pathArr = explode("/", $this->dupRequest->path());

        $lang = $this->language ? $this->language->code : null;
        $instance = $this->instance ? $this->instance->name : null;

        $rootUri = [];

        if ($lang && ($indexLang = array_search($lang, $pathArr)) === 0) {
            unset($pathArr[$indexLang]);
        }

        $indexInstance = array_search($instance, $pathArr);
        if ($instance && is_int($indexInstance) && $indexInstance <= 1) {
            unset($pathArr[$indexInstance]);
        }

        if (!$this->isDefaultLanguage($lang)) {
            $rootUri[] = $lang;
        }

        if (!$this->isDefaultInstance($instance) && !$this->is_subdomain) {
            $rootUri[] = $instance;
        }
        \app()->setLocale($lang);

        $path = implode("/", $pathArr);
        $uri = implode('/', $rootUri);

        // set module and method
        $valuesPath = array_values($pathArr);
        $this->module_name = array_first($valuesPath);
        $this->module_name = $this->module_name ?: 'core';
//        $this->method_name = isset($valuesPath[1]) ? $valuesPath[1] : null;
        $this->method_name = end($valuesPath);

        $uri = $this->host . $uri;
        \URL::forceRootUrl($uri);
        $this->request->server->set('REQUEST_URI', $path);

        return $this->setCurrentInstance();
    }

    /**
     * @return $this
     */
    public function setCurrentInstance()
    {
        if ($this->instance) {
            if ($this->instance->name == 'admin') {
                $this->setCurrentInstanceUserAdmin();
            } elseif ($this->instance->name == 'api') {
                $this->setCurrentInstanceApi();
            }

        }
        return $this;
    }

    /**
     * get current instance user or api
     * @return mixed|bool
     */
    public function getCurrentInstance()
    {

        if ($this->instance) {
            if ($this->instance->name == config('app.admin')) {
                return $this->current_instance_user_admin;
            } elseif ($this->instance->name == config('app.api')) {
                return $this->current_instance_api;
            }
        }
        return $this->instance;
    }


    /**
     * @return string
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return bool
     */
    public function isApi()
    {
        $subDomain = $this->getCurrentSubDomain();
        if ((($subDomain == \config('app.api', "api")))) {
            $this->setIsAjax(true);
            return true;
        }
        return false;
    }


    /**
     * @param bool $status
     * @return $this
     */
    public function setIsAjax($status = true)
    {
        $this->is_ajax = $status;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAjax()
    {
        if (\request()->get("is_ajax") || \request()->ajax()) {
            $this->setIsAjax(true);
        }
        return $this->is_ajax;
    }

    /**
     * @param $host
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getSubDomain($host)
    {
        $subdomain = trim(str_replace(config('app.host'), '', $host), '.');
        if (strlen($subdomain)) {
            return $subdomain;
        }
        return config('app.instance');
    }

    /**
     * @param $request
     * @param $step
     * @return string
     */
    public function getSegment($request, $step)
    {
        return strtolower($request->segment($step));
    }

    /**
     * @param $name
     * @return bool
     */
    public function isDefaultInstance($name)
    {
        if ($name == config('app.instance')) {
            return true;
        }
        return false;
    }

    /**
     * @param $instanceName
     * @return bool|mixed
     */
    public function InstanceDetector($instanceName)
    {
        $instance = $this->getInstanceModel($instanceName);
        if ($instance) {
            if (!$this->language) {
                $this->language = $this->getLanguageModel($instance->language_id, false);
            }
            return $instance;
        }
        return false;
    }

    /**
     * @param $code
     * @return bool
     */
    public function isDefaultLanguage($code)
    {
        if ($code == config('app.locale')) {
            return true;
        }
        return false;
    }

    /**
     * @param $lang
     * @return bool
     */
    public function languageDetector($lang)
    {
        $language = $this->getLanguageModel($lang);
        if ($language) {
            return $language;
        }
        return false;
    }

    /**
     * @param null $lang
     * @param bool $by_code
     * @return bool
     */
    public function getLanguageModel($lang = null, $by_code = true)
    {
        $languages = $this->getLanguages($by_code);
        if (isset($languages[$lang])) {
            return $languages[$lang];
        } else {
            $lang_code = config('app.locale');
            if (isset($languages[$lang_code])) {
                return $languages[$lang_code];
            }
        }
        return false;
    }

    /**
     * @param bool $by_code
     * @return mixed
     */
    public function getLanguages($by_code = true)
    {
        $langModel = new Language();
        $languages = $langModel->get()
            ->where('active', 1);
        if ($by_code) {
            $languages = $languages->keyBy('code');
        } else {
            $languages = $languages->keyBy('id');
        }
        $languages = $languages->all();
        return $languages;
    }

    /**
     * @param bool $code
     * @param bool $by_code
     * @return bool|mixed
     */
    public function getInstanceModel($code = false, $by_code = true)
    {
        $instances = $this->getInstances($by_code);
        if ($code) {
            return isset($instances[$code]) ? $instances[$code] : false;
        }
        return false;
    }

    /**
     * @param bool $by_code
     * @return bool|mixed
     */
    public function getInstances($by_code = true)
    {
        $instanceModel = new Instance();
        $instances = $instanceModel->active()->where(function ($q) {
//            if ($this->language) {
//                $q->where('language_id', $this->language->id);
//            }
        })->get();
        if ($by_code) {
            $instances = $instances->keyBy('name');
        } else {
            $instances = $instances->keyBy('id');
        }
        $instances->all();
        return $instances;
    }

    /**
     *
     * @return instance
     */
    public function getInstance()
    {
        if (!$this->instance) {
            $this->instance = $this->InstanceDetector('www');
        }
        return $this->instance;
    }

    /**
     * @return language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function getLanguageDirection()
    {
        $lang = $this->language;
        return $lang ? strtolower($lang->direction) : 'ltr';
    }

    /**
     * @return string
     */
    public function getLanguageCode()
    {
        $lang = $this->language;
        return $lang ? strtolower($lang->code) : config('app.locale');
    }

    /**
     * @return language_id
     */
    public function getLanguageId()
    {
        return $this->language ? $this->language->id : 0;
    }

    /**
     * @return instance_id
     */
    public function getInstanceId()
    {
        return $this->instance ? $this->instance->id : 0;
    }

    /**
     * @return int
     */
    public function getCurrentInstanceId()
    {
        $currentInstance = $this->getCurrentInstance();
        return $currentInstance ? $currentInstance->id : 0;
    }


    /**
     * @return bool
     */
    public function isSubDomain()
    {
        return $this->is_subdomain;
    }

    /**
     * wstoken is token instance , check in database
     *
     */
    private function setCurrentInstanceApi()
    {
        if ($wstoken = $this->request->get("wstoken")) {
            $tokenInstance = new TokenInstance();
            $tokenInstance = $tokenInstance->setRequestItems($this->request->all(["wstoken"]))->active()->filterToken()->with(['instance'])->first();
            $this->current_instance_api = $tokenInstance ? $tokenInstance->instance : null;
        }
        return $this;
    }

    public function setCurrentInstanceById($instance_id)
    {
        session()->put('current_instance_id', $instance_id);
        $this->current_instance_user_admin = $this->getInstanceModel($instance_id, false);
    }

    /**
     * get instance of user
     * @return $this
     */
    private function setCurrentInstanceUserAdmin()
    {
        $current_instance_admin_id = session()->get('current_instance_id');
        $instance = $this->instance;
        //clear session
        if ($current_instance_admin_id == $this->getInstanceId()) {
            return $this->clearSessionCurrentInstance($instance);
        }

        if ($current_instance_admin_id) {
            $instance = $this->getInstanceModel($current_instance_admin_id, false);
        }

        $this->current_instance_user_admin = $instance;

        return $this;
    }

    /**
     * @param $instance
     * @return $this
     */
    private function clearSessionCurrentInstance($instance)
    {
        $this->current_instance_user_admin = $instance;
        session()->forget('current_instance_id');
        return $this;
    }


    public function setActionAssets($assets)
    {
        foreach ($assets as $asset) {
            $this->assets[] = $asset;
        }
        return $this;
    }


    public function getActionAssets()
    {
        return $this->assets;
    }


    public function setUserToken($token)
    {
        $this->userToken = $token;
        return $this;
    }

    public function getUserToken()
    {
        return $this->userToken;
    }

    public function getInstancePath()
    {
        return $this->getCurrentInstance()->name . DIRECTORY_SEPARATOR;
    }


    /**/
    public function getInstanceTheme()
    {
        if ($this->theme) {
            return $this->theme;
        }
        if ($this->instance && $this->instance->frontend_template_id) {
            $key = 'get_template_id_' . $this->instance->frontend_template_id;
            $this->theme = Cache::get($key);
            if (!$this->theme) {
                $this->theme = Template::find($this->instance->frontend_template_id);
                Cache::forever($key, $this->theme);
            }
            return $this->theme;
        }
        return false;
    }

    /**/
    public function setRelationalData($name, $value)
    {
        $this->relationalData[$name] = $value;
        return $this;
    }

    public function hasRelationalData($name)
    {
        if (isset($this->relationalData[$name])) {
            return true;
        }
        return false;
    }

    public function getRelationalData($name)
    {
        if (isset($this->relationalData[$name])) {
            return $this->relationalData[$name];
        }
        return false;
    }
}
