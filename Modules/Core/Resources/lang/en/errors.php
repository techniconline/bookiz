<?php

return [
    'instance' => [
        'name_required'=>'نام را مشخص کنید!',
        'name_unique'=>'نام تکراری می باشد!',
        'language_id_required'=>'زبان نامشخص است!',
        'currency_id_required'=>'واحد ‍‍‍‍‌‌پول نامشخص است!',
    ],
    'feature' => [
        'title_required'=>'نام را مشخص کنید!',
        'language_id_required'=>'زبان نامشخص است!',
        'type_required'=>'نوع نامشخص است!',
    ],
    'featureValue' => [
        'value_required'=>'مقدار را مشخص کنید!',
        'language_id_required'=>'زبان نامشخص است!',
        'feature_id'=>'ویژگی نامشخص است!',
    ],
    'featureValueRelation' => [
        'language_id_required'=>'زبان نامشخص است!',
        'feature_id'=>'ویژگی نامشخص است!',
    ],
    'featureGroup' => [
        'title_required'=>'نام را مشخص کنید!',
        'instance_id_required'=>'سامانه را مشخص کنید!',
        'language_id_required'=>'زبان نامشخص است!',
    ],
    'featureGroupRelation' => [
        'feature_id_required'=>'ویژگی را مشخص کنید!',
        'feature_group_id_required'=>'گروه ویژگی نامشخص است!',
    ],
    'menu' => [
        'url_required'=>'آدرس را مشخص کنید!',
        'url_unique'=>'آدرس تکراری می باشد!',
        'title_required'=>'عنوان نامشخص است!',
        'type_required'=>'نوع آدرس نامشخص است! url , route or external url ',
    ],
    'template' => [
        'code_required'=>'کد را مشخص کنید!',
        'code_unique'=>'کد تکراری می باشد!',
        'name_required'=>'عنوان نامشخص است!',
    ],
    'language' => [

    ]
];