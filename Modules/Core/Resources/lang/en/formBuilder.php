<?php

return [
    'list' => 'لیست فرم ها',
    'instances' => 'سامانه های سیستم',
    'languages' => 'زبانهای سیستم',
    'select_language' => 'انتخاب زبان',
    'select_instance' => 'انتخاب سامانه',
    'select_status' => 'انتخاب وضعیت',
    'confirm_delete' => 'آیا از حذف اطمینان دارید؟',
    'confirm' => 'آیا اطمینان دارید؟',
    'select_teacher' => 'انتخاب مدرس',

    'delete' => 'حذف فرم',
    'add' => 'افزودن فرم',
    'edit' => 'ویرایش فرم',

    'title' => 'عنوان',
    'params' => 'شخصی سازی',
    'meta_groups' => 'متاها',
    'edit_meta_tags' => 'مدیریت متاها',
    'copy_meta_group' => 'کپی از متا',
    'del_meta_group' => 'حذف متا',

    'instance' => 'مشخصه سامانه (instance)',
    'cancel' => 'انصراف',
    'submit' => 'ثبت',
    'status' => 'وضعیت',
    'statuses' => [
        "0" => "حذف شده",
        "1" => "فعال",
        "2" => "غیرفعال",
    ],
    'record'=>[
        'list'=>'لیست ردیفهای ثبت شده در فرم',
        'changeStatus'=>'تغییر وضعیت',
        'show'=>'نمایش',
        'user_name'=>'نام کاربری',
        'created_at'=>'تاریخ ایجاد',
        'updated_at'=>'تاریخ تغییر',
    ],

];