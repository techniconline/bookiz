<?php

return [

    'list' => 'فهرست متا گروه ها',
    'add' => 'افزودن متا گروه',
    'edit' => 'ویرایش متا گروه',
    'delete' => 'حذف',
    'active' => 'وضعیت',
    'enable' => 'فعال',
    'disable' => 'غیرفعال',
    'confirm_delete' => 'آیا از حذف اطمینان دارید؟',
    'cancel' => 'انصراف',
    'submit' => 'ثبت',
    'select_language'=>'انتخاب زبان',
    'select_instance'=>'انتخاب سامانه',
    'select_system'=>'انتخاب ماژول',
    'systems'=>'ماژول ها',
    'feature_groups'=>'گروه های ویژگی ها',

    'title'=>'عنوان',
    'default'=>'پیش فرض',
    'instance'=>'سامانه',
    'position'=>'جایگاه',
    'language'=>'زبان',


];