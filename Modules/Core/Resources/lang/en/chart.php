<?php

return [

    'action_filter'=>'اعمال فیلتر',
    'show_filter'=>'نمایش فیلتر',
    'cancel'=>'انصراف',
    'submit'=>'ثبت',
    'yes'=>'بله',
    'no'=>'خیر',
    'full_date'=>'روزانه',
    'by_month'=>'ماهانه',
    'by_hours'=>'ساعتی',
    'title_from'=>'تاریخ شروع',
    'title_to'=>'تاریخ پایان',
    'chart_type_text'=>'نوع چارت',
    'filter_group_by_models'=>'گروه بندی',
    'filter_by_models'=>'فیلتر مدل ها',
    'confirm_text'=>'آیا اطمینان دارید؟',
    'chart_types'=>[
        'line'=>'خطی',
        'pie'=>'دایره ای',
        'radar'=>'رادار',
        'bar'=>'میله ای',
    ]
];