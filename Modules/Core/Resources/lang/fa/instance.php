<?php

return [

    'change_instance_list'=>'تغییر سامانه سیستم',
    'instance_list'=>'فهرست سامانه های سیستم',
    'add_instance'=>'افزودن سامانه جدید',
    'edit_instance'=>'ویرایش سامانه',
    'delete_instance'=>'حذف سامانه',
    'confirm_delete'=>'آیا از حذف اطمینان دارید؟',
    'name'=>'عنوان',
    'app_name'=>'عنوان اپ',
    'description'=>'توضیحات',
    'active'=>'وضعیت',
    'enable'=>'فعال',
    'disable'=>'غیرفعال',
    'front_theme'=>'پوسته ی سمت کاربری',
    'backend_theme'=>'پوسته ی سمت مدیریت',
    'default'=>'پیش فرض',
    'currency'=>'نوع پول',
    'select_language'=>'انتخاب زبان',
    'select_currency'=>'انتخاب پول',
    'select_frontend_template'=>'انتخاب تم کاربری',
    'select_backend_template'=>'انتخاب تم ادمین',
    'cancel'=>'انصراف',
    'submit'=>'ثبت',
    'special_blocks_header'=>'بلاک های هدر',
    'special_blocks_footer'=>'بلاک های فوتر',

];