<?php
return [
    'fields' => [
        'id' => 'شناسه',
        'code'=>'کد دوره',
        'html' => ' متن بصورت Html',
        'title' => 'عنوان',
        'prefix_title' => 'پیشوند عنوان',
        'postfix_title' => 'پسوند عنوان',
        'description' => 'توضیحات',
        'is_mobile_box' => 'در موبایل',
        'keywords' => 'کلمات کلیدی',
        'favicon' => 'favicon',
        'logo' => 'لوگو',
        'logo_small' => 'لوگو کوچک',
        'logo_large' => 'لوگو بزرگ',
        'telegram' => 'تلگرام',
        'instagram' => 'اینستاگرام',
        'linkedin' => 'لینکدین',
        'google_plus' => 'گوگل پلاس',
        'aparat' => 'آپارات',
        'twitter' => 'توئیتر',
        'facebook' => 'فیسبوک',
        'soroush' => 'سروش',
        'rubika' => 'روبیکا',
        'active' => 'فعال',
        'name' => 'نام',
        'size' => 'اندازه',
        'caption' => 'عنوان',
        'homeLink' => 'لینک به صفحه اصلی',
        'before_html' => 'html شروع',
        'after_html' => 'پایان Html',
        'is_api' => 'در وب سرویس فعال است',
        'is_api_statuses' => [
            'yes' => 'بله',
            'no' => 'خیر',
        ],
        'open' => 'شروع',
        'close' => 'پایان',
        'save' => 'ذخیره داده ها',
        'css' => 'بلوک CSS',
        'javascript' => 'بلوک JavaScript',
        'slag' => 'آدرس صفحه',
        'column' => 'ستون بندی صفحه',
        'status' => 'وضعیت',
        'content' => 'محتوا',
        'date' => 'تاریخ و ساعت',
        'instance' => 'سامانه',
        'language' => 'زبان',
        'route_name' => 'نام آدرس',
        'register_hide' => 'غیر فعال کردن ثبت نام',
        'login_hide' => 'غیر فعال کردن صفحه ورود',
        'menu' => 'منو',
        'show_title' => 'نمایش عنوان',
        'mobile_menu' => 'منوی موبایلی',
        'class_div' => 'کلاس پدر',
        'class_ul' => 'کلاس UL',
        'class_li' => 'کلاس LI',
        'class_link' => 'کلاس لینک',
        'menu_deep' => 'عمق منو',
        'files_html' => 'فایل های مورد نیاز js , css',
        'input_name' => '',
        'logo_other' => 'لوگوی دیگر',
        'select_file' => 'انتخاب فایل',
        'preview' => 'نمایش نمایش',
        'class_sub_div' => 'زیر کلاس',
        'class_image' => 'کلاس تصویر',
        'socials_show' => 'نمایش لینک :name',
        'show' => 'نمایش  :name',
        'image_url' => 'آدرس تصویر',
        'links' => 'لینک ها',
        'link' => 'لینک',
        'image' => 'تصویر',
        'phone' => 'شماره تماس',
        'email' => 'ایمیل',
        'address' => 'آدرس',
        'start' => 'شروع',
        'end' => 'پایان',

        'counter_type' => 'نوع شمارنده',
        'counter_model' => 'مدل',
        'icon' => 'آیکون',
        'color' => 'رنگ',
        'counter_model_scope' => 'فیلتر مدل',

        'form_builder' => [
            'select_form' => 'انتخاب فرم',
            'form_action' => 'آدرس اکشن فرم',
            'form_method' => 'متد فرم',
            'form_methods' => ["post" => "POST", "get" => "GET"],
            'configs' => 'تنظیمات',
        ],
        'slider_type'=>'نوع اسلایدر',
        'slider_autoplay'=>'حرکت اتوماتیک',
        'slider_time'=>'زمان هر اسلایدر به میلی ثانیه',
        'slider_link_type'=>'نوع لینک',
    ],
    'helper' => [
        'icon' => 'آیکون',
        'color' => 'رنگ',
        'id' => '',
        'html' => '',
        'title' => '',
        'prefix_title' => '',
        'postfix_title' => '',
        'description' => '',
        'keywords' => '',
        'favicon' => '',
        'logo' => '',
        'logo_small' => '',
        'logo_large' => '',
        'telegram' => '',
        'instagram' => '',
        'linkedin' => '',
        'google_plus' => '',
        'aparat' => '',
        'twitter' => '',
        'facebook' => '',
        'soroush' => '',
        'rubika' => '',
        'active' => 'این مورد فعال باشد',
        'name' => 'یک نام انتخاب کنید',
        'size' => '',
        'homeLink' => '',
        'before_html' => '',
        'after_html' => '',
        'save' => '',
        'css' => '',
        'javascript' => '',
        'slag' => 'آدرس صفحه',
        'column' => 'ستون بندی صفحه',
        'status' => 'وضعیت',
        'content' => 'محتوا',
        'date' => 'تاریخ و ساعت',
        'instance' => 'سامانه',
        'route_name' => 'مشخصه روت مورد استفاده',
        'register_hide' => '',
        'login_hide' => '',
        'menu' => '',
        'show_title' => '',
        'mobile_menu' => '',
        'class_div' => '',
        'class_ul' => '',
        'class_li' => '',
        'class_link' => '',
        'menu_deep' => 'عمق منو',
        'files_html' => 'هر آدرس را با , جدا نمایید.',
        'input_name' => 'نام ورودی',
        'logo_other' => '',
        'search' => 'جستجو در دوره های آموزشی ...',
        'placeholder_search' => 'جستجو ...',
        'select_file' => '',
        'preview' => '',
        'class_sub_div' => '',
        'class_image' => '',
        'socials_show' => '',
        'image_url' => '',
        'show' => 'نمایش  :name',
        'link' => '',
        'links' => '',
        'image' => '',
        'phone' => '',
        'email' => '',
        'address' => '',
        'start' => '',
        'end' => '',
        'slider_type'=>'نوع اسلایدر',
        'slider_autoplay'=>'حرکت اتوماتیک',
        'slider_time'=>'زمان هر اسلایدر به میلی ثانیه',
        'slider_link_type'=>'',
    ],
    'titles' => [
        'info' => 'اطلاعات',
        'images' => 'تصاویر',
        'socials' => 'شبکه های اجتماعی',
        'login' => 'ورود به سایت',
        'logout' => 'خروج',
        'special_blocks' => 'بلاک های اختصاصی :name',
        'special_blocks_header' => 'بلاک های اختصاصی هدر :name',
        'special_blocks_footer' => 'بلاک های اختصاصی فوتر :name',
        'create_page' => 'ایجاد صفحه',
        'edit_page' => 'ویرایش صفحه',
        'edit_route_block' => 'ویرایش بلاک های صفحه',
        'route_block' => ' بلاک های صفحه',
        'create_route_block' => 'ایجاد بلاک های صفحه',
        'users' => 'کاربران',
        'show_url_page'=>'نمایش آدرس صفحه',
        'dashboard'=>'میز کار',
        'pages'=>'مدیریت صفحات',
        'index'=>'صفحه اصلی',
    ],
];