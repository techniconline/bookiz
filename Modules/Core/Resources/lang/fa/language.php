<?php

return [

    'language_list'=>'فهرست زبانهای سیستم',
    'add_language'=>'افزودن زبان جدید',
    'edit_language'=>'ویرایش زبان',
    'delete_language'=>'حذف زبان',
    'name'=>'عنوان',
    'active'=>'وضعیت',
    'activeItems'=>[0=>'غیرفعال', 1=>'فعال'],
    'enable'=>'فعال',
    'disable'=>'غیرفعال',
    'code'=>'کد زبان',
    'direction'=>'جهت نوشتار',
    'confirm_delete'=>'آیا از حذف اطمینان دارید؟',
    'directions'=>['RTL'=>'راست به چپ', 'LTR'=>'چپ به راست'],

    'select_language'=>'انتخاب زبان',

    'cancel'=>'انصراف',
    'submit'=>'ثبت',

];