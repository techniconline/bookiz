<?php

return [

    'template_list'=>'فهرست پوسته های سیستم',
    'add_template'=>'افزودن پوسته جدید',
    'edit_template'=>'ویرایش پوسته',
    'delete_template'=>'حذف پوسته',
    'confirm_delete'=>'آیا از حذف اطمینان دارید؟',
    'name'=>'عنوان',
    'code'=>'کد',
    'active'=>'وضعیت',
    'backend'=>'بخش ادمین',
    'renderer'=>'تابع رندر کننده',
    'enable'=>'فعال',
    'disable'=>'غیرفعال',
    'backend_theme'=>'پوسته ی سمت مدیریت',
    'default'=>'پیش فرض',

    'cancel'=>'انصراف',
    'submit'=>'ثبت',

];