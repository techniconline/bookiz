<div class="col-md-12" id="core.instance.config">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cog"></i>
                {{ $BlockViewModel->getTitlePage() }}
            </div>
        </div>
        <div class="portlet-body form">
            <div class="clearfix"></div>

            <div class="clearfix"></div>

            {!! FormHelper::open(['role'=>'form','url'=>route('core.config.instance.save'),'method'=>'POST','class'=>'form-horizontal','id'=>'config_form','enctype'=>"multipart/form-data"]) !!}
            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp
            {!! FormHelper::legend(trans('core::form.titles.info')) !!}
            {!! FormHelper::input('text','title',old('title',$BlockViewModel->getSetting('title')),['required'=>'required','title'=>trans('core::form.fields.title'),'helper'=>trans('core::form.helper.title')]) !!}
            {!! FormHelper::input('text','prefix_title',old('prefix_title',$BlockViewModel->getSetting('prefix_title')),['title'=>trans('core::form.fields.prefix_title'),'helper'=>trans('core::form.helper.prefix_title')]) !!}
            {!! FormHelper::input('text','postfix_title',old('postfix_title',$BlockViewModel->getSetting('postfix_title')),['title'=>trans('core::form.fields.postfix_title'),'helper'=>trans('core::form.helper.postfix_title')]) !!}
            {!! FormHelper::textarea('description',old('description',$BlockViewModel->getSetting('description')),['title'=>trans('core::form.fields.description'),'helper'=>trans('core::form.helper.description')]) !!}
            {!! FormHelper::selectTag('keywords[]',[],old('connector',$BlockViewModel->getSetting('keywords')),['title'=>trans('core::form.fields.keywords'),'helper'=>trans('core::form.helper.keywords'),'multiple'=>'multiple','data-tag'=>'true']) !!}
            {!! FormHelper::input('text','email',old('email',$BlockViewModel->getSetting('email')),['title'=>trans('core::form.fields.email'),'helper'=>trans('core::form.helper.email')]) !!}
            {!! FormHelper::textarea('phone',old('phone',$BlockViewModel->getSetting('phone')),['title'=>trans('core::form.fields.phone'),'helper'=>trans('core::form.helper.phone')]) !!}
            {!! FormHelper::textarea('address',old('address',$BlockViewModel->getSetting('address')),['title'=>trans('core::form.fields.address'),'helper'=>trans('core::form.helper.address')]) !!}

            {!! FormHelper::legend(trans('core::form.titles.images')) !!}
            {!! FormHelper::avatar('favicon',$BlockViewModel->getSetting('favicon'),['title'=>trans('core::form.fields.favicon'),'helper'=>trans('core::form.helper.favicon')]) !!}
            {!! FormHelper::avatar('logo',$BlockViewModel->getSetting('logo'),['title'=>trans('core::form.fields.logo'),'helper'=>trans('core::form.helper.logo')]) !!}
            {!! FormHelper::avatar('logo_small',$BlockViewModel->getSetting('logo_small'),['title'=>trans('core::form.fields.logo_small'),'helper'=>trans('core::form.helper.logo_small')]) !!}
            {!! FormHelper::avatar('logo_large',$BlockViewModel->getSetting('logo_large'),['title'=>trans('core::form.fields.logo_large'),'helper'=>trans('core::form.helper.logo_large')]) !!}
            {!! FormHelper::avatar('logo_other',$BlockViewModel->getSetting('logo_other'),['title'=>trans('core::form.fields.logo_other'),'helper'=>trans('core::form.helper.logo_other')]) !!}
            {!! FormHelper::legend(trans('core::form.titles.socials')) !!}
            @foreach(trans('core::data.socials') as $name=>$trans)
                {!! FormHelper::input('text',$name,old($name,$BlockViewModel->getSetting($name)),['title'=>$trans]) !!}
            @endforeach
            {!! FormHelper::legend(trans('core::form.titles.routes')) !!}
            {!! FormHelper::select('home',$BlockViewModel->getRoutes(),old('home',$BlockViewModel->getSetting('home')),['title'=>trans('core::form.fields.home'),'helper'=>trans('core::form.helper.home'),'placeholder'=>'']) !!}
            {!! FormHelper::legend(trans('core::form.titles.users')) !!}
            {!! FormHelper::checkbox('register_hide',1,old('register_hide',$BlockViewModel->getSetting('register_hide')),['title'=>trans('core::form.fields.register_hide'),'label'=>trans('core::form.helper.register_hide')]) !!}
            {!! FormHelper::checkbox('login_hide',1,old('login_hide',$BlockViewModel->getSetting('login_hide')),['title'=>trans('core::form.fields.login_hide'),'label'=>trans('core::form.helper.login_hide')]) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitOnly() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>