<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="http://ietrat.local/css/app.css" rel="stylesheet">
        <link href="http://ietrat.local/css/bootstrap-rtl.min.css" rel="stylesheet">
        <link href="http://ietrat.local/css/font-awesome.min.css" rel="stylesheet">
        <link href="http://ietrat.local/css/custom.css?time=123" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="http://ietrat.local/css/q-theme/bootstrap.min.css" rel="stylesheet">
        <!-- Bootstrap RTL Theme -->
        <link href="http://ietrat.local/css/q-theme/bootstrap-rtl.css" rel="stylesheet">
        <!-- our CSS  -->
        <link href="http://ietrat.local/css/q-theme/main.css" rel="stylesheet">
        <link href="http://ietrat.local/css/bz-custom.css" rel="stylesheet">
        <script src="http://ietrat.local/js/app.js"></script>

        <script src="http://ietrat.local/js/bootstrap_validator/validator.min.js"></script>
        <title>Module Core</title>
    </head>
    <body>
        @yield('content')
    </body>
</html>
