<div class="row" id="form_template">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("core::template.".(isset($template) && $template?'edit_template':'add_template'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>(isset($template) && $template? route('core.template.update', ['template_id'=>$template->id]) : route('core.template.save'))
                        ,'method'=>(isset($template) && $template?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                {!! FormHelper::input('text','name',old('name',$template?$template->name:null)
                ,['required'=>'required','title'=>trans("core::template.name"),'helper'=>trans("core::template.name")]) !!}

                {!! FormHelper::input('text','code',old('code',$template?$template->code:null)
                ,['required'=>'required','title'=>trans("core::template.code"),'helper'=>trans("core::template.code")]) !!}

                {!! FormHelper::input('text','renderer',old('renderer',$template?$template->renderer:null)
                ,['title'=>trans("core::template.renderer"),'helper'=>trans("core::template.renderer")]) !!}

                {!! FormHelper::checkbox('default',1,old('default',$template?$template->default:null)
                ,['title'=>trans("core::template.default"),'helper'=>trans("core::template.default")]) !!}

                {!! FormHelper::checkbox('backend',1,old('backend',$template?$template->backend:null)
                ,['title'=>trans("core::template.backend"),'helper'=>trans("core::template.backend")]) !!}

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("core::template.submit")], ['title'=>trans("core::template.cancel") , 'url'=>route('core.template.index')]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>
        </div>
    </div>
</div>