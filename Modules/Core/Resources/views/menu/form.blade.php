<div class="row" id="form_menu">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("core::menu.add_menu")
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">

                {!! FormHelper::open(['role'=>'form','url'=> route('core.menu.save')
                        ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="parent_id" value="0" class="_parent_id">
                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="col-md-6">
                {!! FormHelper::input('text','title',old('title',null)
                ,['required'=>'required','title'=>trans("core::menu.title"),'helper'=>trans("core::menu.title")]) !!}
                </div>
                <div class="col-md-6">
                {!! FormHelper::input('text','alias',old('alias',null)
                ,['required'=>'required','title'=>trans("core::menu.alias"),'helper'=>trans("core::menu.alias")]) !!}
                </div>
                <div class="col-md-6">
                {!! FormHelper::select('type',trans("core::menu.types"),old('type',null)
                    ,['title'=>trans("core::menu.type"),'helper'=>trans("core::menu.type"), 'class'=>'_url_type'
                    ,'placeholder'=>trans("core::menu.type")]) !!}
                </div>

                <div class="col-md-6 _route_list" style="display: none">
                {!! FormHelper::select('route_name',isset($route_list)?$route_list:[],old('route_name',null)
                    ,['title'=>trans("core::menu.route_name"),'helper'=>trans("core::menu.route_name"),
                    'placeholder'=>trans("core::menu.route_name")]) !!}
                </div>

                <div class="col-md-6 _url">
                    {!! FormHelper::input('text','url',old('url',null)
                    ,['title'=>trans("core::menu.url"),'helper'=>trans("core::menu.url")]) !!}
                </div>

                <div class="col-md-6 _language_list">
                    {!! FormHelper::select('language_id',$languages,old('language_id',null)
                    ,['title'=>trans("core::menu.select_language"),'helper'=>trans("core::menu.select_language"),
                    'placeholder'=>trans("core::menu.select_language")]) !!}
                </div>

                <div class="col-md-6 _instance_list">
                    {!! FormHelper::select('instance_id',$instances,old('instance_id',null)
                    ,['title'=>trans("core::menu.instance"),'helper'=>trans("core::menu.instance"),
                    'placeholder'=>trans("core::menu.instance")]) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::input('text','sort',old('sort',1)
                    ,['title'=>trans("core::menu.sort"),'helper'=>trans("core::menu.sort")]) !!}
                </div>
                <div class="col-md-6">
                    {!! FormHelper::select('active',trans("core::menu.statuses"),old('active',1)
                    ,['title'=>trans("core::menu.status"),'helper'=>trans("core::menu.select_status")
                    ,'placeholder'=>trans("core::menu.select_status")]) !!}

                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("core::menu.submit")], ['title'=>trans("core::menu.cancel"), 'url'=>route('core.menu.index')], ['class'=>'btn form-control input-small _language_list']) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}


                {{--<form role="form" action="{!! route('core.menu.save') !!}" method="post">--}}
                    {{--{!! csrf_field() !!}--}}
                    {{--<input type="hidden" name="_method" value="POST">--}}
                    {{--<input type="hidden" name="parent_id" value="0" class="_parent_id">--}}
                    {{--<div class="form-body">--}}
                        {{--<div class="form-group">--}}

                            {{--<div class="input-icon right  margin-top-10">--}}
                                {{--<i class="fa fa-check"></i>--}}
                                {{--<input name="title" type="text" class="form-control"--}}
                                       {{--placeholder="@lang("core::menu.title")">--}}
                            {{--</div>--}}


                            {{--<div class="row">--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="input-icon right  margin-top-10">--}}
                                        {{--<i class="fa fa-check"></i>--}}
                                        {{--<input name="alias" type="text" class="form-control"--}}
                                               {{--placeholder="@lang("core::menu.alias")">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6 _language_list">--}}
                                    {{--<div class="input-icon right  margin-top-10">--}}
                                        {{--<select name="language_id" class="form-control ">--}}
                                            {{--<option> ... @lang("core::menu.select_language") ...</option>--}}
                                            {{--@if(isset($languages) && $languages)--}}
                                                {{--@foreach($languages as $language)--}}
                                                    {{--<option value="{!! $language->id !!}">{!! $language->name !!}</option>--}}
                                                {{--@endforeach--}}
                                            {{--@endif--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="row">--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="input-icon right  margin-top-10">--}}
                                        {{--<select name="type" class="form-control _url_type">--}}
                                            {{--<option> ... @lang("core::menu.type") ...</option>--}}
                                            {{--<option value="url">@lang("core::menu.types.url")</option>--}}
                                            {{--<option value="external_url">@lang("core::menu.types.external_url")</option>--}}
                                            {{--<option value="route">@lang("core::menu.types.route")</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-6 _route_list" style="display: none">--}}
                                    {{--<div class="input-icon right  margin-top-10">--}}
                                        {{--<select name="route_name" class="form-control ">--}}
                                            {{--<option> ... @lang("core::menu.route_name") ...</option>--}}
                                            {{--@if(isset($route_list) && $route_list)--}}
                                                {{--@foreach($route_list as $route)--}}
                                                    {{--@if( !in_array("GET", $route->methods) || !$route->getName() || $route->parameters )--}}
                                                        {{--@continue;--}}
                                                    {{--@endif--}}
                                                    {{--<option value="{!! $route->getName() !!}">{!! $route->getName() . ' -- ' . $route->getActionName() !!}</option>--}}
                                                {{--@endforeach--}}
                                            {{--@endif--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-6 _url">--}}

                                    {{--<div class="input-icon right  margin-top-10">--}}
                                        {{--<i class="fa fa-link"></i>--}}
                                        {{--<input name="url" type="text" class="form-control"--}}
                                               {{--placeholder="@lang("core::menu.url")">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="row">--}}
                                {{--<div class="col-md-4 _instance_list">--}}

                                    {{--<div class="input-icon right  margin-top-10">--}}
                                        {{--<select name="instance_id" class="form-control ">--}}
                                            {{--<option> ... @lang("core::menu.instance") ...</option>--}}
                                            {{--@if(isset($instances) && $instances)--}}
                                                {{--@foreach($instances as $instance)--}}
                                                    {{--<option value="{!! $instance->id !!}">{!! $instance->name !!}</option>--}}
                                                {{--@endforeach--}}
                                            {{--@endif--}}
                                        {{--</select>--}}
                                    {{--</div>--}}

                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}

                                    {{--<div class="input-icon right  margin-top-10">--}}
                                        {{--<i class="fa fa-sort-alpha-asc"></i>--}}
                                        {{--<input name="sort" type="number" min="0" max="999" value="0"--}}
                                               {{--class="form-control" placeholder="@lang("core::menu.sort")">--}}
                                    {{--</div>--}}

                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}

                                    {{--<div class="input-icon right margin-top-10">--}}
                                        {{--<i class="fa fa-language"></i>--}}
                                        {{--<input name="key_trans" type="text" class="form-control"--}}
                                               {{--placeholder="@lang("core::menu.key_trans")">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-actions">--}}
                        {{--<a href="{!! route('core.menu.index') !!}"--}}
                           {{--class="btn default _cancel">@lang("core::menu.cancel")</a>--}}
                        {{--<button type="submit" class="btn green _save">@lang("core::menu.submit")</button>--}}
                    {{--</div>--}}
                {{--</form>--}}
            </div>
        </div>
    </div>
</div>