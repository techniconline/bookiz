<div class="row">
    <div class="col-md-12 padding-tb-10">
        <a href="{!! route('core.menu.index') !!}"
           class="btn btn-lg blue circle-right">
            <i class="fa fa-list"></i>
            @lang("core::menu.menus_root_list")
        </a>
    </div>
</div>
<div style="display: none">
    @include("core::menu.form")
</div>

{!!  \Modules\Core\Providers\Helpers\Menu\MenuHelper::createTreeByNestable($menus)  !!}

