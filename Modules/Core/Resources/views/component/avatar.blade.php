<div class="avatar {!! isset($class)?$class:'' !!} ">
    <img class="img-responsive img-circle {!! isset($img_class)?$img_class:'' !!}" src="{!! $avatar !!}" />
</div>
