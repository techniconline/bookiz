@if (isset($success))
    <div class="alert alert-success">
        <strong>{{trans('error.success')}}: </strong> {{ $success}}
    </div>
@endif
@if (isset($error))
    <div class="alert alert-danger">
        <strong>{{trans('error.error')}}: </strong> {{ $error }}
    </div>
@endif
@if (isset($info))
    <div class="alert alert-info">
        <strong>{{trans('error.info')}}: </strong> {{ $info }}
    </div>
@endif
@if (isset($warning))
    <div class="alert alert-warning">
        <strong>توجه: </strong> {{$warning }}
    </div>
@endif