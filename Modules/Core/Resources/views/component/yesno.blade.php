@php
$yes=isset($yes)?$yes:trans('core::data.yes');
$no=isset($no)?$no:trans('core::data.no');
@endphp
<span class="{!! $value?'text-success':'text-danger' !!} "> {{ $value?$yes:$no }}</span>