<div class="row errors-box">
    <div class="col-md-12">
        <div class="row">
            @if (session('success') || isset($success))
                <div class="alert alert-success">
                    <strong>{{trans('error.success')}}: </strong> {{ session('success')? session('success'): $success}}
                    <a href="javascript:void(0);" title="بستن" class="close "><i class="fa fa-times"></i></a>
                </div>
            @endif
            @if (session('error') || isset($error))
                <div class="alert alert-danger">
                    <strong>{{trans('error.error')}}: </strong> {{ session('error')?session('error'):$error }}
                    <a href="javascript:void(0);" title="بستن" class="close "><i class="fa fa-times"></i></a>
                </div>
            @endif
            @if (session('info') || isset($info))
                <div class="alert alert-info">
                    <strong>{{trans('error.info')}}: </strong> {{ session('info')?session('info'):$info }}
                    <a href="javascript:void(0);" title="بستن" class="close " ><i class="fa fa-times"></i></a>
                </div>
            @endif
            @if (session('warning') || isset($warning))
                <div class="alert alert-warning">
                    <strong>توجه: </strong> {{ session('warning')?session('warning'):$warning }}
                    <a href="javascript:void(0);" title="بستن" class="close " ><i class="fa fa-times"></i></a>
                </div>
            @endif
            @if (isset($action_links) && is_array($action_links))
                <div class="col-md-12 buttons">
                    @foreach ($action_links as $button)
                        <a  href="{{ $button->href }}" {!! $button->attributes !!}  title="{{$button->trans}}">
                            {!! $button->icon ? '<i class="'.$button->icon.'"></i>':'' !!} {{ $button->trans ? $button->trans:'' }}
                        </a>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            $(document).on('click','a.close' ,function (e) {
                e.preventDefault();
                $(this).parent().remove();
                return false;
            });
        });
    </script>
</div>
