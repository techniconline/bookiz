@php
$show_key=isset($show_key)?$show_key:false;
$clasess=['default','success','danger','info','warning','primary'];
@endphp
@foreach($data as $key=>$value)
    @php $class=isset($class)?$class:$clasess[rand(0,5)]; @endphp
    <span class="label label-{!! $class !!}">{!! $show_key?$key.': ':'' !!}<b>{{ $value }}</b></span>
@endforeach