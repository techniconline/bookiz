<?php echo '<?xml version="1.0" encoding="UTF-8"?>'."\n"; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($sitemapModel->getSiteMapModel() as $col)
    <url>
        <loc>{{$sitemapModel->getLink(empty($sitemapModel->typeUrl) ? null : $col->{$sitemapModel->typeUrl})}}</loc>
        <lastmod>{{ $col->created_at}}</lastmod>
        <changefreq>{{$sitemapModel->changefreq}}</changefreq>
        <priority>{{$sitemapModel->priority}}</priority>
    </url>
    @endforeach
</urlset>
