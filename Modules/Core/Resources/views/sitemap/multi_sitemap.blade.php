<?php echo '<?xml version="1.0" encoding="UTF-8"?>'."\n"; ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($sitemapModel->data as $data)
    <sitemap>
        <loc>{{url('sitemap/'.$data['sitemap_name'])}}</loc>
        <lastmod>{{$data['last_updated_at']}}</lastmod>
    </sitemap>
    @endforeach
</sitemapindex>