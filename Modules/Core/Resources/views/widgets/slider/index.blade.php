{!! $blockViewModel->getConfig('before_html') !!}
 @if($blockViewModel->getConfig('is_mobile_box'))
    <div class="container   d-xl-none d-md-none  d-block d-sm-block">
		 
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        @for($i=1;$i<=6;$i++)
                            @if($blockViewModel->getConfig('image_url_'.$i))
                                <li data-target="#myCarousel" data-slide-to="{{ $i-1 }}" class="{{ ($i==1)?'active':'' }}"></li>
                            @endif
                        @endfor
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        @for($i=1;$i<=6;$i++)
                            @if($blockViewModel->getConfig('image_url_'.$i))
                                <div class="carousel-item {{ ($i==1)?'active':'' }}">
                                    <a href="{!! ($blockViewModel->getConfig('link_url_'.$i))?$blockViewModel->getConfig('link_url_'.$i):'#' !!}">
                                        <img src="{{ $blockViewModel->getConfig('image_url_'.$i) }}" />
                                    </a>
                                </div>
                            @endif
                        @endfor
                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                        <span class="fas fa-chevron-left mrg_top50precent slider_sircel">  </span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                        <span class="fas fa-chevron-right mrg_top50precent slider_sircel">  </span>
                        <span class="sr-only">Next</span>
                    </a>


                </div>
           
      
    </div>
	
	@else
		 <div class="container d-xl-block">
        
		
		 
                <div id="myCarousel2" style="margin-top:10px" class="carousel slide" data-ride="carousel2">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        @for($i=1;$i<=6;$i++)
                            @if($blockViewModel->getConfig('image_url_'.$i))
                                <li data-target="#myCarousel2" data-slide-to="{{ $i-1 }}" class="{{ ($i==1)?'active':'' }}"></li>
                            @endif
                        @endfor
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        @for($i=1;$i<=6;$i++)
                            @if($blockViewModel->getConfig('image_url_'.$i))
                                <div class="carousel-item {{ ($i==1)?'active':'' }}">
                                    <a href="{!! ($blockViewModel->getConfig('link_url_'.$i))?$blockViewModel->getConfig('link_url_'.$i):'#' !!}">
                                        <img src="{{ $blockViewModel->getConfig('image_url_'.$i) }}" />
                                    </a>
                                </div>
                            @endif
                        @endfor
                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#myCarousel2" data-slide="prev">
                        <span class="fas fa-chevron-left mrg_top50precent slider_sircel">  </span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#myCarousel2" data-slide="next">
                        <span class="fas fa-chevron-right mrg_top50precent slider_sircel">  </span>
                        <span class="sr-only">Next</span>
                    </a>


                </div>
           
      
    </div>
 
@endif
 
{!! $blockViewModel->getConfig('after_html') !!}