@for($i=1;$i<=6;$i++)
    {!! FormHelper::select('link_type_'.$i,trans('core::data.slider_link_type'),old('link_type_'.$i,$blockViewModel->getData('link_type_'.$i,'internal')),['title'=>trans('core::form.fields.slider_link_type'),'helper'=>trans('core::form.helper.slider_link_type')]) !!}
    {!! FormHelper::input('text','link_url_'.$i,old('link_url_'.$i,$blockViewModel->getData('link_url_'.$i)),['title'=>trans('core::form.fields.link'),'helper'=>trans('core::form.helper.link')]) !!}
    {!! FormHelper::inputByButton('image_url_'.$i,old('image_url_'.$i,$blockViewModel->getData('image_url_'.$i)),['type'=>'image','title'=>trans('core::form.fields.image_url'),'helper'=>trans('core::form.helper.image_url')],trans('core::form.fields.select_file'),[],true) !!}
    <hr>
@endfor