<div class="top-menu">
    <ul class="nav navbar-nav pull-right">
        <!-- BEGIN USER LOGIN DROPDOWN -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        <li class="dropdown dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
               data-close-others="true">
                <img alt="{{ $user->full_name }}" class="img-circle" src="{!! $user->small_avatar_url !!}"/>
                <span class="username username-hide-on-mobile"> {{ $user->full_name }} </span>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
                <li>
                    <a href="{!! route("user.profile.edit") !!}"><i class="fa fa-user-edit"></i>@lang("user::form.titles.profile")</a>
                </li>
                <li>
                    <a href="{!! route("user.profile.password.form") !!}"><i class="fa fa-lock"></i>@lang("user::form.titles.change_password")</a>
                </li>
                <li>
                    <a href="{!! route('logout') !!}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="icon-logout"></i> {{ trans('core::form.titles.logout') }}</a>
                </li>

            </ul>
        </li>
        <!-- END USER LOGIN DROPDOWN -->
        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        <li class="dropdown dropdown-quick-sidebar-toggler">
            <a href="{!! route('logout') !!}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
               class="dropdown-toggle">
                <i class="icon-logout"></i>
            </a>
        </li>
        <li>
            <form id="logout-form" action="{!! route('logout') !!}" method="POST"
                  style="display: none;">
                {!! Form::token() !!}
            </form>
        </li>
        <!-- END QUICK SIDEBAR TOGGLER -->
    </ul>
</div>
