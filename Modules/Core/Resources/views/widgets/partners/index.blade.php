{!! $blockViewModel->getConfig('before_html') !!}


<div class="container ">
    <div class="_article FullWidth">
        <div class="col-xl-12 col-lg-12 text-align-center  ">
            <div class="container-fluid  ">
                <div class="container ">
                    <div class="owl-Four owl-carousel    owl-theme ">
                        @for($i=1;$i<=12;$i++)
                            @if($blockViewModel->getConfig('image_url_'.$i))
                                <div>
                                    <a target="_blank"
                                       href="{!! ($blockViewModel->getConfig('link_url_'.$i))?$blockViewModel->getConfig('link_url_'.$i):'#' !!}">
                                        <img src="{{ $blockViewModel->getConfig('image_url_'.$i) }}" class="bnr1"/></a>
                                </div>
                            @endif
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{!! $blockViewModel->getConfig('after_html') !!}