@for($i=1;$i<=12;$i++)
{!! FormHelper::input('text','name_'.$i,old('name_'.$i,$blockViewModel->getData('name_'.$i)),['title'=>trans('core::form.fields.name'),'helper'=>trans('core::form.helper.name')]) !!}
{!! FormHelper::input('text','link_url_'.$i,old('link_url_'.$i,$blockViewModel->getData('link_url_'.$i)),['title'=>trans('core::form.fields.link'),'helper'=>trans('core::form.helper.link')]) !!}
{!! FormHelper::inputByButton('image_url_'.$i,old('image_url_'.$i,$blockViewModel->getData('image_url_'.$i)),['type'=>'image','title'=>trans('core::form.fields.image_url'),'helper'=>trans('core::form.helper.image_url')],trans('core::form.fields.select_file'),[],true) !!}
@endfor