{!! $blockViewModel->getConfig('after_html') !!}
@if($blockViewModel->getConfig('is_mobile_box'))
 <div class="d-md-none d-lg-none">
   <div class="{{ $blockViewModel->getConfig('class_div')  }} small_search ">
    <form class="{{ $blockViewModel->getConfig('class_form') }}" method="get"
          action="{{ \Illuminate\Support\Facades\Route::has($blockViewModel->getConfig('route_name'))?route($blockViewModel->getConfig('route_name')):null }}">
        <input type="text" name="{{ $blockViewModel->getConfig('input_name') }}"
		class="s_box"
		placeholder="@lang('core::form.helper.search')" />
        <button type="submit" class="s_btn">
            <i class="fas fa-search"></i>
        </button>
    </form>
	  </div>

 </div>



 @else
 <div class="{{ $blockViewModel->getConfig('class_div')  }} d-none d-md-block">
    <form class="{{ $blockViewModel->getConfig('class_form') }}" method="get"
          action="{{ \Illuminate\Support\Facades\Route::has($blockViewModel->getConfig('route_name'))?route($blockViewModel->getConfig('route_name')):null }}">
        <input type="text" name="{{ $blockViewModel->getConfig('input_name') }}"
		class="s_box"
		placeholder="@lang('core::form.helper.search')" />
        <button type="submit" class="s_btn">
            <i class="fas fa-search"></i>
        </button>
    </form>

</div>
 @endif


    
 

{!! $blockViewModel->getConfig('after_html') !!}