{!! FormHelper::select('menu_id',$blockViewModel->getMenuRootList(),old('menu_id',$blockViewModel->getData('menu_id')),['title'=>trans('core::form.fields.menu'),'helper'=>trans('core::form.helper.menu')]) !!}
{!! FormHelper::checkbox('show_title',1,old('show_title',$blockViewModel->getData('show_title')),['title'=>trans('core::form.fields.show_title')]) !!}
{!! FormHelper::checkbox('mobile_menu',1,old('mobile_menu',$blockViewModel->getData('mobile_menu')),['title'=>trans('core::form.fields.mobile_menu')]) !!}
{!! FormHelper::input('number','deep',old('deep',$blockViewModel->getData('deep',1)),['title'=>trans('core::form.fields.menu_deep'),'helper'=>trans('core::form.helper.menu_deep')]) !!}
