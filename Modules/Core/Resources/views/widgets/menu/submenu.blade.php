<li class="{{ $blockViewModel->getConfig($pre.'class_li') }}">
    <a class="{{ $blockViewModel->getConfig($pre.'class_link') }}" href="{{ BridgeHelper::getMenuHelper()->getMenuUrl($menu) }}">
        {{ $menu->title }}
    </a>
    @if($menu->childes->count())
        <ul class="submenu">
            @foreach($menu->childes as $menu)
            @include('core::widgets.menu.submenu',['pre'=>$pre])
            @endforeach
        </ul>
    @endif
</li>