{!! $blockViewModel->getConfig('before_html') !!}

<div class="{{ $blockViewModel->getConfig('class_div',"col-lg-3 col-md-3 col-sm-6 col-xs-12") }}" id="{{ $blockViewModel->getConfig('id') }}">

    <div class="dashboard-stat2 ">
        <div class="display">
            <div class="number">
                <h3 class="font-{!! $blockViewModel->getConfig("color","green") !!}">
                    <span data-counter="counterup" data-value="{!! $blockViewModel->getCounterValue("value_model") !!}">0</span>
                    <small class="font-{!! $blockViewModel->getConfig("color","green") !!}"> {!! $blockViewModel->getCounterValue("unit_value") !!} </small>
                </h3>
                <small>{!! $blockViewModel->getData("alias") !!}</small>
            </div>
            <div class="icon">
                <i class="{!! $blockViewModel->getConfig("icon","icon-pie-chart") !!}"></i>
            </div>
        </div>
        <div class="progress-info">

            <div class="progress">
                <span style="width: {!! $blockViewModel->getCounterValue("percent") !!}%;" class="progress-bar progress-bar-success {!! $blockViewModel->getConfig("color","green-sharp") !!}">
                    <span class="sr-only">{!! $blockViewModel->getCounterValue("percent") !!}% @lang("core::block.dashboard_counters.progress")</span>
                </span>
            </div>
            <div class="status">
                <div class="status-title"> <small>{!! $blockViewModel->getCounterValue("progress_text") !!} </small> </div>
                <div class="status-number"> {!! $blockViewModel->getCounterValue("percent") !!}%</div>

            </div>
        </div>
    </div>

</div>

{!! $blockViewModel->getConfig('after_html') !!}