{!! $blockViewModel->getConfig('before_html') !!}
<div class="{{ $blockViewModel->getConfig('class_div') }}" id="{{ $blockViewModel->getConfig('id') }}">

    @foreach($blockViewModel->getFromMetaGroups() as $formMetaGroup)

        {!!  FormHelper::legend($formMetaGroup->metaGroup->title) !!}

        @if($blockViewModel->getConfig('with_actions',true))
            {!! FormHelper::open(['role'=>'form','url'=>route('core.formBuilder.meta.features.value.save'  , ['form_id'=>$blockViewModel->getFormObject()->id, 'form_meta_group_id'=>$formMetaGroup->id]) ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
        @endif

        @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

        @foreach($formMetaGroup->metaGroup->featureGroups as $featureGroup)

            @foreach($featureGroup->featureGroupRelations as $featureGroupRelation)

                @if($featureGroupRelation->feature)
                    <?php
                    $activeValue = null;
                    $metaFeatureDataList = $blockViewModel->getFeatureData();
                    if (isset($metaFeatureDataList) && $metaFeatureDataList && isset($metaFeatureDataList[$formMetaGroup->id][$featureGroupRelation->feature->id])) {
                        $activeValue = $metaFeatureDataList[$formMetaGroup->id][$featureGroupRelation->feature->id];
                    }
                    ?>
                    {!! $blockViewModel->setShowFeatureGroup(false)->setFeatureGroup($featureGroup)->setFeatureModel($featureGroupRelation->feature)->setActiveValueFeature($activeValue)->getHtml() !!}

                @endif

            @endforeach

        @endforeach

        {!! FormHelper::openAction() !!}

        @if($blockViewModel->getConfig('with_actions',true))
            {!! FormHelper::submitOnly(['title'=>trans("core::formBuilder.submit"),'class'=>'btn btn green _save']) !!}
        @endif

        {!! FormHelper::closeAction() !!}
        {!! FormHelper::close() !!}

    @endforeach

</div>
{!! $blockViewModel->getConfig('after_html') !!}