    {!! $blockViewModel->getConfig('before_html') !!}
    @php $hasLink=$blockViewModel->getConfig('homeLink') @endphp
    @if($hasLink)

    <a href="{{ route('index') }}">
    @endif
        <img src="{{ $blockViewModel->getLogoSrc() }}" alt="logo" class="{!! ($blockViewModel->getConfig('class'))?$blockViewModel->getConfig('class'):'logo-default img-responsive' !!}" />
        @if($hasLink)
            </a>
        @endif
    {!! $blockViewModel->getConfig('after_html') !!}