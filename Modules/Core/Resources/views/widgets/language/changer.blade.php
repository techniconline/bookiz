<div class="col-md-2 margin-top-10">
    <form id="_language_changer_form" method="post" action="{!! route('core.language.changerLanguage') !!}">
        {!! method_field("POST") !!}
        {!! csrf_field() !!}
        <?php
        $current_language_id = app('getInstanceObject')->getLanguageId();
        ?>
        <select name="current_language_id" class="btn form-control input-small _language_changer">
            <option> ... @lang("core::language.select_language") ...</option>
            @if(isset($languages) && $languages)
                @foreach($languages as $language)
                    <option {!! $current_language_id==$language->id?'selected':null !!} value="{!! $language->id !!}">{!! $language->name !!}</option>
                @endforeach
            @endif
        </select>
    </form>

</div>
<script>
    jQuery(document).ready(function () {
        $('body').on("change", "select._language_changer", function (event) {

            var $clicked = $(this);
            $( "form#_language_changer_form").submit();
        })
    });
</script>

