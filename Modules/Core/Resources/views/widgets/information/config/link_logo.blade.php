{!! FormHelper::inputByButton('logo',old('logo',$blockViewModel->getData('logo')),['type'=>'image','title'=>trans('core::form.fields.image_url'),'helper'=>trans('core::form.helper.image_url')],trans('core::form.fields.select_file'),[],true) !!}

@for($i=1;$i<=3;$i++)
    {!! FormHelper::input('text','link_title_'.$i,old('link_title_'.$i,$blockViewModel->getData('link_title_'.$i)),['title'=>trans('core::form.fields.title').$i,'helper'=>trans('core::form.helper.title')]) !!}
    {!! FormHelper::input('url','link_url_'.$i,old('link_url_'.$i,$blockViewModel->getData('link_url_'.$i)),['title'=>trans('core::form.fields.link').$i,'helper'=>trans('core::form.helper.link')]) !!}
@endfor