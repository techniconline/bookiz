{!! FormHelper::select('socials_active',trans('core::data.yes_no'),old('socials_active',$blockViewModel->getData('socials_active')),['title'=>trans('core::form.fields.active'),'helper'=>trans('core::form.helper.active')]) !!}
@foreach(trans('core::data.socials') as $name=>$trans)
    {!! FormHelper::select('socials_show_'.$name,trans('core::data.yes_no'),old('socials_show_'.$name,$blockViewModel->getData('socials_show_'.$name,0)),['title'=>trans('core::form.fields.socials_show',['name'=>$trans])]) !!}
@endforeach