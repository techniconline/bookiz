{!! $blockViewModel->getConfig('before_html') !!}
<div class="{{ $blockViewModel->getConfig('class_div') }}">
    <div class="{{ $blockViewModel->getConfig('class_sub_div') }}">
        @if($blockViewModel->getConfig('show_image'))
            <img src="{{  $blockViewModel->getConfig('logo') }}" alt="{{  $blockViewModel->getConfig('title') }}"
                 class="{{$blockViewModel->getConfig('class_image')}}"/>
        @endif
        @if($blockViewModel->getConfig('show_title'))
            <h1>
                {{  $blockViewModel->getConfig('title') }}
            </h1>
        @endif

        @if($blockViewModel->getConfig('show_links'))
            @for($i=1;$i<=3;$i++)
                @if($blockViewModel->getConfig('link_title_'.$i))
                    @if($i>1)
                        <h6>
                            /
                        </h6>
                    @endif
                    <h2>
                        <a href="{{$blockViewModel->getConfig('link_url_'.$i)}}">{{$blockViewModel->getConfig('link_title_'.$i)}}</a>
                    </h2>
                @endif
            @endfor
        @endif

        @if($blockViewModel->getConfig('show_phone'))
            <div class="phoneLine">
                @lang('core::form.fields.phone')
                <span>{{BridgeHelper::getConfig()->getSettings('phone','instance','core')}} </span>
            </div>
        @endif

        @if($blockViewModel->getConfig('show_email'))
            <div class="phoneLine">
                @lang('core::form.fields.email')
                <span>{{BridgeHelper::getConfig()->getSettings('email','instance','core')}} </span>
            </div>
        @endif
        @if($blockViewModel->getConfig('show_address'))
            <div class="footer_address color1 col-lg-12 col-md-12 col-sm-12 col-xs-12 mrg_top_30 txt-center ">
                {{ BridgeHelper::getConfig()->getSettings('address','instance','core') }}
            </div>
        @endif
        @if($blockViewModel->getConfig('socials_active'))
            <div class="bgcolor5 brd-rad-05 _Full100precent shadow6 txt-center SocialMediaBox col-lg-12 col-md-12 col-sm-12 col-xs-12 mrg_top_15 ">
                @foreach(trans('core::data.socials') as $name=>$trans)
                    @if($blockViewModel->getConfig('socials_show_'.$name) && $url=BridgeHelper::getConfig()->getSettings($name,'instance','core'))
                        <a href="{{ $url }}" target="_blank"><i class="fab fa-{{$name}}"></i></a>
                    @endif
                @endforeach
            </div>
        @endif
    </div>
</div>
{!! $blockViewModel->getConfig('after_html') !!}