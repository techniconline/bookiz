<div class="col-md-2 margin-top-10">
    <form id="_instance_changer_form" method="post" action="{!! route('core.instance.changerInstance') !!}">
        {!! method_field("POST") !!}
        {!! csrf_field() !!}
        <?php
        $current_instance_id = app('getInstanceObject')->getCurrentInstanceId();
        ?>
        <select name="current_instance_id" class="btn form-control input-medium _instance_changer">
            <option> ... @lang("core::instance.change_instance_list") ...</option>
            @if(isset($instances) && $instances)
                @foreach($instances as $instance)
                    <option {!! $current_instance_id==$instance->id?'selected':null !!} value="{!! $instance->id !!}">{!! $instance->name !!}</option>
                @endforeach
            @endif
        </select>
    </form>

</div>
<script>
    jQuery(document).ready(function () {
        $('body').on("change", "select._instance_changer", function (event) {

            var $clicked = $(this);
            $( "form#_instance_changer_form").submit();
        })
    });
</script>

