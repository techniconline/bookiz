{!! $blockViewModel->getConfig('before_html') !!}
@for($i=1;$i<=4;$i++)
<div class="{{$blockViewModel->getConfig('class_div')}}" >
    <span id="COUNTER_{{$i}}" class="{{ $blockViewModel->getConfig('class_counter') }} counter-runner" data-duration="{{$blockViewModel->getConfig('duration')}}" 
	data-start="{{$blockViewModel->getConfig('start_'.$i)}}" data-end="{{$blockViewModel->getConfig('end_'.$i)}}" >0</span>
    
	<br>
	<span class="{{ $blockViewModel->getConfig('class_title') }}">{{ $blockViewModel->getConfig('title_'.$i) }}</span>
	
	
	
	
</div>

@endfor
{!! $blockViewModel->getConfig('after_html') !!}
