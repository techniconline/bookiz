@for($i=1;$i<=4;$i++)
    <div class="config-box">
        {!! FormHelper::input('text','title_'.$i,old('title_'.$i,$blockViewModel->getData('title_'.$i)),['title'=>trans('core::form.fields.title').' '.$i]) !!}
        <div class="col-md-6">
            {!! FormHelper::input('number','start_'.$i,old('start_'.$i,$blockViewModel->getData('start_'.$i)),['title'=>trans('core::form.fields.start').' '.$i]) !!}
        </div>
        <div class="col-md-6">
            {!! FormHelper::input('number','end_'.$i,old('end_'.$i,$blockViewModel->getData('end_'.$i)),['title'=>trans('core::form.fields.end').' '.$i]) !!}
        </div>
        <div class="clearfix"></div>
    </div>
@endfor