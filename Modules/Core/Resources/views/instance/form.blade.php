<div class="row" id="form_instance">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("core::instance.add_instance")
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">

                {!! FormHelper::open(['role'=>'form','url'=>(isset($instance) && $instance? route('core.instance.update', ['instance_id'=>$instance->id]) : route('core.instance.save'))
                        ,'method'=>(isset($instance) && $instance?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                {!! FormHelper::input('text','name',old('name',$instance?$instance->name:null)
                ,['required'=>'required','title'=>trans("core::instance.name"),'helper'=>trans("core::instance.name")]) !!}

                {!! FormHelper::input('text','app_name',old('app_name',$instance?$instance->app_name:null)
                ,['title'=>trans("core::instance.app_name"),'helper'=>trans("core::instance.app_name")]) !!}

                {!! FormHelper::checkbox('default',1,old('default',$instance?$instance->default:null)
                ,['title'=>trans("core::instance.default"),'helper'=>trans("core::instance.default")]) !!}

                {!! FormHelper::select('currency_id',$currencies,old('currency_id',$instance?$instance->currency_id:null)
                    ,['title'=>trans("core::instance.select_currency"),'helper'=>trans("core::instance.select_currency")
                    ,'placeholder'=>trans("core::instance.select_currency")]) !!}

                {!! FormHelper::select('frontend_template_id',$frontend_templates,old('frontend_template_id',$instance?$instance->frontend_template_id:null)
                    ,['title'=>trans("core::instance.select_frontend_template"),'helper'=>trans("core::instance.select_frontend_template")
                    ,'placeholder'=>trans("core::instance.select_frontend_template")]) !!}

                {!! FormHelper::select('backend_template_id',$backend_templates,old('backend_template_id',$instance?$instance->backend_template_id:null)
                    ,['title'=>trans("core::instance.select_backend_template"),'helper'=>trans("core::instance.select_backend_template")
                    ,'placeholder'=>trans("core::instance.select_backend_template")]) !!}

                {!! FormHelper::editor('description',old('description',$instance?$instance->description:null)
                ,['title'=>trans("core::instance.description"),'helper'=>trans("core::instance.description")]) !!}

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancelWithLanguage(['title'=>trans("core::instance.submit")], ['title'=>trans("core::instance.cancel")
                , 'url'=>route('core.instance.index')], ['selected'=>$instance?$instance->language_id:null]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}


        </div>
        </div>
    </div>
</div>