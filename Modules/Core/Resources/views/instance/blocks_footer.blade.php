<div class="row" id="form_instance">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("core::form.titles.special_blocks_footer",['name'=>$BlockViewModel->getModelData('name')])
                </div>
            </div>
            <div class="portlet-body form">

                {!! FormHelper::open(['role'=>'form','url'=>route('core.instance.special_blocks_footer',['id'=>$BlockViewModel->getModelData('id')]),'method'=>'POST','class'=>'form-horizontal']) !!}

                {!! FormHelper::advancedEditor('footer',$BlockViewModel->get‌Blocks('footer'),['positions'=>['top','left','right','center','bottom']]) !!}


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel() !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close(true) !!}
            </div>
        </div>
    </div>
</div>