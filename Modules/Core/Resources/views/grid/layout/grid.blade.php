<input type="hidden" name="_token" class="_token" value="{!! csrf_token() !!}">
<div class="grid-box _chart">
    @if(count($model->buttons['before']) || $model->hasFilter() || $model->getCanExport())
        <div class="col-md-12 buttons top-buttons">

            @if(!$model->inWidget())
                @if($model->getCanChart() && !$model->use_chart)
                    <a href="{!! $model->getMakeUrl(null,null,null,1) !!}&use_chart=1" class="btn btn-success pull-left">
                        <i class="fa fa-line-chart"></i>
                        <span>نمایش چارت</span>
                    </a>
                @elseif($model->getCanChart() && $model->use_chart)
                    <a href="{!! $model->getMakeUrl(null,null,null,1) !!}&use_chart=0" class="btn btn-success pull-left">
                        <i class="fa fa-list"></i>
                        <span>نمایش جدول</span>
                    </a>
                @endif
            @endif


            @if($model->getCanExport())
                <a href="{!! $model->getMakeUrl(null,null,null,1) !!}&export=1" class="btn btn-danger pull-left">
                    <i class="fa fa-download"></i>
                    <span>خروجی گرفتن</span>
                </a>
            @endif
            @if($model->hasFilter())
                <button type="button" class="btn btn-warning open-filter-form pull-left">
                    <i class="fa fa-filter"></i>
                    <span>{{ $model->filter?'تغییر فیلتر':'اعمال فیلتر' }}</span>
                </button>
            @endif
            @if(count($model->buttons['before']))
                @foreach ($model->buttons['before'] as $button)
                    <a href="{{ $model->getActionUrl($button->route) }}"
                       {!! $model->getAttributes($button->attributes) !!}  title="{{$button->trans}}">
                        {!! $button->icon ? '<i class="'.$button->icon.'"></i>':'' !!} {{ $button->trans ? $button->trans:'' }}
                    </a>
                @endforeach
            @endif

        </div>
        @if($model->hasFilter())
            {!! $model->getFilterFormsHtml() !!}
        @endif
        <div class="col-md-12 top-ajax hidden">
        </div>
        <div class="clearfix">
            <hr/>
        </div>
    @endif
    <div class="col-md-12 table-responsive">

        @if(($model->use_chart && $model->getCanChart()))
            <div class="_loading "></div>
            {!! $model->initChartGrid()->getChartRenderer() !!}
        @else
            <table class="table table-hover Basket_tbl1">
                <thead>
                <tr class="head">
                    @if($model->showActionAll())
                        <th class="bg-info">
                            <i class="fa fa-check-square-o"></i>
                        </th>
                    @endif
                    @if($model->hasIndex)
                        <th class="bg-info">#</th>
                    @endif
                    @foreach ($model->columns as $column)
                        <th for="{{$column->name}}" {!! $model->getAttributes($column->attributes) !!} >
                            @if($column->sort)
                                @if($model->dir=='DESC')
                                    <a href="{{ $model->getMakeUrl($column->name,'ASC',null,1) }}"
                                       class="{{ $model->sort_by==$column->name?'text-danger':'' }}">{{$column->trans}}
                                        <i
                                                class="fa fa-sort-amount-desc"></i></a>
                                @else
                                    <a href="{{ $model->getMakeUrl($column->name,'DESC',null,1) }}"
                                       class="{{ $model->sort_by==$column->name?'text-danger':'' }}">{{$column->trans}}
                                        <i
                                                class="fa fa-sort-amount-asc"></i></a>
                                @endif
                            @else
                                {{$column->trans}}
                            @endif
                        </th>
                    @endforeach
                    @if($model->hasActions())
                        <th {!! $model->getAttributes($model->actionAttributes) !!}>عملیات ها</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach ($model->getRows() as $row)
                    @php
                        $actions=$model->getActionsUpdate($row);
                        $rowDecorated=$model->getRowsUpdate($row);
                    @endphp
                    <tr {!! $model->getRowsAttributes($row) !!}>
                        @if($model->showActionAll())
                            <td class="bg-info">
                                <input type="checkbox" value="{!! $row->id !!}" name="row_items[]" class="_action_all">
                            </td>
                        @endif
                        @if($model->hasIndex)
                            <td class="bg-info">{{ $loop->iteration+($model->per_page*$model->page) }}</td>
                        @endif
                        @foreach ($model->columns as $column)
                            <td {!! $model->getAttributes($column->attributes) !!} >{!! $rowDecorated->{$column->name}  !!} </td>
                        @endforeach
                        @if($model->hasActions())
                            <td class="actions">
                                @foreach ($actions as $action)
                                    <a href="{{ $model->getActionUrl($action->route,$row) }}"
                                       {!! $model->getAttributes($action->attributes) !!}  title="{{$action->trans}}">
                                        {!! $model->action_icon ? '<i class="'.$action->icon.'"></i>':'' !!} {{ $model->action_text ? $action->trans:'' }}
                                    </a>
                                @endforeach
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        @endif

        @if(!count($model->getRows()))
            <div class="alert alert-info">
                <strong>{{trans('core::messages.alert.info')}}
                    : </strong> {{ trans('core::messages.alert.not_find_data') }}
            </div>
        @endif

        @if($model->showActionAll())
            <div class="col-md-12">
                @foreach($model->getAllActionsConfig() as $item)
                    <a data-method="{!! $item['method'] !!}"
                       data-message-confirm="{!! $item["data-message-confirm"] !!}" href="{!! $item['url'] !!}"
                       class="{!! $item['class'] !!} _action_all"><i
                                class="{!! $item['icon'] !!}"></i> {!! $item['title'] !!}</a>
                @endforeach
            </div>

        @endif


        @if($model->hasPaginate())
            <div class="col-md-12">
                <div class="col-md-12 text-right">
                    {{ $model->getLinks() }}
                </div>
                <div class="col-md-12 text-left">
                    <ul class="pagination">
                        <li>
                            <form class="form-inline">
                                <span>مجموع سطر ها:</span>
                                <strong>{{$model->getRows()->total() }} سطر </strong>
                                <span> | </span>
                                <span>نمایش در هر صفحه</span>
                                {{ Form::select('per_page', $model->limit,$model->per_page,array('class'=>'form-control per_pager','data-url'=>$model->getMakeUrl(null,null,1,null))) }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        @endif

    </div>
    @if(count($model->buttons['after']))
        <div class="col-md-12 down-ajax hidden">
        </div>
        <div class="clearfix">
            <hr/>
        </div>
        <div class="col-md-12 buttons down-buttons">
            @foreach ($model->buttons['after'] as $button)
                <a href="{{ $model->getActionUrl($button->route) }}"
                   {!! $model->getAttributes($button->attributes) !!}  title="{{$button->trans}}">
                    {!! $button->icon ? '<i class="'.$button->icon.'"></i>':'' !!} {{ $button->trans ? $button->trans:'' }}
                </a>
            @endforeach
        </div>
    @endif
</div>
<script type="text/javascript">
    $(function () {
        var $loading = '<div id="floatBarsG" class="loading">\n' +
            '<div id="floatBarsG_1" class="floatBarsG"></div>\n' +
            '<div id="floatBarsG_2" class="floatBarsG"></div>\n' +
            '<div id="floatBarsG_3" class="floatBarsG"></div>\n' +
            '<div id="floatBarsG_4" class="floatBarsG"></div>\n' +
            '<div id="floatBarsG_5" class="floatBarsG"></div>\n' +
            '<div id="floatBarsG_6" class="floatBarsG"></div>\n' +
            '<div id="floatBarsG_7" class="floatBarsG"></div>\n' +
            '<div id="floatBarsG_8" class="floatBarsG"></div>\n' +
            '</div>';
        var newRow = '<tr class="ajaxResult"><td class="ajaxResult-td" colspan="' + parseInt($("tr.head th").length) + '">' + $loading + '</td></tr>';

        $("body").on("click", "a._action_all", function (event) {
            event.preventDefault();
            var $clicked = $(this);
            var $href = $clicked.attr('href');
            var $method = $clicked.attr('data-method');
            var $confirm_message = $clicked.attr('data-message-confirm');
            var $inputs = $('body').find('input._action_all:checkbox:checked');
            var $data = {};
            $data = $inputs.serialize();
            $data["_method"] = $method;
            $ajax($clicked, $href, $method, $inputs, true, $confirm_message, true);
        });

        var $ajax = function ($this, $url, $method, $data, has_confirm, $confirm_message, $refresh) {
            $confirm_message = $confirm_message ? $confirm_message : "Are You Sure?";
            swal({
                title: $confirm_message,
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: $method,
                        headers: {
                            'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                            'ajax': 1
                        },
                        url: $url,
                        data: $data,
                        success: function ($result) {

                            swal($result.message, '', 'success');
                            if ($result.action && $refresh) {
                                location.reload();
                                return true;
                            }


                        },
                        error: function (e) {
                            alert('Error In Process!');
                        }
                    });
                }
            });
        }

        var ajaxUrl = function ($this, $url, $method, $data, $confirm, $confirm_message, $clicked) {

            //console.log($data, $method);
            if ($method == 'POST') {
                $('.ajaxResult-td').html($loading);
                $('.ajaxResult-td .loading').show('fast');
                $sendData = $data;
            } else {
                $(".ajaxResult").remove();
                var row = $($this).closest('tr');
                $(row).after(newRow);
                $('.ajaxResult-td .loading').show('fast');
                $sendData = false;
            }

            var $resultConfirm = true;
            var $processData = false;
            if ($confirm == true) {
                $resultConfirm = confirm($confirm_message ? $confirm_message : "Are You Sure?");
                $sendData = $data;
                $processData = true;
            }

            if (!$resultConfirm) {
                return false;
            }

            jQuery.ajax({
                method: $method,
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                contentType: false,
                headers: {
                    'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                    'ajax': 1
                },
                url: $url,
                data: $sendData,
                processData: $processData,
                success: function ($result) {
                    if ($result.refresh) {
                        location.reload();
                        return true;
                    }

                    if ($confirm == true && $result.action == true) {
                        // $clicked.parents('tr').first().fadeOut();
                        alert($result.message);
                        location.reload();
                        return true;
                    }

                    if ($result.message) {
                        alert($result.message);
                    }

                    if ($result.error) {
                        ajaxError($result, '.ajaxResult-td');
                    } else {
                        $('.ajaxResult-td').html($result.html);
                    }
                    @if($model->success_ajax_function)
                    setTimeout(function () {
                        {{ $model->success_ajax_function}}($result)
                    }, 200);
                    @endif
                    dispatchEvent(new Event('load'));
                    $('.ajaxResult-td').find('.submit').each(function () {
                        $form = $(this.form);
                        $($form).submit(function (e) {
                            e.preventDefault();
                            if ($(this).data('toggle') == 'validator' && $($form).find(".submit").hasClass('disabled')) {
                                return false;
                            }
                            var formData = new FormData(this);
                            ajaxUrl(this, $($form).attr('action'), 'POST', formData);
                            return false;
                        });
                    })
                },
                error: function (e) {
                    $result = {'html': ''};
                    ajaxError($result, '.ajaxResult-td');
                    @if($model->fail_ajax_function)
                    setTimeout(function () {
                        {{ $model->fail_ajax_function}}($result)
                    }, 200);
                    @endif
                }
            });
        };

        var ajaxError = function ($result, $cls) {
            if ($result.html.length > 0) {
                var errorHtml = $result.html;
            } else {
                $result.html = "{{trans('core::messages.alert.process_error')}}";
                var errorHtml = '<div class="alert alert-warning" role="alert"><a class="back"><i class="fa fa-times"></i> </a><strong>{{trans('core::messages.alert.error')}}! </strong>' + $result.html + '</div>';
            }
            $($cls).html(errorHtml);
        };

        $("td.actions a.ajax").click(function (e) {
            $(".top-ajax").html('').addClass('hidden');
            e.preventDefault();
            if ($(this).hasClass('open')) {
                $(".ajaxResult").remove();
                $(this).removeClass('open');
                return false;
            }
            $(this).addClass("open");
            ajaxUrl(this, $(this).attr('href'), 'GET', {});
            return false;
        });

        $("td.actions a._ajax_delete").click(function (e) {
            e.preventDefault();
            var $clicked = $(this);
            var $confirm_message = $clicked.attr("data-message-confirm");
            var $data = {};
            $data["_method"] = "DELETE";
            // $data["_token"] = $('body input[name="_token"]').val();
            ajaxUrl(this, $(this).attr('href'), 'DELETE', $data, true, $confirm_message, $clicked);
            return false;
        });

        $("td.actions a._ajax_confirm").click(function (e) {
            e.preventDefault();
            var $clicked = $(this);
            var $confirm_message = $clicked.attr("data-message-confirm");
            var $method = $clicked.attr("data-method");
            var $data = {};
            $data["_method"] = $method ? $method : "POST";
            ajaxUrl(this, $(this).attr('href'), 'POST', $data, true, $confirm_message, $clicked);
            return false;
        });


        $(document).unbind().on('click', '.open-filter-form', function (e) {

            var $clicked = $(this);
            var $grid = $clicked.parents('.grid-box').first();
            var $filter = $grid.find('.grid-filter');

            if ($clicked.hasClass('open')) {
                $clicked.removeClass('open');
                $filter.addClass('hidden');
            } else {
                $clicked.addClass('open');
                $filter.removeClass('hidden');
            }

        });

        $(document).on('click', '.ajaxResult-td .back', function (e) {
            $(".open").removeClass('open');
            $(".ajaxResult").remove();
            e.preventDefault();
            return false;
        });

        $(document).on('change', '.per_pager', function (e) {
            var value = "per_page=" + $(this).val();
            var href = $(this).attr('data-url');
            var url = href.replace('per_page=1', value);
            window.location = url;
            return false;
        });

        /*for buttons*/
        var ajaxButtonsUrl = function ($url, $method, $pos, $data) {
            if ($method == 'POST') {
                $sendData = $data;
            } else {
                $sendData = false;
            }
            $('.down-ajax,.top-ajax').html('').addClass('hidden');
            $('.' + $pos + '-ajax').html($loading).removeClass('hidden');
            $('.' + $pos + '-ajax .loading').show('fast');
            jQuery.ajax({
                method: $method,
                xhrFields: {
                    withCredentials: true
                },
                contentType: false,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('.ajaxResult-td input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                    'ajax': 1
                },
                url: $url,
                data: $sendData,
                processData: false,
                success: function ($result) {
                    if ($result.refresh) {
                        location.reload();
                        return true;
                    }
                    if ($result.error) {
                        ajaxError($result, '.' + $pos + '-ajax');
                    } else {
                        $('.' + $pos + '-ajax').html($result.html);
                    }
                    @if($model->success_ajax_function)
                    dispatchEvent(new Event('load'));
                    setTimeout(function () {
                        {{ $model->success_ajax_function}}($result)
                    }, 200);
                    @endif

                    $('.' + $pos + '-ajax').find('.submit').each(function () {
                        $form = $(this.form);
                        $($form).submit(function (e) {
                            e.preventDefault();
                            if ($(this).data('toggle') == 'validator' && $($form).find(".submit").hasClass('disabled')) {
                                return false;
                            }
                            var formData = new FormData(this);
                            ajaxButtonsUrl($($form).attr('action'), 'POST', 'top', formData);
                            return false;
                        });
                    })

                },
                error: function (e) {
                    $result = {'html': ''};
                    ajaxError($result, '.' + $pos + '-ajax');
                }
            });
        };

        $(document).on('click', '.top-buttons .ajax', function (e) {
            $(".ajaxResult").remove();
            var url = $(this).attr('href');
            if ($(this).attr('data-open')) {
                $('.top-ajax').html('').addClass('hidden');
                $(this).removeAttr('data-open');
            } else {
                $(this).attr('data-open', 'open');
                ajaxButtonsUrl(url, 'GET', 'top', false);
            }
            e.preventDefault();
            return false;
        });

        $("#filter-form-reset").click(function (e) {
            $form = $(this.form);
            $($form).find('input:text, input:password, select, textarea').val('');
            $($form).find('input:radio, input:checkbox').prop('checked', false);
        });

    });
</script>

