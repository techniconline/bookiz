@if($hasQuizzes)
    <a href="javascript:void(0);" data-toggle="modal" data-target="#quizzess-{{ $id }}"><span class="text-success center-block text-center"><i class="fa fa-check-circle"></i> &nbsp; <i style="font-family: Tahoma, Helvetica, Arial">{{$hasQuizzes}}</i> رشته </span></a>
    <div id="quizzess-{{ $id }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">رشته های ثبت نامی قرآن آموز - {{ $title }}</h4>
                </div>
                <div class="modal-body">
                    <table class="table table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>دسته بندی</th>
                                <th>رشته</th>
                                <th>منبع آزمون</th>
                                <th>تاریخ آزمون</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($quizzes as $quiz)
                                <tr>
                                    <td> {{ $loop->iteration }}</td>
                                    <td> {{ $quiz->field->category->name }}</td>
                                    <td> {{ $quiz->field->name }}</td>
                                    <td> {{ $quiz->source }}</td>
                                    <td> {{ getTimestampToJalali($quiz->date) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
                </div>
            </div>

        </div>
    </div>
    @else
    <span class="text-danger center-block text-center"> <i style="font-family: Tahoma, Helvetica, Arial">{{$hasQuizzes}}</i> رشته </span>
@endif

