<div class="col-md-12" role="main">
    <div class="panel panel-warning">
        <div class="panel-heading">
            {{ $message }}
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="POST" action="{{ $route }}">
                {{ csrf_field() }}

                <div class="form-group text-right">
                    <div class="col-md-8 col-md-offset-1">
                        <button class="btn btn-warning col-md-2 {{ isset($noClass)?$noClass:'back' }}">{{ isset($no)?$no:'خیر' }}</button>
                        <button type="submit" class="btn btn-success col-md-2 col-md-offset-1 {{ isset($yesClass)?$yesClass:'submit' }}">{{ isset($yes)?$yes:'بله' }}</button>
                        @if(isset($attention))
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <span class="help-block text-danger">
                                            <strong>* {!! $attention !!}</strong>
                                        </span>
                                    </div>
                                </div>
                         @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>