@php
    $default=isset($default)?$default:false;
    $required=isset($required)?$required:false;
    if(isset($date) && $date){
        if(is_int($date)){
            $current=getJalaliByField(null,$date);
        }else{
         $current=getDateToArray($date);
        }
    }else{
        $current=getJalaliByField();
    }
    $name=isset($name)?trim($name):'date';
    $hide_name=isset($hide_name)?trim($hide_name):false;
    $fromYear=1396;
    $toYear=1396;
    if($default){
        $def_year=$current['year'];
        $def_month=$current['month'];
        $def_day=$current['day'];
    }else{
        if(isset($date) && $date){
            $def_year=$current['year'];
            $def_month=$current['month'];
            $def_day=$current['day'];
        }else{
            $def_year='';
            $def_month='';
            $def_day='';
        }

    }

    $def_year=1396;
    $def_month=12;

    $months=[12=>'اسفند'];
    $days=[];
    for($i=12;$i<=19;$i++){
        $days[$i]=$i;
    }

    $years=[];
    for($i=$fromYear;$i<=$toYear;$i++){
     $years[$i]=$i;
    }

    $class='form-control';
    if($hide_name){
        $class='form-control remove-date-name';
    }

@endphp
<!-- {{$date}} -->
<div class="col-md-12 form-inline cls-{{$name}}">
    {{ Form::select('date-year-'.$name, $years,old('date-year-'.$name,$def_year),array('class'=>'date-year '.$class,'id'=>'date-year','data-input'=>$name ,'readonly'=>'readonly','placeholder'=>trans('data.year'))) }}
    {{ Form::select('date-month-'.$name, $months,old('date-day-'.$name,$def_month),array('class'=>'date-month '.$class ,'id'=>'date-month','data-input'=>$name,'readonly'=>'readonly','placeholder'=>trans('data.month'))) }}
    {{ Form::select('date-day-'.$name, $days,old('date-day-'.$name,$def_day),array('class'=>'date-day '.$class ,'id'=>'date-day','data-input'=>$name,'placeholder'=>trans('data.day'))) }}
    <input type="text" class="form-control" id="{{$name}}" name="{{$name}}" {{ $required ?'required':'' }} style="width:0px; hidden; opacity: 0" />
</div>
