<div class="form-group">
    <label class="col-md-4 control-label" for="{{ $name }}">{{ $title }}</label>
    <div class="col-md-6">
        @php $default=isset($default)?$default:false; @endphp
        @php $from=isset($value['from'])?$value['from']:null; @endphp
        @php $to=isset($value['to'])?$value['to']:null; @endphp
        @php
            $name_from = isset($relation) ? $name.'_from_'.$relation : $name.'_from';
            $name_to = isset($relation) ? $name.'_to_'.$relation : $name.'_to';
        @endphp
        <label class="control-label pull-right">از تاریخ: </label>@include('core::grid.elements.date',['default'=>$default,'date' => $from,'startYear'=>1396, 'endYear' => 1400,'name'=>$name_from,'required'=>false,'hide_name'=>true])
        <label class="control-label pull-right">تا تاریخ: </label>@include('core::grid.elements.date',['default'=>$default,'date' => $to,'startYear'=>1396, 'endYear' => 1400,'name'=>$name_to,'required'=>false,'hide_name'=>true])
    </div>
</div>



