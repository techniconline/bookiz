<div class="form-group">
    <label class="col-md-4 control-label" for="{{ $name }}">{{ $title }}</label>
    <div class="col-md-6">
        @php
        $name = isset($relation) ? $name.'_'.$relation : $name;
        $attributes=array('class'=>'form-control','data-id'=>$value ,'id'=>$name,'placeholder'=>trans('core::data.please_select'));
        if(isset($disabled) && $disabled){
            $attributes['disabled']='disabled';
        }
        @endphp
        {{ Form::select($name, $options,$value,$attributes) }}
    </div>
</div>

