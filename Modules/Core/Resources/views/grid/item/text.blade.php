<div class="form-group">
    <label class="col-md-4 control-label" for="{{ $name }}">{{ $title }}</label>
    <div class="col-md-6">
        @php
            $name = isset($relation) ? $name.'_'.$relation : $name;
        @endphp
       <input id="{{ $name }}" name="{{ $name }}" {!! (isset($disabled) && $disabled)?' readonly':'' !!} type="text" class="form-control input-md" value="{{ $value }}">
    </div>
</div>

