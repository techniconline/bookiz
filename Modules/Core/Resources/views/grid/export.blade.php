<div class="col-md-12 grid-export-back">
    <a  href="{{ $model->getMakeUrl() }}" class="btn btn-default btn-sm pull-left">
        <i class="fa fa-chevron-right"></i>
        <span> برگشت </span>
    </a>
</div>
<div class="col-md-12"><br></div>
<div class="col-md-12 grid-export-content">
    <div class="export-ajax-links hidden">
        @php
        $total=$model->getRows()->total();
        if($total>0){
            $pages=ceil($total/$model->export_by_page);
        }
        @endphp
        @for ($i = 1; $i <= $pages; $i++)
            <a data-page="{{$i}}" href="{{ $model->getMakeUrl(null,null,null,$i) }}&export=1&export_key={{ $model->getExportKey() }}" class="export-ajax-page">{{ $i }}</a>
        @endfor
    </div>
    <p class="bg-info">تعداد رکوردها برای خروجی گرفتن: {{ $total  }} رکورد </p>
    <p class="btn-start">
        <button type="button" class="btn btn-success export-start">
            <i class="fa fa-cogs"></i>
            آغاز فرآیند خروجی گرفتن
        </button>
    </p>
    <p class="bg-primary after-btn hidden">شروع فرآیند خروجی گرفتن</p>
    <p class="bg-info after-btn hidden" > ایجاد فایل خروجی با نام: {{ $model->getExportFileName() }}</p>
</div>
<script type="text/javascript">
    $(function() {
        var ajaxUrl=function($url){
            jQuery.ajax({
                method: 'GET',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                contentType: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'ajax':1
                },
                url: $url,
                processData: false,
                success: function($result){
                    if($result.result){
                        $(".onRun i.fa").remove();
                        $(".onRun").removeClass('bg-warning').addClass('bg-success').removeClass('onRun');
                        return export_runner($result);
                    }else{
                        $(".onRun i.fa").remove();
                        $(".onRun").removeClass('bg-warning').addClass('bg-danger').removeClass('onRun').text("متاسفانه اجرای عملیات ممکن نیست.");
                    }
                },
                error: function(e){
                    if($(".onRun").length){
                        $(".onRun i.fa").remove();
                        $(".onRun").removeClass('bg-warning').addClass('bg-danger').removeClass('onRun').text("متاسفانه اجرای عملیات ممکن نیست.");
                    }else{
                        var $html='<p class="bg-danger">متاسفانه اجرای عملیات ممکن نیست.<p>';
                        $('.grid-export-content').append($html);
                    }
                }
            });
        };

        function export_runner($result){
            if($(".export-ajax-links .export-ajax-page").length){
                var $element=$(".export-ajax-links .export-ajax-page").first();
                var $page=parseInt($($element).attr('data-page'));
                var $url=$($element).attr('href');
                var $html='<p class="bg-warning onRun">خروجی گرفتن از رکورد '+({!! $model->export_by_page !!}*($page-1))+' تا رکورد '+ ({!! $model->export_by_page !!}*$page) +' <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> <p>';
                $('.grid-export-content').append($html);
                $($element).removeClass('export-ajax-page');
                return ajaxUrl($url);
            }
            if($result && $result.url){
                $html='<a href="'+$result.url+'" class="btn btn-danger pull-right" >\n' +
                    '<i class="fa fa-download"></i>\n' +
                    '<span>دانلود</span>\n' +
                    '</a>';
                $('.grid-export-content').append($html);
                window.location=$result.url;
            }
            return;
        }

        $(".export-start").click(function(e){
            $(".btn-start").remove();
            $(".after-btn").removeClass('hidden');
            export_runner(false);
        });
    });
</script>
