<div class="col-md-12 grid-filter hidden">
    <div class="clearfix">
        <hr/>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">{{ $model->filter?'تغییر فیلتر':'اعمال فیلتر' }}</div>
        <div class="panel-body">
            <div class="col-md-12 ">
                <form class="form-horizontal" id="filterForm" method="GET" action="{{ $model->baseUrl() }}" data-action-chart="{!! $model->getChartActionUrl() !!}">
                    <fieldset>
                        <input type="hidden" name="filter" value="1"/>
                        <input type="hidden" name="sort_by" value="{{ $model->sort_by }}"/>
                        <input type="hidden" name="dir" value="{{ $model->dir }}"/>
                        <input type="hidden" name="per_page" value="{{ $model->per_page }}"/>
                        <input type="hidden" name="page" value="1"/>
                        {!! $model->getFilterItemsHtml() !!}
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success pull-left _filter_grid">
                                    <i class="fa fa-filter"></i>
                                    <span>اجرای فیلتر</span>
                                </button>

                                <button type="button" id="filter-form-reset" class="btn btn-danger pull-left">
                                    <i class="fa fa-times"></i>
                                    <span>حذف مقادیر</span>
                                </button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix">
        <hr/>
    </div>
</div>
@if($model->filter && count($model->getFiltersUrl()))
    <div class="col-md-12">
        <div class="clearfix">
            <hr/>
        </div>
        <strong>فیلتر های اعمال شده:</strong>
        @foreach($model->filters as $filters)
            @foreach($filters as $filter)
                @if (isset($filter->name))
                @php
                    $type = $filter->type;
                @endphp
{{--                    {{dd($type)}}--}}
                    {{--$type = $this->filters[$name]->type;--}}
                @endif
                {{--{{dd($model->getFilterText2($filter->name, $type))}}--}}
                @if($text = $model->getFilterText2($filter->name, $type))
{{--                    {{dd($model->getFilterLinks([$filter->name=>null]) )}}--}}
                    <span class="label label-warning">
                    <strong>
                        <a href="{{ $model->getFilterLinks([$filter->name=>null]) }}" class="text-danger">
                            <i class="fa fa-times"></i>
                        </a>
                        &nbsp;&nbsp;
                    </strong>

                    <strong class="text-primary">{{ $filter->configs['title'] }}:</strong> {{ $text }}
                </span>
                @endif
            @endforeach
        @endforeach
        <a href="{{ $model->getFilterLinks(null,false) }}" class="btn btn-sm btn-danger pull-left">
            <i class="fa fa-times"></i>
            <span>حذف فیلترها</span>
        </a>
        <div class="clearfix">
            <hr/>
        </div>
    </div>
@endif

@if($model->inWidget())
<script>
    jQuery(document).ready(function () {

        $('body').on('click', '._filter_grid', function (event) {
            event.preventDefault();
            var $clicked = $(this);
            var $form = $clicked.parents('form').first();
            var $href = $form.attr("data-action-chart");
            var $confirm_message = $clicked.attr("data-confirm-message");

            var $data = $form.find('input, select').serialize();
            var $method = "GET";

            ajaxCallChartWidget($clicked, $href, $method, $data, $confirm_message);

        });

    });

    var ajaxCallChartWidget = function ($this, $url, $method, $data, $confirm_message) {

        $confirm_message = $confirm_message ? $confirm_message : "Are You Sure?";

        swal({
            title: $confirm_message,
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: '@lang("core::chart.no")',
            confirmButtonText: '@lang("core::chart.yes")'
        }).then((result) => {
            if (result.value) {
                $this.parents('._chart').first().find("._loading").addClass("loading");
                $.ajax({
                    method: $method,
                    headers: {
                        'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                        'is_ajax': 1
                    },
                    url: $url,
                    data: $data,
                    success: function ($result) {
                        if ($result.action) {
                            var $target = $this.parents('div._chart').first().find('div._result_chart');
                            $target.html($result.chart_view.view);
                            eval($result.chart_view.script);
                        }
                        $this.parents('._chart').first().find("._loading").removeClass("loading");
                    },
                    error: function (e) {
                        alert('Error In Process!');
                        $this.parents('._chart').first().find("._loading").removeClass("loading");
                    }
                });
            }
        });
    };
</script>
@endif