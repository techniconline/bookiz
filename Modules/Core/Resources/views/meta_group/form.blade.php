<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("core::meta_group.".($metaGroup?'edit':'add'))
                </div>

            </div>

            <div class="portlet-body form">

                {!! FormHelper::open(['role'=>'form','url'=>(isset($metaGroup) && $metaGroup? route('core.meta.group.update', ['meta_group_id'=>$metaGroup->id]) : route('core.meta.group.save'))
                        ,'method'=>(isset($metaGroup) && $metaGroup?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                {!! FormHelper::input('text','title',old('title',$metaGroup?$metaGroup->title:null)
                ,['required'=>'required','title'=>trans("core::meta_group.title"),'helper'=>trans("core::meta_group.title")]) !!}


                {!! FormHelper::select('system',$modules,$metaGroup?$metaGroup->system:null
                ,['title'=>trans("core::meta_group.systems"),'helper'=>trans("core::meta_group.select_system")
                ,'placeholder'=>trans("core::meta_group.select_system")]) !!}

                {!! FormHelper::checkbox('default',1,$metaGroup?$metaGroup->default:null ,['title'=>trans("core::meta_group.default")]) !!}


            @if(isset($featureGroups)&&$featureGroups)

                    {!! FormHelper::legend(trans("core::meta_group.feature_groups") , []) !!}
                    @php  FormHelper::setCustomAttribute('label','class','col-md-5')->setCustomAttribute('element','class','col-md-7') @endphp


                @foreach($featureGroups as $index => $featureGroup)
                        @php
                            $checked = boolval($featureGroupsActive && in_array($featureGroup->id, array_keys($featureGroupsActive)));
                            $position = ($featureGroupsActive && isset($featureGroupsActive[$featureGroup->id])?$featureGroupsActive[$featureGroup->id]:$index);
                        @endphp
                        <div class="col-md-3">


                            {!! FormHelper::checkbox('feature_group_ids[]',$featureGroup->id,$checked
                            ,['title'=>$featureGroup->title, 'id'=>'fg_id_'.$featureGroup->id]) !!}

                            {!! FormHelper::input('text','position['. $featureGroup->id .']',old('position['. $featureGroup->id .']',$position)
                            ,['title'=>trans("core::meta_group.position"),'helper'=>trans("core::meta_group.instance").': '.$featureGroup->instance->name
                            ,'placeholder'=>trans("core::meta_group.position").': '.$index]) !!}

                        </div>

                    @endforeach


                @endif


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancelWithLanguage(['title'=>trans("core::meta_group.submit")], ['title'=>trans("core::meta_group.cancel"), 'url'=>route('core.meta.group.index')], ['selected'=>$metaGroup?$metaGroup->language_id:null]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}


            </div>


        </div>

    </div>
</div>

