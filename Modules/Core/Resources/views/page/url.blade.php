<div class="row" id="form_instance">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> {{ $BlockViewModel->getTitlePage() }}
                </div>
            </div>
            <div class="portlet-body form">
                <div class="col-md-8 col-md-offset-2">
                    <textarea class="direLtr col-md-12" cols="10">{{str_replace(url('/'),'',route("core.page.show",['slag'=>$BlockViewModel->getModelData('slag')]))}}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>