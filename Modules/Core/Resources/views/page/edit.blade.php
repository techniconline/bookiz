<div class="row" id="form_instance">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> {{ $BlockViewModel->getTitlePage() }}
                </div>
            </div>
            <div class="portlet-body form">
                {!! FormHelper::open(['role'=>'form','url'=>route('core.page.save',['id'=>(int)$BlockViewModel->getModelData('id')]),'method'=>'POST','class'=>'form-horizontal']) !!}
                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-10') @endphp
                {!! FormHelper::input('text','title',old('title',$BlockViewModel->getModelData('title')),['required'=>'required','title'=>trans('core::form.fields.title'),'helper'=>trans('core::form.helper.title')]) !!}
                {!! FormHelper::input('text','slag',old('slag',$BlockViewModel->getModelData('slag')),['required'=>'required','title'=>trans('core::form.fields.slag'),'helper'=>trans('core::form.helper.slag')]) !!}
                {!! FormHelper::select('column',trans('core::data.columns'),old('column',$BlockViewModel->getModelData('column')),['title'=>trans('core::form.fields.column'),'helper'=>trans('core::form.helper.column')]) !!}
                {!! FormHelper::textarea('description',old('description',$BlockViewModel->getModelData('description')),['title'=>trans("core::form.fields.description"),'helper'=>trans("core::form.helper.description")]) !!}
                {!! FormHelper::advancedEditor('content',old('content',$BlockViewModel->getModelData('content')),['title'=>trans('core::form.fields.content'),'positions'=>['top','left','right','center','bottom']]) !!}
                {!! FormHelper::select('status',trans('core::data.page_status'),old('status',$BlockViewModel->getModelData('status')),['title'=>trans('core::form.fields.status'),'helper'=>trans('core::form.helper.status')]) !!}
                {!! FormHelper::openAction() !!}
                @if((int)$BlockViewModel->getModelData('id'))
                    {!! FormHelper::submitByCancelAndPreviewWithLanguage([],[],["url"=>route("core.page.preview",["slug"=>$BlockViewModel->getModelData('slag')])],['selected'=>old('language_id',$BlockViewModel->getModelData('language_id'))]) !!}
                @else
                    {!! FormHelper::submitByCancelWithLanguage([],[],['selected'=>old('language_id',$BlockViewModel->getModelData('language_id'))]) !!}
                @endif
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close(true) !!}
            </div>
        </div>
    </div>
</div>