{{--<a class="btn btn-success _chart_filter_form1111">@lang("core::chart.show_filter")</a>--}}
<div class="col-md-12 _chart_filters" data-confirm="{!! trans("core::chart.confirm_text") !!}"
     data-action="{!! $view_model->getChartActionUrl() !!}" style="display: none">

    {!! FormHelper::open(['role'=>'form','url'=>$view_model->getChartActionUrl() ,'method'=>'GET','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

    {!! FormHelper::legend($view_model->getVariableChart("chart_title")) !!}

    @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp
    @php $filters = $view_model->getFiltersChart() @endphp

    @if($filters)
        @foreach($filters as $key => $filter)
            @if($filter["type"]=="date")

                {!! FormHelper::datetime($key.'_from',request($key.'_from',null) ,['title'=>$filter["title_from"],'helper'=>$filter["title_from"] ,'date-year-current'=>1,'date-year-before'=>2,'set-default'=>0]) !!}

                {!! FormHelper::datetime($key.'_to',request($key.'_to',null) ,['title'=>$filter["title_to"],'helper'=>$filter["title_to"] ,'date-year-current'=>2,'date-year-before'=>2,'set-default'=>0]) !!}

            @endif

            @if(($filter["type"]=="select") && (isset($filter["options"]["values"])) && ($values = $filter["options"]["values"]))
                {!! FormHelper::select($key,$values,request($key,"") ,['title'=>$filter["title"],'helper'=>$filter["title"] ,'placeholder'=>$filter["title"]]) !!}
            @endif


        @endforeach

    @endif

    @php $groupByModels = $view_model->getGroupByItems(); @endphp

    @if($groupByModels->isNotEmpty())
        {!! FormHelper::select("group_by",$groupByModels,request("group_by","") ,['title'=>trans("core::chart.filter_group_by_models"),'helper'=>trans("core::chart.filter_group_by_models") ,'placeholder'=>trans("core::chart.filter_group_by_models")]) !!}
    @endif

    @php $validModels = $view_model->getValidModels(); @endphp

    @if($validModels)

        {!! FormHelper::selectTag('valid_models[]',$validModels,request('valid_models[]',array_keys($validModels)),['multiple'=>'multiple', 'title'=>trans("core::chart.filter_by_models"),'helper'=>trans("core::chart.filter_by_models")]) !!}

    @endif

    {!! FormHelper::openAction() !!}
    {!! FormHelper::submitOnly(['class'=>'btn btn-success _render_chart','title'=>trans("core::chart.action_filter")]) !!}
    {!! FormHelper::closeAction() !!}
    {!! FormHelper::close() !!}

</div>
<script>
    jQuery(document).ready(function () {

        $('body').unbind().on('click', 'a._chart_filter_form', function (event) {
            event.preventDefault();
            var $clicked = $(this);
            var $form = $clicked.parents("div._chart").first().find("._chart_filters").first();
            if ($clicked.hasClass("active")) {
                $form.fadeOut();
                $clicked.removeClass("active");
            }else{
                $form.fadeIn();
                $clicked.addClass("active");
            }

        });
    });

</script>
