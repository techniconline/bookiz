<div class="_loading "></div>

<div class="col-md-6 col-sm-6 _chart" data-confirm="{!! trans("core::chart.confirm_text") !!}"
     data-action="{!! $view_model->getChartActionUrl() !!}">


    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold uppercase font-dark">{!! $view_model->getData('alias', trans('core::form.titles.dashboard')) !!}</span>
                <span class="caption-helper">...</span>
            </div>
            <div class="actions">

                <a class="btn btn-circle btn-icon-only btn-default _chart_filter_form">
                    <i class="icon-wrench"></i>
                </a>

            </div>
            <div class="portlet-body">

                @if($view_model->by_filter_chart)
                    @include('core::chart.filters',["view_model"=>$view_model])
                @endif

            </div>
        </div>
        <div class="portlet-body">


            <div class="_result_chart">
                {!! ($view_model->chart->render()) !!}
            </div>
        </div>
    </div>

</div>

<script>
    jQuery(document).ready(function () {

        $('body').on('click', 'input._render_chart', function (event) {
            event.preventDefault();
            var $clicked = $(this);
            var $form = $clicked.parents('div._chart').first();
            var $data = $form.find('input, select').serialize();
            var $confirm_message = $form.attr("data-confirm");
            var $url = $clicked.parents("form").first().attr("action");
            ajaxCallChart($clicked, $url, "GET", $data, $confirm_message);
        });

        var ajaxCallChart = function ($this, $url, $method, $data, $confirm_message) {

            $confirm_message = $confirm_message ? $confirm_message : "Are You Sure?";

            swal({
                title: $confirm_message,
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: '@lang("core::chart.no")',
                confirmButtonText: '@lang("core::chart.yes")'
            }).then((result) => {
                if (result.value) {
                    $('body').find("._loading").addClass("loading");
                    $.ajax({
                        method: $method,
                        headers: {
                            'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                            'is_ajax': 1
                        },
                        url: $url,
                        data: $data,
                        success: function ($result) {
                            if ($result.action) {
                                var $target = $this.parents('div._chart').first().find('div._result_chart');
                                console.log($result.chart_view.view);
                                $target.html($result.chart_view.view);
                                // chartData($result.chart_view.element, $result.chart_view.type, $result.chart_view.labels, $result.chart_view.datasets, $result.chart_view.options_row, $result.chart_view.options)
                                eval($result.chart_view.script);
                            }
                            $('body').find("._loading").removeClass("loading");
                        },
                        error: function (e) {
                            alert('Error In Process!');
                            $('body').find("._loading").removeClass("loading");
                        }
                    });
                }
            });
        };
    });

    function chartData($element, $type, $labels, $datasets, $optionsRaw, $options) {
        var ctx = document.getElementById($element);
        var $op = $options;
        if ($optionsRaw) {
            $op = $optionsRaw;
        }
        new Chart(ctx, {
            type: $type,
            data: {
                labels: $labels,
                datasets: $datasets
            },
            options: $op
        })

    }

</script>