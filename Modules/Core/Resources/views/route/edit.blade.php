<div class="row" id="form_instance">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> {{ $BlockViewModel->getTitlePage() }}
                </div>
            </div>
            <div class="portlet-body form">
                {!! FormHelper::open(['role'=>'form','url'=>route('core.route.save',['id'=>(int)$BlockViewModel->getModelData('id')]),'method'=>'POST','class'=>'form-horizontal']) !!}
                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-10') @endphp
                {!! FormHelper::selectTag('route_name',$BlockViewModel->getRoutes(),old('route_name',$BlockViewModel->getModelData('route_name')),['title'=>trans('core::form.fields.route_name'),'helper'=>trans('core::form.helper.route_name')]) !!}
                {!! FormHelper::select('column',trans('core::data.columns'),old('column',$BlockViewModel->getModelData('column')),['title'=>trans('core::form.fields.column'),'helper'=>trans('core::form.helper.column')]) !!}
                {!! FormHelper::checkbox('is_api',1,old('is_api',$BlockViewModel->getModelData('is_api')),['title'=>trans("core::form.fields.is_api")]) !!}
                {!! FormHelper::advancedEditor('json',old('json',$BlockViewModel->getModelData('json_object')),['title'=>trans('core::form.fields.content'),'has_content'=>true,'positions'=>['top','left','right','center','bottom']]) !!}
                {!! FormHelper::select('active',trans('core::data.active'),old('active',$BlockViewModel->getModelData('active')),['title'=>trans('core::form.fields.status'),'helper'=>trans('core::form.helper.status')]) !!}
                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancelWithLanguage([],[],['selected'=>old('language_id',$BlockViewModel->getModelData('language_id'))]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close(true) !!}
            </div>
        </div>
    </div>
</div>