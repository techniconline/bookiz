<div class="row" id="form_feature">
    <div class="col-md-12">
        <a href="{!! route('core.formBuilder.index') !!}" class="btn btn-lg green circle-right margin-bottom-5"><i
                    class="fa fa-list"></i> @lang("core::formBuilder.list") </a>
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-course-camera"></i> <i class="fa fa-tags"></i> @lang("core::formBuilder.edit_meta_tags")
                    : {!! $form->title or null  !!}
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">
                {{--<form role="form"--}}
                {{--action="{!! isset($form) && $form? route('core.formBuilder.update', ['form_id'=>$form->id]) : route('core.formBuilder.save') !!}"--}}
                {{--method="POST">--}}
                {{--<input type="hidden" name="_method" value="{!! isset($form) && $form?'PUT':'POST' !!}">--}}
                {{--{!! csrf_field() !!}--}}

                <div class="form-body">
                    <div class="form-group">

                        @if(isset($metaGroups)&&$metaGroups)
                            <div class="row">

                                <div class="col-md-12">
                                    @foreach($metaGroups as $metaGroup)

                                        <div class="col-md-3">
                                            @php
                                              $checked = boolval(in_array($metaGroup->id,$metaGroupsActive));
                                            @endphp
                                            {!! FormHelper::checkbox('meta_group_ids[]',$metaGroup->id,$checked
                                            ,['title'=>$metaGroup->title,'disabled'=>'disabled']) !!}

                                        </div>

                                    @endforeach

                                </div>

                            </div>

                        @endif

                    </div>
                </div>
                {{--</form>--}}

            </div>


        </div>

        @if(isset($form) && $form && !empty($metaGroupsActive))

            @foreach($formMetaGroups as $formMetaGroup)

                <div class="portlet box blue-dark" style="margin: 5px">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-tags"></i> {!! $formMetaGroup->group_name !!}
                        </div>
                        <div class="tools">
                            <a href="" class="expand"> </a>
                            {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}

                            {{--<a href="{!! route('core.formBuilder.meta.copy',['form_id'=>$form->id, 'course_meta_group_id'=>$formMetaGroup->id]) !!}"--}}
                               {{--class="_copy_course_meta_group"--}}
                               {{--title="@lang("core::formBuilder.copy_meta_group"): {!! $formMetaGroup->group_name !!}"--}}
                               {{--data-confirm-message="@lang("core::formBuilder.copy_meta_group"): {!! $formMetaGroup->group_name !!} ?">--}}
                                {{--<i class="fa fa-copy" style="color: white; font-size: 24px"></i> </a>--}}

                            {{--<a href="{!! route('core.formBuilder.meta.delete',['form_id'=>$form->id, 'course_meta_group_id'=>$formMetaGroup->id]) !!}"--}}
                               {{--class="_del_course_meta_group"--}}
                               {{--title="@lang("core::formBuilder.del_meta_group"): {!! $formMetaGroup->group_name !!}"--}}
                               {{--data-confirm-message="@lang("core::formBuilder.del_meta_group"): {!! $formMetaGroup->group_name !!} ?">--}}
                                {{--<i class="fa fa-remove" style="color: orangered; font-size: 24px"></i> </a>--}}
                        </div>
                    </div>

                    <div class="portlet-body form" style="display: none;">

                    {!! FormHelper::open(['role'=>'form','url'=>route('core.formBuilder.meta.features.value.save', ['form_id'=>$form->id, 'course_meta_group_id'=>$formMetaGroup->id])
                                ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                    @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                            @foreach($formMetaGroup->metaGroup->featureGroups as $featureGroup)

                                @foreach($featureGroup->featureGroupRelations as $featureGroupRelation)

                                    @if($featureGroupRelation->feature)

                                        <?php

                                        $activeValue = null;
                                        if ($metaFeatureDataList && isset($metaFeatureDataList[$formMetaGroup->id][$featureGroupRelation->feature->id])) {
                                            $activeValue = $metaFeatureDataList[$formMetaGroup->id][$featureGroupRelation->feature->id];
                                        }

                                        ?>

                                        {!!
                                            $view_model->setFeatureGroup($featureGroup)
                                            ->setFeatureModel($featureGroupRelation->feature)
                                            ->setActiveValueFeature($activeValue)
                                            ->getHtml()
                                        !!}

                                    @endif

                                @endforeach

                            @endforeach


                        {!! FormHelper::openAction() !!}
                        {!! FormHelper::submitByCancel(['title'=>trans("core::formBuilder.submit"),'class'=>'btn btn green _save_meta'], ['title'=>trans("core::formBuilder.cancel"), 'url'=>'#']) !!}
                        {!! FormHelper::closeAction() !!}
                        {!! FormHelper::close() !!}


                    </div>
                </div>

            @endforeach

            <hr>

        @endif

    </div>

</div>

<div>

</div>
