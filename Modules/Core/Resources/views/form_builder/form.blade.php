<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("core::formBuilder.".($form?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>(isset($form) && $form? route('core.formBuilder.update', ['form_id'=>$form->id]) : route('core.formBuilder.save'))
                        ,'method'=>(isset($form) && $form?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp


                {!! FormHelper::input('text','title',old('title',$form?$form->title:null)
                ,['required'=>'required','title'=>trans("core::formBuilder.title"),'helper'=>trans("core::formBuilder.title")]) !!}

                {!! FormHelper::textarea('params',old('params',$form?$form->params:null)
                    ,['title'=>trans("core::formBuilder.params"). '(JSON)','placeholder'=>'sample: {"grid_view_feature_show":{"feature_id":[12,15]}}','helper'=>'sample: {"grid_view_feature_show":{"feature_id":[12,15]}}']) !!}


                @if(isset($metaGroups)&&$metaGroups)
                    <hr>
                    {!! FormHelper::selectTag('meta_group_ids[]',$metaGroups,old('meta_group_ids',$metaGroupsActive?$metaGroupsActive:null),['multiple'=>'multiple'
                         , 'title'=>trans("video::video.meta_groups"),'helper'=>trans("video::video.meta_groups")]) !!}

                @endif

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("core::formBuilder.submit")], ['title'=>trans("core::formBuilder.cancel"), 'url'=>route('core.formBuilder.index')]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>
