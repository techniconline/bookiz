<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("core::formBuilder.record.show")

                    <a href="{!! route('core.formBuilder.record.index',['form_id'=>isset($form_record)&&$form_record?$form_record->form_id:0]) !!}"
                       class="btn btn-danger btn-sm"><i
                                class="fa fa-forward"></i> @lang("core::formBuilder.record.list")</a>

                </div>
                <div class="tools">
                    {{--<a href="" class="collapse"> </a>--}}
                    {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                    {{--<a href="" class="reload"> </a>--}}
                    {{--<a href="" class="remove"> </a>--}}
                </div>
            </div>
            <div class="portlet-body form">
                {!! BridgeHelper::getBlockHelper()->getBlock('form_block')->setConfigs(['form_id'=>isset($form_record)&&$form_record?$form_record->form_id:0,'with_actions'=>false, 'form_record_id'=>isset($form_record)&&$form_record?$form_record->id:0])->render() !!}
            </div>


        </div>

    </div>

</div>
