<div class="row" id="form_menu">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("core::language.".(isset($language) && $language?'edit_language':'add_language'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">

                {!! FormHelper::open(['role'=>'form','url'=>(isset($language) && $language? route('core.language.update', ['language_id'=>$language->id]) : route('core.language.save'))
                        ,'method'=>(isset($language) && $language?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                {!! FormHelper::input('text','name',old('name',$language?$language->name:null)
                ,['required'=>'required','title'=>trans("core::language.name"),'helper'=>trans("core::language.name")]) !!}


                {!! FormHelper::input('text','code',old('code',$language?$language->code:null)
                ,['required'=>'required','title'=>trans("core::language.code"),'helper'=>trans("core::language.code")]) !!}

                <div class="col-md-6 _direction">
                    {!! FormHelper::select('direction',trans("core::language.directions"),old('direction',$language?$language->direction:null)
                    ,['title'=>trans("core::language.direction"),'helper'=>trans("core::language.direction"),
                    'placeholder'=>trans("core::language.direction")]) !!}
                </div>
                <div class="col-md-6 _active">
                    {!! FormHelper::select('active',trans("core::language.activeItems"),old('active',$language?$language->active:null)
                    ,['title'=>trans("core::language.active"),'helper'=>trans("core::language.active"),
                    'placeholder'=>trans("core::language.active")]) !!}
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("core::language.submit")], ['title'=>trans("core::language.cancel") , 'url'=>route('core.language.index')]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>
        </div>
    </div>
</div>