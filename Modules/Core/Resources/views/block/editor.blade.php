@extends('core::layouts.master')

@section('content')

<div class="col-md-12 editor">
    {{ csrf_field() }}
    <div class="col-md-12 editor-action">
        <button class="btn btn-danger pull-right" title="preview"><i class="fa fa-eye"></i> preview</button>
    </div>
    <div class="col-md-12 editor-box">

        <div class="col-md-3 editor-box-blocks" id="editor-left" data-id="left">
            <p class="editor-caption">Right Content</p>
            <div class="col-md-3 editor-action">
                <button class="btn btn-primary pull-right editor-modal-run" data-id="editor-left" title="add" ><i class="fa fa-plus"></i> add blocks</button>
            </div>
            <div class="col-md-12 editor-blocks" id="editor-left-blocks">

            </div>
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-4 editor-box-blocks" id="editor-main" data-id="content">
            <p class="editor-caption">Main Content</p>
            <div class="col-md-12 editor-action">
                <button class="btn btn-primary pull-right editor-modal-run" data-id="editor-main" title="add"><i class="fa fa-plus"></i> add blocks</button>
            </div>
            <div class="col-md-12 editor-blocks" id="editor-main-blocks">

            </div>
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-3 editor-box-blocks" id="editor-right" data-id="right">
            <p class="editor-caption">Left Content</p>
            <div class="col-md-12 editor-action">
                <button class="btn btn-primary pull-right editor-modal-run" data-id="editor-right" title="add"><i class="fa fa-plus"></i> add blocks</button>
            </div>
            <div class="col-md-12 editor-blocks" id="editor-right-blocks">

            </div>
        </div>
    </div>
    <!-- Large modal -->
    <div class="modal fade editor-modal" tabindex="-1" role="dialog" aria-labelledby="editor-modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-head">
                </div>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">انتخاب ویجت</h4>
                </div>
                <div class="modal-body editor-ajax-box">
                    @include('core::block.elements.allblocklink')
                </div>
            </div>
        </div>
    </div>
    <textarea class="editor-all-json hidden" name="json"></textarea>
</div>

<style type="text/css">
    .direction-ltr{
        direction: ltr !important;
    }
    .direction-rtl{
        direction: rtl !important;
    }
    .loader {
        border: 5px solid #f3f3f3;
        -webkit-animation: spin 1s linear infinite;
        animation: spin 1s linear infinite;
        border-top: 5px solid #555;
        border-radius: 50%;
        width: 50px;
        height: 50px;
    }


    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    .editor{
        background:#eee ;
    }
    .editor .editor-action{
        padding: 10px;
    }
    .editor .editor-box{
        min-height: 320px;
    }
    .editor .editor-box-blocks{
        border: 1px #aaa solid;
        padding: 0px;
        min-height: 300px;

    }
    .editor .editor-box-blocks .editor-caption{
        margin: 0px;
        padding: 5px;
        font-weight: bold;
        text-align: center;
        background: #FFF;
    }

    .editor .editor-box-blocks .editor-blocks{
        min-height: 100px;
    }

    .editor .editor-box-blocks .editor-blocks .block,.editor .editor-box-blocks .editor-blocks .block-place{
        width: 100%;
        min-height: 80px;
        background: #FFF;
        padding: 15px;
        margin: 10px 0px;
        border-radius: 15px;
        border: 1px #DDD solid;
        overflow: auto;
    }
    .editor .editor-box-blocks .editor-blocks .block-place{
        background: #00ffff;
    }
    .editor .editor-box-blocks .editor-blocks .ui-sortable-helper .block-mover{
        cursor: move;
    }

    .editor .editor-box-blocks .editor-blocks .block .block-name{
        font-size: 18px;
    }
    .editor .editor-box-blocks .editor-blocks .block .block-info{
        font-size: 12px;
        color: #aaa;
    }
    .editor .editor-box-blocks .editor-blocks .block .btn{
        float: left;
        margin-left: 4px;
    }
    .editor-modal .a-btn-ajax{
        margin: 15px;
    }
    .editor-modal .tab-pane{
        padding: 15px 5px;
    }
</style>
<script type="text/javascript">
    layoutEditUrl='{!! route('core.block.editor.edit') !!}';
</script>

@stop
