@php
    $tabs=$blockViewModel->getTabs();
@endphp
@foreach($tabs as $tab)
    {!! FormHelper::legend($tab['title']) !!}
    @if($loop->iteration==1)
        @include('core::block.creator.form.default')
    @endif
    @include($tab['view'])
@endforeach