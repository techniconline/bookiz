<div>
@php
    $tabs=$blockViewModel->getTabs();
@endphp
<!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        @foreach($tabs as $tab)
            <li role="presentation" class="{!! ($loop->iteration==1)?'active':'' !!}">
                <a href="#{{ $tab['name'] }}" aria-controls="{{ $tab['name'] }}" role="tab" data-toggle="tab">{{ $tab['title'] }}</a>
            </li>
        @endforeach
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        @foreach($tabs as $tab)
            <div role="tabpanel" class="tab-pane {!! ($loop->iteration==1)?'active':'' !!}" id="{{ $tab['name'] }}">
                @if($loop->iteration==1)
                    @include('core::block.creator.form.default')
                @endif
                @include($tab['view'])
            </div>
        @endforeach

    </div>

</div>