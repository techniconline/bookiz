{!! FormHelper::checkbox('active',1,old('active',$blockViewModel->getData('active')),['title'=>trans('core::form.fields.active'),'label'=>trans('core::form.helper.active')]) !!}
{!! FormHelper::input('text','name',old('name',$blockViewModel->getData('name')),['required'=>'required','title'=>trans('core::form.fields.name'),'helper'=>trans('core::form.helper.name')]) !!}
