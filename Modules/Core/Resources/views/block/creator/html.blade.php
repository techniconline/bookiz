{!! FormHelper::select('is_mobile_box',trans('core::data.yes_no'),old('is_mobile_box',$blockViewModel->getData('is_mobile_box')),['title'=>trans('core::form.fields.is_mobile_box'),'helper'=>trans('core::form.helper.is_mobile_box')]) !!}
@if(!$blockViewModel->getOnlyFilesHtmlTab())
{!! FormHelper::textarea('before_html',old('before_html',$blockViewModel->getData('before_html')),['title'=>trans('core::form.fields.before_html'),'helper'=>trans('core::form.helper.before_html'),'class'=>'direction-ltr']) !!}
{!! FormHelper::textarea('after_html',old('after_html',$blockViewModel->getData('after_html')),['title'=>trans('core::form.fields.after_html'),'helper'=>trans('core::form.helper.after_html'),'class'=>'direction-ltr']) !!}
@endif
{!! FormHelper::textarea('files_html',old('files_html',$blockViewModel->getData('files_html')),['title'=>trans('core::form.fields.files_html'),'helper'=>trans('core::form.helper.files_html'),'class'=>'direction-ltr']) !!}
