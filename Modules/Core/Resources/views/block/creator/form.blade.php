{!! FormHelper::open($blockViewModel->getAction()) !!}
{!! FormHelper::hidden('block_id',$blockViewModel->getBlock('id')) !!}

@php  FormHelper::setCustomAttribute('label','class','col-md-3')->setCustomAttribute('element','class','col-md-9')->setErrors($blockViewModel->getErrors()) @endphp

@if($blockViewModel->hasTab())
    @include('core::block.creator.form.tabbed')
@else
    @include('core::block.creator.form.tab')
@endif
<div class="clearfix"></div>
<hr>
<div class="form-group">
    <label for=""></label>
    <button type="submit" class="btn btn-primary pull-right submit">@lang('core::form.fields.save')</button>
</div>
{!! FormHelper::close() !!}