@php
$block_tag_id='block_'.rand().'_'.time();
$group=null;
if(isset($result['group'])){
    $group=$result['group'];
}
@endphp
<div class="block {!! is_null($group)?'':$group !!}" id="{{ $block_tag_id }}"  data-group="{!! is_null($group)?'':$group !!}" data-id="{{ $result['block_id'] }}">
    <div class="col-md-8 block-mover">
        <p class="block-name">{{$result['alias']}}</p>
        <span class="block-info">{{$result['block']}}</span>
        <textarea class="hidden editor-json-value">{!! json_encode($result) !!}</textarea>
    </div>
    <div class="col-md-4">
        <button class="btn btn-default btn-sm editor-block-edit" title="edit"><i class="fa fa-pencil"></i> </button>
        <button class="btn btn-default btn-sm editor-block-show {!! $result['active']?'show':'' !!}" title="show"><i class="fa {!! $result['active']?'fa-eye':'fa-eye-slash' !!}"></i> </button>
        <button class="btn btn-default btn-sm editor-block-remover" title="remove"><i class="fa fa-times"></i> </button>
    </div>
</div>