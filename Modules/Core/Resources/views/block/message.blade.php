
@if (isset($success))
    <div class="alert alert-success">
        <strong>{{trans('core::error.success')}}: </strong> {{$success}}
    </div>
@endif
@if (isset($error))
    <div class="alert alert-danger">
        <strong>{{trans('core::error.error')}}: </strong> {{$error}}
    </div>
@endif
