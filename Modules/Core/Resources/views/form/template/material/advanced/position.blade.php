<div class="col-md-{{ $col }} editor-box-blocks" id="editor-{{ $name }}" data-id="{{ $name }}">
    <p class="editor-caption">@lang('core::block.position.'.$name)</p>
    <div class="col-md-3 editor-action">
        <button type="button" class="btn btn-primary pull-left editor-modal-run" data-id="editor-{{$name}}" title="add" ><i class="fa fa-plus"></i> @lang('core::block.add')</button>
    </div>
    <div class="col-md-12 editor-blocks" id="editor-{{$name}}-blocks">
        @if(isset($value->{$name}))
            @foreach($value->{$name} as $block)
                @include('core::form.template.material.advanced.block')
            @endforeach
        @elseif($name=='center' && !isset($value->{$name}))
            @include('core::form.template.material.advanced.content_block')
        @endif
    </div>
</div>