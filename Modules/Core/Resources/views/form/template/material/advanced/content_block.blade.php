@php $blockId='block_content'; @endphp
<div class="block"  data-id="0">
    <div class="col-md-8 block-mover">
        <p class="block-name">@lang('core::form.fields.content')</p>
        <span class="block-info">ContentPlace</span>
        <textarea class="hidden editor-json-value">
            {"block_id":0,"block":"content","alias":"content","active":1,"configs":{}}
        </textarea>
    </div>
    <div class="col-md-4">
        <button class="btn btn-default btn-sm editor-block-show show" title="show"><i class="fa fa-eye"></i> </button>
    </div>
</div>