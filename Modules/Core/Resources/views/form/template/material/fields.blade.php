@php
$class='';
if(isset($options['class'])){
    $class=$options['class'];
    unset($options['class']);
}
@endphp
<div class="{!! $class !!} {{ $errors->has($name) ? $error_class : '' }}" {!! Html::attributes($options) !!} >
    @if($label)
        {!! $label !!}
    @endif
    @if($hasParent)
       <div {!! Html::attributes($parentOptions) !!}>
    @endif
    @if($before)
       {!! $before !!}
    @endif
    {!! $html !!}
    @if($after)
       {!! $after !!}
    @endif
    @if($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span> <br/>
    @endif
    @if($helper)
       <span class="help-block">
            <strong>{!! $helper !!}</strong>
       </span>
    @endif
    @if($hasParent)
        </div>
    @endif
</div>