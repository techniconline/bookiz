<div class="avatar">
    @php $avatar=empty($avatar)? asset('/storage/user/avatars/avatar.png'):$avatar @endphp
    <img class="img-responsive img-circle" src="{!! $avatar !!}" />
</div>
