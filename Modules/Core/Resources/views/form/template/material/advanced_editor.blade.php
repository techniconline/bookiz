<div class="col-md-12 editor">
<div class="col-md-12 editor-box">
    @if(!$hasTitle)
    {!! FormHelper::legend(trans('core::block.position.'.$name)) !!}
    @endif
    @if(in_array('top',$positions))
        {!! FormHelper::legend(trans('core::block.position.top')) !!}
        @include('core::form.template.material.advanced.position',['col'=>12,'name'=>'top'])
        @if(in_array('right',$positions) || in_array('left',$positions) || in_array('center',$positions))
            <div class="clearfix"></div>
            {!! FormHelper::legend(trans('core::block.position.content')) !!}
        @endif
    @endif
    @if(in_array('right',$positions))
        @include('core::form.template.material.advanced.position',['col'=>3,'name'=>'right'])
        <div class="col-md-1"></div>
    @endif
    @if(in_array('center',$positions))
        @php
        if(in_array('right',$positions) && in_array('left',$positions)){
            $col=4;
        }elseif(in_array('right',$positions) || in_array('left',$positions)){
         $col=8;
        }else{
            $col=12;
        }
        @endphp
        @include('core::form.template.material.advanced.position',['col'=>$col,'name'=>'center'])
    @endif
    @if(in_array('left',$positions))
            <div class="col-md-1"></div>
            @include('core::form.template.material.advanced.position',['col'=>3,'name'=>'left'])
    @endif
    @if(in_array('bottom',$positions))
            <div class="clearfix"></div>
            {!! FormHelper::legend(trans('core::block.position.bottom')) !!}
            @include('core::form.template.material.advanced.position',['col'=>12,'name'=>'bottom'])
    @endif
</div>
    <textarea class="editor-all-json hidden" name="{{ $name }}"></textarea>
</div>