<div class="md-radio-list">
    @if(isset($items))
        @foreach($items as $radio)
        <div class="md-radio">
            {!! $radio['html']  !!}
            <label for="{!! $radio['id'] !!}">
                <span class="inc"></span>
                <span class="check"></span>
                <span class="box"></span>
                {!! $radio['label'] !!}
            </label>
        </div>
        @endforeach
    @else
        <div class="md-radio">
            {!! $html  !!}
            <label for="{!! $id !!}">
                <span class="inc"></span>
                <span class="check"></span>
                <span class="box"></span>
                {!! $label !!}
            </label>
        </div>
    @endif
</div>