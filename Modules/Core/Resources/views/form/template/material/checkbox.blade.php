<div class="md-checkbox-list">
    @if(isset($items))
        @foreach($items as $checkbox)
        <div class="md-checkbox">
            {!! $checkbox['html']  !!}
            <label for="{!! $checkbox['id'] !!}">
                <span class="inc"></span>
                <span class="check"></span>
                <span class="box"></span>
                {!! $checkbox['label'] !!}
            </label>
        </div>
        @endforeach
    @else
        <div class="md-checkbox">
            {!! $html  !!}
            <label for="{!! $id !!}">
                <span class="inc"></span>
                <span class="check"></span>
                <span class="box"></span>
                {!! $label !!}
            </label>
        </div>
    @endif
</div>