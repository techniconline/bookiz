@php $blockId='block_'.rand(18888,10000000).'_'.rand(18888,10000000); @endphp
<div class="block {!! isset($block->group)?$block->group:'' !!}" id="{{ $blockId }}" data-group="{!! isset($block->group)?$block->group:'' !!}" data-id="{{ $block->block_id }}">
    <div class="col-md-8 block-mover">
        <p class="block-name">{{ $block->alias }}</p>
        <span class="block-info">{{ isset($block->block)?$block->block:'' }}</span>
        <textarea class="hidden editor-json-value">{!! json_encode($block) !!}</textarea>
    </div>
    <div class="col-md-4">
        <button class="btn btn-default btn-sm editor-block-edit" title="edit"><i class="fa fa-pencil"></i> </button>
        <button class="btn btn-default btn-sm editor-block-show show" title="show"><i class="fa fa-eye"></i> </button>
        <button class="btn btn-default btn-sm editor-block-remover" title="remove"><i class="fa fa-times"></i> </button>
    </div>
</div>