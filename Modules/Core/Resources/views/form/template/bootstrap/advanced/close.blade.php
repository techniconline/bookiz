<!-- Large modal -->
<div class="modal fade editor-modal" tabindex="-1" role="dialog" aria-labelledby="editor-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-head">
            </div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">انتخاب ویجت</h4>
            </div>
            <div class="modal-body editor-ajax-box">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    @foreach($blocks as $block)
                        @php $class=($loop->iteration==1)?'active':''; @endphp
                        <li role="presentation"><a class="{{ $class }}" href="#{{ $block['module'] }}" aria-controls="{{$block['module']}}" role="tab" data-toggle="tab">{{ $block['title'] }}</a></li>
                    @endforeach
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    @foreach($blocks as $block)
                        @php $class=($loop->iteration==1)?'active':''; @endphp
                        <div role="tabpanel" class="tab-pane {{ $class }}" id="{{ $block['module'] }}">
                            @foreach($block['blocks'] as $block)
                                <a class="btn a-btn-ajax btn-sm btn-primary" href="{{ route('core.block.editor.create',['id'=>$block->id]) }}">{{ $block->title }}</a>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .direction-ltr{
        direction: ltr !important;
    }
    .direction-rtl{
        direction: rtl !important;
    }
    .loader {
        border: 5px solid #f3f3f3;
        -webkit-animation: spin 1s linear infinite;
        animation: spin 1s linear infinite;
        border-top: 5px solid #555;
        border-radius: 50%;
        width: 50px;
        height: 50px;
    }


    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    .editor{
    }
    .editor .editor-action{
        padding: 10px;
    }
    .editor .editor-box{
        min-height: 320px;
    }
    .editor .editor-box-blocks{
        border: 1px #aaa solid;
        padding: 0px;
        min-height: 300px;
        margin-bottom: 25px;
    }
    .editor .editor-box-blocks .editor-caption{
        margin: 0px;
        padding: 5px;
        font-weight: bold;
        text-align: center;
        background: #ddd;
    }

    .editor .editor-box-blocks .editor-blocks{
        min-height: 100px;
    }

    .editor .editor-box-blocks .editor-blocks .block,.editor .editor-box-blocks .editor-blocks .block-place{
        width: 100%;
        min-height: 80px;
        background: #FFF;
        padding: 15px;
        margin: 10px 0px;
        border-radius: 15px;
        border: 1px #DDD solid;
        overflow: auto;
    }
    .editor .editor-box-blocks .editor-blocks .block-place{
        background: #00ffff;
    }
    .editor .editor-box-blocks .editor-blocks .ui-sortable-helper .block-mover{
        cursor: move;
    }

    .editor .editor-box-blocks .editor-blocks .block .block-name{
        font-size: 18px;
    }
    .editor .editor-box-blocks .editor-blocks .block .block-info{
        font-size: 12px;
        color: #aaa;
    }
    .editor .editor-box-blocks .editor-blocks .block .btn{
        float: left;
        margin-left: 4px;
    }
    .editor-modal .a-btn-ajax{
        margin: 15px;
    }
    .editor-modal .tab-pane{
        padding: 15px 5px;
    }
</style>
<script type="text/javascript">
    layoutEditUrl='{!! route('core.block.editor.edit') !!}';
</script>