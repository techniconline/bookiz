@php
$currentDateObject=DateHelper::setDateTime();

if($date_year_current){
     $currntYear=$currentDateObject->getLocaleFormat('Y');
     $toYear=$currntYear+($date_year_current);
}else{
    $toYear=$currentDateObject->getLocaleFormat('Y');
}
$fromYear=$toYear-($date_year_before);

if(empty($value) && $set_default){
    $def_year=$currentDateObject->getLocaleFormat('Y');
    $def_month=$currentDateObject->getLocaleFormat('m');
    $def_day=$currentDateObject->getLocaleFormat('d');
    $def_hour=$currentDateObject->getLocaleFormat('H');
    $def_minute=$currentDateObject->getLocaleFormat('i');
}else{
    if(!empty($value) && $value){
        $dateObject=DateHelper::setDateTime($value);
        $def_year=$dateObject->getLocaleFormat('Y');
        $def_month=$dateObject->getLocaleFormat('m');
        $def_day=$dateObject->getLocaleFormat('d');
        $def_hour=$dateObject->getLocaleFormat('H');
        $def_minute=$dateObject->getLocaleFormat('i');
    }else{
        $def_year='';
        $def_month='';
        $def_day='';
        $def_hour='';
        $def_minute='';
    }
}

$years=[];
for($i=$fromYear;$i<=$toYear;$i++){
    $years[$i]=$i;
}

$monthList=DateHelper::getMonthList();
$months=[];
$monthsAttributes=[];
foreach($monthList as $index=>$values){
    $months[$index]=$values['name'];
    $monthsAttributes[$index]=['data-days'=>$values['days']];
}
/**/
$days=[];
$toDay=31;
for($i=1;$i<=$toDay;$i++){
    if($i<10){
        $days['0'.$i]='0'.$i;
    }else{
        $days[$i]=$i;
    }
}


/**/
$hours=[];
for($i=0;$i<=23;$i++){
    if($i<10){
        $hours['0'.$i]='0'.$i;
    }else{
        $hours[$i]=$i;
    }
}


/**/
$minutes=[];
for($i=0;$i<=59;$i++){
    if($i<10){
        $minutes['0'.$i]='0'.$i;
    }else{
        $minutes[$i]=$i;
    }
}


/*options of date*/
$yearOptions=array('class'=>'date-year form-control','id'=>'date-year','data-input'=>$name ,'placeholder'=>trans('core::date.fields.year'));
$monthOptions=array('class'=>'date-month form-control','id'=>'date-month','data-input'=>$name,'placeholder'=>trans('core::date.fields.month'));
$dayOptions=array('class'=>'date-day form-control','id'=>'date-day','data-input'=>$name,'placeholder'=>trans('core::date.fields.day'));
$hourOptions=array('class'=>'date-hour form-control','id'=>'date-hour','data-input'=>$name,'placeholder'=>trans('core::date.fields.hour'));
$minuteOptions=array('class'=>'date-minute form-control','id'=>'date-minute','data-input'=>$name,'placeholder'=>trans('core::date.fields.minute'));
if(isset($attributes['required'])){
    $yearOptions['required']='required';
    $monthOptions['required']='required';
    $dayOptions['required']='required';
    $hourOptions['required']='required';
    $minuteOptions['required']='required';
}
@endphp
<div class="col-md-12 form-inline cls-{{$name}}">
    {{ Form::select('date-year-'.$name, $years,old('date-year-'.$name,$def_year),$yearOptions) }}
    {{ Form::select('date-month-'.$name, $months,old('date-month-'.$name,$def_month),$monthOptions,$monthsAttributes) }}
    {{ Form::select('date-day-'.$name, $days,old('date-day-'.$name,$def_day),$dayOptions) }}
    {{ Form::select('date-hour-'.$name, $hours,old('date-hour-'.$name,$def_hour),$hourOptions) }}
    {{ Form::select('date-minute-'.$name, $minutes,old('date-minute-'.$name,$def_minute),$minuteOptions) }}
    <input type="hidden" data-date="1" data-time="1" class="form-control" id="{{$name}}" name="{{$name}}" {{ isset($attributes['required']) ?'required':'' }} />
</div>
