<div class="mt-checkbox-list">
    @if(isset($items))
        @foreach($items as $checkbox)
            <label class="mt-checkbox">
                {!! $checkbox['label'] !!}
                {!! $checkbox['html']  !!}
                <span></span>
            </label>
        @endforeach
    @else
        <label class="mt-checkbox">
            {!! $label !!}
            {!! $html  !!}
            <span></span>
        </label>
    @endif
</div>