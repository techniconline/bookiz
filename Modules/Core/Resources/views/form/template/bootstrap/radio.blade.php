<div class="mt-radio-list">
    @if(isset($items))
        @foreach($items as $radio)
            <label class="mt-radio">
                {!! $radio['label'] !!}
                {!! $radio['html']  !!}
                <span></span>
            </label>
        @endforeach
    @else
        <label class="mt-radio">
            {!! $label !!}
            {!! $html  !!}
            <span></span>
        </label>
    @endif
</div>