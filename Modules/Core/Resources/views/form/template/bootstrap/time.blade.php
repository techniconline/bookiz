@php
$currentDateObject=DateHelper::setDateTime();

if(empty($value) && $set_default){
    $def_hour=$currentDateObject->getLocaleFormat('H');
    $def_minute=$currentDateObject->getLocaleFormat('i');
}else{
    if(!empty($value) && $value){
        $dateObject=DateHelper::setLocaleDateTime($value,$date_input_format);
        $def_hour=$dateObject->getLocaleFormat('H');
        $def_minute=$dateObject->getLocaleFormat('i');
    }else{
        $def_hour='';
        $def_minute='';
    }
}

/**/
$hours=[];
for($i=0;$i<=23;$i++){
    if($i<10){
        $hours['0'.$i]='0'.$i;
    }else{
        $hours[$i]=$i;
    }
}


/**/
$minutes=[];
for($i=0;$i<=59;$i++){
    if($i<10){
        $minutes['0'.$i]='0'.$i;
    }else{
        $minutes[$i]=$i;
    }
}


/*options of date*/
$hourOptions=array('class'=>'date-hour form-control','id'=>'date-hour','data-input'=>$name,'placeholder'=>trans('core::date.fields.hour'));
$minuteOptions=array('class'=>'date-minute form-control','id'=>'date-minute','data-input'=>$name,'placeholder'=>trans('core::date.fields.minute'));
if(isset($attributes['required'])){
    $hourOptions['required']='required';
    $minuteOptions['required']='required';
}
@endphp
<div class="col-md-12 form-inline cls-{{$name}}">
    {{ Form::select('date-hour-'.$name, $hours,old('date-hour-'.$name,$def_hour),$hourOptions) }}
    {{ Form::select('date-minute-'.$name, $minutes,old('date-minute-'.$name,$def_minute),$minuteOptions) }}
    <input type="hidden" class="form-control" id="{{$name}}" name="{{$name}}" {{ isset($attributes['required']) ?'required':'' }} />
</div>
