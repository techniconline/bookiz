<div class="form-group {{ $viewModel->errors->has($field->name) ? ' has-error' : '' }}">
    <label for="{{ $field->name }}">{{  $field->label }}</label>
    <input type="text" name="{{$field->name}}" value="{!! $field->value !!}"  {!! Html::attributes($field->options) !!} >
    @include('core::form.basic.helper')
    @include('core::form.basic.error')
</div>