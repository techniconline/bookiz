<div class="form-group {{ $viewModel->errors->has($field->name) ? ' has-error' : '' }}">
    <label for="{{ $field->name }}">{{  $field->label }}</label>
    @php
    if(is_null($field->value)){
        $field->value=1;
    }
    $yesno=[
        0=>trans('core::no'),
        1=>trans('core::yes'),
    ];
    @endphp
    {{ Form::select($field->name, $yesno,$field->value,$field->options) }}
    @include('core::form.basic.helper')...................................................................................................................................................................................................................................................................
    @include('core::form.basic.error')
</div>