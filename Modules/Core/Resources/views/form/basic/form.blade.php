@if($action=$viewModel->getAction())
    <form action="{{ $action->url }}" {!! Html::attributes($action->options) !!}>
    @else
    <form method="post">
@endif
        {{ csrf_field() }}

    @if($viewModel->hasTab())
            @include('core::form.basic.tabbed')
        @else
            @include('core::form.basic.elements',['fields'=>$viewModel->getFields()])
    @endif
    <div class="form-group">
        <label for=""></label>
        <button type="submit" class="btn btn-primary pull-left submit" >submit</button>
    </div>
</form>