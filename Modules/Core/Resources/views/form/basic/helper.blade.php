@if(strlen($field->description))
    <span class="help-block text-muted">
            - {!! $field->description !!}
    </span>
@endif