<div class="form-group {{ $viewModel->errors->has($field->name) ? ' has-error' : '' }}">
    <label for="{{ $field->name }}">{{  $field->label }}</label>
    <select name="{{$field->name}}"  {!! Html::attributes($field->options) !!} >
    @include('core::form.basic.helper')
    @include('core::form.basic.error')
</div>