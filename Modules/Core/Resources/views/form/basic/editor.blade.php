<div class="form-group {{ $viewModel->errors->has($field->name) ? ' has-error' : '' }}">
    <label for="{{ $field->name }}">{{  $field->label }}</label>
    <button class="btn btn-sm btn-danger pull-left wysiwyg-close" >editor remove</button>
    <button class="btn btn-sm btn-success pull-left wysiwyg-open" >editor open</button>
    <textarea  name="{{$field->name}}" {!! Html::attributes($field->options) !!} >{!! $field->value !!}</textarea>
    @include('core::form.basic.helper')
    @include('core::form.basic.error')
</div>