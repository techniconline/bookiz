<div class="form-group {{ $viewModel->errors->has($field->name) ? ' has-error' : '' }}">
    <label for="{{ $field->name }}">{{  $field->label }}</label>
    <textarea  name="{{$field->name}}" {!! Html::attributes($field->options) !!} >{!! $field->value !!}</textarea>
    @include('core::form.basic.helper')
    @include('core::form.basic.error')
</div>