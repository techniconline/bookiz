@foreach($fields  as $field)
    @php
        $field->options['class']=isset($field->options['class'])?$field->options['class'].' form-control':$field->options['class']='form-control';
    @endphp
        @include('core::form.basic.'.$field->type,['field'=>$field])
@endforeach