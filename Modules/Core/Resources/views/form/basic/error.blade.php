@if ($viewModel->errors->has($field->name))
    <span class="help-block">
            <strong>{{  $viewModel->errors->first($field->name) }}</strong>
    </span>
@endif