<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
    <label class="col-md-2 control-label" for="body">{{ trans('validation.attributes.content') }}</label>
    <div class="col-md-10">
        <textarea id="body" name="body" class="form-control input-md editor">{!!  old('body',$model->getData('body')) !!}</textarea>
        @if ($errors->has('body'))
            <span class="help-block">
                <strong>{{ $errors->first('body') }}</strong>
            </span>
        @endif
    </div>
</div>