<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("core::feature.feature_value.".($featureValue?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>(isset($featureValue) && $featureValue? route('core.feature.values.update'
                , ['feature_value_id'=>$featureValue->id]) : route('core.feature.values.save', ['feature_id'=>$feature->id]))
                     ,'method'=>(isset($featureValue) && $featureValue?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="col-md-6">
                    {!! FormHelper::input('text','title',$feature?$feature->title:null
                    ,['disabled'=>'disabled','title'=>trans("core::feature.title"),'helper'=>trans("core::feature.title")]) !!}
                </div>
                <div class="col-md-6">
                    {!! FormHelper::select('type',$types,$feature?$feature->type:null
                    ,['disabled'=>'disabled','title'=>trans("core::feature.type"),'helper'=>trans("core::feature.type")
                    ,'placeholder'=>trans("core::feature.type"), 'class'=>'_types']) !!}
                </div>
                <div class="col-md-6">
                    {!! FormHelper::input('text','title',old('title',$featureValue?$featureValue->title:null)
                    ,['required'=>'required','title'=>trans("core::feature.feature_value.title"),'helper'=>trans("core::feature.feature_value.title")]) !!}
                </div>
                <div class="col-md-6">
                    {!! FormHelper::input('text','value',old('value',$featureValue?$featureValue->value:null)
                    ,['title'=>trans("core::feature.feature_value.value"),'helper'=>trans("core::feature.feature_value.value")]) !!}
                </div>
                {{--<div class="col-md-4">--}}
                    {!! FormHelper::select('parent_id',$featureValues,old('parent_id',$featureValue?$featureValue->parent_id:null)
                    ,['title'=>trans("core::feature.feature_value.parent"),'helper'=>trans("core::feature.feature_value.parent")
                    ,'placeholder'=>trans("core::feature.feature_value.parent"), 'class'=>'_parent_feature_values']) !!}

                {{--</div>--}}

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancelWithLanguage(['title'=>trans("core::feature.submit")], ['title'=>trans("core::feature.cancel")
                , 'url'=>isset($featureValue->edit)&&$featureValue->edit ? route('core.feature.values.create',['feature_id'=>$feature->id]) : route('core.feature.index')]
                , ['selected'=>$featureValue?$featureValue->language_id:null]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>
        </div>
    </div>
</div>