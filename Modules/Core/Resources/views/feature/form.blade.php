<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("core::feature.".($feature?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>(isset($feature) && $feature? route('core.feature.update', ['course_id'=>$feature->id]) : route('core.feature.save'))
                     ,'method'=>(isset($feature) && $feature?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                {!! FormHelper::input('text','title',old('title',$feature?$feature->title:null)
                ,['required'=>'required','title'=>trans("core::feature.title"),'helper'=>trans("core::feature.title")]) !!}

                {!! FormHelper::select('type',$types,old('type',$feature?$feature->type:null)
                ,['title'=>trans("core::feature.type"),'helper'=>trans("core::feature.type")
                ,'placeholder'=>trans("core::feature.type"), 'class'=>'_types']) !!}

                {!! FormHelper::select('parent_id',$features,old('type',$feature?$feature->parent_id:null)
                ,['title'=>trans("core::feature.parent"),'helper'=>trans("core::feature.parent")
                ,'placeholder'=>trans("core::feature.parent")]) !!}

                {!! FormHelper::checkbox('custom',1,old('custom',$feature?$feature->custom:null)
                ,['title'=>trans("core::feature.custom"),'helper'=>trans("core::feature.custom"), 'class'=>'_custom']) !!}

                {!! FormHelper::textarea('attributes',old('title',$feature?$feature->attributes:null)
                ,['title'=>trans("core::feature.attributes").'(JSON {})','helper'=>trans("core::feature.attributes")
                ,'placeholder'=>trans("core::feature.attributes").'(JSON {})']) !!}


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancelWithLanguage(['title'=>trans("core::feature.submit")], ['title'=>trans("core::feature.cancel"), 'url'=>route('core.feature.index')], ['selected'=>$feature?$feature->language_id:null]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>
        </div>
    </div>
</div>
