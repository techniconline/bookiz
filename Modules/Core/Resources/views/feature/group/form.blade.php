<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("core::feature.group.".($featureGroup?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">
                {!! FormHelper::open(['role'=>'form','url'=>(isset($featureGroup) && $featureGroup? route('core.feature.group.update'
                      , ['feature_group_id'=>$featureGroup->id]) : route('core.feature.group.save'))
                           ,'method'=>(isset($featureGroup) && $featureGroup?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp


                {{--<div class="col-md-6">--}}
                    {!! FormHelper::input('text','title',old('title',$featureGroup?$featureGroup->title:null)
                    ,['required'=>'required','title'=>trans("core::feature.group.title"),'helper'=>trans("core::feature.group.title")]) !!}
                {{--</div>--}}

                {{--<div class="col-md-6">--}}
                    {!! FormHelper::select('instance_id',$instances,$featureGroup?$featureGroup->instance_id:null
                    ,['title'=>trans("core::feature.select_instance"),'helper'=>trans("core::feature.select_instance")
                    ,'placeholder'=>trans("core::feature.select_instance")]) !!}
                {{--</div>--}}


                @if(isset($featureGroup)&&$featureGroup&&$features)

                    {!! FormHelper::selectTag('feature_ids[]',$features,old('feature_ids',$activeFeatures?$activeFeatures:null),['multiple'=>'multiple'
                         , 'title'=>trans("core::feature.group.list_feature"),'helper'=>trans("core::feature.group.list_feature")]) !!}

                @endif

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancelWithLanguage(['title'=>trans("core::feature.submit")], ['title'=>trans("core::feature.cancel")
                , 'url'=>route('core.feature.group.index')]
                , ['selected'=>$featureGroup?$featureGroup->language_id:null]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}
            </div>
        </div>
    </div>
</div>