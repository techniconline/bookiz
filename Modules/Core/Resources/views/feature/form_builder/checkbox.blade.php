<hr>
@if(isset($featureGroup) && $featureGroup)

    @if($builder->getShowFeatureGroup())
    {!! FormHelper::label('label_name_group', $featureGroup->title , ['class'=>'feature_group group_'.$featureGroup->id]) !!}
    @endif

    <div class="form-group" id="group_id_{!! $featureGroup->id !!}">
        @endif


                {!! FormHelper::label('label_name_'.$feature->id , $feature->title
                , ['class'=>'group_'.isset($featureGroup->id)?$featureGroup->id:str_random(4), 'data-group-id'=>isset($featureGroup->id)?$featureGroup->id:0 ]) !!}

                <div>

                    @foreach($feature->featureValues as $featureValue)
                        @php
                            $checked = boolval($activeValue && in_array($featureValue->id,$activeValue));
                        @endphp

                        <label class="col-md-3" data-active="{!! json_encode($activeValue) !!}">
                            {!! FormHelper::checkbox('feature['.$featureValue->feature_id.'][]'
                            ,$featureValue->id, $checked
                            , ["id"=>"checkbox_".$featureValue->id , "title"=>$featureValue->title] ) !!}

                        </label>

                    @endforeach

                </div>


        @endif

        @if(isset($featureGroup) && $featureGroup)
    </div>
@endif