<hr>
@if(isset($featureGroup) && $featureGroup)
    <div class="form-group" id="group_id_{!! $featureGroup->id !!}">
        @endif
        <?php
        $group_id = isset($featureGroup->id) ? $featureGroup->id : str_random(4);
        ?>
        @if($builder->getShowFeatureGroup())
        {!! FormHelper::label('label_name_group', $featureGroup->title , ['class'=>'feature_group group_'.$group_id]) !!}
        @endif

        {!! FormHelper::textarea('feature['.$feature->id.']',old('feature['.$feature->id.']',isset($activeValue)?$activeValue:null)
            ,['title'=>$feature->title,'helper'=>$feature->title, "class"=>'group_'.$group_id]) !!}


    @if(isset($featureGroup) && $featureGroup)
    </div>
@endif