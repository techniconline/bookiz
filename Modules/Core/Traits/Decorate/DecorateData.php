<?php

namespace Modules\Core\Traits\Decorate;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

trait DecorateData
{

    /**
     * @param $list
     * @param null $by_this_relation
     * @param array $append_fields
     * @return \Illuminate\Support\Collection
     */
    public function decorateList($list, $by_this_relation = null, array $append_fields = [])
    {
        if (empty($list)) {
            return $list;
        }

        $origin = clone $list;
        if (method_exists($list, "getCollection")) {
            $list = $list->getCollection();
        }

        $newList = $list->map(function ($row) use ($by_this_relation, $append_fields) {
            $row = $by_this_relation ? $row->$by_this_relation : $row;
            $row = $this->decorateAttributes($row, true, $append_fields);
            return $row;
        });

        if (method_exists($origin, "setCollection")) {
            $list = $origin->setCollection($newList);
        } else {
            $list = $newList;
        }

        return $list;
    }

    /**
     * @param Model $row
     * @param bool $for_list
     * @param array $append_fields
     * @return Model|\Illuminate\Support\Collection|mixed
     */
    public function decorateAttributes($row, $for_list = false, array $append_fields = [])
    {
        if (!$row) {
            return $row;
        }

        $model = get_class($row);

        if (!class_exists($model)) {
            return $row;
        }

        $relations = $row->getRelations();

        if ($relations) {
            $relations = $this->decorateRelations($relations);
        }


        $model = new $model();
        $decorate_fields = $for_list ? $model->getListFields() : $model->getApiFields();
        $decorate_append_fields = $for_list ? $model->getAppendListFields() : $model->getApiAppendFields();
        $decorate_append_fields = $append_fields ? array_merge($decorate_append_fields, $append_fields) : $decorate_append_fields;
        if ($decorate_append_fields) {
            $row = $this->decorateAppendFieldsRow($row, $decorate_append_fields, $append_fields);
        }

        if (empty($decorate_fields)) {
            return collect($row)->merge($relations);
        }

        $decorate_fields = $decorate_append_fields ? array_merge($decorate_fields, $decorate_append_fields) : $decorate_fields;
        $globalRow = [];

        $row = collect($row)->filter(function ($item, $key) use ($decorate_fields, $globalRow) {
            if (in_array($key, $decorate_fields)) {
                $globalRow[$key] = $item;
                return $globalRow;
            }
        })->merge($relations);


        return $row;
    }

    /**
     * @param $row
     * @param $decorate_append_fields
     * @param array $append_fields
     * @return mixed
     */
    private function decorateAppendFieldsRow($row, $decorate_append_fields)
    {
        return collect($decorate_append_fields)->map(function ($append) use ($row) {
            if(!$append){
                return $row;
            }
            try {
                $row[$append] = $row->$append;
                return $row;
            } catch (\Exception $exception) {

            }

        })->first();
    }

    /**
     * @param $relations
     * @return \Illuminate\Support\Collection
     */
    private function decorateRelations($relations)
    {
        return collect($relations)->mapWithKeys(function ($relation, $key) {
            $key = get_lowercase_separated_by_underscores($key);
            try {
                if ($relation instanceof Collection) {
                    $arr[$key] = collect($relation)->map(function ($item) {
                        $row = $this->decorateAttributes($item);
                        return $row;
                    });
                } else {
                    $arr[$key] = $this->decorateAttributes($relation);
                }
                return $arr;
            } catch (\Exception $exception) {
                return [$key => $relation];
            }
        });
    }

}
