<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 6/23/18
 * Time: 11:22 AM
 */

namespace Modules\Core\Traits\Form;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;



Trait Form{

    use ValidatesRequests,Elements;

    private $theme='basic';
    private $hasTab=null;
    private $tab=['general'=>['name'=>'general','title'=>'general','order'=>1,'fields'=>0]];
    private $currentTab='general';
    private $fields=[];
    private $data=[];
    private $rules=[];
    public $errors=null;
    private $action=null;

    public function addField($name,$type='text',$label=null,$description=null,$value=null,$rules=[],$options=[],$data=null,$tab=null){
        $newField=new \stdClass();
        $newField->name=$name;
        $newField->label=$label;
        $newField->description=$description;
        $newField->type=$type;
        if(is_null($value)){
            $value=$this->getValueFromRequest($name,$value);
        }
        $newField->value=$value;
        if(count($rules)){
            $this->rules[$name]=$rules;
        }
        $newField->options=$options;
        if(is_null($tab) || !isset($this->tab[$tab])){
            $tab=$this->currentTab;
        }
        $newField->data=$data;
        $newField->tab=$tab;
        $this->fields[]=$newField;
        $this->tab[$tab]['fields']=$this->tab[$tab]['fields']+1;
        return $this;
    }


    public function addFirstField($name,$type='text',$label=null,$description=null,$value=null,$rules=[],$options=[],$data=null,$tab=null){
        $newField=new \stdClass();
        $newField->name=$name;
        $newField->label=$label;
        $newField->type=$type;
        $newField->description=$description;
        if(is_null($value)){
            $value=$this->getValueFromRequest($name,$value);
        }
        $newField->value=$value;
        if(count($rules)){
            $this->rules[$name]=$rules;
        }
        $newField->options=$options;
        if(is_null($tab) || !isset($this->tab[$tab])){
            $tab=$this->currentTab;
        }
        $newField->data=$data;
        $newField->tab=$tab;
        array_unshift($this->fields,$newField);
        $this->tab[$tab]['fields']=$this->tab[$tab]['fields']+1;
        return $this;
    }


    public function addTab($name,$title,$order=0){
        $this->tab[$name]=['name'=>$name,'title'=>$title,'order'=>$order,'fields'=>0];
        $this->currentTab=$name;
        return $this;
    }

    public function validate(Request $request)
    {
        $validatorFactory = $this->getValidationFactory();
        $this->data = $request->all();
        unset($this->data['_token']);
        $validator = $validatorFactory->make($this->data, $this->rules);
        if (!$validator->passes()) {
            $this->errors = $validator->errors();
            return false;
        }
        return true;
    }

    public function getData($key=null){
        if(is_null($key)){
            return $this->data;
        }
        return isset($this->data[$key])?$this->data[$key]:null;
    }

    public function setData($data){
        $this->data=$data;
        return $this;
    }


    public function hasTab(){
        if(!is_null($this->hasTab)){
            return $this->hasTab;
        }
        $tabs=[];
        foreach($this->tab as $index=>$tab){
            if($tab['fields']>0){
                $tabs[]=$tab;
            }
        }
        usort($tabs, function($a, $b) {
            return $a['order'] <=> $b['order'];
        });
        $this->tab=$tabs;
        if(count($this->tab)>1){
            $this->hasTab=true;
            $fields=[];
            foreach($this->fields as $field){
                if(!isset($fields[$field->tab])){
                    $fields[$field->tab]=[];
                }
                $fields[$field->tab][]=$field;
            }
            $this->fields=$fields;
        }else{
            $this->hasTab=false;
        }
        return $this->hasTab;
    }

    public function getTabs(){
     return $this->tab;
    }

    public function getErrors(){
        return $this->errors;
    }


    public function getFields($tab=null){
        if(!is_null($tab)){
            if(isset($this->fields[$tab])){
                return $this->fields[$tab];
            }else{
                return [];
            }
        }

        return $this->fields;
    }

    public function toHtml(){
        $viewModel=&$this;
        return view('core::form.'.$this->theme.'.form',compact('viewModel'))->render();
    }


    public function setMessage(){
        $this->errors=new MessageBag;
    }

    public function setAction($url,$options=[]){
        $this->action=(object)['url'=>$url,'options'=>$options];
        return $this;
    }

    public function getAction(){
        return $this->action;
    }


    public function getValueFromRequest($key,$default){
        if(app('request')->has($key)){
           return app('request')->input($key);
        }
        return $default;
    }

}