<?php

namespace Modules\Core\Traits\Form;

trait FormFeatureValueRelation
{
    public function messages()
    {
        $this->messages = [
            'feature_id.required' => trans('core::errors.form.feature_id_required'),
            'form_record_id.required' => trans('core::errors.form.form_record_id_required'),
            'item_id.required' => trans('core::errors.form.item_id_required'),
        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feature()
    {
        return $this->belongsTo('Modules\Core\Models\Feature\Feature');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function featureValue()
    {
        return $this->belongsTo('Modules\Core\Models\Feature\FeatureValue');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formRecord()
    {
        return $this->belongsTo('Modules\Core\Models\Form\FormRecord');
    }
}
