<?php

namespace Modules\Core\Traits\Form;


trait FormRecord
{


    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'instance_id.required' => trans('core::errors.form.instance_id_required'),
        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function form()
    {
        return $this->belongsTo('Modules\Core\Models\Form');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formFeatureValueRelations()
    {
        return $this->hasMany('Modules\Core\Models\Form\FormFeatureValueRelation');
    }
}
