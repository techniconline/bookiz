<?php

namespace Modules\Core\Traits\Form;



trait FormMetaGroup
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function form()
    {
        return $this->belongsTo('Modules\Core\Models\Form');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function metaGroup()
    {
        return $this->belongsTo('Modules\Core\Models\Meta\MetaGroup');
    }
}
