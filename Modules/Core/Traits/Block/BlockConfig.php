<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 6/20/18
 * Time: 10:34 AM
 */

namespace Modules\Core\Traits\Block;
use Illuminate\Routing\Route;
use Modules\Core\Traits\Form\Form;


trait BlockConfig{
    use Form;

    private $block=null;
    private $assets=[];
    private $submitFunctions=[];
    private $firstTab='general';


    public function setBlock(&$block){
        $this->setMessage();
        $this->addFirstField('name','text',trans('core::name'),null,null,['required','string','min:3'],[],[],$this->firstTab);
        $this->addFirstField('active','yes_no',trans('core::active'),null,null,['required'],[],[],$this->firstTab);
        $this->block=$block;
        return $this;
    }

    public function render($result=false){
        if($result){
            return $this->toJson();
        }else{
            if(is_null($this->getAction())){
                $this->setAction(route('core.block.editor.validate',['id'=>$this->block->id]),['method'=>"post"]);
            }
            return $this->toHtml();
        }
    }

    public function toJson(){
        $data=$this->getData();
        $json=[[
            'block_id'=>$this->block->id,
            'block'=>$this->baseClass,
            'alias'=>$data['name'],
            'active'=>(int)$data['active'],
            'configs'=>$data,
        ]];
        return $json;
    }

    public function getAssetUrl($name){
        return 'http://static.farayad.local/modules/core/'.strtolower($this->baseClass).'/'.$name;
    }

    public function addAsset($name,$type,$content,$iscontent,$callback=null){
        $this->assets[]=['name'=>$name,'type'=>$type,'content'=>$content,'iscontent'=>$iscontent,'callback'=>$callback];
        return $this;
    }

    public function getAssets(){
        return $this->assets;
    }

    public function addSubmitFunctions($functionName){
        $this->submitFunctions[]=['name'=>$functionName];
        return $this;
    }


    public function getSubmitFunctions(){
        return $this->submitFunctions;
    }

}