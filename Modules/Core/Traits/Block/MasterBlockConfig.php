<?php

namespace Modules\Core\Traits\Block;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Modules\Core\Providers\Helpers\Form\Facades\MenuHelper;

trait MasterBlockConfig{
    use ValidatesRequests;

    private $tabs=[];
    private $action=null;
    private $block=false;
    private $assets=[];
    private $data=[];
    public $errors=null;
    public $name=null;
    public $theme_src=null;
    private $submitFunctions=[];
    private $hasHtmlTab=true;
    private $onlyFile=false;

    public function hasTab(){
        if(count($this->tabs)>1){
            return true;
        }
        return false;
    }

    public function setOnlyFilesHtmlTab($value=true){
        $this->onlyFile=$value;
        return $this;
    }

    public function getOnlyFilesHtmlTab(){
        return $this->onlyFile;
    }

    public function removeHtmlTab(){
        $this->hasHtmlTab=false;
        return $this;
    }

    public function getTabs(){
        return $this->tabs;
    }
    public function setBlock($block){
        $this->errors=new MessageBag;
        $this->block=$block;
        return $this;
    }

    public function getBlock($name=null){
        if(is_null($name)){
            return $this->block;
        }
        if(isset($this->block->{$name})){
            return $this->block->{$name};
        }
        return null;
    }


    public function addTab($name,$title=null,$view='core::block.creator.empty'){
        if(is_null($title)){
            if($this->name){
                $title=$this->name;
            }else{
                $title=trans('core::form.titles.general');
            }
        }
        $this->tabs[$name]=['name'=>$name,'title'=>$title,'view'=>$view];
        return $this;
    }

    public function render($result=false){
        if($result){
            return $this->toJson();
        }else{
            if(is_null($this->getAction())){
                $this->setAction(route('core.block.editor.validate',['id'=>$this->block->id]),['enctype'=>"multipart/form-data",'role'=>'form','method'=>'POST']);
            }
            if($this->hasHtmlTab){
                $this->addTab('html',trans('core::block.html'),'core::block.creator.html');
            }
            $html=$this->toHtml();
            //$this->setFormAssets();
            return $html;
        }
    }


    public function setFormAssets(){
        $assets=MenuHelper::allUsedAssets();
        if(is_array($assets)){
            foreach($assets as $asset){
                $type=substr($asset['src'],strrpos($asset['src'],'.')+1);
                $url=$this->getAssetUrl($asset['src']);
                $this->addAsset($asset['name'],$type,$url,false);
            }
        }

    }

    public function toHtml(){
        $blockViewModel=&$this;
        return view('core::block.creator.form',compact('blockViewModel'))->render();
    }

    public function setAction($url,$options=[]){
        $options['url']=$url;
        $options['id']='block_config';
        $this->action=$options;
        return $this;
    }

    public function getAction(){
        return $this->action;
    }

    public function getData($name=null,$return=null){
        if(is_null($name)){
            if(!isset($this->data['active'])){
                $this->data['active']=0;
            }
            return $this->data;
        }
        if(isset($this->data[$name])){
            return $this->data[$name];
        }
        if(app('request')->has($name)){
            return app('request')->input($name);
        }
        if($name=='name'){
            return $this->getData('alias');
        }
        if($name=='alias'){
            return $this->getDefaultName();
        }
        return $return;
    }

    public function toJson(){
        $data=$this->getData();
        $configs=$data;
        if(isset($configs['active'])){
            unset($configs['active']);
        }
        if(isset($configs['block_id'])){
            unset($configs['block_id']);
        }
        if(isset($configs['name'])){
            unset($configs['name']);
        }
        $json=[[
            'block_id'=>$this->block->id,
            'block'=>$this->baseClass,
            'alias'=>$data['name'],
            'active'=>(int)$data['active'],
            'configs'=>$configs,
        ]];
        return $json;
    }

    public function getAssetUrl($uri){
        $path=$this->getThemeSrc();
        return url($path.$uri);
    }

    public function addAsset($name,$type,$content,$iscontent,$callback=null){
        $this->assets[]=['name'=>$name,'type'=>$type,'content'=>$content,'iscontent'=>$iscontent,'callback'=>$callback];
        return $this;
    }

    public function getAssets(){
        return $this->assets;
    }

    public function addSubmitFunctions($functionName){
        $this->submitFunctions[]=['name'=>$functionName];
        return $this;
    }


    public function getSubmitFunctions(){
        return $this->submitFunctions;
    }

    public function getBlockRules(){
        return [];
    }

    public function getDefaultRules(){
        return [
            'name'=>'required|string|min:2|max:150',
        ];
    }

    public function validate(Request $request)
    {
        $validatorFactory = $this->getValidationFactory();
        $this->data = $request->all();
        unset($this->data['_token']);
        $rules=array_merge($this->getBlockRules(),$this->getDefaultRules());
        $validator = $validatorFactory->make($this->data, $rules);
        if (!$validator->passes()){
            $this->errors = $validator->errors();
            return false;
        }
        return true;
    }


    public function getErrors(){
        return $this->errors;
    }


    public function setDefaultName($name){
        $this->name=$name;
        return $this;
    }

    public function getDefaultName(){
        return $this->name;
    }

    public function getThemeSrc(){
        if($this->theme_src){
            return $this->theme_src;
        }
        $instance = app("getInstanceObject")->getInstance();
        $theme = $instance && $instance->frontend_template_id ? $instance->frontTemplate : ($instance && $instance->backend_template_id ? $instance->backendTemplate : null);
        $use_theme = $theme ? $theme->code : Config::get('theme.themeDefault');
        $this->theme_src="themes/" . $use_theme . "/";
        return $this->theme_src;
    }



}