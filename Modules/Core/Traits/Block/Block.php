<?php

namespace Modules\Core\Traits\Block;

use Illuminate\Database\Eloquent\SoftDeletes;


trait Block
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function module()
    {
        return $this->belongsTo('Modules\Core\Models\Module\Module');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pageConfigs()
    {
        return $this->hasMany('Modules\Core\Models\PageConfig');
    }


    public static function getAllBlocksByGroup(){
        $key='AllBlocksCreators';
        $items=cache($key);
        if(!$items){
            $blocks=Self::with('module')->get();
            $list=[];
            foreach($blocks as $block){
                $module=$block->module->name;
                if(!isset($list[$module])){
                    $list[$module]=['module'=>$module,'title'=>trans($module.'::block.module'),'blocks'=>[]];
                }
                $list[$module]['blocks'][$block->id]=(object)['id'=>$block->id,'name'=>$block->name,'title'=>trans($module.'::block.'.$block->name)];
            }
            $items=collect($list);
            cache([$key=>$items],1440);
       }
       return $items;
    }
}
