<?php

namespace Modules\Core\Traits\Block;


use Illuminate\Support\Facades\Cache;
use Modules\Core\Traits\String\StringHelper;

trait MasterBlock
{

    use StringHelper;

    public $data = [];
    public $configs = [];
    public $block = [];
    private $more_link;

    /**
     * @param null $name
     * @param null $default
     * @return array|null
     */
    public function getData($name = null, $default = null)
    {
        if ($name) {
            if (isset($this->data[$name])) {
                return $this->data[$name];
            }
            return $default;
        }
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
        if (isset($this->data['configs']) && is_array($this->data['configs'])) {
            $this->configs = $this->data['configs'];
        }
        $this->setAssetsOnTheme();
        return $this;
    }

    public function setConfigs($configs, $setAssets = true)
    {
        if (is_array($configs)) {
            $this->configs = $configs;
        }
        if ($setAssets) {
            $this->setAssetsOnTheme();
        }
        return $this;
    }

    /**
     * @param $name
     * @param null $return
     * @return null
     */
    public function getConfig($name, $return = null)
    {
        if (isset($this->configs[$name])) {
            return $this->configs[$name];
        }
        return $return;
    }

    /**
     * @return array
     */
    public function getConfigs()
    {
        return $this->configs;
    }


    /**
     * @param $block
     * @return $this
     */
    public function setBlock($block)
    {
        $this->block = $block;
        return $this;
    }

    /**
     * @param null $name
     * @return array|null
     */
    public function getBlock($name = null)
    {
        if (is_null($name)) {
            return $this->block;
        }
        if (isset($this->block[$name])) {
            return $this->block[$name];
        }
        return null;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setDataBlock($data = null)
    {
        if ($this->block) {
            $this->block["more_link"] = $this->getMoreLink();
            $this->block["configs"] = $this->getConfigs();
            if ($data) {
                $this->block["data"] = $data;
            }
        }
        return $this;
    }

    /**
     * @param $link
     * @return $this
     */
    public function setMoreLink($link)
    {
        $this->more_link = $link;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMoreLink()
    {
        return $this->more_link;
    }

    /**
     * @return $this
     */
    public function setAssetsOnTheme()
    {
        if (isset($this->configs['files_html'])) {
            $files_html = $this->configs['files_html'];
            $assets = [];
            if (strlen($files_html)) {
                foreach (explode(',', $files_html) as $value) {
                    if (strlen($value)) {
                        $name = basename($value);
                        if (strpos($name, '.css') != false) {
                            $assets [] = ['container' => 'block_style', 'src' => trim($value), 'name' => $name];
                        } elseif (strpos($name, '.js') != false) {
                            $assets [] = ['container' => 'block_js', 'src' => trim($value), 'name' => $name];
                        }
                    }
                }

                $this->setAssetsOnRoute($assets);
            }
        }

        $assetsUrls = $this->getAssetUrls();
        if (count($assetsUrls)) {
            foreach ($assetsUrls as $value) {
                if (strlen($value)) {
                    $name = basename($value);
                    if (strpos($name, '.css') != false) {
                        $assets [] = ['container' => 'block_style', 'src' => trim($value), 'name' => $name];
                    } elseif (strpos($name, '.js') != false) {
                        $assets [] = ['container' => 'block_js', 'src' => trim($value), 'name' => $name];
                    }
                }
            }
            $this->setAssetsOnRoute($assets);
        }

        return $this;
    }

    public function getAssetUrls()
    {
        return [];
    }

    public function setAssetsOnRoute($assets)
    {
        if (count($assets)) {
            app('getInstanceObject')->setActionAssets($assets);
        }
        return $this;
    }


    public function setRelationalDataOnInstance($name, $value)
    {
        app('getInstanceObject')->setRelationalData($name, $value);
        return $this;
    }

    public function getRelationalDataOnInstance($name)
    {
        return app('getInstanceObject')->getRelationalData($name);
    }

    public function hasRelationalDataOnInstance($name)
    {
        return app('getInstanceObject')->getRelationalData($name);
    }

    public function hasRelationalData()
    {
        if (isset($this->relationalData) && is_array($this->relationalData)) {
            foreach ($this->relationalData as $name) {
                if (!$this->hasRelationalDataOnInstance($name)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param $key
     * @param $configs
     * @return mixed
     */
    public function setCacheConfigs($key, $configs)
    {
        return Cache::forever($key, $configs);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getCacheConfig($key)
    {
        return Cache::get($key);
    }
}