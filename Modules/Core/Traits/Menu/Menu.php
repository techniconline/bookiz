<?php

namespace Modules\Core\Traits\Menu;


use Illuminate\Support\Collection;

trait Menu
{


    public function messages()
    {
        $this->messages = [
            'url.unique' => trans('core::errors.menu.url_unique'),
            'url.required' => trans('core::errors.menu.url_required'),
            'title.required' => trans('core::errors.menu.title_required'),
            'type.required' => trans('core::errors.menu.type_required'),
        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('Modules\Core\Models\Menu\Menu', 'parent_id')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language')->active();
    }




}