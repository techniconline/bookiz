<?php

namespace Modules\Core\Traits\Currency;


trait Currency
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function instances()
    {
        return $this->hasMany('Modules\Core\Models\Instance\Instance');
    }
}
