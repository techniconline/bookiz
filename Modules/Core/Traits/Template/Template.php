<?php

namespace Modules\Core\Traits\Template;


trait Template
{

    public function messages()
    {
        $this->messages = [
            'code.unique' => trans('core::errors.template.code_unique'),
            'code.required' => trans('core::errors.template.code_required'),
            'name.required' => trans('core::errors.template.name_required'),
        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function backInstances()
    {
        return $this->hasMany('Modules\Core\Models\Instance\Instance', 'backend_template_id')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function frontInstances()
    {
        return $this->hasMany('Modules\Core\Models\Instance\Instance', 'frontend_template_id')->active();
    }
}
