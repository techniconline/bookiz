<?php

namespace Modules\Core\Traits\Instance;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

trait Instance
{
    public function messages()
    {
        $this->messages = [
            'name.unique' => trans('core::errors.instance.name_unique'),
            'name.required' => trans('core::errors.instance.name_required'),
            'language_id.required' => trans('core::errors.instance.language_id_required'),
            'currency_id.required' => trans('core::errors.instance.currency_id_required'),
        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('Modules\Core\Models\Currency\Currency');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function backendTemplate()
    {
        return $this->belongsTo('Modules\Core\Models\Template\Template', 'backend_template_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function frontTemplate()
    {
        return $this->belongsTo('Modules\Core\Models\Template\Template', 'frontend_template_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function instanceConfigs()
    {
        return $this->hasMany('Modules\Core\Models\Instance\InstanceConfig');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function instanceRouteBlocks()
    {
        return $this->hasMany('Modules\Core\Models\Instance\InstanceRouteBlock');
    }

    /**
     * @param null $is_api
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function instanceRouteBlock()
    {
        $instanceObject = app('getInstanceObject');
        $model = (new \Modules\Core\Models\Instance\InstanceRouteBlock());
        $instance_id = $instanceObject->getCurrentInstanceId();
        $is_api = $instanceObject->isApi()?1:0;
        $key = "instance_route_block_".$instance_id."_is_api_".$is_api."_route_name_".Route::getCurrentRoute()->getName();

        if(Cache::has($key)){
            $data = Cache::get($key);
            return $data;
        }else{
            $data = $model->active()->where($model->getTable() . ".instance_id", $instanceObject->getCurrentInstanceId())
                ->where(function ($q) use ($instanceObject) {
                    if ($instanceObject->isApi()) {
                        $q->where('is_api', 1);
                    } else {
                        $q->whereNull("is_api");
                    }
                })
                ->where('route_name', Route::getCurrentRoute()->getName())->first();
            if(!$data){
                $data = 0;
            }

            Cache::forever($key,$data);
            return $data;
        }


    }

    public function instanceRouteBlock_Old()
    {
        return $this->hasOne('Modules\Core\Models\Instance\InstanceRouteBlock')->active()->where('route_name', Route::getCurrentRoute()->getName());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function instanceSpecialBlock()
    {
        return $this->hasOne('Modules\Core\Models\Instance\InstanceSpecialBlock')->active()->where('name', request()->get("isb_name", 'header_footer_default'));
    }

    public function instanceSpecialBlockOld()
    {
        return $this->hasOne('Modules\Core\Models\Instance\InstanceSpecialBlock')->active()->where('name', request()->get("isb_name", 'header_footer_default'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pages()
    {
        return $this->hasMany('Modules\Core\Models\Page\Page');
    }

}