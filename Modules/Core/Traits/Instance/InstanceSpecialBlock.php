<?php

namespace Modules\Core\Traits\Instance;


trait InstanceSpecialBlock
{

    /**
     * @return mixed|null
     */
    public function getJsonArrayAttribute()
    {
        if($value = $this->json){
            return json_decode($value,true);
        }
        return null;
    }

    public function getBlocksAttribute()
    {
        return json_decode( $this->attributes['json']);
    }

    public function setBlocksAttribute( $val )
    {
        if(!is_array($val) && !is_object($val)){
            $val=explode( ', ',  $val);
        }
        $this->attributes['json'] = json_encode( $val );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance')->active();
    }

}