<?php

namespace Modules\Core\Traits\Instance;


trait InstanceRouteBlock
{

    public function getJsonObjectAttribute()
    {
        return json_decode($this->attributes['json']);
    }

    public function setJsonAttribute( $val )
    {
        if(!is_array($val) && !is_object($val)){
            $this->attributes['json'] =  $val ;

        }else{
            $this->attributes['json'] = json_encode( $val );
        }
    }

    /**
     * @return mixed|null
     */
    public function getJsonArrayAttribute()
    {
        if($value = $this->json){
            return json_decode($value,true);
        }
        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance')->active();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language');
    }

}