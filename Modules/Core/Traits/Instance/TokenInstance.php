<?php

namespace Modules\Core\Traits\Instance;


trait TokenInstance
{


    public function messages()
    {
        $this->messages = [

        ];
        return $this;
    }

    public function scopeFilterToken($query)
    {
        $query->where(function ($q){
            if (isset($this->requestItems["wstoken"])){
                $q->where('token', $this->requestItems["wstoken"]);
            }else{
                $q->where('id', 0);
            }
        });

        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance');
    }


}