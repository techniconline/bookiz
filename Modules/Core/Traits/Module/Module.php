<?php

namespace Modules\Core\Traits\Module;
use Illuminate\Support\Facades\Cache;

trait Module
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function blocks()
    {
        return $this->hasMany('Modules\Core\Models\Block\Block')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function instanceConfigs()
    {
        return $this->hasMany('Modules\Core\Models\Instance\InstanceConfig')->active();
    }

}
