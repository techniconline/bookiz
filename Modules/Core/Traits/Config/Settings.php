<?php
namespace Modules\Core\Traits\Config;
use Modules\Code\Libraries\Uploader\UploaderLibrary;
use Modules\Core\Models\Instance\InstanceConfigs;
use Modules\Core\Models\Module\Module;
use Illuminate\Support\Facades\Cache;

/**
 * Trait Settings
 * @package Modules\Core\Traits\Config
 */
trait Settings
{
    /**
     * @var array
     */
    private $exceptionData=['_token','language_id','instance_id'];
    /**
     * @var bool
     */
    private $currentSettings=false;
    /**
     * @var string
     */
    public $setting_name='';
    /**
     * @var string
     */
    public $module_name='';
    /**
     * @var null
     */
    public $instanceId=null;
    /**
     * @var null
     */
    public $languageId=null;
    /**
     * @param null $name
     * @return array|bool|null
     */
    public function getSetting($name=null){
        $settings=$this->getCurrentSettings();
        if($settings){
            if(is_null($name)){
                return $settings;
            }
            if(isset($settings->configs->{$name})){
                return $settings->configs->{$name};
            }
        }
        if(is_null($name)){
            return [];
        }
        return null;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setSettings($module_name,$name){
        $this->currentSettings=false;
        $this->setting_name=$name;
        $this->module_name=$module_name;
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setModuleName($name){
        $this->module_name=$name;
        return $this;
    }

    /**
     * @param $data
     * @return bool|InstanceConfigs
     */
    public function saveConfig($data){
        $data=$this->getClearedData($data);
        try{
            $settings=$this->getCurrentSettings();

            if(!$settings){
                $settings=new InstanceConfigs;
                $settings->name=$this->setting_name;
                $settings->module_id=$this->getModuleId($this->module_name);
                $settings->instance_id=$this->getInstanceId();
                $settings->language_id=$this->getLanguageId();
            }
            if($settings->configs){
                $data=array_merge((array)$settings->configs,$data);
            }
            $settings->configs=$data;
            $settings->save();
            Cache::forget($this->getCacheKey());
            $this->currentSettings=$settings;
            return $settings;
        }catch (\Exception $e){
            report($e);
            return false;
        }
    }

    public function uploadFilesOnRequest($files=[],$request=null,$data=[]){
        if(is_null($request)){
            $request=&$this->request;
        }
        $settingPath='settings'.DIRECTORY_SEPARATOR.$this->module_name.DIRECTORY_SEPARATOR.$this->setting_name.DIRECTORY_SEPARATOR;
        $instancePath=app('getInstanceObject')->getInstancePath();

        $path=$instancePath.$settingPath;
        foreach($files as $file){
            if($request->hasFile($file)){
                $address=$this->saveFile($file,$path);
                if($address){
                    $data[$file]=DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$address;
                }
            }
        }
        return $data;
    }

    public function makePath($path){
        if(!File::exists($path)){
            File::makeDirectory($path,0777,true,true);
        }
        return true;
    }
    public function saveFile($inputName='avatar',$path){
        $libUploader = new UploaderLibrary($inputName,$path,'');
        $result = $libUploader->setFileName($inputName)->upload();
        if($result['action']){
            return $result['items']['src'];
        }
        return false;
    }
    /**
     * @param $moduleName
     * @return bool
     */
    public function getModuleId($moduleName){
        $modules=Module::all();
        if($modules){
            $module=$modules->where('name',$moduleName)->first();
            if($module){
                return $module->id;
            }
        }
        return false;
    }


    /**
     * @return mixed
     */
    public function getInstanceId(){
        if(is_null($this->instanceId)){
            $this->instanceId=app('getInstanceObject')->getCurrentInstanceId();
        }
        return $this->instanceId;
    }

    /**
     * @return mixed
     */
    public function getLanguageId(){
        if(is_null($this->languageId)){
            $this->languageId=app('getInstanceObject')->getLanguageId();
        }
        return $this->languageId;
    }


    /**
     * @param $instance_id
     * @return $this
     */
    public function setInstanceId($instance_id){
        $this->instanceId=$instance_id;
        return $this;
    }


    /**
     * @param $languageId
     * @return $this
     */
    public function setLanguageId($languageId){
        $this->languageId=$languageId;
        return $this;
    }

    /**
     * @return bool
     */
    public function getCurrentSettings(){
        if($this->currentSettings===false){
            $key=$this->getCacheKey();
            if(!Cache::has($key)){
                $this->currentSettings=InstanceConfigs::Where('name',$this->setting_name)
                    ->where('module_id',$this->getModuleId($this->module_name))
                    ->where('instance_id',$this->getInstanceId())
                    ->where('language_id',$this->getLanguageId())->first();
                if(!$this->currentSettings){
                    $this->currentSettings = 0;
                }
                Cache::forever($key,$this->currentSettings);
            }else{
                $this->currentSettings= Cache::get($key);
            }
        }
        return $this->currentSettings;
    }

    /**
     * @return $this
     */
    public function reload(){
        $this->currentSettings=false;
        $this->getCurrentSettings();
        return $this;
    }

    /**
     * @return string
     */
    public function getCacheKey(){
        return 'Setting_of_Module_'.$this->setting_name.'_'.$this->module_name.'_'.$this->getInstanceId().'_'.$this->getLanguageId();
    }


    /**
     * @param $name
     * @param bool $value
     * @param string $return
     * @return string
     */
    public function isChecked($name, $value=true, $return='checked'){
        $saved=old($name,$this->getSetting($name));
        if($saved==$value){
            return $return;
        }
        return '';
    }


    /**
     * @param $data
     * @return mixed
     */
    public function getClearedData($data){
        foreach($this->exceptionData as $key){
            if(isset($data[$key])){
                unset($data[$key]);
            }
        }
        return $data;
    }
}