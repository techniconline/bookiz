<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 10/29/18
 * Time: 1:59 PM
 */

namespace Modules\Core\Traits\String;


trait StringHelper
{

    public function text_truncate($string, $length, $stopanywhere=false) {
        //truncates a string to a certain char length, stopping on a word if not specified otherwise.
        if (strlen($string) > $length) {
            //limit hit!
            $string = substr($string,0,($length -3));
            if ($stopanywhere) {
                //stop anywhere
                $string .= '...';
            } else{
                //stop on a word.
                $string = substr($string,0,strrpos($string,' ')).'...';
            }
        }
        return $string;
    }

}