<?php

namespace Modules\Core\Traits\Page;

use BridgeHelper;

trait PageMeta
{
    public $meta=[];
    public $title_page=false;

    public function setDescriptionMeta($value){
        return $this->setPageMeta('description',$value);
    }

    public function setKeywordsMeta($value){
        $text=is_array($value)?implode(',',$value):$value;
        return $this->setPageMeta('keywords',$text);
    }

    public function getPageMeta(){
        if(!count($this->meta)){
            $this->setDefaultMeta();
        }
        return $this->meta;
    }

    public function setPageMeta($name,$value,$attributes=[]){
        $this->meta[$name]=['name'=>$name,'value'=>$value,'attributes'=>$attributes];
        return $this;
    }


    /**
     * @param $title
     * @return $this
     */
    public function getTitlePage()
    {
        $title=$this->title_page;
        if(!$title){
            $title=BridgeHelper::getConfig()->getSettings('title','instance','core');
        }
        $pre=BridgeHelper::getConfig()->getSettings('prefix_title','instance','core');
        $post=BridgeHelper::getConfig()->getSettings('postfix_title','instance','core');
        return $pre.' '.$title.' '.$post;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitlePage($title = null)
    {
        $this->title_page = strip_tags($title);
        return $this;
    }


    public function setDefaultMeta(){
        $description=BridgeHelper::getConfig()->getSettings('description','instance','core');
        if($description){
            $this->setDescriptionMeta($description);
        }
        $keywords=BridgeHelper::getConfig()->getSettings('keywords','instance','core');
        if($keywords){
            $this->setKeywordsMeta($keywords);
        }
    }

}
