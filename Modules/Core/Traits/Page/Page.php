<?php

namespace Modules\Core\Traits\Page;


trait Page
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language');
    }




    public function getContentAttribute()
    {
        return json_decode($this->attributes['content']);
    }

    /**
     * @return mixed
     */
    public function getContentArrayAttribute()
    {
        return json_decode($this->attributes['content'], true);
    }

    public function setContentAttribute( $val )
    {
        if(!is_array($val) && !is_object($val)){
            $this->attributes['content'] =  $val ;

        }else{
            $this->attributes['content'] = json_encode( $val );
        }
    }

}
