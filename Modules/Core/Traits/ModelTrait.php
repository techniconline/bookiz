<?php

namespace Modules\Core\Traits;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;

trait ModelTrait
{
    /**
     * @param $model_type
     * @return null
     */
    public function getClassObject($model_type)
    {
        $model = get_uppercase_by_underscores($model_type);
        $class = null;
        if ($model_type && method_exists($this, "get" . $model . "Class")) {
            $class = $this->{"get" . $model . "Model"}();
        }
        return $class;
    }

    /**
     * @param $model_type
     * @return null
     */
    public function getModelObject($model_type)
    {
        $model = get_uppercase_by_underscores($model_type);
        $class = null;
        if ($model_type && method_exists($this, "get" . $model . "Model")) {
            $class = $this->{"get" . $model . "Model"}();
        }
        return $class;
    }

    /**
     * @return mixed
     */
    public function getEntityRelationServicesClass()
    {
        return BridgeHelper::getEntity()->getEntityRelationServiceClass();
    }

    /**
     * @return mixed
     */
    public function getEntityRelationServicesModel()
    {
        return BridgeHelper::getEntity()->getEntityRelationServiceModel();
    }

    /**
     * @return mixed
     */
    public function getEntitiesModel()
    {
        return BridgeHelper::getEntity()->getEntityModel();
    }

    /**
     * @return mixed
     */
    public function getEntitiesClass()
    {
        return BridgeHelper::getEntity()->getEntityClass();
    }

    /**
     * @return mixed
     */
    public function getCourseModel()
    {
        return BridgeHelper::getCourse()->getCourseModel();
    }

    /**
     * @return mixed
     */
    public function getCourseClass()
    {
        return BridgeHelper::getCourse()->getCourseClass();
    }

    /**
     * @return mixed
     */
    public function getBookingDetailsModel()
    {
        return BridgeHelper::getBooking()->getBookingDetailsModel();
    }

    /**
     * @return mixed
     */
    public function getBookingDetailsClass()
    {
        return BridgeHelper::getBooking()->getBookingDetailsClass();
    }

}
