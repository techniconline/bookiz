<?php

namespace Modules\Core\Traits\Meta;


trait MetaGroup
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [

        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function featureGroups()
    {
        return $this->belongsToMany('Modules\Core\Models\Feature\FeatureGroup', 'meta_group_feature_groups')->orderBy('position')->filterCurrentInstance()->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function videos()
    {
        return $this->belongsToMany('Modules\Video\Models\Video\Video', 'video_meta_groups')->active();
    }

}
