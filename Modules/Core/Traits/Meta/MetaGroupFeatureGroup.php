<?php

namespace Modules\Core\Traits\Meta;


trait MetaGroupFeatureGroup
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [

        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function featureGroup()
    {
        return $this->belongsTo('Modules\Video\Models\Feature\FeatureGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function metaGroup()
    {
        return $this->belongsTo('Modules\Core\Models\Meta\MetaGroup');
    }

}
