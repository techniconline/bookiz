<?php

namespace Modules\Core\Traits\Language;


trait Language
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function instances()
    {
        return $this->hasMany('Modules\Core\Models\Instance\Instance')->active();
    }
}
