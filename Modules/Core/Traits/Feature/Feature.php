<?php

namespace Modules\Core\Traits\Feature;


use Illuminate\Support\Facades\Lang;

trait Feature
{

    public function types($key = null)
    {
        $types = [
            'text' => trans('core::feature.types.text'),
            'textarea' => trans('core::feature.types.textarea'),
            'checkbox' => trans('core::feature.types.checkbox'),
            'radio' => trans('core::feature.types.radiobox'),
            'hidden' => trans('core::feature.types.hidden'),
            'select' => trans('core::feature.types.select'),
            'selectTag' => trans('core::feature.types.selectTag'),
        ];

        if ($key) {
            return isset($types[$key]) ? $types[$key] : null;
        }
        return $types;
    }

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'title.required' => trans('core::errors.feature.title_required'),
            'language_id.required' => trans('core::errors.feature.language_id_required'),
            'type.required' => trans('core::errors.feature.type_required'),
        ];
        return $this;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsNotCustom($query)
    {
        return $query->where('custom', 0);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsCustom($query)
    {
        return $query->where('custom', 1);
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getTextCustomAttribute()
    {
        if (Lang::has("core::feature.text_custom." . $this->custom)) {
            return trans("core::feature.text_custom." . $this->custom);
        }
        return '-';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('Modules\Core\Models\Feature\Feature', 'parent_id')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function featureGroupRelations()
    {
        return $this->hasMany('Modules\Core\Models\Feature\FeatureGroupRelation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function featureValueRelations()
    {
        return $this->hasMany('Modules\Core\Models\Feature\FeatureValueRelation')->active()->filterLanguage();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function featureValues()
    {
        return $this->hasMany('Modules\Core\Models\Feature\FeatureValue')->active()->filterLanguage();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parentFeatureValues()
    {
        return $this->hasMany('Modules\Core\Models\Feature\FeatureValue', 'parent_feature_id');
    }
}
