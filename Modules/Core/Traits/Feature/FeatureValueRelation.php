<?php

namespace Modules\Core\Traits\Feature;

trait FeatureValueRelation
{
    public function messages()
    {
        $this->messages = [
            'feature_id.required' => trans('core::errors.featureValueRelation.feature_id_required'),
            'language_id.required' => trans('core::errors.featureValueRelation.language_id_required'),
        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feature()
    {
        return $this->belongsTo('Modules\Core\Models\Feature\Feature');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function featureValue()
    {
        return $this->belongsTo('Modules\Core\Models\Feature\FeatureValue');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language');
    }

    /** TODO !!! warning
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function videoMetaGroup()
    {
        return $this->belongsTo('Modules\Core\Models\Feature\VideoMetaGroup', 'item_id');
    }
}
