<?php

namespace Modules\Core\Traits\Feature;

use phpDocumentor\Reflection\Types\This;

trait FormFeatureBuilder
{

    protected $feature;
    protected $row_number;
    protected $feature_group;
    protected $active_value;
    protected $show_feature_group = true;

    /**
     * @param $feature
     * @return $this
     */
    public function setFeatureModel($feature)
    {
        $this->feature = $feature;
        return $this;
    }

    /**
     * @param int $number
     * @return $this
     */
    public function setRowNumber($number = 0)
    {
        $this->row_number = $number;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRowNumber()
    {
        return $this->row_number;
    }

    /**
     * @param $featureGroup
     * @return $this
     */
    public function setFeatureGroup($featureGroup)
    {
        $this->feature_group = $featureGroup;
        return $this;
    }

    /**
     * @param bool $show
     * @return $this
     */
    public function setShowFeatureGroup($show = true)
    {
        $this->show_feature_group = $show;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShowFeatureGroup()
    {
        return $this->show_feature_group;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setActiveValueFeature($value)
    {
        $this->active_value = $value;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function get()
    {
        if(app("getInstanceObject")->isApi()){
            return $this->getJson();
        }
        return $this->getHtml();
    }

    /**
     * @return mixed
     */
    public function getHtml()
    {
        if (method_exists($this, "render" . ucfirst($this->feature->type))) {
            return $this->{"render" . ucfirst($this->feature->type)}();
        }
        return null;
    }

    public function getJson()
    {
        if (method_exists($this, "render" . ucfirst($this->feature->type) . "Json")) {
            return $this->{"render" . ucfirst($this->feature->type) . "Json"}();
        }
        return null;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    private function renderCheckbox()
    {
        $feature = $this->feature;
        $featureGroup = $this->feature_group;
        $activeValue = $this->active_value;
        $builder =& $this;
        return view('core::feature.form_builder.checkbox', compact('feature', 'featureGroup', 'activeValue', 'builder'))->render();
    }

    /**
     * @return array
     */
    private function renderCheckboxJson()
    {
        $json = [
            "feature" => $this->feature->title,
            "feature_group" => $this->feature_group,
            "active_value" => $this->active_value,
        ];
        return $json;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    private function renderSelect()
    {
        $feature = $this->feature;
        $featureGroup = $this->feature_group;
        $activeValue = array_first($this->active_value);
        $builder =& $this;
        return view('core::feature.form_builder.select', compact('feature', 'featureGroup', 'activeValue', 'builder'))->render();
    }


    /**
     * @return array
     */
    private function renderSelectJson()
    {
        $json = [
            "feature" => $this->feature->title,
            "feature_group" => $this->feature_group,
            "active_value" => array_first($this->active_value),
        ];
        return $json;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    private function renderSelectTag()
    {
        $feature = $this->feature;
        $featureGroup = $this->feature_group;
        $activeValues = $this->active_value;
        $builder =& $this;
        return view('core::feature.form_builder.selectTag', compact('feature', 'featureGroup', 'activeValues', 'builder'))->render();
    }


    /**
     * @return array
     */
    private function renderSelectTagJson()
    {
        $json = [
            "feature" => $this->feature->title,
            "feature_group" => $this->feature_group,
            "active_value" => $this->active_value,
        ];
        return $json;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    private function renderText()
    {
        $feature = $this->feature;
        $featureGroup = $this->feature_group;
        $activeValue = array_first($this->active_value);
        $builder =& $this;
        return view('core::feature.form_builder.text', compact('feature', 'featureGroup', 'activeValue', 'builder'))->render();
    }

    /**
     * @return array
     */
    private function renderTextJson()
    {
        $json = [
            "feature" => $this->feature->title,
            "feature_group" => $this->feature_group,
            "active_value" => array_first($this->active_value),
        ];
        return $json;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    private function renderTextarea()
    {
        $feature = $this->feature;
        $featureGroup = $this->feature_group;
        $activeValue = array_first($this->active_value);
        $builder =& $this;
        return view('core::feature.form_builder.textarea', compact('feature', 'featureGroup', 'activeValue', 'builder'))->render();
    }

    /**
     * @return array
     */
    private function renderTextareaJson()
    {
        $json = [
            "feature" => $this->feature->title,
            "feature_group" => $this->feature_group,
            "active_value" => array_first($this->active_value),
        ];
        return $json;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    private function renderEditor()
    {
        $feature = $this->feature;
        $featureGroup = $this->feature_group;
        $activeValue = array_first($this->active_value);
        $builder =& $this;
        return view('core::feature.form_builder.editor', compact('feature', 'featureGroup', 'activeValue', 'builder'))->render();
    }

    /**
     * @return array
     */
    private function renderEditorJson()
    {
        $json = [
            "feature" => $this->feature->title,
            "feature_group" => $this->feature_group,
            "active_value" => array_first($this->active_value),
        ];
        return $json;
    }

}
