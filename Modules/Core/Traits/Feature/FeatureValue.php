<?php

namespace Modules\Core\Traits\Feature;

trait FeatureValue
{
    public function messages()
    {
        $this->messages = [
            'value.required' => trans('core::errors.featureValue.value_required'),
            'language_id.required' => trans('core::errors.featureValue.language_id_required'),
            'feature_id.required' => trans('core::errors.featureValue.feature_id_required'),
        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feature()
    {
        return $this->belongsTo('Modules\Core\Models\Feature\Feature');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('Modules\Core\Models\Feature\FeatureValue', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function featureValueRelations()
    {
        return $this->hasMany('Modules\Core\Models\Feature\FeatureValueRelation');
    }
}
