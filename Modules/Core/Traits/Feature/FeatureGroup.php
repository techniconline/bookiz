<?php

namespace Modules\Core\Traits\Feature;


trait FeatureGroup
{

    public function messages()
    {
        $this->messages = [
            'title.required' => trans('core::errors.featureGroup.title_required'),
            'language_id.required' => trans('core::errors.featureGroup.language_id_required'),
            'instance_id.required' => trans('core::errors.featureGroup.instance_id_required'),

        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function featureGroupRelations()
    {
        return $this->hasMany('Modules\Core\Models\Feature\FeatureGroupRelation')->orderBy('position');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance');
    }

}
