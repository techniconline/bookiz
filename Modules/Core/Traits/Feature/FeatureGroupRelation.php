<?php

namespace Modules\Core\Traits\Feature;

trait FeatureGroupRelation
{
    public function messages()
    {
        $this->messages = [
            'feature_id.required' => trans('core::errors.featureGroupRelation.feature_id_required'),
            'feature_group_id.required' => trans('core::errors.featureGroupRelation.feature_group_required'),
        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function featureGroup()
    {
        return $this->belongsTo('Modules\Core\Models\Feature\FeatureGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feature()
    {
        return $this->belongsTo('Modules\Core\Models\Feature\Feature');
    }

}
