<?php

namespace Modules\Core\Traits\Location;


trait Location
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('Modules\Core\Models\Location\Location', 'country_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state()
    {
        return $this->belongsTo('Modules\Core\Models\Location\Location', 'state_id');
    }

}
