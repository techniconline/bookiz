<?php

namespace Modules\Core\Traits;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;

trait BaseTrait
{

    use ModelTrait;

    private $field_types = [
        "string" => ["varchar", "char", "text", "tinytext", "mediumtext", "longtext"],
        "integer" => ["int", "bigint", "tinyint", "smallint", "mediumint"],
        "numeric" => ["float", "decimal", "double", "real"],
        "date" => ["date", "datetime", "timestamp"],
        "date_format:H:i:s" => ["time"],
    ];

    /**
     * @param bool $byModelRule
     * @return $this
     */
    public function setRules($byModelRule = false)
    {
        $this->rules = $this->getRules($byModelRule);
        return $this;
    }

    /**
     * @return mixed
     */
    private function getColumnsTable()
    {
        return DB::select(DB::raw('SHOW COLUMNS FROM ' . $this->getTable() . ''));
    }

    /**
     * @param bool $byModelRule
     * @return array
     */
    private function getRules($byModelRule = false)
    {
        $rule_cache_key = md5(json_encode($this->getFillable()));
        if (Cache::has($rule_cache_key)) {
            $rules = Cache::get($rule_cache_key);
        } else {
            $table_info_columns = $this->getColumnsTable();
            $rules = $this->generateRulesTable($table_info_columns, $byModelRule);
        }
        return $rules;
    }

    /**
     * @param $table_info_columns
     * @param $byModelRule
     * @return array
     */
    private function generateRulesTable($table_info_columns, $byModelRule)
    {
        $rulesValue = $rules = [];
        foreach ($table_info_columns as $column) {
            if ($column->Extra == "auto_increment") {
                continue;
            }

            $type_column = $this->getTypeColumn($column->Type);
            $type = $this->getTypeCol($type_column["type"]);
            $rulesValue[] = [
                "name" => $column->Field
                , "type" => $type
                , "max" => (int)$type_column["max"]
                , "type_column" => $type_column
                , "key" => $column->Key
                , "is_unique" => $column->Key == "UNI" ? true : false
                , "default" => $column->Default
                , "has_default" => is_null($column->Default) ? false : true
                , "required" => $column->Null == "YES" ? false : true
            ];
        }

        $modelRules = $this->rules;

        $byModelRule = true;
        if (empty($modelRules)) {
            $byModelRule = false;
        }

        foreach ($rulesValue as $index => $item) {
            if ($byModelRule && isset($modelRules[$item["name"]])) {
                $modelRule = $modelRules[$item["name"]];
                $modelRuleArr = explode("|", $modelRule);
                $rules[$item["name"]] = (array_search("required", $modelRuleArr) !== false) ? "required" : "nullable";
            }

            //TODO comment
            $rules[$item["name"]] = (isset($rules[$item["name"]])) ? $rules[$item["name"]] : (($item["required"] && !$item["has_default"]) ? "required" : "nullable");
            $rules[$item["name"]] = $rules[$item["name"]] . "|";
            $rules[$item["name"]] .= $item["type"] . "|";
            $rules[$item["name"]] .= $item["is_unique"] ? "unique:" . $this->getTable() . "," . $item["name"] . "|" : "";
            if (in_array($item["type"], ["numeric"]) && $item["max"]) {
                $rules[$item["name"]] .= 'regex:/^-?[0-9]{1,' . $item["max"] . '}+(?:\.[0-9]{1,2})?$/';
            } else {
                $rules[$item["name"]] .= $item["max"] ? (in_array($item["type"], ["integer"]) ? "digits_between:1," . $item["max"] : "max:" . $item["max"]) : "";
            }
            $ruleArr = array_filter(explode("|", $rules[$item["name"]]));
            $rules[$item["name"]] = implode("|", $ruleArr);
        }
//        if($this->getTable()=='rates'){
//            dd($rules);
//        }
        $rule_cache_key = md5(json_encode($this->getFillable()));
        Cache::forever($rule_cache_key, $rules);
        return $rules;
    }

    /**
     * @param $typeFieldTableInDB
     * @return array
     */
    private function getTypeColumn($typeFieldTableInDB)
    {
        $typeAttrArr = array_values(array_filter(explode(" ", $typeFieldTableInDB)));
        $attribute = count($typeAttrArr) > 1 ? last($typeAttrArr) : null;
        $typeArr = array_values(array_filter(explode("(", array_first($typeAttrArr))));
        $max = null;
        if (count($typeArr) == 1) {
            //is with out max char for example text
            $type = array_first($typeArr);
        } else {
            $type = array_first($typeArr);
            unset($typeArr[0]);
            $max = str_replace(")", "", last($typeArr));
        }
        return ["type" => $type, "max" => $max, "attribute" => $attribute];
    }


    /**
     * @param $type
     * @return string
     */
    private function getTypeCol($type)
    {
        $types = collect($this->field_types);
        $typeValidation = $types->filter(function ($items) use ($type) {
            if (in_array($type, $items)) {
                return true;
            }
        })->keys()->first();
        return $typeValidation ?: "string";
    }

    /**
     * @param $value for searchable
     * @param array $enum is array of enums
     * @param string $type : key or value
     * @return false|int|mixed|null|string
     */
    public function searchEnumItem($value, array $enum, $type = 'key')
    {

        if ($type == 'key' && isset($enum[$value])) {
            return $enum[$value];
        }

        if ($type == 'value' && $value = array_search($value, $enum)) {
            return $value;
        }

        return null;

    }

    /**
     * @param $requestItems
     * @return $this
     */
    public function setRequestItems($requestItems)
    {
        if (is_array($this->requestItems)) {
            $this->requestItems = array_merge($this->requestItems, $requestItems);
        } else {
            $this->requestItems = $requestItems ?: request()->all();
        }
        return $this;
    }

    /**
     * @param $key
     * @return null
     */
    public function getRequestItem($key = null, $default = null)
    {
        if (is_null($key)) {
            return $this->requestItems;
        }
        return isset($this->requestItems[$key]) ? $this->requestItems[$key] : $default;
    }

    /**
     * @return mixed|null
     */
    public function getParamsJsonAttribute()
    {
        if ($params = $this->params) {
            return json_decode($params);
        }
        return null;
    }

    /**
     * @return mixed|null
     */
    public function getParamsArrayAttribute()
    {
        if ($params = $this->params) {
            return json_decode($params, true);
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getCreatedDateAttribute()
    {
        $value = isset($this->created_at) ? $this->created_at : null;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.medium'));
        }
        return $value;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDateAttribute()
    {
        $value = isset($this->updated_at) ? $this->updated_at : null;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.medium'));
        }
        return $value;
    }

    /**
     * @param $value
     * @return bool|string
     */
    public function getCreatedAtAttribute($value)
    {
        if ($this->convertCreatedAtToJalali) {
            $value = getTimestampToJalali(strtotime($value), "Y-m-d H:i:s");
        }
        return $value;
    }

    /**
     * @param $value
     * @return bool|string
     */
    public function getUpdatedAtAttribute($value)
    {
        if ($this->convertUpdatedAtToJalali) {
            $value = getTimestampToJalali(strtotime($value), "Y-m-d H:i:s");
        }
        return $value;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {

        $fillable = $this->getFillable();
        if (in_array('active', $fillable)) {
            $query->where($this->getTable() . '.active', 1);
        }

        if (in_array('deleted_at', $fillable)) {
            $query->whereNull($this->getTable() . '.deleted_at');
        }
        return $query;
    }

    /**
     * @param $query
     * @param string $key
     * @param null $value
     * @param string $format_date
     * @param string $condition
     * @return mixed
     */
    public function scopeFilterDate($query, $key = 'created_at', $value = null, $format_date = 'Y-m-d H:i:s', $condition = '=')
    {
        $fillable = $this->getFillable();
        $value = $value ?: $this->getRequestItem($key);
        if (in_array($key, $fillable) && $value) {
            $value = DateHelper::setLocaleDateTime($value, $format_date)->getDateTime($format_date);
            $query->where($key, $condition, $value);
        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterUser($query)
    {
        $fillable = $this->getFillable();
        $user_id = $this->getRequestItem('user_id');
        if (!$user_id) {
            $user = BridgeHelper::getAccess()->getUser();
            $user_id = $user ? $user->id : 0;
        }
        if ($user_id && in_array('user_id', $fillable)) {
            $query->where($this->getTable() . '.user_id', $user_id);
        }

        return $query;
    }


    /**
     * @param $query
     * @return mixed
     */
    public function scopePublished($query)
    {
        $fillable = $this->getFillable();
        if (in_array('published', $fillable)) {
            return $query->where('published', 1);
        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByUser($query)
    {
        $fillebale = $this->getFillable();
        if (in_array('user_id', $fillebale)) {
            $query->where(function ($q) {

                if ($user_id = $this->getRequestItem('user_id')) {
                    $q->where('user_id', $user_id);
                }

            });
        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByModel($query)
    {
        $fillebale = $this->getFillable();
        if (in_array('model_type', $fillebale) && in_array('model_id', $fillebale)) {
            $query->where(function ($q) {
                if (($model_type = $this->getRequestItem('model_type')) && ($model_id = $this->getRequestItem('model_id'))) {
                    $q->where('model_type', $model_type);
                    $q->where('model_id', $model_id);
                }

            });
        }
        return $query;
    }


    /**
     * @param $query
     * @param array $relations
     * @return mixed
     */
    public function scopeFilterRelations($query, $relations = [])
    {
        foreach ($relations as $relation) {
            $query = $query->whereHas($relation, function ($q) {
                return $q->active();
            });
        }
        return $query;
    }

    /**
     * @param $query
     * @param array $relations
     * @return mixed
     */
    public function scopeCreateRelationsWithFilter($query, $relations = [])
    {
        foreach ($relations as $relation) {
            $query->with([$relation => function ($q) {
                return $q->active();
            }]);
        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeEnable($query)
    {

        $fillable = $this->getFillable();
        if (in_array('active', $fillable)) {
            $query->where($this->getTable() . '.active', '>', 0);
        }

        if (in_array('deleted_at', $fillable)) {
            $query->whereNull($this->getTable() . '.deleted_at');
        }

        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsDefault($query)
    {

        $fillable = $this->getFillable();
        if (in_array('is_default', $fillable)) {
            $query->where($this->getTable() . '.is_default', 1);
        }

        if (in_array('default', $fillable)) {
            $query->where($this->getTable() . '.default', 1);
        }

        return $query->orderBy($this->getTable() . '.id', 'DESC');
    }

    /**
     * @param $query
     * @param $system
     * @return mixed
     */
    public function scopeFilterSystem($query, $system)
    {
        $fillable = $this->getFillable();
        if (in_array('system', $fillable)) {
            $query->where($this->getTable() . '.system', $system);
        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterLanguage($query)
    {
        $language_id = isset($this->requestItems['language_id']) ? $this->requestItems['language_id'] : app('getInstanceObject')->getLanguageId();
        if (in_array('language_id', $this->getFillable())) {
            $query->where($this->getTable() . '.language_id', $language_id);
        } else {
            $query = $query->whereHas('language', function ($q) use ($language_id) {
                $q->where('id', $language_id);
            });
        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterInstance($query)
    {

        $instance_id = isset($this->requestItems['instance_id']) ? $this->requestItems['instance_id'] : app('getInstanceObject')->getCurrentInstanceId();
        if (in_array('instance_id', $this->getFillable())) {
            $query->where($this->getTable() . '.instance_id', $instance_id);
        } else {
            $query = $query->whereHas('instance', function ($q) use ($instance_id) {
                $q->where('id', $instance_id);
            });
        }
        return $query;
    }

    /**
     * @param $query
     * @param null $instance_id
     * @return mixed
     */
    public function scopeFilterCurrentInstance($query, $instance_id = null)
    {

        $instance_id = $instance_id ?: app('getInstanceObject')->getCurrentInstanceId();
        if (in_array('instance_id', $this->getFillable())) {
            $query->where($this->getTable() . '.instance_id', $instance_id);
        } else {
            $query = $query->whereHas('instance', function ($q) use ($instance_id) {
                $q->where('id', $instance_id);
            });
        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilters($query)
    {
        $rFilter = $this->requestItems;
        if (empty($rFilter)) {
            return $query;
        }

        $fillable = $this->getFillable();

        foreach ($rFilter as $key => $value) {

            if (is_null($value) || !in_array($key, $fillable)) {
                continue;
            }

            if (is_array($value)) {
                $query->whereIn($this->getTable() . '.' . $key, $value);
            } elseif ((int)$value && is_int((int)$value)) {
                $query->where($this->getTable() . '.' . $key, $value);
            } elseif (is_string($value)) {
                $query->where($this->getTable() . '.' . $key, 'LIKE', '%' . $value . '%');
            }
        }
        return $query;
    }

    /**
     * @param null $attributes
     * @param null $rules
     * @param null $messages
     * @return bool
     */
    public function isValid($attributes = null, $rules = null, $messages = null)
    {
        $validation = Validator::make($attributes ? $attributes : $this->attributes
            , $rules ? $rules : isset($this->rules) ? $this->rules : []
            , $messages ? $messages : isset($this->messages) ? $this->messages : []);

        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function fill(array $attributes)
    {
        $newAttrs = [];
        foreach ($attributes as $key => $attribute) {
            if (!is_null($attribute)) {
                $newAttrs[$key] = $attribute;
            }
        }
        return parent::fill($newAttrs);
    }

    /**
     * @param array $option
     * @return mixed
     */
    public function get(array $option = ['*'])
    {
        if ($key = $this->key_cache_all) {
            if (Cache::has($key)) {
                return Cache::get($key);
            } else {
                $duration = $this->duration_cache_all;
                $value = parent::get($option);
                if (!$duration) {
                    Cache::forever($key, $value);
                } else {
                    Cache::put($key, $value, $duration);
                }
                return $value;
            }

        } else {
            return parent::get($option);
        }
    }

    /**
     * @return mixed
     */
    public function first()
    {
        $value = null;
        if ($key = $this->key_cache_single) {
            if (Cache::has($key)) {
                $value = Cache::get($key);
            } else {
                $duration = $this->duration_cache_single;
                $value = parent::first();
                if (!$duration) {
                    Cache::forever($key, $value);
                } else {
                    Cache::put($key, $value, $duration);
                }
            }

        } else {
//            $value = parent::get($this->api_fields?$this->api_fields:'*')->first();
            $value = parent::first();
        }
        return $value;
    }

    /**
     * @param array $option
     * @return mixed
     */
    public function save(array $option = [])
    {
        if ($saved = parent::save($option)) {
            $value = $this->find($this->id);
            if ($this->create_cache) {
                if ($key = $this->key_cache_single) {
                    //remove cache
                    if (Cache::has($key)) {
                        Cache::forget($key);
                    }
                } else {
                    $key = $this->getTable() . '_' . $this->id;
                }

                //create cache
                if ($this->duration_cache_single) {
                    Cache::put($key, $value, $this->duration_cache_single);
                } else {
                    Cache::forever($key, $value);
                }
            }
        }
        return $saved;
    }

    /**
     * @return $this
     */
    public function setApiAttributes()
    {
        $isApi = app('getInstanceObject')->isApi();
        if (!$isApi) {
            return $this;
        }

        if (empty($this->api_fields) && empty($this->api_append_fields)) {
            return $this;
        }

        $hidden_fields = collect($this->getFillable())->filter(function ($item) {
            return !in_array($item, $this->api_fields);
        })->toArray();
        $this->addHidden($hidden_fields);

        if (!empty($this->api_append_fields)) {
            $this->append($this->api_append_fields);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function isList()
    {
        if (empty($this->list_fields) && empty($this->list_append_fields)) {
            return $this;
        }

        $hidden_fields = collect($this->getFillable())->filter(function ($item) {
            return !in_array($item, $this->list_fields);
        })->toArray();
        $this->addHidden($hidden_fields);

        if (!empty($this->list_append_fields)) {
            $this->append($this->list_append_fields);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getListFields()
    {
        return $this->list_fields;
    }

    /**
     * @return mixed
     */
    public function getAppendListFields()
    {
        return $this->list_append_fields;
    }

    /**
     * @param $key
     * @return array
     */
    public function setAppendListFields($key)
    {
        $this->list_append_fields = array_merge([$key], $this->list_append_fields);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApiFields()
    {
        return $this->api_fields;
    }

    /**
     * @return mixed
     */
    public function getApiAppendFields()
    {
        return $this->api_append_fields;
    }

    /**
     * @param $key
     * @return $this
     */
    public function setKeyCacheSingle($key)
    {
        $this->key_cache_single = $key;
        return $this;
    }

    /**
     * @param $key
     * @return $this
     */
    public function setKeyCacheAll($key)
    {
        $this->key_cache_all = $key;
        return $this;
    }


    /**
     * @return mixed
     */
    public function scopeSoftDelete($query)
    {
        $fillable = $this->getFillable();
        if (in_array('deleted_at', $fillable)) {
            $items['deleted_at'] = date("Y-m-d H:i:s");
        }

        if (in_array('active', $fillable)) {
            $items['active'] = 0;
        }

        return $query->update($items);
    }

}
