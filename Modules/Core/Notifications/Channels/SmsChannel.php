<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 7/22/18
 * Time: 4:21 PM
 */

namespace Modules\Core\Notifications\Channels;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class SmsChannel
{
    private $provider_name='Kavenegar';
    private $api_key='514262464A375552466F7261704A67546A34413975773D3D';
    private $api_password='';
    private $api_number='10004400444000';
    private $provider=false;

    public function boot(){
        $sender="Modules\\Core\\Notifications\\Channels\\Provider\\".$this->provider_name."\\".$this->provider_name;
        $this->provider = new $sender($this->api_key);
    }
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $this->boot();
        $message=$notification->toSms($notifiable);
        if($this->provider->canSendByTemplate() && isset($message['token1'],$message['template'])){
            $token1=$message['token1'];
            $token2=isset($message['token2'])?$message['token2']:null;
            $token3=isset($message['token3'])?$message['token3']:null;
            return $this->provider->VerifySms($message['to'], $message['template'],$token1,$token2,$token3);
        }else{
            return $this->provider->send($message['to'],$this->api_number,$message['message']);
        }
    }


}