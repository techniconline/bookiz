<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 7/22/18
 * Time: 4:46 PM
 */
namespace Modules\Core\Notifications\Channels\Sms;


class SmsMessage
{
    private $content='';
    private $type='';
    private $to='';
    private $custom=[];

    public function setContent($content){
        $this->content=$content;
        return $this;
    }

    public function setType($type){
        $this->type=$type;
        return $this;
    }

    public function setTo($to){
        $this->to=$to;
        return $this;
    }


    public function setCustomFields(Array $custom){
        $this->custom=$custom;
        return $this;
    }


    public function getMessage(){
        $message=[
            'message'=>$this->content,
            'type'=>$this->type,
            'to'=>$this->to,
        ];
        return array_merge($message,$this->custom);
    }
}