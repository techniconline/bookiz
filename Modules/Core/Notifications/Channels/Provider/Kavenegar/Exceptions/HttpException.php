<?php
namespace Modules\Core\Notifications\Channels\Provider\Kavenegar\Exceptions;


class HttpException extends BaseRuntimeException
{
	public function getName()
    {
        return 'HttpException';
    }	
}
