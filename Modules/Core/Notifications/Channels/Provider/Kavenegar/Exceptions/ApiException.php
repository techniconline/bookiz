<?php
namespace Modules\Core\Notifications\Channels\Provider\Kavenegar\Exceptions;

class ApiException extends BaseRuntimeException
{
	public function getName()
    {
        return 'ApiException';
    }
}

