<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SitemapGenerator extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    protected $sitemapNames = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get table rows.
     *
     * @return array
     */

    public function getModules()
    {
        return $this->laravel['modules']->getByStatus(1);
    }

    public function getAllSitemapClass()
    {
        $allFiles = [];

        foreach ($this->getModules() as $module) {
            if (File::exists($module->getPath() . "/ViewModels/Sitemap")) {
                foreach (File::files($module->getPath() . "/ViewModels/Sitemap") as $value) {
                    $fileWithpath = $this->generateNamespace($module) . pathinfo($value)['basename'];
                    $allFiles[] = $fileWithpath;
                }
            }
        }

        return $allFiles;
    }

    public function handle()
    {
        $returnData = [];

        Storage::makeDirectory('sitemap', 0777);
        foreach ($this->getAllSitemapClass() as $class) {
            $class = str_replace('.php', '', $class);
            $returnData[] = app()->call("$class@createSitemap");
        }
        // Generate sitemap
        app()->call("\Modules\Core\ViewModels\MultiSitemap@generateSitemap", [$returnData]);
        // Delete sitemap path from public if exists
        $this->deletePath('/sitemap');
        // Create shortcut for sitemap from sorage path to public path
        $this->createShortcut('app/public/sitemap', '');

        return true;
    }

    /**
     * @param $module
     * @return string
     */
    public function generateNamespace($module): string
    {
        return "\\Modules\\" . $module->getName() . "\\ViewModels\\Sitemap\\";
    }

    public function deletePath($address)
    {
      return File::exists(public_path($address)) ? File::delete(public_path($address)) : '';
    }

    public function createShortcut($storagePath, $publicPath)
    {
        $storagePath = storage_path($storagePath);
        $publicPath = public_path($publicPath);
        shell_exec(" ln -s $storagePath $publicPath");
    }
}
