<?php

namespace Modules\Core\ViewModels\Dashboard;

use Modules\Core\ViewModels\BaseViewModel;

class IndexViewModel extends BaseViewModel
{
    public function getHomePage(){
        $this->setTitlePage(trans('core::form.titles.index'));
        return $this->setResponse(true,'');
    }
}
