<?php

namespace Modules\Core\ViewModels\Dashboard;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Payment;
use Modules\Sale\Models\Traits\Payment\OrderStatesTrait;

class DashboardViewModel extends BaseViewModel
{
    use GridViewModel;
    use OrderStatesTrait;
    private $chartjs_assets;


    /**
     * @return $this
     */
    public function renderChartBlock()
    {
        $block_name = $this->request->get('block_name');
        $widget = BridgeHelper::getBlockHelper()->getBlock($block_name);
        if ($widget && method_exists($widget, "renderJson") && get_instance()->isAjax()) {
            $data = $widget->renderJson();
            return $this->setDataResponse($data, 'chart_view')->setResponse(true);
        }
        return $this->setResponse(false);
    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->Model = Order\OrderItems::whereNotNull(DB::raw('(select id from orders where id=order_id and state in ("'.$this->getOrderCompleteState().'") and instance_id=' . get_instance()->getCurrentInstanceId() . ')'))
            ->select(['item_type', 'item_id', DB::raw('sum(qty) as total_qty'), DB::raw('sum(amount) as total_amount')
                , DB::raw('sum(discount_amount) as total_discount_amount'), DB::raw('count(id) as count_sale')])
            ->groupBy(['item_type', 'item_id'])
            ->orderBy('count_sale', 'DESC');
    }


    /**
     * @return $this
     */
    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        $this->sort_by = 'count_sale';
        /*add columns to grid*/
        $this->addColumn('item_type', trans('sale::sale.item_type'), false)
            ->addColumn('item_id', trans('sale::sale.item_id'), false)
            ->addColumn('course_title', trans('sale::sale.course_title'), false)
            ->addColumn('total_qty', trans('sale::sale.total_qty'), false)
            ->addColumn('total_amount', trans('sale::sale.total_amount'), false)
            ->addColumn('total_discount_amount', trans('sale::sale.total_discount_amount'), false)
            ->addColumn('count_sale', trans('sale::sale.count_sale'), false);

        //chart --------------------------------
        $this->can_chart = true;
        $this->setNameChart("sample_chart");
        $this->setXField("course_title");
        $this->setYField("total_qty", trans('sale::sale.total_qty'));
        $this->setYField('total_amount', trans('sale::sale.total_amount'));
        $this->setYField('total_discount_amount', trans('sale::sale.total_discount_amount'));
        $this->setYField('total_amount', trans('sale::sale.total_amount'));
        $this->setChartSize(["width"=>600, "height"=>400]);
//
//        $optionsChart['scales'] = [
//            'yAxes'=>[['display'=>true,'ticks'=>['beginAtZero'=>true,'steps'=>10,'stepValue'=>5,'max'=>200]]]
//        ];
//        $this->setChartOptions($optionsChart);
        //chart --------------------------------

        $this->addFilter('item_type', 'text');
        $this->addFilter('item_id', 'text');

        return $this;
    }

    public function getRowsUpdate($row)
    {
        $row->course_title = isset($row->item_row->title) ? $row->item_row->title : '-';
//        $row->gateway = isset($row->options->payment) ? $row->options->payment : "-";
//        $row->reference_id = $row->reference_id ? ltrim($row->reference_id, 0) : "-";
//        $row->request_id = $row->request_id ? ltrim($row->request_id, 0) : "-";
//        $order = $row->getActionModel();
//        $row->action_id = $row->action && $row->action_id && $order ? '<a target="_blank" href="' . route('sale.order.index', ['filter' => 1, 'reference_code' => $order->reference_code]) . '">' . $row->action_id . '</a>' : $row->action_id;
//        $row->state = $row->status_text;
//        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
//        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        return $this->generateGridList()->renderedView("sale::payment.index", ['view_model' => $this], "payment_list");
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $chartjs_assets = [];
        if ($this->chartjs_assets) {
            $chartjs_assets [] = ['container' => 'general_style', 'src' => ("css/loading.css"), 'name' => 'loading'];
            $chartjs_assets [] = ['container' => 'plugin_general_2', 'src' => ("/plugins/chartjs/Chart.bundle.min.js"), 'name' => 'Chartjs.bundle'];
            $chartjs_assets [] = ['container' => 'plugin_general_2', 'src' => ("/plugins/chartjs/Chart.min.js"), 'name' => 'Chartjs'];
        }

        return $chartjs_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function generateChart()
    {
        $this->setTitleChart(trans('core::form.titles.dashboard'));
        $this->setNameChart('barayetest');
        $this->setChartSize(["width" => 200, "height" => 100]);
        $this->setChartType("bar");
        $this->filter_models = true;

        $model = Order::active()->filterCurrentInstance();
        $this->setChartModel("orders", "order list", $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);
//        $this->setChartModel("orders", "order list", $model, ["x" => ["field_name" => "created_time", "selected" => "DATE_FORMAT(created_at, '%Y-%m-%d') as created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);

        $model = Payment::active()->filterCurrentInstance();
        $this->setChartModel("payments", "payment list", $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);
//        $this->setChartModel("payments", "payment list", $model, ["x" => ["field_name" => "created_time", "selected" => "DATE_FORMAT(created_at, '%Y-%m-%d') as created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);

        $model = Payment::active()->whereIn("state", ["success", "complete"])->filterCurrentInstance();
        $this->setChartModel("payments_success", "payment success list", $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);

        $model = Payment::active()->whereIn("state", ["success", "complete"])->where("bank", "zarinpal")->filterCurrentInstance();
        $this->setChartModel("payments_success_zarin", "payment success zarinpal list", $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);

        $model = Payment::active()->whereIn("state", ["success", "complete"])->where("bank", "asan")->filterCurrentInstance();
        $this->setChartModel("payments_success_asan", "payment success asan list", $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);

        $model = Payment::active()->whereNotIn("state", ["success", "complete"])->filterCurrentInstance();
        $this->setChartModel("payments_fail", "payment fail list", $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);

        $this->setGroupByItems("full_date", ["type" => "date", "format" => "Y-m-d", "value" => 'DATE_FORMAT(created_at, "%Y-%m-%d")', "field_name" => "created_time", "title" => trans("core::chart.full_date")]);
        $this->setGroupByItems("by_month", ["type" => "date", "format" => "Y-m", "value" => 'DATE_FORMAT(created_at, "%Y-%m")', "field_name" => "created_time", "title" => trans("core::chart.by_month")]);
        $this->setGroupByItems("by_hours", ["type" => "date", "format" => "Y-m-d H", "value" => 'DATE_FORMAT(created_at, "%Y-%m-%d %H")', "field_name" => "created_time", "title" => trans("core::chart.by_hours")]);

        $this->setFilter("created_at", "date", ["title_to" => trans("core::chart.title_to"), "title_from" => trans("core::chart.title_from")]);
        $this->setFilterTypeChart();
        $this->initChart();

        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getDashboard()
    {
        $this->setTitlePage(trans('core::form.titles.dashboard'));
//        return $this->getViewListIndex();
        return $this->setResponse(true, '');
        $this->chartjs_assets = true;
        if (get_instance()->isAjax()) {
            $json = $this->generateChart()->getChart();
            return $this->setDataResponse($json, "chart_view")->setResponse(true);
        }
        return $this->generateChart()->renderedView("core::dashboard.index", ['view_model' => $this], "dashboard");
    }

    public function createChart()
    {

        $orders = Order::active()
            ->where("created_at", ">=", "2018-12-30 00:00:00")
            ->where("created_at", "<=", "2019-01-10 00:00:00")
            ->groupBy(DB::raw("DATE(created_at)"))
            ->get([DB::raw("DATE(created_at) as created_at"), DB::raw("count(id) as count")]);
        $orders = $orders->pluck("count", "created_at");
//        dd($orders->keys());

        return app()->chartjs
            ->name('lineChartTest')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels(['January', 'February', 'March', 'April', 'May', 'June', 'July'])
            ->datasets([
                [
                    "label" => "My First dataset",
                    'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => [65, 59, 80, 81, 56, 55, 40],
                ],
                [
                    "label" => "My Second dataset",
                    'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => [12, 33, 44, 44, 55, 23, 40],
                ]
            ])
            ->options([]);

    }

}

