<?php

namespace Modules\Core\ViewModels\Config;

use Illuminate\Support\Facades\Route;
use Modules\User\Models\Connector;
use Illuminate\Support\Facades\Cache;

class InstanceViewModel extends SettingViewModel
{

    public $setting_name='instance';
    public $module_name='core';

    public function getConfigForm(){
        $BlockViewModel=&$this;
        $this->setTitlePage(trans('core::menu.config.instance'));
        return $this->renderedView('core::config.instance',compact('BlockViewModel'));
    }


    public function getClearCache(){
        Cache::flush();
        return $this->redirectBack()->setResponse(true,trans('core::messages.alert.clear_cache'));
    }
    public function save(){
        $rules=[
            'title'=>'required',
        ];
        $files=[
            'favicon'=>'mimes:png|max:256',
            'logo'=>'mimes:jpeg,jpg,png|max:1024',
            'logo_small'=>'mimes:jpeg,jpg,png|max:1024',
            'logo_large'=>'mimes:jpeg,jpg,png|max:1024',
            'logo_other'=>'mimes:jpeg,jpg,png|max:1024',
        ];
        foreach($files as $name=>$rule){
            if($this->request->hasFile($name)){
                $rules[$name]=$rule;
            }
        }
        $this->requestValuesUpdate()->validate($rules);
        $data=$this->request->all();
        $data= $this->uploadFilesOnRequest(array_keys($files),null,$data);
        $result=$this->saveConfig($data);
        if($result){
            return $this->redirectBack()->setResponse($result,trans('core::messages.alert.save_success'));
        }
        return $this->redirectBack()->setResponse($result,trans('core::messages.alert.error'));

    }


    public function getRoutes(){
        $routeCollection = Route::getRoutes();
        $list=[];
        foreach ($routeCollection as $value) {
            if($value->getName()=='index'){
                continue;
            }
            $list[$value->getName()]=$value->getActionName();
        }
        return $list;
    }
}
