<?php
namespace Modules\Core\ViewModels\Config;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\Traits\Config\Settings;
use Illuminate\Support\Facades\Request;

class SettingViewModel extends BaseViewModel
{
    use Settings;

}
