<?php

namespace Modules\Core\ViewModels\Config;

use Modules\Core\ViewModels\BaseViewModel;

class ApiConfigViewModel extends BaseViewModel
{

    protected function getSplashData()
    {
        $subDomain = $subDomainOrg = app("getInstanceObject")->getCurrentSubDomain();
        $currentInstance = app("getInstanceObject")->getCurrentInstance();

        if ($subDomainOrg != $currentInstance->name) {
            $subDomain = $currentInstance->name;
        }

        $data = [
            "pages" => [
                "contact_us_url" => app("getInstanceObject")->getCurrentUrl(url("core/page/show/ارتباط-با-ما", [], true)),
                "about_us_url" => app("getInstanceObject")->getCurrentUrl(url("core/page/show/درباره-ما", [], true)),
                "faq_url" => app("getInstanceObject")->getCurrentUrl(url("core/page/show/سوالات-متداول", [], true)),
                "privacy_url" => app("getInstanceObject")->getCurrentUrl(url("/core/page/show/rules", [], true)),
            ],
            "instance" => $this->decorateAttributes($currentInstance),
            "android" => [
                "force_version" => ["ver" => config("mobile.splash.ANDROID_FORCE_VER", "1.0.0"), "url" => app("getInstanceObject")->getCurrentUrl(url("core/page/preview/application"))],
                "last_version" => ["ver" => config("mobile.splash.ANDROID_LAST_VER", "1.0.0"), "url" => app("getInstanceObject")->getCurrentUrl(url("core/page/preview/application"))],
            ],
            "ios" => [
                "force_version" => ["ver" => config("mobile.splash.IOS_FORCE_VER", "1.0.0"), "url" => app("getInstanceObject")->getCurrentUrl(url("core/page/preview/application"))],
                "last_version" => ["ver" => config("mobile.splash.IOS_LAST_VER", "1.0.0"), "url" => app("getInstanceObject")->getCurrentUrl(url("core/page/preview/application"))],
            ],
            "config" => [
                "login" => ["fields" => [
                    "connector" => [
                        "required" => true,
                        "hint" => trans('core::data.splash.hint.connector'),
                        "role" => ["mobile", "email"
                        ]
                    ], "password" => ["required" => true]]],
                "register" => ["fields" => [
                    "first_name" => ["required" => true, "hint" => trans('core::data.splash.hint.first_name')]
                    , "last_name" => ["required" => true, "hint" => trans('core::data.splash.hint.last_name')]
                    , "connector" => ["required" => true, "hint" => trans('core::data.splash.hint.connector'), "role" => ["mobile", "email"]]
                    , "password" => ["required" => true, "hint" => trans('core::data.splash.hint.password')]
                    , "password_confirmation" => ["required" => true, "hint" => trans('core::data.splash.hint.password_confirmation')]
                ]],
            ],
            "base_url" => config("mobile.BASE_URL", "https://api.farayad.org"),
            "support" => [
                'telephone' => config("mobile.splash.SUPPORT_NUMBER", "02188176609"),
                'text' => trans('core::data.splash.support_text'),
                'chat_link' => config("mobile.splash.SUPPORT_CHAT_LINK"),
            ],
            "app_menus" => [
                ['title' => trans('core::data.splash.menu.edit'), 'slug' => 'edit', 'sort' => 0, 'enable' => true, 'login' => true, 'show_in_login' => true, 'options' => ['icon' => 'fa fa-user-edit']],
                ['title' => trans('core::data.splash.menu.basket'), 'slug' => 'basket', 'sort' => 0, 'enable' => true, 'login' => true, 'show_in_login' => true, 'options' => ['icon' => 'fa fa-shopping-cart']],
                ['title' => trans('core::data.splash.menu.fav'), 'slug' => 'fav', 'sort' => 0, 'enable' => false, 'login' => true, 'show_in_login' => true, 'options' => ['icon' => 'fa fa-heart']],
                ['title' => trans('core::data.splash.menu.order'), 'slug' => 'order', 'sort' => 0, 'enable' => true, 'login' => true, 'show_in_login' => true, 'options' => ['icon' => 'fa fa-clipboard-list']],
                ['title' => trans('core::data.splash.menu.questions'), 'slug' => 'questions', 'sort' => 0, 'enable' => true, 'login' => true, 'show_in_login' => true, 'options' => ['icon' => 'fa fa-question-circle']],
                ['title' => trans('core::data.splash.menu.qr'), 'slug' => 'qr', 'sort' => 0, 'enable' => true, 'login' => false, 'show_in_login' => false, 'options' => ['icon' => 'fa fa-qrcode']],
                ['title' => trans('core::data.splash.menu.contact'), 'slug' => 'contact', 'sort' => 0, 'enable' => true, 'login' => false, 'show_in_login' => true, 'options' => ['icon' => 'fa fa-phone']],
                ['title' => trans('core::data.splash.menu.about'), 'slug' => 'about', 'sort' => 0, 'enable' => true, 'login' => false, 'show_in_login' => true, 'options' => ['icon' => 'fa fa-info-circle']],
                ['title' => trans('core::data.splash.menu.setting'), 'slug' => 'setting', 'sort' => 0, 'enable' => true, 'login' => false, 'show_in_login' => true, 'options' => ['icon' => 'fa fa-wrench']],
                ['title' => trans('core::data.splash.menu.support'), 'slug' => 'support', 'sort' => 0, 'enable' => true, 'login' => false, 'show_in_login' => true, 'options' => ['icon' => 'fa fa-phone-square']],
                ['title' => trans('core::data.splash.menu.exit'), 'slug' => 'exit', 'sort' => 0, 'enable' => true, 'login' => true, 'show_in_login' => true, 'options' => ['icon' => 'fa fa-sign-out-alt']],
            ],
        ];

        return $this->setDataResponse($data)->setResponse(true);
    }


}
