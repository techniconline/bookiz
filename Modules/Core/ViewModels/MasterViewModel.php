<?php

namespace Modules\Core\ViewModels;

use Illuminate\Support\Facades\Route;
use BridgeHelper;
use Modules\Core\ViewModels\Traits\ConfigThemeTrait;
use Modules\Core\ViewModels\Traits\MenuTrait;
use Modules\Core\ViewModels\Traits\RequestTrait;
use Modules\Core\ViewModels\Traits\ThemeTrait;
use Modules\Core\ViewModels\Traits\WidgetTrait;

class MasterViewModel
{

    use ThemeTrait;
    use WidgetTrait;
    use ConfigThemeTrait;
    use RequestTrait;
    use MenuTrait;

    private $view_model;
    private $module_name;
    private $access;
    private $response;
    private $action_method = 'render'; //default method
    private $result;
    private $route_name;


    /**
     * @param $viewModel
     * @return $this
     */
    public function setViewModel($viewModel)
    {
        $this->view_model = $viewModel;
        return $this;
    }

    /**
     * @param $moduleName
     * @return $this
     */
    public function setModuleName($moduleName = null)
    {
        $this->module_name = $moduleName ?: app('getInstanceObject')->getModuleName();
        return $this;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function response()
    {
        $this->setInstance()->setModuleName($this->module_name)
            ->setActionMethod($this->action_method)
            ->setConfig($this->module_name)
            ->setMenuBackend();
        $viewModel = $this->getViewModelNamespace();
        $viewModel = new $viewModel();
        $this->checkAccess($viewModel->getAccess())->setRouteName();
        BridgeHelper::getAccess()->canAccessRoute($this->route_name);

        $this->result = $viewModel->setMethod($this->action_method)->setConfig($this->config)->setRequest($this->request)->response();

        if($this->result instanceof \Symfony\Component\HttpFoundation\Response || $this->result instanceof \Illuminate\Contracts\Routing\ResponseFactory){
            return $this->result;
        }else{
            if (isset($this->result['route_widgets'])) {
                $this->route_widgets = collect((array)$this->result['route_widgets']);
            }

            if ((!$this->isAjaxRequest() && !$this->result["has_redirect"]) || (app('getInstanceObject')->isApi() && !isset($this->result["data"]))) {
                $this->getRouteWidgets();
                if (!app('getInstanceObject')->isApi()) {
                    $this->getInstanceSpecialWidgets()->setBackendMenuResult();
                }
            }

            return $this->setResponse();
        }
    }


    /**
     * @return string
     */
    private function getViewModelNamespace()
    {
        $namespaces = explode('.', $this->view_model);
        $viewModel = "\\Modules\\" . ucfirst($this->module_name) . "\\ViewModels";
        if (!isset($namespaces[1]) && isset($namespaces[0])) {
            $namespaces[1] = $namespaces[0];
        }
        foreach ($namespaces as $namespace) {
            $viewModel .= "\\" . get_uppercase_by_underscores($namespace);
        }
        $viewModel .= "ViewModel";
        return $viewModel;
    }

    /**
     * @return bool
     */
    private function isAjaxRequest()
    {
        if (request()->ajax() || request()->is_ajax) {
            return true;
        } elseif (app('getInstanceObject')->isAjax()) {
            return true;
        } else {
            return false;
        }
    }

    /**use Illuminate\Http\Request;
     * set Route Name in attribute
     * @return $this
     */
    private function setRouteName()
    {
        $this->route_name = Route::getCurrentRoute()->getName();
        return $this;
    }

    /**
     * @return $this
     */
    private function setResultApi()
    {
        if (app('getInstanceObject')->isApi()) {
            $notValidKeys = ['theme', 'assets', 'layout'];
            $this->result = collect($this->result)->reject(function ($item, $key) use ($notValidKeys) {
                if (in_array($key, $notValidKeys)) {
                    return true;
                }
            })->toArray();
        }
        return $this;
    }

    /**
     * @return $this|mixed|redirect|render view
     */
    private function setResponse()
    {
        if (key_exists('file_download', $this->result)) {
            return $this->result['file_download'];
        }

        if ($this->setResultApi()->isAjaxRequest()) {
            return $this->result;
        } else {
            if (key_exists('redirect', $this->result)) {
                return redirect($this->result['redirect'])->withInput();
            }
            if (key_exists('redirect_back', $this->result)) {
                if (isset($this->result['message']) && isset($this->result['action'])) {
                    if ($this->result['action']) {
                        return redirect()->back()->with('success', $this->result['message'])->withInput($this->result['with_input_redirect']);
                    } else {
                        return redirect()->back()->with('error', $this->result['message'])->withInput($this->result['with_input_redirect']);
                    }
                }
                return redirect()->back();
            }
            return $this->setTheme()->setLayout()->setData($this->result)->setTitle()->setAssets()->render();
        }
    }

    /**
     * @param $method
     * @return $this
     */
    public function setActionMethod($method = null)
    {
        $this->action_method = $method ?: app('getInstanceObject')->getMethodName();
        return $this;
    }

    /**
     * @param $permission
     * @return $this
     */
    private function checkAccess($permission)
    {
        if(is_null($permission)){
            return $this;
        }
        BridgeHelper::getAccess()->canAccess($permission,true);
        return $this;
    }

}
