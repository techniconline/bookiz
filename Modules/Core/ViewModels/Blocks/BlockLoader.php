<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 6/19/18
 * Time: 4:48 PM
 */

namespace Modules\Core\ViewModels\Blocks;
use Modules\Core\Models\Block\Block;

class BlockLoader
{
    private $block=false;
    private $content='';
    private $action=false;
    private $asset=[];
    private $submitFunctions=[];
    private $error=0;
    private $state='create';

    public  function __construct($id){
        $this->block=Block::find($id);
    }

    public function getConfigContent($request){
            if(!$this->block){
                return $this->getError("not_find_block");
            }
            $className=$this->getNameClass($this->block->name,true);
            $className=str_replace('\\\\','\\',$this->block->namespace).'\\'.$className;
            $widget=new $className();
            $this->content=$widget->setBlock($this->block);
            if($this->state=='result' &&  $widget->validate($request)){
                $results=$widget->render(true);
                $this->action='set';
                $this->content=[];
                foreach($results as $result){
                    $this->content[]=view('core::block.elements.block',['result'=>$result])->render();
                }
            }else{
                $this->content=$widget->render();
                $this->asset=$widget->getAssets();
                $this->submitFunctions=$widget->getSubmitFunctions();
            }

        return $this->getResponse();
    }

    public function getValidateContent($request){
        if(!$this->block){
            return $this->getError("not_find_block");
        }
        $className=$this->block->namespace.'\\'.$this->getNameClass($this->block->name,true);
        $widget=new $className();
        $widget->setID($this->block->id);

        $this->asset=$widget->getAssets();
        $this->submitFunctions=$widget->getSubmitFunctions();
        return $this->getResponse();
    }

    public function getBlockContent(){
        if(!$this->block){
            return $this->getError("not_find_block");
        }
        return $this->getResponse();
    }

    public function getError($error,$type='error'){
        if($type==='error'){
            $this->error=1;
        }
        $this->content=view('core::block.message',[$type=>$error])->render();
        return $this->getResponse();
    }

    public function getResponse(){
        $response=[];
        $response['error']=$this->error;
        $response['action']=$this->action;
        $response['content']=$this->content;
        if(count($this->asset)){
            $response['asset']=$this->asset;
        }
        if(count($this->submitFunctions)){
            $response['submitFunc']=$this->submitFunctions;
        }
        return $response;
    }

    public function getNameClass($name,$config=false){
        $names=explode('_',$name);
        $nameClass='';
        foreach($names as $name){
            $nameClass.=ucfirst($name);
        }
        if($config){
            $nameClass.='Config';
        }
        return $nameClass;
    }


    public function setState($state){
        $this->state=$state;
        return $this;
    }

}