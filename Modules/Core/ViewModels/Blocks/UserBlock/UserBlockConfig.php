<?php

namespace Modules\Core\ViewModels\Blocks\UserBlock;


use Modules\Core\Traits\Block\MasterBlockConfig;

class UserBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='UserBlock';

    public function __construct(){
        $this->addTab('user_block',trans('core::block.user_block'));
    }


}