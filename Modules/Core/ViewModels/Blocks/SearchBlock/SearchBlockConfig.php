<?php

namespace Modules\Core\ViewModels\Blocks\SearchBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;

class SearchBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='SearchBlock';

    public function __construct(){
        $this->setDefaultName(trans('core::block.search_block'))
            ->addTab('search',trans('core::block.search_block'),'core::widgets.search.config.form');
    }

}
