<?php

namespace Modules\Core\ViewModels\Blocks\SearchBlock;


use BridgeHelper;
use Modules\Core\Traits\Block\MasterBlock;

class SearchBlock
{
    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel=& $this;
        return view('core::widgets.search.index',compact('blockViewModel'))->render();
    }

}
