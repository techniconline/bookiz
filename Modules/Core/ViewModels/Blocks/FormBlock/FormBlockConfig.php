<?php

namespace Modules\Core\ViewModels\Blocks\FormBlock;

use Modules\Core\Models\Form;
use Modules\Core\Traits\Block\MasterBlockConfig;
use Modules\Core\Models\Menu\Menu;

class FormBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='FormBlock';

    public function __construct(){
        $this->setDefaultName(trans('core::block.form_builder'))
        ->addTab('form',trans('core::block.form_builder'),'core::widgets.form.config.form');
    }

    public function getBlockRules(){
        return [
            'form_id'=>'required|exists:forms,id'
        ];
    }

    public function getFormList(){
        $forms = Form::active()->get()->pluck("title","id");
        return $forms;
    }

}
