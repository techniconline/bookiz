<?php

namespace Modules\Core\ViewModels\Blocks\FormBlock;


use BridgeHelper;
use Modules\Core\Models\Form;
use Modules\Core\Traits\Block\MasterBlock;
use Modules\Core\Traits\Feature\FormFeatureBuilder;

class FormBlock
{
    public $form;

    use MasterBlock;
    use FormFeatureBuilder;

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel =& $this;
        return view('core::widgets.form.index', compact('blockViewModel'))->render();
    }

    /**
     * @return mixed
     */
    public function getFromMetaGroups()
    {
        $this->form = $this->getFormObject();
        return $this->form->formMetaGroups()->get();
    }

    /**
     * @return mixed
     */
    public function getFormObject()
    {
        if (!$this->form) {
            $form = new Form();
            return $form->enable()->find($this->getConfig("form_id"));
        }
        return $this->form;
    }

    /**
     * @return null
     */
    public function getFeatureValues()
    {
        $form_record_id = $this->getConfig("form_record_id");
        if(!$form_record_id){
            return null;
        }
        $formData = Form\FormFeatureValueRelation::enable()->where('form_record_id', $form_record_id)->get();
        return $formData;
    }

    /**
     * @return array
     */
    public function getFeatureData()
    {
        $data = $this->getFeatureValues();
        $decorated = [];
        if($data){
            foreach ($data as $datum) {
                $decorated[$datum->item_id][$datum->feature_id][] = $datum->feature_value_id?:$datum->feature_value;
            }
        }
        return $decorated;
    }


}
