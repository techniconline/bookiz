<?php

namespace Modules\Core\ViewModels\Blocks\SliderBlock;


use Modules\Core\Traits\Block\MasterBlockConfig;

class SliderBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='sliderBlock';

    public function __construct(){
        $this->setDefaultName(trans('core::block.slider_block'));
        $this->addTab('slider_block',trans('core::block.slider_block'),'core::widgets.slider.config.form');
        $this->addTab('slider',trans('core::block.slider'),'core::widgets.slider.config.slider');
    }

    public function getBlockRules(){
        $rules=[];
        for($i=1;$i<=6;$i++){
            if(!empty($this->configs['image_url_'.$i])){
                $rules['link_type_'.$i]='required|string';
            }
        }
        return $rules;
    }

}