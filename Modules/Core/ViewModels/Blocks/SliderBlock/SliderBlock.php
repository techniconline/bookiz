<?php

namespace Modules\Core\ViewModels\Blocks\SliderBlock;

use Modules\Core\Traits\Block\MasterBlock;


class SliderBlock
{

    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel=& $this;
        return view('core::widgets.slider.index',compact('blockViewModel'))->render();
    }

    public function renderJson(){
        $sliders=[];
        for($i=1;$i<=6;$i++){
            if(!empty($this->configs['image_url_'.$i])){
                $sliders[]=(object)array(
                    'image_url'=>url($this->configs['image_url_'.$i]),
                    'link_type'=>$this->configs['link_type_'.$i],
                    'link_url'=>$this->configs['link_url_'.$i],
                );
            }
            unset($this->configs['image_url_'.$i],$this->configs['link_type_'.$i],$this->configs['link_url_'.$i]);
        }
        $this->configs['slides']=$sliders;
        $block = $this->setDataBlock()->getBlock();
        return $block;
    }

}
