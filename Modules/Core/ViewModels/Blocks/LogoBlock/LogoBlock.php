<?php

namespace Modules\Core\ViewModels\Blocks\LogoBlock;


use BridgeHelper;
use Modules\Core\Traits\Block\MasterBlock;

class LogoBlock
{
    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel=& $this;
        return view('core::widgets.logo.index',compact('blockViewModel'))->render();
    }

    public function getLogoSrc(){
        $size=$this->getConfig('size');
        if($size=='normal'){
            $src=BridgeHelper::getConfig()->getSettings('logo','instance','core');
        }elseif($size=='large'){
            $src=BridgeHelper::getConfig()->getSettings('logo_large','instance','core');
        }elseif($size=='other'){
        $src=BridgeHelper::getConfig()->getSettings('logo_other','instance','core');
        }else{
            $src=BridgeHelper::getConfig()->getSettings('logo_small','instance','core');
        }
        return $src;
    }

}
