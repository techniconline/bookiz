<?php

namespace Modules\Core\ViewModels\Blocks\LogoBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;

class LogoBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='LogoBlock';

    public function __construct(){
        $this->setDefaultName(trans('core::block.logo'))->addTab('logo',trans('core::block.logo'),'core::widgets.logo.config.form');
    }

    public function getBlockRules(){
        return [
            'size'=>'required|in:normal,small,large,other'
        ];
    }

}
