<?php

namespace Modules\Core\ViewModels\Blocks\PartnersBlock;


use Modules\Core\Traits\Block\MasterBlockConfig;

class PartnersBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='PartnersBlock';

    public function __construct(){
        $this->setDefaultName(trans('core::block.partners_block'));
        $this->addTab('partners_block',trans('core::block.partners_block'),'core::widgets.partners.config.form');
        $this->addTab('partners',trans('core::block.partners'),'core::widgets.partners.config.partners');
        $this->addTab('mobile',trans('core::block.mobile'),'core::widgets.partners.config.mobile');
    }


}