<?php

namespace Modules\Core\ViewModels\Blocks\PartnersBlock;

use Modules\Core\Traits\Block\MasterBlock;


class PartnersBlock
{

    use MasterBlock;

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel =& $this;
        return view('core::widgets.partners.index', compact('blockViewModel'))->render();
    }

    /**
     * @return array
     */
    public function renderJson()
    {
        $block = $this->getDataPartners();
        return $block;
    }

    /**
     * @return array
     */
    private function getDataPartners()
    {
        $arr = [];
        $arr["title"] = $this->getConfig('title');
        $arr["language_id"] = $this->getConfig('language_id');
        for ($i = 0; $i <= 12; $i++) {
            if (!$this->getConfig('name_' . $i)) {
                continue;
            }
            $arr['data'][] = [
                "name" => $this->getConfig('name_' . $i),
                "url" => url($this->getConfig('link_url_' . $i)),
                "image" => $this->getConfig('image_url_' . $i),
            ];
        }
        return $arr;
    }

}
