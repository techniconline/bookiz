<?php

namespace Modules\Core\ViewModels\Blocks\InstanceChangeBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;

class InstanceChangeBlockConfig
{

    use MasterBlockConfig;

    public $baseClass='InstanceChangeBlock';

    public function __construct(){
        $this->addTab('InstanceChange');
    }


}
