<?php

namespace Modules\Core\ViewModels\Blocks\InstanceChangeBlock;

use Modules\Core\Models\Instance\Instance;
use Modules\Core\Traits\Block\MasterBlock;
use BridgeHelper;

class InstanceChangeBlock
{

    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        if(BridgeHelper::getAccess()->hasMultiInstance() || BridgeHelper::getAccess()->isSuperAdmin()){
            $instances = Instance::active()->get();
            return view('core::widgets.instance.changer',compact('instances'))->render();
        }else{
            return '';
        }
    }

}
