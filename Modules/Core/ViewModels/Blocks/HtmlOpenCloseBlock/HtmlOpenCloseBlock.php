<?php

namespace Modules\Core\ViewModels\Blocks\HtmlOpenCloseBlock;


use Modules\Core\Traits\Block\MasterBlock;

class HtmlOpenCloseBlock
{
    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel=&$this;
        return view('core::widgets.html_open_close.index',compact('blockViewModel'))->render();
    }


}
