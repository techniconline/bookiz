<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 6/19/18
 * Time: 4:40 PM
 */

namespace Modules\Core\ViewModels\Blocks\HtmlOpenCloseBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;

class HtmlOpenCloseBlockConfig{
   use MasterBlockConfig;

   public $baseClass='HtmlOpenCloseBlock';

   public function __construct(){
       $this->setDefaultName(trans('core::block.html_open_close'))->addTab('html',trans('core::block.html_open_close'),'core::widgets.html_open_close.config.form');
   }

    public function toJson(){
        $data=$this->getData();
        $openData=['before_html'=>$data['before_html'],'state'=>'open'];
        $closeData=['after_html'=>$data['after_html'],'state'=>'close'];
        $group='group_block_'.time();
        $json=[[
            'block_id'=>$this->block->id,
            'group'=>$group,
            'block'=>$this->baseClass,
            'alias'=>$data['name'].' : '.trans('core::form.fields.open'),
            'active'=>(int)$data['active'],
            'configs'=>$openData,
        ],[
            'block_id'=>$this->block->id,
            'group'=>$group,
            'block'=>$this->baseClass,
            'alias'=>$data['name'].' : '.trans('core::form.fields.close'),
            'active'=>(int)$data['active'],
            'configs'=>$closeData,
        ]];
        return $json;
    }


}