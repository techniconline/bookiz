<?php

namespace Modules\Core\ViewModels\Blocks\InformationBlock;


use Modules\Core\Traits\Block\MasterBlockConfig;

class InformationBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='InformationBlock';

    public function __construct(){
        $this->setDefaultName(trans('core::block.information_block'));
        $this->addTab('information_block',trans('core::block.information_block'),'core::widgets.information.config.form');
        $this->addTab('socials',trans('core::block.socials'),'core::widgets.information.config.socials');
        $this->addTab('link_logo',trans('core::block.link_logo'),'core::widgets.information.config.link_logo');
        $this->addTab('show_control',trans('core::block.show_control'),'core::widgets.information.config.show_control');
    }


}