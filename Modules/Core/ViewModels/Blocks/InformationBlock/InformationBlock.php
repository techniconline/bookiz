<?php

namespace Modules\Core\ViewModels\Blocks\InformationBlock;

use Modules\Core\Traits\Block\MasterBlock;


class InformationBlock
{

    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel=& $this;
        return view('core::widgets.information.index',compact('blockViewModel'))->render();
    }

}
