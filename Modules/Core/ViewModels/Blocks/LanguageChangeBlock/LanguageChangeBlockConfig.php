<?php

namespace Modules\Core\ViewModels\Blocks\LanguageChangeBlock;



use Modules\Core\Traits\Block\MasterBlockConfig;

class LanguageChangeBlockConfig
{

    use MasterBlockConfig;

    public $baseClass='LanguageChangeBlock';

    public function __construct(){
        $this->addTab('LanguageChange');
    }



}
