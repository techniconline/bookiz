<?php

namespace Modules\Core\ViewModels\Blocks\LanguageChangeBlock;

use Modules\Core\Models\Language\Language;
use BridgeHelper;
use Modules\Core\Traits\Block\MasterBlock;

class LanguageChangeBlock
{

    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        if(BridgeHelper::getAccess()->isAdmin() || BridgeHelper::getAccess()->isSuperAdmin()){
            $languages = Language::active()->get();
            return view('core::widgets.language.changer',compact('languages'))->render();
        }else{
            return '';
        }

    }

}
