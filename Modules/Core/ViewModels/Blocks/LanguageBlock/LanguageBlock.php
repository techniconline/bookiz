<?php

namespace Modules\Core\ViewModels\Blocks\LanguageBlock;

use Modules\Core\Traits\Block\MasterBlock;

class LanguageBlock
{

    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        return view('core::widgets.language.index')->render();
    }

}
