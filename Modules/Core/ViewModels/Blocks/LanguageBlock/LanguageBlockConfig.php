<?php

namespace Modules\Core\ViewModels\Blocks\LanguageBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;

class LanguageBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='LanguageBlock';

    public function __construct(){
        $this->addTab('Language');
    }


}
