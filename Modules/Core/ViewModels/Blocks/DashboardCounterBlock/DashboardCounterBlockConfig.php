<?php

namespace Modules\Core\ViewModels\Blocks\DashboardCounterBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;

class DashboardCounterBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='DashboardCounterBlock';

    public function __construct(){
        $this->setDefaultName(trans('core::block.dashboard_counter'))
        ->addTab('dashboard_counter',trans('core::block.dashboard_counter'),'core::widgets.dashboard_counter.config.form');
    }

    /**
     * @return array
     */
    public function getBlockRules(){
        return [];
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getCounterTypeList(){
        $list = trans('core::block.counter_types');
        return $list;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getCounterModelList(){
        $list = trans('core::block.counter_models');
        return $list;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getColors(){
        $list = trans('core::block.colors');
        return $list;
    }

}
