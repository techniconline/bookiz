<?php

namespace Modules\Core\ViewModels\Blocks\DashboardCounterBlock;

use BridgeHelper;
use Illuminate\Support\Facades\DB;
use Modules\Core\Traits\Block\MasterBlock;
use Modules\Sale\Models\Order;

class DashboardCounterBlock
{
    use MasterBlock;

    public $value_model = [];

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $this->setAssetsBlock();
        return $this->generateModel();;
    }


//    /**
//     * @return array|null
//     * @throws \Throwable
//     */
//    public function renderJson()
//    {
////        $this->generateModel();
////
////        return $data;
//    }

    /**
     *
     */
    private function setAssetsBlock()
    {
        $access_assets = [];
        $access_assets [] = ['container' => 'general_style', 'src' => ("css/loading.css"), 'name' => 'loading'];
        $access_assets [] = ['container' => 'plugin_general_2', 'src' => ("assets/global/plugins/counterup/jquery.waypoints.min.js"), 'name' => 'waypoints'];
        $access_assets [] = ['container' => 'plugin_general_2', 'src' => ("assets/global/plugins/counterup/jquery.counterup.min.js"), 'name' => 'counterup'];

        $this->setAssetsOnRoute($access_assets);
    }


    /**
     * @return null|string
     * @throws \Throwable
     */
    private function generateModel()
    {
        //$this->setTitleChart($this->getData('alias', trans('core::form.titles.dashboard')));

        $counter_models = $this->getConfig('counter_models', ['sale_all']);
        foreach ($counter_models as $counter_model) {
            $counter_model = get_uppercase_by_underscores($counter_model);
            $methodName = "setCounterModel" . $counter_model;
            if (method_exists($this, $methodName)) {
                $this->{$methodName}();
            }
            return $this->getView();

        }

        return null;
    }

    /**
     * @param null $key
     * @return array|mixed|null
     */
    public function getCounterValue($key = null)
    {
        if ($key) {

            if (isset($this->value_model[$key])) {
                return $this->value_model[$key];
            }
            return null;

        }
        return $this->value_model;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function getView()
    {
        $blockViewModel =& $this;
        return view('core::widgets.dashboard_counter.index', compact('blockViewModel'))->render();
    }

    /**
     * @return mixed
     */
    private function setCounterModelSaleAll()
    {
        $data = Order::active()->filterCurrentInstance()->filterCompleted()->sum(DB::raw('( if( currency_id=1 , payable_amount , payable_amount * 10) )'));
        $this->value_model["value_model"] = number_format($data);

        $data = Order::active()->filterCurrentInstance()->filterCompleted()->count('id');
        $this->value_model["count_complete_sale_all"] = $data;
        $data = Order::active()->filterCurrentInstance()->count('id');
        $this->value_model["count_sale_all"] = $data;

        $percent = (100 * $this->value_model["count_complete_sale_all"]) / $this->value_model["count_sale_all"];
        $this->value_model["percent"] = (int)$percent;
        $this->value_model["unit_value"] = trans("core::block.counter_type_symbol.currency");
        $this->value_model["progress_text"] = trans("core::block.dashboard_counters.progress_text_sale");

        return $this;
    }

    /**
     * @return mixed
     */
    private function setCounterModelCountUser()
    {
        $model = BridgeHelper::getUser()->getUserModel();
        $data = $model->active()->filterCurrentInstance()->isConfirm()->count('id');
        $this->value_model["value_model"] = $data;

        $data = $model->active()->filterCurrentInstance()->count('id');
        $this->value_model["total_value_model"] = $data;

        $percent = (100 * $this->value_model["value_model"]) / $this->value_model["total_value_model"];
        $this->value_model["percent"] = (int)$percent;

        $this->value_model["unit_value"] = trans("core::block.counter_type_symbol.count_user");
        $this->value_model["progress_text"] = trans("core::block.dashboard_counters.progress_text_confirm");

        return $this;
    }

    /**
     * @return mixed
     */
    private function setCounterModelCountComment()
    {
        $model = BridgeHelper::getComment()->getCommentModel();
        $data = $model->active()->count('id');
        $this->value_model["value_model"] = $data;

        $data = $model->enable()->count('id');
        $this->value_model["total_value_model"] = $data;

        $percent = (100 * $this->value_model["value_model"]) / $this->value_model["total_value_model"];

        $this->value_model["percent"] = (int)$percent;

        $this->value_model["unit_value"] = trans("core::block.counter_type_symbol.count");
        $this->value_model["progress_text"] = trans("core::block.dashboard_counters.progress_text_confirm");

        return $this;
    }

}
