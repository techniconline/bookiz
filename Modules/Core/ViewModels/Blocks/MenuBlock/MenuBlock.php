<?php

namespace Modules\Core\ViewModels\Blocks\MenuBlock;


use BridgeHelper;
use Modules\Core\Providers\Helpers\Menu\Facades\MenuHelper;
use Modules\Core\Traits\Block\MasterBlock;

class MenuBlock
{
    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel=& $this;
        return view('core::widgets.menu.index',compact('blockViewModel'))->render();
    }

    public function getMenus(){
        $menus=MenuHelper::getNestableMenu($this->getConfig('menu_id'),$this->getConfig('deep'));
        return $menus;
    }



}
