<?php

namespace Modules\Core\ViewModels\Blocks\MenuBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;
use Modules\Core\Models\Menu\Menu;

class MenuBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='MenuBlock';

    public function __construct(){
        $this->setDefaultName(trans('core::block.menu'))
        ->addTab('menu',trans('core::block.menu'),'core::widgets.menu.config.form')
        ->addTab('menu_advanced',trans('core::block.advanced'),'core::widgets.menu.config.advanced')
        ->addTab('menu_mobile',trans('core::block.mobile'),'core::widgets.menu.config.mobile');
    }

    public function getBlockRules(){
        return [
            'menu_id'=>'required|exists:menus,id'
        ];
    }

    public function getMenuRootList(){
        $menuList=Menu::filterLanguage()->filterCurrentInstance()->get();
        $list=[];
        foreach ($menuList as $menu){
            $list[$menu->id]=$menu->title;
        }
        return $list;
    }

}
