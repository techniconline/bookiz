<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 6/19/18
 * Time: 4:40 PM
 */

namespace Modules\Core\ViewModels\Blocks\HtmlBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;

class HtmlBlockConfig{
   use MasterBlockConfig;

   public $baseClass='HtmlBlock';

   public function __construct(){
       $this->removeHtmlTab()->setDefaultName(trans('core::block.html'))
       ->addTab('html',trans('core::block.html'),'core::widgets.html.config.html')
       ->addTab('css',trans('core::block.css'),'core::widgets.html.config.css')
       ->addTab('javascript',trans('core::block.javascript'),'core::widgets.html.config.javascript');
   }




}