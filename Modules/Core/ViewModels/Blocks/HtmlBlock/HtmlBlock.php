<?php

namespace Modules\Core\ViewModels\Blocks\HtmlBlock;


use Modules\Core\Traits\Block\MasterBlock;

class HtmlBlock
{
    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel=&$this;
        return view('core::widgets.html.index',compact('blockViewModel'))->render();
    }


}
