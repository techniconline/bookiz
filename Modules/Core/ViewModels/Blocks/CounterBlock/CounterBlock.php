<?php

namespace Modules\Core\ViewModels\Blocks\CounterBlock;

use Modules\Core\Traits\Block\MasterBlock;


class CounterBlock
{

    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel=& $this;
        return view('core::widgets.counter.index',compact('blockViewModel'))->render();
    }

}
