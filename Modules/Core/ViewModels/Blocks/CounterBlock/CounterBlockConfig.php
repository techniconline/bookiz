<?php

namespace Modules\Core\ViewModels\Blocks\CounterBlock;


use Modules\Core\Traits\Block\MasterBlockConfig;

class CounterBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='CounterBlock';

    public function __construct(){
        $this->setDefaultName(trans('core::block.counter_block'));
        $this->addTab('counter_block',trans('core::block.counter_block'),'core::widgets.counter.config.form');
        $this->addTab('counter',trans('core::block.counter'),'core::widgets.counter.config.counter');
    }


}