<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 6/19/18
 * Time: 4:40 PM
 */

namespace Modules\Core\ViewModels\Blocks\JsCssBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;

class JsCssBlockConfig{
   use MasterBlockConfig;

   public $baseClass='JsCssBlock';

   public function __construct(){
       $this->setOnlyFilesHtmlTab(true)->setDefaultName(trans('core::block.html'));
   }




}