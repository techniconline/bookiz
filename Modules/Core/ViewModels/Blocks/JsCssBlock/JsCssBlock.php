<?php

namespace Modules\Core\ViewModels\Blocks\JsCssBlock;


use Modules\Core\Traits\Block\MasterBlock;

class JsCssBlock
{
    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        return '';
    }


}
