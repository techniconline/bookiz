<?php

namespace Modules\Core\ViewModels\Location;

use function foo\func;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\Models\Location\Location;

class JsonViewModel extends BaseViewModel
{

    public function getJson(){
        $country_id=false;
        if($this->request->has('country_id')){
            $country_id=$this->request->get('country_id');
        }
        $state_id=false;
        if($this->request->has('state_id')){
             $state_id=$this->request->get('state_id');
        }
        $model=Location::select('id','name','code','latitude','longitude');
        if($country_id){
            $model->where('country_id',$country_id);
        }else{
            $model->whereNull('country_id');
        }
        if($state_id){
            $model->where('state_id',$state_id);
        }else{
            $model->whereNull('state_id');
        }
        $items=$model->orderBy('name','ASC')->get();
        $items=$items->map(function ($item) {
            $item->title=$item->name;//trans('core::location.'.$item->name);
            $item->value=$item->id;
            return $item;
        });
        return $this->setDataResponse($items)->setResponse(true) ;
    }



    public function getCountryByApi(){
        $q=false;
        $selected=false;
        if($this->request->has('q')){
            $q=$this->request->get('q');
        }elseif($this->request->has('selected')){
            $inputs=explode(',',$this->request->get('selected'));
            $selected=[];
            foreach($inputs as $select){
                $select=trim($select);
                if(strlen($select)){
                    $selected[]=$select;
                }
            }
        }
        $model=Location::select('id','name');
        if($q){
            $model->where('name', 'like', '%'.$q.'%');
            $items=$model->get();
        }elseif($selected && count($selected)){
            $model->whereIN('id', $selected);
            $items=$model->get();

        }else{
            $items=collect([]);
        }
        $items=$items->map(function ($item) {
            $item->text=trans('core::location.'.$item->name);
            return $item;
        });
        return $this->setDataResponse($items)->setResponse(true) ;
    }
}
