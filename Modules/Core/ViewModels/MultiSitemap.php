<?php
/**
 * Created by PhpStorm.
 * User: hossein
 * Date: 2/13/19
 * Time: 4:43 PM
 */

namespace Modules\Core\ViewModels;


use Illuminate\Support\Facades\Storage;

class MultiSitemap
{
    public $names = [];
    public $data= [];

    public function generateSitemap($data)
    {
        $this->data = $data;
        $this->sitemapName = 'sitemap.xml';
        $sitemapModel = &$this;
        Storage::disk('sitemap_base')->put($this->sitemapName, view('core::sitemap.multi_sitemap', compact('sitemapModel')));
    }

    public function getLink($id = null)
    {
        return url("/course/category/$id/courses/lifestyl");
    }
}