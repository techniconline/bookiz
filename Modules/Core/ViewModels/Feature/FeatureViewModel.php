<?php

namespace Modules\Core\ViewModels\Feature;


use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureGroupRelation;
use Modules\Core\Models\Language\Language;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;


class FeatureViewModel extends BaseViewModel
{

    use GridViewModel;

    private $feature_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = Feature::active();
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $feature_assets = [];

        //use bested assets
        if ($this->feature_assets) {
            $feature_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/feature.js"), 'name' => 'feature-backend'];
        }

        return $feature_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('core::feature.title'), true)
            ->addColumn('text_custom', trans('core::feature.custom'), false)
            ->addColumn('type', trans('core::feature.type'), true);

        /*add action*/
        $add = array(
            'name' => 'core.feature.create',
            'parameter' => null
        );
        $this->addButton('create_feature', $add, trans('core::feature.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        /*add action*/
//        $add = array(
//            'name' => 'core.feature.create',
//            'parameter' => null
//        );
//        $this->addAction('add', $add, trans('core::feature.add_feature'), 'fa fa-plus', false, ['target' => '_blank', 'class' => 'btn btn-sm btn-warning']);


        $show = array(
            'name' => 'core.feature.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('core::feature.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'core.feature.values.create',
            'parameter' => ['id']
        );
        $this->addAction('add_options', $show, trans('core::feature.add_feature_values'), 'fa fa-plus', false, ['target' => '', 'class' => 'btn btn-sm btn-warning']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('core::feature.confirm_delete')];
        $delete = array(
            'name' => 'core.feature.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('core::feature.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
//        $this->addFilter('id', 'text', ['op' => '=']);
        $this->addFilter('title', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
//        $row->backend_template_name = isset($row->backendTemplate->name) ? $row->backendTemplate->name : '-';
//        $row->frontend_template_name = isset($row->frontTemplate->name) ? $row->frontTemplate->name : '-';
        return $row;
    }

    public function getActionsUpdate($row)
    {
        $action = $this->actions;
        if ($row->custom) {
            unset($action['add_options']);
        }
        return $action;

    }

    protected function setAssetsCreateFeature()
    {
        $this->feature_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('core::feature.feature_list'));
        $this->generateGridList()->renderedView("core::feature.index", ['view_model' => $this], "feature_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createFeature()
    {
        $languages = Language::active()->get();
        $feature = new Feature();
        $types = $feature->types();
        $features = $feature->active()->isNotCustom()->where(function ($q){
            if($feature_id = $this->request->get('feature_id')){
                $q->where('id', '!=', $feature_id);
            }
        })->get();

        $features = $features?$features->mapWithKeys(function ($item){
            return [$item->id => $item->title.' - '.$item->type];
        })->toArray():[];

        $feature = $feature->active()->with(['featureValues'])->find($this->request->get('feature_id'));

        $this->setTitlePage(trans('core::feature.' . ($feature ? 'edit' : 'add')));
        return $this->renderedView("core::feature.form", ['feature' => $feature, 'languages' => $languages, 'types' => $types, 'features'=>$features], "form");
    }

    /**
     * @return $this
     */
    protected function destroyFeature()
    {
        $feature_id = $this->request->get('feature_id');
        $feature = Feature::active()->find($feature_id);

        if ($feature->delete()) {
            FeatureGroupRelation::where('feature_id', $feature_id)->delete();
            return $this->setResponse(true, trans("core::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("core::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveFeature()
    {
        $feature = new Feature();
        if ($feature_id = $this->request->get("feature_id")) {
            $feature = $feature->active()->find($feature_id);
            if (!$feature) {
                return $this->redirect(route('core.feature.edit', ['feature_id' => $this->request->get('feature_id')]))->setResponse(false, trans("core::messages.alert.not_find_data"));
            }
        }


        if (is_int((int)$this->request->get('custom')) && !$this->request->get('custom')) {
            $this->request->request->set('custom', 0);
        }

        if (is_int((int)$this->request->get('parent_id')) && !$this->request->get('parent_id')) {
            $this->request->request->set('parent_id', null);
        }

        if (($feature->fill($this->request->all())->isValid()) || ($feature_id && $feature)) {
            $feature->save();
            $feature->updated = $feature_id ? true : false;
            return $this->redirect(route('core.feature.edit', ['feature_id' => $feature->id]))->setDataResponse($feature)->setResponse(true, trans("core::messages.alert.save_success"));
        }

        return $this->redirect(route('core.feature.create'))->setResponse(false, trans("core::messages.alert.mis_data"), $feature->errors);
    }


}
