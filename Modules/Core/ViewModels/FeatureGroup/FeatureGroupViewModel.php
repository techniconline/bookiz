<?php

namespace Modules\Core\ViewModels\FeatureGroup;


use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureGroup;
use Modules\Core\Models\Feature\FeatureGroupRelation;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;


class FeatureGroupViewModel extends BaseViewModel
{

    use GridViewModel;

    private $feature_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = FeatureGroup::active()->with(['instance', 'language']);
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $feature_assets = [];

        //use bested assets
        if ($this->feature_assets) {
            $feature_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/feature.js"), 'name' => 'feature-backend'];
        }

        return $feature_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('core::feature.group.title'), true);
        $this->addColumn('instance', trans('core::feature.group.instance'), true);
        $this->addColumn('language', trans('core::feature.group.language'), true);

        /*add action*/
        $add = array(
            'name' => 'core.feature.group.create',
            'parameter' => null
        );
        $this->addButton('create_feature', $add, trans('core::feature.group.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        /*add action*/
//        $add = array(
//            'name' => 'core.feature.group.create',
//            'parameter' => null
//        );
//        $this->addAction('add', $add, trans('core::feature.group.add_feature'), 'fa fa-plus', false, ['target' => '_blank', 'class' => 'btn btn-sm btn-warning']);


        $show = array(
            'name' => 'core.feature.group.edit',
            'parameter' => ['id']
        );
        $this->addAction('group_edit', $show, trans('core::feature.group.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('core::feature.confirm_delete')];
        $delete = array(
            'name' => 'core.feature.group.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('core::feature.group.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
//        $this->addFilter('id', 'text', ['op' => '=']);
        $this->addFilter('title', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->instance = isset($row->instance->name) ? $row->instance->name : '-';
        $row->language = isset($row->language->name) ? $row->language->name : '-';
        return $row;
    }

    protected function setAssetsCreateFeatureGroup()
    {
//        $this->feature_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('core::feature.group.list'));
        $this->generateGridList()->renderedView("core::feature.group.index", ['view_model' => $this], "feature_group_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createFeatureGroup()
    {
        $languages = Language::active()->get();
        $featureGroup = new FeatureGroup();
        $featureGroup = $featureGroup->active()->with(['featureGroupRelations.feature'])->find($this->request->get('feature_group_id'));
        $instances = Instance::active()->with(['language'])->get();

        $instances = $instances ? $instances->pluck('name', 'id') : [];

        $features = null;
        $activeFeatures = [];
        if ($featureGroup) {
            $activeFeatures = $featureGroup->featureGroupRelations->pluck('feature_id')->toArray();

            $features = Feature::active()->get();
            $features = $features ? $features->pluck('title', 'id') : [];
        }

        $this->setTitlePage(trans('core::feature.group.' . ($featureGroup ? 'edit' : 'add')));
        return $this->renderedView("core::feature.group.form", ['featureGroup' => $featureGroup, 'languages' => $languages, 'features' => $features, 'instances' => $instances, 'activeFeatures' => $activeFeatures], "form");
    }

    /**
     * @return $this
     */
    protected function destroyFeatureGroup()
    {
        $feature_group_id = $this->request->get('feature_group_id');
        $feature = FeatureGroup::active()->find($feature_group_id);

        if ($feature->delete()) {
            return $this->setResponse(true, trans("core::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("core::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveFeatureGroup()
    {
        $featureGroup = new FeatureGroup();
        if ($feature_group_id = $this->request->get("feature_group_id")) {
            $featureGroup = $featureGroup->active()->find($feature_group_id);
            if (!$featureGroup) {
                return $this->redirect(route('core.feature.group.edit', ['feature_group_id' => $this->request->get('feature_group_id')]))->setResponse(false, trans("core::messages.alert.not_find_data"));
            }
        }

        if (is_int((int)$this->request->get('instance_id')) && !$this->request->get('instance_id')) {
            $this->request->request->set('instance_id', null);
        }

        if (($featureGroup->fill($this->request->all())->isValid()) || ($feature_group_id && $featureGroup)) {
            $featureGroup->save();
            $featureGroup->updated = $feature_group_id ? true : false;

            $this->saveFeatures($feature_group_id);

            return $this->redirect(route('core.feature.group.edit', ['feature_group_id' => $featureGroup->id]))->setDataResponse($featureGroup)->setResponse(true, trans("core::messages.alert.save_success"));
        }

        return $this->redirect(route('core.feature.group.create'))->setResponse(false, trans("core::messages.alert.mis_data"), $featureGroup->errors);
    }

    /**
     * @param $feature_group_id
     * @return bool
     */
    protected function saveFeatures($feature_group_id)
    {
        $this->deleteFeatures($feature_group_id);
        $features = $this->request->get('feature_ids');
        if (!$features) {
            return false;
        }

        $insertItems = [];
        foreach ($features as $index => $feature) {
            $insertItems[] = ['feature_id' => $feature, 'feature_group_id' => $feature_group_id, 'position' => $index];
        }

        $featureGroupRelations = new FeatureGroupRelation();
        return $featureGroupRelations->insert($insertItems);
    }

    /**
     * @param $feature_group_id
     * @return mixed
     */
    public function deleteFeatures($feature_group_id)
    {
        $featureGroupRelations = new FeatureGroupRelation();
        return $featureGroupRelations->where('feature_group_id', $feature_group_id)->delete();

    }


    public function getFeatureGroupJson(){
        $q=false;
        $selected=false;
        if($this->request->has('q')){
            $q=$this->request->get('q');
        }elseif($this->request->has('selected')){
            $inputs=explode(',',$this->request->get('selected'));
            $selected=[];
            foreach($inputs as $select){
                $select=trim($select);
                if(strlen($select)){
                    $selected[]=$select;
                }
            }
        }
        $model=FeatureGroup::select('id','title')->filterCurrentInstance()->filterLanguage();
        if($q){
            $model->where('title', 'like', '%'.$q.'%');
            $items=$model->get();
        }elseif($selected && count($selected)){
            $model->whereIN('id', $selected);
            $items=$model->get();
        }else{
            $items=collect([]);
        }
        $items=$items->map(function ($item) {
            $item->text=$item->title;
            return $item;
        });
        return $this->setDataResponse($items)->setResponse(true) ;
    }


}
