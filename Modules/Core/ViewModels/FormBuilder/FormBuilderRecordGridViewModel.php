<?php

namespace Modules\Core\ViewModels\FormBuilder;


use Illuminate\Http\Request;
use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureValueRelation;
use Modules\Core\Models\Form;
use Modules\Core\Models\Form\FormFeatureValueRelation;
use Modules\Core\Models\Form\FormMetaGroup;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Meta\MetaGroup;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\Traits\Feature\FormFeatureBuilder;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;


class FormBuilderRecordGridViewModel extends BaseViewModel
{

    use GridViewModel;
    use FormFeatureBuilder;

    private $form_assets;
    private $form_id;

    public function __construct()
    {

    }

    /**
     * @return FormBuilderRecordGridViewModel
     */
    public function boot()
    {
        return $this->setFormId();
    }

    /**
     * @param null $form_id
     * @return $this
     */
    public function setFormId($form_id = null)
    {
        if (!$this->form_id) {
            $this->form_id = $form_id ?: $this->request->get('form_id');
        }
        return $this;
    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->boot();
        $this->Model = Form\FormRecord::enable()->filterCurrentInstance()->where('form_id', $this->form_id)->with(['user', 'form']);
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $form_assets = [];

        if ($this->form_assets) {
            $form_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/form/formBuilder.js"), 'name' => 'form-backend'];
        }

        return $form_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return $this
     */
    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('form_title', trans('core::formBuilder.title'), true);
        $this->addColumn('user_name', trans('core::formBuilder.record.user_name'), false);
        $this->addColumn('created_at', trans('core::formBuilder.record.created_at'), false);

        /*add action*/
        $add = array(
            'name' => 'core.formBuilder.index',
            'parameter' => null
        );
        $this->addButton('list_form', $add, trans('core::formBuilder.list'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);


        $show = array(
            'name' => 'core.formBuilder.record.show',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('core::formBuilder.record.show'), 'fa fa-file-text-o', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'core.formBuilder.record.changeStatusRecord',
            'parameter' => ['id']
        );
        $this->addAction('changeStatus', $show, trans('core::formBuilder.record.changeStatus'), 'fa fa-eye', false
            , ['target' => '', 'class' => 'btn btn-sm btn-primary _ajax_confirm', 'data-method' => "POST", 'data-message-confirm' => trans('core::formBuilder.confirm')]);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('core::formBuilder.confirm_delete')];
        $delete = array(
            'name' => 'core.formBuilder.record.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('core::formBuilder.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        //$this->addFilter('title', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->form_title = $row->form->title;
        $row->user_name = isset($row->user->first_name) ? $row->user->full_name : '-';
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $actions = $this->actions;
        if ($row->active == 1) {
            $actions["changeStatus"]->icon = "fa fa-eye";
        } else {
            $actions["changeStatus"]->icon = "fa fa-eye-slash";
        }
        return $actions;
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('core::formBuilder.record.list'));
        $this->generateGridList()->renderedView("core::form_builder.record.index", ['view_model' => $this], "formBuilder_record_list");
        return $this;
    }

}
