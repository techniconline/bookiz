<?php

namespace Modules\Core\ViewModels\FormBuilder;


use Illuminate\Http\Request;
use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureValueRelation;
use Modules\Core\Models\Form;
use Modules\Core\Models\Form\FormFeatureValueRelation;
use Modules\Core\Models\Form\FormMetaGroup;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Meta\MetaGroup;
use Modules\Core\Traits\Feature\FormFeatureBuilder;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;


class FormBuilderViewModel extends BaseViewModel
{

    use GridViewModel;
    use FormFeatureBuilder;

    private $form_assets;
    private $form_meta_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = Form::enable();
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $form_assets = [];

        //use bested assets
        if ($this->form_assets) {
//            $form_assets [] = ['container' => 'up_theme_js_2', 'src' => ("assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"), 'name' => 'ui.widget'];
//
//            $form_assets [] = ['container' => 'plugin_general_2', 'src' => ("jquery-ui/jquery-ui.min.js"), 'name' => 'jquery-ui'];
//            $form_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/plupload.full.min.js"), 'name' => 'plupload.full'];
//            $form_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/jquery.ui.plupload.js"), 'name' => 'jquery.ui.plupload'];
//            $form_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/i18n/fa.js"), 'name' => 'fa.js'];
//
//            $form_assets [] = ['container' => 'general_style', 'src' => ("jquery-ui/jquery-ui.min.css"), 'name' => 'jquery-ui'];
//            $form_assets [] = ['container' => 'general_style', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css"), 'name' => 'jquery.ui.plupload.css'];

        }

        if ($this->form_assets || $this->form_meta_assets) {
            $form_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/form/formBuilder.js"), 'name' => 'form-backend'];
        }

        return $form_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return $this
     */
    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('core::formBuilder.title'), true);

        /*add action*/
        $add = array(
            'name' => 'core.formBuilder.create',
            'parameter' => null
        );
        $this->addButton('create_form', $add, trans('core::formBuilder.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);


        $show = array(
            'name' => 'core.formBuilder.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('core::formBuilder.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'core.formBuilder.meta.editMetaTags',
            'parameter' => ['id']
        );
        $this->addAction('edit_meta_tags', $show, trans('core::formBuilder.edit_meta_tags'), 'fa fa-tags', false, ['target' => '', 'class' => 'btn btn-sm btn-warning']);

        $show = array(
            'name' => 'core.formBuilder.record.index',
            'parameter' => ['id']
        );
        $this->addAction('list_records_form', $show, trans('core::formBuilder.record.list'), 'fa fa-list', false, ['target' => '', 'class' => 'btn btn-sm btn-primary']);


        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('core::formBuilder.confirm_delete')];
        $delete = array(
            'name' => 'core.formBuilder.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('core::formBuilder.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('title', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        return $row;
    }

    /**
     * @return $this
     */
    protected function setAssetsEditForm()
    {
        $this->form_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('core::formBuilder.list'));
        $this->generateGridList()->renderedView("core::form_builder.index", ['view_model' => $this], "formBuilder_list");
        return $this;
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createForm()
    {
        $instances = Instance::active()->get();
        $array = [
            "grid_view_feature_show"=>[
                "feature_id"=>[12,15]
            ]
        ];

        $form = new Form();

        $form = $form->enable()->find($this->request->get('form_id'));

        $metaGroups = null;
        $metaGroupsActive = [];
        if ($form) {
            $metaGroups = new MetaGroup();
            $metaGroups = $metaGroups->active()->filterLanguage()->filterSystem(Form::SYSTEM_NAME)->get();
            $metaGroups = $metaGroups ? $metaGroups->pluck('title', 'id') : [];

            $formMetaGroups = $form->formMetaGroups()->get();

            if ($formMetaGroups) {
                $metaGroupsActive = $formMetaGroups->pluck('meta_group_id')->toArray();
            }

        }

        $this->setTitlePage(trans('core::formBuilder.' . ($form ? 'edit' : 'add')));
        return $this->renderedView("core::form_builder.form", ['form' => $form, 'instances' => $instances
            , 'metaGroups' => $metaGroups, 'metaGroupsActive' => $metaGroupsActive], "form");
    }


    /**
     * @return FormBuilderViewModel
     * @throws \Throwable
     */
    protected function editForm()
    {
        return $this->createForm();
    }

    /**
     * @return $this
     */
    protected function destroyForm()
    {
        $form_id = $this->request->get('form_id');
        $form = Form::enable()->find($form_id);

        if ($form && $form->delete()) {
            return $this->setResponse(true, trans("core::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("core::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveForm()
    {
        $response = $this->saveFormService();
        $form = $response['form'];
        if ($response['action']) {
            return $this->redirect(route('core.formBuilder.edit', ['form_id' => $form->id]))->setDataResponse($form)->setResponse(true, trans("core::messages.alert.save_success"));
        }
        return $this->redirect(isset($response['redirect']) ? $response['redirect'] : route('formBuilder.create'))->setResponse(false, trans("core::messages.alert.mis_data"), isset($form->errors) ? $form->errors : null);
    }

    /**
     * @return $this|array
     */
    private function saveFormService()
    {
        $form = new Form();
        if ($form_id = $this->request->get("form_id")) {
            $form = $form->enable()->find($form_id);
            if (!$form) {
                return ['action' => false, 'form' => $form, 'redirect' => route('core.formBuilder.edit', ['form_id' => $this->request->get('form_id')])];
            }
            $this->saveMetaGroups($form_id, $this->request->get('meta_group_ids'));
        }

        if (($form->fill($this->request->all())->isValid()) || ($form_id && $form)) {
            $form->save();
            $form->updated = $form_id ? true : false;
            $response = ['action' => true, 'form' => $form];
        } else {
            $response = ['action' => false, 'form' => $form];
        }

        return $response;
    }

    /**
     * @param $form_id
     * @param $meta_group_ids
     * @return bool
     */
    protected function saveMetaGroups($form_id, $meta_group_ids)
    {

        $fmgModel = new FormMetaGroup();
        $system = $fmgModel->getTable();

        //TODO
        $fmgIds = $fmgModel->where('form_id', $form_id)->pluck('id');
        if ($fmgIds) {
//            FeatureValueRelation::where('system', $system)->whereIn('item_id', $fmgIds)->delete();
            FormMetaGroup::whereIn('id', $fmgIds)->delete();
        }

        if (empty($meta_group_ids) || !$form_id) {
            return false;
        }

        $insertItems = [];
        foreach ($meta_group_ids as $meta_group_id) {
            $insertItems[] = ['meta_group_id' => $meta_group_id, 'form_id' => $form_id];
        }

        $result = FormMetaGroup::insert($insertItems);
        return $result;
    }


    /**
     * @return $this
     */
    protected function setAssetsEditMetaTags()
    {
        $this->form_meta_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function editMetaTags()
    {
        $form = new Form();
        $form = $form->enable()->with(['metaGroups.featureGroups.featureGroupRelations.feature.featureValues'])->find($this->request->get('form_id'));

        if (!$form) {
            return $this->redirect(route('formBuilder.meta.editMetaTags', ['form_id' => $this->request->get('form_id')]))->setResponse(false, trans("core::messages.alert.not_find_data"));
        }

        $metaGroups = null;
        $metaFeatureDataList = $metaGroupsActive = [];
        $metaGroups = MetaGroup::active()->filterLanguage()->filterSystem(Form::SYSTEM_NAME)->get();

        $formMetaGroups = $form->formMetaGroups()->get();

//        $this->getFormBuilderRows($form->id);

        if ($formMetaGroups) {
            $metaGroupsActive = $formMetaGroups->pluck('meta_group_id')->toArray();
        }

        $this->setTitlePage(trans('core::formBuilder.edit_meta_tags') . ':' . $form->title);

        return $this->renderedView("core::form_builder.form_builder_meta_tags", ['form' => $form, 'metaGroups' => $metaGroups
            , 'metaGroupsActive' => $metaGroupsActive, 'formMetaGroups' => $formMetaGroups
            , 'metaFeatureDataList' => $metaFeatureDataList, 'view_model' => $this], "form");


    }

    protected function getFormBuilderRows($form_id, $paginate = true)
    {

        $form = Form::with(['metaGroups.featureGroups.featureGroupRelations.feature.featureValues'])->find($form_id);
        if (!$form) {
            return $this->redirect(route('formBuilder.meta.editMetaTags', ['form_id' => $this->request->get('form_id')]))->setResponse(false, trans("core::messages.alert.not_find_data"));
        }

        $metaGroups = $form->metaGroups;

        $listMetaGroup = $metaGroups ? $metaGroups->pluck('title', 'id') : null;

        $formMetaGroups = $form->formMetaGroups()->orderBy("position")->get();

        $formMetaGroups = ($formMetaGroups)->map(function ($item) use ($listMetaGroup) {
            $item['group_name'] = isset($listMetaGroup[$item->meta_group_id]) ? $listMetaGroup[$item->meta_group_id] : null;
            return $item;
        });

        //GET DATA
        $formMetaGroupList = $formMetaGroups->pluck('meta_group_id', 'id');
        $formMetaGroupIds = $formMetaGroupList->keys();
        $metaFeatureData = Form\FormRecord::active()->where("system", "form_meta_groups")->with(["formFeatureValueRelations"=>function($q)use ($formMetaGroupIds){
            if($formMetaGroupIds->isNotEmpty()){
                $q->whereIn("item_id", $formMetaGroupIds);
            }else{
                $q->whereIn("id", 0);
            }
        }]);
        if ($paginate) {
            $metaFeatureData = $metaFeatureData->simplePaginate();
        } else {
            $metaFeatureData = $metaFeatureData->get();
        }

        foreach ($metaFeatureData as $item) {
            foreach ($item->formFeatureValueRelations as $formFeatureValueRelation) {
                $metaFeatureDataList[$item->id][$formFeatureValueRelation->item_id][$formFeatureValueRelation->feature_id][] = $formFeatureValueRelation->feature_value_id ? $formFeatureValueRelation->feature_value_id : $formFeatureValueRelation->feature_value;
            }
        }

        return $metaFeatureDataList;

    }

    /**
     * @return $this
     */
    protected function saveFeaturesValue()
    {
        $form_meta_group_id = $this->request->get('form_meta_group_id');
        $form_id = $this->request->get('form_id');
        $formMetaGroup = FormMetaGroup::find($form_meta_group_id);

        if (!$formMetaGroup) {
            return $this->redirectBack()->setResponse(false, trans("core::messages.alert.not_find_data"));
        }
        $formMetaGroup->save();
        //save time

        //insert feature values
        $features = $this->request->get('feature');

        if (!$features) {
            return $this->redirectBack()->setResponse(false);
        }

        $feature_ids = array_keys($features);

        $featureModel = new Feature();
        $useFeatures = $featureModel->active()->whereIn('id', $feature_ids)->get();

        if (!$useFeatures) {
            return $this->redirectBack()->setResponse(false);
        }

        $featuresType = $useFeatures->pluck('type', 'id')->toArray();

        $customTypes = $featureModel->customTypes;

        $insertItems = [];
        $language_id = app('getInstanceObject')->getLanguageId();
        $item_id = $formMetaGroup->id;
        $system = 'form_meta_groups';
        $this->request->offsetSet("system", $system);
        $this->request->offsetSet("instance_id", app("getInstanceObject")->getCurrentInstanceId());
        $this->request->offsetSet("language_id", app("getInstanceObject")->getLanguageId());
        //form records
        $formRecordId = null;
        $formRecord = new Form\FormRecord();
        if ($formRecord->fill($this->request->all())->isValid()) {
            $formRecord->active = 2;
            $formRecord->save();
            $formRecordId = $formRecord->id;
        }

        if (!$formRecordId) {
            return $this->redirectBack()->setResponse(false, trans("core::messages.alert.save_un_success"));
        }


        foreach ($features as $f_id => $value) {
            $type = isset($featuresType[$f_id]) ? $featuresType[$f_id] : null;

            if (!$type || !$value)
                continue;

            if (in_array($type, $customTypes)) {
                //insert value
                $insertItems[] = ['form_record_id' => $formRecordId, 'item_id' => $item_id, 'feature_id' => $f_id, 'feature_value' => $value, 'feature_value_id' => null, 'created_at' => date("Y-m-d H:i:s")];
            } else {
                //insert value_id
                if (is_array($value)) {

                    foreach ($value as $item) {
                        $insertItems[] = ['form_record_id' => $formRecordId, 'item_id' => $item_id, 'feature_id' => $f_id, 'feature_value' => null, 'feature_value_id' => $item, 'created_at' => date("Y-m-d H:i:s")];
                    }

                } else {
                    $insertItems[] = ['form_record_id' => $formRecordId, 'item_id' => $item_id, 'feature_id' => $f_id, 'feature_value' => null, 'feature_value_id' => $value, 'created_at' => date("Y-m-d H:i:s")];
                }
            }


        }

        $formFeatureValueRelation = new FormFeatureValueRelation();

        if (!$insertItems) {
            return $this->redirectBack()->setResponse(false);
        }

        $result = $formFeatureValueRelation->insert($insertItems);
        //insert feature values

        if ($result) {
            return $this->redirectBack()->setResponse(true, trans("core::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, trans("core::messages.alert.save_un_success"));

    }

    /**
     * @return $this
     */
    protected function copyFormMetaGroup()
    {
        $form_meta_group_id = $this->request->get('form_meta_group_id');
        $form_id = $this->request->get('form_id');
        $formMetaGroup = FormMetaGroup::find($form_meta_group_id);
        $system = $formMetaGroup->getTable();

        $resultCopy = null;
        if ($formMetaGroup) {
            $newFormMetaGroup = $formMetaGroup->replicate();
            $resultCopy = $newFormMetaGroup->save();
            if ($resultCopy) {
                $this->copyFeatureValueRelation($form_meta_group_id, $system, $newFormMetaGroup->id);
            }

        }
        return $this->redirectBack()->setResponse($resultCopy ? true : false, trans("core::messages.alert." . ($resultCopy ? 'save_success' : 'save_un_success')));

    }

    /**
     * @param $item_id
     * @param $system
     * @param $new_item_id
     * @return bool
     */
    protected function copyFeatureValueRelation($item_id, $system, $new_item_id)
    {
        $featureValueRelation = new FeatureValueRelation();
        $fvrs = $featureValueRelation->active()->where('system', $system)->where('item_id', $item_id)->get();

        if (!$fvrs) {
            return false;
        }

        $insertItem = [];
        foreach ($fvrs as $fvr) {
            $insertItem[] = ['language_id' => $fvr->language_id, 'system' => $fvr->system, 'item_id' => $new_item_id
                , 'feature_id' => $fvr->feature_id, 'feature_value_id' => $fvr->feature_value_id
                , 'feature_value' => $fvr->feature_value, 'active' => $fvr->active];
        }

        $featureValueRelation->insert($insertItem);
        return true;
    }

    /**
     * @return $this
     */
    protected function deleteFormMetaGroup()
    {
        $form_meta_group_id = $this->request->get('form_meta_group_id');
        $form_id = $this->request->get('form_id');
        $formMetaGroup = FormMetaGroup::find($form_meta_group_id);
        $resultDelete = null;
        if ($formMetaGroup) {
            $resultDelete = $formMetaGroup->delete();
            if ($resultDelete) {
                $system = 'form_meta_groups';
                $this->deleteFeatureValueRelation($form_meta_group_id, $system);
            }

        }
        return $this->redirectBack()->setResponse($resultDelete ? true : false, trans("core::messages.alert." . ($resultDelete ? 'del_success' : 'del_un_success')));
    }

    /**
     * @param $item_id
     * @param $system
     * @return bool
     */
    protected function deleteFeatureValueRelation($item_id, $system)
    {
        $featureValueRelation = new FeatureValueRelation();
        $fvrs = $featureValueRelation->active()->where('system', $system)->where('item_id', $item_id)->delete();
        if (!$fvrs) {
            return false;
        }
        return true;
    }


}
