<?php

namespace Modules\Core\ViewModels\FormBuilder;


use Illuminate\Http\Request;
use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureValueRelation;
use Modules\Core\Models\Form;
use Modules\Core\Models\Form\FormFeatureValueRelation;
use Modules\Core\Models\Form\FormMetaGroup;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Meta\MetaGroup;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\Traits\Feature\FormFeatureBuilder;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;


class FormBuilderRecordViewModel extends BaseViewModel
{

    use FormFeatureBuilder;

    private $form_assets;
    private $record_id;

    public function __construct()
    {

    }


    public function boot()
    {
        return $this->setRecordId();
    }

    /**
     * @param null $record_id
     * @return $this
     */
    public function setRecordId($record_id = null)
    {
        if (!$this->record_id) {
            $this->record_id = $record_id ?: $this->request->get('record_id');
        }
        return $this;
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $form_assets = [];

        if ($this->form_assets) {
            $form_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/form/formBuilder.js"), 'name' => 'form-backend'];
        }

        return $form_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    protected function show()
    {
        $formRecord = $this->getFormRecord();
        $viewModel =& $this;
        $this->setTitlePage(trans('core::formBuilder.show'));
        return $this->renderedView("core::form_builder.record.form", ['view_model' => $viewModel, 'form_record' => $formRecord], "form");
    }

    public function getFormRecord()
    {
        $this->boot();
        $formRecord = Form\FormRecord::enable()->find($this->record_id);
        return $formRecord;
    }

    /**
     * @param null $record_id
     * @return $this
     */
    protected function destroyRecord($record_id = null)
    {
        $this->boot();
        $record_id = $record_id ?: $this->record_id;
        $formRecord = Form\FormRecord::enable()->find($record_id);

        if ($formRecord && $formRecord->delete()) {
            return $this->setResponse(true, trans("core::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("core::messages.alert.del_un_success"));

    }

    /**
     * @param null $record_id
     * @return $this
     */
    protected function changeStatusRecord($record_id = null)
    {
        $this->boot();
        $record_id = $record_id ?: $this->record_id;
        $formRecord = Form\FormRecord::enable()->find($record_id);
        if ($formRecord) {
            $formRecord->active = $formRecord->active == 1 ? 2 : 1;
            $formRecord->save();
            return $this->setResponse(true, trans("course::messages.alert.update_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.update_un_success"));

    }

}
