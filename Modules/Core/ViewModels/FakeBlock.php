<?php

namespace Modules\Core\ViewModels\Blocks;


use BridgeHelper;
use Modules\Core\Models\Form;
use Modules\Core\Traits\Block\MasterBlock;
use Modules\Core\Traits\Feature\FormFeatureBuilder;

class FakeBlock
{

    use MasterBlock;

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        return view('core::widgets.fake.index')->render();
    }
}
