<?php

namespace Modules\Core\ViewModels\FeatureValue;


use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureValue;
use Modules\Core\Models\Language\Language;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;


class FeatureValueViewModel extends BaseViewModel
{

    use GridViewModel;

    private $feature_assets;
    private $feature_id;

    public function __construct()
    {

    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->Model = FeatureValue::active()->where(function ($q) {
            if ($this->feature_id) {
                $q->where('feature_id', $this->feature_id);
            }
        });
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $feature_assets = [];

        //use bested assets
        if ($this->feature_assets) {
            $feature_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/feature.js"), 'name' => 'feature-backend'];
        }

        return $feature_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('core::feature.feature_value.title'), true)
            ->addColumn('value', trans('core::feature.feature_value.value'), false);

        /*add action*/
//        $add = array(
//            'name' => 'core.feature.values.create',
//            'parameter' => null
//        );
//        $this->addButton('create_feature_value', $add, trans('core::feature.feature_value.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        $show = array(
            'name' => 'core.feature.values.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('core::feature.feature_value.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('core::feature.confirm_delete')];
        $delete = array(
            'name' => 'core.feature.values.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('core::feature.feature_value.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
//        $this->addFilter('value', 'text');
//        $this->addFilter('direction', 'select', ['field' => 'direction', 'options' => $this->getDirections(), 'title' => trans('core::validations.attributes.category')]);
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
//        $row->backend_template_name = isset($row->backendTemplate->name) ? $row->backendTemplate->name : '-';
//        $row->frontend_template_name = isset($row->frontTemplate->name) ? $row->frontTemplate->name : '-';
        return $row;
    }

    /**
     * @return $this
     */
    protected function setAssetsCreateFeatureValue()
    {
        $this->feature_assets = true;
        return $this;
    }

    private function setFeatureId($feature_id = null)
    {
        $this->feature_id = $feature_id;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $languages = Language::active()->get();
        $feature = new Feature();
        $types = $feature->types();
        $features = $feature->active()->isNotCustom()->get();

        $feature = $feature->active()->with(['featureValues','parent.featureValues'])->find($this->request->get('feature_id'));

        $featureValues = isset($feature->parent->featureValues)?$feature->parent->featureValues:null;
        $featureValues = $this->decorateFeatureValues($featureValues);

        $this->setTitlePage(trans('core::feature.feature_value.feature_value_list'));
        $this->setFeatureId($this->request->get('feature_id'))->generateGridList()->renderedView("core::feature.values.index"
            , ['view_model' => $this, 'feature' => $feature,'featureValues'=>$featureValues, 'languages' => $languages
                , 'types' => $types, 'featureValue' => null, 'features' => $features], "feature_value_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createFeatureValue()
    {
        $languages = Language::active()->get();
        $featureValue = new FeatureValue();
        $featureValue = $featureValue->active()->with(['parent','feature.parent.featureValues'])->find($this->request->get('feature_value_id'));
        if($featureValue){
            $featureValue->edit = true;
        }

        $feature = new Feature();
        $types = $feature->types();
        $featureValues = isset($featureValue->feature->parent->featureValues)?$featureValue->feature->parent->featureValues:null;
        $featureValues = $this->decorateFeatureValues($featureValues);

        $feature = isset($featureValue->feature)?$featureValue->feature:null;

        $this->setTitlePage(trans('core::feature.feature_value.' . ($featureValue ? 'edit' : 'add')));
        return $this->renderedView("core::feature.values.form", ['languages' => $languages
            , 'featureValue' => $featureValue,'featureValues'=>$featureValues, 'feature' => $feature, 'types' => $types], "form");
    }

    /**
     * @param $featureValues
     * @return array
     */
    private function decorateFeatureValues($featureValues)
    {
        return $featureValues?$featureValues->mapWithKeys(function ($item){
            return [$item->id => $item->title . ' -> '. $item->value];
        })->toArray():[];
    }

    /**
     * @return $this
     */
    protected function destroyFeatureValue()
    {
        $feature_value_id = $this->request->get('feature_value_id');
        $featureValue = FeatureValue::active()->find($feature_value_id);

        if ($featureValue->delete()) {
            return $this->setResponse(true, trans("core::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("core::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveFeatureValue()
    {
        $featureValue = new FeatureValue();
        if ($feature_value_id = $this->request->get("feature_value_id")) {
            $featureValue = $featureValue->active()->find($feature_value_id);
            if (!$featureValue) {
                return $this->redirect(route('core.feature.values.edit', ['feature_value_id' => $this->request->get('feature_value_id')]))->setResponse(false, trans("core::messages.alert.not_find_data"));
            }
        }

        if (is_int((int)$this->request->get('parent_id')) && !$this->request->get('parent_id')) {
            $this->request->request->set('parent_id', null);
        }

        if (($featureValue->fill($this->request->all())->isValid()) || ($feature_value_id && $featureValue)) {
            $featureValue->save();
            $featureValue->updated = $feature_value_id ? true : false;
            return $this->redirect(route('core.feature.values.edit', ['feature_value_id' => $featureValue->id]))->setDataResponse($featureValue)->setResponse(true, trans("core::messages.alert.save_success"));
        }

        return $this->redirect(route('core.feature.values.create',['feature_id'=>$this->request->get('feature_id')]))->setResponse(false, trans("core::messages.alert.mis_data"), $featureValue->errors);
    }


}
