<?php

namespace Modules\Core\ViewModels\MetaGroup;

use Modules\Core\Models\Feature\FeatureGroup;
use Modules\Core\Models\Meta\MetaGroup;
use Modules\Core\Models\Language\Language;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Core\Models\Meta\MetaGroupFeatureGroup;


class MetaGroupViewModel extends BaseViewModel
{

    use GridViewModel;

    private $meta_group_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = MetaGroup::active();
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $meta_group_assets = [];

        //use bested assets
        if ($this->meta_group_assets) {
            $meta_group_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/meta_group.js"), 'name' => 'meta_group-backend'];
        }

        return $meta_group_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('core::meta_group.title'), true);
        $this->addColumn('system', trans('core::meta_group.systems'), true);
        $this->addColumn('default', trans('core::meta_group.default'), true);

        /*add action*/
        $add = array(
            'name' => 'core.meta.group.create',
            'parameter' => null
        );
        $this->addButton('create_meta_group', $add, trans('core::meta_group.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        /*add action*/
//        $add = array(
//            'name' => 'video.create',
//            'parameter' => null
//        );
//        $this->addAction('add', $add, trans('core::video.add_video'), 'fa fa-plus', false, ['target' => '_blank', 'class' => 'btn btn-sm btn-warning']);


        $show = array(
            'name' => 'core.meta.group.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('core::meta_group.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('core::meta_group.confirm_delete')];
        $delete = array(
            'name' => 'core.meta.group.delete',
            'parameter' => 'id',
        );
        $this->addAction('meta_group_delete', $delete, trans('core::meta_group.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
//        $this->addFilter('id', 'text', ['op' => '=']);
        $this->addFilter('title', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        return $row;
    }

    protected function setAssetsCreateVideo()
    {
        $this->meta_group_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('core::meta_group.list'));
        $this->generateGridList()->renderedView("core::meta_group.index", ['view_model' => $this], "meta_group_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createMetaGroup()
    {
        $languages = Language::active()->get();
        $metaGroup = new MetaGroup();
        $modules = collect(\Module::all())->mapWithKeys(function ($module) {
            $name = strtolower($module->getName());
            return [$name => ['path' => $module->getPath(), 'name' => $name]];
        })->merge($metaGroup->anotherModuleType ? $metaGroup->anotherModuleType : [])->pluck('name', 'name');

        $metaGroup = $metaGroup->active()->with(['language'])->find($this->request->get('meta_group_id'));

        $featureGroups = null;
        $featureGroupsActive = [];
        if ($metaGroup) {

            $featureGroupsActive = MetaGroupFeatureGroup::where('meta_group_id', $metaGroup->id)->get();
            if ($featureGroupsActive) {
                $featureGroupsActive = $featureGroupsActive->pluck('position', 'feature_group_id');
            }
            $featureGroupsActive = $featureGroupsActive->toArray();
            $featureGroups = FeatureGroup::active()->with(['instance', 'language'])->filterInstance()->get();
        }

        $this->setTitlePage(trans('core::meta_group.' . ($metaGroup ? 'edit' : 'add')));
        return $this->renderedView("core::meta_group.form", ['metaGroup' => $metaGroup, 'languages' => $languages, 'featureGroups' => $featureGroups, 'modules' => $modules, 'featureGroupsActive' => $featureGroupsActive], "form");
    }

    /**
     * @return $this
     */
    protected function destroyMetaGroup()
    {
        $meta_group_id = $this->request->get('meta_group_id');
        $metaGroup = MetaGroup::active()->find($meta_group_id);

        if ($metaGroup->delete()) {
            return $this->setResponse(true, trans("core::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("core::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveMetaGroup()
    {
        $metaGroup = new MetaGroup();
        if ($meta_group_id = $this->request->get("meta_group_id")) {
            $metaGroup = $metaGroup->active()->find($meta_group_id);
            if (!$metaGroup) {
                return $this->redirect(route('core.meta.group.edit', ['meta_group_id' => $this->request->get('meta_group_id')]))->setResponse(false, trans("core::messages.alert.not_find_data"));
            }

            $this->saveFeatureGroups($meta_group_id, $this->request->get('feature_group_ids'));
        }

        if (!$this->request->get("default")) {
            $this->request->offsetSet("default", 0);
        }

        if (($metaGroup->fill($this->request->all())->isValid()) || ($meta_group_id && $metaGroup)) {
            $metaGroup->save();
            $metaGroup->updated = $meta_group_id ? true : false;
            return $this->redirect(route('core.meta.group.edit', ['meta_group_id' => $metaGroup->id]))->setDataResponse($metaGroup)->setResponse(true, trans("core::messages.alert.save_success"));
        }

        return $this->redirect(route('video.create'))->setResponse(false, trans("core::messages.alert.mis_data"), $metaGroup->errors);
    }

    /**
     * @param $meta_group_id
     * @param $feature_group_ids
     * @return bool
     */
    protected function saveFeatureGroups($meta_group_id, $feature_group_ids)
    {
        $metaGroupFeatures = MetaGroupFeatureGroup::where('meta_group_id', $meta_group_id)->get();
        $featureGroups = $metaGroupFeatures ? $metaGroupFeatures->pluck("feature_group_id") : [];

        if (empty($feature_group_ids) || !$meta_group_id) {
            return false;
        }

        $positions = $this->request->get('position');
        $insertItems = [];
        $feature_group_ids_new = collect($feature_group_ids)->diff($featureGroups);
        $feature_group_ids_del = collect($featureGroups)->diff($feature_group_ids);

        if($feature_group_ids_del){
            MetaGroupFeatureGroup::where('meta_group_id', $meta_group_id)->whereIn('feature_group_id', $feature_group_ids_del)->delete();
        }

        if ($feature_group_ids_new) {
            foreach ($feature_group_ids_new as $feature_group_id) {
                $insertItems[] = ['meta_group_id' => $meta_group_id, 'feature_group_id' => $feature_group_id, 'position' => isset($positions[$feature_group_id]) ? $positions[$feature_group_id] : 0];
            }
        }

        $result = $insertItems ? MetaGroupFeatureGroup::insert($insertItems) : 0;
        return $result;
    }


}
