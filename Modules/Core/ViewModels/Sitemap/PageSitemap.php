<?php
/**
 * Created by PhpStorm.
 * User: hossein
 * Date: 2/13/19
 * Time: 11:01 AM
 */

namespace Modules\Core\ViewModels\Sitemap;

use Illuminate\Support\Facades\Storage;
use Modules\Core\Models\Page\Page;
use Modules\Core\ViewModels\BaseSitemap;

class PageSitemap extends BaseSitemap
{
    public $changefreq;
    public $priority;
    public $sitemapName;
    public $lastUpdatedAt;
    public $typeUrl;

    public function getSiteMapModel()
    {
        return Page::where('status', 'publish')->get();
    }

    public function createSitemap()
    {
        $this->sitemapName = 'page_sitemap.xml';
        $this->changefreq = 'weekly';
        $this->priority = '0.8';
        $this->typeUrl = 'slag';
        $this->lastUpdatedAt();
        $sitemapModel =& $this;
        Storage::disk('sitemap')->put($this->sitemapName, view('core::sitemap.sitemap', compact('sitemapModel')));

        return ['sitemap_name' => $this->sitemapName, 'last_updated_at' => $this->lastUpdatedAt];

    }

    public function getLink($urlType = null)
    {
        return url("/core/page/show/$urlType");
    }

    public function lastUpdatedAt()
    {
        $this->lastUpdatedAt = Page::latest()->first()->updated_at;
    }
}