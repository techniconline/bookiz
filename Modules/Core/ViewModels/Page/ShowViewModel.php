<?php

namespace Modules\Core\ViewModels\Page;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\Models\Page\Page;


class ShowViewModel extends BaseViewModel
{

    private $slug = false;
    private $preview = false;

    /**
     * get data model
     */
    public function boot()
    {
        $this->slug = $this->request->get('slug');
        if ($this->slug) {
            $this->modelData = Page::where('slag', $this->slug);
            if(!$this->preview){
                $this->modelData = $this->modelData->where("status", Page::PUBLISH_STATUS);
            }
            $this->modelData = $this->modelData->first();
        }
    }

    /**
     * @return $this
     */
    public function getShowPage()
    {
        $this->boot();
        if ($this->slug && !$this->modelData) {
            return $this->redirect(url("/404.html"))->setResponse(false, trans('core::message.alert.not_find_data'));
        }
        BridgeHelper::getAccess()->hasAccessModelOnInstanceAndLanguage($this->modelData);
        $this->layout = $this->modelData->column;
        $this->setTitlePage($this->modelData->title);
        return $this->setDataResponse($this->modelData->content_array, 'route_widgets')->setResponse(true);
    }

    /**
     * @return $this
     */
    public function getPreviewPage()
    {
        $this->preview = true;
        return $this->getShowPage();
    }


}
