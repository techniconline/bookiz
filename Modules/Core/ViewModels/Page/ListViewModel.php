<?php

namespace Modules\Core\ViewModels\Page;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Core\Models\Page\Page;
use DateHelper;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;


    public function setGridModel()
    {
        $this->Model = Page::with(['instance','language'])->filterCurrentInstance()->filterLanguage();
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('core::form.fields.id'), true)
             ->addColumn('instance_name', trans('core::form.fields.instance'), false)
             ->addColumn('language_name', trans('core::form.fields.language'), false)
             ->addColumn('title', trans('core::form.fields.title'), true)
             ->addColumn('slag', trans('core::form.fields.slag'), true)
             ->addColumn('column', trans('core::form.fields.column'), false)
            ->addColumn('status', trans('core::form.fields.status'), true)
            ->addColumn('updated_at', trans('core::form.fields.date'), true);


        $edit = array(
            'name' => 'core.page.edit',
            'parameter' => ['id']
        );
        $this->addAction('edit', $edit, trans('core::form.titles.edit_page'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $options=['class'=>"gridAction ajax btn btn-sm btn-danger"];
        $showUrl=array(
            'name'=>'core.page.url',
            'parameter'=>'id'
        );
        $this->addAction('url',$showUrl,trans('core::form.titles.show_url_page'),'fa fa-link',false,$options);

        $this->action_text = false;

        $routeAdd=route('core.page.edit',['id'=>0]);
        $options=['class'=>"btn btn-primary",'target'=>"_blank"];
        $this->addButton('add',$routeAdd,trans('core::form.titles.create_page'),'fa fa-plus',false,'before',$options);

        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->instance_name=$row->instance->name;
        $row->language_name=$row->language->name;
        $row->column=trans('core::data.columns.'.$row->column);
        $row->status=trans('core::data.page_status.'.$row->status);
        $row->updated_at=DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getPageList()
    {
        $this->setTitlePage(trans('core::form.titles.pages'));
        $this->generateGridList()->renderedView("core::page.list", ['view_model' => $this], trans('core::form.titles.pages'));
        return $this;
    }


}
