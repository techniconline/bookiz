<?php

namespace Modules\Core\ViewModels\Page;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\Models\Page\Page;



class EditViewModel extends BaseViewModel
{

    private $id=0;

    public function boot(){
        $this->id=$this->request->get('id');
        if($this->id){
            $this->modelData=Page::find($this->id);
        }
    }

    public function getEditForm(){
        $this->boot();
        if($this->id && !$this->modelData){
            return $this->redirectBack()->setResponse(false,trans('core::message.alert.not_find_data'));
        }
        BridgeHelper::getAccess()->hasAccessModelOnInstanceAndLanguage($this->modelData);
        $BlockViewModel=&$this;
        if($this->id){
            $title=trans('core::form.titles.edit_page');
        }else{
            $title=trans('core::form.titles.create_page');
        }
        $this->setTitlePage($title);
        return $this->renderedView('core::page.edit',compact('BlockViewModel'));
    }


    public function getPageUrl()
    {
        $this->boot();
        if ($this->id && !$this->modelData) {
            return $this->redirectBack()->setResponse(false, trans('core::message.alert.not_find_data'));
        }
        $BlockViewModel=&$this;
        return $this->setDataResponse(view('core::page.url',compact('BlockViewModel'))->render(), "html")->setResponse(true);
    }

    public function savePage(){
        $this->boot();
        BridgeHelper::getAccess()->hasAccessModelOnInstanceAndLanguage($this->modelData);
        if(($this->id && !$this->modelData)){
            return $this->redirectBack()->setResponse(false,trans('core::message.alert.not_find_data'));
        }
        $instance_id=app('getInstanceObject')->getCurrentInstance()->id;
        if(!$this->modelData){
            $this->modelData=new Page();
            $this->modelData->instance_id= $instance_id;
        }
        $language_id=($this->request->get('language_id'))?$this->request->get('language_id'):app('getInstanceObject')->getCurrentLanguage()->id;
        $rules=[
            'title'=>'required|string',
            'status'=>'required|in:publish,saved,draft',
            'column'=>'required|in:3Column,2ColumnRight,2ColumnLeft,1Column,EmptyColumn,layout',
            'description'=>'required|string',
            'content'=>'required|json',
        ];
        if($this->id && $this->modelData->slag==$this->request->get('slag')){
            $rules['slag']='required|string';
        }else{
            $rules['slag']='required|string|unique:pages,slag,null,null,instance_id,'.$instance_id.',language_id,'.$language_id;
        }
        $this->requestValuesUpdate()->validate($rules);
        $data=$this->request->all();
        $this->modelData->language_id=$language_id;
        $this->modelData->title=$data['title'];
        $this->modelData->slag=$data['slag'];
        $this->modelData->status=$data['status'];
        $this->modelData->column=$data['column'];
        $this->modelData->description=$data['description'];
        $this->modelData->content=$data['content'];
        $result=$this->modelData->save();
        if($result){
            return $this->redirect(route('core.page.index'))->setResponse(true,trans('core::message.alert.success'));
        }
        return $this->redirectBack()->setResponse($result,trans('core::message.alert.error'));

    }

}
