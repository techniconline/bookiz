<?php

namespace Modules\Core\ViewModels\Instance;


use Modules\Core\Models\Instance\Instance;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\Models\Instance\InstanceSpecialBlock;


class BlocksViewModel extends BaseViewModel
{

    private $block;

    public function getBlocksHeaderForm(){
        $instance_id = $this->request->get('instance_id');
        $this->modelData = Instance::find($instance_id);
        if (!$this->modelData) {
            return $this->redirectBack()->setResponse(false, trans("core::messages.alert.not_find_data"));
        }
        $BlockViewModel=&$this;
        $this->setTitlePage(trans('core::form.titles.special_blocks_header',['name'=>$this->modelData->name]));
        return $this->renderedView('core::instance.blocks_header',compact('BlockViewModel'));
    }

    public function getBlocksFooterForm(){
        $instance_id = $this->request->get('instance_id');
        $this->modelData = Instance::find($instance_id);
        if (!$this->modelData) {
            return $this->redirectBack()->setResponse(false, trans("core::messages.alert.not_find_data"));
        }
        $BlockViewModel=&$this;
        $this->setTitlePage(trans('core::form.titles.special_blocks_footer',['name'=>$this->modelData->name]));
        return $this->renderedView('core::instance.blocks_footer',compact('BlockViewModel'));
    }


    public function saveBlocksHeader(){
        return $this->saveBlocks('header');
    }

    public function saveBlocksFooter(){
        return $this->saveBlocks('footer');
    }

    public function saveBlocks($name='header'){
        $instance_id = $this->request->get('instance_id');
        $this->modelData = Instance::find($instance_id);
        if (!$this->modelData) {
            return $this->redirectBack()->setResponse(false, trans("core::messages.alert.not_find_data"));
        }
        $this->get‌Blocks();
        if(!$this->block){
            $this->block=new InstanceSpecialBlock;
            $this->block->instance_id=$this->modelData->id;
            $blockArray=['header'=>[],'footer'=>[]];
            $this->block->name='header_footer_default';
        }else{
            $blockArray=$this->block->json_array;
        }
        $blockArray[$name]=json_decode($this->request->get($name),true);
        $this->block->blocks=$blockArray;
        $result=$this->block->save();
        if($result){
            return $this->redirectBack()->setResponse($result,trans('messages.alert.success'));
        }
        return $this->redirectBack()->setResponse($result,trans('messages.alert.error'));
    }

    public function get‌Blocks($name=null){
        $this->block=InstanceSpecialBlock::where('instance_id',$this->modelData->id)->first();

        if(!$this->block){
            return false;
        }
        if(is_null($name)){
            return $this->block->blocks;
        }

        if(isset($this->block->blocks->{$name})){
         return $this->block->blocks->{$name};
        }
        return null;
    }



}
