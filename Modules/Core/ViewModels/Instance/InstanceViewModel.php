<?php

namespace Modules\Core\ViewModels\Instance;

use Modules\Core\Models\Currency\Currency;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;
use Modules\Core\Models\Template\Template;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;


class InstanceViewModel extends BaseViewModel
{

    use GridViewModel;

    private $create_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $currentInstance = app('getInstanceObject')->getCurrentInstance();
        $this->Model = Instance::active()->where("id", $currentInstance?$currentInstance->id:0)->with(['frontTemplate', 'backendTemplate', 'currency', 'language'])->filterLanguage();
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $assets = [];

        //use bested assets
        if ($this->create_assets) {
            $assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/instance.js"), 'name' => 'instance-backend'];
        }

        return $assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('name', trans('core::instance.name'), true)
            ->addColumn('app_name', trans('core::instance.app_name'), false)
            ->addColumn('default', trans('core::instance.default'), false)
            ->addColumn('backend_template_name', trans('core::instance.backend_theme'), false)
            ->addColumn('frontend_template_name', trans('core::instance.front_theme'), false);

        /*add action*/
        $add = array(
            'name' => 'core.instance.create',
            'parameter' => null
        );
        $this->addButton('create_instance', $add, trans('core::instance.add_instance'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);


        /*add action*/
//        $add = array(
//            'name' => 'core.instance.create',
//            'parameter' => null
//        );
//        $this->addAction('add', $add, trans('core::instance.add_language'), 'fa fa-plus', false, ['target' => '_blank', 'class' => 'btn btn-sm btn-warning']);


        $show = array(
            'name' => 'core.instance.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('core::instance.edit_instance'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('core::instance.confirm_delete')];
        $delete = array(
            'name' => 'core.instance.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('core::instance.delete_instance'), 'fa fa-trash', false, $options);

        $special_blocks_header = array(
            'name' => 'core.instance.special_blocks_header',
            'parameter' => ['id']
        );
        $this->addAction('special_blocks_header', $special_blocks_header, trans('core::instance.special_blocks_header'), 'fa fa-file', false, ['target' => '', 'class' => 'btn btn-sm btn-info']);

        $special_blocks_footer = array(
            'name' => 'core.instance.special_blocks_footer',
            'parameter' => ['id']
        );
        $this->addAction('special_blocks_footer', $special_blocks_footer, trans('core::instance.special_blocks_footer'), 'fa fa-file', false, ['target' => '', 'class' => 'btn btn-sm btn-info']);


        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('name', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->backend_template_name = isset($row->backendTemplate->name) ? $row->backendTemplate->name : '-';
        $row->frontend_template_name = isset($row->frontTemplate->name) ? $row->frontTemplate->name : '-';
        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('core::instance.instance_list'));
        $this->generateGridList()->renderedView("core::instance.index", ['view_model' => $this], "instance_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createInstance()
    {
        $instance = Instance::active()->find($this->request->get('instance_id'));
        $currencies = Currency::active()->get();
        $currencies = $currencies ? $currencies->pluck('name', 'id') : [];

        $backend_templates = Template::active()->where('backend', 1)->get();
        $frontend_templates = Template::active()->where('backend', 0)->get();

        $backend_templates = $backend_templates ? $backend_templates->pluck('name', 'id') : [];
        $frontend_templates = $frontend_templates ? $frontend_templates->pluck('name', 'id') : [];

        $this->setTitlePage(trans('core::instance.' . ($instance ? 'edit_instance' : 'add_instance')));
        return $this->renderedView("core::instance.form", ['instance' => $instance
                , 'currencies' => $currencies
                , 'frontend_templates' => $frontend_templates
                , 'backend_templates' => $backend_templates
            ]
            , "form_instance");
    }

    /**
     * active assets for method called!
     * @return $this
     */
    protected function setAssetsCreateInstance()
    {
        $this->create_assets = true;
        return $this;
    }

    /**
     * @return $this
     */
    protected function destroyInstance()
    {
        $instance_id = $this->request->get('instance_id');
        $instance = Instance::active()->find($instance_id);

        if ($instance->delete()) {
            return $this->setResponse(true, trans("core::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("core::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveInstance()
    {
        $instance = new Instance();
        if ($instance_id = $this->request->get("instance_id")) {
            $instance = $instance->active()->find($instance_id);
            if (!$instance) {
                return $this->redirect(route('core.instance.edit', ['instance_id' => $this->request->get('instance_id')]))->setResponse(false, trans("core::messages.alert.not_find_data"));
            }
        }

        $this->request->request->set('default', $this->request->get('default', 0));
        $this->request->request->set('frontend_template_id', $this->request->get('frontend_template_id', null));
        $this->request->request->set('backend_template_id', $this->request->get('backend_template_id', null));

        if (($instance->fill($this->request->all())->isValid()) || ($instance_id && $instance)) {
            $instance->save();
            $instance->updated = $instance_id ? true : false;
            return $this->redirect(route('core.instance.edit', ['instance_id' => $instance->id]))->setDataResponse($instance)->setResponse(true, trans("core::messages.alert.save_success"));
        }

        return $this->redirect(route('core.instance.create'))->setResponse(false, trans("core::messages.alert.mis_data"), $instance->errors);
    }

    /**
     * @return $this
     */
    protected function changerInstance()
    {
        $current_instance_id = $this->request->get("current_instance_id");
        if ($current_instance_id) {
            session()->put('current_instance_id', $current_instance_id);
        }
        return $this->redirectBack()->setResponse(true);
    }


}
