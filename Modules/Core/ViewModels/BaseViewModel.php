<?php

namespace Modules\Core\ViewModels;

use DateHelper;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use BridgeHelper;
use Modules\Core\Traits\Decorate\DecorateData;
use Modules\Core\Traits\Page\PageMeta;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;


class BaseViewModel implements InterfaceViewModel
{

    use ValidatesRequests;
    use DecorateData;
    use PageMeta;

    public $method;
    public $access;
    public $layout;
    public $cache_info;
    public $response;
    public $message;
    public $view;
    public $theme;
    protected $errors;
    public $assets;
    public $asset_version;
    public $request;
    public $data;
    public $modelData = false;
    protected $config;
    protected $file_download;
    private $dataName = 'data';
    protected $is_api;
    protected $breadCrumbs = [];
    public $useModel; //for validation -- is model
    public $isValidRequest = true;
    public $perPage=15;

    public function __construct()
    {
        $this->is_api = app('getInstanceObject')->isApi();
        $this->request = \request();
        $this->perPage = $this->request->get("perPage", 15);
    }

    /**
     * @param int $perPage
     * @return $this
     */
    public function setPerPage($perPage = 15)
    {
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @param $config
     * @return $this
     */
    public function setConfig($config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->useModel;
    }

    /**
     * @param $model
     * @return $this
     */
    public function setModel($model)
    {
        $this->useModel = $model;
        return $this;
    }



    /**
     * @param Request $request
     * @return $this
     */
    public function setRequest(Request $request = null)
    {
        $this->request = $request ?: \request();
        return $this;
    }

    /**
     * @param $method
     * @return $this|mixed
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }


    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return null;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        return $this->assets;
    }

    /**
     * @return array|mixed
     */
    public function getAssetVersion()
    {
        //
    }

    /**
     * @return array|mixed
     */
    public function setAssetVersion()
    {
        $this->asset_version = $this->getAssetVersion();
        return $this;
    }

    /**
     * @return $this
     */
    protected function setAssetsFunction()
    {
        if ($this->method && !$this->isApiRequest()) {
            try {
                $this->{'setAssets' . ucfirst($this->method)}();
            } catch (\Throwable $ex) {
                //
            }
        }
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function setAssets()
    {
        $this->assets = $this->setAssetsFunction()->setAssetVersion()->getAssets();
        $this->assets = collect($this->assets)->map(function ($item) {
            $item["src"] = $item["src"] . ($this->asset_version ? '?_v_m=' . $this->asset_version . '.' . pathinfo($item["src"], PATHINFO_EXTENSION) : null);
            return $item;
        })->toArray();

        return $this;
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        if (!$this->layout) {
            $this->layout = Config::get('theme.layoutDefault');
        }
        return $this;
    }

    /**
     * @return $this|mixed
     */
    public function getTheme()
    {
        $instance = app("getInstanceObject")->getInstance();
        $key = "instance_template_".$instance->id;
        if(!Cache::has($key)){
            $theme = $instance && $instance->frontend_template_id ? $instance->frontTemplate : ($instance && $instance->backend_template_id ? $instance->backendTemplate : null);
            Cache::forever($key,$theme);
        }else{
            $theme = Cache::get($key);
        }
        $useTheme = $theme ? $theme->code : Config::get('theme.themeDefault');
        $this->theme = $useTheme;
        return $this;
    }

    /**
     * @return mixed|void
     */
    public function getCacheInfo()
    {

    }

    /**
     * @return mixed|void
     */
    public function helperData()
    {

    }

    /**
     * @return bool
     */
    protected function isAjaxRequest()
    {
        if (request()->ajax()) {
            return true;
        } elseif (request()->get('is_ajax', false)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function response()
    {
        return $this->call();
    }

    /**
     * @return mixed
     */
    public function call()
    {
        if (method_exists($this, $this->method)) {

            if ($this->isValidRequest()->isValidRequest) {
                $response=$this->{$this->method}();
                if($response instanceof \Symfony\Component\HttpFoundation\Response || $response instanceof \Illuminate\Contracts\Routing\ResponseFactory){
                    return $response;
                }
                return $response->getResponse();
            } else {
                return $this->redirectBack()->setResponse(false, trans("core::messages.alert.mis_data"), $this->errors)->getResponse();
            }

        } else {
            throw new HttpException(404, 'Not Find ViewModelMethod.');
        }
    }

    /**
     * @return bool
     */
    private function hasRedirect()
    {
        if ($this->response) {
            if (isset($this->response["redirect"]) || isset($this->response["redirect_back"])) {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        if (!$this->response) {
            $this->response["msg"] = $this->message ?: "Not Call Method!";
        }

        $this->response["has_redirect"] = $this->hasRedirect();

        if (!$this->isAjaxRequest() && !$this->response["has_redirect"]) {

            $this->getLayout()->getTheme()->setAssets();
            $this->response["theme"] = $this->theme;
            $this->response["layout"] = $this->layout;
            $this->response["assets"] = $this->assets;
            $this->response["title_page"] = $this->getTitlePage();
            $this->response["mete"] = $this->getPageMeta();

        }
        return $this->response;
    }

    /**
     * @param bool $action
     * @param null $message
     * @param array $errors
     * @param array $result_items
     * @return $this
     */
    protected function setResponse($action = true, $message = null, $errors = [], $result_items = [])
    {
        $this->response['action'] = $action;
        if ($this->file_download) {
            $this->response['file_download'] = $this->file_download;
        }
        if ($this->data) {
            $this->response[$this->dataName] = $this->data;
        }
        if ($message) {
            $this->response['message'] = $message;
        }
        if ($errors) {
            $this->response['errors'] = $errors;
        }
        if ($result_items) {
            $this->response['result_items'] = $result_items;
        }
        if(app('getInstanceObject')->isApi()){
            $this->response['quest_token'] = app('getInstanceObject')->getUserToken();
        }
        return $this->setMessageInSession();
    }

    /**
     * @return $this
     */
    protected function setMessageInSession()
    {
        if (!$this->isAjaxRequest()) {
            if (isset($this->response['message'])) {
//                Session::flash("message", $this->response['message']);
                if (isset($this->response['action'])) {
                    if ($this->response['action']) {
                        Session::flash("success", $this->response['message']);
                    } else {
                        Session::flash("error", $this->response['message']);
                    }
                }
            }

            if (isset($this->response['errors'])) {
                Session::flash("errors", $this->response['errors']);
            }
        }
        return $this;
    }

    /**
     * @param $data
     * @param string $name
     * @return $this
     */
    protected function setDataResponse($data, $name = 'data')
    {
        $this->data = $data;
        $this->dataName = $name;
        return $this;
    }

    /**
     * @param $file
     * @return $this
     */
    protected function setFileResponse($file)
    {
        $this->file_download = $file;
        return $this;
    }

    /**
     * @param $view
     * @param array $data
     * @param null $title
     * @return $this
     * @throws \Throwable
     */
    public function renderedView($view, $data = [], $title = null)
    {
        if (!$this->isApiRequest()) {
            $this->view = view($view, $data)->render();
            $this->response["content"] = $this->view;
        } else {
            foreach ($data as $key => $datum) {
                if (is_object($datum)) {
                    if (method_exists($datum, 'toArray')) {
                        $this->response["content"][$key] = $datum->toArray();
                    }
                } else {
                    $this->response["content"][$key] = $datum;
                }
            }

        }
        return $this;
    }

    /**
     * @param $url
     * @param array $with_inputs
     * @return $this
     */
    public function redirect($url, $with_inputs = [])
    {
        if ($url) {
            $this->response['redirect'] = $url;
            $this->response['with_input_redirect'] = $with_inputs;
        }
        return $this;
    }

    /**
     * @param array $with_inputs
     * @return $this
     */
    protected function redirectBack($with_inputs=[])
    {
        $this->response['redirect_back'] = true;
        $this->response['with_input_redirect'] = $with_inputs;
        return $this;
    }

    /**
     * @return bool
     */
    protected function isApiRequest()
    {
        if (request()->get('is_api', false)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * @param array $dateFields
     * @return mixed
     */
    protected function requestValuesUpdate($dateFields = [])
    {
        if (!$this->request) {
            $this->request = new Request();
        }
        $data = $this->requestDataUpdate($this->request->all(), $dateFields);
        $this->request->merge($data);
        return $this->request;
    }

    /**
     * @param $data
     * @param array $dateFields
     * @return mixed
     */
    protected function requestDataUpdate($data, $dateFields = [])
    {
        foreach ($data as $key => $values) {
            if (is_object($values)) {
                unset($data[$key]);
            } elseif (is_array($values)) {
                $data[$key] = $this->requestDataUpdate($values, $dateFields);
            } else {
                if (strlen($values) == 0) {
                    $values = null;
                } else {
                    $values = $this->persianNumberReplace($values);
                    if (isset($dateFields[$key])) {
                        $values = DateHelper::setLocaleDateTime($values, $dateFields[$key])->getDateTime($dateFields[$key]);
                    }
                }
                $data[$key] = $values;
            }
        }
        return $data;
    }


    /**
     * @param $value
     * @return mixed
     */
    private function persianNumberReplace($value)
    {
        $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
        $english = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        return str_replace($persian, $english, $value);
    }


    /**
     * @param $modelData
     * @return $this
     */
    public function setModelData($modelData)
    {
        $this->modelData = $modelData;
        return $this;
    }


    /**
     * @param null $name
     * @param null $return
     * @return bool|mixed|null
     */
    public function getModelData($name = null, $return = null)
    {
        if (is_null($name)) {
            return $this->modelData;
        }
        if (is_object($this->modelData)) {
            if (isset($this->modelData->{$name})) {
                return $this->modelData->{$name};
            } else {
                return $return;
            }
        }
        if (is_array($this->modelData)) {
            if (isset($this->modelData[$name])) {
                return $this->modelData[$name];
            }
        }
        return $return;
    }

    /**
     * @param null $key
     * @param null $default
     * @return null
     */
    public function getModelDataRelation($key = null, $default = null)
    {
        if ($this->modelData){

            try{
                $relations = $this->modelData->getRelations();
                if (!$key){
                    return $relations;
                }
                $keyArr = explode(".", $key);
                $first = array_first($keyArr);
                $relation = isset($relations[$first])?$relations[$first]:null;

                if ($relation){
                    unset($keyArr[0]);
                    $obj = $relation;
                    $counter = 0;
                    foreach ($keyArr as $item) {
                        $obj = !$counter ? $obj->$item : $item;
                    }
                    return $obj;
                }
            }catch (\Exception $exception){
                report($exception);
                return $default;
            }

        }
        return $default;
    }


    /**
     * @param $name
     * @param array $data
     * @param string $module
     * @return string
     * @throws \Throwable
     */
    public function getComponent($name, $data = [], $module = 'core')
    {
        return view($module . '::component.' . $name, $data)->render();
    }


    public function getWidget($block_name,$data){
        $widget=BridgeHelper::getBlockHelper()->getBlock($block_name);
        if (!$widget) {
            return false;
        }
        if (app('getInstanceObject')->isApi()) {
            if(method_exists($widget, "renderJson")){
                return $widget->setConfigs($data)->renderJson();
            }else{
                return false;
            }
        }
        return $widget->setConfigs($data)->render();
    }
    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function setRelationalData($name, $value)
    {
        app('getInstanceObject')->setRelationalData($name, $value);
        return $this;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getRelationalData($name)
    {
        return app('getInstanceObject')->getRelationalData($name);
    }

    /**
     * @param null $attributes
     * @param null $rules
     * @param null $messages
     * @param string $method
     * @param null $model
     * @return $this
     */
    public function isValidRequest($attributes = null, $rules = null, $messages = null, $method = null, $model = null)
    {
        $method = $method?:($this->request?$this->request->method():null);
        if (in_array(strtoupper($method), ["PUT", "POST"])) {
            $model = $model?:$this->useModel;
            if(!$model){
                return $this;
            }
            $attributes = $attributes ?: ($this->request?$this->request->all():[]);
            $rules = $rules ?: ($model?$model->rules:[]);
            $messages = $messages ?: ($model?$model->messages:[]);
            $validation = Validator::make($attributes, $rules, $messages);

            if ($validation->fails()) {
                $this->errors = $validation->messages();
                if($model){
                    $model->errors = $this->errors;
                }
                $this->isValidRequest = false;
            }
        }
        return $this;
    }

}
