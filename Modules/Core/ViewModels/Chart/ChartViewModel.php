<?php

namespace Modules\Core\ViewModels\Chart;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;

trait ChartViewModel
{
    public $chart;
    public $by_filter_chart = true;
    private $chart_type = "line";
    private $chart_title;
    private $chart_name;
    private $chart_options = [];
    private $chart_options_raw;
    private $chart_size;
    private $chart_labels;
    private $chart_datasets;
    private $chart_data = [];
    private $chart_models = [];
    private $valid_chart_models = [];
    private $options;
    private $chart_filters;
    private $chart_filter_values;
    private $filter_models = false;
    private $group_by_filter;
    private $used_group_by_filter;
    private $model_used_group_by_filter;
    private $fields;
    private $action_url;

    private $chart_x_field;
    private $chart_y_field;
    private $with_grid = false;

    /**
     *
     */
    public function bootChart()
    {
        if (get_instance()->isAjax()) {
            $this->by_filter_chart = false;
        }

        return $this->decorateData();
    }

    /**
     *
     */
    public function bootChartOnly()
    {
        $this->chart = app()->chartjs
            ->name($this->chart_name)
            ->type($this->chart_type)
            ->size($this->chart_size)
            ->labels($this->chart_labels)
            ->datasets(array_values($this->chart_datasets))
            ->options($this->chart_options);
    }


    /**
     * @param $key
     * @param null $type
     * @return bool
     */
    public function hasFilterColumn($key, $type = null)
    {
        $filter_key = collect($this->chart_filters)->filter(function ($item, $k) use ($key, $type) {
            if ($k == $key) {
                if ($type) {
                    if (isset($item["type"]) && $item["type"] == $type) {
                        return $item;
                    }
                } else {
                    return $item;
                }
            }
        })->keys()->first();

        if ($filter_key) {
            if ($type == 'date') {
                $arr = ["_from", "_to"];
                foreach ($arr as $item) {
                    if (\request()->get($filter_key . $item)) {
                        return true;
                    }
                }
            } elseif (\request()->get($filter_key)) {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $x
     * @return $this
     */
    public function setXField($x)
    {
        $this->chart_x_field = $x;
        return $this;
    }

    /**
     * @param $y
     * @param $title
     */
    public function setYField($y, $title)
    {
        $this->chart_y_field[$y] = $title;
    }

    /**
     * @return array|\Illuminate\Http\Request|string
     */
    public function getYFields()
    {
        $y_request = \request("y_fields");
        $y_fields = $y_request ? $y_request : null;
        if ($y_fields) {
            $chart_y_field = [];
            foreach ($y_fields as $item) {
                if (isset($this->chart_y_field[$item])) {
                    $chart_y_field[$item] = $this->chart_y_field[$item];
                }
            }
            if (!empty($chart_y_field)) {
                $y_fields = $chart_y_field;
            }
        } else {
            $y_fields = $this->chart_y_field;
        }
        return $y_fields;
    }

    /**
     * @return $this
     */
    public function initChartGrid()
    {
        $this->setChartType(\request("chart_type", $this->chart_type));

        $rows = $this->getRows();
        $x_field = $this->chart_x_field;
        $y_fields = $this->getYFields();
        foreach ($rows as $row) {
            $rowDecorated = $this->getRowsUpdate($row);
            $this->chart_labels[] = isset($rowDecorated->$x_field) ? $rowDecorated->$x_field : '-';
            foreach ($y_fields as $y => $y_title) {
                if (isset($rowDecorated->getAttributes()[$y])) {
                    $this->chart_data[$y]['title'] = $y_title;
                    $this->chart_data[$y]['data'][] = $rowDecorated->getAttributes()[$y];
                }
            }

        }

        if (empty($this->chart_data)) {
            return $this;
        }

        foreach ($this->chart_data as $yKey => $datum) {
            $dataSet[] = [
                "label" => isset($datum["title"]) && $datum["title"] ? $datum["title"] : null,
                "data" => isset($datum["data"]) && $datum["data"] ? $datum["data"] : null,
            ];
            $this->setDataSets($yKey, $dataSet);
        }
        return $this;
    }


    /**
     *  decorate data for chart
     */
    private function decorateData()
    {
        $label = [];
        $labels = [];
        $type = $format = null;
        foreach ($this->chart_data as $key => $item) {
            $label[$key] = array_keys($item);
            $labels = array_sort(array_unique(array_merge($labels, $label[$key])));
            $type = isset($this->model_used_group_by_filter[$key]["type"]) ? $this->model_used_group_by_filter[$key]["type"] : null;
            $format = isset($this->model_used_group_by_filter[$key]["format"]) ? $this->model_used_group_by_filter[$key]["format"] : null;
        }

        $dataSet = [];
        foreach ($this->chart_data as $key => $chart_datum) {
            foreach ($labels as $label) {
                $dataSet[$key][] = isset($chart_datum[$label]) ? $chart_datum[$label] : 0;
            }
            if (isset($this->chart_datasets[$key])) {
                $this->chart_datasets[$key]["data"] = isset($dataSet[$key]) ? $dataSet[$key] : [];
            }
        }

        if ($type == "date") {
            $format = $format ?: "Y-m-d";
            foreach ($labels as &$label) {
                $label = DateHelper::setDateTime($label, null, $format)->getLocaleFormat($format);
            }
        }
        $this->chart_labels = $labels;
        return $this;
    }

    /**
     * @return array
     */
    public function getValidModels()
    {
        if (!$this->filter_models) {
            return null;
        }
        return $this->valid_chart_models;
    }

    /**
     * @param $name
     * @return bool
     */
    private function checkValidModel($name)
    {
        $models = \request("valid_models");

        if ($models && in_array($name, $models)) {
            return true;
        }
        if (empty($models) && $this->valid_chart_models[$name]) {
            return true;
        }
        return false;
    }

    /**
     * @return $this
     */
    public function initChart()
    {
        $this->setChartType(\request("chart_type", $this->chart_type));
        if ($models = $this->chart_models) {
            foreach ($models as $name => $model) {
                if (!$this->checkValidModel($name)) {
                    continue;
                }

                $modelData = isset($model["model"]) && $model["model"] ? $model["model"] : null;
                if (!$modelData) {
                    continue;
                }

                $modelData = $this->setFilterModel($modelData);
                $group_by = isset($model["group_by"]) && $model["group_by"] ? $model["group_by"] : null;
                if ($group_by) {
                    $modelData = $this->setGroupByModel($modelData, $group_by, $name);
                }
                $fields = isset($model["fields"]) && $model["fields"] ? $model["fields"] : null;
                $result = collect([]);
                if ($fields) {
                    $result = $this->getResultFieldModel($modelData, $fields, $name);
                }
                $this->chart_data[$name] = $result->toArray();
                $dataSet[] = [
                    "label" => isset($model["title"]) && $model["title"] ? $model["title"] : null,
                    "data" => $result->values()->toArray(),
                ];
                $this->setDataSets($name, $dataSet);
            }

        }
        return $this;
    }


    /**
     * @param $model
     * @param $fields
     * @param $name
     * @return mixed
     */
    private function getResultFieldModel($model, $fields, $name)
    {
        $result = $arrGet = $arrPluck = [];
        foreach ($fields as $key => $item) {
            if (isset($item['selected']) && $item['selected'] && isset($item['field_name']) && $item['field_name']) {
                $arrGet[] = DB::raw($item['selected']);
                $arrPluck[$key] = $item['field_name'];
            } elseif (isset($item['field_name']) && $item['field_name'] && isset($this->used_group_by_filter[$item['field_name']])) {
                $arrGet[] = DB::raw($this->used_group_by_filter[$item['field_name']] . ' as ' . $item['field_name']);
                $arrPluck[$key] = $item['field_name'];
            }
        }

        if (count($arrGet) == 2 && isset($arrPluck["y"]) && isset($arrPluck["x"])) {
            $result = $model->get($arrGet);
            $result = $result ? $result->pluck($arrPluck["y"], $arrPluck["x"]) : [];
            $labels = collect($result)->keys()->toArray();
            $this->setChartLabels($labels, $name);
        }
        return $result;
    }

    /**
     * @return mixed
     */
    private function getRandomColor()
    {
        $colors = ['Red', 'Orange', 'Yellow', 'Green', 'Blue'];
        return array_random($colors);
    }

    /**
     * @param $name
     * @param array $dataSets
     * @return $this
     */
    public function setDataSets($name, $dataSets = [])
    {

        foreach ($dataSets as $index => $dataSet) {

            if ($this->chart_type == 'pie') {
                $this->chart_datasets[$name]["label"] = isset($dataSet['label']) ? $dataSet['label'] : "Label " . $index;

                foreach ($dataSet['data'] as $datum) {
                    $this->chart_datasets[$name]["backgroundColor"][] = $this->getRandomColor();
                    $this->chart_datasets[$name]["data"][] = $datum;
                }

            } else {
                $this->chart_datasets[$name] = [
                    "label" => isset($dataSet['label']) ? $dataSet['label'] : "Label " . $index,
                    "backgroundColor" => isset($dataSet['backgroundColor']) ? $dataSet['backgroundColor'] : "rgba(" . rand(50, 255) . ", " . rand(100, 200) . ", " . rand(1, 100) . ", " . (rand(5, 10) / 25) . ")",
                    "borderColor" => isset($dataSet['borderColor']) ? $dataSet['borderColor'] : "rgba(" . rand(50, 255) . ", " . rand(100, 200) . ", " . rand(1, 100) . ", " . (rand(15, 18) / 25) . ")",
                    "pointBorderColor" => isset($dataSet['pointBorderColor']) ? $dataSet['pointBorderColor'] : "rgba(" . rand(50, 255) . ", " . rand(100, 200) . ", " . rand(1, 100) . ", " . (rand(19, 23) / 25) . ")",
                    "pointBackgroundColor" => isset($dataSet['pointBackgroundColor']) ? $dataSet['pointBackgroundColor'] : "rgba(" . rand(50, 255) . ", " . rand(100, 200) . ", " . rand(1, 100) . ", " . (rand(20, 23) / 25) . ")",
                    "pointHoverBackgroundColor" => isset($dataSet['pointHoverBackgroundColor']) ? $dataSet['pointHoverBackgroundColor'] : "#fff",
                    "pointHoverBorderColor" => isset($dataSet['pointHoverBorderColor']) ? $dataSet['pointHoverBorderColor'] : "rgba(" . rand(100, 255) . ", " . rand(50, 200) . ", " . rand(50, 255) . ", 1)",
                    "data" => isset($dataSet['data']) ? $dataSet['data'] : [],
                ];
            }

        }

        return $this;
    }

    /**
     * if request has group_by item
     *  set in $group_by
     *
     * @param $model
     * @param $group_by
     * @return mixed
     */
    private function setGroupByModel($model, $group_by, $model_name)
    {
        if ($values = \request()->get("group_by")) {
            $group_by = [$values];
        }

        foreach ($group_by as $name) {
            if ($name) {
                $value = $this->getGroupByItem($name);
                if ($value) {
                    $field_name = $this->getGroupByItem($name, 'field_name');
                    $this->used_group_by_filter[$field_name] = $value;
                    $this->model_used_group_by_filter[$model_name]["type"] = $this->getGroupByItem($name, 'type');
                    $this->model_used_group_by_filter[$model_name]["format"] = $this->getGroupByItem($name, 'format');
                    $model = $model->groupBy(DB::raw($value));
                }
            }
        }
        return $model;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getVariableChart($key)
    {
        return $this->{$key};
    }

    /**
     * @param $name
     * @param $title
     * @param $model
     * @param array $fields = [x=>['field_name'=>value, 'selected'=>value], y=>['field_name'=>value, 'selected'=>value]] ;
     *                          sample:=> [x=> ['field_name'=>created_time, 'selected'=>DATE(created_at) as created_time]]
     * @param string $out_put
     * @param array $group_by : ['name'];
     * @return $this
     */
    public function setChartModel($name, $title, $model, $fields, $group_by)
    {
        $this->chart_models[sha1($name)] = ["name" => $name, "title" => $title, "model" => $model, "fields" => $fields, "group_by" => $group_by];
//        if ($this->filter_models) {
        $this->setValidModel(sha1($name), $title);
//        }
        return $this;
    }

    /**
     * @param $name
     * @param $title
     * @return $this
     */
    public function setValidModel($name, $title)
    {
        $this->valid_chart_models[$name] = $title;
        return $this;
    }

    /**
     * @param $name
     * @param array $item : [value=>'DATE_FORMAT(created_at, "%m-%Y-%d")', 'title'=>trans('xxx')];
     * @return $this
     */
    public function setGroupByItems($name, $item = [])
    {
        $this->group_by_filter[$name] = $item;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroupByItems()
    {
        $values = collect($this->group_by_filter)->mapWithKeys(function ($item, $key) {
            return [$key => $item["title"]];
        });
        return $values;
    }

    /**
     * @param $name
     * @param string $key
     * @return null
     */
    private function getGroupByItem($name, $key = 'value')
    {
        if (isset($this->group_by_filter[$name][$key])) {
            return $this->group_by_filter[$name][$key];
        }
        return null;
    }

    /**
     * @param $name
     * @param $type
     * @param array $options
     * @return $this
     */
    public function setFilter($name, $type, $options = [])
    {
        $this->chart_filters[$name] = ["title" => isset($options["title"]) ? $options["title"] : $name, "type" => $type, "options" => $options];
        if ($type == "date") {
            $this->chart_filters[$name]["title_from"] = isset($options["title_from"]) ? $options["title_from"] : $name . " from";
            $this->chart_filters[$name]["title_to"] = isset($options["title_to"]) ? $options["title_to"] : $name . " to";

            $formatFrom = isset($this->chart_filters[$name]["options"]["date_format"]) ? $this->chart_filters[$name]["options"]["date_format"] : trans('core::date.datetime.medium');

            $from = $name . "_from";
            $to = $name . "_to";

            if ($date_from = \request($from)) {
                \request()->offsetSet($from, DateHelper::setLocaleDateTime($date_from, $formatFrom)->getDateTime($formatFrom));
            }

            if ($date_to = \request($to)) {
                \request()->offsetSet($to, DateHelper::setLocaleDateTime($date_to, $formatFrom)->getDateTime($formatFrom));
            }
        }
        return $this;
    }

    /**
     * @param null $values
     * @return $this
     */
    public function setFilterTypeChart($values = null)
    {
        return $this->setFilter("chart_type", "select", ["values" => $values ?: trans('core::chart.chart_types'), "title" => trans("core::chart.chart_type_text")]);
    }

    /**
     * @param null $name
     * @return null
     */
    public function getFiltersChart($name = null)
    {
        if ($name) {
            if (isset($this->chart_filters[$name])) {
                return $this->chart_filters[$name];
            } else {
                return null;
            }
        }
        return $this->chart_filters;
    }

    /**
     * @param $url
     * @return $this
     */
    public function setActionUrl($url)
    {
        $this->action_url = $url;
        return $this;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setNameChart($name)
    {
        $this->chart_name = $name;
        return $this;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitleChart($title)
    {
        $this->chart_title = $title;
        return $this;
    }

    /**
     * @param string $type is : bar, line, pie, radar
     * @return $this
     */
    public function setChartType($type = 'line')
    {
        $this->chart_type = $type;
        return $this;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setChartOptions($options = [])
    {
        $this->chart_options = $options;
        return $this;
    }

    /**
     * @param string $optionsRaw
     * @return $this
     */
    public function setChartOptionsRaw($optionsRaw = '')
    {
        $this->chart_options_raw = $optionsRaw;
        return $this;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setChartSize($options = ['width' => 400, 'height' => 300])
    {
        $this->chart_size = $options;
        return $this;
    }

    /**
     * @param array $labels
     * @param null $name
     * @return $this
     */
    public function setChartLabels($labels = [], $name = null)
    {
        $this->chart_labels[$name] = $labels;
        return $this;
    }

    /**
     * @param $model
     * @return mixed
     */
    private function setFilterModel($model)
    {

        if (!$this->chart_filters) {
            return $model;
        }
        foreach ($this->chart_filters as $title => $filter) {
            $values = $this->getFilterValueChart($title, $filter["type"]);
            if ($filter["type"] == "date") {
                $model = $this->setFilterTypeDate($model, $title, $values);
            } elseif ($filter["type"] == "select") {
                // TOBE TODO
            }
        }
        return $model;
    }

    /**
     * @param $model
     * @param $title
     * @param null $values
     * @return mixed
     */
    private function setFilterTypeDate($model, $title, $values = null)
    {
        if (!is_array($values) && $values) {
            $model = $model->where($title, $values[$title]);
        } else {
            if (isset($values["from"]) && $values["from"]) {
                $model = $model->where($title, ">=", $values["from"]);
            }

            if (isset($values["to"]) && $values["to"]) {
                $model = $model->where($title, "<=", $values["to"]);
            }
        }
        return $model;
    }

    /**
     * @param $name
     * @param string $type
     * @return array|bool|null
     */
    private function getFilterValueChart($name, $type = 'date')
    {
        if ($type == 'date') {
            $value = [];
            $toName = $name . '_to';
            $fromName = $name . '_from';

            $formatFrom = isset($this->chart_filters[$name]["options"]["date_format"]) ? $this->chart_filters[$name]["options"]["date_format"] : trans('core::date.datetime.medium');
            if ($fromValue = $this->getFilterValueRequest($fromName)) {
                $value['from'] = $fromValue;
//                $value['from'] = DateHelper::setLocaleDateTime($fromValue, $formatFrom)->getDateTime($formatFrom);
            }

            if ($toValue = $this->getFilterValueRequest($toName)) {
                $value['to'] = $toValue;
//                $value['to'] = DateHelper::setLocaleDateTime($toValue, $formatFrom)->getDateTime($formatFrom);
            }

            if (count($value)) {
                return $value;
            }

        } else {
            $value = $this->getFilterValueRequest($name);
            if (!is_null($value) && $value != '') {
                return $value;
            }
        }

        return null;
    }

    /**
     * @param $name
     * @return bool
     */
    private function getFilterValueRequest($name)
    {
        if (isset($this->chart_filter_values[$name]) || Request::has($name)) {
            $value = Request::input($name);
            if (isset($this->chart_filter_values[$name])) {
                return $this->chart_filter_values[$name];
            }
            return $value;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getChartActionUrl()
    {
        return $this->action_url ? $this->action_url : route('core.dashboard.index');
    }

    /**
     * @return array|string
     * @throws \Throwable
     */
    public function getChart()
    {
        $this->bootChart();
        if (get_instance()->isAjax()) {
            $this->bootChartOnly();
            $script = view('core::chart.chartjs')
                ->with('datasets', array_values($this->chart_datasets))
                ->with('element', $this->chart_name)
                ->with('labels', $this->chart_labels)
                ->with('options', $this->chart_options)
                ->with('optionsRaw', $this->chart_options_raw)
                ->with('type', $this->chart_type)
                ->with('size', $this->chart_size)
                ->render();
            $view = '<canvas id="' . $this->chart_name . '" width="' . $this->chart_size['width'] . '" height="' . $this->chart_size['height'] . '"></canvas>';
            return [
                'view' => $view, 'script' => $script, 'datasets' => array_values($this->chart_datasets), 'element' => $this->chart_name,
                'labels' => $this->chart_labels, 'options' => $this->chart_options, 'options_row' => $this->chart_options_raw,
                'type' => $this->chart_type, 'size' => $this->chart_size
            ];
        }
        return $this->getChartRenderer("index");
    }


    /**
     * @param string $view
     * @return array|string
     * @throws \Throwable
     */
    public function getChartRenderer($view = "chart")
    {
        $this->bootChartOnly();
        if (get_instance()->isAjax()) {
            $script = view('core::chart.chartjs')
                ->with('datasets', array_values($this->chart_datasets))
                ->with('element', $this->chart_name)
                ->with('labels', $this->chart_labels)
                ->with('options', $this->chart_options)
                ->with('optionsRaw', $this->chart_options_raw)
                ->with('type', $this->chart_type)
                ->with('size', $this->chart_size)
                ->render();
            $view = '<canvas id="' . $this->chart_name . '" width="' . $this->chart_size['width'] . '" height="' . $this->chart_size['height'] . '"></canvas>';
            return [
                'view' => $view, 'script' => $script, 'datasets' => array_values($this->chart_datasets), 'element' => $this->chart_name,
                'labels' => $this->chart_labels, 'options' => $this->chart_options, 'options_row' => $this->chart_options_raw,
                'type' => $this->chart_type, 'size' => $this->chart_size
            ];
        }
        $view_model =& $this;
        return view('core::chart.' . $view, compact('view_model'))->render();
    }

}