<?php

namespace Modules\Core\ViewModels\Route;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Core\Models\Instance\InstanceRouteBlock;
use DateHelper;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;


    public function setGridModel()
    {
        $this->Model = InstanceRouteBlock::with(['instance', 'language'])->filterCurrentInstance()->filterLanguage();
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('core::form.fields.id'), true)
            ->addColumn('instance_name', trans('core::form.fields.instance'), false)
            ->addColumn('language_name', trans('core::form.fields.language'), false)
            ->addColumn('route_name', trans('core::form.fields.route_name'), true)
            ->addColumn('is_api', trans('core::form.fields.is_api'), false)
            ->addColumn('active', trans('core::form.fields.active'), true)
            ->addColumn('updated_at', trans('core::form.fields.date'), true);


        $edit = array(
            'name' => 'core.route.edit',
            'parameter' => ['id']
        );
        $this->addAction('edit', $edit, trans('core::form.titles.edit_route_block'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $this->action_text = false;

        $routeAdd = route('core.route.edit', ['id' => 0]);
        $options = ['class' => "btn btn-primary"];
        $this->addButton('add', $routeAdd, trans('core::form.titles.create_route_block'), 'fa fa-plus', false, 'before', $options);

        return $this;
    }


    public function getRowsUpdate($row)
    {
        $row->instance_name = $row->instance->name;
        $row->language_name = $row->language->name;
        $row->is_api = $row->is_api?trans('core::form.fields.is_api_statuses.yes'):trans('core::form.fields.is_api_statuses.no');
        $row->active = $this->getComponent('yesno', ['value' => $row->active]);
        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));

        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getRouteList()
    {
        $this->setTitlePage(trans('core::form.titles.route_block'));
        $this->generateGridList()->renderedView("core::route.list", ['view_model' => $this], trans('core::form.titles.route_block'));
        return $this;
    }


}
