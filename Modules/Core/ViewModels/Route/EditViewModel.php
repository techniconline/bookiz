<?php

namespace Modules\Core\ViewModels\Route;

use BridgeHelper;
use Illuminate\Support\Facades\Route;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\Models\Instance\InstanceRouteBlock;
use DateHelper;


class EditViewModel extends BaseViewModel
{

    private $id = 0;

    public function boot()
    {
        $this->id = $this->request->get('id');
        if ($this->id) {
            $this->modelData = InstanceRouteBlock::find($this->id);
        }
    }

    public function getEditForm()
    {
        $this->boot();
        if ($this->id && !$this->modelData) {
            return $this->redirectBack()->setResponse(false, trans('core::message.alert.not_find_data'));
        }
        BridgeHelper::getAccess()->hasAccessModelOnInstanceAndLanguage($this->modelData);
        $BlockViewModel =& $this;
        if ($this->id) {
            $title = trans('core::form.titles.edit_route_block');
        } else {
            $title = trans('core::form.titles.create_route_block');
        }
        $this->setTitlePage($title);
        return $this->renderedView('core::route.edit', compact('BlockViewModel'));
    }


    public function savePage()
    {
        $this->boot();
        if ($this->id && !$this->modelData) {
            return $this->redirectBack()->setResponse(false, trans('core::message.alert.not_find_data'));
        }
        BridgeHelper::getAccess()->hasAccessModelOnInstanceAndLanguage($this->modelData);
        $instance_id = app('getInstanceObject')->getCurrentInstance()->id;
        if (!$this->modelData) {
            $this->modelData = new InstanceRouteBlock();
            $this->modelData->instance_id = $instance_id;
        }
        $language_id = ($this->request->get('language_id')) ? $this->request->get('language_id') : app('getInstanceObject')->getCurrentLanguage()->id;
        $is_api = ($this->request->get('is_api')) ? $this->request->get('is_api') : 0;

        $rules = [
            'active' => 'required|in:1,0',
            'column' => 'required|in:3Column,2ColumnRight,2ColumnLeft,1Column,EmptyColumn,layout',
            'json' => 'required|json',
        ];
        if ($this->id && $this->modelData->route_name == $this->request->get('route_name')) {
            $rules['route_name'] = 'required|string';
        } else {
            $rules['route_name'] = 'required|string|unique:instance_route_blocks,route_name,null,null,instance_id,' . $instance_id . ',language_id,' . $language_id.',is_api,'.$is_api;
        }
        $this->requestValuesUpdate()->validate($rules);
        $data = $this->request->all();
        $this->modelData->language_id = $language_id;
        $this->modelData->route_name = $data['route_name'];
        $this->modelData->active = $data['active'];
        $this->modelData->column = $data['column'];
        $this->modelData->json = $data['json'];
        $this->modelData->is_api = isset($data['is_api']) ? $data['is_api'] : null;
        $result = $this->modelData->save();
        if ($result) {
            return $this->redirect(route('core.route.index'))->setResponse(true, trans('core::message.alert.success'));
        }
        return $this->redirectBack()->setResponse($result, trans('core::message.alert.error'));

    }


    public function getRoutes()
    {
        $routeCollection = Route::getRoutes();
        $list = [];
        foreach ($routeCollection as $value) {
            if (in_array('GET', $value->methods) || in_array('HEAD', $value->methods)){
                $list[$value->getName()] = $value->uri();
            }
        }
        return $list;
    }

}
