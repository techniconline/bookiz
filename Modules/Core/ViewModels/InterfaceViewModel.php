<?php

namespace Modules\Core\ViewModels;

interface InterfaceViewModel
{

    /**
     * @param $method
     * @return mixed
     */
    public function setMethod($method);

    /**
     * @param $permission
     * @return mixed
     */
    public function getAccess();

    /**
     * @return mixed
     */
    public function getLayout();

    /**
     * @return mixed
     */
    public function getTheme();

    /**
     * @return mixed
     */
    public function getCacheInfo();

    /**
     * @return mixed
     */
    public function helperData();

    /**
     * @return mixed
     */
    public function response();

    /**
     * @return mixed
     */
    public function getAssets();

    /**
     * @return mixed
     */
    public function setAssets();

    /**
     * @return version type float
     */
    public function getAssetVersion();

    /**
     * @return version type float
     */
    public function setAssetVersion();

}
