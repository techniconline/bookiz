<?php

namespace Modules\Core\ViewModels\Grid;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\File;
use BridgeHelper;
use Illuminate\Support\Facades\Route;
use Modules\Core\ViewModels\Chart\ChartViewModel;

trait GridViewModel
{
    use ChartViewModel;

//    private $request = false;
    private $columns = [];
    private $rows = false;
    private $limit = [10 => 10, 20 => 20, 30 => 30, 40 => 40, 50 => 50];
    private $per_page = 10;
    private $page = 0;
    private $actions = [];
    private $buttons = ['after' => [], 'before' => []];
    private $sort_by = 'id';
    private $dir = 'DESC';
    private $all_sort_by = false;
    private $all_dir = 'DESC';
    private $Model = false;
    private $boot = false;
    private $action_icon = true;
    private $with_paginate = true;
    private $action_text = true;
    private $success_ajax_function = false;
    private $fail_ajax_function = false;
    private $hasIndex = false;
    private $actionAttributes = [];
    private $filters = [];
    private $filter_relations = [];
    private $filter = 0;
    private $export = 0;
    private $export_file_name = 'grid';
    private $export_key = false;
    private $export_url = '';
    private $can_export = false;
    private $filter_values = [];
    private $export_by_page = 1000;
    private $show_checkbox = false;
    private $delete_config = [];
    private $publish_config = [];
    private $unpublish_config = [];
    private $all_actions_config = [];
    private $current_page = 1;

    private $in_widget = false;
    private $can_chart = false;
    private $use_chart = false;
    static $x = 0;

    /**
     * @return bool
     */
    public function inWidget()
    {
        return $this->in_widget;
    }

    public function initGrid()
    {
        $this->boot = true;
        $per_page = Request::input('per_page');
        if ($per_page && in_array($per_page, $this->limit)) {
            $this->per_page = $per_page;
        }

        $sort_by = Request::input('sort_by');
        if ($sort_by && isset($this->columns[$sort_by])) {
            $this->sort_by = $sort_by;
        }

        $dir = strtoupper(Request::input('dir'));
        if ($dir && ($dir == 'DESC' || $dir == 'ASC')) {
            $this->dir = $dir;
        }

        if (Request::input('page')) {
            $this->page = Request::input('page') - 1;
        }

        if (Request::has('filter')) {
            $this->filter = Request::input('filter');
        }

        if (Request::input('export')) {
            $this->export = Request::input('export');
        }

        if (Request::input('export_key')) {
            $this->export_key = Request::input('export_key');
            $this->per_page = $this->export_by_page;
        }

        if (Request::input('use_chart')) {
            $this->use_chart = Request::input('use_chart');
        }

        $this->setRequestOfDate();
    }

    /**
     * void
     */
    private function setRequestOfDate()
    {
        $all = \request()->all();
        $n_k = [];
        foreach ($all as $key => $value) {
            if (strpos($key, 'date-') !== false) {
                $arr = explode("-", $key);
                $new_key = end($arr);
                if (in_array('year', $arr)) {
                    $n_k[$new_key]['year'] = $value;
                } elseif (in_array('month', $arr)) {
                    $n_k[$new_key]['month'] = $value;
                }
                if (in_array('day', $arr)) {
                    $n_k[$new_key]['day'] = $value;
                }

                if (isset($n_k[$new_key]['day']) && isset($n_k[$new_key]['month']) && isset($n_k[$new_key]['year'])) {
                    $value = $n_k[$new_key]['year'] . '-' . $n_k[$new_key]['month'] . '-' . $n_k[$new_key]['day'];
                    \request()->request->set($new_key, $value);
                }

            }
        }
    }

    public function setActionRows($options = ['delete' => ['title' => '', 'route' => '', 'method' => 'POST']])
    {
        $this->show_checkbox = true;
        foreach ($options as $key => $option) {
            $method_name = 'set' . ucfirst($key) . 'All';
            if (method_exists($this, $method_name)) {
                $this->{$method_name}($option);
            }
        }
    }

    private function setDeleteAll($config = ['title' => '', 'route' => '', 'method' => 'POST'])
    {
        $this->delete_config['title'] = isset($config['title']) ? $config['title'] : 'delete';
        $this->delete_config['url'] = isset($config['route']) && Route::has($config['route']) ? route($config['route']) : '';
        $this->delete_config['method'] = isset($config['method']) ? ($config['method']) : '';
        $this->delete_config['data-message-confirm'] = isset($config['data-message-confirm']) ? ($config['data-message-confirm']) : 'Are you sure?';
        $this->delete_config['icon'] = isset($config['icon']) ? ($config['icon']) : 'fa fa-floppy-o';
        $this->delete_config['class'] = isset($config['class']) ? ($config['class']) : 'btn btn-warning btn-sm';
        $this->all_actions_config['delete'] = $this->delete_config;
    }

    private function setPublishAll($config = ['title' => '', 'route' => '', 'method' => 'POST'])
    {
        $this->publish_config['title'] = isset($config['title']) ? $config['title'] : 'delete';
        $this->publish_config['url'] = isset($config['route']) && Route::has($config['route']) ? route($config['route']) : '';
        $this->publish_config['data-message-confirm'] = isset($config['data-message-confirm']) ? ($config['data-message-confirm']) : 'Are you sure?';
        $this->publish_config['method'] = isset($config['method']) ? ($config['method']) : '';
        $this->publish_config['icon'] = isset($config['icon']) ? ($config['icon']) : 'fa fa-floppy-o';
        $this->publish_config['class'] = isset($config['class']) ? ($config['class']) : 'btn btn-primary btn-sm';
        $this->all_actions_config['publish'] = $this->publish_config;
    }

    private function setUnpublishAll($config = ['title' => '', 'route' => '', 'method' => 'POST'])
    {
        $this->unpublish_config['title'] = isset($config['title']) ? $config['title'] : 'delete';
        $this->unpublish_config['url'] = isset($config['route']) && Route::has($config['route']) ? route($config['route']) : '';
        $this->unpublish_config['data-message-confirm'] = isset($config['data-message-confirm']) ? ($config['data-message-confirm']) : 'Are you sure?';
        $this->unpublish_config['method'] = isset($config['method']) ? ($config['method']) : '';
        $this->unpublish_config['icon'] = isset($config['icon']) ? ($config['icon']) : 'fa fa-floppy-o';
        $this->unpublish_config['class'] = isset($config['class']) ? ($config['class']) : 'btn btn-primary btn-sm';
        $this->all_actions_config['nupublish'] = $this->unpublish_config;
    }

    public function getAllActionsConfig()
    {
        return $this->all_actions_config;
    }

    public function getDeleteAll()
    {
        return $this->delete_config;
    }

    public function getPublishAll()
    {
        return $this->publish_config;
    }

    public function getUnpublishAll()
    {
        return $this->publish_config;
    }

    /**
     * @return bool
     */
    public function showActionAll()
    {
        return $this->show_checkbox;
    }

    /**
     * return data grid
     * @return mixed|bool
     */
    public function toArray()
    {
        return $this->getRows();
    }

    public function getRows()
    {
        if ($this->rows) {
            return $this->rows;
        }
        if (is_a($this->Model, 'Illuminate\Database\Eloquent\Builder')) {
            $model = $this->Model->orderBy($this->sort_by, $this->dir);
        } else {
            $model = $this->Model::orderBy($this->sort_by, $this->dir);
        }
        $model = $this->setFilterOnModel($model);
        if ($this->all_sort_by) {
            $model->orderBy($this->all_sort_by, $this->all_dir);
        }
        if ($this->with_paginate) {
            $this->rows = $model->paginate($this->per_page);
            $this->current_page = $this->rows->currentPage();
        } else {
            $this->rows = $model->get();
        }
        return $this->rows;
    }

    /**
     * @param bool $status
     * @return $this
     */
    public function setPaginate($status = true)
    {
        $this->with_paginate = $status;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasPaginate()
    {
        return $this->with_paginate;
    }

    public function hasActions()
    {
        return count($this->actions);
    }

    public function addAction($name, $route, $trans = false, $icon = false, $access = false, $attributes = array())
    {

        if ($access) {
            if (!BridgeHelper::getAccess()->canAccess($access, false)) {
                return $this;
            }
        } else {
            if (is_array($route) && isset($route['name'])) {
                if (!BridgeHelper::getAccess()->canAccessRoute($route['name'], false)) {
                    return $this;
                }
            }
        }

        $newColumn = new \stdClass();
        if (isset($attributes['class'])) {
            $attributes['class'] .= ' ' . $name;
        } else {
            $attributes['class'] = ' ' . $name;
        }
        $newColumn->name = $name;
        $newColumn->route = $route;
        $newColumn->trans = $trans;
        $newColumn->icon = $icon;
        $newColumn->attributes = $attributes;
        $this->actions[$name] = $newColumn;
        return $this;
    }

    public function removeAction($name)
    {
        if (isset($this->actions[$name])) {
            unset($this->actions[$name]);
        }
        return $this;
    }


    public function addButton($name, $route, $trans = false, $icon = false, $access = false, $position = 'before', $attributes = array())
    {
        if ($access) {
//            if (!access()->canAccess($access)) {
//                return $this;
//            }
        }
        $newColumn = new \stdClass();
        if (isset($attributes['class'])) {
            $attributes['class'] .= ' ' . $name;
        } else {
            $attributes['class'] = ' ' . $name;
        }
        $newColumn->name = $name;
        $newColumn->route = $route;
        $newColumn->trans = $trans;
        $newColumn->icon = $icon;
        $newColumn->attributes = $attributes;
        $this->buttons[$position][$name] = $newColumn;
        return $this;
    }

    public function removeButton($name)
    {
        if (isset($this->buttons['before'][$name])) {
            unset($this->buttons['before'][$name]);
        }
        if (isset($this->buttons['after'][$name])) {
            unset($this->buttons['after'][$name]);
        }
        return $this;
    }

    public function getAttributes($attribute)
    {
        $str = '';
        if (is_array($attribute)) {
            foreach ($attribute as $key => $attr) {
                $str .= ' ' . $key . '="' . $attr . '" ';
            }
        }
        return $str;
    }

    public function getRowsAttributes($row)
    {
        return '';
    }

    public function getActionsUpdate($row)
    {
        return $this->actions;
    }

    public function getRowsUpdate($row)
    {
        return $row;
    }

    public function getGridHtml()
    {
        if (!$this->boot) {
            $this->initGrid();
        }
        $model = $this;
        if ($this->export && $this->getCanExport()) {
            if (Request::input('export_key')) {
                @ob_clean();
                $this->per_page = 1000;
                $name = session('export_key_' . $this->export_key);
                $result = $this->makeExport($name);
                $response = ['result' => $result, 'url' => $this->export_url];
                response()->json($response, 200)->send();
                return '';
            } else {
                $this->export_key = crc32(Request::fullUrl() . time() . rand(1000, 2000));
                $name = $this->getExportFileName();
                session(['export_key_' . $this->export_key => $name]);
                return view('core::grid.export', compact('model'))->render();
            }

        } else {
            return view('core::grid.layout.grid', compact('model'))->render();
        }
    }

    public function getGridJson()
    {
        if (!$this->boot) {
            $this->initGrid();
        }
        $model = $this;
        return $model->getRows();

        if ($this->export && $this->getCanExport()) {
            if (Request::input('export_key')) {
                @ob_clean();
                $this->per_page = 1000;
                $name = session('export_key_' . $this->export_key);
                $result = $this->makeExport($name);
                $response = ['result' => $result, 'url' => $this->export_url];
                response()->json($response, 200)->send();
                return '';
            } else {
                $this->export_key = crc32(Request::fullUrl() . time() . rand(1000, 2000));
                $name = $this->getExportFileName();
                session(['export_key_' . $this->export_key => $name]);
                return view('core::grid.export', compact('model'))->render();
            }

        } else {
            return view('core::grid.layout.grid', compact('model'))->render();
        }
    }

    public function addColumn($name, $trans, $sort = true, $attributes = array())
    {
        $newColumn = new \stdClass();
        if (isset($attributes['class'])) {
            $attributes['class'] .= ' ' . $name;
        } else {
            $attributes['class'] = ' ' . $name;
        }
        $newColumn->name = $name;
        $newColumn->trans = $trans;
        $newColumn->sort = $sort;
        $newColumn->attributes = $attributes;
        $this->columns[$name] = $newColumn;
        return $this;
    }

    public function removeColumn($name)
    {
        if (isset($this->columns[$name])) {
            unset($this->columns[$name]);
        }
        return $this;
    }

    public function getLinks()
    {
        if (!$this->with_paginate) {
            return null;
        }
        $data = ['filter' => $this->filter, 'sort_by' => $this->sort_by, 'dir' => $this->dir, 'per_page' => $this->per_page];
        $filters = $this->getFiltersUrl();
        if ($this->filter && count($filters)) {
            $data = array_merge($data, $filters);
        } elseif (count($this->filters)) {
            $filters = [];
            foreach ($this->filters as $name => $filter) {
                $filters[$name] = null;
            }
            $data = array_merge($data, $filters);
        }
        return $this->getRows()->appends($data)->links();
    }

    public function baseUrl()
    {
        if ($this->with_paginate){
            return $this->getRows()->url(1);
        }
        return null;
    }

    public function getFilterLinks($newfilters = [], $filter = null)
    {
        if (!$this->with_paginate) {
            return null;
        }
        $filter = is_null($filter) ? $this->filter : $filter;
        $filters = $this->getFiltersUrl();
        $data = ['filter' => $filter, 'sort_by' => $this->sort_by, 'dir' => $this->dir, 'per_page' => $this->per_page];
        if ($filter && count($filters)) {
            foreach ($newfilters as $name => $value) {
                foreach ($this->filters[$name] as $filter) {
                    if (isset($this->filters[$name])) {
                        $type = $filter->type;
                    }
                    if ($type == 'date') {
                        if (isset($filter->configs['relation'])) {

                            if (isset($filters[$name . '_from_' . $filter->configs['relation']])) {
                                $filters[$name . '_from_' . $filter->configs['relation']] = $value;
                            }
                            if (isset($filters[$name . '_to_' . $filter->configs['relation']])) {
                                $filters[$name . '_to_' . $filter->configs['relation']] = $value;
                            }
                        }
                        if (isset($filters[$name . '_from'])) {
                            $filters[$name . '_from'] = $value;
                        }
                        if (isset($filters[$name . '_to'])) {
                            $filters[$name . '_to'] = $value;
                        }


                    } else {
                        if (isset($filters[$name])) {
                            $filters[$name] = $value;
                        }
                    }
                }
            }
            $data = array_merge($data, $filters);
        } elseif (count($this->filters)) {
            $filters = [];
            foreach ($this->filters as $name => $filter) {
                $filters[$name] = null;
            }
            $data = array_merge($data, $filters);
        }
        return $this->getRows()->appends($data)->url(1);
    }

    public function getMakeUrl($sort_by = null, $dir = null, $per_page = null, $page = null, $filter = null)
    {
        if (!$this->with_paginate) {
            return null;
        }
        $filter = $filter ? $filter : $this->filter;
        $sort_by = $sort_by ? $sort_by : $this->sort_by;
        $dir = $dir ? $dir : $this->dir;

        $per_page = $per_page ? $per_page : $this->per_page;
        $page = $this->getRows()->currentPage();
        // $page = $page ? $page : $this->getRows()->currentPage();
        $data = ['filter' => $filter, 'sort_by' => $sort_by, 'dir' => $dir, 'per_page' => $per_page, 'use_chart' => $this->use_chart];
        $filters = $this->getFiltersUrl();
        if ($this->filter && count($filters) && $filter) {
            $data = array_merge($data, $filters);
        } elseif (count($this->filters)) {
            $filters = [];
            foreach ($this->filters as $name => $filter) {
                $filters[$name] = null;
            }
            $data = array_merge($data, $filters);
        }
        return $this->getRows()->appends($data)->url($page);
    }

    public function getActionUrl($route, $row = null)
    {
        if (is_string($route)) {
            return url($route);
        }
        if (is_array($route)) {
            $routeParams = [];
            if (isset($route['parameter'])) {
                if (is_array($route['parameter'])) {
                    $routeParams = [];
                    foreach ($route['parameter'] as $key => $value) {
                        if (is_array($value) && isset($value['value'])) {
                            $routeParams[$key] = $value['value'];
                        } else {
                            if (isset($row->{$key})) {
                                $routeParams[$value] = $row->{$key};
                            } else {
                                $routeParams[$value] = $row->{$value};
                            }

                        }
                    }
                } else {
                    $routeParams = [$route['parameter'] => $row->{$route['parameter']}];
                }
            }
            return route($route['name'], $routeParams);
        }
        return '#';
    }

    public function addFilter($name, $type = true, $configs = array())
    {
        $newFilter = new \stdClass();
        $newFilter->name = $name;
        $newFilter->type = strtolower($type);
        $configs['name'] = $name;
        if (!isset($configs['value'])) {
            $configs['value'] = $this->getFilterValue($name, $type, $configs['relation'] ?? null);
        }
        if (!isset($configs['options'])) {
            $configs['options'] = [];
        }
        if (!isset($configs['title'])) {
            $configs['title'] = trans('core::validations.attributes.' . $name);
        }
        if (!isset($configs['process'])) {
            $configs['process'] = true;
        }
        if (isset($configs['relation'])) {
            $this->addFilterRelation($name, $type, $configs);
        }
        $newFilter->configs = $configs;

        $this->filters[$name][self::$x] = $newFilter;
        self::$x = self::$x + 1;
        return $this;
    }

    private function addFilterRelation($name, $type = true, $configs = array())
    {
        if (isset($configs['relation'])) {
            $this->filter_relations[$name]['relation'][self::$x] = $configs['relation'];
            $this->filter_relations[$name]['op'] = isset($configs['op']) ? $configs['op'] : '=';
            $this->filter_relations[$name]['type'] = $type;
            $this->filter_relations[$name]['name'] = $name;
        }

        return $this;
    }

    public function hasFilter()
    {
        return count($this->filters);
    }

    public function removeFilter($name)
    {
        if (isset($this->filters[$name])) {
            unset($this->filters[$name]);
        }
        return $this;
    }

    public function getFilterFormsHtml()
    {
        $model =& $this;
        return view('core::grid.filter', compact('model'))->render();
    }

    public function getFilterItemsHtml()
    {
        $html = '';
        foreach ($this->filters as $filters) {
            foreach ($filters as $filter) {
                $data = $filter->configs;
                $html .= view('core::grid.item.' . $filter->type, $data)->render();
            }
        }
        return $html;
    }

    public function getFiltersUrl()
    {
        $list = [];
        foreach ($this->filters as $filters) {
            foreach ($filters as $filter) {
                $relation = $filter->configs['relation'] ?? null;
                $list['relation'] = $relation;
                $value = $this->getFilterValue($filter->name, $filter->type, $relation);
                if (!is_null($value)) {
                    if (is_array($value)) {
                        foreach ($value as $key => $val) {
                            if ($filter->type == 'date') {
                                $list['date_' . $key] = getTimestampToJalali($val);
                            } else {
                                $list[$key] = $val;
                            }

                        }
                    } else {
                        $list[$filter->name] = $value;
                    }
                }
            }
        }

        return $list;
    }

    public function setFilterOnModel($model, $fieldByReplace = [], $process = true)
    {
        foreach ($this->filters as $filters) {

            foreach ($filters as $filter) {
                if (!$filter->configs['process'] && $process) {
                    continue;
                }


                if (isset($filter->configs['relation'])) {
                    if (isset($this->filter_relations[$filter->name]['relation'])) {
                        foreach ($this->filter_relations[$filter->name]['relation'] as $relation) {
                            $relation;
                            if (!$relation) {
                                return $model;
                            }
                        }
                    }
                    $model = $this->setFilterOnRelationModel($filter->name, $model);
                    continue;
                }
                $value = $this->getFilterValue($filter->name, $filter->type);
                if (!is_null($value)) {
                    $field = isset($filter->configs['field']) ? $filter->configs['field'] : $filter->name;
                    if (isset($filter->configs['fields']) && is_array($filter->configs['fields']) && isset($filter->configs['fields'][$value])) {
                        $data = $filter->configs['fields'][$value];
                        if (isset($data['value'])) {
                            $model->where($field, $data['op'], $data['value']);
                        } else {
                            $model->where($field, $data['op'], $data['value']);
                        }
                        continue;
                    }
                    if (isset($fieldByReplace[$field])) {
                        $field = $fieldByReplace[$field];
                    }
                    if ($filter->type == 'text' || $filter->type == 'hidden') {
                        if (isset($filter->configs['op'])) {
                            $model->where($field, $filter->configs['op'], $value);
                        } else {
                            $model->where($field, 'like', '%' . $value . '%');
                        }
                    } elseif ($filter->type == 'select') {
                        $model->where($field, '=', $value);
                    } elseif ($filter->type == 'date') {
                        if (isset($value['from']) && !isset($value['to'])) {
                            $model->where($field, '>=', getDateTimeFromTimestamp($value['from']));
                        } elseif (isset($value['to']) && !isset($value['from'])) {
                            $model->where($field, '<=', getDateTimeFromTimestamp($value['to']));
                        } elseif (isset($value['to']) && isset($value['from'])) {
                            $model->whereBetween($field, [getDateTimeFromTimestamp($value['from']), getDateTimeFromTimestamp($value['to'])]);

                        }
                    }
                }
            }
        }

        return $model;
    }

    private function setFilterOnRelationModel($name, $model)
    {
        $filter = isset($this->filter_relations[$name]) ? $this->filter_relations[$name] : null;
        if (!$filter) {
            return $model;
        }
        foreach ($filter['relation'] as $relation) {
            $value = $this->getFilterValue($name, $filter['type'], $relation);
            if (!is_null($value) && $relation) {
                $model->whereHas($relation, function ($q) use ($filter, $value, $name, $relation) {
                    if ($filter['type'] == 'integer') {

                        $op = $filter['op'];
                        $q->where($name, $op, $value);

                    } elseif ($filter['type'] == 'date') {
                        if (isset($value['from_' . $relation]) && !isset($value['to_' . $relation])) {
                            $q->where($name, '>=', getDateTimeFromTimestamp($value['from_' . $relation]));
                        } elseif (isset($value['to_' . $relation]) && !isset($value['from_' . $relation])) {
                            $q->where($name, '<=', getDateTimeFromTimestamp($value['to_' . $relation]));
                        } elseif (isset($value['to_' . $relation]) && isset($value['from_' . $relation])) {
                            $q->whereBetween($name, [getDateTimeFromTimestamp($value['from_' . $relation]), getDateTimeFromTimestamp($value['to_' . $relation])]);
                        }

                    } elseif ($filter['type'] == 'select') {

                        $valueArr = explode(",", $value);
                        $q->whereIn($name, $valueArr);

                    } elseif ($filter['type'] == 'text') {
                        $q->where($name, 'like', '%' . $value . '%');
                    } else {
                        if (is_array($value)) {
                            $q->whereIn($name, $value);
                        } else {
                            $q->where($name, $value);
                        }
                    }
                });

            }
        }
        return $model;
    }

    public function setFilterValue($name, $value, $disabled = false)
    {
        $this->filter_values[$name] = $value;
        if (isset($this->filters[$name])) {
            $this->filters[$name]->configs['value'] = $value;
            if ($this->Model) {

            }
            if ($disabled) {
                $this->filters[$name]->configs['disabled'] = true;
            }
        }
        return $this;
    }


    public function getFilterValue($name, $type = 'text', $relation = null)
    {
        if ($type == 'date') {
            $value = [];
            $toName = $name . '_to';
            $fromName = $name . '_from';
            if ($relation) {
                $toName = $name . '_to_' . $relation;
                $fromName = $name . '_from_' . $relation;
            }
            if ($fromValue = $this->getFilterValueFromRequest($fromName)) {
                if ($relation) {
                    $value['from_' . $relation] = getJalaliDateToTimestamp($fromValue, '00:00:00', '-');
                } else {
                    $value['from'] = getJalaliDateToTimestamp($fromValue, '00:00:00', '-');
                }
            }
            if ($toValue = $this->getFilterValueFromRequest($toName)) {
                if ($relation) {
                    $value['to_' . $relation] = getJalaliDateToTimestamp($toValue, '23:59:59', '-');
                } else {
                    $value['to'] = getJalaliDateToTimestamp($toValue, '23:59:59', '-');
                }
            }
            if (count($value)) {
                return $value;
            }
            return null;
        } else {

            $value = empty($relation) ? $this->getFilterValueFromRequest($name) : $this->getFilterValueFromRequest($name, $relation);
            if (!is_null($value) && $value != '') {
                return $value;
            }
        }

        return null;
    }

    public function getFilterValueFromRequest($name, $relation = null)
    {
        if ($relation) {
            if (isset($this->filter_values[$name . '_' . $relation]) || Request::has($name . '_' . $relation)) {
                $value = Request::input($name . '_' . $relation);
                if (isset($this->filter_values[$name . '_' . $relation])) {
                    return $this->filter_values[$name . '_' . $relation];
                }
                return $value;
            }
        } else {
            if (isset($this->filter_values[$name]) || Request::has($name)) {
                $value = Request::input($name);
                if (isset($this->filter_values[$name])) {
                    return $this->filter_values[$name];
                }
                return $value;
            }
        }

        return false;
    }

    public function getFilterText($name)
    {
        $type = 'text';
        if (isset($this->filters[$name])) {
            $type = $this->filters[$name]->type;
        }
        $value = $this->getFilterValue($name, $type);
        if ($type == 'text' || $type == 'hidden') {
            if (!is_null($value)) {
                return $value;
            }
        } elseif ($type == 'select') {
            if (!is_null($value)) {
                if (count($this->filters[$name]->configs['options'])) {
                    return $this->filters[$name]->configs['options'][$value];
                }
            }
        } elseif ($type == 'date') {
            if (isset($value['from']) && !isset($value['to'])) {
                return trans('core::data.date_from') . getTimestampToJalali($value['from']);
            } elseif (isset($value['to']) && !isset($value['from'])) {
                return trans('core::data.date_to') . getTimestampToJalali($value['to']);
            } elseif (isset($value['to']) && isset($value['from'])) {
                return trans('core::data.date_from') . getTimestampToJalali($value['from']) . ' | ' . trans('core::data.date_to') . getTimestampToJalali($value['to']);
            }

        }
        return null;
    }

    public function getFilterText2($name, $type)
    {
        $type = 'text';

        $value = $this->getFilterValue($name, $type);
        if ($type == 'text' || $type == 'hidden') {
            if (!is_null($value)) {
                return $value;
            }
        } elseif ($type == 'select') {
            if (!is_null($value)) {
                if (count($this->filters[$name]->configs['options'])) {
                    return $this->filters[$name]->configs['options'][$value];
                }
            }
        } elseif ($type == 'date') {
            if (isset($value['from']) && !isset($value['to'])) {
                return trans('core::data.date_from') . getTimestampToJalali($value['from']);
            } elseif (isset($value['to']) && !isset($value['from'])) {
                return trans('core::data.date_to') . getTimestampToJalali($value['to']);
            } elseif (isset($value['to']) && isset($value['from'])) {
                return trans('core::data.date_from') . getTimestampToJalali($value['from']) . ' | ' . trans('core::data.date_to') . getTimestampToJalali($value['to']);
            }

        }
        return null;
    }

    public function setExportFileName($name)
    {
        $this->export_file_name = $name;
        return $this;
    }

    public function getExportFileName()
    {
        $name = 'export_' . $this->export_file_name . '_' . getTimestampToJalali(null, '%Y_%m_%d') . '_' . $this->export_key . '.csv';
        return $name;
    }

    public function getExportKey()
    {
        return $this->export_key;
    }

    public function getExportFileAddress($name)
    {
        $path = storage_path('app/public/export/');
        if (!File::exists($path)) {
            File::makeDirectory($path, 0755, true, true);
        }
        $fileAddress = $path . $name;
        if ($this->page == 0) {
            if (File::exists($fileAddress)) {
                File::delete($fileAddress);
            }
        }
        return $fileAddress;
    }

    public function makeExport($name)
    {
        try {
            $fileAddress = $this->getExportFileAddress($name);
            $out = fopen($fileAddress, 'a+');
            if ($this->page == 0) {
                $header = $this->getExportHeader();
                fputcsv($out, $header);
            }
            $rows = $this->getRows();
            foreach ($rows as $row) {
                $CsvRow = $this->getExportRow($row);
                if (isset($CsvRow[0])) {
                    foreach ($CsvRow as $item) {
                        fputcsv($out, $item);
                    }
                } else {
                    fputcsv($out, $CsvRow);
                }
            }
            if ($rows->count() < 1000) {
                Request::session()->forget('export_key_' . $this->export_key);
            }
            fclose($out);
            /*create url*/
            $url = str_replace(storage_path('app/public/export/'), 'public/export/', $fileAddress);
            $this->export_url = url($url);
            /**/
            return true;
        } catch (Exception $e) {
            report($e);
            return false;
        }
    }

    public function getExportHeader()
    {
        $header = [];
        foreach ($this->columns as $column) {
            $header[$column->name] = $column->trans;
        }
        return $header;
    }

    public function getExportRow($row)
    {
        $csvRow = [];
        foreach ($this->columns as $column) {
            $csvRow[$column->name] = $row->{$column->name};
        }
        return $csvRow;
    }

    public function getCanExport()
    {
        if ($this->can_export && $this->getRows()->count() > 0) {
            return true;
        }
        return false;
    }

    public function isExportState()
    {
        if (Request::has('export') && Request::input('export')) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function getCanChart()
    {
        if ($this->can_chart && $this->getRows()->count() > 0) {
            return true;
        }
        return false;
    }

    public function __get(string $name)
    {
        if (isset($this->{$name})) {
            return $this->{$name};
        }
        return null;
    }

    public function __set(string $name, mixed $value)
    {
        $this->$name = $value;
        return $this;
    }


}