<?php
/**
 * Created by PhpStorm.
 * User: hossein
 * Date: 2/16/19
 * Time: 10:07 AM
 */

namespace Modules\Core\ViewModels;


abstract class BaseSitemap
{
    abstract public function getSiteMapModel();

    abstract public function createSitemap();

    abstract public function getLink($urlType = null);

    abstract public function lastUpdatedAt();
}