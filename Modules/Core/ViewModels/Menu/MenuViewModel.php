<?php

namespace Modules\Core\ViewModels\Menu;

use Illuminate\Support\Facades\Route;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Core\Models\Menu\Menu;
use Modules\Core\ViewModels\Traits\MenuTrait;

class MenuViewModel extends BaseViewModel
{

    use GridViewModel;
    use MenuTrait;

    private $nestable_assets;

    public function __construct()
    {

    }


    public function getPostCategory($id = null)
    {
        return [['id' => 1, 'title' => 'test']];
    }

    public function getRowsUpdate($row)
    {
//        $row->post_category_id = $this->getPostCategory($row->post_category_id);
//        if ($row->publish) {
//            $row->publish = view('core::grid.elements.text', ['class' => 'text-success', 'text' => getGeneralData()->getPublishData($row->publish)])->render();
//        } else {
////            $row->publish = view('core::grid.elements.text', ['class' => 'text-danger', 'text' => getGeneralData()->getPublishData($row->publish)])->render();
//        }

        return $row;
    }


    public function setGridModel()
    {
        $this->Model = Menu::whereNull('parent_id')->filterCurrentInstance()->filterLanguage();
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $menu_assets = [];

        //use bested assets
        if ($this->nestable_assets) {
            $menu_assets [] = ['container' => 'theme_style', 'src' => ("assets/global/plugins/jquery-nestable/jquery.nestable.css"), 'name' => 'jquery.nestable'];
            $menu_assets [] = ['container' => 'theme_js', 'src' => ("assets/global/plugins/jquery-nestable/jquery.nestable-rtl.js"), 'name' => 'jquery.nestable'];
            $menu_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/menu.js"), 'name' => 'menu-backend'];
        }

        return $menu_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return mixed
     */
    private function getList()
    {
        return Menu::active()->filters()->get();
    }

    private function generateGridMenuList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('core::validations.attributes.id'), true)
            ->addColumn('title', trans('core::menu.title'), true)
//            ->addColumn('post_category_id', trans('core::validations.attributes.category'), true)
            //->addColumn('publish',trans('core::validations.attributes.publish'),true)
            ->addColumn('created_at', trans('core::validations.attributes.date'), true);

        $add = array(
            'name' => 'core.menu.create',
            'parameter' => null
        );
        $this->addButton('create_root_menu', $add, trans('core::menu.add_menu_root'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('core::menu.confirm_delete')];
        $delete = array(
            'name' => 'core.menu.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('core::menu.delete_menu'), 'fa fa-trash', false, $options);

        $show = array(
            'name' => 'core.menu.show',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('core::menu.menus_list'), 'fa fa-list', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('id', 'text', ['op' => '=']);
        $this->addFilter('title', 'text');
//        $this->addFilter('category', 'select', ['field' => 'post_category_id', 'options' => $this->getPostCategory(), 'title' => trans('core::validations.attributes.category')]);

        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('core::menu.menus_root_list'));
        $this->generateGridMenuList()->renderedView("core::menu.index", ['view_model' => $this], "menu_list");
        return $this;
    }

    /**
     * active assets for method called!
     * @return $this
     */
    protected function setAssetsShowMenuTree()
    {
        $this->nestable_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function showMenuTree()
    {
        $instances = Instance::active()->get();
        $instances = $instances ? $instances->pluck('name', 'id') : [];
        $languages = Language::active()->get();
        $languages = $languages ? $languages->pluck('name', 'id') : [];

        $routes = $this->getRoutes();
        $menu = Menu::active()->find($this->request->get('menu_id', 0));
        $this->setTitlePage(trans("core::menu.list_menu", ["root_menu" => $menu ? ' ( ' . $menu->title . ' ) ' : '...']));
        $root_id = $menu ? $menu->root_id : 0;
        $menus = $this->getNestable(Menu::active()->where('root_id', $root_id)->with(['instance'])->get(), null, $root_id);
        return $this->renderedView("core::menu.show_tree_menus", ['menus' => $menus, 'instances' => $instances, 'languages' => $languages, 'route_list' => $routes], "tree_menus");;
    }

    /**
     * @return array
     */
    private function getRoutes()
    {
        $routes = Route::getRoutes();
        $routes = collect($routes)->mapWithKeys(function ($route) {
            if (!in_array("GET", $route->methods) || !$route->getName() || $route->parameters) {
                return [];
            } else {
                return [$route->getName() => $route->getName() . ' -- ' . $route->getActionName()];
            }
        })->toArray();
        return $routes;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createMenu()
    {
        $instances = Instance::active()->get();
        $instances = $instances ? $instances->pluck('name', 'id') : [];
        $languages = Language::active()->get();
        $languages = $languages ? $languages->pluck('name', 'id') : [];
        return $this->renderedView("core::menu.form", ['languages' => $languages,'instances' => $instances], "edit_menu");
    }

    /**
     * @return $this
     */
    protected function destroyMenu()
    {
        $menu_id = $this->request->get('menu_id');
        $menu = Menu::active()->find($menu_id);

        if ($menu->delete()) {
            return $this->setResponse(true, trans("core::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("core::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveMenu()
    {
        $menu = new Menu();
        if ($menu_id = $this->request->get("menu_id")) {
            $menu = $menu->active()->find($menu_id);

            if (!$menu) {
                return $this->redirect(route('core.menu.show', ['menu_id' => $this->request->get('menu_id')]))->setResponse(false, trans("core::messages.alert.not_find_data"));
            }

        } else {
            $this->setRootId();
        }

        if (is_int((int)$this->request->get('parent_id')) && !$this->request->get('parent_id')) {
            $this->request->request->set('parent_id', null);
        }

        if ($this->request->get('type') == 'route') {
            $this->request->request->set('url', $this->request->get('route_name'));
        }

        if (($menu->fill($this->request->all())->isValid()) || ($menu_id && $menu)) {
            $menu->save();
            $menu->url_edit = route('core.menu.update', ['menu_id' => $menu->id]);
            $menu->url_add_child = route('core.menu.add_child', ['menu_id' => $menu->id]);
            $menu->url_delete = route('core.menu.delete', ['menu_id' => $menu->id]);
            $menu->updated = $menu_id ? true : false;
            return $this->redirect(route('core.menu.show', ['menu_id' => $menu->id]))->setDataResponse($menu)->setResponse(true, trans("core::messages.alert.save_success"));
        }

        return $this->redirect(route('core.menu.create'))->setResponse(false, trans("core::messages.alert.mis_data"), $menu->errors);
    }

    /**
     * @return $this
     */
    private function setRootId()
    {
        $this->request->request->add(['root_id' => 0]);
        if ($parent_id = $this->request->get("parent_id")) {
            $parent = Menu::active()->find($parent_id);
            if ($parent) {
                $this->request->request->add([
                    'instance_id' => $parent->instance_id,
                    'language_id' => $parent->language_id,
                    'root_id' => $parent->root_id,
                ]);
            }
        } else {
            $lastRoot = Menu::active()->orderBy('root_id', 'DESC')->first();
            if ($lastRoot) {
                $this->request->request->add(['root_id' => $lastRoot->root_id + 1]);
            }
        }
        return $this;
    }

    /**
     * @return $this
     */
    protected function changePosition()
    {
        $menu_ids = array_values($this->request->only('prev', 'next', 'menu_id', 'parent_id'));
        $menus = Menu::active()->whereIn('id', $menu_ids)->get();

        if (!$menus)
            return $this;

        $sourceMenu = $menus->where('id', $this->request->get('menu_id'))->first();
        $prevMenu = $menus->where('id', $this->request->get('prev'))->first();
        $nextMenu = $menus->where('id', $this->request->get('next'))->first();
        $parentMenu = $menus->where('id', $this->request->get('parent_id'))->first();

        // change parent
        $this->changeParent($sourceMenu, $parentMenu);

        //change sort
        $menusSorting = $this->changeSorting($sourceMenu, $this->request->get('parent_id'), $prevMenu, $nextMenu);

        return $this->setResponse(true, trans("core::messages.alert.save_success"));

    }

    /**
     * @param $menu
     * @param $new_parent
     * @return bool
     */
    private function changeParent($menu, $new_parent)
    {
        if ($menu && $new_parent && $new_parent->id != $menu->parent_id) {
            $menu->parent_id = $new_parent->id;
            return $menu->save();
        }
        return false;
    }

    /**
     * @param $menu
     * @param $parent_id
     * @param null $prev
     * @param null $next
     * @return $this
     */
    private function changeSorting($menu, $parent_id, $prev = null, $next = null)
    {
        $source_id = $menu->id;
        $parentMenuList = Menu::active()->where('parent_id', $parent_id)->orderBy('sort')->get();
        $menusSortingId = $parentMenuList->pluck('id')->toArray();
        $collection = collect($menusSortingId);
        unset($menusSortingId[array_search($source_id, $collection->toArray())]);
        $collection = collect($menusSortingId)->values();
        $array = $collection->toArray();
        $offset = 0;
        $length = 0;
        if ($prev) {
            $offset = array_search($prev->id, $array) + 1;
        } elseif ($next) {
            $offset = array_search($next->id, $array);
        }

        $collection->splice($offset, $length, [$source_id]);
        $updatedMenuId = $collection->each(function ($menu_id, $index) {
            $menuUpd = Menu::active()->find($menu_id);
            $menuUpd->sort = $index;
            $menuUpd->save();
            return $menu_id;
        });

        return $updatedMenuId;
    }

}
