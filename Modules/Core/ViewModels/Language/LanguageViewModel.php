<?php

namespace Modules\Core\ViewModels\Language;

use Modules\Core\Models\Language\Language;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;

class LanguageViewModel extends BaseViewModel
{

    use GridViewModel;

    private $nestable_assets;

    public function __construct()
    {

    }


    public function getDirections($id = null)
    {
        return [ 'rtl' => 'RTL',  'ltr' => 'LTR'];
    }

    public function setGridModel()
    {
        $this->Model = Language::active();
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $language_assets = [];

        //use bested assets
        if ($this->nestable_assets) {
            $language_assets [] = ['container' => 'theme_style', 'src' => ("assets/global/plugins/jquery-nestable/jquery.nestable.css"), 'name' => 'jquery.nestable'];
            $language_assets [] = ['container' => 'theme_js', 'src' => ("assets/global/plugins/jquery-nestable/jquery.nestable-rtl.js"), 'name' => 'jquery.nestable'];
            $language_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/language.js"), 'name' => 'language-backend'];
        }

        return $language_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('name', trans('core::language.name'), true)
            ->addColumn('code', trans('core::language.code'), true)
            ->addColumn('direction',trans('core::language.direction'),true);

        /*add action*/
        $add = array(
            'name' => 'core.language.create',
            'parameter' => null
        );
        $this->addButton('create_language', $add,trans('core::language.add_language'), 'fa fa-plus', false,'before', ['target' => '', 'class' => 'btn btn-danger']);

        /*add action*/
//        $add = array(
//            'name' => 'core.language.create',
//            'parameter' => null
//        );
//        $this->addAction('add', $add, trans('core::language.add_language'), 'fa fa-plus', false, ['target' => '_blank', 'class' => 'btn btn-sm btn-warning']);


        $show = array(
            'name' => 'core.language.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('core::language.edit_language'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm'=>trans('core::language.confirm_delete')];
        $delete = array(
            'name' => 'core.language.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('core::language.delete_language'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
//        $this->addFilter('id', 'text', ['op' => '=']);
        $this->addFilter('name', 'text');
        $this->addFilter('code', 'text');
        $this->addFilter('direction', 'select', ['field' => 'direction', 'options' => $this->getDirections(), 'title' => trans('core::validations.attributes.category')]);
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('core::language.language_list'));
        $this->generateGridList()->renderedView("core::language.index", ['view_model' => $this], "language_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createMenu()
    {
        $language = Language::active()->find($this->request->get('language_id'));
        $this->setTitlePage(trans('core::language.'.($language?'edit_language':'add_language')));
        return $this->renderedView("core::language.form", ['language' => $language,], "form_language");
    }

    /**
     * @return $this
     */
    protected function destroyLanguage()
    {
        $language_id = $this->request->get('language_id');
        $language = Language::active()->find($language_id);

        if ($language->delete()) {
            return $this->setResponse(true, trans("core::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("core::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveLanguage()
    {
        $language = new Language();
        if ($language_id = $this->request->get("language_id")) {
            $language = $language->active()->find($language_id);
            if (!$language) {
                return $this->redirect(route('core.language.edit', ['language_id' => $this->request->get('language_id')]))->setResponse(false, trans("core::messages.alert.not_find_data"));
            }
        }


        if (($language->fill($this->request->all())->isValid()) || ($language_id && $language)) {
            $language->save();
            $language->updated = $language_id ? true : false;
            return $this->redirect(route('core.language.edit', ['language_id' => $language->id]))->setDataResponse($language)->setResponse(true, trans("core::messages.alert.save_success"));
        }

        return $this->redirect(route('core.language.create'))->setResponse(false, trans("core::messages.alert.mis_data"), $language->errors);
    }


    /**
     * @return $this
     */
    protected function changerLanguage()
    {
        $current_language_id = $this->request->get("current_language_id");
        $language = Language::active()->where('id', $current_language_id)->first();
//        dd(url()->previous(), request()->getHost(), request()->getScheme(), url()->to('video/2/edit'));

        $url = str_replace(request()->getScheme()."://","",url()->previous());
        $url = str_replace(request()->getHost().'/',"",$url);
        $urlArr = explode("/", $url);

        $prvLanguage = null;
        if(isset($urlArr[0]) && strlen($urlArr[0])==2){
            $prvLanguage = $urlArr[0];
            unset($urlArr[0]);
        }

        $url = url()->to($language->code.'/'.implode("/",$urlArr));

        if($prvLanguage){
            $url = str_replace("/".$prvLanguage."/", "/", $url);
        }

        if($language){
            return $this->redirect($url)->setResponse(true);
        }else{
            return $this->redirectBack()->setResponse(true);
        }

    }



}
