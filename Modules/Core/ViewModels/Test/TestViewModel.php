<?php

namespace Modules\Core\ViewModels\Test;

use Modules\Core\ViewModels\Dashboard\IndexViewModel;


class TestViewModel extends IndexViewModel
{

    public $layout='None';
    public function index(){
        return response()->json(['1'=>2]);
    }
}
