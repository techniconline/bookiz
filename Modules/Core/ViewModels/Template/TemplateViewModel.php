<?php

namespace Modules\Core\ViewModels\Template;

use Modules\Core\Models\Template\Template;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;

class TemplateViewModel extends BaseViewModel
{

    use GridViewModel;

    private $create_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = Template::active();
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $assets = [];

        //use bested assets
        if ($this->create_assets) {
//            $assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/template.js"), 'name' => 'template-backend'];
        }

        return $assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('name', trans('core::template.name'), true)
            ->addColumn('default', trans('core::template.default'), false)
            ->addColumn('code', trans('core::template.code'), false);

        /*add action*/
        $add = array(
            'name' => 'core.template.create',
            'parameter' => null
        );
        $this->addButton('create_template', $add, trans('core::template.add_template'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        /*add action*/
//        $add = array(
//            'name' => 'core.template.create',
//            'parameter' => null
//        );
//        $this->addAction('add', $add, trans('core::template.add_language'), 'fa fa-plus', false, ['target' => '_blank', 'class' => 'btn btn-sm btn-warning']);


        $show = array(
            'name' => 'core.template.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('core::template.edit_template'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm'=>trans('core::template.confirm_delete')];
        $delete = array(
            'name' => 'core.template.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('core::template.delete_template'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('name', 'text');
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('core::template.template_list'));
        $this->generateGridList()->renderedView("core::template.index", ['view_model' => $this], "template_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createTemplate()
    {
        $template = Template::active()->find($this->request->get('template_id'));
        $this->setTitlePage(trans('core::template.' . ($template ? 'edit_template' : 'add_template')));
        return $this->renderedView("core::template.form", ['template' => $template]
            , "form_template");
    }

    /**
     * active assets for method called!
     * @return $this
     */
    protected function setAssetsCreateTemplate()
    {
        $this->create_assets = true;
        return $this;
    }

    /**
     * @return $this
     */
    protected function destroyTemplate()
    {
        $template_id = $this->request->get('template_id');
        $template = Template::active()->find($template_id);

        if ($template->delete()) {
            return $this->setResponse(true, trans("core::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("core::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveTemplate()
    {
        $template = new Template();
        if ($template_id = $this->request->get("template_id")) {
            $template = $template->active()->find($template_id);
            if (!$template) {
                return $this->redirect(route('core.template.edit', ['template_id' => $this->request->get('template_id')]))->setResponse(false, trans("core::messages.alert.not_find_data"));
            }
        }

        $this->request->request->set('default', $this->request->get('default', 0));
        $this->request->request->set('backend', $this->request->get('backend', 0));

        if (($template->fill($this->request->all())->isValid()) || ($template_id && $template)) {
            $template->save();
            $template->updated = $template_id ? true : false;
            return $this->redirect(route('core.template.edit', ['template_id' => $template->id]))->setDataResponse($template)->setResponse(true, trans("core::messages.alert.save_success"));
        }

        return $this->redirect(route('core.template.create'))->setResponse(false, trans("core::messages.alert.mis_data"), $template->errors);
    }


}
