<?php

namespace Modules\Core\ViewModels\Traits;

use App\Http\Services\BaseService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Modules\Core\Models\Block\Block;
use BridgeHelper;

trait WidgetTrait
{
    public $widgets_rendered;
    public $instance;
    public $is_admin_panel;
    public $route_widgets;
    public $special_widgets;
    public $rendered_widgets;
    public $instance_route_block_cache_key_backend;
    public $can_widget_cache=null;

    public function __construct()
    {
        $this->route_widgets = collect();
        $this->special_widgets = collect();
    }

    /**
     * @param array $widgets --> [  key_widget => [ alias=> value, data=>view_model ] , ... ]
     * key_widget an alias is required
     * @return $this
     */
    public function setWidgets(array $widgets = [])
    {
        if ($this->theme_use && !empty($widgets)) {
            $widgets_rendered = [];
            foreach ($widgets as $key => $widget) {
                $alias = isset($widget['alias']) && $widget['alias'] ? $widget['alias'] : null;
                if (!$alias)
                    continue;

                if ($widget_rendered = $this->renderWidget($widget, $alias)) {
                    $widgets_rendered['widget_' . $key] = $widget_rendered;
                }
            }
            $this->widgets_rendered = $widgets_rendered;
        }
        return $this;
    }

    /**
     * @param $widget
     * @param $html_blade
     * @return mixed
     */
    private function renderWidget($widget, $alias)
    {
        $data = isset($widget['data']) && $widget['data'] ? $widget['data'] : null;
        return $this->theme_use->widget($alias, compact('data'))->render();
    }

    /**
     * widgets_rendered = [ key=>value, ... ]
     * data => [ key => value ]
     * @return $this
     */
    private function addWidgetsToData()
    {
        if ($this->widgets_rendered && $this->data) {
            $this->data['widgets'] = $this->widgets_rendered;
        }
        return $this;
    }

    /**
     * @return $this
     */
    private function setInstance()
    {
        $this->instance = app("getInstanceObject")->getInstance();
        return $this->setInstanceIsAdminPanel();
    }

    /**
     * @return $this
     */
    private function setInstanceIsAdminPanel()
    {
        if ($this->instance) {
            $this->is_admin_panel = $this->instance->name == "admin" ? true : false;
        }
        return $this;
    }

    /**
     * @return $this
     */
    private function getRouteWidgets()
    {
        $instanceRouteBlock = $this->instance ? $this->instance->instanceRouteBlock() : null;
        $this->getRouteWidgetsData($instanceRouteBlock);
        $this->result['widgets'] = $this->rendered_widgets;

        return $this;
    }


    /**
     * @param $instanceBlockModel
     * @return $this
     */
    private function getRouteWidgetDataBackend($instanceBlockModel = null)
    {
        $blocks = $this->getAllBackedPreBlocks();

        if ($blocks) {
            $baseService = new BaseService();
            $keyCache = md5(json_encode($blocks)). $this->instance->id . "_lang_" . get_instance()->getLanguageId();
            $this->route_widgets = $baseService->caching($this->instance_route_block_cache_key_backend . $keyCache, function () use ($blocks) {
                $arrayWidgets = $blocks;
                return collect($arrayWidgets)->mapWithKeys(function ($item, $key) {
                    $new_item[$key]["blocks"]['view_model'] = collect($item)->pluck("view_model", "alias")->toArray();
                    $new_item[$key]["blocks"]['data'] = $item;
                    return $new_item;
                });

            });

        }
        return $this->renderWidgetsBackend($instanceBlockModel);
    }

    private function getAllBackedPreBlocks()
    {
        $blocks = [];
        foreach ($this->configs as $module => $config) {
            if (isset($config['backend']['blocks']) && count($config['backend']['blocks'])) {
                foreach ($config['backend']['blocks'] as $positon => $block) {
                    if (is_array($block) && count($block)) {
                        if (!isset($blocks[$positon])) {
                            $blocks[$positon] = [];
                        }
                        foreach ($block as $item) {
                            $blocks[$positon][] = $item;
                        }
                    }
                }
            }
        }
        if (!count($blocks)) {
            return false;
        }
        return $blocks;
    }


    /**
     * @param null $instanceBlockModel
     * @return $this
     */
    private function renderWidgetsBackend($instanceBlockModel = null)
    {
        if (!$this->route_widgets)
            return $this;

        $this->rendered_widgets = $this->route_widgets->mapWithKeys(function ($item, $position) {

            $rendered[$position] = collect($item["blocks"]['data'])->map(function ($block, $k_block) use ($item, $position) {

                // render widgets data
                if (isset($block["view_model"]) && $block_view_model = $block["view_model"]) {
                    return $block = $this->getWidgetBackend($block_view_model);
                }


            })->values()->filter();
            return $rendered;
        });

        return $this->setContentBackend($instanceBlockModel);
    }

    /**
     * @param null $instanceBlockModel
     * @return $this
     */
    public function setContentBackend($instanceBlockModel = null)
    {
        if ($instanceBlockModel){
            $this->renderOnlyRouteWidget($instanceBlockModel);
        }

        if ($content = isset($this->result["content"]) ? $this->result["content"] : null) {
            $this->rendered_widgets->put("content", collect($content));
        }
        return $this;
    }

    /**
     * @param $block_view_model
     * @return bool
     */
    private function getWidgetBackend($block_view_model)
    {
        if (class_exists($block_view_model)) {
            $widget = new $block_view_model();
        } else {
            return false;
        }
        return $widget->render();
    }


    /**
     * use cache system
     * @param $instanceBlockModel
     * @param null $model_cache_key
     * @param string $variable_response
     * @return $this
     */
    private function getRouteWidgetsData($instanceBlockModel)
    {

        if ($this->is_admin_panel) {
            return $this->getRouteWidgetDataBackend($instanceBlockModel);
        }

        if (collect($this->route_widgets)->count()) {
            $this->route_widgets = $this->renderRouteWidgets($this->route_widgets);
            return $this->renderWidgets();
        }

        if (!$instanceBlockModel)
            return $this->renderWidgetOnlyContent();

        return $this->renderOnlyRouteWidget($instanceBlockModel);
    }

    /**
     * @param $instanceBlockModel
     * @return $this
     */
    private function renderOnlyRouteWidget($instanceBlockModel)
    {
        $this->layout_name = $instanceBlockModel->column;

        $baseService = new BaseService();
        $keyCache = md5($instanceBlockModel->json). $this->instance->id . "_lang_" . get_instance()->getLanguageId();
        $this->route_widgets = $baseService->caching($instanceBlockModel->instance_route_block_cache_key . $keyCache, function () use ($instanceBlockModel) {
            $arrayWidgets = $instanceBlockModel->json_array;
            return $this->renderRouteWidgets($arrayWidgets);
        });
        return $this->renderWidgets();
    }

    /**
     * @param $routeWidgets
     * @return static
     */
    private function renderRouteWidgets($routeWidgets)
    {
        return collect($routeWidgets)->mapWithKeys(function ($item, $key) {
            $block_ids = $new_item[$key]['block_ids'] = collect($item)->where('active', 1)->pluck("block_id")->toArray();
            $new_item[$key]['blocks'] = Block::active()->whereIn("id", $block_ids)->get()->keyBy("id")->toArray();
            $new_item[$key]['data'] = collect($item)->where('active', 1)->toArray();
            return $new_item;
        });
    }


    /**
     * TODO NOT COMPLETED
     *
     * this function is for merge route_widget system with route_widget pag
     * @param $routeWidgets
     *
     */
    private function mergeDataRouteWidgets($routeWidgets)
    {
        $globalRow = [];
        $r = $this->route_widgets->map(function ($item, $key) use ($routeWidgets) {
//                dd($item, $key);

            $appendItems = $routeWidgets->get("top");
//                dd($appendItems["blocks"]["data"], $item);
            if ($appendItems && isset($appendItems["blocks"]["data"])) {
                $item = collect(collect($item)->get("blocks"))->get("view_model");
                $item = array_merge($item, $appendItems["blocks"]["view_model"]);
                dd($item);
            }
            dd($appendItems["blocks"]["data"], $item);
        });
    }

    /**
     * @return $this
     */
    private function renderWidgets()
    {
        if (!$this->route_widgets)
            return $this;

        $this->rendered_widgets = $this->route_widgets->mapWithKeys(function ($item, $key) {
            $rendered[$key] = collect($item['data'])->map(function ($block, $k_block) use ($item, $key) {
                // assign content to widgets data
                if ($block && isset($block['block']) && $k_block == 0 && $block['block'] == "content") {
                    return $block = $this->getContent();
                }
                // render widgets data
                if ($block && isset($block['block_id']) && isset($item['blocks'][$block['block_id']]) && $item['blocks'][$block['block_id']]) {
                    $blockConfig = isset($item['data'][$k_block]) ? $item['data'][$k_block] : [];
                    try {
                        return $this->getWidget($item['blocks'][$block['block_id']], $blockConfig);
                    } catch (\Exception $exception) {
                        report($exception);
                        //dd($item, $block, $k_block, $this->special_widgets);
                    }
                }

            })->values()->filter()->values();

            return $rendered;

        });
        return $this;
    }

    private function renderWidgetOnlyContent()
    {
        $this->rendered_widgets = ['content' => [$this->getContent()]];
        return $this;
    }

    /**
     * @return $this
     */
    private function getInstanceSpecialWidgets()
    {
        $key = "instance_special_block_" . $this->instance->id . "_lang_" . get_instance()->getLanguageId();
        if (Cache::has($key)) {
            $instanceSpecialBlock = Cache::get($key);
        } else {
            $instanceSpecialBlock = $this->instance ? $this->instance->instanceSpecialBlock : null;
            Cache::forever($key, $instanceSpecialBlock);
        }
        $this->getSpecialWidgetsData($instanceSpecialBlock);
        $this->result['special_widgets'] = $this->rendered_widgets;

        return $this;
    }


    /**
     * @param $instanceBlockModel
     * @param null $model_cache_key
     * @param string $variable_response
     * @return $this
     */
    private function getSpecialWidgetsData($instanceBlockModel)
    {
        if (!$instanceBlockModel)
            return $this;

        $baseService = new BaseService();
        $keyCache = md5($instanceBlockModel->json) . $this->instance->id . "_lang_" . get_instance()->getLanguageId();
        $this->special_widgets = $baseService->caching($instanceBlockModel->instance_special_block_cache_key . $keyCache, function () use ($instanceBlockModel) {
            $arrayWidgets = $instanceBlockModel->json_array;
            return collect($arrayWidgets)->mapWithKeys(function ($values, $position) {
                $new_item = [];
                $new_item[$position] = collect($values)->mapWithKeys(function ($item, $key) {
                    $block_ids = $new_item[$key]['block_ids'] = collect($item)->where('active', 1)->pluck("block_id")->toArray();
                    $new_item[$key]['blocks'] = Block::active()->whereIn("id", $block_ids)->get()->keyBy("id")->toArray();
                    $new_item[$key]['data'] = collect($item)->where('active', 1)->values()->toArray();
                    return $new_item;
                });
                return $new_item;
            });

        });
        return $this->renderSpecialWidgets();
    }

    /**
     * @return $this
     */
    private function renderSpecialWidgets()
    {
        if (!$this->special_widgets)
            return $this;

        $this->rendered_widgets = $this->special_widgets->mapWithKeys(function ($values, $position) {

            $new_value = [];
            $new_value[$position] = $values->mapWithKeys(function ($item, $key) {
                $rendered[$key] = collect($item['block_ids'])->map(function ($block, $k_block) use ($item, $key) {

                    // assign content to widgets data
                    if ($key == "content" && !$k_block) {
                        return $block = $this->getContent();
                    }
                    // render widgets data
                    if (isset($item['blocks'][$block]) && $item['blocks'][$block]) {
                        try {
                            return $this->getWidget($item['blocks'][$block], $item['data'][$k_block]);
                        } catch (\Exception $exception) {
                            report($exception);
                            //dd($item, $block, $k_block, $this->special_widgets);
                        }
                    }

                })->filter()->values();
                return $rendered;
            });

            return $new_value;
        });

        return $this;
    }


    /**
     * @return $this
     */
    private function getContent()
    {
        return isset($this->result['content']) ? $this->result['content'] : null;
    }

    /**
     * @param $block
     * @return mixed
     */
    private function getWidget($block, $data = [])
    {
        $key = $this->getBlockCacheKey($block, $data);
        if ($key) {
            if (Cache::has($key)) {
                if (isset($data['configs']) && is_array($data['configs'])) {
                    $this->setWidgetAssetsOnTheme($data['configs']);
                }
                return Cache::get($key);
            }
        }
        $widget = BridgeHelper::getBlockHelper()->getBlock($block['name'], $block);
        if (!$widget) {
            return false;
        }

        if (app('getInstanceObject')->isApi()) {
            if (method_exists($widget, "renderJson")) {
                return $widget->setData($data)->renderJson();
            } else {
                return false;
            }
        }
        $rendered = $widget->setData($data)->render();
        if ($key) {
            Cache::put($key, $rendered, 60);
        }
        return $rendered;
    }

    /**
     * @param $block
     * @param $data
     * @return bool|string
     */
    public function getBlockCacheKey($block, $data)
    {
        $key = false;
        if ($this->getCanCache() && in_array($block['name'], ["slider_block", "html_open_close_block", "html_block", "counter_block", "course_category_block", "menu_block", "search_block", "logo_block", "partners_block", "courses_block"])) {
            $instance = app('getInstanceObject')->getCurrentInstance();
            $instanceOrg = app('getInstanceObject')->getInstance();
            $nameOrg = isset($instanceOrg->name) ? $instanceOrg->name : 'www';

            $code = crc32(json_encode($data));
            $name = isset($instance->name) ? $instance->name : 'www';
            $key = 'Master_Block_' . $this->can_widget_cache . '_' . $code . '_' . $block['id'] . '_' . $name. '_' . $nameOrg . "_lang_" . get_instance()->getLanguageId();
        }
        return $key;
    }


    public function getCanCache()
    {
        if (is_null($this->can_widget_cache)) {
            $instance = app('getInstanceObject')->getCurrentInstance();
            $this->can_widget_cache = false;
            if ($instance && $instance->name == 'www') {
                $this->can_widget_cache = $instance->name;
            }
        }
        return $this->can_widget_cache;
    }

    public function setWidgetAssetsOnTheme($configs)
    {
        if (isset($configs['files_html'])) {
            $files_html = $configs['files_html'];
            $assets = [];
            if (strlen($files_html)) {
                foreach (explode(',', $files_html) as $value) {
                    if (strlen($value)) {
                        $name = basename($value);
                        if (strpos($name, '.css') != false) {
                            $assets [] = ['container' => 'block_style', 'src' => trim($value), 'name' => $name];
                        } elseif (strpos($name, '.js') != false) {
                            $assets [] = ['container' => 'block_js', 'src' => trim($value), 'name' => $name];
                        }
                    }
                }

                $this->setWidgetAssetsOnRoute($assets);
            }
        }

        $assetsUrls = [];
        if (count($assetsUrls)) {
            foreach ($assetsUrls as $value) {
                if (strlen($value)) {
                    $name = basename($value);
                    if (strpos($name, '.css') != false) {
                        $assets [] = ['container' => 'block_style', 'src' => trim($value), 'name' => $name];
                    } elseif (strpos($name, '.js') != false) {
                        $assets [] = ['container' => 'block_js', 'src' => trim($value), 'name' => $name];
                    }
                }
            }
            $this->setWidgetAssetsOnRoute($assets);
        }

        return $this;
    }

    public function setWidgetAssetsOnRoute($assets)
    {
        if (count($assets)) {
            app('getInstanceObject')->setActionAssets($assets);
        }
        return $this;
    }
}
