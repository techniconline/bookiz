<?php

namespace Modules\Core\ViewModels\Traits;

use Illuminate\Support\Facades\Config;
use Modules\Core\Libraries\Theme\Facades\Theme;

trait ThemeTrait
{
    private $theme_name;
    private $theme_use;
    private $view_name = 'index';
    private $data = [];
    private $layout_name;
    private $layout_use;
    private $use_resource;
    private $resource_folder = "Resources";

    /**
     * @param null $theme
     * @return $this
     */
    public function setTheme($theme = null)
    {
        $this->theme_name = $theme?:isset($this->result['theme'])?$this->result['theme']:Config::get('theme.useTheme');
        $this->theme_use = Theme::uses($this->theme_name);
        return $this;
    }

    /**
     * @param null $layout , sample : mobile ,...
     * @return $this
     */
    public function setLayout($layout = null)
    {
        if ($this->theme_use) {
            if(!$this->layout_name){
                $this->layout_name = $layout?:isset($this->result['layout'])?$this->result['layout']:Config::get('theme.layoutDefault');
            }
            $this->layout_use = $this->theme_use->layout($this->layout_name);
        }

        return $this;
    }

    /**
     * @param null $theme
     * @return mixed
     */
    public function getTheme($theme = null)
    {
        return $this->setTheme($theme)->theme_use;
    }

    /**
     * use with set theme before get theme!
     * @return mixed
     */
    public function get()
    {
        return $this->theme_use;
    }

    /**
     * @param $view
     * @param bool $use_resource , for get blade html from resource folders
     * @return $this
     */
    public function setView($view, $use_resource = false)
    {
        $this->use_resource = $use_resource;
        $this->view_name = $view;
        return $this;
    }

    /**
     * @param $data
     * data => [ key => value ]
     * @return $this
     */
    public function setData(array $data = [])
    {
        $this->data = $data;
        return $this;
    }

    /**
     * return view after render
     * @return mixed
     */
    public function render()
    {
        $render = $this->addWidgetsToData()->setUrlView()->theme_use;
        if($this->use_resource){
            $render = $render->load($this->view_name, $this->data);
        }else{
            $render = $render->scope($this->view_name, $this->data);
        }
        return $render->render();
    }

    /**
     * @return $this
     */
    private function setUrlView()
    {
        $uri = "views";
        if($this->use_resource){
            if(strpos($this->view_name, "::")!==false){
                $folderArr = explode("::", $this->view_name,2);
                $uri = "Modules.".ucfirst(array_first($folderArr)).".".$this->resource_folder.".".$uri.".".end($folderArr);
            }else{
                $uri = "resources.".$uri.".".$this->view_name;
            }
            $this->view_name = $uri;
        }
        return $this;
    }
}
