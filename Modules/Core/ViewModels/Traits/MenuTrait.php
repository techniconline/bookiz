<?php

namespace Modules\Core\ViewModels\Traits;

use Illuminate\Support\Collection;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;

trait MenuTrait
{

    public $groups_menu_backend;
    public $menus_backend;
    public $append_menus;

    /**
     * @return $this
     */
    public function setMenuBackend()
    {
        if ( $this->is_admin_panel && $this->configs) {
            collect($this->configs)->map(function ($items) {
                if (isset($items['backend']['group_menu']) && $items['backend']['group_menu']) {
                    collect($items['backend']['group_menu'])->map(function ($items, $section) {
                        $this->getGroupMenuBackend($section)->decorateMenuBackend($section);
                    });
                }
            });
        }
        return $this;
    }

    /**
     * @return $this
     */
    private function setBackendMenuResult()
    {
        if ($this->is_admin_panel) {
            $this->result['backend_menus'] = $this->menus_backend;
        }
        return $this;
    }

    /**
     * @param $key_section
     * @return $this
     */
    private function decorateMenuBackend($key_section)
    {
        if ($this->configs) {

            $menus = collect($this->configs)->map(function ($items) use ($key_section) {
                if (isset($items['backend']['menus'][$key_section]) && $items['backend']['menus'][$key_section]) {
                    return collect($items['backend']['menus'][$key_section])->mapToGroups(function ($item, $key) {
                        $access = true;
                        if (isset($item["route"]) && $item["route"]) {
                            $access = BridgeHelper::getAccess()->canAccessRoute($item["route"],false);
                        }

                        if (!$access) {
                            return [$item['group_menu'] => null];
                        }

                        $item["can_access"] = $access;

                        if (is_null($item["order"]) || $item["order"] == "") {
                            $item["order"] = 1000000 + $key;
                        }

                        $new_item[$item['group_menu']] = $item;
                        return $new_item;
                    });
                }
            })->flatten(2)->filter()->groupBy("group_menu");


            $this->menus_backend[$key_section] = collect($menus)->mapWithKeys(function ($item, $key) use ($key_section) {
                return [$key => ($item)->sortBy("order")->values()];
            });

            //set in groups
            $menus = $this->getMenusToGroup($key_section);

            $this->menus_backend[$key_section] = $menus->merge($this->appendMenus($key_section)->append_menus);

        }

        return $this;
    }

    /**
     * @param $key_section
     * @return static
     */
    private function getMenusToGroup($key_section)
    {
        return collect($this->groups_menu_backend[$key_section]->sortBy('order'))->map(function ($item, $key) use ($key_section) {
            if (isset($this->menus_backend[$key_section][$key])) {
                $item["list"] = $this->menus_backend[$key_section][$key];
                unset($this->menus_backend[$key_section][$key]);
            }
            return $item;
        });
    }

    /**
     * @param $key_section
     * @return $this
     */
    private function appendMenus($key_section)
    {
        //append all menus with out group!
        $this->append_menus = collect($this->menus_backend[$key_section])->map(function ($item, $key) {
            $new_item = [
                "alias" => $key,
                "key_trans" => "core::menu.group." . $key,
                "icon_class" => "",
                "before" => "",
                "after" => "",
                "order" => 1000000,
                "list" => $item
            ];
            return $new_item;
        });

        return $this;
    }

    /**
     * @param $key_section
     * @return $this
     */
    private function getGroupMenuBackend($key_section)
    {
        $this->groups_menu_backend[$key_section] = collect($this->configs)->flatMap(function ($items) use ($key_section) {
            if (isset($items['backend']['group_menu'][$key_section]) && $items['backend']['group_menu'][$key_section]) {
                $d = collect($items['backend']['group_menu'][$key_section])->mapWithKeys(function ($item, $key) use ($key_section) {

                    if (is_null($item["order"]) || $item["order"] == "") {
                        $item["order"] = 1000000 + $key;
                    }
                    $new_item[$item['alias']] = $item;
                    return $new_item;
                });

                return $d;

            }
        });

        return $this;
    }

    /**
     * @param Collection $menus
     * @param null $parent_id
     * @param null $root_id
     * @return Collection
     */
    protected function getNestable(Collection $menus, $parent_id = null, $root_id = null)
    {
        $subMenus = $menus
            ->where('parent_id', $parent_id);
        if ($root_id) {
            $subMenus = $subMenus->where('root_id', $root_id);
        }
        $subMenus = $subMenus->sortBy('sort');
        $newArr = [];
        foreach ($subMenus as $menu) {
            $menu["childes"] = $this->getNestable($menus, $menu->id);
            $newArr[] = $menu;
        }
        return collect($newArr);
    }

}
