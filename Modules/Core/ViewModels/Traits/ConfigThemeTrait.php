<?php

namespace Modules\Core\ViewModels\Traits;

use function GuzzleHttp\Psr7\build_query;
use function GuzzleHttp\Psr7\parse_query;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Nwidart\Modules\Facades\Module;

trait ConfigThemeTrait
{
    public $configs = false;
    public $config;
    private $cache_key = 'all_cache_config_files';
    private $cache_permission_key = 'all_route_permission_cache';
    private $duration = 1440; //minutes, 1440 is 1 day


    /**
     * find files of config module and caching
     * @return $this
     */
    private function setConfigFiles()
    {
        $modules = Module::getByStatus(1);
        $configs = [];
        foreach ($modules as $key => $module) {
            $base_config_url = $module->getPath() . '/Config/';
            $files = File::files($base_config_url);

            foreach ($files as $file) {
                $path = $file->getPathname();
                $file_name = $file->getFilename();
                $ext = $file->getExtension();
                $key_file = str_replace($ext, "", $file_name);
                $keys_array = array_filter(explode(".", $key_file));
                $configs[$key][implode("_", $keys_array)] = File::getRequire($path);
            }
        }
        $this->configs = $configs;
        return $this->setCacheConfigs();
    }

    /**
     * cache configs
     * @return $this
     */
    private function setCacheConfigs()
    {
        if ($this->cache_key && $this->configs) {
            Cache::put($this->cache_key, $this->configs, $this->duration);
        }
        return $this;
    }

    /**
     * return all cache in object
     * @return $this
     */
    public function setConfigs()
    {
        if (Cache::has($this->cache_key)) {
            $this->configs = Cache::get($this->cache_key);
        } else {
            $this->setConfigFiles();
        }
        return $this;
    }

    public function getConfigs()
    {
        if ($this->configs) {
            return $this->configs;
        }
        $this->setConfigs();
        return $this->configs;
    }

    /**
     * @param null $module
     * @return null
     */
    public function getConfig($module = null)
    {
        $configs = $this->getConfigs();
        if (isset($configs[$module])) {
            return $configs[$module];
        }
        return null;
    }

    /**
     * @param $module
     * @return $this
     */
    public function setConfig($module)
    {
        return $this->setConfigModule($this->getConfig(ucfirst($module)));
    }

    /**
     * @param array $config --> [title=>value , assets => []]
     * @return $this
     */
    public function setConfigModule($config)
    {
        $this->config = $config && is_array($config) ? $config : null;

        if (!$this->config)
            return $this;

        return $this;
    }

    /**
     * @return $this
     */
    private function setTitle()
    {
        if ($this->theme_use && (key_exists('title', $this->config) || isset($this->data["title_page"]))) {
            if (isset($this->config['title'])) {
                $this->theme_use->setTitle($this->config['title']);
            }

            if (isset($this->data["title_page"])) {
                $this->theme_use->setTitle($this->data["title_page"]);
            }
        }
        return $this;
    }

    /**
     *
     * $this->>configs[assets]  => [ ['title'=>value, 'url'=> value , 'container'=>value ] ]
     *
     * in this data url is required
     *
     * @return $this
     */
    private function setAssets()
    {
        $currentTheme = app('getInstanceObject')->getInstanceTheme();
        if ($this->theme_use && key_exists('assets', $this->result) && !empty($this->result['assets'])) {
            foreach (app('getInstanceObject')->getActionAssets() as $item) {
                $this->result['assets'][] = $item;
            }
        } elseif ($this->theme_use) {
            $this->result['assets'] = app('getInstanceObject')->getActionAssets();
        }
        if ($this->theme_use && key_exists('assets', $this->result) && !empty($this->result['assets'])) {
            $assets = $this->result['assets'];
            $assets_title = [];
            foreach ($assets as $asset) {
                $objAsset = $container = null;
                $urlArr = parse_url($asset['src']);
                $path = $urlArr['path'];
                $extension = pathinfo($path, PATHINFO_EXTENSION);
                $qArr = [];
                if(isset($urlArr['query'])){
                    $qArr = explode("&", $urlArr['query']);
                }
                $version = $currentTheme?$currentTheme->version:'1.0';
                $qArr[] = '_v_t='.(number_format($version,1)).'.'.$extension;
                $query_str = implode("&", $qArr);
                $asset['src'] = $path.'?'.$query_str;
                $url = isset($asset['src']) && $asset['src'] ? $asset['src'] : null;
                if (!$url) {
                    continue;
                }
                $container = isset($asset['container']) ? $asset['container'] : '';
                $hash = md5($url . $container);
                if (in_array($hash, $assets_title)) {
                    continue;
                }
                $assets_title[] = $hash;
                $objAsset = $this->theme_use->asset();
                $url_theme = "";
                if ((isset($asset['container'])) && ($container = $asset['container'])) {
                    $objAsset = $objAsset->container($container);

                    if (strpos($container, "theme") !== false) {
                        $url_theme = "themes/" . $this->theme_name . "/";
                    }

                }
                $url = str_replace("//", "/", $url_theme . $url);
                $objAsset->add($hash, asset($url));
            }
        }
        return $this;
    }


    public function getRoutePermission($route)
    {
        $allRoutes = $this->getRoutesByPermissions();
        if (isset($allRoutes[$route]) && $allRoutes[$route]) {
            return $allRoutes[$route];
        }
        return true;
    }


    private function getRoutesByPermissions()
    {
        if (Cache::has($this->cache_permission_key)) {
            return Cache::get($this->cache_permission_key);
        }
        $permissions = [];
        $configs = $this->getConfigs();
        foreach ($configs as $module) {
            if (isset($module['config']) && isset($module['config']['permissions']) && is_array($module['config']['permissions'])) {
                $permissions = array_merge($permissions, $module['config']['permissions']);
            }
        }
        Cache::forever($this->cache_permission_key, $permissions);
        return $permissions;
    }
}
