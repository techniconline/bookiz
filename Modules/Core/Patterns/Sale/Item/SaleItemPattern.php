<?php
namespace Modules\Core\Patterns\Sale\Item;


/**
 * Class SaleItemPattern
 * @package Modules\Core\Patterns\Sale\Item
 */
abstract class SaleItemPattern
{
    /**
     * @var string
     */
    protected $type='';

    /**
     * @var int
     */
    protected $itemID=0;


    /**
     * @param $item_id
     * @return mixed
     */
    abstract public function setItemId($item_id);

    /**
     * @param $options
     * @return mixed
     */
    abstract public function setOptions($options);

    /**
     * @return mixed
     */
    abstract public function canSaleItem();

    /**
     * @return mixed
     */
    abstract public function getMaxQtyForSale();

    /**
     * @return mixed
     */
    abstract public function getSaleItem();

    /**
     * @return mixed
     */
    abstract public function getItemQty();

    /**
     * @return mixed
     */
    abstract public function getItemExtraData();

    /**
     * @param $order
     * @param $item
     * @return mixed
     */
    abstract public function setOrderComplete($order, $item);

    /**
     * @return mixed
     */
    abstract public function getCategoryIds();


    /**
     * @param $id
     * @param $type
     * @return bool
     */
    public function isSameItem($id, $type){
        if($this->itemID==$id && $type==$this->type){
            return true;
        }
        return false;
    }



}