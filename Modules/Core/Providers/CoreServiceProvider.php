<?php

namespace Modules\Core\Providers;

use App\Http\Services\BaseService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use \Illuminate\Support\Facades\Config;
use Modules\Core\Libraries\InstanceLibrary;
use Illuminate\Routing\Router;
use Illuminate\Http\Request;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @param Router $router
     * @param Request $request
     * @return void
     */
    public function boot(Router $router, Request $request)
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        if (function_exists('get_instance')){
            get_instance()->setMultiLangRoutes($router, $request);
        }

    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('getInstanceObject', function (){
            return new InstanceLibrary();
        });
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('core.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'core'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/core');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/core';
        }, \Config::get('view.paths')), [$sourcePath]), 'core');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/core');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'core');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'core');
        }
    }

    /**
     * Register an additional directory of factories.
     * @source https://github.com/sebastiaanluca/laravel-resource-flow/blob/develop/src/Modules/ModuleServiceProvider.php#L66
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'DateHelper'=>\Modules\Core\Providers\Helpers\Date\DateHelperServiceProvider::class,
            'FormHelper'=>\Modules\Core\Providers\Helpers\Form\FormHelperServiceProvider::class,
            'BridgeHelper'=>\Modules\Core\Providers\Helpers\Bridge\BridgeHelperServiceProvider::class
        ];
    }
}
