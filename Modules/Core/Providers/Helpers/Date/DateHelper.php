<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Core\Providers\Helpers\Date;

use Morilog\Jalali\jDateTime;
use Morilog\Jalali\Facades\jDate;
use Carbon\Carbon;

class DateHelper
{
    /*datetime*/
    const  DATETIME_LONG_FORMAT = 'l jS \of F Y h:i:s A';
    const DATETIME_MEDIUM_FORMAT = 'Y-m-d H:i:s';
    const DATETIME_SHORT_FORMAT = 'Y-m-d';

    /*date*/
    const DATE_FORMAT = 'Y-m-d';

    /*time*/
    const TIME_FORMAT = 'H:i:s';

    /*saved*/
    const DB_DATETIME_FORMAT = 'Y-m-d H:i:s';
    const DB_DATE_FORMAT = 'Y-m-d';
    const DB_TIME_FORMAT = 'H:i:s';

    private $helper = false;
    private $calender = null;
    private $timezone = 'asia/tehran';
    private $defaultTimezone = 'UTC';


    public function __construct()
    {
        $this->defaultTimezone = config('app.timezone');
        $this->calender = config('app.type_calendar', 'jalali');
        $this->timezone = $this->defaultTimezone;
        $this->boot();
    }

    private function boot()
    {
        $this->helper = new Carbon(null, $this->defaultTimezone);
    }

    public function setCalender($calender)
    {
        $this->calender = $calender;
        $this->boot();
        return $this;
    }

    public function setLocaleDateTime($str, $format = 'Y-m-d H:i:s', $timezone = null)
    {
        if (is_null($timezone)) {
            $timezone = $this->timezone;
        }
        switch ($this->calender) {
            case 'jalali':
                $jdate = jDateTime::createCarbonFromFormat($format, $str, $this->timezone);
                $str = $jdate->format($format);
                break;
        }
        $this->helper = new Carbon($str, $this->defaultTimezone);
        return $this;
    }

    public function setDateTime($str = null, $timezone = null, $format = 'Y-m-d H:i:s')
    {
        if ($str){
            $arr_str = explode(" ", $str);
            $arr_format = explode(" ", $format);
            if (count($arr_str)!=count($arr_format)){
                if (count($arr_str)>count($arr_format)){
                    $str = array_first($arr_str);
                }else{
                    $format = array_first($arr_format);
                }
            }
        }
        if (is_null($str)) {
            $str = date($format);
        }
        if (is_null($timezone)) {
            $timezone = $this->defaultTimezone;
        }
        $date = Carbon::createFromFormat($format, $str);
        $this->helper = new Carbon($date->toDateTimeString(), $timezone);
        return $this;
    }

    public function getDateTime($format = 'Y-m-d H:i:s')
    {
        return $this->helper->format($format);
    }

    public function getLocaleFormat($format = 'Y-m-d H:i:s')
    {

        switch ($this->calender) {
            case 'jalali':
                $jdate = jDate::forge($this->helper->getTimestamp(), $this->timezone);
                return $jdate->format($format);
                break;
        }
        return $this->helper->format($format);
    }


    public function getMonthList()
    {
        switch ($this->calender) {
            case 'jalali':
                $list = [
                    '01' => ['name' => trans('core::date.jalali.month.month1'), 'days' => (int)trans('core::date.jalali.days.month1')],
                    '02' => ['name' => trans('core::date.jalali.month.month2'), 'days' => (int)trans('core::date.jalali.days.month2')],
                    '03' => ['name' => trans('core::date.jalali.month.month3'), 'days' => (int)trans('core::date.jalali.days.month3')],
                    '04' => ['name' => trans('core::date.jalali.month.month4'), 'days' => (int)trans('core::date.jalali.days.month4')],
                    '05' => ['name' => trans('core::date.jalali.month.month5'), 'days' => (int)trans('core::date.jalali.days.month5')],
                    '06' => ['name' => trans('core::date.jalali.month.month6'), 'days' => (int)trans('core::date.jalali.days.month6')],
                    '07' => ['name' => trans('core::date.jalali.month.month7'), 'days' => (int)trans('core::date.jalali.days.month7')],
                    '08' => ['name' => trans('core::date.jalali.month.month8'), 'days' => (int)trans('core::date.jalali.days.month8')],
                    '09' => ['name' => trans('core::date.jalali.month.month9'), 'days' => (int)trans('core::date.jalali.days.month9')],
                    '10' => ['name' => trans('core::date.jalali.month.month10'), 'days' => (int)trans('core::date.jalali.days.month10')],
                    '11' => ['name' => trans('core::date.jalali.month.month11'), 'days' => (int)trans('core::date.jalali.days.month11')],
                    '12' => ['name' => trans('core::date.jalali.month.month12'), 'days' => (int)trans('core::date.jalali.days.month12')],
                ];
                break;
            default:
                $list = [
                    '01' => ['name' => trans('core::date.gregorian.month.month1'), 'days' => (int)trans('core::date.gregorian.days.month1')],
                    '02' => ['name' => trans('core::date.gregorian.month.month2'), 'days' => (int)trans('core::date.gregorian.days.month2')],
                    '03' => ['name' => trans('core::date.gregorian.month.month3'), 'days' => (int)trans('core::date.gregorian.days.month3')],
                    '04' => ['name' => trans('core::date.gregorian.month.month4'), 'days' => (int)trans('core::date.gregorian.days.month4')],
                    '05' => ['name' => trans('core::date.gregorian.month.month5'), 'days' => (int)trans('core::date.gregorian.days.month5')],
                    '06' => ['name' => trans('core::date.gregorian.month.month6'), 'days' => (int)trans('core::date.gregorian.days.month6')],
                    '07' => ['name' => trans('core::date.gregorian.month.month7'), 'days' => (int)trans('core::date.gregorian.days.month7')],
                    '08' => ['name' => trans('core::date.gregorian.month.month8'), 'days' => (int)trans('core::date.gregorian.days.month8')],
                    '09' => ['name' => trans('core::date.gregorian.month.month9'), 'days' => (int)trans('core::date.gregorian.days.month9')],
                    '10' => ['name' => trans('core::date.gregorian.month.month10'), 'days' => (int)trans('core::date.gregorian.days.month10')],
                    '11' => ['name' => trans('core::date.gregorian.month.month11'), 'days' => (int)trans('core::date.gregorian.days.month11')],
                    '12' => ['name' => trans('core::date.gregorian.month.month12'), 'days' => (int)trans('core::date.gregorian.days.month12')],
                ];
                break;
        }
        return $list;
    }


    public function __call($name, $arguments)
    {
        return call_user_func_array(array($this->helper, $name), $arguments);
    }


}