<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Core\Providers\Helpers\Date;

use Illuminate\Support\ServiceProvider;

class DateHelperServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->registerThisHelperBuilder();
        $this->app->alias("DateHelper", "Modules\Core\Providers\Helpers\Date");
    }

    protected function registerThisHelperBuilder(){
        $this->app->singleton("DateHelper", function (){
            return new DateHelper();
        });
    }

}