<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Core\Providers\Helpers\Menu;

use Illuminate\Support\ServiceProvider;

class MenuHelperServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->registerThisHelperBuilder();
        $this->app->alias("menu", "Modules\Core\Providers\Helpers\Menu");
    }

    protected function registerThisHelperBuilder(){
        $this->app->singleton("menu", function (){
            return new MenuHelper();
        });
    }

}