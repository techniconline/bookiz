<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Core\Providers\Helpers\Menu;

use Illuminate\Support\Facades\Facade;

class MenuHelperFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "menu";
    }
}