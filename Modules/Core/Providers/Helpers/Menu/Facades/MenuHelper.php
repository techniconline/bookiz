<?php
namespace Modules\Core\Providers\Helpers\Menu\Facades;

use Illuminate\Support\Facades\Facade;

class MenuHelper extends Facade
{
    protected static function getFacadeAccessor() { return 'menu'; }

}