<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Core\Providers\Helpers\Menu;
use Illuminate\Support\Collection;
use Modules\Core\Models\Menu\Menu;

class MenuHelper
{

    private static $html = '';

    public static function createTreeByNestable($menus)
    {

        self::$html = '
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bubble font-purple"></i>
                        <span class="caption-subject font-purple sbold uppercase">'.trans("core::menu.list_menu",["root_menu"=>isset($menus[0])?' ( '.$menus[0]->title.' ) ':'...']).'</span>
                    </div>
                    <div class="actions">
                        <!--<div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                                <input type="radio" name="options" class="toggle" id="option1">New</label>
                            <label class="btn btn-transparent grey-salsa btn-circle btn-sm">
                                <input type="radio" name="options" class="toggle" id="option2">Returning</label>
                        </div>
                        -->
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="dd" id="nestable_list_menu_tree" data-token="'.csrf_token().'">
                        <ol class="dd-list">
        ';
        self::$html  .= '<script>';
        self::$html  .= 'var $json_data = {};';
        self::$html  .= '</script>';

        self::renderNestableChildes($menus, true);

        self::$html .= '
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        ';

        return self::$html;

    }

    private static function renderNestableChildes($menus, $is_root = false)
    {

        foreach ($menus as $menu) {
            self::$html  .= '<script>';
            self::$html  .= '$json_data['.$menu->id.'] = '.(json_encode($menu->toArray())).'';
            self::$html  .= '</script>';
            self::$html  .= '<li class="dd-item dd3-item" data-id="'.$menu->id.'" data-is-root="'.($is_root?1:0).'">';
            self::$html .= '
                    <div class="dd-handle dd3-handle _change_position" change-position-url="'.route('core.menu.change_position',['menu_id'=>$menu->id]).'"></div>
                    <div class="dd3-content">  
                        <a href="'.route('core.menu.update',['menu_id'=>$menu->id]).'" class="btn btn-xs btn-default blue _edit" data-json=""><i class="fa fa-edit"></i></a> 
                        <a href="'.route('core.menu.add_child',['menu_id'=>$menu->id]).'" class="btn btn-xs btn-default green _add_child"><i class="fa fa-plus-circle"></i></a> 
                        <a href="'.route('core.menu.delete',['menu_id'=>$menu->id]).'" class="btn btn-xs btn-default red _delete"><i class="fa fa-trash"></i></a> 
                    '.$menu->title.' 
                    </div>
                ';
            if($menu->childes->count()){

                self::$html .= '
                    <ol class="dd-list">
                ';

                self::renderNestableChildes($menu->childes);

                self::$html .= '
                    </ol>
                ';
            }

            self::$html .= '</li>';
        }

    }


    public function getNestableMenu($id,$deep=1){
        $menuModel=new Menu();
        $menu=$menuModel->active()->filterLanguage()->filterCurrentInstance()->find($id);
        if(!$menu){
            return collect([]);
        }
        $root_id=$menu->root_id;
        $roots=$menuModel->active()->where('root_id', $root_id)->get();
        $menus=$this->getNestable($roots, null, $root_id,$deep+1)->first();
        if($menus){
            return $menus->childes;
        }

        return $menus;
    }

    /**
     * @param Collection $menus
     * @param null $parent_id
     * @param null $root_id
     * @return Collection
     */
    protected function getNestable(Collection $menus, $parent_id = null, $root_id = null,$deep=10)
    {
        $subMenus = $menus
            ->where('parent_id', $parent_id);
        if ($root_id) {
            $subMenus = $subMenus->where('root_id', $root_id);
        }
        $subMenus = $subMenus->sortBy('sort');
        $newArr = [];
        if($deep){
            foreach ($subMenus as $menu) {
                $newdeep=$deep-1;
                $menu["childes"] = $this->getNestable($menus, $menu->id,null,$newdeep);
                $newArr[] = $menu;
            }
        }
        return collect($newArr);
    }



    public function getMenuUrl(Menu $menu){
        if(empty($menu->url)){
            $menu->url='#';
        }
        switch ($menu->type){
            case 'external_url':
                return $menu->url;
            break;
            case 'route':
                return route($menu->url);
            break;
            default:
                return url($menu->url);
                break;
        }
    }

}