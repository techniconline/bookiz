<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Core\Providers\Helpers\Bridge;

/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class BridgeHelper
{
    private $cached = [];
    private $namespaces = [
        'BlockHelper' => "\\Modules\\Core\\Bridge\\BlockHelper\\BlockHelper",
        'Access' => "\\Modules\\User\\Bridge\\Access\\Access",
        'User' => "\\Modules\\User\\Bridge\\User\\User",
        'Config' => "\\Modules\\Core\\Bridge\\Config\\Config",
        'MenuHelper' => "\\Modules\\Core\\Providers\\Helpers\\Menu\\MenuHelper",
        'CurrencyHelper' => "\\Modules\\Core\\Bridge\\CurrencyHelper\\CurrencyHelper",
        'LocationHelper' => "\\Modules\\Core\\Bridge\\LocationHelper\\LocationHelper",
        'Qtoa' => "\\Modules\\Feedback\\Bridge\\Qtoa\\Qtoa",
        'Comment' => "\\Modules\\Feedback\\Bridge\\Comment\\Comment",
        'Rate' => "\\Modules\\Feedback\\Bridge\\Rate\\Rate",
        'CartHelper' => "\\Modules\\Sale\\Bridge\\CartHelper\\CartHelper",
        'EnrollmentHelper' => "\\Modules\\Course\\Bridge\\EnrollmentHelper\\EnrollmentHelper",
        'VideoUse' => "\\Modules\\Video\\Bridge\\VideoUse\\VideoUse",
        'Course' => "\\Modules\\Course\\Bridge\\Course\\Course",
        'Video' => "\\Modules\\Video\\Bridge\\Video\\Video",
        'Exam' => "\\Modules\\Exam\\Bridge\\Exam\\Exam",
        'Entity' => "\\Modules\\Entity\\Bridge\\Entity\\Entity",
        'Booking' => "\\Modules\\Booking\\Bridge\\Booking\\Booking",
        'Like' => "\\Modules\\Feedback\\Bridge\\Like\\Like",
    ];


    public function get($name)
    {
        $name = ucfirst($name);
        if (isset($this->namespaces[$name])) {
            if (isset($this->cached[$name])) {
                return $this->cached[$name];
            }
            $this->cached[$name] = new $this->namespaces[$name]();
            return $this->cached[$name];
        }
        return null;
    }

    public function set($name, $namespace)
    {
        $this->namespaces[$name] = $namespace;
        return $this;
    }

    public function __call($method, $args)
    {
        if (strpos($method, 'get') == 0) {
            $method = str_replace('get', '', $method);
        }
        return $this->get($method);
    }

}