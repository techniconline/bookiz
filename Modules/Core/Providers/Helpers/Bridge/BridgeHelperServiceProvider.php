<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Core\Providers\Helpers\Bridge;

use Illuminate\Support\ServiceProvider;

class BridgeHelperServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->registerThisHelperBuilder();
        $this->app->alias("BridgeHelper", "Modules\Core\Providers\Helpers\Bridge");
    }

    protected function registerThisHelperBuilder(){
        $this->app->singleton("BridgeHelper", function ($app){
            return new BridgeHelper();
        });
    }

}