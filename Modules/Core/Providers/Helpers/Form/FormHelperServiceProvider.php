<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Core\Providers\Helpers\Form;

use Illuminate\Support\ServiceProvider;

class FormHelperServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->registerThisHelperBuilder();
        $this->app->alias("FormHelper", "Modules\Core\Providers\Helpers\Form");
    }

    protected function registerThisHelperBuilder(){
        $this->app->singleton("FormHelper", function ($app){
            return new FormHelper($app['html'], $app['url'], $app['view'], $app['session.store']->token(), $app['request']);
        });
    }

}