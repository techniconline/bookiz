<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Core\Providers\Helpers\Form;

use \Collective\Html\FormBuilder;
use \Collective\Html\HtmlBuilder;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use DateHelper;
use Illuminate\Support\MessageBag;

/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class FormHelper extends FormBuilder
{

    use ExtraFieldsTrait;

    /**
     * @var array
     */
    private $assets = [];
    /**
     * @var string
     */
    private $templateName = '';
    /**
     * @var string
     */
    private $renderer = '';


    /**
     * FormHelper constructor.
     * @param HtmlBuilder $html
     * @param UrlGenerator $url
     * @param Factory $view
     * @param $csrfToken
     * @param Request|null $request
     */
    public function __construct(HtmlBuilder $html, UrlGenerator $url, Factory $view, $csrfToken, Request $request = null)
    {
        parent::__construct($html, $url, $view, $csrfToken, $request);
        $this->assets[] = [ 'src' => "assets/modules/core/form/form.js", 'name' => 'form-default'];
        $this->templateName = 'material';
        $rendererClass = "\\Modules\\Core\\Providers\\Helpers\\Form\\Template\\" . ucfirst($this->templateName) . 'Template';
        $this->renderer = new $rendererClass();
    }

    /**
     * @param array $options
     * @return \Illuminate\Support\HtmlString|mixed
     */
    public function open(array $options = [])
    {
        $html = parent::open($options);
        return $this->renderByView('open', ['html' => $html]);
    }

    /**
     * @return mixed|string
     */
    public function close($allAssets=false)
    {
        $html = parent::close();
        $html.=$this->afterHtml;
        if($allAssets){
            $this->setAllAssets();
        }
        $this->setAssetsOnTheme();
        return $this->renderByView('close', ['html' => $html]);
    }

    public function setAllAssets(){
        return $this->renderer->setAllAsset();
    }
    /**
     * @param string $type
     * @param string $name
     * @param null $value
     * @param array $options
     * @return \Illuminate\Support\HtmlString|mixed
     */
    public function input($type, $name, $value = null, $options = [])
    {

        list($options, $renderOptions) = $this->getClearedOptions($name, $type, $options);
        $html = parent::input($type, $name, $value, $options);
        return $this->render($name, $type, $html, $renderOptions);
    }

    /**
     * @param string $name
     * @param null $value
     * @param array $options
     * @return \Illuminate\Support\HtmlString|mixed
     */
    public function text($name, $value = null, $options = [])
    {
        return $this->input('text', $name, $value, $options);
    }


    /**
     * @param string $name
     * @param array $options
     * @return \Illuminate\Support\HtmlString|mixed
     */
    public function password($name, $options = [])
    {
        return $this->input('password', $name, '', $options);
    }

    /**
     * @param string $name
     * @param null $value
     * @param array $options
     * @return \Illuminate\Support\HtmlString|mixed
     */
    public function tel($name, $value = null, $options = [])
    {
        return $this->input('tel', $name, $value, $options);
    }

    /**
     * Create a number input field.
     *
     * @param  string $name
     * @param  string $value
     * @param  array $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function number($name, $value = null, $options = [])
    {
        return $this->input('number', $name, $value, $options);
    }

    /**
     * Create a hidden input field.
     *
     * @param  string $name
     * @param  string $value
     * @param  array $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function hidden($name, $value = null, $options = [])
    {
        return parent::input('hidden', $name, $value, $options);
    }

    /**
     * Create a date input field.
     *
     * @param  string $name
     * @param  string $value
     * @param  array $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function date($name, $value = null, $options = [])
    {
        $type = 'date';
        list($options, $renderOptions) = $this->getClearedOptions($name, $type, $options);
        $defaultValues = ["date-year-current" => 0, "date-year-before" => 50, "set-default" => false, "date-input-format" => 'Y-m-d'];
        $data = $renderOptions;
        foreach ($defaultValues as $key => $ivalue) {
            if (isset($options[$key])) {
                $data[str_replace('-', '_', $key)] = $options[$key];
                unset($options[$key]);
            } else {
                $data[str_replace('-', '_', $key)] = $ivalue;
            }
        }
        $data['value'] = $value;
        $data['name'] = $name;
        $data['attributes'] = $options;
        $html = $this->renderByView('date', $data);
        return $this->render($name, $type, $html, $renderOptions);
    }

    /**
     * Create a datetime input field.
     *
     * @param  string $name
     * @param  string $value
     * @param  array $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function datetime($name, $value = null, $options = [])
    {
        $type = 'datetime';
        list($options, $renderOptions) = $this->getClearedOptions($name, $type, $options);
        $defaultValues = ['date-year-current' => 0, 'date-year-before' => 50, 'set-default' => false, 'date-input-format' => 'Y-m-d'];
        $data = $renderOptions;
        foreach ($defaultValues as $key => $ivalue) {
            if (isset($options[$key])) {
                $data[str_replace('-', '_', $key)] = $options[$key];
                unset($options[$key]);
            } else {
                $data[str_replace('-', '_', $key)] = $ivalue;
            }
        }
        $data['value'] = $value;
        $data['name'] = $name;
        $data['attributes'] = $options;
        $html = $this->renderByView('datetime', $data);
        return $this->render($name, $type, $html, $renderOptions);
    }


    /**
     * Create a datetime input field.
     *
     * @param  string $name
     * @param  string $value
     * @param  array $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function time($name, $value = null, $options = [])
    {
        $type = 'datetime';
        list($options, $renderOptions) = $this->getClearedOptions($name, $type, $options);
        $defaultValues = ['set-default' => false, 'date-input-format' => 'H:i'];
        $data = $renderOptions;
        foreach ($options as $key => $ivalue) {
            if (isset($defaultValues[$key])) {
                $data[str_replace('-', '_', $key)] = $ivalue;
                unset($options[$key]);
            }
        }
        $data['value'] = $value;
        $data['name'] = $name;
        $data['attributes'] = $options;
        $html = $this->renderByView('time', $data);
        return $this->render($name, $type, $html, $renderOptions);
    }

    /**
     * Create a datetime-local input field.
     *
     * @param  string $name
     * @param  string $value
     * @param  array $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function datetimeLocal($name, $value = null, $options = [])
    {
        if ($value instanceof DateTime) {
            $value = $value->format('Y-m-d\TH:i');
        }

        return $this->input('datetime-local', $name, $value, $options);
    }

    /**
     * Create a url input field.
     *
     * @param  string $name
     * @param  string $value
     * @param  array $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function url($name, $value = null, $options = [])
    {
        return $this->input('url', $name, $value, $options);
    }

    /**
     * Create a file input field.
     *
     * @param  string $name
     * @param  array $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function file($name, $options = [])
    {
        return $this->input('file', $name, null, $options);
    }

    /**
     * @param string $name
     * @param null $value
     * @param array $options
     * @return \Illuminate\Support\HtmlString|mixed
     */
    public function search($name, $value = null, $options = [])
    {
        return $this->input('search', $name, $value, $options);
    }


    /**
     * @param string $name
     * @param null $value
     * @param array $options
     * @return \Illuminate\Support\HtmlString|mixed
     */
    public function email($name, $value = null, $options = [])
    {
        return $this->input('email', $name, $value, $options);
    }


    /**
     * Create a select box field.
     *
     * @param  string $name
     * @param  array $list
     * @param  string|bool $selected
     * @param  array $selectAttributes
     * @param  array $optionsAttributes
     * @param  array $optgroupsAttributes
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function select(
        $name,
        $list = [],
        $selected = null,
        array $selectAttributes = [],
        array $optionsAttributes = [],
        array $optgroupsAttributes = []
    )
    {
        $type = 'select';
        list($selectAttributes, $renderOptions) = $this->getClearedOptions($name, $type, $selectAttributes);
        $jsonFill = false;
        if (isset($selectAttributes['data-fill-url'])) {
            $jsonFill = true;
            if (!empty($selected) && !isset($selectAttributes['data-value'])) {
                if (isset($selectAttributes['multiple'])) {
                    $selectAttributes['data-value'] = json_encode($selected);
                } else {
                    $selectAttributes['data-value'] = $selected;
                }
            }
        }
        $html = parent::select($name, $list, $selected, $selectAttributes, $optionsAttributes, $optgroupsAttributes);
        if ($jsonFill) {
            $renderOptions['after'] = $this->renderByView('loading');
        }
        return $this->render($name, $type, $html, $renderOptions);
    }


    /**
     * @param string $name
     * @param null $value
     * @param array $options
     * @return \Illuminate\Support\HtmlString|mixed
     */
    public function textarea($name, $value = null, $options = [])
    {
        $type = 'textarea';
        list($options, $renderOptions) = $this->getClearedOptions($name, $type, $options);
        $html = parent::textarea($name, $value, $options);
        return $this->render($name, $type, $html, $renderOptions);
    }

    /**
     * Create a select range field.
     *
     * @param  string $name
     * @param  string $begin
     * @param  string $end
     * @param  string $selected
     * @param  array $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function selectRange($name, $begin, $end, $selected = null, $options = [])
    {
        $range = array_combine($range = range($begin, $end), $range);

        return $this->select($name, $range, $selected, $options);
    }


    /**
     * Create a checkbox input field.
     *
     * @param  string $name
     * @param  mixed $value
     * @param  bool $checked
     * @param  array $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function checkbox($name, $value = 1, $checked = null, $options = [])
    {
        $type = 'checkbox';
        list($options, $renderOptions) = $this->getClearedOptions($name, $type, $options);
        if (!isset($options['label'])) {
            $options['label'] = isset($renderOptions['helper'])?$renderOptions['helper']:null;
        }
        if(!isset($options['id'])){
            $options['id']=$name;
        }
        $html = $this->checkable($type, $name, $value, $checked, $options);
        return $this->render($name, $type, $html, $renderOptions);
    }

    /**
     * Create a radio button input field.
     *
     * @param  string $name
     * @param  mixed $value
     * @param  bool $checked
     * @param  array $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function radio($name, $value = null, $checked = null, $options = [])
    {
        $type = 'radio';
        list($options, $renderOptions) = $this->getClearedOptions($name, $type, $options);
        if (!isset($options['label'])) {
            $options['label'] = isset($renderOptions['helper'])?$renderOptions['helper']:null;
        }
        if(!isset($options['id'])){
            $options['id']=$name;
        }
        $html = $this->checkable($type, $name, $value, $checked, $options);
        return $this->render($name, $type, $html, $renderOptions);
    }


    /**
     * Create a checkable input field.
     *
     * @param  string $type
     * @param  string $name
     * @param  mixed $value
     * @param  bool $checked
     * @param  array $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    protected function checkable($type, $name, $value, $checked, $options)
    {
        if (is_array($value)) {
            $data = ['items' => []];
            if (!is_array($checked)) {
                $checkedList = [$checked];
            } else {
                $checkedList = [];
            }
            $i = 0;
            foreach ($value as $key => $item) {
                if (is_array($item)) {
                    if (isset($item['value'])) {
                        $itemValue = $item['value'];
                    } else {
                        $itemValue = $key;
                    }
                    $label = $item['title'];
                } else {
                    $itemValue = $key;
                    $label = $item;
                }
                $itemOptions = $options;
                $itemOptions['label'] = $label;
                $checked = $this->getCheckedState($type, $name, $itemValue, in_array($itemValue, $checkedList));
                if ($checked) {
                    $itemOptions['checked'] = 'checked';
                }
                $id = isset($itemOptions['id']) ? $itemOptions['id'] . ' ' . $itemOptions['id'] . '_' . $i : str_replace('[]', '', $name) . '_' . $i;
                $itemOptions['id'] = $id;
                $data['items'][] = [
                    'html' => parent::input($type, $name, $itemValue, $itemOptions),
                    'label' => $label,
                    'id' => $id,
                ];

                $i = $i + 1;
            }
        } else {
            $checked = $this->getCheckedState($type, $name, $value, $checked);

            if ($checked) {
                $options['checked'] = 'checked';
            }
            $data = [
                'html' => parent::input($type, $name, $value, $options),
                'label' => $options['label'],
                'id' => isset($options['id']) ? $options['id'] : $name,
            ];
        }

        return $this->renderByView($type, $data);
    }


    /*new added functions*/
    /**
     * @param $name
     * @param $value
     * @param $options
     * @return mixed
     */
    public function editor($name, $value, $options)
    {
        $type = 'editor';
        $this->setElementAsset($type);
        list($options, $renderOptions) = $this->getClearedOptions($name, $type, $options);
        $data = [];
        $options['name'] = $name;
        $data['value'] = $value;
        $options['class'] = isset($options['class']) ? $options['class'] . ' ' . 'html-editor' : 'html-editor';
        $options['rows'] = isset($options['rows']) ? $options['rows'] : 6;
        $options['id'] = isset($options['id']) ? $options['id'] : $name;
        $data['attributes'] = $this->html->attributes($options);
        $html = $this->renderByView($type, $data);
        return $this->render($name, $type, $html, $renderOptions);
    }

    /**
     * @param $name
     * @param array $list
     * @param null $selected
     * @param array $selectAttributes
     * @param array $optionsAttributes
     * @param array $optgroupsAttributes
     * @return mixed
     */
    public function selectTag(
        $name,
        $list = [],
        $selected = null,
        array $selectAttributes = [],
        array $optionsAttributes = [],
        array $optgroupsAttributes = []
    )
    {
        $type = 'select';
        list($selectAttributes, $renderOptions) = $this->getClearedOptions($name, $type, $selectAttributes);
        $selectTagClass = $this->renderer->setElementAsset('selectTag')->getElementClass('selectTag');
        if (isset($selectAttributes['data-ajax-url'])) {
            if (is_array($selected)) {
                $selectAttributes['data-ajax-values'] = json_encode($selected);
            } elseif ($selected) {
                $selectAttributes['data-ajax-values'] = json_encode([$selected]);
            }
            $renderOptions['after'] = $this->renderByView('loading');
        }
        $selectAttributes['class'] = isset($selectAttributes['class']) ? $selectAttributes['class'] . ' ' . $selectTagClass : $selectTagClass;
        $html = parent::select($name, $list, $selected, $selectAttributes, $optionsAttributes, $optgroupsAttributes);
        return $this->render($name, $type, $html, $renderOptions);
    }

    /**
     * @param $name
     * @return $this
     */
    public function setTemplate($name)
    {
        $this->templateName = $name;
        return $this;
    }


    /**
     * @param $name
     * @param $options
     * @return \Illuminate\Support\HtmlString
     */
    public function createLabel($name, $options)
    {
        return $this->label($name, $options['title'], ['class' => $this->renderer->getLabelClass(), 'for' => $name]);
    }


    /**
     * @param $name
     * @param $type
     * @param $options
     * @return array
     */
    public function getClearedOptions($name, $type, $options)
    {
        $renderOptions = [];
        if (isset($options['title'])) {
            $renderOptions['label'] = $this->createLabel($name, $options);
            unset($options['title']);
        }
        if (!isset($options['class'])) {
            $options['class'] = $this->renderer->getElementClass($type);
        } else {
            $options['class'] = $this->renderer->getElementClass($type) . ' ' . $options['class'];
        }
        if (isset($options['helper'])) {
            $renderOptions['helper'] = $options['helper'];
            unset($options['helper']);
        }
        if (isset($options['error'])) {
            $renderOptions['error'] = $options['error'];
            unset($options['helper']);
        }
        if (isset($options['parent_class'])) {
            $renderOptions['parent_class'] = $options['parent_class'];
            unset($options['parent_class']);
        }
        return [$options, $renderOptions];
    }

    /**
     * @param null $type
     * @return $this
     */
    public function resetCustomAttributes($type = null)
    {
        $this->renderer->resetCustoms($type);
        return $this;
    }

    /**
     * @param $type
     * @param $name
     * @param $value
     * @return $this
     */
    public function setCustomAttribute($type, $name, $value)
    {
        $this->renderer->setCustomAttribute($type, $name, $value);
        return $this;
    }

    /**
     * @param $type
     * @param $class
     * @return $this
     */
    public function setCustomClass($type, $class)
    {
        $this->renderer->setCustomClass($type, $class);
        return $this;
    }

    /**
     * @param $name
     * @param $type
     * @param $html
     * @param array $renderOptions
     * @return mixed
     */
    public function render($name, $type, $html, $renderOptions = [])
    {
        return $this->renderer->render($name, $type, $html, $renderOptions);
    }

    /**
     * @param $name
     * @param array $data
     * @return mixed
     */
    public function renderByView($name, $data = [])
    {
        return $this->renderer->renderByView($name, $data);
    }


    /**
     * @param $type
     * @return $this
     */
    public function setElementAsset($type)
    {
        $this->renderer->setElementAsset($type);
        return $this;
    }

    /**
     * @return array
     */
    public function getAssets()
    {
        $assets = $this->renderer->getAssets();
        foreach ($this->assets as $item) {
            $assets[] = $item;
        }
        return $assets;
    }


    /**
     * @return $this
     */
    public function setAssetsOnTheme()
    {
        $assets = $this->getAssets();
        if (count($assets)) {
            app('getInstanceObject')->setActionAssets($assets);
        }
        return $this;
    }


    /*action form*/
    public function openAction(array $options = [])
    {
        if (!isset($options['class'])) {
            $options['class'] = 'form-actions';
        }
        return $this->renderByView('open_action', ['attributes' => $this->html->attributes($options)]);
    }

    /**/

    public function closeAction()
    {
        return $this->renderByView('close_action', []);
    }


    public function setErrors(MessageBag $errors){
        $this->renderer->setErrors($errors);
        return $this;
    }

}