<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 7/29/18
 * Time: 1:12 PM
 */

namespace Modules\Core\Providers\Helpers\Form\Template;

use Modules\Core\Providers\Helpers\Form\Template\Master\MasterTemplate;
use Collective\Html\HtmlFacade;

class MaterialTemplate extends MasterTemplate
{

    private $clasess=[
        'element'=>'form-control',
        'label'=>'control-label',
        'checkbox'=>'md-check',
        'radio'=>'md-radio',
        'selectTag'=>'select2',
        'submit'=>'btn btn green _save',
        'cancel'=>'btn default _cancel',
    ];
    private $customAttributes=[];
    private $errorClass='has-error';
    private $assets=[];
    private $exceptionElementClass=['submit','cancel'];
    private $errors=false;

    public function render($name,$type,$html,$renderOptions=[]){
        $renderOptions=$this->getParentAttribute($type,$renderOptions);
        if(isset($renderOptions['parent_class'])){
            unset($renderOptions['parent_class']);
        }
        $helper=false;
        if(isset($renderOptions['helper'])){
            $helper=$renderOptions['helper'];
            unset($renderOptions['helper']);
        }
        $label=false;
        if(isset($renderOptions['label'])){
           $label=$renderOptions['label'];
            unset($renderOptions['label']);
        }
        $hasParent=false;
        $parentOptions=[];
        if($this->hasCustomAttributes($type)){
            $hasParent=true;
            $parentOptions=$this->getCustomAttributes($type);
        }

        $before=false;
        if(isset($renderOptions['before'])){
            $before=$renderOptions['before'];
            unset($renderOptions['before']);
        }

        $after=false;
        if(isset($renderOptions['after'])){
            $after=$renderOptions['after'];
            unset($renderOptions['after']);
        }
        $data=[
            'name'=>$name,
            'before'=>$before,
            'after'=>$after,
            'label'=>$label,
            'helper'=>$helper,
            'error_class'=>$this->getErrorClass(),
            'html'=>$html,'options'=>$renderOptions,
            'hasParent'=>$hasParent,
            'parentOptions'=>$parentOptions
        ];
        if($this->errors){
            $data['errors']=$this->errors;
        }
        return view('core::form.template.material.fields',$data);
    }

    public function renderByView($name,$data=[]){
        if($this->errors){
            $data['errors']=$this->errors;
        }
        return view('core::form.template.material.'.$name,$data);
    }

    public function getElementClass($type){
        if(in_array($type,$this->exceptionElementClass)){
            return isset($this->clasess[$type])?$this->clasess[$type]:'';
        }
        return isset($this->clasess[$type])?$this->clasess['element'].' '.$this->clasess[$type]:$this->clasess['element'];
    }

    public function getLabelClass($type='label'){
        $class='';
        if(isset($this->customAttributes[$type]['class'])){
            $class=$this->customAttributes[$type]['class'].' ';
        }
        return isset($this->clasess[$type])?$class.$this->clasess[$type]:$class;
    }


    public function getParentAttribute($type,$renderOptions){
        $parent_class=isset($renderOptions['parent_class'])?$renderOptions['parent_class']:'';
        switch ($type){
            case 'checkbox':
                $renderOptions['class']=$parent_class.' form-group form-md-checkboxes';
                break;
            case 'radio':
                $renderOptions['class']=$parent_class.' form-group form-md-radios';
                break;
            default:
                $renderOptions['class']=$parent_class.' form-group form-md-line-input';
                break;

        }
        return $renderOptions;
    }


    public function getErrorClass(){
        return $this->errorClass;
    }

    public function setErrorClass($class){
      $this->errorClass=$class;
      return $this;
    }

    public function setCustomClass($type,$class){
        if(!isset($this->customAttributes[$type])){
            $this->customAttributes[$type]=[];
        }
        if(isset($this->customAttributes[$type]['class'])){
            $this->customAttributes[$type]['class']=$this->customAttributes[$type]['class'].' '.$class;
        }else{
            $this->customAttributes[$type]['class']=$class;
        }
        return $this;
    }

    public function setCustomAttribute($type,$name,$value){
        if(!isset($this->customAttributes[$type])){
            $this->customAttributes[$type]=[];
        }
        $this->customAttributes[$type][$name]=$value;
        return $this;
    }


    public function hasCustomAttributes($type){
        return (isset($this->customAttributes[$type]) || isset($this->customAttributes['element']));
    }

    public function getCustomAttributes($type){
        $attributes=isset($this->customAttributes[$type])?$this->customAttributes[$type]:[];
        $elemntAttributes=isset($this->customAttributes['element'])?$this->customAttributes['element']:[];
        return array_merge($elemntAttributes,$attributes);
    }

    public function resetCustoms($type=null){
        if(is_null($type)){
            $this->customAttributes=[];
        }else{
            if(isset($this->customAttributes[$type])){
                unset($this->customAttributes[$type]);
            }
        }
        return $this;
    }


    public function getHelperHtml($html){
      return HtmlFacade::tag('span',(string)$html,['class'=>'help-block']);
    }



    public function setElementAsset($type){
        switch ($type){
            case 'selectTag':
                $this->assets [] = ['container' => 'general_style', 'src' => "assets/global/plugins/select2/css/select2.min.css", 'name' => 'select2'];
                $this->assets [] = ['container' => 'general_style', 'src' => "assets/global/plugins/select2/css/select2-bootstrap.min.css", 'name' => 'select2-bootstrap'];
                $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/select2/js/select2.full.min.js", 'name' => 'select2'];
                break;
            case 'advanced_editor':
                $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/draganddrop/jquery-ui.js", 'name' => 'jquery-ui'];
                $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/draganddrop/jquery-ui.js", 'name' => 'jquery-ui'];
                $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/draganddrop/jquery-ui.js", 'name' => 'jquery-ui'];
                $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/draganddrop/jquery.ui.touch-punch.min.js", 'name' => 'jquery.ui.touch'];
                $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/editor/editor.js", 'name' => 'advanced_editor'];
                break;
            case 'editor':
                $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/ckeditor/ckeditor.js", 'name' => 'ckeditor'];
                $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/ckeditor/config.js", 'name' => 'ckeditor-config'];
                break;
            default:
                break;
        }
        return $this;
    }

    public function setAllAsset(){
        $this->assets=[];
        $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/draganddrop/jquery-ui.js", 'name' => 'jquery-ui'];
        $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/draganddrop/jquery-ui.js", 'name' => 'jquery-ui'];
        $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/draganddrop/jquery-ui.js", 'name' => 'jquery-ui'];
        $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/draganddrop/jquery.ui.touch-punch.min.js", 'name' => 'jquery.ui.touch'];
        $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/editor/editor.js", 'name' => 'advanced_editor'];
        $this->assets [] = ['container' => 'general_style', 'src' => "assets/global/plugins/select2/css/select2.min.css", 'name' => 'select2'];
        $this->assets [] = ['container' => 'general_style', 'src' => "assets/global/plugins/select2/css/select2-bootstrap.min.css", 'name' => 'select2-bootstrap'];
        $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/select2/js/select2.full.min.js", 'name' => 'select2'];
        $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/ckeditor/ckeditor.js", 'name' => 'ckeditor'];
        $this->assets [] = ['container' => 'general_js', 'src' => "assets/global/plugins/ckeditor/config.js", 'name' => 'ckeditor-config'];
    }

    public function getAssets(){
        return $this->assets;
    }

    public function setErrors($errors){
        $this->errors=$errors;
        return $this;
    }


}