<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Core\Providers\Helpers\Form;


use Modules\Core\Models\Language\Language;
use Modules\Core\Models\Block\Block;

Trait ExtraFieldsTrait
{

    private $afterHtml = '';
    private $usedAdvancedEditor = false;

    public function advancedEditor($name, $value, $options = [])
    {
        $type = 'advanced_editor';
        $hasTitle = false;
        if (isset($options['title'])) {
            $hasTitle = true;

        }
        $this->setElementAsset($type);
        list($options, $renderOptions) = $this->getClearedOptions($name, $type, $options);
        $data = [];
        $options['name'] = $name;
        $data['value'] = $value;
        $data['hasTitle'] = $hasTitle;
        $data['name'] = $name;
        $data['positions'] = $options['positions'];
        $data['has_content'] = isset($options['has_content']) ? $options['has_content'] : false;
        unset($options['positions']);
        $data['attributes'] = $this->html->attributes($options);
        $data['blocks'] = Block::getAllBlocksByGroup();
        $html = $this->renderByView($type, $data);
        if (!$this->usedAdvancedEditor) {
            $this->afterHtml = $this->renderByView('advanced.close', $data);
        }
        $this->usedAdvancedEditor = true;
        return $this->render($name, $type, $html, $renderOptions);
    }

    public function avatar($name, $value, $options = [])
    {
        $type = 'file';
        list($options, $renderOptions) = $this->getClearedOptions($name, $type, $options);
        $html = parent::input('file', $name, $value, $options);
        $renderOptions['before'] = $this->renderByView('avatar', ['avatar' => $value]);
        return $this->render($name, $type, $html, $renderOptions);
    }


    public function inputByButton($name, $value, $inputOptions = [], $buttonValue = '', $buttonOptions = [], $isFileInput = false)
    {
        $type = 'input_by_button';
        if (is_null($buttonValue)) {
            $buttonValue = trans('core::form.fields.select_file');
        }
        if ($isFileInput) {
            $fileType = 'all';
            if (isset($inputOptions['file_type'])) {
                $fileType = $inputOptions['file_type'];
                unset($inputOptions['file_type']);
            }
            $fileFolder = '/';
            if (isset($inputOptions['file_folder'])) {
                $fileFolder = $inputOptions['file_folder'];
                unset($inputOptions['file_folder']);
            }
            $inputOptions['id'] = isset($inputOptions['id']) ? $inputOptions['id'] : $name . '_' . rand(1000, 100000);
            $buttonOptions['class'] = isset($buttonOptions['class']) ? $buttonOptions['class'] . ' ' . ' fileServerInput' : 'btn btn-default fileServerInput';
            $buttonOptions['data-input'] = $inputOptions['id'];
            $buttonOptions['data-type'] = $fileType;
            $buttonOptions['data-folder'] = $fileFolder;
            $buttonOptions['data-url'] = url(config('filesystems.file_manager'));
            $buttonPreview = ['data-input' => $inputOptions['id'], 'class' => 'btn btn-success fileServerPreview'];
        }
        list($options, $renderOptions) = $this->getClearedOptions($name, 'text', $inputOptions);
        $inputHtml = parent::input('text', $name, $value, $options);
        $buttonHtml = parent::button($buttonValue, $buttonOptions);
        if ($isFileInput) {
            $buttonHtml .= parent::button(trans('core::form.fields.preview'), $buttonPreview);
        }
        $html = $this->renderByView($type, ['input' => $inputHtml, 'button' => $buttonHtml]);
        return $this->render($name, $type, $html, $renderOptions);
    }


    public function yesno($name, $value, $options)
    {
        $range = [0 => trans('core::data.no'), 1 => trans('core::data.yes')];
        return $this->select($name, $range, $value, $options);
    }


    public function languages($name, $value, $options)
    {
        if (!isset($options['title'])) {
            $options['title'] = trans('core::instance.select_language');
        }
        $values = app('getInstanceObject')->getLanguages(false);
        $values = collect($values)->pluck('name', 'id');
        return $this->select($name, $values, $value, $options);
    }


    public function instances($name, $value, $options)
    {
        if (!isset($options['title'])) {
            $options['title'] = trans('core::instance.select_instance');
        }
        $values = app('getInstanceObject')->getInstances(false);
        $values = collect($values)/*->where('selectable',1)*/
        ->pluck('name', 'id');
        return $this->select($name, $values, $value, $options);
    }

    public function submitOnly(array $submitOptions = [])
    {
        if (!isset($submitOptions['class'])) {
            $submitOptions['class'] = $this->renderer->getElementClass('submit');
        }
        $submit_title = isset($submitOptions['title']) ? $submitOptions['title'] : trans('core::data.submit');
        $html = parent::input('submit', null, $submit_title, $submitOptions);
        return $html;
    }


    public function submitByCancel(array $submitOptions = [], array $cancelOptions = [])
    {
        if (!isset($submitOptions['class'])) {
            $submitOptions['class'] = $this->renderer->getElementClass('submit');
        }
        $submit_title = isset($submitOptions['title']) ? $submitOptions['title'] : trans('core::data.submit');

        $html = $this->buttonLink($cancelOptions);

        $html .= parent::input('submit', null, $submit_title, $submitOptions);
        return $html;
    }

    /**
     * @param array $options
     * @return mixed
     */
    public function buttonLink(array $options = [])
    {
        $cancel_title = isset($options['title']) ? $options['title'] : trans('core::data.cancel');

        if (!isset($options['class'])) {
            $options['class'] = $this->renderer->getElementClass('cancel');
        }

        if (isset($options['url'])) {
            $backUrl = $options['url'];
            unset($options['url']);
        } else {
            $backUrl = url()->previous();
        }
        $html = $this->html->link($backUrl, $cancel_title, $options);
        return $html;
    }

    /**
     * @param array $submitOptions
     * @param array $cancelOptions
     * @param array $languageOptions
     * @return mixed|string
     */
    public function submitByCancelWithLanguage(array $submitOptions = [], array $cancelOptions = [], array $languageOptions = [])
    {
        $html = $this->submitByCancel($submitOptions, $cancelOptions);
        $selected = 0;
        if (isset($languageOptions['selected'])) {
            $selected = $languageOptions['selected'];
            unset($languageOptions['selected']);
        }


        $languageOptions['title'] = isset($languageOptions['title']) ? $languageOptions['title'] : trans('core::language.select_language');
        $languageOptions['placeholder'] = isset($languageOptions['placeholder']) ? $languageOptions['placeholder'] : trans('core::language.select_language');
        $languageOptions['class'] = isset($languageOptions['class']) ? $languageOptions['class'] : "btn form-control input-small";

        $languages = Language::active()->pluck('name', 'id');
        $html .= parent::select('language_id', $languages, (string)$selected, $languageOptions);
        return $html;
    }

    /**
     * @param array $submitOptions
     * @param array $cancelOptions
     * @param array $previewOptions
     * @param array $languageOptions
     * @return mixed|string
     */
    public function submitByCancelAndPreviewWithLanguage(array $submitOptions = [], array $cancelOptions = [], array $previewOptions = [], array $languageOptions = [])
    {
        $html = $this->submitByCancelWithLanguage($submitOptions, $cancelOptions, $languageOptions);

        $previewOptions['title'] = isset($previewOptions['title']) ? $previewOptions['title'] : trans('core::data.preview');
        $previewOptions['url'] = isset($previewOptions['url']) ? $previewOptions['url'] : "#";
        $currentInstance = app("getInstanceObject")->getCurrentInstance();
        $instance = app("getInstanceObject")->getInstance();

        if ($instance->name != $currentInstance->name) {
            $previewOptions['url'] = str_replace($instance->name, $currentInstance->name, $previewOptions['url']);
        }

        $previewOptions['class'] = isset($previewOptions['class']) ? $previewOptions['class'] : "btn primary blue _preview";
        $previewOptions['target'] = isset($previewOptions['target']) ? $previewOptions['target'] : "_blank";

        $html .= $this->buttonLink($previewOptions);
        return $html;
    }


    public function legend($title, $options = [])
    {
        return $this->html->tag('legend', $title . ':', $options);

    }
}