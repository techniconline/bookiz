<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 7/29/18
 * Time: 3:13 PM
 */

namespace Modules\Core\Providers\Helpers\Form\Facades;

use Illuminate\Support\Facades\Facade;

class FormHelper extends Facade
{

    /**
     * get sms service
     * @return string
     */
    protected static function getFacadeAccessor() { return 'FormHelper'; }

}