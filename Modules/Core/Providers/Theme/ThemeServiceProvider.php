<?php namespace Modules\Core\Providers\Theme;

use \Facuz\Theme\ThemeServiceProvider as ThemeServiceProviderOrigin;
use Modules\Core\Libraries\Theme\Theme;

class ThemeServiceProvider extends ThemeServiceProviderOrigin {


	/**
	 * Register theme provider.
	 *
	 * @return void
	 */
	public function registerTheme()
	{
		$this->app->singleton('theme', function($app)
		{
			return new Theme($app['config'], $app['events'], $app['view'], $app['asset'], $app['files'], $app['breadcrumb'], $app['manifest']);
		});

		$this->app->alias('theme', 'Facuz\Theme\Contracts\Theme');
	}

}