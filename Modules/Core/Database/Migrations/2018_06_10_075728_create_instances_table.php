<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInstancesTable extends Migration {

	public function up()
	{
		Schema::create('instances', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 255);
			$table->string('discription', 500)->nullable();
			$table->integer('language_id')->unsigned();
			$table->integer('currency_id')->unsigned();
			$table->integer('frontend_template_id')->unsigned()->nullable();
			$table->integer('backend_template_id')->unsigned()->nullable();
			$table->boolean('selectable')->default(0);
			$table->boolean('default')->index()->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('instances');
	}
}