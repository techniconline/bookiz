<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("root_id")->nullable()->index();
            $table->unsignedInteger("parent_id")->nullable()->index();
            $table->unsignedInteger("instance_id")->nullable()->index();
            $table->string("title")->nullable();
            $table->string("alias")->nullable();
            $table->string("type")->default("url")->comment("url, route, external_url");
            $table->string("url")->nullable();
            $table->string("key_trans")->nullable();
            $table->mediumText("attributes")->nullable();
            $table->unsignedInteger("sort")->default(0);
            $table->tinyInteger("active")->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('menus', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('menus')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menus', function (Blueprint $table) {
            $table->dropForeign('menus_parent_id_foreign');
            $table->dropForeign('menus_instance_id_foreign');
        });
        Schema::dropIfExists('menus');
    }
}
