<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocationsTable extends Migration {

	public function up()
	{
		Schema::create('locations', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 100)->index();
			$table->string('code', 15)->nullable();
			$table->decimal('latitude', 11,8)->nullable()->index();
			$table->decimal('longitude', 11,8)->nullable()->index();
			$table->integer('country_id')->unsigned()->nullable();
			$table->integer('state_id')->unsigned()->nullable();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('locations');
	}
}