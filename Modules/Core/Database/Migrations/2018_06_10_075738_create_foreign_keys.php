<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('locations', function(Blueprint $table) {
			$table->foreign('country_id')->references('id')->on('locations')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
        Schema::table('currencies', function(Blueprint $table) {
            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
		Schema::table('locations', function(Blueprint $table) {
			$table->foreign('state_id')->references('id')->on('locations')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('instances', function(Blueprint $table) {
			$table->foreign('language_id')->references('id')->on('languages')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('instances', function(Blueprint $table) {
			$table->foreign('currency_id')->references('id')->on('currencies')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('instances', function(Blueprint $table) {
			$table->foreign('frontend_template_id')->references('id')->on('templates')
						->onDelete('set null')
						->onUpdate('set null');
		});
		Schema::table('instances', function(Blueprint $table) {
			$table->foreign('backend_template_id')->references('id')->on('templates')
						->onDelete('set null')
						->onUpdate('set null');
		});
		Schema::table('blocks', function(Blueprint $table) {
			$table->foreign('module_id')->references('id')->on('modules')
						->onDelete('cascade')
						->onUpdate('cascade');
		});

		Schema::table('instance_configs', function(Blueprint $table) {
			$table->foreign('instance_id')->references('id')->on('instances')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
        Schema::table('instance_configs', function(Blueprint $table) {
            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
		Schema::table('instance_configs', function(Blueprint $table) {
			$table->foreign('module_id')->references('id')->on('modules')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('locations', function(Blueprint $table) {
			$table->dropForeign('locations_country_id_foreign');
		});
		Schema::table('locations', function(Blueprint $table) {
			$table->dropForeign('locations_state_id_foreign');
		});
		Schema::table('instances', function(Blueprint $table) {
			$table->dropForeign('instances_language_id_foreign');
		});
		Schema::table('instances', function(Blueprint $table) {
			$table->dropForeign('instances_currency_id_foreign');
		});
		Schema::table('instances', function(Blueprint $table) {
			$table->dropForeign('instances_frontend_template_id_foreign');
		});
		Schema::table('instances', function(Blueprint $table) {
			$table->dropForeign('instances_backend_template_id_foreign');
		});
		Schema::table('blocks', function(Blueprint $table) {
			$table->dropForeign('blocks_module_id_foreign');
		});
		Schema::table('pages', function(Blueprint $table) {
			$table->dropForeign('pages_instance_id_foreign');
		});
		Schema::table('page_configs', function(Blueprint $table) {
			$table->dropForeign('page_configs_page_id_foreign');
		});
		Schema::table('page_configs', function(Blueprint $table) {
			$table->dropForeign('page_configs_block_id_foreign');
		});
		Schema::table('instance_configs', function(Blueprint $table) {
			$table->dropForeign('instance_configs_instance_id_foreign');
		});
		Schema::table('instance_configs', function(Blueprint $table) {
			$table->dropForeign('instance_configs_module_id_foreign');
		});
	}
}