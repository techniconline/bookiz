<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInstanceConfigsTable extends Migration {

	public function up()
	{
		Schema::create('instance_configs', function(Blueprint $table) {
			$table->increments('id');
            $table->string('name', 50)->index();
			$table->integer('instance_id')->unsigned();
			$table->integer('language_id')->unsigned();
			$table->integer('module_id')->unsigned();
			$table->mediumText('configs');
			$table->timestamps();
			$table->unique(['name','instance_id','module_id']);
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('instance_configs');
	}
}