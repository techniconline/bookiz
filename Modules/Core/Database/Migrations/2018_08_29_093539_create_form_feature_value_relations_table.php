<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormFeatureValueRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('form_records', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('language_id');
            $table->string('system')->nullable();
            $table->unsignedInteger('instance_id');
            $table->unsignedInteger('form_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('form_feature_value_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('form_record_id');
            $table->unsignedInteger('item_id')->nullable();
            $table->unsignedInteger('feature_id');
            $table->unsignedInteger('feature_value_id')->nullable()->comment('fill by value feature type is not custom, select box, ... ');
            $table->unsignedInteger('feature_value')->nullable()->comment('fill by value feature type is custom. text , textarea ...');
            $table->tinyInteger('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('form_records', function (Blueprint $table) {

            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('form_id')->references('id')->on('forms')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

        Schema::table('form_feature_value_relations', function (Blueprint $table) {

            $table->foreign('form_record_id')->references('id')->on('form_records')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('feature_id')->references('id')->on('features')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('feature_value_id')->references('id')->on('feature_values')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('form_records', function (Blueprint $table) {
            $table->dropForeign('form_feature_value_relations_form_id_foreign');
            $table->dropForeign('form_feature_value_relations_language_id_foreign');
            $table->dropForeign('form_feature_value_relations_language_id_foreign');
        });

        Schema::table('form_feature_value_relations', function (Blueprint $table) {
            $table->dropForeign('form_feature_value_relations_form_record_id_foreign');
            $table->dropForeign('form_feature_value_relations_feature_id_foreign');
            $table->dropForeign('form_feature_value_relations_feature_value_id_foreign');
        });

        Schema::dropIfExists('form_records');
        Schema::dropIfExists('form_feature_value_relations');
    }
}
