<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeInstanceRouteBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('instance_route_blocks', function (Blueprint $table) {
            $table->boolean("is_api")->nullable()->after('json');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instance_route_blocks', function (Blueprint $table) {
            $table->dropColumn("is_api");
        });
    }
}
