<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetaGroupFeatureGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_group_feature_groups', function (Blueprint $table) {

            $table->unsignedInteger('meta_group_id');
            $table->unsignedInteger('feature_group_id');

        });

        Schema::table('meta_group_feature_groups', function (Blueprint $table) {
            $table->foreign('meta_group_id')->references('id')->on('meta_groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('feature_group_id')->references('id')->on('feature_groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('meta_group_feature_groups', function (Blueprint $table) {
            $table->dropForeign('meta_group_feature_groups_meta_group_id_foreign');
            $table->dropForeign('meta_group_feature_groups_feature_group_id_foreign');
        });

        Schema::dropIfExists('meta_group_feature_groups');
    }
}
