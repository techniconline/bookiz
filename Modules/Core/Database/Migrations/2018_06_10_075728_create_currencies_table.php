<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCurrenciesTable extends Migration {

	public function up()
	{
		Schema::create('currencies', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 255);
            $table->integer('language_id')->unsigned();
            $table->string('code', 4)->unique();
			$table->string('symbol', 15)->index();
			$table->enum('direction', array('LTR', 'RTL'))->index();
			$table->boolean('active')->default(0);
			$table->softDeletes();
		});
	}

	public function down()
	{

		Schema::drop('currencies');
	}
}