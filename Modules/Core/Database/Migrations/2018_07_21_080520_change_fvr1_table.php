<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFvr1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('feature_value_relations', function (Blueprint $table) {
            $table->string('feature_value')->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('feature_value_relations', function (Blueprint $table) {
            $table->unsignedInteger('feature_value')->change();
        });


    }
}
