<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCurrencyRatesTable extends Migration {

	public function up()
	{
		Schema::create('currency_rates', function(Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('from_currency_id');
			$table->unsignedInteger('to_currency_id');
			$table->float('rate', 10,2)->index();
            $table->foreign('from_currency_id')->references('id')->on('currencies')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('to_currency_id')->references('id')->on('currencies')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unique(['from_currency_id','to_currency_id']);
		});
	}

	public function down()
	{
        Schema::table('currency_rates', function(Blueprint $table) {
            $table->dropForeign('currency_rates_from_currency_id_foreign');
            $table->dropForeign('currency_rates_to_currency_id_foreign');
        });
		Schema::drop('currency_rates');
	}
}