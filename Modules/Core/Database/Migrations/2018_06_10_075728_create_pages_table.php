<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesTable extends Migration {

	public function up()
	{
		Schema::create('pages', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('instance_id')->unsigned();
			$table->integer('language_id')->unsigned();
			$table->string('title', 255);
			$table->string('slag', 255);
			$table->enum('column', array('3Column', '2ColumnRight','2ColumnLeft', '1Column','EmptyColumn','layout'))->default('layout')->index();
			$table->text('description')->nullable();
			$table->longText('content')->nullable();
            $table->enum('status', array('publish', 'saved','draft'))->default('draft')->index();
            $table->unique(['instance_id','language_id','slag']);
            $table->timestamps();
			$table->softDeletes();
		});
        Schema::table('pages', function (Blueprint $table) {
            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
	}

	public function down()
	{
        Schema::table('pages', function (Blueprint $table) {
            $table->dropForeign('pages_instance_id_foreign');
            $table->dropForeign('pages_language_id_foreign');
        });
		Schema::drop('pages');
	}
}