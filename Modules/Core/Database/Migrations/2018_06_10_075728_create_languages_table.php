<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLanguagesTable extends Migration {

	public function up()
	{
		Schema::create('languages', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 255);
			$table->string('code', 4)->unique();
			$table->enum('direction', array('LTR', 'RTL'))->index();
			$table->boolean('active')->default(0);
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('languages');
	}
}