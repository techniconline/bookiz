<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureValueRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_value_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('language_id');
            $table->unsignedInteger('feature_id');
            $table->unsignedInteger('feature_value_id')->nullable()->comment('fill by value feature type is not custom, select box, ... ');
            $table->unsignedInteger('feature_value')->nullable()->comment('fill by value feature type is custom. text , textarea ...');
            $table->tinyInteger('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });


        Schema::table('feature_value_relations', function (Blueprint $table) {

            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('feature_id')->references('id')->on('features')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('feature_value_id')->references('id')->on('feature_values')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('feature_value_relations', function (Blueprint $table) {
            $table->dropForeign('feature_value_relations_language_id_foreign');
            $table->dropForeign('feature_value_relations_feature_id_foreign');
            $table->dropForeign('feature_value_relations_feature_value_id_foreign');
        });

        Schema::dropIfExists('feature_value_relations');
    }
}
