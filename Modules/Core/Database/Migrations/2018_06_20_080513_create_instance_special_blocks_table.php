<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstanceSpecialBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instance_special_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("instance_id")->index();
            $table->string("name");
            $table->longText("json")->nullable();
            $table->tinyInteger("active")->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('instance_special_blocks', function (Blueprint $table) {
            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instance_special_blocks', function (Blueprint $table) {
            $table->dropForeign('instance_special_blocks_instance_id_foreign');
        });
        Schema::dropIfExists('instance_special_blocks');
    }
}
