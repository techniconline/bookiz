<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeInstanceSpecialBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('instance_special_blocks', function (Blueprint $table) {
            $table->unsignedInteger("language_id")->nullable()->after('instance_id');
        });

        Schema::table('instance_special_blocks', function (Blueprint $table) {
            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instance_special_blocks', function (Blueprint $table) {
            $table->dropForeign('instance_special_blocks_language_id_foreign');
        });
        Schema::table('instance_special_blocks', function (Blueprint $table) {
            $table->dropColumn("language_id");
        });
    }
}
