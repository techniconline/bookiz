<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeVideoMetaGroupFeatureGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta_group_feature_groups', function (Blueprint $table) {
            $table->tinyInteger('position')->after('feature_group_id')->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_group_feature_groups', function (Blueprint $table) {
            $table->dropColumn('position');
        });
    }
}
