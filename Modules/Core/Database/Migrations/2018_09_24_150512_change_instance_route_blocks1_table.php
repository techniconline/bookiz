<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeInstanceRouteBlocks1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('instance_route_blocks', function (Blueprint $table) {
            $table->dropUnique('instance_route_blocks_instance_id_language_id_route_name_unique');
            $table->unique(['instance_id','language_id','route_name','is_api'],'instance_route_blocks_unique_fields');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
