<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('type')->default('text')->nullable()->comment('type of html inputs');
            $table->unsignedInteger('language_id')->nullable();
            $table->unsignedInteger('parent_id')->nullable();
            $table->text('attributes')->nullable();
            $table->boolean('custom')->default(1);
            $table->tinyInteger('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('features', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('features')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('features', function (Blueprint $table) {
            $table->dropForeign('features_parent_id_foreign');
            $table->dropForeign('features_language_id_foreign');
        });

        Schema::dropIfExists('features');
    }
}
