<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFgrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('feature_group_relations', function (Blueprint $table) {
            $table->unsignedTinyInteger('position')->default(0)->after('feature_group_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('feature_group_relations', function (Blueprint $table) {
            $table->dropColumn(['position']);
        });


    }
}
