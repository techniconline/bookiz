<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetaGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('language_id')->nullable();
            $table->string('title')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('meta_groups', function (Blueprint $table) {
            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_groups', function (Blueprint $table) {
            $table->dropForeign('meta_groups_language_id_foreign');
        });

        Schema::dropIfExists('meta_groups');
    }
}
