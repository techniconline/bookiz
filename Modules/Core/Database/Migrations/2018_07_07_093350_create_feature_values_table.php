<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_values', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->unsignedInteger('feature_id');
            $table->unsignedInteger('language_id');
            $table->string('value')->nullabel()->index();
            $table->string('title')->nullable();
            $table->tinyInteger('active')->default(1);

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('feature_values', function (Blueprint $table) {

            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('feature_id')->references('id')->on('features')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('parent_id')->references('id')->on('feature_values')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feature_values', function (Blueprint $table) {
            $table->dropForeign('feature_values_language_id_foreign');
            $table->dropForeign('feature_values_feature_id_foreign');
            $table->dropForeign('feature_values_parent_id_foreign');
        });

        Schema::dropIfExists('feature_values');
    }
}
