<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureGroupRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_group_relations', function (Blueprint $table) {
            $table->unsignedInteger('feature_id');
            $table->unsignedInteger('feature_group_id');
        });


        Schema::table('feature_group_relations', function (Blueprint $table) {

            $table->foreign('feature_id')->references('id')->on('features')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('feature_group_id')->references('id')->on('feature_groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feature_group_relations', function (Blueprint $table) {
            $table->dropForeign('feature_group_relations_feature_id_foreign');
            $table->dropForeign('feature_group_relations_feature_group_id_foreign');
            $table->dropForeign('feature_group_relations_instance_id_foreign');
        });

        Schema::dropIfExists('feature_group_relations');
    }
}
