<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFvrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('feature_value_relations', function (Blueprint $table) {
            $table->string('system')->nullable()->after('language_id');
            $table->unsignedInteger('item_id')->nullable()->after('system');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('feature_value_relations', function (Blueprint $table) {
            $table->dropColumn(['item_id','system']);
        });


    }
}
