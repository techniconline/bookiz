<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFfvrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_feature_value_relations', function (Blueprint $table) {
            $table->longText('feature_value')->nullable()->change();
        });

        Schema::table('feature_value_relations', function (Blueprint $table) {
            $table->longText('feature_value')->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feature_value_relations', function (Blueprint $table) {
            $table->string('feature_value')->nullable()->change();
        });
        Schema::table('form_feature_value_relations', function (Blueprint $table) {
            $table->string('feature_value')->nullable()->change();
        });
    }
}
