<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLocations1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('locations', function (Blueprint $table) {
            $table->unsignedInteger('language_id')->nullable()->after('code');
        });

        Schema::table('locations', function (Blueprint $table) {
            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('locations', function (Blueprint $table) {
            $table->dropForeign('locations_language_id_foreign');
        });

        Schema::table('locations', function (Blueprint $table) {
            $table->dropColumn('language_id');
        });

    }
}
