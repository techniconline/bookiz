<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstanceRouteBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instance_route_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("instance_id")->index();
            $table->unsignedInteger("language_id")->index();
            $table->string("route_name");
            $table->enum('column', array('3Column', '2ColumnRight','2ColumnLeft', '1Column','EmptyColumn','layout'))->default('layout')->index();
            $table->longText("json")->nullable();
            $table->tinyInteger("active")->default(1);
            $table->unique(['instance_id','language_id','route_name']);
            $table->softDeletes();
            $table->timestamps();
        });


        Schema::table('instance_route_blocks', function (Blueprint $table) {
            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instance_route_blocks', function (Blueprint $table) {
            $table->dropForeign('instance_route_blocks_instance_id_foreign');
            $table->dropForeign('instance_route_blocks_language_id_foreign');
        });
        Schema::dropIfExists('instance_route_blocks');
    }
}
