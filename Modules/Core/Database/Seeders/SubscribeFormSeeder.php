<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureGroup;
use Modules\Core\Models\Feature\FeatureGroupRelation;
use Modules\Core\Models\Form;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;
use Modules\Core\Models\Meta\MetaGroup;
use Modules\Core\Models\Meta\MetaGroupFeatureGroup;

class SubscribeFormSeeder extends Seeder
{

    /**
     * @throws \Throwable
     */
    public function run()
    {
        $feature = new Feature();
        $language = Language::where("code", "fa")->first();
        $language_id = $language ? $language->id : null;

        $feature_ids = $items = [];
        $items[] = [
            "title" => "نام و نام خانوادگی",
            "type" => "text",
            "language_id" => $language_id,
            "custom" => 1,
        ];

        $items[] = [
            "title" => "پست الکترونیک",
            "type" => "text",
            "language_id" => $language_id,
            "custom" => 1,
        ];

        foreach ($items as $item) {

            $f = Feature::active()->where("title", $item["title"])->first();
            if($f){
                $feature_ids[] = $f->id;
                continue;
            }

            $feature->fill($item);
            if ($feature->save()) {
                $feature_ids[] = $feature->id;
            }
            $feature = new Feature();
        }

        // insert GROUP
        $instances = Instance::all();
        $fgIds = [];
        foreach ($instances as $instance) {
            $featureGroup = new FeatureGroup();
            $items = [
                "title" => "فرم اشتراک در خبر نامه",
                "instance_id" => $instance->id,
                "language_id" => $instance->language_id,
            ];
            $featureGroup->fill($items);
            if ($featureGroup->save()) {
                $fgIds[] = $feature_group_id = $featureGroup->id;
                foreach ($feature_ids as $index => $feature_id) {
                    $featureGroupRelation = new FeatureGroupRelation();
                    $itemsFGR = ["feature_id" => $feature_id, "feature_group_id" => $feature_group_id, "position" => $index];
                    $featureGroupRelation->insert($itemsFGR);
                }
            }
        }

        //insert Meta Group
        $metaGroup = new MetaGroup();
        $items = [
            "title"=>"فرم اشتراک در خبر نامه",
            "language_id"=>$language_id,
            "system"=>"form_builder",
        ];
        $metaGroup->fill($items);
        $meta_group_id = null;
        if($metaGroup->save()){
            $meta_group_id = $metaGroup->id;
        }

        //insert meta_group_feature_groups
        foreach ($fgIds as $fgId) {
            $mgfgModel = new MetaGroupFeatureGroup();
            $items = [
                "meta_group_id"=>$meta_group_id,
                "feature_group_id"=>$fgId,
                "position"=>0,
            ];
            $mgfgModel->insert($items);
        }
        //------------------------

        //insert form
        $formModel = new Form();
        $items = [
            "title"=>"فرم اشتراک در خبر نامه",
            "active"=>1,
        ];
        $formModel->fill($items);
        $form_id = null;
        if($formModel->save()){
            $form_id = $formModel->id;
        }

        if($form_id && $meta_group_id){
            $formMetaGroup = new Form\FormMetaGroup();
            $items = [
                "form_id"=>$form_id,
                "meta_group_id"=>$meta_group_id,
            ];
            $formMetaGroup->fill($items);
            $formMetaGroup->save();
        }




    }
}
