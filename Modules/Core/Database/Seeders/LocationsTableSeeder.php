<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class LocationsTableSeeder extends Seeder
{

    public function run()
    {
        Model::unguard();

        $path = base_path("Modules/Core/Database/Seeders/locations.sql");
        DB::unprepared(file_get_contents($path));
        $this->command->info('Country table seeded!');
    }
}