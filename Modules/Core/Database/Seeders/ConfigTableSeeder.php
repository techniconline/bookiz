<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;

use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Instance\InstanceConfigs;
use Modules\Core\Models\Module\Module;


class ConfigTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('currencies')->delete();

        $instance=Instance::where('name','admin')->first();

        $module_id=Module::where('name','core')->first()->id;

        $InstanceConfigs=new InstanceConfigs();
        $InstanceConfigs->fill(array(
            'name'=>'instance',
            'instance_id'=>$instance->id,
            'language_id'=>$instance->language_id,
            'module_id'=>$module_id,
            'configs'=>array (
                'title' => 'پنل مدیریت',
                'prefix_title' => NULL,
                'postfix_title' => NULL,
                'description' => NULL,
                'telegram' => NULL,
                'instagram' => NULL,
                'google_plus' => NULL,
                'twitter' => NULL,
                'facebook' => NULL,
                'linkedin' => NULL,
                'aparat' => NULL,
                'soroush' => NULL,
                'rubika' => NULL,
                'home' => 'core.dashboard.index',
                'favicon' => '/storage/admin/settings/core/instance/favicon.png',
                'logo' => '/storage/admin/settings/core/instance/logo.png',
                'logo_small' => '/storage/admin/settings/core/instance/logo_small.png',
                'logo_large' => '/storage/admin/settings/core/instance/logo_large.png',
            ),
        ))->save();

        $instance=Instance::where('name','www')->first();

        $InstanceConfigs=new InstanceConfigs();
        $InstanceConfigs->fill(array(
            'name'=>'instance',
            'instance_id'=>$instance->id,
            'language_id'=>$instance->language_id,
            'module_id'=>$module_id,
            'configs'=>array (
                'title' => 'فرایاد',
                'prefix_title' => NULL,
                'postfix_title' => NULL,
                'description' => NULL,
                'telegram' => NULL,
                'instagram' => NULL,
                'google_plus' => NULL,
                'twitter' => NULL,
                'facebook' => NULL,
                'linkedin' => NULL,
                'aparat' => NULL,
                'soroush' => NULL,
                'rubika' => NULL,
                'home' => 'core.dashboard.index',
                'favicon' => '/storage/admin/settings/core/instance/favicon.png',
                'logo' => '/storage/admin/settings/core/instance/logo.png',
                'logo_small' => '/storage/admin/settings/core/instance/logo_small.png',
                'logo_large' => '/storage/admin/settings/core/instance/logo_large.png',
            ),
        ))->save();

	}
}