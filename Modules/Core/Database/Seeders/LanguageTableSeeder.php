<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Language\Language;

class LanguageTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('languages')->delete();

		// LanguagesSeeder
        $language=new Language();
		$language->fill(array(
				'name' => 'فارسی',
				'code' => 'fa',
				'direction' => 'RTL',
				'active' => 1
			))->save();

		// LanguagesSeeder
        $language=new Language();
        $language->fill(array(
				'name' => 'English',
				'code' => 'en',
				'direction' => 'LTR',
                'active' => 0
        ))->save();
	}
}