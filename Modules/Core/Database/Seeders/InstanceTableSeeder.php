<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;
use Modules\Core\Models\Currency\Currency;
use Modules\Core\Models\Template\Template;


class InstanceTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('currencies')->delete();

        $language_id=Language::where('code','fa')->first()->id;
        $currency_id=Currency::where('code','IRT')->first()->id;
        $frontend_template_id=Template::where('code','default')->first()->id;
        $backend_template_id=Template::where('code','admin')->first()->id;

		// CurrenciesSeeder
        $instance = new Instance();
        $instance->fill(array(
				'name' => 'www',
				'description' => 'Farayad',
				'language_id' => $language_id,
				'currency_id' => $currency_id,
				'frontend_template_id' => $frontend_template_id,
				'backend_template_id' => $backend_template_id,
				'selectable' => 1,
				'default' => 1,
			))->save();

        $instance = new Instance();
        $instance->fill(array(
            'name' => 'admin',
            'description' => 'Farayad Admin',
            'language_id' => $language_id,
            'currency_id' => $currency_id,
            'frontend_template_id' => $backend_template_id,
            'backend_template_id' => $backend_template_id,
            'selectable' => 1,
            'default' =>0,
        ))->save();

        $instance = new Instance();
        $instance->fill(array(
            'name' => 'api',
            'description' => 'Farayad Api',
            'language_id' => $language_id,
            'currency_id' => $currency_id,
            'frontend_template_id' => $frontend_template_id,
            'backend_template_id' => $backend_template_id,
            'selectable' => 0,
            'default' => 0,
        ))->save();

	}
}