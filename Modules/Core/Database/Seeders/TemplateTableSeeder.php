<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Template\Template;

class TemplateTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('currencies')->delete();

		// CurrenciesSeeder
        $template=new Template();
        $template->fill(array(
				'name' => 'Admin Panel',
				'code' => 'admin',
				'backend' => 1,
				'default' => 1,
			))->save();

        $template=new Template();
        $template->fill(array(
            'name' => 'Default Theme',
            'code' => 'default',
            'backend' => 0,
            'default' => 1,
        ))->save();

	}
}