<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CoreDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Model::unguard();

        $this->call('Modules\Core\Database\Seeders\ModuleTableSeeder');
        $this->command->info('Module table seeded!');

        $this->call('Modules\Core\Database\Seeders\LanguageTableSeeder');
        $this->command->info('Language table seeded!');

        $this->call('Modules\Core\Database\Seeders\CurrencyTableSeeder');
        $this->command->info('Currency table seeded!');


        $this->call('Modules\Core\Database\Seeders\TemplateTableSeeder');
        $this->command->info('Template table seeded!');

        $this->call('Modules\Core\Database\Seeders\InstanceTableSeeder');
        $this->command->info('Instance table seeded!');

        $this->call('Modules\Core\Database\Seeders\BlockTableSeeder');
        $this->command->info('Blocks table seeded!');

        $this->call('Modules\Core\Database\Seeders\ConfigTableSeeder');
        $this->command->info('Config table seeded!');

        $this->call('Modules\Core\Database\Seeders\TokenTableSeeder');
        $this->command->info('Token table seeded!');

        $this->call('Modules\Core\Database\Seeders\LocationsTableSeeder');
        $this->command->info('Locations table seeded!');

        $this->call('Modules\Core\Database\Seeders\ContactUsFormSeeder');
        $this->command->info('Form ContactUs seeded!');

        $this->call('Modules\Core\Database\Seeders\SubscribeFormSeeder');
        $this->command->info('Form Subscribe seeded!');


    }
}
