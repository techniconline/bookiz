<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Currency\Currency;
use Modules\Core\Models\Language\Language;

class CurrencyTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('currencies')->delete();
        $language_id=Language::where('code','fa')->first()->id;
		// CurrenciesSeeder
        $currency=new Currency();
		$currency->fill(array(
				'name' => 'ریال',
				'code' => 'IRR',
				'symbol' => 'ریال',
				'language_id'=>$language_id,
				'direction' => 'RTL',
				'active' => 1
			))->save();
		// CurrenciesSeeder
        $currency=new Currency();
        $currency->fill(array(
				'name' => 'تومان',
				'code' => 'IRT',
				'symbol' => 'تومان',
                'language_id'=>$language_id,
                'direction' => 'RTL',
				'active' => 1
			))->save();

        /**/

	}
}