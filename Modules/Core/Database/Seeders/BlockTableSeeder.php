<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Block\Block;
use Modules\Core\Models\Module\Module;


class BlockTableSeeder extends Seeder
{

    public function run()
    {
        //DB::table('currencies')->delete();

        $module_id = Module::where('name', 'core')->first()->id;

        if (!Block::where("module_id", $module_id)->where("name", "dashboard_counter_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'dashboard_counter_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\DashboardCounterBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "language_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'language_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\LanguageBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "logo_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'logo_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\LogoBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "instance_change_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'instance_change_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\InstanceChangeBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "language_change_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'language_change_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\LanguageChangeBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "html_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'html_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\HtmlBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "html_open_close_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'html_open_close_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\HtmlOpenCloseBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "user_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'user_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\UserBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "search_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'search_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\SearchBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "js_css_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'js_css_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\JsCssBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "partners_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'partners_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\PartnersBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "information_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'information_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\InformationBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "counter_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'counter_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\CounterBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "form_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'form_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\FormBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "slider_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'slider_block',
                'namespace' => 'Modules\Core\ViewModels\Blocks\SliderBlock',
                'active' => 1,
            ))->save();
        }


    }
}