<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;

use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Instance\TokenInstance;



class TokenTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('currencies')->delete();

        $instance=Instance::where('name','www')->first();

        $tokenInstance= new TokenInstance();
        $tokenInstance->fill(array(
                'instance_id'=>$instance->id,
				'token' => sha1(time().rand(1000,10000000)),
				'system' => 'api',
				'active' => 1,
			))->save();

	}
}