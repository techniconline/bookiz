<?php

namespace Modules\Core\Models\Language;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Language\Language as TraitModel;


class Language extends BaseModel
{

    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['name', 'code', 'direction', 'active', 'deleted_at'];
    public $timestamps = false;

    use SoftDeletes;

    public $rules = [
        'code' => 'required|unique:languages',
        'name' => 'required',
        'direction' => 'required',
    ];

    public $api_fields = ['name', 'code', 'direction'];
    public $list_fields = ['name', 'code'];

    protected $dates = ['deleted_at'];
    public $key_cache_all = 'all_languages';
    protected $softDelete = true;

}
