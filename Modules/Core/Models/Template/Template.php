<?php

namespace Modules\Core\Models\Template;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Template\Template as TraitModel;


class Template extends BaseModel
{

    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['name', 'code', 'version', 'backend', 'renderer', 'default', 'created_at', 'updated_at', 'deleted_at'];

    public $rules = [
        'code' => 'required|unique:templates',
        'name' => 'required',
    ];

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $softDelete = true;

}
