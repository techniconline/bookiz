<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\BaseTrait;

class BaseModel extends Model
{
    use BaseTrait;

    public $errors = [];
    public $rules = [];
    public $messages = [];
    public $key_cache_all;
    protected $duration_cache_all;
    public $key_cache_single;
    protected $duration_cache_single;
    protected $remove_cache = false;
    protected $create_cache = false;
    protected $convertCreatedAtToJalali = false;
    protected $convertUpdatedAtToJalali = false;
    protected $requestItems = [];
    protected $api_fields = [];
    protected $list_fields = [];
    protected $api_append_fields = [];
    protected $list_append_fields = [];
    protected $request;

    public function __construct()
    {
//        $this->request = request();
//        $this->setRequestItems(request()->all($this->getFillable()));
        $this->setRules();
//        $this->setApiAttributes();
    }


}
