<?php

namespace Modules\Core\Models\Module;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Module\Module as TraitModel;


class Module extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['name', 'version'];

    use SoftDeletes;

    public $key_cache_all = 'all_modules';
    public $create_cache=true;

    protected $dates = ['deleted_at'];

}
