<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Form extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'params', 'active', 'deleted_at', 'created_at', 'updated_at'];
    public $messages = [];
    public $rules = [
        'title' => 'required',
    ];

    use SoftDeletes;

    const SYSTEM_NAME = 'form_builder';
    protected $dates = ['deleted_at'];

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'title.required' => trans('core::errors.form.title_required'),
        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formMetaGroups()
    {
        return $this->hasMany('Modules\Core\Models\Form\FormMetaGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formRecords()
    {
        return $this->hasMany('Modules\Core\Models\Form\FormRecord');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function metaGroups()
    {
        return $this->belongsToMany('Modules\Core\Models\Meta\MetaGroup', 'form_meta_groups');
    }

}
