<?php

namespace Modules\Core\Models\Meta;

use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Meta\MetaGroupFeatureGroup as TraitModel;

class MetaGroupFeatureGroup extends BaseModel
{
    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['meta_group_id', 'feature_group_id', 'position'];
    public $messages = [];
    public $timestamps=false;
    public $rules = [
        'meta_group_id' => 'required',
        'feature_group_id' => 'required',
    ];

}
