<?php

namespace Modules\Core\Models\Meta;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Meta\MetaGroup as TraitModel;

class MetaGroup extends BaseModel
{
    use SoftDeletes;
    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['language_id', 'title', 'system', 'default', 'active', 'deleted_at', 'created_at', 'updated_at'];
    public $messages = [];
    public $rules = [
        'language_id' => 'required',
        'title' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public $anotherModuleType = [
        'form_builder' => ["path" => "", "name" => "form_builder"]
    ];

}
