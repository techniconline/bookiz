<?php

namespace Modules\Core\Models\Instance;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Instance\TokenInstance as TraitModel;

class TokenInstance extends BaseModel
{
    use SoftDeletes;
    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'token', 'system', 'active', 'deleted_at', 'created_at', 'updated_at'];
    public $messages = [];

    public $rules = [
        'token' => 'required|unique:token_instances',
        'instance_id' => 'required',
        'system' => 'required',
    ];

    protected $dates = ['deleted_at'];

}
