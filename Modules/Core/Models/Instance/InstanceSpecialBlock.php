<?php

namespace Modules\Core\Models\Instance;

use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Instance\InstanceSpecialBlock as TraitModel;


class InstanceSpecialBlock extends BaseModel
{
    use TraitModel;

    public $key_cache_all = "key_all_isb";
    public $instance_special_block_cache_key = "isb_";
    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'name', 'json', 'active', 'deleted_at', 'created_at', 'updated_at'];




}
