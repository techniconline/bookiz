<?php

namespace Modules\Core\Models\Instance;

use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Instance\InstanceRouteBlock as TraitModel;


class InstanceRouteBlock extends BaseModel
{
    use TraitModel;

    public $instance_route_block_cache_key = "instance_route_block_widgets";

    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'route_name', 'json','is_api', 'active', 'deleted_at', 'created_at', 'updated_at'];

}
