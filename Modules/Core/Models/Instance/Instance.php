<?php

namespace Modules\Core\Models\Instance;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Instance\Instance as TraitModel;

class Instance extends BaseModel
{
    use TraitModel;

    protected $table = 'instances';
    public $timestamps = true;

    protected $fillable = ['name','app_name', 'description', 'language_id', 'currency_id', 'frontend_template_id', 'backend_template_id', 'default', 'deleted_at'];

    public $api_fields = ['id', 'name','app_name', 'description', 'language_id', 'currency_id'];
    public $list_fields = ['id', 'name','app_name', 'description', 'language_id', 'currency_id'];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [
        'name' => 'required|unique:instances',
        'language_id' => 'required',
        'currency_id' => 'required',
    ];

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    public $key_cache_all = 'all_instances';

}