<?php

namespace Modules\Core\Models\Instance;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;

class InstanceConfigs extends BaseModel
{

    protected $table = 'instance_configs';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function getConfigsAttribute()
    {
        if(!isset($this->attributes['configs']) || !$this->attributes['configs']){
            return null;
        }
        return json_decode( $this->attributes['configs']);
    }

    public function setConfigsAttribute( $val )
    {
        if(!is_array($val) && !is_object($val)){
            $this->attributes['configs'] =$val;
        }else{
            $this->attributes['configs'] = json_encode( $val );
        }
    }

}