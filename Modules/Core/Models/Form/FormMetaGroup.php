<?php

namespace Modules\Core\Models\Form;

use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Form\FormMetaGroup as TraitModel;

class FormMetaGroup extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['form_id', 'meta_group_id'];
    public $timestamps = false;
}
