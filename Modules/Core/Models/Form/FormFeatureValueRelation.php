<?php

namespace Modules\Core\Models\Form;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Form\FormFeatureValueRelation as TraitModel;

class FormFeatureValueRelation extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['form_record_id', 'item_id', 'feature_id', 'feature_value_id', 'feature_value', 'active', 'deleted_at', 'created_at', 'updated_at'];
    public $messages = [];
    public $rules = [
        'feature_id' => 'required',
        'form_record_id' => 'required',
        'item_id' => 'required',
    ];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}
