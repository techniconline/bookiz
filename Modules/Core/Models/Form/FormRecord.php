<?php

namespace Modules\Core\Models\Form;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Form\FormRecord as TraitModel;


class FormRecord extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['language_id', 'instance_id', 'form_id', 'user_id', 'system', 'active', 'deleted_at', 'created_at', 'updated_at'];
    public $messages = [];
    public $rules = [
        'instance_id' => 'required',
        'form_id' => 'required',
    ];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}
