<?php

namespace Modules\Core\Models\Location;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Location\Location as TraitModel;


class Location extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['country_id', 'state_id', 'name', 'code', 'latitude', 'longitude', 'deleted_at'];
    public $timestamps = false;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
