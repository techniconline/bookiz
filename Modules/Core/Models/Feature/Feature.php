<?php

namespace Modules\Core\Models\Feature;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Feature\Feature as TraitModel;

class Feature extends BaseModel
{
    use TraitModel;
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['language_id', 'title', 'type', 'parent_id', 'custom', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $messages = [];
    public $rules = [
        'title' => 'required',
        'language_id' => 'required',
        'type' => 'required',
    ];

    protected $appends = ['text_custom'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public $customTypes = ['text', 'textarea', 'hidden'];


}
