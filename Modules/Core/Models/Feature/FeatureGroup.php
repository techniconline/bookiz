<?php

namespace Modules\Core\Models\Feature;

use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Feature\FeatureGroup as TraitModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class FeatureGroup extends BaseModel
{
    use TraitModel;
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['language_id','instance_id', 'title', 'active', 'deleted_at', 'created_at', 'updated_at'];
    public $messages = [];
    public $rules = [
        'title' => 'required',
        'language_id' => 'required',
        'instance_id' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
