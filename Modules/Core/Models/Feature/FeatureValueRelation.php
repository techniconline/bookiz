<?php

namespace Modules\Core\Models\Feature;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Feature\FeatureValueRelation as TraitModel;

class FeatureValueRelation extends BaseModel
{

    use TraitModel;
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['language_id', 'system', 'item_id', 'feature_id', 'feature_value_id', 'feature_value', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $messages = [];
    public $rules = [
        'feature_id' => 'required',
        'language_id' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
