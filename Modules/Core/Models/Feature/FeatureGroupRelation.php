<?php

namespace Modules\Core\Models\Feature;

use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Feature\FeatureGroupRelation as TraitModel;


class FeatureGroupRelation extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['feature_id', 'feature_group_id', 'position'];

    public $timestamps = false;
    public $messages = [];
    public $rules = [
        'feature_id' => 'required',
        'feature_group_id' => 'required',
    ];


}
