<?php

namespace Modules\Core\Models\Feature;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Feature\FeatureValue as TraitModel;


class FeatureValue extends BaseModel
{
    use TraitModel;
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['parent_id', 'feature_id', 'language_id', 'value', 'title', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $messages = [];
    public $rules = [
        'value' => 'required',
        'language_id' => 'required',
        'feature_id' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];


}
