<?php

namespace Modules\Core\Models\Currency;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Currency\Currency as TraitModel;


class Currency extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['name', 'code', 'symbol', 'direction', 'active', 'deleted_at'];
    public $timestamps = false;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}
