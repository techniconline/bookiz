<?php

namespace Modules\Core\Models\Currency;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Currency\Currency as TraitModel;


class CurrencyRate extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['from_currency_id', 'to_currency_id', 'rate'];
    public $timestamps = false;


}
