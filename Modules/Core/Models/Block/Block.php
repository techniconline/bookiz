<?php

namespace Modules\Core\Models\Block;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Block\Block as TraitModel;


class Block extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['module_id', 'name', 'namespace', 'active', 'created_at', 'updated_at', 'deleted_at'];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}
