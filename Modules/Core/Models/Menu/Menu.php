<?php

namespace Modules\Core\Models\Menu;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Menu\Menu as TraitModel;


class Menu extends BaseModel
{
    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['parent_id', 'root_id', 'instance_id', 'language_id', 'title', 'alias', 'type', 'url', 'attributes', 'sort', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $rules = [
        'url' => 'required|unique:menus',
        'type' => 'required',
        'title' => 'required',
    ];

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $softDelete = true;

}
