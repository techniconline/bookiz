<?php

namespace Modules\Core\Models\Page;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Traits\Page\Page as TraitModel;


class Page extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'name', 'route', 'uri', 'column', 'data', 'created_at', 'updated_at', 'deleted_at'];

    const PUBLISH_STATUS = "publish";
    const DRAFT_STATUS = "draft";

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}
