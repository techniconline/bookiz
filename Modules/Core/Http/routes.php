<?php

Route::get('/', 'Modules\Core\Http\Controllers\CoreController@home')->name('index')->middleware('web');
Route::get('/core/test', 'Modules\Core\Http\Controllers\Test\TestController@test')->name('test')->middleware('web');

Route::post('core/formBuilder/meta/{form_id}/{form_meta_group_id}/features/value/save', 'Modules\Core\Http\Controllers\FormBuilder\FormBuilderController@saveFeaturesValue')->name('core.formBuilder.meta.features.value.save')->middleware('web');

Route::group(['middleware' => 'web', 'prefix' => 'core', 'as'=>'core.', 'namespace' => 'Modules\Core\Http\Controllers'], function()
{

    Route::group(['prefix' => 'errors', 'as'=>'error.', 'namespace' => '\Error'], function()
    {
        Route::get('/index', 'ErrorController@404')->name('index');
        Route::get('/access', 'ErrorController@access')->name('access');
        Route::get('/404', 'ErrorController@find')->name('404');
        Route::get('/role', 'ErrorController@role')->name('role');
    });


    Route::group(['prefix' => 'page', 'as'=>'page.', 'namespace' => '\Page'], function()
    {
        Route::get('/show/{slug}', 'PageController@show')->name('show');
        Route::get('/preview/{slug}', 'PageController@preview')->name('preview');
    });

    Route::get('/', 'CoreController@index')->name('index');


    Route::group(['prefix' => 'location', 'as'=>'location.','namespace' => '\Location'], function()
    {
        Route::get('/json', 'LocationController@json')->name('json');
        Route::get('/api/country', 'LocationController@countryApi')->name('api.country');
    });

    Route::group(['prefix' => 'config', 'as' => 'config.', 'namespace' => '\Config'], function () {
        Route::get('/api/getSplashData', 'ApiConfigController@getSplashData')->name('api.getSplashData');
    });

    Route::group(['prefix' => 'formBuilder', 'as' => 'formBuilder.', 'namespace' => '\FormBuilder'], function () {
        Route::group(['prefix' => 'meta', 'as' => 'meta.'], function () {
//            Route::post('/{form_id}/{form_meta_group_id}/features/value/save', 'FormBuilderController@saveFeaturesValue')->name('features.value.save');
        });
    });

});


Route::group(['middleware' => ['web', 'auth', 'admin'], 'prefix' => 'core', 'as' => 'core.', 'namespace' => 'Modules\Core\Http\Controllers'], function () {


    Route::group(['prefix' => 'dashboard', 'as'=>'dashboard.', 'namespace' => '\Dashboard'], function()
    {
        Route::get('/', 'DashboardController@index')->name('index');
        Route::get('/{block_name}/renderChartBlock', 'DashboardController@renderChartBlock')->name('render_chart_block');
    });

    Route::group(['prefix' => 'block', 'as'=>'block.', 'namespace' => '\Block'], function()
    {
        Route::get('/', 'BlockController@index')->name('editor');
        Route::post('/edit/', 'BlockController@edit')->name('editor.edit');
        Route::get('/create/{id}', 'BlockController@create')->name('editor.create');
        Route::post('/validate/{id}', 'BlockController@validate')->name('editor.validate');
    });

    Route::group(['prefix' => 'page', 'as'=>'page.', 'namespace' => '\Page'], function()
    {
        Route::get('/', 'PageController@index')->name('index');
        Route::get('edit/{id}', 'PageController@edit')->name('edit');
        Route::post('save/{id}', 'PageController@save')->name('save');
        Route::get('url/{id}', 'PageController@url')->name('url');
    });

    Route::group(['prefix' => 'route', 'as'=>'route.', 'namespace' => '\Route'], function()
    {
        Route::get('/', 'RouteController@index')->name('index');
        Route::get('edit/{id}', 'RouteController@edit')->name('edit');
        Route::post('save/{id}', 'RouteController@save')->name('save');
    });


    Route::group(['prefix' => 'meta', 'as' => 'meta.', 'namespace' => '\Meta'], function () {

        // for MetaGroup
        Route::group(['prefix' => 'group', 'as' => 'group.'], function () {
            Route::get('/', 'MetaGroupController@index')->name('index');
            Route::get('/create', 'MetaGroupController@create')->name('create');
            Route::get('/{meta_group_id}/edit', 'MetaGroupController@edit')->name('edit');

            Route::post('/save', 'MetaGroupController@store')->name('save');
            Route::put('/{meta_group_id}/update', 'MetaGroupController@update')->name('update');
            Route::delete('/{meta_group_id}/delete', 'MetaGroupController@destroy')->name('delete');
        });

    });

    Route::group(['prefix' => 'formBuilder', 'as' => 'formBuilder.', 'namespace' => '\FormBuilder'], function () {

            Route::get('/', 'FormBuilderController@index')->name('index');
            Route::get('/create', 'FormBuilderController@create')->name('create');
            Route::get('/{form_id}/edit', 'FormBuilderController@edit')->name('edit');

            Route::post('/save', 'FormBuilderController@store')->name('save');
            Route::put('/{form_id}/update', 'FormBuilderController@update')->name('update');
            Route::delete('/{form_id}/delete', 'FormBuilderController@destroy')->name('delete');

        Route::group(['prefix' => 'meta', 'as' => 'meta.'], function () {

            Route::get('/{form_id}/tags/edit', 'FormBuilderController@editMetaTags')->name('editMetaTags');
            Route::post('/{form_id}/{form_meta_group_id}/copy', 'FormBuilderController@copyCourseMetaGroup')->name('copy');
            Route::delete('/{form_id}/{form_meta_group_id}/delete', 'FormBuilderController@deleteCourseMetaGroup')->name('delete');

        });

        Route::group(['prefix' => 'record', 'as' => 'record.'], function () {

            Route::get('{form_id}/', 'FormBuilderRecordController@index')->name('index');
            Route::get('{record_id}/show', 'FormBuilderRecordController@show')->name('show');
            Route::post('{record_id}/changeStatus', 'FormBuilderRecordController@changeStatusRecord')->name('changeStatusRecord');
            Route::delete('{record_id}/delete', 'FormBuilderRecordController@destroyRecord')->name('delete');

        });

    });

    Route::group(['prefix' => 'menu', 'as'=>'menu.', 'namespace' => '\Menu'], function()
    {
        Route::get('/', 'MenuController@index')->name('index');
        Route::get('/create', 'MenuController@create')->name('create');
        Route::post('/save', 'MenuController@store')->name('save');
        Route::post('/add/child/{parent_id}', 'MenuController@addChild')->name('add_child');
        Route::put('/{menu_id}/change_position', 'MenuController@changePosition')->name('change_position');
        Route::put('/{menu_id}/update', 'MenuController@update')->name('update');
        Route::get('/{menu_id}/show', 'MenuController@show')->name('show');
        Route::delete('/{menu_id}/delete', 'MenuController@destroy')->name('delete');
    });

    Route::group(['prefix' => 'language', 'as'=>'language.', 'namespace' => '\Language'], function()
    {
        Route::get('/', 'LanguageController@index')->name('index');
        Route::get('/create', 'LanguageController@create')->name('create');
        Route::post('/save', 'LanguageController@store')->name('save');
        Route::put('/{language_id}/update', 'LanguageController@update')->name('update');
        Route::get('/{language_id}/edit', 'LanguageController@edit')->name('edit');
        Route::delete('/{language_id}/delete', 'LanguageController@destroy')->name('delete');
        Route::post('/changerLanguage', 'LanguageController@changerLanguage')->name('changerLanguage');

    });

    Route::group(['prefix' => 'instance', 'as'=>'instance.', 'namespace' => '\Instance'], function()
    {
        Route::get('/', 'InstanceController@index')->name('index');
        Route::get('/create', 'InstanceController@create')->name('create');
        Route::post('/save', 'InstanceController@store')->name('save');
        Route::put('/{instance_id}/update', 'InstanceController@update')->name('update');
        Route::get('/{instance_id}/edit', 'InstanceController@edit')->name('edit');
        Route::delete('/{instance_id}/delete', 'InstanceController@destroy')->name('delete');
        Route::post('/changerInstance', 'InstanceController@changerInstance')->name('changerInstance');

        Route::get('special/blocks/header/{id}', 'InstanceController@getSpecialBlocksHeader')->name('special_blocks_header');
        Route::post('special/blocks/header/{id}', 'InstanceController@saveSpecialBlocksHeader')->name('special_blocks_header');
        Route::get('special/blocks/footer/{id}', 'InstanceController@getSpecialBlocksFooter')->name('special_blocks_footer');
        Route::post('special/blocks/footer/{id}', 'InstanceController@saveSpecialBlocksFooter')->name('special_blocks_footer');

    });

    Route::group(['prefix' => 'template', 'as'=>'template.', 'namespace' => '\Template'], function()
    {
        Route::get('/', 'TemplateController@index')->name('index');
        Route::get('/create', 'TemplateController@create')->name('create');
        Route::post('/save', 'TemplateController@store')->name('save');
        Route::put('/{template_id}/update', 'TemplateController@update')->name('update');
        Route::get('/{template_id}/edit', 'TemplateController@edit')->name('edit');
        Route::delete('/{template_id}/delete', 'TemplateController@destroy')->name('delete');
    });

    Route::group(['prefix' => 'feature', 'as'=>'feature.', 'namespace' => '\Feature'], function()
    {
        Route::get('/', 'FeatureController@index')->name('index');
        Route::get('/create', 'FeatureController@create')->name('create');
        Route::post('/save', 'FeatureController@store')->name('save');
        Route::put('/{feature_id}/update', 'FeatureController@update')->name('update');
        Route::get('/{feature_id}/edit', 'FeatureController@edit')->name('edit');
        Route::delete('/{feature_id}/delete', 'FeatureController@destroy')->name('delete');

        Route::group(['prefix' => 'values', 'as'=>'values.'], function()
        {
            Route::get('/{feature_id}/create', 'FeatureValueController@create')->name('create');
            Route::post('/{feature_id}/save', 'FeatureValueController@store')->name('save');
            Route::put('/{feature_value_id}/update', 'FeatureValueController@update')->name('update');
            Route::get('/{feature_value_id}/edit', 'FeatureValueController@edit')->name('edit');
            Route::delete('/{feature_value_id}/delete', 'FeatureValueController@destroy')->name('delete');

        });

        Route::group(['prefix' => 'group', 'as'=>'group.'], function()
        {
            Route::get('/', 'FeatureGroupController@index')->name('index');
            Route::get('/create', 'FeatureGroupController@create')->name('create');
            Route::post('/save', 'FeatureGroupController@store')->name('save');
            Route::put('/{group_id}/update', 'FeatureGroupController@update')->name('update');
            Route::get('/{group_id}/edit', 'FeatureGroupController@edit')->name('edit');
            Route::delete('/{group_id}/delete', 'FeatureGroupController@destroy')->name('delete');
            Route::get('/json', 'FeatureGroupController@json')->name('json');

        });
    });

    Route::group(['prefix' => 'config', 'as' => 'config.', 'namespace' => '\Config'], function () {
        Route::get('/instance/index', 'InstanceController@index')->name('instance.index');
        Route::post('/instance/save', 'InstanceController@save')->name('instance.save');
        Route::get('/instance/cache', 'InstanceController@cache')->name('instance.cache');
    });

    Route::group(['namespace' => '\FileManager'], function () {
        //Route::get('/filemanager', 'FileManagerController@app')->name('filemanger');
       // Route::post('/filemanager', 'FileManagerController@app')->name('filemanger');
        Route::get('/filemanager', 'FileManagerController@getFileManager')->name('filemanger');

    });

});



Route::get('/transfer/get', 'Modules\Core\Http\Controllers\TransferController@get')->name('transfer.get');
Route::get('/transfer/save/{id}/{hash}', 'Modules\Core\Http\Controllers\TransferController@save')->name('transfer.save');
Route::get('/transfer/poster', 'Modules\Core\Http\Controllers\TransferController@getPoster')->name('transfer.poster');
