<?php

namespace Modules\Core\Http\Middleware;

use Closure;

class Loader1
{
    private $language = false;
    private $instance = false;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $subdomain = $this->getSubDomain($request->getHost());
//        $dupRequest = $request->duplicate();
//        $step = 1;
//        $segments = [];
//        $segment = $this->getSegment($dupRequest, $step);
//        $this->language = $this->languageDetector($segment);
//        if ($this->language) {
//            $segments[] = $segment;
//            $step = 2;
//        }
//
//        if ($this->isDefaultInstance($subdomain)) {
//            $segment = $this->getSegment($dupRequest, $step);
//            $this->instance = $this->InstanceDetector($segment);
//            if ($this->instance) {
//                $segments[] = $segment;
//            }
//        }
//
//        if (!$this->instance) {
//            $this->instance = $this->getInstances($subdomain);
//        }

        $dupRequest = $request->duplicate();
        $inUrl = $request->getRequestUri();
        $instanceObject = (app('getInstanceObject'))->setRequest($request);
        $segments = $request->segments();
        $host = $request->getHost();
        $hostArr = explode(".",$host);

        //$pathArray = array_filter(explode("/",$dupRequest->path()));
        $instance_name = isset($instanceObject->instance->name)?$instanceObject->instance->name:null;
        $lang = isset($instanceObject->language->code)?$instanceObject->language->code:null;
        $path = collect($segments)->filter(function ($item, $key) use ($instance_name){
            if (!$key && strlen($item)==2)
            {
                return;
            }
            if ($item!=$instance_name){
                return $item;
            }
        })->values()->implode('/');

//        dd($dupRequest->path(), $path);
//        dd($dupRequest->path(), str_replace('en', '', $dupRequest->path()));

        \URL::forceRootUrl(\Config::get('app.url') . '/en/');
        $request->server->set('REQUEST_URI', $path);
//        dd($dupRequest, $request);

        return $next($request);
    }


    public function getSubDomain($host)
    {
        return 'www';
    }

    public function getSegment($request, $step)
    {
        return strtolower($request->segment($step));
    }

    public function isDefaultInstance($name)
    {
        if ($name == config('app.instance')) {
            return true;
        }
        return false;
    }

    public function InstanceDetector($instanceName)
    {
        $instance = $this->getInstances($instanceName);
        if ($instance) {
            if (!$this->language) {
                $this->language = $this->getLanguages($instance->lang_code);
            }
            return $instance;
        }
        return false;
    }

    public function isDefaultLanguage($name)
    {
        if ($name == 'www') {
            return true;
        }
        return false;
    }


    public function languageDetector($lang)
    {
        $language = $this->getLanguages($lang);
        if ($language) {
            return $language;
        }
        return false;
    }

    public function getLanguages($code = false)
    {
        $languages = ['fa' => (object)['id' => 1, 'code' => 'fa', 'direction' => 'RTL'], 'en' => (object)['id' => 2, 'code' => 'en', 'direction' => 'LTR']];
        if ($code) {
            return isset($languages[$code]) ? $languages[$code] : false;
        }
        return $languages;
    }

    public function getInstances($code = false)
    {
        $instances = ['www' => (object)['id' => 1, 'name' => 'www', 'lang_code' => 'fa'], 'rahpooyan' => (object)['id' => 2, 'name' => 'rahpooyan', 'lang_code' => 'fa']];
        if ($code) {
            return isset($instances[$code]) ? $instances[$code] : false;
        }
        return $instances;
    }

}
