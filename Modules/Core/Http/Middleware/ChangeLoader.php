<?php

namespace Modules\Core\Http\Middleware;

use Closure;

class ChangeLoader
{
    private $instance_object;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->instance_object = app('getInstanceObject')->setCurrentInstance();
        return $next($request);
    }


}
