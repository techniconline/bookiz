<?php

namespace Modules\Core\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class CheckRequestUrl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle(Request $request, Closure $next)
    {
        $key = 'url_redirect_old_system';
        if (Cache::has($key)) {
            //Cache::forget($key);
            $urls = Cache::get($key);
        }else{
            $urls = DB::table('redirect_url')->get();
            $urls = $urls->map(function ($row){
                $row->f1 = urldecode($row->f1);
                $row->f2 = urldecode($row->f2);
                return $row;
            })->pluck('f2','f1')->toArray();
            Cache::forever($key, $urls);
        }

        if(key_exists(ltrim(urldecode($_SERVER['REQUEST_URI']),"/"), $urls)){
            return redirect($urls[ltrim(urldecode($_SERVER['REQUEST_URI']),"/")]);
        }


        return $next($request);


        return redirect('/');
        dd("sss");
        $dupRequest = $request->duplicate();
        $inUrl = $request->getRequestUri();
        $instanceObject = app('getInstanceObject');
        $segments = $request->segments();
        $host = $request->getHost();
        $hostArr = explode(".",$host);

        //$pathArray = array_filter(explode("/",$dupRequest->path()));
        $instance_name = isset($instanceObject->instance->name)?$instanceObject->instance->name:null;
        $lang = isset($instanceObject->language->code)?$instanceObject->language->code:null;
        $path = collect($segments)->filter(function ($item, $key) use ($instance_name){
            if (!$key && strlen($item)==2)
            {
                return;
            }
            if ($item!=$instance_name){
                return $item;
            }
        })->values()->implode('/');


//        if(($path))
//        {
            URL::forceRootUrl(Config::get('app.url').'/'.$lang);
            $request->server->set('REQUEST_URI','/'.$path);
//        }

//        dd($route = $request->route());
//
//        if (count($hostArr)==3){
//            unset($hostArr[0]);
//            $host = implode(".",$hostArr);
//        }
         dd($hostArr,$host,$request->getHost(),$instanceObject, $path,$dupRequest->path(),$dupRequest, $request->duplicate());
//        dd($request->getHost().'/'.$path);
//        return redirect($path);

        return $next($request);
    }

}
