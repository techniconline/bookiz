<?php

namespace Modules\Core\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Modules\Core\Libraries\InstanceLibrary;
use Modules\Core\Models\Instance;
use Modules\Core\Models\Language;

class Loader
{

    private $instance_object;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->instance_object = $instanceObject = app('getInstanceObject');
        $request = $instanceObject->init($request)->getRequest();
        $request = $this->setDefaultRequest($request);
        $isApiInstance = $this->isApiInstance();
        if ($isApiInstance === false) {
            return response()->json(['action' => false, 'message' => trans('core::validations.alert.not_valid_instance'), 'code' => 302]);
        }

        return $next($request);
    }

    /**
     * TODO check in Instance ADMIN !!!
     * @return bool
     */
    private function isApiInstance()
    {
        $subDomain = $this->instance_object->getCurrentSubDomain();
        $instanceId = $this->instance_object->getCurrentInstanceId();
        if ((($this->instance_object->isApi()) && ($instanceId))) {
            return true;
        } elseif (($subDomain != \config('app.api', "api"))) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param Request $request
     * @return Request|mixed
     */
    private function setDefaultRequest(Request $request)
    {
//        $request = $this->setIsApi($request);
        $request = $this->setXRequested($request);
        $request = $this->setLanguageRequest($request);
//        $request = $this->setInstanceRequest($request);
        return $request;
    }

    /**
     * @param $request
     * @return mixed
     */
    private function setIsApi($request)
    {
        $subDomain = $this->instance_object->getCurrentSubDomain();
        $instanceId = $this->instance_object->getCurrentInstanceId();
        if ((($subDomain == \config('app.api', "api")) && ($instanceId))) {
//            $request->request->set('is_api', true);
//            $request->request->set('is_ajax', 1);
        }
        return $request;
    }

    /**
     * add language_id in request
     * @param $request
     * @return mixed
     */
    private function setLanguageRequest($request)
    {
        if (!$request->get('language_id') || !((int)$request->get('language_id'))) {
            $request->request->set('language_id', $this->instance_object->getLanguageId());
        }
        return $request;
    }

    /**
     * add instance_id in request
     * @param $request
     * @return mixed
     */
    private function setInstanceRequest($request)
    {
        if (!$request->get('instance_id') || !((int)$request->get('instance_id'))) {
            $request->request->set('instance_id', $this->instance_object->getCurrentInstanceId());
        }
        return $request;
    }

    /**
     * add ajax request in header
     * @param $request
     * @return mixed
     */
    private function setXRequested($request)
    {
        if ($request->get('is_ajax', 0) && !$request->hasHeader('X-Requested-With')) {
            $request->headers->set('X-Requested-With', true);
        }
        return $request;
    }

}
