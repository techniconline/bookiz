<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Console\Commands\Moodle\Master;

class TransferController extends Controller
{
    private $master;

    public function get(){
        $this->boot();
        return response()->json($this->master->getFileList(),200);
    }

    public function getPoster(){
        $this->boot();
        return response()->json($this->master->getPosterList(),200);
    }


    public function save($id,$hash){
        $this->boot();
        return response()->json($this->master->saveFileOnList($id,$hash),200);
    }

    private function boot(){
        $this->master=new Master();
    }
}
