<?php

namespace Modules\Core\Http\Controllers\Page;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class PageController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('page.list')->setActionMethod("getPageList")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id,Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('page.edit')->setActionMethod("getEditForm")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save($id,Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('page.edit')->setActionMethod("savePage")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function url($id,Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('page.edit')->setActionMethod("getPageUrl")->response();
    }

    /**
     * @param $slug
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($slug,Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['slug'=>$slug],$request)->setViewModel('page.show')->setActionMethod("getShowPage")->response();
    }

    /**
     * @param $slug
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function preview($slug,Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['slug'=>$slug],$request)->setViewModel('page.show')->setActionMethod("getPreviewPage")->response();
    }

}
