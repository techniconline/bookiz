<?php

namespace Modules\Core\Http\Controllers\Block;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\Blocks\BlockLoader;

class BlockController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('core::block.editor');
    }


    public function create($id,Request $request){
        $model=new BlockLoader($id);
        return $model->getConfigContent($request);
    }


    public function validate($id,Request $request){
        $model=new BlockLoader($id);
        return $model->setState('result')->getConfigContent($request);
    }


    public function edit(Request $request){
        $model=new BlockLoader($request->input('block_id'));
        return $model->getConfigContent($request);
    }


}
