<?php

namespace Modules\Core\Http\Controllers\Location;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class LocationController extends Controller
{


    public function json(Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['is_ajax'=>1],$request)->setViewModel('location.json')->setActionMethod("getJson")->response();
    }

    public function countryApi(Request $request,MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['is_ajax'=>1],$request)->setViewModel('location.json')->setActionMethod("getCountryByApi")->response();
    }
}
