<?php

namespace Modules\Core\Http\Controllers\Menu;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class MenuController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('menu')->setActionMethod("getViewListIndex")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('menu')
            ->setActionMethod("createMenu")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('menu')
            ->setActionMethod("saveMenu")->response();
    }

    /**
     * @param $parent_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addChild($parent_id, MasterViewModel $masterViewModel, Request $request)
    {
        $request->request->set('parent_id', $parent_id);
        return $this->store($masterViewModel, $request);
    }

    /**
     * Show the specified resource.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['menu_id' => $id], $request)->setViewModel('menu')
            ->setActionMethod("showMenuTree")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePosition($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['menu_id' => $id], $request)->setViewModel('menu')
            ->setActionMethod("changePosition")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['menu_id' => $id], $request)->setViewModel('menu')
            ->setActionMethod("saveMenu")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['menu_id' => $id], $request)->setViewModel('menu')
            ->setActionMethod("destroyMenu")->response();
    }
}
