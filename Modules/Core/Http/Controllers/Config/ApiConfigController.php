<?php

namespace Modules\Core\Http\Controllers\Config;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class ApiConfigController extends Controller
{
    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getSplashData(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setViewModel('config.apiConfig')->setActionMethod("getSplashData")->response();
    }

}
