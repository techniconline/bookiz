<?php

namespace Modules\Core\Http\Controllers\FormBuilder;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\MasterViewModel;

class FormBuilderRecordController extends Controller
{

    /**
     * @param $form_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($form_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['form_id' => $form_id], $request)->setViewModel('formBuilder.formBuilderRecordGrid')->setActionMethod("getViewListIndex")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $record_id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($record_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['record_id' => $record_id], $request)->setViewModel('formBuilder.formBuilderRecord')
            ->setActionMethod("show")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $record_id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatusRecord($record_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['record_id' => $record_id], $request)->setViewModel('formBuilder.formBuilderRecord')
            ->setActionMethod("changeStatusRecord")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $record_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyRecord($record_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['record_id' => $record_id], $request)->setViewModel('formBuilder.formBuilderRecord')
            ->setActionMethod("destroyRecord")->response();
    }


}
