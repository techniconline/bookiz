<?php

namespace Modules\Core\Http\Controllers\FormBuilder;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\MasterViewModel;

class FormBuilderController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('formBuilder')->setActionMethod("getViewListIndex")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('formBuilder')
            ->setActionMethod("createForm")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('formBuilder')
            ->setActionMethod("saveForm")->response();
    }

    /**
     * Show the specified resource.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['form_id' => $id], $request)->setViewModel('formBuilder')
            ->setActionMethod("createForm")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['form_id' => $id], $request)->setViewModel('formBuilder')
            ->setActionMethod("saveForm")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['form_id' => $id], $request)->setViewModel('formBuilder')
            ->setActionMethod("destroyForm")->response();
    }

    /**
     * @param $form_id
     * @param $form_meta_group_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveFeaturesValue($form_id, $form_meta_group_id, MasterViewModel $masterViewModel, Request $request)
    {
        $user = BridgeHelper::getAccess()->getUser();
        return $masterViewModel->setItemsRequest(['user_id' => $user ? $user->id : null, 'form_id' => $form_id, 'form_meta_group_id' => $form_meta_group_id], $request)
            ->setViewModel('formBuilder')
            ->setActionMethod("saveFeaturesValue")->response();
    }

    /**
     * @param $form_id
     * @param $form_meta_group_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function copyFormMetaGroup($form_id, $form_meta_group_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['form_id' => $form_id, 'form_meta_group_id' => $form_meta_group_id], $request)
            ->setViewModel('formBuilder')
            ->setActionMethod("copyFormMetaGroup")->response();
    }

    /**
     * @param $form_id
     * @param $form_meta_group_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteFormMetaGroup($form_id, $form_meta_group_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['form_id' => $form_id, 'form_meta_group_id' => $form_meta_group_id], $request)
            ->setViewModel('formBuilder')
            ->setActionMethod("deleteFormMetaGroup")->response();
    }

    /**
     * Show the specified resource.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editMetaTags($form_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['form_id' => $form_id], $request)->setViewModel('formBuilder')
            ->setActionMethod("editMetaTags")->response();
    }

}
