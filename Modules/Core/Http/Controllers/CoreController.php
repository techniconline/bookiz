<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Route;
use BridgeHelper;
use Modules\Core\ViewModels\CoreViewModel;
use Modules\Core\ViewModels\MasterViewModel;
use Modules\Core\Services\InstanceService;
use Nwidart\Modules\Facades\Module;

class CoreController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel)
    {
//        dd(json_encode(["header"=>['center'=>
//            [
//                ["block_id"=>3 , "alias"=>"logo_block", "active"=>1, "configs"=>[]],
//                ["block_id"=>2 , "alias"=>"user_block", "active"=>1, "configs"=>[]],
//            ]
//        ],
//            "footer"=>['center'=>
//                [["block_id"=>1 , "alias"=>"language_block", "active"=>1, "configs"=>[]]]
//            ]]));
        return $masterViewModel->setViewModel('dashboard.index')->setActionMethod("getHomePage")->response();
//        return $coreViewModel->setConfig('core')->setTheme('admin')->setView('core::index', true)->render();
    }


    public function home(){
        $instance=app('getInstanceObject')->getInstance();
        if($instance->name==config('app.admin')) {
            $route='core.dashboard.index';
        }else{
            $route=BridgeHelper::getConfig()->getSettings('home','instance','core');
            if(!$route){
                $route='core.index';
            }
        }
       $module=substr($route,0,strpos($route,'.'));
       if($module!='core'){
           $module = Module::find($module);
           if($module){
               app('getInstanceObject')->setModuleName($module->getLowerName());
           }
       }
       return Route::dispatch(Request::create(route($route), 'GET'));
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('core::create');
    }


    /**
     * @param Request $request
     * @param InstanceService $instanceService
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, InstanceService $instanceService)
    {
        return $instanceService->save($request)->response();
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('core::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
