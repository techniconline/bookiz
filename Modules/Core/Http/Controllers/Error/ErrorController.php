<?php

namespace Modules\Core\Http\Controllers\Error;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ErrorController extends Controller
{
    public function index(){
        return view('core::error.find');
    }

    public function access(){
        return view('core::error.access');
    }

    public function find(){
        return view('core::error.find');
    }

    public function role(){
        return view('core::error.role');
    }

}
