<?php

namespace Modules\Core\Http\Controllers\Dashboard;

use Illuminate\Routing\Controller;

use Modules\Core\ViewModels\MasterViewModel;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel)
    {

        return $masterViewModel->setViewModel('dashboard')->setActionMethod("getDashboard")->response();
    }

    /**
     * @param $chart_block_name
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function renderChartBlock($chart_block_name, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['block_name' => $chart_block_name], $request)->setViewModel('dashboard')->setActionMethod("renderChartBlock")->response();
    }

}
