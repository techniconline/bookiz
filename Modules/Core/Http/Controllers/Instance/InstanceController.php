<?php

namespace Modules\Core\Http\Controllers\Instance;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class InstanceController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('instance')->setActionMethod("getViewListIndex")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('instance')
            ->setActionMethod("createInstance")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('instance')
            ->setActionMethod("saveInstance")->response();
    }

    /**
     * Show the specified resource.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['instance_id' => $id], $request)->setViewModel('instance')
            ->setActionMethod("createInstance")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['instance_id' => $id], $request)->setViewModel('instance')
            ->setActionMethod("saveInstance")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['instance_id' => $id], $request)->setViewModel('instance')
            ->setActionMethod("destroyInstance")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changerInstance(MasterViewModel $masterViewModel,Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('instance')
            ->setActionMethod("changerInstance")->response();
    }



    public function getSpecialBlocksHeader($id, MasterViewModel $masterViewModel, Request $request){
        return $masterViewModel->setItemsRequest(['instance_id' => $id], $request)->setViewModel('instance.blocks')
            ->setActionMethod("getBlocksHeaderForm")->response();
    }

    public function saveSpecialBlocksHeader($id, MasterViewModel $masterViewModel, Request $request){
            return $masterViewModel->setItemsRequest(['instance_id' => $id], $request)->setViewModel('instance.blocks')
                ->setActionMethod("saveBlocksHeader")->response();
    }



    public function getSpecialBlocksFooter($id, MasterViewModel $masterViewModel, Request $request){
        return $masterViewModel->setItemsRequest(['instance_id' => $id], $request)->setViewModel('instance.blocks')
            ->setActionMethod("getBlocksFooterForm")->response();
    }

    public function saveSpecialBlocksFooter($id, MasterViewModel $masterViewModel, Request $request){
        return $masterViewModel->setItemsRequest(['instance_id' => $id], $request)->setViewModel('instance.blocks')
            ->setActionMethod("saveBlocksFooter")->response();
    }

}
