<?php

namespace Modules\Core\Http\Controllers\Route;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class RouteController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('route.list')->setActionMethod("getRouteList")->response();
    }


    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id, Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['id' => $id], $request)->setViewModel('route.edit')->setActionMethod("getEditForm")->response();
    }

    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save($id, Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['id' => $id], $request)->setViewModel('route.edit')->setActionMethod("savePage")->response();
    }

}
