<?php

namespace Modules\Core\Http\Controllers\FileManager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller;
use BridgeHelper;
use RFM\Application as FileManager;
use Modules\Core\Libraries\Filemanager\Storage as Storage;
use RFM\Repository\Local\Storage as StorageLocal;
use RFM\Api\AwsS3Api as S3_SYS;
use RFM\Api\LocalApi as Local_SYS;

class FileManagerController extends Controller
{

    public function app(Request $request)
    {
        $instanceApp = app('getInstanceObject');
        $instance = $instanceApp->getCurrentInstance();
        require_once base_path('public/plugins/filemanager/connectors/php/events.php');

        if (env('USE_FILESYSTEM', 's3') == 's3') {
            $config = $this->getS3Config();
        } else {
            $config = $this->getLocalConfig();

        }
        $app = new FileManager();
        if (!$instance) {
            die('can not find instance');
        }

        $source_path = DIRECTORY_SEPARATOR . $instance->name . DIRECTORY_SEPARATOR;
        if ($request->has('mode') && $request->get('mode') == 'getimage' && $request->get('thumbnail')) {
            $path = '/storage' . $source_path . ltrim($request->get('path'), DIRECTORY_SEPARATOR);
            $fileUrl = url($path);
            $filename = public_path('upload' . $path);
            $filename = substr($filename, 0, strrpos($filename, '.')) . '.jpg';
            $result = $this->Thumbnail($fileUrl, $filename, 150);
            if ($result) {
                $url = str_replace(public_path('/'), '/', $result);
                $response = Response::make(null, 200);
                $response->header('X-Accel-Redirect', $url);
                return $response;
            } else {
                $url = parse_url($fileUrl);
                $response = Response::make(null, 200);
                $response->header('X-Accel-Redirect', $url['path']);
                return $response;
            }
        }

        if (env('USE_FILESYSTEM', 's3') == 's3') {
            $sys = new Storage($config);
        } else {
            $sys = new StorageLocal($config);
        }

        $sys->setRoot($source_path, true);
        $app->setStorage($sys);
        if (env('USE_FILESYSTEM', 's3') == 's3') {
            $app->api = new S3_SYS();
        } else {
            $app->api = new Local_SYS();
        }
        return $app->run();
    }


    private function getLocalConfig()
    {
        $config = require base_path('public/plugins/filemanager/connectors/php/configs/config.local.php');
        return $config;
    }

    private function getS3Config()
    {
        $config = require base_path('public/plugins/filemanager/connectors/php/configs/config.php');
        return $config;
    }

    private function Thumbnail($url, $filename, $width = 150, $height = true)
    {
        // download and create gd image
        if (File::exists($filename)) {
            return $filename;
        } else {
            try {
                $image = ImageCreateFromString(file_get_contents($url));
                if ($image !== false) {
                    // calculate resized ratio
                    // Note: if $height is set to TRUE then we automatically calculate the height based on the ratio
                    $height = $height === true ? (ImageSY($image) * $width / ImageSX($image)) : $height;

                    $parsedPath = dirname($filename);
                    if (!file_exists($parsedPath)) {
                        mkdir($parsedPath, 0777, true);
                    }
                    // create image
                    $output = ImageCreateTrueColor($width, $height);
                    $backgroundColor = imagecolorallocate($output, 255, 255, 255);
                    imagefill($output, 0, 0, $backgroundColor);
                    ImageCopyResampled($output, $image, 0, 0, 0, 0, $width, $height, ImageSX($image), ImageSY($image));
                    // save image
                    $result = ImageJPEG($output, $filename, 80);
                    chmod($filename, 0755);
                    if ($result) {
                        // $url=DIRECTORY_SEPARATOR.str_replace(public_path('/'),'',$filename);
                        return $filename;
                    }
                }
            } catch (\Exception $e) {
            }

        }
        return false;
    }



    public function getFileManager(){
        return view("core::filemanager.index");
    }
}
