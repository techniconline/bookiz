<?php
namespace Modules\Core\Bridge\LocationHelper;

use BridgeHelper;
use Modules\Core\Models\Location\Location;
/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class LocationHelper
{
    public function getLocation($id){
        $key='location_key_id_'.$id;
        if(!($location=cache($key))){
            $location=Location::find($id);
            if($location){
                $location=(object)$location->toArray();
            }
        }
        return $location;
    }

    public function getLocationName($id){
        if($id){
            $location=$this->getLocation($id);
            if($location){
                return $location->name;
            }
        }
        return null;
    }
}