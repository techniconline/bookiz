<?php
namespace Modules\Core\Bridge\CurrencyHelper\Facades;

use Illuminate\Support\Facades\Facade;

class CurrencyHelper extends Facade
{

    /**
     * get sms service
     * @return string
     */
    protected static function getFacadeAccessor() { return 'CurrencyHelper'; }

}