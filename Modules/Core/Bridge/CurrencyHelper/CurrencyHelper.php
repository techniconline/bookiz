<?php
namespace Modules\Core\Bridge\CurrencyHelper;

use Illuminate\Support\Facades\Cache;
use Modules\Core\Models\Currency\Currency;
use BridgeHelper;
use Modules\Core\Models\Currency\CurrencyRate;
/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class CurrencyHelper
{

    private $currentCurrency=null;

    public function getCurrency($id,$bycode=false){
        $key='currency_by_id_code_'.$id;
        $currency=Cache::get($key);
        if(!$currency){
            if($bycode){
                $currency=Currency::where('code',$id)->first();
            }else{
                $currency=Currency::find($id);
            }
            if($currency){
                Cache::put($key,$currency,3600);
            }
        }
        return $currency;
    }

    public function getCurrencyList($forSelect=false){
        $list=Currency::where('active',1)->get();
        if(!$forSelect){
            return $list;
        }
        $lists=[];
        foreach ($list as $currency){
            $lists[$currency->id]=$currency->name;
        }
        return $lists;
    }


    public function getFormattedPrice($price,$code,$decimal=0,$negative=false){
        $price=(float)$price;
        $price=number_format($price,$decimal);
        if($negative){
            $price=$price.'- ';
        }
        $formatedPrice=trans('core::currency.format',['price'=>$price,'symbol'=>trans('core::currency.'.$code)]);
        return $formatedPrice;
    }


    public function getConvertPrice($price , $from_currency_id,$to_currency_id,$formated=false){
        $price=$this->getNormalPrice($price);
        $currentCurrency=$this->getCurrency($to_currency_id);
        if(!$currentCurrency){
            return false;
        }
        if(!$from_currency_id){
            $from_currency_id=$currentCurrency->id;
        }
        if(!$to_currency_id){
            $to_currency_id=$currentCurrency->id;
        }
        if($from_currency_id==$to_currency_id){
            if($formated){
                return $this->getFormattedPrice($price,$currentCurrency->code,$currentCurrency->decimal);
            }else{
                return $price;
            }
        }

        $rate=CurrencyRate::where('to_currency_id',$currentCurrency->id)->where('from_currency_id',$from_currency_id)->first();
        $price=($price*$rate->rate);
        if($formated){
            return $this->getFormattedPrice($price,$currentCurrency->code,$currentCurrency->decimal);
        }
        return $price;
    }


    public function getPriceByCurrencyRate($price , $from_currency_id,$formated=false,$negative=false){
        $price=$this->getNormalPrice($price);
        $currentCurrency=$this->getCurrentCurrency();
        $currentCurrencyId=$currentCurrency->id;
        if(!$from_currency_id){
            $from_currency_id=$currentCurrency->id;
        }
        if($currentCurrencyId==$from_currency_id){
           if($formated){
               return $this->getFormattedPrice($price,$currentCurrency->code,$currentCurrency->decimal,$negative);
           }else{
               return $price;
           }
        }
        $rate=CurrencyRate::where('to_currency_id',$currentCurrencyId)->where('from_currency_id',$from_currency_id)->first();
        $price=($price*$rate->rate);
        if($formated){
            return $this->getFormattedPrice($price,$currentCurrency->code,$currentCurrency->decimal,$negative);
        }
        return $price;
    }


    public function getCurrentCurrency(){
        if($this->currentCurrency){
            return $this->currentCurrency;
        }
        $instance=app('getInstanceObject')->getCurrentInstance();
        $this->currentCurrency=Currency::find($instance->currency_id);
        return $this->currentCurrency;
    }

    public function getCurrentCurrencyId(){
        $currency=$this->getCurrentCurrency();
        return $currency->id;
    }


    public function getNormalPrice($price){
        return (float)str_replace(',','',$price);
    }


    public function getCurrencyByCode($code,$field=null){
        $currency=$this->getCurrency($code,true);
        if($field){
            if(isset($currency->{$field})){
                return $currency->{$field};
            }
            return null;
        }
        return $currency;
    }
}