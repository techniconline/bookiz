<?php
namespace Modules\Core\Bridge\Config\Facades;

use Illuminate\Support\Facades\Facade;

class Config extends Facade
{

    /**
     * get sms service
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Config'; }

}