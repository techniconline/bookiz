<?php
namespace Modules\Core\Bridge\Config;
use Modules\Core\Traits\Config\Settings;

/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class Config
{
    use Settings;

    public function getSettings($name=null,$setting_name,$module,$instance_id=null,$language_id=null){
        return $this->setInstanceId($instance_id)->setLanguageId($language_id)->setSettings($module,$setting_name)->getSetting($name);
    }
}