<?php
namespace Modules\Core\Bridge\BlockHelper\Facades;

use Illuminate\Support\Facades\Facade;

class BlockHelper extends Facade
{

    /**
     * get sms service
     * @return string
     */
    protected static function getFacadeAccessor() { return 'BlockHelper'; }

}