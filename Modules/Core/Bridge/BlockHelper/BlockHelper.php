<?php
namespace Modules\Core\Bridge\BlockHelper;
use Illuminate\Support\Facades\Cache;
use Modules\Core\Models\Block\Block;
/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class BlockHelper
{

    public function getBlock($name,$block=null){
        if(!$block){
            $block=$this->getBlockModel($name);
        }
        if($block){
            $arrNameSpace = explode("\\", $block["namespace"]);
            $widgetClass = implode("\\", array_filter($arrNameSpace)) . "\\" . end($arrNameSpace);
        }else{
            $block=[];
            $widgetClass ="Modules\Core\ViewModels\Blocks\FakeBlock";
        }
        $widget=false;
        if (class_exists($widgetClass)) {
            $widget = new $widgetClass();
            $widget->setBlock($block);
        }
        return $widget;
    }

    private function getBlockModel($name){
        $key='get_block_by_name_'.$name;
        $block=Cache::get($key);
        if(!$block){
            $block=Block::where('name',$name)->first();
            $block=$block->toArray();
            Cache::forever($key,$block);
        }
        return $block;
    }
}