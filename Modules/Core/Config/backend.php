<?php

return [
    'version' => '20180619001',
    'blocks' => [
        'left' => [],
        'top' => [
            ['alias' => 'logo_block',
                'view_model' => \Modules\Core\ViewModels\Blocks\LogoBlock\MenuBlock::class,
                'order' => null,
                'position' => 'top',
                'before' => '',
                'after' => '',
                'configs' => []
            ],
        ]
    ],
    'menus' => [
        'management' => [
            ['alias' => '', //**
                'route' => 'core.language.index', //**
                'key_trans' => 'core::menu.language', //**
                'order' => 5, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-language', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'languages',
            ],
            [
                'alias' => '', //**
                'route' => 'core.instance.index', //**
                'key_trans' => 'core::menu.instance_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => '', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'instances',
            ],
            [
                'alias' => 'core.route.index', //**
                'route' => 'core.route.index', //**
                'key_trans' => 'core::menu.route_block_list', //**
                'order' => 3, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-cubes', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'instances',
            ],
            [
                'alias' => '', //**
                'route' => 'core.template.index', //**
                'key_trans' => 'core::menu.templates_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => '', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'templates',
            ],
            [
                'alias' => '', //**
                'route' => 'core.formBuilder.index', //**
                'key_trans' => 'core::menu.formBuilder_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => '', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'formBuilder',
            ]
        ],
        'management_admin' => [
            ['alias' => '', //**
                'route' => 'core.dashboard.index', //**
                'key_trans' => 'core::menu.dashboard', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-tachometer', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_dashboard',
            ],
            ['alias' => '', //**
                'route' => 'core.page.index', //**
                'key_trans' => 'core::menu.admin_page', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-files-o', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_pages',
            ],
            ['alias' => '', //**
                'route' => 'core.filemanger', //**
                'key_trans' => 'core::menu.file_manger', //**
                'order' => 10, //if null end menu and if zero (0) top menu
                'target' => '_blank',
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-folder-open', //fa-XXXXX
                'class' => '',
                'id' => '',
                'group_menu' => 'admin_pages',
            ],
            [
                'alias' => '', //**
                'route' => 'core.menu.index', //**
                'key_trans' => 'core::menu.menus_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => '', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'menus',
            ],
            [
                'alias' => '', //**
                'route' => 'core.feature.index', //**
                'key_trans' => 'core::menu.feature_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => '', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_features',
            ],

            [
                'alias' => '', //**
                'route' => 'core.feature.group.index', //**
                'key_trans' => 'core::menu.feature_group_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => '', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_features',
            ],
            [
                'alias' => '', //**
                'route' => 'core.meta.group.index', //**
                'key_trans' => 'core::menu.meta_group_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'icon-tag', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'meta_groups',
            ],
            [
                'alias' => 'core.cache.delete', //**
                'route' => 'core.config.instance.cache', //**
                'key_trans' => 'core::menu.config.cache_clear', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-cog', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_manage_settings',
            ],
            [
                'alias' => 'core.config.instance.index', //**
                'route' => 'core.config.instance.index', //**
                'key_trans' => 'core::menu.config.instance', //**
                'order' => 1000, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-cog', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_manage_settings',
            ],

        ],
    ],
    'group_menu' => [
        'management_admin' => [
            ['alias' => 'admin_dashboard', //**
                'key_trans' => 'core::menu.group.dashboard', //**
                'icon_class' => 'fa fa-tachometer',
                'before' => '',
                'after' => '',
                'order' => 1,
            ],
            ['alias' => 'admin_pages', //**
                'key_trans' => 'core::menu.group.pages', //**
                'icon_class' => 'fa fa-file',
                'before' => '',
                'after' => '',
                'order' => 2,
            ],
            ['alias' => 'admin_features', //**
                'key_trans' => 'core::menu.group.features', //**
                'icon_class' => 'fa fa-cog',
                'before' => '',
                'after' => '',
                'order' => 1000,
            ],
            ['alias' => 'meta_groups', //**
                'key_trans' => 'core::menu.group.meta_groups', //**
                'icon_class' => '',
                'before' => '',
                'after' => '',
                'order' => 1001,
            ],
            ['alias' => 'menus', //**
                'key_trans' => 'core::menu.group.menus', //**
                'icon_class' => '',
                'before' => '',
                'after' => '',
                'order' => 800,
            ],
            ['alias' => 'admin_manage_settings', //**
                'key_trans' => 'core::menu.group.settings', //**w
                'icon_class' => 'fa fa-cog',
                'before' => '',
                'after' => '',
                'order' => 10000000,
            ],
        ],
        'management' => [
            ['alias' => 'settings', //**
                'key_trans' => 'core::menu.group.settings', //**
                'icon_class' => 'fa fa-cog',
                'before' => '',
                'after' => '',
                'order' => 10000000,],
            ['alias' => 'instances', //**
                'key_trans' => 'core::menu.group.instances', //**
                'icon_class' => '',
                'before' => '',
                'after' => '',
                'order' => 60,],
            ['alias' => 'templates', //**
                'key_trans' => 'core::menu.group.templates', //**
                'icon_class' => '',
                'before' => '',
                'after' => '',
                'order' => 80,
            ],
            ['alias' => 'languages', //**
                'key_trans' => 'core::menu.group.languages', //**
                'icon_class' => '',
                'before' => '',
                'after' => '',
                'order' => 90,
            ],
            ['alias' => 'formBuilder', //**
                'key_trans' => 'core::menu.group.formBuilder', //**
                'icon_class' => '',
                'before' => '',
                'after' => '',
                'order' => 90,
            ]
        ],
    ],

];
