<?php

return [
    'name' => 'Core',
    'permissions'=>[
        'index'=>true,
        'test'=>true,
//        'core.formBuilder.meta.features.value.save'=>['type' => '|', 'access' => ''],
        'core.formBuilder.meta.features.value.save'=>true,
        'core.error.index'=>true,
        'core.error.access'=>true,
        'core.error.404'=>true,
        'core.page.show'=>true,
        'core.page.preview'=>['type' => '|', 'access' => ['core/manage.page.add','core/manage.page.edit','core/manage.page.publish','core/manage.page.delete']],
        'core.index'=>true,
        'core.location.json'=>true,
        'core.location.api.country'=>true,
        'core.config.api.getSplashData'=>true,
        'core.dashboard.index'=>['type' => '|', 'access' => ['core/is.super.admin','core/is.admin']],
        'core.block.editor'=>['type' => '|', 'access' => 'core/is.super.admin'],
        'core.block.editor.edit'=>['type' => '|', 'access' => 'core/is.super.admin'],
        'core.block.editor.create'=>['type' => '|', 'access' => 'core/is.super.admin'],
        'core.block.editor.validate'=>['type' => '|', 'access' => 'core/is.super.admin'],
        'core.page.index'=>['type' => '|', 'access' => ['core/manage.page.add','core/manage.page.edit','core/manage.page.publish','core/manage.page.delete']],
        'core.page.edit'=>['type' => '|', 'access' => 'core/manage.page.edit'],
        'core.page.save'=>['type' => '|', 'access' => ['core/manage.page.add','core/manage.page.edit','core/manage.page.publish','core/manage.page.delete']],
        'core.page.url'=>['type' => '|', 'access' => 'core/manage.page.publish'],
        'core.route.index'=>['type' => '|', 'access' => 'core/is.super.admin'],
        'core.route.edit'=>['type' => '|', 'access' => 'core/is.super.admin'],
        'core.route.save'=>['type' => '|', 'access' => 'core/is.super.admin'],
        'core.meta.group.index'=>['type' => '|', 'access' => 'core/manage.feature.group'],
        'core.meta.group.create'=>['type' => '|', 'access' => 'core/manage.feature.group.add'],
        'core.meta.group.edit'=>['type' => '|', 'access' => 'core/manage.feature.group.edit'],
        'core.meta.group.save'=>['type' => '|', 'access' => ['core/manage.feature.group.add','core/manage.feature.group.edit','manage.feature.group.delete']],
        'core.meta.group.update'=>['type' => '|', 'access' => ['core/manage.feature.group.add','core/manage.feature.group.edit','manage.feature.group.delete']],
        'core.meta.group.delete'=>['type' => '|', 'access' => 'manage.feature.group.delete'],
        'core.formBuilder.index'=>['type' => '|', 'access' => ['core/is.super.admin','core/manage.instance']],
        'core.formBuilder.create'=>['type' => '|', 'access' => ['core/is.super.admin','core/manage.instance']],
        'core.formBuilder.edit'=>['type' => '|', 'access' => ['core/is.super.admin','core/manage.instance']],
        'core.formBuilder.save'=>['type' => '|', 'access' => ['core/is.super.admin','core/manage.instance']],
        'core.formBuilder.update'=>['type' => '|', 'access' => ['core/is.super.admin','core/manage.instance']],
        'core.formBuilder.delete'=>['type' => '|', 'access' => ['core/is.super.admin','core/manage.instance']],
        'core.formBuilder.meta.editMetaTags'=>['type' => '|', 'access' => ['core/is.super.admin','core/is.admin','core/manage.instance']],
        'core.formBuilder.meta.copy'=>['type' => '|', 'access' => ['core/is.super.admin','core/is.admin','core/manage.instance']],
        'core.formBuilder.meta.delete'=>['type' => '|', 'access' => ['core/is.super.admin','core/is.admin','core/manage.instance']],
        'core.menu.index'=>['type' => '|', 'access' => 'core/manage.menu'],
        'core.menu.create'=>['type' => '|', 'access' => 'core/manage.menu.add'],
        'core.menu.save'=>['type' => '|', 'access' => ['core/manage.menu.add','core/manage.menu.edit']],
        'core.menu.add_child'=>['type' => '|', 'access' => 'core/manage.menu.add'],
        'core.menu.change_position'=>['type' => '|', 'access' => ['core/manage.menu.add','core/manage.menu.edit']],
        'core.menu.update'=>['type' => '|', 'access' => ['core/manage.menu.add','core/manage.menu.edit']],
        'core.menu.show'=>['type' => '|', 'access' => ['core/manage.menu.add','core/manage.menu.edit']],
        'core.menu.delete'=>['type' => '|', 'access' => 'core/manage.menu.delete'],
        'core.language.index'=>['type' => '|', 'access' => 'core/manage.language'],
        'core.language.create'=>['type' => '|', 'access' => 'core/manage.language.add'],
        'core.language.save'=>['type' => '|', 'access' => ['core/manage.language.add','core/manage.language.edit']],
        'core.language.update'=>['type' => '|', 'access' => ['core/manage.language.add','core/manage.language.edit']],
        'core.language.edit'=>['type' => '|', 'access' => 'core/manage.language.edit'],
        'core.language.delete'=>['type' => '|', 'access' => 'core/manage.language.delete'],
        'core.language.changerLanguage'=>['type' => '|', 'access' => 'core/language.change'],
        'core.instance.index'=>['type' => '|', 'access' => 'core/manage.instance'],
        'core.instance.create'=>['type' => '|', 'access' => 'core/manage.instance.add'],
        'core.instance.save'=>['type' => '|', 'access' => 'core/manage.instance.add'],
        'core.instance.update'=>['type' => '|', 'access' => 'core/manage.instance.edit'],
        'core.instance.edit'=>['type' => '|', 'access' => 'core/manage.instance.edit'],
        'core.instance.delete'=>['type' => '|', 'access' => 'core/manage.instance.delete'],
        'core.instance.changerInstance'=>['type' => '|', 'access' => 'core/is.super.admin'],
        'core.instance.special_blocks_header'=>['type' => '|', 'access' => 'core/is.super.admin'],
        'core.instance.special_blocks_header'=>['type' => '|', 'access' => 'core/is.super.admin'],
        'core.instance.special_blocks_footer'=>['type' => '|', 'access' => 'core/is.super.admin'],
        'core.instance.special_blocks_footer'=>['type' => '|', 'access' => 'core/is.super.admin'],
        'core.template.index'=>['type' => '|', 'access' => 'core/manage.template'],
        'core.template.create'=>['type' => '|', 'access' => 'core/manage.template.add'],
        'core.template.save'=>['type' => '|', 'access' => 'core/manage.template.add'],
        'core.template.update'=>['type' => '|', 'access' => 'core/manage.template.edit'],
        'core.template.edit'=>['type' => '|', 'access' => 'core/manage.template.edit'],
        'core.template.delete'=>['type' => '|', 'access' => 'core/manage.template.delete'],
        'core.feature.index'=>['type' => '|', 'access' => 'core/manage.feature'],
        'core.feature.create'=>['type' => '|', 'access' => 'core/manage.feature.add'],
        'core.feature.save'=>['type' => '|', 'access' => 'core/manage.feature.add'],
        'core.feature.update'=>['type' => '|', 'access' => 'core/manage.feature.edit'],
        'core.feature.edit'=>['type' => '|', 'access' => 'core/manage.feature.edit'],
        'core.feature.delete'=>['type' => '|', 'access' => 'core/manage.feature.delete'],
        'core.feature.values.create'=>['type' => '|', 'access' => 'core/manage.feature.publish'],
        'core.feature.values.save'=>['type' => '|', 'access' => 'core/manage.feature.publish'],
        'core.feature.values.update'=>['type' => '|', 'access' => 'core/manage.feature.publish'],
        'core.feature.values.edit'=>['type' => '|', 'access' => 'core/manage.feature.publish'],
        'core.feature.values.delete'=>['type' => '|', 'access' => 'core/manage.feature.publish'],
        'core.feature.group.index'=>['type' => '|', 'access' => 'core/manage.feature.group'],
        'core.feature.group.create'=>['type' => '|', 'access' => 'core/manage.feature.group.add'],
        'core.feature.group.save'=>['type' => '|', 'access' => 'core/manage.feature.group.add'],
        'core.feature.group.update'=>['type' => '|', 'access' => 'core/manage.feature.group.edit'],
        'core.feature.group.edit'=>['type' => '|', 'access' => 'core/manage.feature.group.edit'],
        'core.feature.group.delete'=>['type' => '|', 'access' => 'core/manage.feature.group.delete'],
        'core.feature.group.json'=>['type' => '|', 'access' => 'core/manage.feature.group'],
        'core.config.instance.index'=>['type' => '|', 'access' =>  ['core/manage.instance.config']],
        'core.config.instance.save'=>['type' => '|', 'access' =>  ['core/manage.instance.config']],
        'core.config.instance.cache'=>['type' => '|', 'access' =>  ['core/manage.instance.config']],
        'core.filemanger'=>['type' => '|', 'access' =>  ['core/is.super.admin','core/is.admin']],
        'core.filemanger'=>['type' => '|', 'access' =>  ['core/is.super.admin','core/is.admin']],
    ],
];
