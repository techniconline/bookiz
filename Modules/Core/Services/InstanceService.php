<?php

namespace Modules\Core\Services;

use App\Http\Services\BaseService;
use Illuminate\Http\Request;
use Modules\Core\Models\Instance;

class InstanceService extends BaseService
{
    private $instance;
    private $instance_model;

    public function __construct()
    {
        $this->instance_model = new Instance();
    }

    public function save(Request $request)
    {
        if ($this->instance_model->fill($request->all())->messages()->isValid()) {
            if ($this->instance_model->save()) {
                $this->setActionTrue()->setData($this->instance_model)->setMessage(trans('core::messages.alert.save_success'));
            } else {
                $this->setActionFalse()->setMessage(trans('core::messages.alert.save_un_success'));
            }
        } else {
            $this->setActionFalse()->setMessage(trans('core::messages.alert.mis_data'))->setErrors($this->instance_model->errors);
        }
        return $this->setResponse();
    }


}
