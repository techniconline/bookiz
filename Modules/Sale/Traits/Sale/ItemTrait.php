<?php
namespace Modules\Sale\Models\Traits\Sale;

use BridgeHelper;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Course\CourseActivity;
use Modules\Entity\Models\Service\EntityRelationServices;

Trait ItemTrait
{

    public function getSaleItem($item_type, $item_id, $options = [])
    {
        $item = BridgeHelper::getCartHelper()->getItemManager()->getItem($item_type);
        if ($item) {
            $item->setItemId($item_id);
            $item->setOptions($options);
        }
        return $item;
    }

    /**
     * @return bool
     */
    public function getDataModel()
    {
        $model = get_uppercase_by_underscores($this->item_type);
        $model = 'get' . $model . 'Data';
        if (method_exists($this, $model)) {
            $data = $this->$model();
            return $data;
        }
        return false;
    }

    /**
     * @return array|null
     */
    public function getEntityRelationServicesData()
    {
        $model = new EntityRelationServices();
        $data = $model->active()->with('entity')->find($this->item_id);
        if ($data) {
            return [
                'image' => $data->image,
                'icon' => $data->icon,
                'url' => $data->entity->single_link,
            ];
        }
        return null;
    }

    /**
     * @return array|null
     */
    public function getCourseData()
    {
        $model = new Course();
        $data = $model->active()->find($this->item_id);
        if ($data) {
            return [
                'image' => $data->image,
                'url' => $data->url_single_course,
            ];
        }
        return null;
    }

    /**
     * @return array|null
     */
    public function getActivityData()
    {
        $model = new CourseActivity();
        $data = $model->active()->with('course')->whereHas('course', function ($q) {
            return $q->active();
        })->find($this->item_id);
        $course = $data ? $data->course : null;
        if ($course) {
            return [
                'image' => $course->image,
                'url' => $course->url_single_course,
            ];
        }
        return null;
    }

}