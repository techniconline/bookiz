<?php
namespace Modules\Sale\Models\Traits\Payment;

use BridgeHelper;
use Modules\Sale\Libraries\Payment\MasterPayment;
use Modules\Sale\Models\Payment;

Trait PaymentTrait{
    
    private $masterPayment=false;

    public function getMasterPayment(){
        if($this->masterPayment){
            return $this->masterPayment;
        }
        $this->masterPayment=new MasterPayment();
        return $this->masterPayment;
    }



    public function createOrderPayment($order,$options=[],$type='online'){
        $options["reference_code"] = $order->reference_code;
        $payment=new Payment;
        $payment->instance_id=$order->instance_id;
        $payment->user_id=$order->user_id;
        $payment->action_id=$order->id;
        $payment->action='order';
        $payment->amount=$order->payable_amount;
        $payment->currency_id=$order->currency_id;
        $payment->type=$type;
        $payment->bank=(isset($options['payment']))?$options['payment']:null;
        $payment->options=$options;
        $payment->description=trans('sale::sale.payment_details',['id'=>$order->id,'user_id'=>$order->user_id]);
        $payment->state=$this->getPaymentNewState();
        $payment->save();
        return true;
    }

}