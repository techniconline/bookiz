<?php
namespace Modules\Sale\Models\Traits\Payment;

use Illuminate\Support\Facades\DB;
use BridgeHelper;
use Modules\Sale\Models\Payment;
use Modules\Sale\ViewModels\Order\PaymentViewModel;

Trait PaymentCloseTrait{

    public function closeOpenedPayments($user_id=null,$states=null){
        if(is_null($user_id)){
            $user=BridgeHelper::getAccess()->getUser();
            $user_id=$user->id;
        }
        if(is_null($states)){
            $states=$this->getDefaultCanCloseStates();
        }
        $payments=Payment::with('order')
            ->where('user_id',$user_id)
            ->where('bank','zarinpal')
            ->whereNull('deleted_at')
            ->Where(function($q) use ($states){
                $q->Where(function($q) use ($states){
                    $q->whereIN('state',$states)->where('updated_at','<=',DB::raw("SUBTIME(NOW(),'00:00:15')"));
                })
                ->orWhere(function($q){
                    $q->whereIN('state',['new','send'])->where('updated_at','<=',DB::raw("SUBTIME(NOW(),'00:11:00')"));
                });
            })
            ->get();
        if($payments && $payments->count()){
            try{
                foreach($payments as $payment){
                    $viewModel=new PaymentViewModel();
                    $viewModel->setOrderBoot($payment->order->id,$payment->order->reference_code)->setPayment($payment)->getClosePayment();
                }
            }catch (\Exception $e){
                report($e);
            }
        }
        return $this;
    }


    public function getDefaultCanCloseStates(){
        return ['back','success'];
    }
}