<?php

namespace Modules\Sale\Models\Traits\Payment;


use Modules\Core\Models\Currency\Currency;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;

Trait PaymentModelTrait
{

    use PaymentStatesTrait;


    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterCompleted($query)
    {
        return $query->whereIn('state', $this->getPaymentSuccessStates());
    }

    /**
     * @return mixed
     */
    public function getCostAttribute()
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->amount, $this->currency_id, true);
    }

    /**
     * @return mixed
     */
    public function getCreatedAtByMiniFormatAttribute()
    {
        $value = $this->created_at;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.medium'));
        }
        return $value;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getTypeTextAttribute()
    {
        if ($this->type) {
            return trans('sale::payment.types.' . $this->type);
        }
        return null;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getStatusTextAttribute()
    {
        if ($this->type) {
            return trans('sale::payment.states.' . $this->state);
        }
        return null;
    }

    public function getOptionsAttribute()
    {
        if (!$this->attributes['options']) {
            return new \stdClass();
        }
        return json_decode($this->attributes['options']);
    }

    public function getOptionsArrayAttribute()
    {
        if (!$this->attributes['options']) {
            return [];
        }
        return json_decode($this->attributes['options'], true);
    }

    public function setOptionsAttribute($val)
    {
        if (!is_array($val) && !is_object($val)) {
            $this->attributes['options'] = $val;

        } else {
            $this->attributes['options'] = json_encode($val);
        }
    }


    public function getInfoAttribute()
    {
        if (!$this->attributes['info']) {
            return new \stdClass();
        }
        return json_decode($this->attributes['info']);
    }

    public function getInfoArrayAttribute()
    {
        if (!$this->attributes['info']) {
            return [];
        }
        return json_decode($this->attributes['info'], true);
    }

    public function setInfoAttribute($val)
    {
        if (!is_array($val) && !is_object($val)) {
            $this->attributes['info'] = $val;
        } else {
            $this->attributes['info'] = @json_encode($val);
        }
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('Modules\Sale\Models\Order', 'action_id');
    }

    public function getActionModel()
    {
        if ($this->action && method_exists($this, $this->action)) {
            return $this->{$this->action}()->first();
        }
        return null;
    }


    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

}