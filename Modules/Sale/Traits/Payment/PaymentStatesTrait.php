<?php
namespace Modules\Sale\Models\Traits\Payment;

use BridgeHelper;
use Modules\Sale\Libraries\Payment\MasterPayment;

Trait PaymentStatesTrait{

    public function getPaymentSuccessStates(){
        return ['complete','success'];
    }

    public function getPaymentSuccessState(){
        return 'success';
    }

    public function getPaymentErrorState(){
        return 'error';
    }

    public function getPaymentFailState(){
        return 'fail';
    }

    public function getPaymentCancelState(){
        return 'cancel';
    }

    public function getPaymentCompleteState(){
        return 'complete';
    }

    public function getPaymentNewState(){
        return 'new';
    }

    public function getPaymentBackState(){
        return 'back';
    }

    public function getPaymentSendState(){
        return 'send';
    }

    public function canRePay($order=null){
        if(is_null($order)){
            $order=$this->order;
        }
        if(!$order){
            return false;
        }
        if(in_array($order->state,$this->getOrderRepayStates()) && $order->payable_amount > 0){
            $now=time()-86400;
            $orderTime=strtotime($order->created_at);
            if($orderTime>=$now){
                return true;
            }
        }
        return false;
    }

}