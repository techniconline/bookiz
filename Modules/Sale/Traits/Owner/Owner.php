<?php

namespace Modules\Sale\Traits\Owner;

use Modules\Core\Models\Currency\Currency;
use Modules\Core\Models\Instance\Instance;

trait Owner
{

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getStatusTextAttribute()
    {
        return trans('sale::owner.statuses.' . $this->active);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo(Instance::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ownerItems()
    {
        return $this->hasMany(\Modules\Sale\Models\Owner\OwnerItem::class);
    }
}
