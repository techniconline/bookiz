<?php

namespace Modules\Sale\Traits\Owner;

use Modules\Core\Models\Currency\Currency;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;

trait OwnerItem
{

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getStatusTextAttribute()
    {
        return trans('sale::owner.statuses.' . $this->active);
    }

    /**
     * @return null
     */
    public function getItemDataAttribute()
    {
        if ($this->model_id && $this->model_type){
            $method = 'get'.get_uppercase_by_underscores($this->model_type).'Model';
            if (method_exists($this, $method)){
                return $this->{$method}()->find($this->model_id);
            }
        }
        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(\Modules\Sale\Models\Owner\Owner::class);
    }

    /**
     * @return mixed
     */
    public function getCoursesModel()
    {
        return BridgeHelper::getCourse()->getCourseModel();
    }

}
