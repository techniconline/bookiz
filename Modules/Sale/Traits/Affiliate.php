<?php

namespace Modules\Sale\Traits;

use Modules\Core\Models\Instance\Instance;
use Modules\Sale\Models\Affiliate\AffiliateCode;
use Modules\Sale\Models\Affiliate\AffiliateCodeUse;
use Modules\Sale\Models\Order\OrderPromotions;
use Modules\User\Models\User;

trait Affiliate
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo(Instance::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function orderPromotion()
    {
        return $this->hasOne(OrderPromotions::class, 'type_id')->where('type', 'affiliate');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function orderPromotions()
    {
        return $this->hasMany(OrderPromotions::class, 'type_id')->where('type', 'affiliate');
    }

}
