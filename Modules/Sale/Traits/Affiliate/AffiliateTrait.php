<?php

namespace Modules\Sale\Traits\Affiliate;

use Modules\Course\Models\Course\CourseCategory;
use Modules\Sale\Models\Affiliate;

trait AffiliateTrait
{

    private $user_id;
    private $percent;
    private $price;
    private $payment_amount;
    private $affiliate_amount;
    private $origin_payment_amount;
    private $max_value;
    private $owner_value;
    private $response_action = [];
    public $is_valid = true;

    /**
     * @param array $configs
     * @return $this
     */
    public function setConfigs(array $configs)
    {
        foreach ($configs as $key => $config) {
            $this->{$key} = $config;
        }
        return $this->setResponseAction();
    }

    /**
     * @param array $res
     * @return $this
     */
    private function setResponseAction(array $res = [])
    {
        $this->response_action = array_merge($res, $this->response_action);
        return $this;
    }

    /**
     * @param $payment_amount
     * @return $this
     */
    public function initAffiliate($payment_amount)
    {

        if($affiliate_id = $this->cart->affiliate_id){
            $affiliate = Affiliate::active()->find($affiliate_id);
            $this->affiliate = $affiliate;
        }

        if ($this->affiliate) {
            $this->checkValidation();
            $this->payment_amount = $payment_amount;
            $this->origin_payment_amount = $payment_amount;
            $this->calcAffiliate();
        } else {
            $this->is_valid = false;
            $this->error = trans('sale::message.not_find_affiliate');
        }
        return $this;
    }

    /**
     * @return $this
     */
    private function checkValidation()
    {
        if(!$this->affiliate){
            $this->is_valid = false;
            return $this;
        }

//        if($this->affiliate->user_id == $this->cart->user_id){
//            $this->is_valid = false;
//            $this->error = trans('sale::message.now_valid_user');
//            return $this;
//        }

        $expire_date = $this->affiliate->expire_date;
        $active_date = $this->affiliate->active_date;

        if ($active_date) {
            $timeStart = strtotime($active_date);
            if ($timeStart > time()) {
                $this->is_valid = false;
                $this->error = trans('sale::message.now_valid_date');
            }

        }

        if ($expire_date) {
            $timeEnd = strtotime($expire_date);
            if ($timeEnd < time()) {
                $this->is_valid = false;
                $this->error = trans('sale::message.now_valid_date');
            }

        }
        return $this;
    }

    /**
     * @return $this
     */
    private function calcAffiliate()
    {
        if (!$this->is_valid) {
            return $this;
        }

        if ($this->cart) {
            $this->calcCartAffiliate();
        } else {
            $this->is_valid = false;
        }
        return $this;
    }

    private function calcCartAffiliate()
    {
        if (!$this->is_valid) {
            return $this;
        }

        //check conditions TODO ...
        $conditions = $this->affiliate->conditions;


        // calc payment amount
        $type = $this->affiliate->type;
        $value = $this->affiliate->value;
        $this->max_value = $this->affiliate->max_value;
        $this->owner_value = $this->affiliate->owner_value;
        $this->calcValidProductAffiliate();

        if (!$value) {
            $this->is_valid = false;
            $this->error = trans('sale::message.has_not_value_affiliate');
        }

        if ($type == 'percent') {
            $this->percent = $value;
            $this->calcPercent();
        } else {
            $this->price = $value;
            $this->calcPrice();
        }


        return $this;
    }


    /**
     * @return $this
     */
    private function calcValidProductAffiliate()
    {
        //TODO .....
        $cartItems = $this->cart->cartItems;
        $modelCategory = new CourseCategory();
        $validCategories = $modelCategory->getChildesId(1)->toArray();
        $total_price = 0;
        foreach ($cartItems as $cartItem) {
            $item = $cartItem->getSaleItem($cartItem->item_type, $cartItem->item_id, $cartItem->options_array);
            $itemCategories = $item->getCategoryIds();
            $valid = false;
            foreach ($itemCategories as $itemCategory) {
                if (!$valid && in_array($itemCategory, $validCategories)) {
                    $valid = true;
                }
            }
            if ($valid) {
                $total_price += $cartItem->total_amount;
            }
        }
        $this->payment_amount = $total_price;
        return $this;
    }

    /**
     * @return $this
     */
    private function calcPercent()
    {
        if ($this->percent) {
            $amount = $this->payment_amount * (1 - ($this->percent / 100));
            $discount = $this->payment_amount * (($this->percent / 100));
            if ($this->max_value && $this->max_value >= $discount) {
                $this->payment_amount = $amount > 0 ? $amount : 0;
                $this->affiliate_amount = $discount;
            } elseif ($this->max_value && $this->max_value <= $discount) {
                $this->payment_amount = $this->payment_amount - $this->max_value;
                $this->message = trans('sale::message.max_value_message', ['max_value' => $this->max_value]);
                $this->affiliate_amount = $this->max_value;
            } else {
                $this->payment_amount = $amount > 0 ? $amount : 0;
                $this->affiliate_amount = $discount;
            }

            $this->cart->affiliate_id = $this->affiliate->id;
            $this->cart->affiliate_amount = $this->affiliate_amount;
            $this->changeCartItemsForAffiliate();

        }
        return $this;
    }

    /**
     * calculate by item
     */
    private function changeCartItemsForAffiliate()
    {
        $coupen_amount = $this->cart->affiliate_amount;
        $items = $this->cart->cartItems;
        $countItems = count($items);
        $sumItems = $items->sum('total_amount');
        $segT = 0;
        $counter = 0;
        foreach ($items as $item) {
            $counter++;
            if ($countItems == $counter){
                $seg = $coupen_amount - $segT;
            }else{
                $seg = ($item->total_amount * $coupen_amount) / $sumItems;
                $segT += $seg;
            }
            $item->discount_amount += $seg;
            $item->total_amount -= $seg;
        }
    }


    /**
     * @return $this
     */
    private function calcPrice()
    {
        if ($this->price) {
            $amount = $this->payment_amount - $this->price;
            $discount = $this->price > $this->payment_amount ? $this->payment_amount : $this->price;
            $this->payment_amount = $amount > 0 ? $amount : 0;
            $this->affiliate_amount = $discount;

            $this->cart->affiliate_id = $this->affiliate->id;
            $this->cart->affiliate_amount = $this->affiliate_amount;
            $this->changeCartItemsForAffiliate();
        }
        return $this;
    }

}
