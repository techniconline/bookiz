<?php

namespace Modules\Sale\Models\Traits\Cart;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;

Trait CartModelTrait
{

    /**
     * @return int
     */
    public function getDiscountAttribute()
    {
        $percent = 0;
        $items = $this->cartItems()->active()->get();
        if ($items->count()) {
            $percent = $items->sum('discount');
        }
        return $percent;
    }

    /**
     * @return float|int
     */
    public function getDiscountPercentAttribute()
    {
        try {
            $percent = (int)(abs(((($this->amount - $this->discount) * 100) / $this->amount) - 100));
        } catch (\Exception $exception) {
            return 0;
        }
        return $percent;
    }

    /**
     * @return mixed|null
     */
    public function getDataArrayAttribute()
    {
        if ($this->data) {
            return json_decode($this->data, true);
        }
        return null;
    }

    /**
     * @return array
     */
    public function getDiscountItemsAttribute()
    {
        return [];
    }

    /**
     * @return mixed
     */
    public function getDiscountTextAttribute()
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->discount, $this->currency_id, true);;
    }

    /**
     * @return mixed
     */
    public function getCoupenAmountTextAttribute()
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->coupen_amount, $this->currency_id, true);
    }

    /**
     * @return mixed
     */
    public function getCostAttribute()
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->amount, $this->currency_id, true);;
    }

    /**
     * @return mixed
     */
    public function getTotalAttribute()
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->payable_amount, $this->currency_id, true);;
    }

    /**
     * @return mixed
     */
    public function cartItems()
    {
        return $this->hasMany('Modules\Sale\Models\Cart\CartItem');
    }

}