<?php
namespace Modules\Sale\Models\Traits\Cart;
use BridgeHelper;
use Illuminate\Support\Facades\Cache;

Trait CartTrait{

    private $cartCacheTime=1440;


    public function getUserCartToken(){
        if(BridgeHelper::getAccess()->isLogin()) {
            $token = BridgeHelper::getAccess()->getUser()->id;
        }else{
            $token = $this->getUserToken();
        }
        return $token;
    }


    public function getCartInfoFromCache(){
        $key=$this->getCartCacheKey();
        if(Cache::has($key)){
            return Cache::get($key);
        }
        return null;
    }

    public function updateCartCache($cartInfo){
        $key=$this->getCartCacheKey();
        Cache::put($key,$cartInfo,$this->cartCacheTime);
        return $this;
    }

    public function getCartCacheKey(){
        $token=$this->getUserCartToken();
        return 'user_cart_token_'.$token;
    }

    public function getUserToken(){
        return  app('getInstanceObject')->getUserToken();
    }

}