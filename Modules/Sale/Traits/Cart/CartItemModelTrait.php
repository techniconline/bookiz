<?php
namespace Modules\Sale\Models\Traits\Cart;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Sale\Models\Traits\Sale\ItemTrait;

Trait CartItemModelTrait{

    use ItemTrait;
    private $item=false;
    /**
     * @return int
     */
    public function getDiscountAttribute()
    {
        return $this->discount_amount;
    }

    /**
     * @return float|int
     */
    public function getDiscountPercentAttribute()
    {
        try{
            $percent = (int)(abs(((($this->amount - $this->discount)*100)/$this->amount) - 100));
        }catch (\Exception $exception){
            return 0;
        }
        return $percent;
    }


    /**
     * @return array
     */
    public function getDiscountItemsAttribute()
    {
        return [];
    }

    /**
     * @return mixed|string
     */
    public function getImageAttribute()
    {
        $item=$this->getItem();
        if($item){
            $itemExtraData=$item->getItemExtraData();
            if($itemExtraData && isset($itemExtraData['image'])){
                return $itemExtraData['image'];
            }
        }
        return asset('/assets/img/noimage-course.jpg');
    }

    /**
     * @return mixed
     */
    public function getUrlItemAttribute()
    {
        $result = $this->getDataModel();
        $url = isset($result['url'])?$result['url']:null;
        return $url;
    }

    /**
     * @return mixed|string
     */
    public function getLinkAttribute()
    {
        $item=$this->getItem();
        if($item){
            $itemExtraData=$item->getItemExtraData();
            if($itemExtraData && isset($itemExtraData['url'])){
                return $itemExtraData['url'];
            }
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getDiscountTextAttribute()
    {
        return  BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->discount, $this->currency_id, true);;
    }

    /**
     * @return mixed
     */
    public function getCostAttribute()
    {
        return  BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->amount, $this->currency_id, true);;
    }

    /**
     * @return mixed
     */
    public function getTotalAttribute()
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->total_amount, $this->currency_id, true);;
    }

    public function getOptionsAttribute()
    {
        if(!$this->attributes['options']){
            return new \stdClass();
        }
        return json_decode($this->attributes['options']);
    }

    public function getOptionsArrayAttribute()
    {
        if(!$this->attributes['options']){
            return [];
        }
        return json_decode($this->attributes['options'],true);
    }

    public function setOptionsAttribute( $val )
    {
        if(!is_array($val) && !is_object($val)){
            $this->attributes['options'] =  $val ;

        }else{
            $this->attributes['options'] = json_encode( $val );
        }
    }


    public function getItem(){
        if($this->item && $this->item->isSameItem($this->item_type,$this->item_id)){
            return $this->item;
        }
        $this->item=$this->getSaleItem($this->item_type,$this->item_id,$this->options_array);
        return $this->item;
    }



}