<?php
namespace Modules\Sale\Models\Traits\Order;

use BridgeHelper;
use Modules\Sale\Models\Order\OrderMessages;

Trait OrderMessageTrait{

    public function setOrderMessage($order_id,$message,$user_id=null){
        if(empty($message)){
            return true;
        }

        if(!is_array($message)){
            $messages[] = $message;
        }else{
            $messages = $message;
        }

        foreach ($messages as $message) {
            $messageModel= new OrderMessages;
            $messageModel->message=$message;
            $messageModel->order_id=$order_id;
            if($user_id){
                $messageModel->user_id=$user_id;
            }
            $messageModel->save();
        }
        return true;
    }

    public function getOrderMessages($order_id){
        return OrderMessages::where('order_id',$order_id)->orderBy('id','DESC')->get();
    }

}