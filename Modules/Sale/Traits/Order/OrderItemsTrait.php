<?php

namespace Modules\Sale\Models\Traits\Order;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Traits\ModelTrait;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Course\CourseCategory;
use Modules\Course\Models\Course\CourseCategoryRelation;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Traits\Sale\ItemTrait;

Trait OrderItemsTrait
{

    use ItemTrait;
    use ModelTrait;
    private $item = false;

    /**
     * @return int
     */
    public function getDiscountAttribute()
    {
        return 0;
    }

    /**
     * @return mixed
     */
    public function getUrlItemAttribute()
    {
        $result = $this->getDataModel();
        $url = isset($result['url']) ? $result['url'] : null;
        return $url;
    }


    /**
     * @return float|int
     */
    public function getDiscountPercentAttribute()
    {
        try {
            $percent = (int)((($this->amount - $this->discount) * 100) / $this->amount) - 100;
        } catch (\Exception $exception) {
            return 0;
        }
        return $percent;
    }

    /**
     * @return mixed
     */
    public function getDiscountTextAttribute()
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->discount, $this->currency_id, true);;
    }

    /**
     * @return mixed
     */
    public function getCostAttribute()
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->amount, $this->currency_id, true);;
    }

    /**
     * @return mixed
     */
    public function getTotalAttribute()
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->attributes['total_amount'], $this->currency_id, true);
    }


    public function getOptionsAttribute()
    {
        if (!$this->attributes['options']) {
            return new \stdClass();
        }
        return json_decode($this->attributes['options']);
    }

    public function getOptionsArrayAttribute()
    {
        if (!$this->attributes['options']) {
            return [];
        }
        return json_decode($this->attributes['options'], true);
    }

    public function setOptionsAttribute($val)
    {
        if (!is_array($val) && !is_object($val)) {
            $this->attributes['options'] = $val;

        } else {
            $this->attributes['options'] = json_encode($val);
        }
    }


    /**
     * @return mixed|string
     */
    public function getImageAttribute()
    {
        $item = $this->getItem();
        if ($item) {
            $itemExtraData = $item->getItemExtraData();
            if ($itemExtraData && isset($itemExtraData['image'])) {
                return $itemExtraData['image'];
            }
        }
        return asset('/assets/img/noimage-course.jpg');
    }

    /**
     * @return mixed|string
     */
    public function getLinkAttribute()
    {
        $item = $this->getItem();
        if ($item) {
            $itemExtraData = $item->getItemExtraData();
            if ($itemExtraData && isset($itemExtraData['url'])) {
                return $itemExtraData['url'];
            }
        }
        return null;
    }


    /**
     * @return bool
     */
    public function getItem()
    {
        if ($this->item) {
            return $this->item;
        }
        $this->item = $this->getSaleItem($this->item_type, $this->item_id, $this->options_array);
        return $this->item;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | null
     */
    public function getItemRowAttribute()
    {
        $class = $this->getClassObject($this->item_type);

        if (!$class) {
            return null;
        }

        return $this->getBelongsTo($class);
    }

    /**
     * @return null
     */
    public function getItemExtraDataAttribute()
    {
        $item = $this->getItem();
        if ($item){
            return $item->getItemExtraData();
        }
        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courseCategories()
    {
        return $this->hasMany(CourseCategoryRelation::class, 'course_id', 'item_id')->active();
    }

    /**
     * @param $class
     * @return null
     */
    public function getBelongsTo($class)
    {
        try{
            $class_name = class_basename($class);
            return $this->{"get".$class_name."Belong"}($class);
        }catch (\Exception $exception){
            report($exception);
            return null;
        }
    }

    /**
     * @param $class
     * @return mixed
     */
    public function getBookingDetailsBelong($class)
    {
        return $this->belongsTo($class, 'item_id')->first();
    }

    /**
     * @param $class
     * @return mixed
     */
    public function getCourseBelong($class)
    {
        return $this->belongsTo($class, 'item_id')->first();
    }

    /**
     * @return string
     */
    public function getCourse()
    {
        return $this->getClassObject('course');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @param $value
     * @return string
     */
    public function getTotalAmountAttribute($value)
    {
        return number_format($value);
    }

    /**
     * @param $value
     * @return string
     */
    public function getTotalSumAmountAttribute($value)
    {
        return number_format($value);
    }

    /**
     * @param $value
     * @return string
     */
    public function getTotalDiscountAmountAttribute($value)
    {
        return number_format($value);
    }

}