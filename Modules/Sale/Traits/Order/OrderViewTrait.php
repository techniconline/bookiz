<?php

namespace Modules\Sale\Models\Traits\Order;

use BridgeHelper;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Payment;

Trait OrderViewTrait
{

    public function getOrderById($order_id)
    {
        $order = Order::with('orderItems')->find($order_id);
        return $order;
    }

    public function getOrderByUser($order_id)
    {
        $user = BridgeHelper::getAccess()->getUser();
        $order = Order::with('orderItems')->where('id', $order_id)->where('user_id', $user->id)->first();
        return $order;
    }

    /**
     * @param $order_id
     * @param $reference_code
     * @return $this|\Illuminate\Database\Eloquent\Model|mixed|null|object|static
     */
    public function getOrderByReference($order_id, $reference_code = null)
    {
        $order = Order::with('user','orderItems', 'orderPayment', 'orderPayments','orderPromotions', 'orderMessages')->where('id', $order_id)
            ->where(function ($q) use ($reference_code) {
                if (($user = BridgeHelper::getAccess()->getUser()) && !BridgeHelper::getAccess()->isAdmin() && !BridgeHelper::getAccess()->isOwnerEntity($user)) {
                    $q = $q->where('user_id', $user->id);
                } else {
                    $q = $q->where('reference_code', $reference_code);
                }
                return $q;
            })->first();
        return $order;
    }

    public function getOrderCurrentPayment(Order $order, $status = 'new')
    {
        return Payment::where('type', 'online')->where('state', $status)->where('action', 'order')->where('action_id', $order->id)->orderBy('id', 'DESC')->first();
    }

    public function getOrderLastPayment($order_id)
    {
        return Payment::where('action', 'order')->where('action_id', $order_id)->orderBy('id', 'DESC')->first();
    }

    public function getOrderPayments($order_id)
    {
        return Payment::where('action', 'order')->where('action_id', $order_id)->orderBy('id', 'DESC')->get();
    }

}