<?php

namespace Modules\Sale\Models\Traits\Order;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Sale\Models\Payment;
use Modules\Sale\Models\Traits\Payment\OrderStatesTrait;

Trait OrderModelTrait
{

    use OrderStatesTrait;

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterCompleted($query)
    {
        return $query->whereIn('state', $this->getOrderCompleteStates());
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterNotCompleted($query)
    {
        return $query->whereIn('state', $this->getOrderRepayStates());
    }

    /**
     * @return int
     */
    public function getDiscountAttribute()
    {
        return 0;
    }

    /**
     * @return float|int
     */
    public function getDiscountPercentAttribute()
    {
        try {
            $percent = (int)((($this->amount - $this->discount) * 100) / $this->amount) - 100;
        } catch (\Exception $exception) {
            return 0;
        }
        return $percent;
    }

    /**
     * @return mixed
     */
    public function getDiscountTextAttribute()
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->discount, $this->currency_id, true);
    }

    /**
     * @return mixed
     */
    public function getCostAttribute()
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->amount, $this->currency_id, true);
    }

    /**
     * @return mixed
     */
    public function getTotalAttribute()
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->payable_amount, $this->currency_id, true);
    }

    /**
     * @return mixed
     */
    public function getCreatedAtByMiniFormatAttribute()
    {
        $value = $this->created_at;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.medium'));
        }
        return $value;
    }

    /**
     * @return null
     */
    public function getStateTextAttribute()
    {
        if ($value = $this->state) {
            return trans('sale::sale.order.states.' . $value);
        }
        return null;
    }

    /**
     * @return string
     */
    public function getReferenceCode()
    {
        $value = "INS";
        $value .= $this->instance_id * 3;
        $value .= "-" . strtoupper(str_random(10));
        return $value;
    }

    public function getHasSuccessPaymentAttribute()
    {
        $payment = Payment::where('action', 'order')->where('action_id', $this->id)->whereIn('state', $this->getPaymentSuccessStates())->first();
        if ($payment) {
            return true;
        }
        return false;
    }

    public function getSingleUrlAttribute()
    {
        return route("sale.order.view", ["order_id" => $this->id,"reference_code" => $this->reference_code]);
    }

    /**
     * @return mixed
     */
    public function orderItems()
    {
        return $this->hasMany('Modules\Sale\Models\Order\OrderItems');
    }

    /**
     * @return mixed
     */
    public function orderMessages()
    {
        return $this->hasMany('Modules\Sale\Models\Order\OrderMessages');
    }

    /**
     * @return mixed
     */
    public function orderPayments()
    {
        return $this->hasMany('Modules\Sale\Models\Payment', 'action_id')->where('action', 'order');
    }

    /**
     * @return mixed
     */
    public function orderPayment()
    {
        return $this->hasOne('Modules\Sale\Models\Payment', 'action_id')->where('action', 'order')->orderBy('id', 'DESC');
    }

    /**
     * @return mixed
     */
    public function orderPaymentSuccess()
    {
        return $this->hasOne('Modules\Sale\Models\Payment', 'action_id')->filterCompleted()->where('action', 'order')->orderBy('id', 'DESC');
    }

    /**
     * @return mixed
     */
    public function orderPromotions()
    {
        return $this->hasMany('Modules\Sale\Models\Order\OrderPromotions', 'order_id');
    }

    /**
     * @return mixed
     */
    public function orderPromotion()
    {
        return $this->hasOne('Modules\Sale\Models\Order\OrderPromotions', 'order_id')->active()->orderBy('id', "DESC");
    }

    /**
     * @return mixed
     */
    public function orderPromotionVoucher()
    {
        return $this->hasOne('Modules\Sale\Models\Order\OrderPromotions', 'order_id')->active()->where('type', 'coupen')->orderBy('id', "DESC");
    }

    /**
     * @return mixed
     */
    public function orderPromotionAffiliate()
    {
        return $this->hasOne('Modules\Sale\Models\Order\OrderPromotions', 'order_id')->active()->where('type', 'affiliate')->orderBy('id', "DESC");
    }

    /**
     * @return mixed
     */
    public function orderPromotionPro()
    {
        return $this->hasOne('Modules\Sale\Models\Order\OrderPromotions', 'order_id')->active()->where('type', 'promotion')->orderBy('id', "DESC");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Models\User');
    }
}