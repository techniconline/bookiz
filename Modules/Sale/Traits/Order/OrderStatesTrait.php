<?php
namespace Modules\Sale\Models\Traits\Payment;

use BridgeHelper;
use Modules\Sale\Libraries\Payment\MasterPayment;

Trait OrderStatesTrait{

    public function getOrderCompleteStates(){
        return ['complete'];
    }

    public function getOrderRepayStates(){
        return ['new','payment','cancel'];
    }

    public function getOrderCompleteState(){
        return 'complete';
    }
    public function getOrderCancelState(){
        return 'cancel';
    }

    public function getOrderNewState(){
        return 'new';
    }
    public function getOrderPaymentState(){
        return 'payment';
    }

    public function getOrderShipmentState(){
        return 'shipment';
    }
    public function getOrderReviewState(){
        return 'review';
    }

}