<?php

namespace Modules\Sale\Traits\Voucher;

use Modules\Course\Models\Course\CourseCategory;
use Modules\Sale\Models\Cart;
use Modules\Sale\Models\Order\OrderPromotions;
use Modules\Sale\Models\Voucher\Voucher;
use Modules\Sale\Models\Voucher\VoucherCodes;

trait VoucherTrait
{

    private $percent_voucher;
    private $price_voucher;
    private $payment_amount;
    private $voucher_amount;
    private $origin_payment_amount;
    private $max_value;
    private $max_qty;
    private $response_action = [];
    public $is_valid = true;

    /**
     * @param array $configs
     * @return $this
     */
    public function setVoucherConfigs(array $configs)
    {
        foreach ($configs as $key => $config) {
            $this->{$key} = $config;
        }
        return $this->setResponseActionVoucher();
    }

    /**
     * @param array $res
     * @return $this
     */
    private function setResponseActionVoucher(array $res = [])
    {
        $this->response_action = array_merge($res, $this->response_action);
        return $this;
    }

    /**
     * @param $payment_amount
     * @return $this
     */
    public function initVoucher($payment_amount)
    {

        if ($voucher_code_id = $this->cart->coupen) {
            $voucher_code = VoucherCodes::active()->with(['voucher'])->find($voucher_code_id);
            $this->voucher_code = $voucher_code;
        }

        if ($this->voucher_code) {
            $this->checkValidationVoucher();
            $this->payment_amount = $payment_amount;
            $this->origin_payment_amount = $payment_amount;
            $this->calcVoucher();
        } else {
            $this->is_valid = false;
            $this->error = trans('sale::message.not_find_voucher');
        }
        return $this;
    }

    /**
     * @return $this
     */
    private function checkValidationVoucher()
    {
        if (!$this->voucher_code) {
            $this->is_valid = false;
            return $this;
        }

//        if($this->voucher->user_id == $this->cart->user_id){
//            $this->is_valid = false;
//            $this->error = trans('sale::message.now_valid_user');
//            return $this;
//        }

        return $this->checkDateValidate()->checkUseUser()->checkMaxQty();
    }

    private function checkDateValidate()
    {
        $this->max_qty = $this->voucher_code->voucher->max_qty;
        $expire_date = $this->voucher_code->active_time; // this date is expire date
        if ($expire_date) {
            $timeEnd = strtotime($expire_date);
            if ($timeEnd < time()) {
                $this->is_valid = false;
                $this->error = trans('sale::message.now_valid_date');
            }

        }
        return $this;
    }

    /**
     * @return $this
     */
    private function checkUseUser()
    {
        $user_id = $this->cart->user_id;
        if (!$user_id) {
            $this->is_valid = false;
            $this->error = trans('sale::message.not_find_user');
        }

        $countUse = OrderPromotions::where('user_id', $user_id)->where('type', OrderPromotions::TYPE_COUPEN)->where('type_id', $this->voucher_code->id)->count();
        $use_type = $this->voucher_code->voucher->use_type;
        if ($use_type == 'unlimited') {
            return $this;
        } elseif ($use_type == 'disposable_per_user') {
            if ($countUse) {
                $this->is_valid = false;
                $this->error = trans('sale::message.not_allow_voucher_code');
            }
        } elseif ($use_type == 'disposable_per_use_qty') {
            if ($countUse >= $this->voucher_code->voucher->use_qty) {
                $this->is_valid = false;
                $this->error = trans('sale::message.not_allow_voucher_code');
            }
        } else {
            $this->is_valid = false;
            $this->error = trans('sale::message.not_allow_voucher_code');
        }

        return $this;
    }

    /**
     * @return $this
     */
    private function checkMaxQty()
    {
        $max_qty = $this->voucher_code->voucher->max_qty;
        if (!$max_qty) {
            return $this;
        }
        $used_qty = $this->voucher_code->used_qty;
        if ($used_qty >= $max_qty) {
            $this->voucher_code->active = 0;
            $this->voucher_code->save();
            $this->is_valid = false;
            $this->error = trans('sale::message.not_allow_voucher_code');
        }
        return $this;
    }

    /**
     * @return $this
     */
    private function calcVoucher()
    {
        if (!$this->is_valid) {
            return $this;
        }

        if ($this->cart) {
            $this->calcCartVoucher();
        } else {
            $this->is_valid = false;
        }
        return $this;
    }

    /**
     * @return $this
     */
    private function calcCartVoucher()
    {
        if (!$this->is_valid) {
            return $this;
        }

        //check conditions TODO ...
        $conditions = isset($this->voucher_code->voucher->conditions) ? $this->voucher_code->voucher->conditions : null;


        // calc payment amount
        $type = $this->voucher_code->voucher->type;
        $value = $this->voucher_code->voucher->value;
        $this->max_value = $this->voucher_code->voucher->max_value;
//        $this->calcValidProductVoucher();

        if (!$value) {
            $this->is_valid = false;
            $this->error = trans('sale::message.has_not_value_affiliate');
        }

        if ($type == 'percent') {
            $this->percent_voucher = $value;
            $this->calcPercentVoucher();
        } else {
            $this->price_voucher = $value;
            $this->calcPriceVoucher();
        }


        return $this;
    }

    /**
     * @return $this
     */
    private function calcValidProductVoucher()
    {
        //TODO .....
        $cartItems = $this->cart->cartItems;
        $modelCategory = new CourseCategory();
        $validCategories = $modelCategory->getChildesId(1)->toArray();
        $total_price = 0;
        foreach ($cartItems as $cartItem) {
            $item = $cartItem->getSaleItem($cartItem->item_type, $cartItem->item_id, $cartItem->options_array);
            $itemCategories = $item->getCategoryIds();
            $valid = false;
            foreach ($itemCategories as $itemCategory) {
                if (!$valid && in_array($itemCategory, $validCategories)) {
                    $valid = true;
                }
            }
            if ($valid) {
                $total_price += $cartItem->total_amount;
            }
        }
        $this->payment_amount = $total_price;
        return $this;
    }

    /**
     * @return $this
     */
    private function calcPercentVoucher()
    {
        if ($this->percent_voucher) {
            $amount = $this->payment_amount * (1 - ($this->percent_voucher / 100));
            $discount = $this->payment_amount * (($this->percent_voucher / 100));
            if ($this->max_value && $this->max_value >= $discount) {
                $this->payment_amount = $amount > 0 ? $amount : 0;
                $this->voucher_amount = $discount;
            } elseif ($this->max_value && $this->max_value <= $discount) {
                $this->payment_amount = $this->payment_amount - $this->max_value;
                $this->message = trans('sale::message.max_value_message', ['max_value' => $this->max_value]);
                $this->voucher_amount = $this->max_value;
            } else {
                $this->payment_amount = $amount > 0 ? $amount : 0;
                $this->voucher_amount = $discount;
            }
            $this->cart->coupen = $this->voucher_code->id;
            $this->cart->coupen_amount = $this->voucher_amount;
            $this->changeCartItemsForVoucher();
        }
        return $this;
    }

    /**
     *
     */
    private function changeCartItemsForVoucher()
    {
        $coupen_amount = $this->cart->coupen_amount;
        $items = $this->cart->cartItems;
        $countItems = count($items);
        $sumItems = $items->sum('total_amount');
        $segT = 0;
        $counter = 0;
        foreach ($items as $item) {
            $counter++;
            if ($countItems == $counter){
                $seg = $coupen_amount - $segT;
            }else{
                $seg = ($item->total_amount * $coupen_amount) / $sumItems;
                $segT += $seg;

            }
            $item->discount_amount += $seg;
            $item->total_amount -= $seg;
        }
    }

    /**
     * @return $this
     */
    private function calcPriceVoucher()
    {
        if ($this->price_voucher) {
            $amount = $this->payment_amount - $this->price_voucher;
            $discount = $this->price_voucher > $this->payment_amount ? $this->payment_amount : $this->price_voucher;
            $this->payment_amount = $amount > 0 ? $amount : 0;
            $this->voucher_amount = $discount;

            $this->cart->coupen = $this->affiliate->id;
            $this->cart->coupen_amount = $this->voucher_amount;
            $this->changeCartItemsForVoucher();
        }
        return $this;
    }

}
