<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangePaymentTable extends Migration
{

    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->mediumText("info")->nullable()->after('state')->comment('callback info json');
        });

    }

    public function down()
    {

        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn(['info']);
        });

    }
}