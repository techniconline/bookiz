<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	public function up()
	{
		Schema::create('orders', function(Blueprint $table) {
			$table->increments('id');
            $table->integer('instance_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
			$table->enum('state', array('new', 'payment', 'review', 'shipment', 'complete', 'cancel'))->index();
			$table->float('amount', 12,2)->default('0.00');
			$table->float('payable_amount', 12,2)->default('0.00');
            $table->unsignedInteger("currency_id")->index();
            $table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('orders');
	}
}