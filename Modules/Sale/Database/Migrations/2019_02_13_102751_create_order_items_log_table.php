<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderItemsLogTable extends Migration {

	public function up()
	{
		Schema::create('order_items_log', function(Blueprint $table) {
			$table->increments('id');
            $table->unsignedInteger("order_id");
            $table->unsignedInteger("order_item_id");
            $table->unsignedInteger("item_id")->index();
            $table->string("item_type")->index();
            $table->unsignedInteger("instance_id");
            $table->unsignedInteger("user_id");
            $table->unsignedInteger("currency_id")->nullable();
            $table->double("amount")->nullable();
            $table->double("discount")->nullable();
            $table->double("total_amount")->nullable();
            $table->unsignedInteger("payment_id")->nullable();
            $table->double("payment_amount")->nullable();
            $table->unsignedInteger("order_promotion_id")->nullable();
            $table->double("promotion_amount")->nullable();
            $table->string("bank")->nullable();
            $table->unsignedTinyInteger('active')->deafult(1)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});

        Schema::table('order_items_log', function(Blueprint $table) {

            $table->foreign('order_id')->references('id')->on('orders')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('order_item_id')->references('id')->on('order_items')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('payment_id')->references('id')->on('payments')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('order_promotion_id')->references('id')->on('order_promotions')
                ->onDelete('cascade')
                ->onUpdate('cascade');

//            $table->foreign('currency_id')->references('id')->on('currencies')
//                ->onDelete('cascade')
//                ->onUpdate('cascade');

        });

	}

	public function down()
	{
        Schema::table('order_items_log', function(Blueprint $table) {
            $table->dropForeign('order_items_log_order_id_foreign');
            $table->dropForeign('order_items_log_order_item_id_foreign');
            $table->dropForeign('order_items_log_instance_id_foreign');
            $table->dropForeign('order_items_log_user_id_foreign');
            $table->dropForeign('order_items_log_payment_id_foreign');
            $table->dropForeign('order_items_log_order_promotion_id_foreign');
//            $table->dropForeign('order_items_log_currency_id_foreign');
        });
		Schema::drop('order_items_log');
	}
}