<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangePaymentIndexField extends Migration
{

    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->string("bank",50)->index()->nullable()->after('type');
            $table->index('request_id');
            $table->index('reference_id');
        });
    }

    public function down()
    {


    }
}