<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOwnerCalculateLogTable extends Migration {

	public function up()
	{
		Schema::create('owner_calculate_log', function(Blueprint $table) {
			$table->increments('id');
            $table->unsignedInteger("owner_id");
            $table->unsignedInteger("model_id")->index();
            $table->string("model_type")->index();
            $table->enum('type',['minus', 'plus'])->default('plus')->index();
            $table->float('amount',14,2)->nullable();
            $table->float('current_credit',16,2)->nullable();
            $table->unsignedInteger("currency_id");
            $table->unsignedTinyInteger('active')->deafult(1)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});

        Schema::table('owner_calculate_log', function(Blueprint $table) {

            $table->foreign('owner_id')->references('id')->on('owners')
                ->onDelete('cascade')
                ->onUpdate('cascade');

//            $table->foreign('currency_id')->references('id')->on('currencies')
//                ->onDelete('cascade')
//                ->onUpdate('cascade');

        });

	}

	public function down()
	{
        Schema::table('owner_calculate_log', function(Blueprint $table) {
            $table->dropForeign('owner_calculate_log_owner_id_foreign');
//            $table->dropForeign('owner_calculate_log_currency_id_foreign');
        });
		Schema::drop('owner_calculate_log');
	}
}