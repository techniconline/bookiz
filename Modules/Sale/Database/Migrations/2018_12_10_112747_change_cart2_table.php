<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangeCart2Table extends Migration
{

    public function up()
    {
        Schema::table('cart', function (Blueprint $table) {
            $table->text("data")->nullable()->after('currency_id')->comment('is json');
        });

    }

    public function down()
    {

        Schema::table('cart', function (Blueprint $table) {
            $table->dropColumn(['data']);
        });

    }
}