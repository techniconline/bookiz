<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVoucherCodeUsesTable extends Migration {

	public function up()
	{
		Schema::create('voucher_code_uses', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('voucher_code_id')->unsigned()->index();
            $table->unsignedInteger('user_id')->index();
			$table->timestamps();
			$table->softDeletes();
		});
        Schema::table('voucher_code_uses', function(Blueprint $table) {
            $table->foreign('voucher_code_id')->references('id')->on('voucher_codes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
	}

	public function down()
	{
        Schema::table('voucher_code_uses', function(Blueprint $table) {
            $table->dropForeign('voucher_code_uses_voucher_code_id_foreign');
        });
		Schema::drop('voucher_code_uses');
	}
}