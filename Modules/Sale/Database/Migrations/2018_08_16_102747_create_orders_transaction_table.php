<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTransactionTable extends Migration {

	public function up()
	{
		Schema::create('payments', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('instance_id')->unsigned()->index();
			$table->integer('user_id')->unsigned()->index();
			$table->integer('action_id')->unsigned()->index();
			$table->string('action',25)->index();
			$table->float('amount', 12,2)->default('0.00');
            $table->unsignedInteger("currency_id")->index();
            $table->enum('type', array('online', 'cart2cart', 'bank', 'cash',))->index();
            $table->text('options')->nullable();
            $table->string('request_id',150)->nullable();
            $table->string('reference_id',150)->nullable();
            $table->string('description',255)->nullable();
            $table->enum('state', array('new', 'send','error', 'fail', 'cancel', 'success', 'complete'))->index();
            $table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('payments');
	}
}