<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCartItemsTable extends Migration {

	public function up()
	{
		Schema::create('cart_items', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('cart_id')->unsigned()->index();
			$table->string('name', 255);
			$table->string('item_type', 30)->index();
			$table->integer('item_id')->unsigned()->index();
			$table->text('options')->nullable();
			$table->integer('qty')->unsigned()->index();
			$table->float('amount', 10,2);
			$table->float('total_amount', 10,2);
            $table->unsignedInteger("currency_id")->index();
            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('cart_items');
	}
}