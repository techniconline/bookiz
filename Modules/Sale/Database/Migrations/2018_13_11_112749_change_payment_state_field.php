<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangePaymentStateField extends Migration
{

    public function up()
    {
        $sql="ALTER TABLE `payments` CHANGE `state` `state` ENUM('new','send','back','error','fail','cancel','success','complete') NOT NULL;";
        DB::statement($sql);
    }

    public function down()
    {


    }
}