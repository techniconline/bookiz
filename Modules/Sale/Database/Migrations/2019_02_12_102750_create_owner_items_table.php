<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOwnerItemsTable extends Migration {

	public function up()
	{
		Schema::create('owner_items', function(Blueprint $table) {
			$table->increments('id');
            $table->unsignedInteger("owner_id");
            $table->enum('type', array('percent', 'fixed'))->nullable()->default('percent')->index();
            $table->float('value', 8,2)->nullable()->default('0.00');
            $table->float('max_value', 10,2)->nullable();
            $table->unsignedInteger("model_id")->index();
            $table->string("model_type")->index();
            $table->unsignedInteger("currency_id");
            $table->unsignedTinyInteger('active')->deafult(1)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});

        Schema::table('owner_items', function(Blueprint $table) {

            $table->foreign('owner_id')->references('id')->on('owners')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('currency_id')->references('id')->on('currencies')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

	}

	public function down()
	{
        Schema::table('owner_items', function(Blueprint $table) {
            $table->dropForeign('owner_items_owner_id_foreign');
            $table->dropForeign('owner_items_currency_id_foreign');

        });
		Schema::drop('owner_items');
	}
}