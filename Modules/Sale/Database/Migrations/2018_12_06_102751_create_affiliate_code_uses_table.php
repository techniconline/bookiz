<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAffiliateCodeUsesTable extends Migration {

	public function up()
	{
		Schema::create('affiliate_code_uses', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('affiliate_id')->unsigned()->index();
			$table->morphs('model');
			$table->float('amount',14,2)->nullable();
            $table->unsignedInteger('user_id')->index();
			$table->timestamps();
			$table->softDeletes();
		});
        Schema::table('affiliate_code_uses', function(Blueprint $table) {

            $table->foreign('affiliate_id')->references('id')->on('affiliates')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
	}

	public function down()
	{
        Schema::table('affiliate_code_uses', function(Blueprint $table) {
            $table->dropForeign('affiliate_code_uses_affiliate_id_foreign');
            $table->dropForeign('affiliate_code_uses_user_id_foreign');
        });
		Schema::drop('affiliate_code_uses');
	}
}