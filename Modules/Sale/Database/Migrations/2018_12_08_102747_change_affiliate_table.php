<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangeAffiliateTable extends Migration {

	public function up()
	{
        Schema::table('affiliates', function (Blueprint $table) {
            $table->string('code', 30)->unique()->after('type');
        });

	}

	public function down()
	{
        Schema::table('affiliates', function (Blueprint $table) {
           $table->dropColumn(['code']);
        });
	}
}