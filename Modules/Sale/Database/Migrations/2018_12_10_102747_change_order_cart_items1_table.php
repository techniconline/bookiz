<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangeOrderCartItems1Table extends Migration
{

    public function up()
    {
        Schema::table('cart', function (Blueprint $table) {
            $table->unsignedInteger("affiliate_id")->nullable()->after('promotion_amount');
            $table->float("affiliate_amount", 12, 2)->nullable()->after('affiliate_id');
        });

        Schema::table('affiliates', function (Blueprint $table) {
            $table->longText('conditions')->after('expire_date')->nullable()->comment('is json');
        });

        Schema::table('order_promotions', function (Blueprint $table) {
            $table->dropColumn('type');
        });

        Schema::table('order_promotions', function (Blueprint $table) {
            $table->enum('type', ['coupen', 'promotion', 'affiliate'])->after('order_id')->default('promotion')->index();
        });

        Schema::table('cart', function (Blueprint $table) {
            $table->foreign('affiliate_id')->references('id')->on('affiliates')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


    }

    public function down()
    {

        Schema::table('cart', function (Blueprint $table) {
            $table->dropForeign('cart_affiliate_id_foreign');
        });

        Schema::table('cart', function (Blueprint $table) {
            $table->dropColumn(['affiliate_id','affiliate_amount']);
        });

    }
}