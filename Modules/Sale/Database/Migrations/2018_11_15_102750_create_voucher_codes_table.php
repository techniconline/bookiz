<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVoucherCodesTable extends Migration {

	public function up()
	{
		Schema::create('voucher_codes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('voucher_id')->unsigned()->index();
			$table->string('code', 30)->unique();
            $table->unsignedInteger('used_qty')->default(0);
            $table->dateTime('active_time');
			$table->timestamps();
			$table->softDeletes();
		});

        Schema::table('voucher_codes', function(Blueprint $table) {
            $table->foreign('voucher_id')->references('id')->on('vouchers')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

	}

	public function down()
	{
        Schema::table('voucher_codes', function(Blueprint $table) {
            $table->dropForeign('voucher_codes_voucher_id_foreign');
        });
		Schema::drop('voucher_codes');
	}
}