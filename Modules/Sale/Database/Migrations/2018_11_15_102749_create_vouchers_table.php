<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVouchersTable extends Migration {

	public function up()
	{
		Schema::create('vouchers', function(Blueprint $table) {
			$table->increments('id');
            $table->unsignedInteger("instance_id")->index();
            $table->string('title', 255);
            $table->enum('type', array('percent', 'fixed'))->index();
			$table->float('value', 8,2)->default('0.00');
			$table->float('max_value', 10,2);
            $table->unsignedInteger("currency_id")->index();
			$table->unsignedInteger('max_qty')->default(0);
			$table->enum('use_type', array('unlimited', 'disposable_per_user','disposable_per_use_qty'));
            $table->unsignedInteger('use_qty')->default(1);
            $table->tinyInteger("active")->index();
			$table->unsignedInteger('duration');
			$table->mediumText('conditions')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('vouchers');
	}
}