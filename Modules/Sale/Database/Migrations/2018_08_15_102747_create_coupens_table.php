<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoupensTable extends Migration {

	public function up()
	{
		Schema::create('coupens', function(Blueprint $table) {
			$table->increments('id');
			$table->enum('pay_type', array('percent', 'fixed'))->index();
			$table->float('value', 8,2)->default('0.00');
			$table->float('max_value', 10,2);
            $table->unsignedInteger("currency_id")->index();
            $table->mediumInteger('coupen_qty');
			$table->enum('coupen_generator', array('automatically', 'manually'))->index();
			$table->mediumInteger('used_qty');
			$table->enum('used_type', array('unlimited', 'disposable', 'disposable_per_user'));
			$table->datetime('start_time')->index();
			$table->datetime('end_time');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('coupens');
	}
}