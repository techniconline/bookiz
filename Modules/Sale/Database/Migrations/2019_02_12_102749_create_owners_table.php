<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOwnersTable extends Migration {

	public function up()
	{
		Schema::create('owners', function(Blueprint $table) {
			$table->increments('id');
            $table->string("title")->index();
            $table->unsignedInteger("instance_id")->index();
            $table->enum('type', array('percent', 'fixed'))->nullable()->default('percent')->index();
            $table->float('value', 8,2)->nullable()->default('0.00');
            $table->float('max_value', 10,2)->nullable();
            $table->unsignedInteger("currency_id");
            $table->unsignedTinyInteger('active')->deafult(1)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});

        Schema::table('owners', function(Blueprint $table) {

            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('currency_id')->references('id')->on('currencies')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

	}

	public function down()
	{
        Schema::table('owners', function(Blueprint $table) {
            $table->dropForeign('owners_instance_id_foreign');
            $table->dropForeign('owners_currency_id_foreign');

        });
		Schema::drop('owners');
	}
}