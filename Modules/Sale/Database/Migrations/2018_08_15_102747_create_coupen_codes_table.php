<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoupenCodesTable extends Migration {

	public function up()
	{
		Schema::create('coupen_codes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('coupen_id')->unsigned()->index();
			$table->string('code', 100)->index();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('coupen_codes');
	}
}