<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAffiliatesTable extends Migration {

	public function up()
	{
		Schema::create('affiliates', function(Blueprint $table) {
			$table->increments('id');
            $table->unsignedInteger("instance_id")->index();
            $table->unsignedInteger('user_id');
            $table->enum('type', array('percent', 'fixed'))->index();
            $table->float('value', 8,2)->default('0.00');
            $table->float('max_value', 10,2);
            $table->unsignedInteger("currency_id")->index();
            $table->unsignedInteger('max_qty')->nullable()->default(0);
            $table->dateTime('active_date')->nullable();
            $table->dateTime('expire_date')->nullable();
            $table->unsignedTinyInteger('active')->deafult(1)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});

        Schema::table('affiliates', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

	}

	public function down()
	{
        Schema::table('affiliates', function(Blueprint $table) {
            $table->dropForeign('affiliates_user_id_foreign');
            $table->dropForeign('affiliates_instance_id_foreign');

        });
		Schema::drop('affiliates');
	}
}