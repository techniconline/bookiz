<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderPromotionsTable extends Migration {

	public function up()
	{
		Schema::create('order_promotions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('order_id')->unsigned()->index();
			$table->enum('type', array('coupen', 'promotion'))->index();
			$table->integer('type_id')->unsigned()->index();
			$table->text('options')->nullable();
			$table->float('amount', 12,2);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('order_promotions');
	}
}