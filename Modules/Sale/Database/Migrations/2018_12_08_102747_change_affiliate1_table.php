<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangeAffiliate1Table extends Migration {

	public function up()
	{
        Schema::table('affiliates', function (Blueprint $table) {
            $table->float('owner_value', 4,2)->nullable()->after('user_id');
        });

	}

	public function down()
	{
        Schema::table('affiliates', function (Blueprint $table) {
           $table->dropColumn(['owner_value']);
        });
	}
}