<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAffiliateCodesTable extends Migration {

	public function up()
	{
		Schema::create('affiliate_codes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('affiliate_id')->unsigned()->index();
			$table->string('code', 30)->unique();
            $table->unsignedTinyInteger('active')->deafult(1)->nullable();
            $table->timestamps();
			$table->softDeletes();
		});

        Schema::table('affiliate_codes', function(Blueprint $table) {
            $table->foreign('affiliate_id')->references('id')->on('affiliates')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

	}

	public function down()
	{
        Schema::table('affiliate_codes', function(Blueprint $table) {
            $table->dropForeign('affiliate_codes_affiliate_id_foreign');
        });
		Schema::drop('affiliate_codes');
	}
}