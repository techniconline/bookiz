<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderMessagesTable extends Migration {

	public function up()
	{
		Schema::create('order_messages', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('order_id')->unsigned()->index();
			$table->integer('user_id')->unsigned()->nullable()->index();
            $table->text('message')->nullable();
            $table->boolean('readed')->default(0);
		});
        Schema::table('order_messages', function(Blueprint $table) {
            $table->foreign('order_id')->references('id')->on('orders')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
	}

	public function down()
	{
        Schema::table('order_messages', function(Blueprint $table) {
            $table->dropForeign('order_messages_order_id_foreign');
        });
		Schema::drop('order_messages');
	}
}