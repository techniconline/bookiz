<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCartTable extends Migration {

	public function up()
	{
		Schema::create('cart', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned()->nullable()->index();
			$table->string('user_token',100)->nullable()->index();
			$table->string('coupen', 100)->nullable();
			$table->float('coupen_amount', 10,2)->default('0.00');
			$table->integer('promotion_id')->unsigned()->nullable()->index();
			$table->float('promotion_amount', 10,2)->default('0.00');
			$table->float('amount', 12,2)->default('0.00');
			$table->float('payable_amount', 12,2)->default('0.00');
            $table->unsignedInteger("currency_id")->index();
            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('cart');
	}
}