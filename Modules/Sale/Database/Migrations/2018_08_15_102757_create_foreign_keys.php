<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('cart_items', function(Blueprint $table) {
			$table->foreign('cart_id')->references('id')->on('cart')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('order_items', function(Blueprint $table) {
			$table->foreign('order_id')->references('id')->on('orders')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('order_promotions', function(Blueprint $table) {
			$table->foreign('order_id')->references('id')->on('orders')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('cart_items', function(Blueprint $table) {
			$table->dropForeign('cart_items_cart_id_foreign');
		});
		Schema::table('order_items', function(Blueprint $table) {
			$table->dropForeign('order_items_order_id_foreign');
		});
		Schema::table('order_promotions', function(Blueprint $table) {
			$table->dropForeign('order_promotions_order_id_foreign');
		});
	}
}