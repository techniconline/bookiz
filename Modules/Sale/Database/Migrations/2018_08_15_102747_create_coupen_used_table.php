<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoupenUsedTable extends Migration {

	public function up()
	{
		Schema::create('coupen_used', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('coupen_code_id');
			$table->integer('user_id')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('coupen_used');
	}
}