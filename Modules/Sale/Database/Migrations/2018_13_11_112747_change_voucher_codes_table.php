<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangeVoucherCodesTable extends Migration
{

    public function up()
    {
        Schema::table('voucher_codes', function (Blueprint $table) {
            $table->tinyInteger("active")->nullable()->default(1)->after('active_time');
        });

    }

    public function down()
    {

    }
}