<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderItemsTable extends Migration {

	public function up()
	{
		Schema::create('order_items', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('order_id')->unsigned()->index();
			$table->string('item_type', 30);
			$table->integer('item_id')->unsigned()->index();
			$table->string('name', 255);
			$table->text('options')->nullable();
			$table->integer('qty');
			$table->float('amount', 12,2)->default('0.00');
            $table->boolean('has_shipment')->default(0);
			$table->float('total_amount', 12,2)->default('0.00');
            $table->unsignedInteger("currency_id")->index();
            $table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('order_items');
	}
}