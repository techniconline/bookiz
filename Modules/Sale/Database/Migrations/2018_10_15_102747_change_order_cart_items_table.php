<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangeOrderCartItemsTable extends Migration {

	public function up()
	{
        Schema::table('cart_items', function (Blueprint $table) {
            $table->float("discount_amount", 10,2)->default('0.00')->after('amount');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->float("discount_amount", 10,2)->default('0.00')->after('amount');
        });
        Schema::table('order_promotions', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
        });
        Schema::table('orders', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('payments', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('order_promotions', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
	}

	public function down()
	{
        Schema::table('order_items', function (Blueprint $table) {
           $table->dropColumn(['discount_amount']);
        });
        Schema::table('cart_items', function (Blueprint $table) {
            $table->dropColumn(['discount_amount']);
        });
        Schema::table('order_promotions', function (Blueprint $table) {
            $table->dropColumn(['user_id']);
        });

        Schema::table('payments', function(Blueprint $table) {
            $table->dropForeign('payments_user_id_foreign');
        });

        Schema::table('orders', function(Blueprint $table) {
            $table->dropForeign('orders_user_id_foreign');
        });

        Schema::table('order_promotions', function(Blueprint $table) {
            $table->dropForeign('order_promotions_user_id_foreign');
        });
	}
}