<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCreditsTable extends Migration {

	public function up()
	{
		Schema::create('credits', function(Blueprint $table) {
			$table->bigIncrements('id');
            $table->unsignedInteger("instance_id");
            $table->morphs('model');
			$table->enum('type',['minus', 'plus'])->default('plus')->index();
			$table->float('amount',14,2)->nullable();
			$table->float('current_credit',16,2)->nullable();
            $table->unsignedInteger('user_id')->index();
			$table->timestamps();
		});

        Schema::table('credits', function(Blueprint $table) {

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
	}

	public function down()
	{
        Schema::table('credits', function(Blueprint $table) {
            $table->dropForeign('credits_user_id_foreign');
            $table->dropForeign('credits_instance_id_foreign');
        });

		Schema::drop('credits');
	}
}