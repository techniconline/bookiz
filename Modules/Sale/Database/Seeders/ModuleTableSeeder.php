<?php

namespace Modules\Sale\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Module\Module;

class ModuleTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('currencies')->delete();
		// CurrenciesSeeder
        $module=new Module();
        if (Module::where("name", "sale")->first()){
            return false;
        }

        $module->fill(array(
				'name' =>'sale',
				'version' => '2017010101',
			))->save();

	}
}