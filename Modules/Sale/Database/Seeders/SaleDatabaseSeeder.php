<?php

namespace Modules\Sale\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SaleDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();

        $this->call('Modules\Sale\Database\Seeders\ModuleTableSeeder');
        $this->command->info('Module table seeded!');

        $this->call('Modules\Sale\Database\Seeders\BlockTableSeeder');
        $this->command->info('Blocks table seeded!');
    }
}
