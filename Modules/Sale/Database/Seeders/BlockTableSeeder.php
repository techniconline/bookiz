<?php

namespace Modules\Sale\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Block\Block;
use Modules\Core\Models\Module\Module;


class BlockTableSeeder extends Seeder
{

    public function run()
    {
        //DB::table('currencies')->delete();

        $module_id = Module::where('name', 'sale')->first()->id;

        if (!Block::where("module_id", $module_id)->where("name", "cart_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'cart_block',
                'namespace' => 'Modules\Sale\ViewModels\Blocks\CartBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "chart_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'chart_block',
                'namespace' => 'Modules\Sale\ViewModels\Blocks\ChartBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "chart_course_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'chart_course_block',
                'namespace' => 'Modules\Sale\ViewModels\Blocks\ChartCourseBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::where("module_id", $module_id)->where("name", "report_course_block")->first()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'report_course_block',
                'namespace' => 'Modules\Sale\ViewModels\Blocks\ReportCourseBlock',
                'active' => 1,
            ))->save();
        }


    }
}