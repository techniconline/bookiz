<?php

return [
    'version' => '20180619001',
    'blocks' => [
        'left' => [],
        'top' => []
    ],
    'menus' => [
        'management' => [],
        'management_admin' => [

            [
                'alias' => '', //**
                'route' => 'sale.order.index', //**
                'key_trans' => 'sale::menu.orders_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-institution', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_orders',
            ],
            [
                'alias' => '', //**
                'route' => 'sale.payment.index', //**
                'key_trans' => 'sale::menu.payment_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-credit-card', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_orders',
            ],
            [
                'alias' => '', //**
                'route' => 'sale.owner.index', //**
                'key_trans' => 'sale::menu.owner_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-male', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_orders',
            ],
            [
                'alias' => '', //**
                'route' => 'sale.voucher.index', //**
                'key_trans' => 'sale::menu.voucher_manage', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-usd', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_orders',
            ],
            [
                'alias' => '', //**
                'route' => 'sale.affiliate.index', //**
                'key_trans' => 'sale::menu.affiliate_manage', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-usd', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_orders',
            ],
            [
                'alias' => 'sale.config.instance.index', //**
                'route' => 'sale.config.instance.index', //**
                'key_trans' => 'sale::menu.config.sale_instance', //**
                'order' => 1000, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-cog', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_manage_settings',
            ],

        ],
    ],
    'group_menu' => [
        'management' => [],
        'management_admin' => [
            ['alias' => 'admin_orders', //**
                'key_trans' => 'sale::menu.group.sale', //**
                'icon_class' => 'fa fa-money',
                'before' => '',
                'after' => '',
                'order' => 10,
            ],
        ]
    ],

];
