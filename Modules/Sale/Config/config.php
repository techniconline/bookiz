<?php

return [
    'name' => 'Sale',
     'permissions'=>[
         'sale.cart'=>true,
         'sale.cart.invoice'=>true,
         'sale.cart.add'=>true,
         'sale.cart.save'=>true,
         'sale.cart.delete'=>true,
         'sale.order.payment'=>true,
         'sale.order.payment.callback'=>true,
         'sale.order.complete'=>true,
         'sale.order.view'=>true,
         'sale.order.edit'=>['type' => '|', 'access' => 'core/is.super.admin'],
         'sale.order.index'=>['type' => '|', 'access' => 'core/is.super.admin'],
         'sale.payment.edit'=>['type' => '|', 'access' => 'core/is.super.admin'],
         'sale.payment.update'=>['type' => '|', 'access' => 'core/is.super.admin'],

         'sale.owner.index'=>['type' => '|', 'access' => 'core/is.super.admin'],
         'sale.owner.create'=>['type' => '|', 'access' => 'core/is.super.admin'],
         'sale.owner.save'=>['type' => '|', 'access' => 'core/is.super.admin'],
         'sale.owner.edit'=>['type' => '|', 'access' => 'core/is.super.admin'],
         'sale.owner.update'=>['type' => '|', 'access' => 'core/is.super.admin'],
         'sale.owner.delete'=>['type' => '|', 'access' => 'core/is.super.admin'],
     ],
];
