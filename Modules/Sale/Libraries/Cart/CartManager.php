<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 9/6/18
 * Time: 4:10 PM
 */

namespace Modules\Sale\Libraries\Cart;

use BridgeHelper;
use Illuminate\Support\Facades\DB;
use Modules\Sale\Models\Cart;
use Modules\Sale\Models\Cart\CartItem;
use Modules\Sale\Models\Traits\Cart\CartTrait;
use Modules\Sale\Models\Traits\Sale\ItemTrait;
use Modules\Sale\Models\Order\OrderPromotions;
use Modules\Sale\Traits\Affiliate\AffiliateTrait;
use Modules\Sale\Traits\Voucher\VoucherTrait;

class CartManager
{
    use CartTrait;
    use ItemTrait;
    use AffiliateTrait;
    use VoucherTrait;

    public $cart = false;
    private $error = false;
    private $remove_item_cart = false;
    private $message = false;
    public $affiliate;
    public $voucher_code;
    private $updated = false;

    /**
     * CartManager constructor.
     * @param bool $create
     */
    public function __construct($create = true)
    {
        $token = $this->getUserToken();
        if (BridgeHelper::getAccess()->isLogin()) {
            $user_id = BridgeHelper::getAccess()->getUser()->id;
            if (!$token) {
                $token = 0;
            }
            $carts = Cart::with('cartItems')->orWhere('user_id', $user_id)->orWhere('user_token', $token)->get();
            if ($carts->count()) {
                $this->setUserCart($carts, $user_id, $token);
            }
        } else {
            if ($token) {
                $this->cart = Cart::with('cartItems')->where('user_token', $token)->first();
            }
        }
        if ($create) {
            if (!$this->cart) {
                $this->createCart();
            }
        }

    }


    /**
     * @param $item
     * @return bool
     */
    public function addItemToCart($item)
    {
        $saleData = $item->getSaleItem();
        if (!isset($saleData['options'])) {
            $saleData['options'] = [];
        }
        $cartItem = $this->getSavedItem($saleData);
        $new = false;
        if (!$cartItem) {
            $cartItem = new CartItem;
            $new = true;
        }
        if (!$this->canSaleItem($cartItem, $item)) {
            return false;
        }
        DB::beginTransaction();
        try {
            if ($new) {
                $cartItem->cart_id = $this->cart->id;
                $cartItem->item_type = $saleData['item_type'];
                $cartItem->name = $saleData['name'];
                $cartItem->item_id = $saleData['item_id'];
                $cartItem->options = $saleData['options'];
                $cartItem->qty = $saleData['qty'];
                $cartItem->amount = $saleData['sale_cost'];
                if (isset($saleData['cost']) && $saleData['cost'] > $saleData['sale_cost']) {
                    $discount = $saleData['cost'] - $saleData['sale_cost'];
                    $cartItem->amount = $saleData['cost'];
                    $cartItem->discount_amount = $discount * $saleData['qty'];
                }
                $cartItem->currency_id = $saleData['currency'];
                $cartItem->total_amount = $saleData['sale_cost'] * $saleData['qty'];
            } else {
                $cartItem->qty = $cartItem->qty + $saleData['qty'];
                $cartItem->amount = $saleData['sale_cost'];
                if (isset($saleData['cost']) && $saleData['cost'] > $saleData['sale_cost']) {
                    $discount = $saleData['cost'] - $saleData['sale_cost'];
                    $cartItem->amount = $saleData['cost'];
                    $cartItem->discount_amount = $discount * $saleData['qty'];
                }
                $cartItem->currency_id = $saleData['currency'];
                $cartItem->total_amount = $saleData['sale_cost'] * $cartItem->qty;
            }
            $cartItem->save();
            $result = $this->updateCart($cartItem);
            if ($result) {
                DB::commit();
                $this->updateCartCache($this->getCartInfo());
            } else {
                $this->error = trans('sale::message.item_add_error');
                DB::rollBack();
            }
            return $result;
        } catch (\Exception $e) {
            report($e);
            DB::rollBack();
            $this->error = trans('sale::message.item_add_error');
            return false;
        }

    }

    /**
     * @param $cart_item_id
     * @return bool
     */
    public function delete($cart_item_id)
    {
        $cartItem = CartItem::find($cart_item_id);
        DB::beginTransaction();
        try {
            if ($cartItem) {
                $cartItem->delete();
                $this->cart->promotion_id = null;
                $this->cart->promotion_amount = 0.00;
                $result = $this->updateCart(null, [$cart_item_id]);
                if ($result) {
                    DB::commit();
                    $this->updateCartCache($this->getCartInfo());
                } else {
                    DB::rollBack();
                }
                return $result;
            } else {
                DB::rollBack();
            }
        } catch (\Exception $e) {
            report($e);
            DB::rollBack();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function deleteCart()
    {
        if (isset($this->cart->cartItems) && $this->cart->cartItems){
            foreach ($this->cart->cartItems as $cartItem) {
                $this->delete($cartItem->id);
            }
            return true;
        }
        return false;
    }

    /**
     * @param $cartItem
     * @param $item
     * @return bool
     */
    public function canSaleItem($cartItem, $item)
    {
        $qty = isset($cartItem->qty) ? $cartItem->qty : 0;
        $saleQty = $qty + $item->getItemQty();
        $maxQty = $item->getMaxQtyForSale();
        if ($saleQty > $maxQty) {
            $this->error = trans('sale::message.item_max_qty_error', ['max' => $maxQty]);
            $this->remove_item_cart = $this->removeCartItem($cartItem, $item);
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function getCartItemRemoved()
    {
        return $this->remove_item_cart;
    }

    /**
     * @param $cartItem
     * @param $item
     * @return bool
     */
    public function removeCartItem($cartItem, $item)
    {
        if (method_exists($item, 'canDeleteItem')){
            if ($item->canDeleteItem()){
                if ($cartItem->delete()){
                    $this->error = trans('sale::message.item_delete_success');
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param $saleData
     * @return bool
     */
    public function getSavedItem($saleData)
    {
        if (isset($this->cart->cartItems) && $this->cart->cartItems->count()) {
            $items = $this->cart->cartItems->where('item_type', $saleData['item_type'])->where('item_id', $saleData['item_id']);
            $saved_item = false;
            if ($items->count()) {
                foreach ($items as $item) {
                    if ($item->options_array === $saleData['options']) {
                        $saved_item = $item;
                        break;
                    }
                }
            }
            if ($saved_item) {
                return $saved_item;
            }
        }
        return false;
    }


    /**
     *
     */
    private function createCart()
    {
        try {
            $user = BridgeHelper::getAccess()->getUser();
            $this->cart = new Cart;
            if ($user) {
                $this->cart->user_id = $user->id;
            } else {
                $token = $this->getUserToken();
                $this->cart->user_token = $token;
            }
            $this->cart->currency_id = BridgeHelper::getCurrencyHelper()->getCurrentCurrencyId();
            $this->cart->save();
        } catch (\Exception $e) {
            report($e);
            abort("503", 'Service Unavailable');
            die();
        }
    }

    /**
     * @param $item_id
     */
    public function deleteItem($item_id)
    {
    }

    /**
     * @param null $AddedCartItem
     * @param array $deletedId
     * @return bool
     */
    public function updateCart($AddedCartItem = null, $deletedId = [])
    {
        $amount = 0.00;

        foreach ($this->cart->cartItems as $index => $item) {
            if (!in_array($item->id, $deletedId)) {
                $convertedPrice = BridgeHelper::getCurrencyHelper()->getConvertPrice($item->total_amount, $item->currency_id, $this->cart->currency_id);
                if ($convertedPrice === false) {
                    $this->error = trans('sale::message.currency_cenvert_error');
                }
                $amount = $amount + $convertedPrice;
            }else{
                unset($this->cart->cartItems[$index]);
            }
        }
        if ($AddedCartItem) {
            $convertedPrice = BridgeHelper::getCurrencyHelper()->getConvertPrice($AddedCartItem->total_amount, $AddedCartItem->currency_id, $this->cart->currency_id);
            if ($convertedPrice === false) {
                $this->error = trans('sale::message.currency_cenvert_error');
            }
            $amount = $amount + $convertedPrice;
        }

        //----------------------------------
        if ($this->updated){
            if ($this->cart->coupen_amount){
                $amount += $this->cart->coupen_amount;
            }

            if ($this->cart->promotion_amount){
                $amount += $this->cart->promotion_amount;
            }

            if ($this->cart->affiliate_amount){
                $amount += $this->cart->affiliate_amount;
            }
        }


        $this->cart->amount = $amount;
        if ($this->cart->coupen) {
//            $this->setCoupenOnCart();
        }
        $payable_amount = $amount;
        /* $this->setPromotionOnCart();
         if($this->setPromotionOnCart() && $this->cart->promotion_id){
             $payable_amount=$payable_amount-$this->cart->promotion_amount;
         }*/
        $this->cart->payable_amount = $payable_amount;
        if ($this->affiliate || $this->cart->affiliate_id) {
            $this->initAffiliate($payable_amount);
            $this->cart->payable_amount = $payable_amount - $this->affiliate_amount;
        }

        if ($this->voucher_code || $this->cart->coupen) {
            $this->initVoucher($payable_amount);
            $this->cart->payable_amount = $payable_amount - $this->voucher_amount;
        }


        $this->cart->save();

        return true;
    }

    /**
     * @return bool
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return bool|\Illuminate\Database\Eloquent\Model|mixed|null|object|static
     */
    public function getCart()
    {
        if (!$this->hasItems()) {
            return false;
        }
        return $this->cart;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getCartInfo()
    {
        $cart = $this->cart->toJson();
        return collect(['cart' => json_decode($cart)]);
    }

    /**
     * @param $carts
     * @param $user_id
     * @param $token
     */
    public function setUserCart($carts, $user_id, $token)
    {
        $deleted = [];
        if ($carts->count() > 1) {
            $this->cart = $carts->first();
            foreach ($carts as $cart) {
                if ($this->cart->id != $cart->id) {
                    if (isset($cart->cartItems) && $cart->cartItems->count()) {
                        foreach ($cart->cartItems as $item) {
                            if ($saleItem = $this->getSaleItem($item->item_type, $item->item_id, $item->options_array)) {
                                if ($saleItem->canSaleItem()) {
                                    $this->addItemToCart($saleItem);
                                }
                                $deleted[] = $item->id;
                                $item->delete();
                            }
                        }
                    }
                    $cart->delete();
                } else {
                    if (isset($cart->cartItems) && $cart->cartItems->count()) {
                        foreach ($cart->cartItems as $item) {
                            if ($saleItem = $this->getSaleItem($item->item_type, $item->item_id, $item->options_array)) {
                                if (!$saleItem->canSaleItem()) {
                                    $deleted[] = $item->id;
                                    $item->delete();
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $this->cart = $carts->first();
            $this->validateCart(false);
        }
        if ($this->cart->user_id != $user_id || $this->cart->user_token != $token) {
            $this->cart->user_token = $token;
            $this->cart->user_id = $user_id;
            $this->cart->save();
        }
        $this->updateCart(null, $deleted);
        $this->updateCartCache($this->getCartInfo());
    }

    /**
     * @param bool $updated
     * @return $this
     */
    public function validateCart($updated = true)
    {
        $deleted = [];
        foreach ($this->cart->cartItems as $item) {
            if ($saleItem = $this->getSaleItem($item->item_type, $item->item_id, $item->options_array)) {
                if (!$saleItem->canSaleItem()) {
                    $deleted[] = $item->id;
                    $item->delete();
                }
            }
        }

        $this->updated = $updated;
        if($updated){
            $this->updateCart(null, $deleted);
            $this->updateCartCache($this->getCartInfo());
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function hasItems()
    {
        if (!$this->cart) {
            return false;
        }
        if (!isset($this->cart->cartItems) || !$this->cart->cartItems || !$this->cart->cartItems->count()) {
            return false;
        }
        return true;
    }


    /**
     * @return int
     */
    public function getItemsCount()
    {
        if ($this->hasItems()) {
            return $this->cart->cartItems->count();
        }
        return 0;
    }

    /**
     * @return bool
     */
    public function setPromotionOnCart()
    {
        $user_id = 0;
        if (BridgeHelper::getAccess()->isLogin()) {
            $user_id = BridgeHelper::getAccess()->getUser()->id;
        }
        if (!$user_id) {
            return false;
        }
        if ($this->cart->promotion_id) {
            return true;
        }
        if ($this->getItemsCount()) {
            $promotion_id = 1;
            if ($this->canUsePromotion($user_id, $promotion_id)) {
                $items = $this->cart->cartItems()->get();
                foreach ($items as $item) {
                    if (isset($item->options->cat_ids) && is_array($item->options->cat_ids)) {
                        $itemCategories = $item->options->cat_ids;
                        if (in_array(1, $itemCategories)) {
                            $convertedPrice = BridgeHelper::getCurrencyHelper()->getConvertPrice($item->total_amount, $item->currency_id, $this->cart->currency_id);
                            if ($convertedPrice) {
                                $discount = (int)($convertedPrice * 20 / 100);
                                $this->cart->promotion_id = $promotion_id;
                                $this->cart->promotion_amount = $discount;
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }


    /**
     * @param $user_id
     * @param $promotion_id
     * @return bool
     */
    public function canUsePromotion($user_id, $promotion_id)
    {
        $orderPromotionsCount = OrderPromotions::where('user_id', $user_id)->where('type', 'promotion')->where('type_id', $promotion_id)->count();
        if ($orderPromotionsCount) {
            return false;
        }
        return true;
    }


    /**
     * @return bool
     */
    public function canUseDiscount()
    {
        $user_id = 0;
        if (BridgeHelper::getAccess()->isLogin()) {
            $user_id = BridgeHelper::getAccess()->getUser()->id;
        }
        if (!$this->cart->promotion_id) {
            return true;
        }
        if ($user_id) {
            if ($this->cart->promotion_id) {
                $result = $this->canUsePromotion($user_id, $this->cart->promotion_id);
                if (!$result) {
                    $this->cart->promotion_id = null;
                    $this->cart->promotion_amount = 0.00;
                    $this->updateCart();
                }
                return $result;
            }
        }
        return false;
    }

    /**
     * @return $this
     */
    public function addCodeToCart()
    {
        $this->updateCart();
        return $this;
    }

    /**
     * @return $this
     */
    public function removeCodeFromCart()
    {
        $this->affiliate = null;
        $this->cart->affiliate_amount = null;
        $this->cart->affiliate_id = null;
        $this->updateCart();
        return $this;
    }

    /**
     * @return $this
     */
    public function removeVoucherCodeFromCart()
    {
        $this->voucher_code = null;
        $this->cart->coupen_amount = 0;
        $this->cart->coupen = null;
        $this->updateCart();
        return $this;
    }

}