<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 9/8/18
 * Time: 3:05 PM
 */

namespace Modules\Sale\Libraries\Payment\Template;


Abstract class PaymentTemplate
{
    Abstract public function getName();

    Abstract public function getRenderView($template);

    Abstract public function getConfigView();

}