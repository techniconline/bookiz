<?php

namespace Modules\Sale\Libraries\Payment\Online\Payments\Asan;

use Illuminate\Support\Facades\Auth;
use Modules\Sale\Libraries\Payment\Online\Payments\OnlineTemplate;
use Modules\Sale\Libraries\Payment\Online\Payments\‌Asan\Functions;

class Asan extends OnlineTemplate
{
    use Functions;

    public $code = 'asan';

    public $config = [
        'asan_key' => 'EC5g1wBVXz+TyDIMWeHYP/T+azxQPdd0ZmK5iDonsBI=',
        'asan_iv' => '9TNn9eI0mLOtW+zvMNfupcTah35ZygRObvyCyzCpfJo=',
        'username' => 'rhpoi3783533',
        'password' => '6ZxN3xi',
        'asan_webservice_url' => 'https://services.asanpardakht.net/paygate/merchantservices.asmx?WSDL',
        'asan_internalutils_url' => 'https://services.asanpardakht.net/paygate/internalutils.asmx?WSDL',
        'asan_check_url' => 'https://services.asanpardakht.net/paygate/statuswatch.asmx?WSDL',
        'asan_merchant_id' => '4076',
        'asan_pay_url' => 'https://asan.shaparak.ir/',
        'asan_currency' => 'IRR',
    ];


    public $instanceConfig = [];
    protected $is_active = true;


    /**
     * @return array|bool
     */
    public function getOptions()
    {
        if ($this->is_active) {
            return [
                "option" => ["payment" => $this->code, "name" => trans("sale::payment." . $this->code), 'image' => url('/themes/default/assets/image/pay.png')]
            ];
        }
        return false;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /** STEP 5 - payment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View|mixed|string
     */
    public function getPaymentRedirectPage()
    {
        $MerchantID = $this->getConfig('asan_merchant_id');
        $Amount = $this->getCostByConvert($this->payment->amount, $this->payment->currency_id, $this->getConfig('asan_currency'));
        $CallbackURL = $this->getCallBack();
        $localDate = date("Ymd His");
        $req = "1,{$this->getConfig('username')},{$this->getConfig('password')},{$this->payment->id},{$Amount},{$localDate},{$this->payment->description},{$CallbackURL},0";
        $encryptedRequest = $this->encrypt($req);
        try {
            $opts = array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false));
            $params = array('stream_context' => stream_context_create($opts));
            $client = @new \soapclient($this->getConfig('asan_webservice_url'), $params);
        } catch (\SoapFault $E) {
            report($E);
            $this->payment->state = $this->getPaymentErrorState();
            $this->updateTransaction();
            return $this->getErrorBox(trans('sale::message.asan.webservice_error'));
        }
        $params = array(
            'merchantConfigurationID' => $MerchantID,
            'encryptedRequest' => $encryptedRequest
        );
        $result = $client->RequestOperation($params);
        if (!$result || !isset($result->RequestOperationResult)) {
            $this->payment->state = $this->getPaymentErrorState();
            $this->updateTransaction();
            return $this->getErrorBox(trans('sale::message.asan.webservice_error'));
        }
        $result = $result->RequestOperationResult;
        if ($result{0} == '0') {
            $this->payment->request_id = substr($result, 2);
            $this->payment->state = $this->getPaymentSendState();
            $this->updateTransaction();
            $action = $this->getConfig('asan_pay_url');
            return view('sale::payment.online.asan.redirect', ['action' => $action, 'request_id' => $this->payment->request_id]);
        } else {
            $this->payment->state = $this->getPaymentErrorState();
            $this->updateTransaction();
            return $this->getErrorBox($result);
        }
    }


    public function getPaymentVerify()
    {
        if (in_array($this->payment->state, $this->getPaymentSuccessStates())) {
            return $this->payment;
        }
        $currentState = $this->payment->state;
        $ReturningParams = $this->getFromRequest('ReturningParams', null);
        if (!$ReturningParams) {
            $this->payment->state = $this->getPaymentErrorState();
            return $this->getVerifyReturn($currentState);
        }
        $ReturningParams = $this->decrypt($ReturningParams);
        if (!$ReturningParams) {
            $this->payment->state = $this->getPaymentErrorState();
            return $this->getVerifyReturn($currentState);
        }
        $RetArr = explode(",", $ReturningParams);
        $Amount = $RetArr[0];
        $SaleOrderId = $RetArr[1];
        $RefId = $RetArr[2];
        $ResCode = $RetArr[3];
        $ResMessage = $RetArr[4];
        $PayGateTranID = $RetArr[5];
        $RRN = $RetArr[6];
        $LastFourDigitOfPAN = $RetArr[7];
        $RetArr = [
            'Amount' => $Amount,
            'SalePaymentId' => $SaleOrderId,
            'RefId' => $RefId,
            'ResCode' => $ResCode,
            'ResMessage' => $ResMessage,
            'PayGateTranID' => $PayGateTranID,
            'RRN' => $RRN,
            'LastFourDigitOfPAN' => $LastFourDigitOfPAN,
            'isVerified' => 0,
        ];
        if ($ResCode != '0' && $ResCode != '00') {
            $this->payment->state = $this->getPaymentFailState();
            return $this->getVerifyReturn($currentState, $RetArr);
        }
        return $this->getVerifyPay($RetArr);
    }


    public function getVerifyPay($RetArr)
    {
        $PayGateTranID = $RetArr['PayGateTranID'];
        $RefId = $RetArr['RefId'];
        $currentState = $this->payment->state;
        $MerchantID = $this->getConfig('asan_merchant_id');
        try {
            $opts = array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false));
            $params = array('stream_context' => stream_context_create($opts));
            $client = @new \soapclient($this->getConfig('asan_webservice_url'), $params);
            $encryptedCredintials = $this->encrypt("{$this->getConfig('username')},{$this->getConfig('password')}");
            $params = array(
                'merchantConfigurationID' => $MerchantID,
                'encryptedCredentials' => $encryptedCredintials,
                'payGateTranID' => $PayGateTranID
            );
            $result = $client->RequestVerification($params);
            if (!$result || !isset($result->RequestVerificationResult)) {
                $this->payment->state = $this->getPaymentErrorState();
                $RetArr['error'] = $this->error;
                $RetArr['isVerified'] = 0;
                return $this->getVerifyReturn($currentState, $RetArr);
            }
            $result = $result->RequestVerificationResult;
            if ($result != '500') {
                $this->payment->state = $this->getPaymentFailState();
                $RetArr['isVerified'] = 0;
                $RetArr['result'] = $result;
            } else {
                $RetArr['isVerified'] = 1;
                $this->payment->state = $this->getPaymentSuccessState();
                $this->payment->reference_id = $RefId;
                $RetArr['result'] = $result;
                $result = $client->RequestReconciliation($params);
                if ($result && isset($result->RequestReconciliationResult)) {
                    $RetArr['settlement'] = $result;
                }
            }
            return $this->getVerifyReturn($currentState, $RetArr);
        } catch (\Exception $e) {
            report($e);
            $this->payment->state = $this->getPaymentErrorState();
            $RetArr['error'] = $e->getMessage();
            return $this->getVerifyReturn($currentState, $RetArr);
        }
    }

    public function getVerifyReturn($currentState, $RetArr = [])
    {
        if ($currentState != $this->payment->state) {
            $this->payment->info = $RetArr;
            $this->updateTransaction();
        }
        if (in_array($this->payment->state, $this->getPaymentSuccessStates())) {
            return $this->payment;
        }
        return false;
    }

    public function getConfig($name)
    {
        if (isset($this->instanceConfig[$this->getInstanceName()])) {
            return $this->instanceConfig[$this->getInstanceName()][$name];
        }
        return $this->config[$name];
    }


    public function canPaymentRevalidate()
    {
        if (in_array($this->payment->state, $this->getPaymentSuccessStates())) {
            return false;
        }
        return true;
    }


    public function getPaymentRevalidate()
    {
        $RetArr = [];
        if ($this->canPaymentRevalidate()) {
            $currentState = $this->payment->state;
            $MerchantID = $this->getConfig('asan_merchant_id');
            try {
                $opts = array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false));
                $params = array('stream_context' => stream_context_create($opts));
                $client = @new \soapclient($this->getConfig('asan_webservice_url'), $params);
            } catch (SoapFault $E) {
                report($E);
                $this->payment->state = $this->getPaymentErrorState();
                $RetArr['error'] = $this->error;
                return $this->getVerifyReturn($this->payment->state, []);
            }
            try {
                $encryptedCredintials = $this->encrypt("{$this->getConfig('username')},{$this->getConfig('password')}");
                $params = array(
                    'merchantConfigurationID' => (int)$MerchantID,
                    'encryptedCredintials' => $encryptedCredintials,
                    'localInvoiceID' => $this->payment->id
                );
                $result = $client->TrxStatusFromLocalInvoiceID($params);
                if (!$result || !isset($result->TrxStatusFromLocalInvoiceIDResult)) {
                    $this->payment->state = $this->getPaymentErrorState();
                    $RetArr['error'] = $this->error;
                    return $this->getVerifyReturn($currentState, $RetArr);
                }
                $resultCode = $result->TrxStatusFromLocalInvoiceIDResult->FetchResult;
                if ($resultCode != '400') {
                    $RetArr = (array)$result;
                    $this->payment->state = $this->getPaymentFailState();
                } else {
                    $TransactionList = $result->TrxStatusFromLocalInvoiceIDResult->TransactionList;
                    foreach ($TransactionList as $transaction) {
                        $RetArr = [
                            'Amount' => $transaction->ActualTrxAmount,
                            'SalePaymentId' => $transaction->LocalInvoiceID,
                            'RefId' => $transaction->Token,
                            'ResCode' => $transaction->TrxResultCode,
                            'ResMessage' => '',
                            'PayGateTranID' => $transaction->PayGateTranID,
                            'RRN' => $transaction->RRN,
                            'LastFourDigitOfPAN' => $transaction->CardNumber,
                            'isVerified' => $transaction->IsVerified,
                        ];
                        if ($transaction->IsVerified) {
                            $this->payment->state = $this->getPaymentSuccessState();
                            $this->payment->reference_id = $transaction->‫‪Token;
                            return $this->getVerifyReturn($currentState, $RetArr);
                        } else {
                            return $this->getVerifyPay($RetArr);
                        }
                    }
                }
                return $this->getVerifyReturn($currentState, $RetArr);
            } catch (\Exception $e) {
                report($e);
            }
            return $this->getVerifyReturn($currentState, $RetArr);
        }
        return false;
    }

}
