<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 12/13/18
 * Time: 11:14 AM
 */

namespace Modules\Sale\Libraries\Payment\Online\Payments\‌Asan;


Trait Functions
{
    protected $error=false;

    protected function addpadding($string, $blocksize = 32)
    {
        $len = strlen($string);
        $pad = $blocksize - ($len % $blocksize);
        $string .= str_repeat(chr($pad), $pad);
        return $string;
    }

    protected function strippadding($string)
    {
        $slast = ord(substr($string, -1));
        $slastc = chr($slast);
        $pcheck = substr($string, -$slast);
        if (preg_match("/$slastc{" . $slast . "}/", $string)) {
            $string = substr($string, 0, strlen($string) - $slast);
            return $string;
        } else {
            return false;
        }
    }

    protected function encrypt($string = "")
    {
        $KEY=$this->getConfig('asan_key');
        $IV=$this->getConfig('asan_iv');
        if (PHP_MAJOR_VERSION <= 5) {
            $key = base64_decode($KEY);
            $iv = base64_decode($IV);
            return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $this->addpadding($string), MCRYPT_MODE_CBC, $iv));
        } else
            return $this->EncryptWS($string);
    }

    protected function decrypt($string = "")
    {
        $KEY=$this->getConfig('asan_key');
        $IV=$this->getConfig('asan_iv');
        if (PHP_MAJOR_VERSION <= 5) {
            $key = base64_decode($KEY);
            $iv = base64_decode($IV);
            $string = base64_decode($string);
            return $this->strippadding(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $string, MCRYPT_MODE_CBC, $iv));
        } else
            return $this->DecryptWS($string);
    }

    protected function EncryptWS($string = "")
    {
        $KEY=$this->getConfig('asan_key');
        $IV=$this->getConfig('asan_iv');
        try {
            $opts = array(
                'ssl' => array('verify_peer' => false, 'verify_peer_name' => false)
            );
            $params = array('stream_context' => stream_context_create($opts));
            $client = @new \soapclient($this->getConfig('asan_internalutils_url'), $params);
        } catch (\SoapFault $E) {
            report($E);
            $this->error=trans('sale::message.asan.webservice_error');
            return false;
        }
        $params = array(
            'aesKey' => $KEY,
            'aesVector' => $IV,
            'toBeEncrypted' => $string
        );
        $result = $client->EncryptInAES($params);
        if(!$result){
            $this->error=trans('sale::message.asan.encrypt_error');
            return false;
        }
        return $result->EncryptInAESResult;
    }

    protected function DecryptWS($string = "")
    {
        $KEY=$this->getConfig('asan_key');
        $IV=$this->getConfig('asan_iv');
        try {
            $opts = array(
                'ssl' => array('verify_peer' => false, 'verify_peer_name' => false)
            );
            $params = array('stream_context' => stream_context_create($opts));
            $client = @new \soapclient($this->getConfig('asan_internalutils_url'), $params);
        } catch (\SoapFault $E) {
            report($E);
            $this->error=trans('sale::message.asan.webservice_error');
            return false;
        }
        $params = array(
            'aesKey' => $KEY,
            'aesVector' => $IV,
            'toBeDecrypted' => $string
        );
        $result = $client->DecryptInAES($params);
        if(!$result){
            $this->error=trans('sale::message.asan.encrypt_error');
            return false;
        }
        return $result->DecryptInAESResult;
    }

}