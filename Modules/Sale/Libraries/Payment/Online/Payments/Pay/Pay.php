<?php

namespace Modules\Sale\Libraries\Payment\Online\Payments\Pay;


use Modules\Sale\Libraries\Payment\Online\Payments\OnlineTemplate;

class Pay extends OnlineTemplate
{
    public $code = 'pay';

    public $config = [
        'pay_api_key' => 'e90da14a-6ca8-11e8-95e0-005056a205be',
        'pay_token'=>'',
        'pay_currency' => 'IRR',
        'pay_send_url' => 'https://pay.ir/pg/send',
        'pay_redirect_url'=>'https://pay.ir/pg/',
        'pay_verify_url' => 'https://pay.ir/pg/verify',
    ];

    public $instanceConfig=[
        'medu'=>[
            'pay_api_key' => 'e90da14a-6ca8-11e8-95e0-005056a205be',
            'pay_token'=>'',
            'pay_currency' => 'IRR',
            'pay_send_url' => 'https://pay.ir/pg/send',
            'pay_redirect_url'=>'https://pay.ir/pg/',
            'pay_verify_url' => 'https://pay.ir/pg/verify',
        ]
    ];
    protected $is_active = true;


    /**
     * @return array|bool
     */
    public function getOptions()
    {
        if ($this->is_active) {
            return [
                "option" => ["payment" => $this->code, "name" => trans("sale::payment." . $this->code), 'image'=>url('/themes/default/assets/image/pay.png')]
            ];
        }
        return false;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /** STEP 5 - payment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View|mixed|string
     */
    public function getPaymentRedirectPage()
    {
        $Api = $this->getConfig('pay_api_key');
        $Amount = $this->getCostByConvert($this->payment->amount, $this->payment->currency_id, $this->getConfig('pay_currency'));
        $CallbackURL = $this->getCallBack();

        $result=$this->curl_post($this->getConfig('pay_send_url'), [
            'api'          => $Api,
            'amount'       => $Amount,
            'redirect'     => $CallbackURL,
            'factorNumber' => $this->payment->id,
            'description'  => $this->payment->description,
        ]);

        if ($result && $result->status) {
            $this->payment->request_id = $result->token;
            $this->payment->state = $this->getPaymentSendState();
            $this->updateTransaction();
            $action = $this->getConfig('pay_redirect_url') .$result->token;
            return view('sale::payment.online.pay.redirect', ['action' => $action]);
        } else {
            $this->payment->state = $this->getPaymentErrorState();
            $this->updateTransaction();
            return $this->getErrorBox($result->Status);
        }

    }


    public function getPaymentVerify()
    {
        // TODO: Implement getPaymentVerify() method.
        $Api = $this->getConfig('pay_api_key');
        $Amount = $this->getCostByConvert($this->payment->amount, $this->payment->currency_id, $this->getConfig('pay_currency'));
        $authority = $this->getFromRequest('token',$this->payment->request_id);
        $status = $this->getFromRequest('status','1');
        $currentState = $this->payment->state;
        if (in_array($this->payment->state, $this->getPaymentSuccessStates())) {
            return true;
        }

        if ($authority == $this->payment->request_id) {
            $result = curl_post($this->getConfig('pay_verify_url'), [
                'api' 	=> $Api,
                'token' => $this->payment->request_id,
            ]);
            if ($result && isset($result->status) && $result->status == 1) {
                $this->payment->state = $this->getPaymentSuccessState();
                $this->payment->reference_id = $this->payment->request_id;
            } else {
                $this->payment->state = $this->getPaymentFailState();
            }
        } elseif ($status != "1") {
            $this->payment->state = $this->getPaymentCancelState();
        } else {
            $this->payment->state = $this->getPaymentErrorState();
        }
        if ($currentState != $this->payment->state) {
            $this->updateTransaction();
        }
        if (in_array($this->payment->state, $this->getPaymentSuccessStates())) {
            return $this->payment;
        }
        return false;
    }


    public function getConfig($name){
        if(isset($this->instanceConfig[$this->getInstanceName()])){
            return $this->instanceConfig[$this->getInstanceName()][$name];
        }
        return $this->config[$name];
    }


    public function canPaymentRevalidate(){
        if (in_array($this->payment->state, $this->getPaymentSuccessStates())) {
            return false;
        }
        return true;
    }


    public function getPaymentRevalidate(){
        if($this->canPaymentRevalidate()){
            return $this->setRevalidate(true)->getPaymentVerify();
        }
        return false;
    }



    public function curl_post($url, $params)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
        ]);
        $res = curl_exec($ch);
        curl_close($ch);
        if($res){
            return json_decode($res);
        }
        return false;
    }

}