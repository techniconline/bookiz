<?php

namespace Modules\Sale\Libraries\Payment\Online\Payments;


use BridgeHelper;
use Illuminate\Support\Facades\Request;
use Modules\Sale\Models\Traits\Payment\PaymentStatesTrait;

Abstract class OnlineTemplate
{
    use PaymentStatesTrait;

    public $payment;
    public $instance_name=false;
    public $revalidate=false;


    public function setPayment($payment)
    {
        $this->payment = $payment;
        return $this;
    }

    public function getPayment()
    {
        return $this->payment;
    }

    public function updateTransaction()
    {
        return $this->payment->save();
    }

    public function getErrorBox($status)
    {
        return view('sale::payment.error', ['status' => $status, 'payment' => $this->payment]);
    }

    public function getFromRequest($name,$revalidateValue=null){
        if($this->revalidate){
            return $revalidateValue;
        }
        return Request::get($name);
    }

    public function setRevalidate($value)
    {
        $this->revalidate=$value;
        return $this;
    }
    /**
     * convert cost by rate
     * @param float $cost
     * @param $fromCurrency
     * @param bool $toCurrency
     * @return mixed
     */
    public function getCostByConvert($price, $from_currency_id, $to_currency_code)
    {
        $to_currency=BridgeHelper::getCurrencyHelper()->getCurrencyByCode($to_currency_code);
        if(!$to_currency){
            abort(500);
        }
        return BridgeHelper::getCurrencyHelper()->getConvertPrice($price, $from_currency_id, $to_currency->id);
    }

    public function getInstanceName(){
        if($this->instance_name){
            return $this->instance_name;
        }
        $instance=app('getInstanceObject')->getCurrentInstance();
        if(!$instance){
            $this->instance_name=trim(strtolower(config('app.instance')));
            return $this->instance_name;
        }
        $this->instance_name=strtolower(trim($instance->name));
        return $this->instance_name;
    }

    /**
     * @return string
     */
    public function getCallBack()
    {
        return route('sale.' . $this->payment->action . '.payment.callback', ['id' => $this->payment->action_id, "reference_code" => $this->payment->options->reference_code, "app" =>request()->get("app", "web")]);
    }

    abstract public function getCode();

    /**
     * @param object $course
     * @param object $instance
     * @return string
     */
    abstract public function getPaymentRedirectPage();

    /**
     * @param $payment
     * @param $instance
     * @return bool
     */
    abstract public function getPaymentVerify();

    abstract public function canPaymentRevalidate();
    abstract public function getPaymentRevalidate();
}