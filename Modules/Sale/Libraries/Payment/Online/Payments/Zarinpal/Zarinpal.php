<?php

namespace Modules\Sale\Libraries\Payment\Online\Payments\Zarinpal;

use Illuminate\Support\Facades\Request;
use Modules\Sale\Libraries\Payment\Online\Payments\OnlineTemplate;

class Zarinpal extends OnlineTemplate
{
    public $code = 'zarinpal';

    public $config = [
        'zarin_merchant_id' => '94477bca-2ea1-11e9-8890-005056a205be',
        'zarin_currency' => 'IRT',
        'zarin_get_url' => 'https://www.zarinpal.com/pg/services/WebGate/wsdl',
        'zarin_pay_url' => 'https://www.zarinpal.com/pg/StartPay/',
        'zarin_extra_url' => '/ZarinGat',
    ];

    public $instanceConfig = [
        'ltms' => [
//            'zarin_merchant_id' => '548747b8-f91a-11e6-a4e0-005056a205be',
//            'zarin_currency' => 'IRT',
//            'zarin_get_url' => 'https://www.zarinpal.com/pg/services/WebGate/wsdl',
//            'zarin_pay_url' => 'https://www.zarinpal.com/pg/StartPay/',
//            'zarin_extra_url' => '/ZarinGat',
        ]
    ];
    protected $is_active = true;


    /**
     * @return array|bool
     */
    public function getOptions()
    {
        if ($this->is_active) {
            return [
                "option" => ["payment" => $this->code, "name" => trans("sale::payment." . $this->code), 'image' => url('/assets/img/zarinpal.png')]
            ];
        }
        return false;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /** STEP 5 - payment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View|mixed|string
     */
    public function getPaymentRedirectPage()
    {
        $MerchantID = $this->getConfig('zarin_merchant_id');
        $Amount = $this->getCostByConvert($this->payment->amount, $this->payment->currency_id, $this->getConfig('zarin_currency'));
        $CallbackURL = $this->getCallBack();

        $client = new \SoapClient($this->getConfig('zarin_get_url'), ['encoding' => 'UTF-8']);

        $result = $client->PaymentRequest([
            'MerchantID' => $MerchantID,
            'Amount' => $Amount,
            'Description' => $this->payment->description,
            'CallbackURL' => $CallbackURL,
        ]);

        if ($result->Status == 100) {
            $this->payment->request_id = $result->Authority;
            $this->payment->state = $this->getPaymentSendState();
            $this->updateTransaction();
            $action = $this->getConfig('zarin_pay_url') . $result->Authority . $this->getConfig('zarin_extra_url');
            return view('sale::payment.online.zarinpal.redirect', ['action' => $action]);
        } else {
            $this->payment->state = $this->getPaymentErrorState();
            $this->updateTransaction();
            return $this->getErrorBox($result->Status);
        }

    }


    public function getPaymentVerify()
    {
        // TODO: Implement getPaymentVerify() method.
        $MerchantID = $this->getConfig('zarin_merchant_id');
        $Amount = $this->getCostByConvert($this->payment->amount, $this->payment->currency_id, $this->getConfig('zarin_currency'));
        $authority = $this->getFromRequest('Authority', $this->payment->request_id);
        $status = $this->getFromRequest('Status', 'OK');
        $currentState = $this->payment->state;
        if (in_array($this->payment->state, $this->getPaymentSuccessStates())) {
            return $this->payment;
        }
        $info = ['status' => $status];
        if ($status == 'OK' && $authority == $this->payment->request_id) {
            $client = new \SoapClient($this->getConfig('zarin_get_url'), ['encoding' => 'UTF-8']);
            $options = [
                'MerchantID' => $MerchantID,
                'Authority' => $authority,
                'Amount' => $Amount,
            ];
            $result = $client->PaymentVerification($options);
            if ($result->Status == 100 || $result->Status == 101) {
                $this->payment->state = $this->getPaymentSuccessState();
                $this->payment->reference_id = $result->RefID;
            } else {
                $this->payment->state = $this->getPaymentFailState();
            }
            if (is_object($result) || is_array($result)) {
                $info = $result;
            }
        } elseif ($status == "NOK") {
            $this->payment->state = $this->getPaymentCancelState();
        } else {
            $this->payment->state = $this->getPaymentErrorState();
        }
        if ($currentState != $this->payment->state) {
            $this->payment->info = $info;
            $this->updateTransaction();
        }
        if (in_array($this->payment->state, $this->getPaymentSuccessStates())) {
            return $this->payment;
        }
        return false;
    }


    public function getConfig($name)
    {
        if (isset($this->instanceConfig[$this->getInstanceName()])) {
            return $this->instanceConfig[$this->getInstanceName()][$name];
        }
        return $this->config[$name];
    }


    public function canPaymentRevalidate()
    {
        if (in_array($this->payment->state, $this->getPaymentSuccessStates())) {
            return false;
        }
        return true;
    }


    public function getPaymentRevalidate()
    {
        if ($this->canPaymentRevalidate()) {
            return $this->setRevalidate(true)->getPaymentVerify();
        }
        return false;
    }

}