<?php

namespace Modules\Sale\Libraries\Payment\Online\Payments\Fake;

use Illuminate\Support\Facades\Request;

use Modules\Sale\Libraries\Payment\Online\Payments\OnlineTemplate;

class Fake extends OnlineTemplate
{
    public $code = 'fake';

    public $config = [];

    protected $is_active = true;

    /**
     * @return array|bool
     */
    public function getOptions()
    {
        if ($this->is_active) {
            return [
                "option" => ["payment" => $this->code, "name" => trans("sale::payment." . $this->code), 'image' => 'https://shaparak.ir/themes/shaparak94/assets/img/logo.png']
            ];
        }
        return false;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    public function getPaymentRedirectPage()
    {
        $this->payment->request_id = sha1($this->payment->id . time());
        $this->payment->state = $this->getPaymentSendState();
        $this->updateTransaction();
        $action = $this->getCallBack();
        return view('sale::payment.online.fake.redirect', ['action' => $action, 'request_id' => $this->payment->request_id]);
    }


    public function getPaymentVerify()
    {
        // TODO: Implement getPaymentVerify() method.
        $authority = Request::get('request_id');
        $currentState = $this->payment->state;
        if (in_array($this->payment->state, $this->getPaymentSuccessStates())) {
            return true;
        }
        if ($authority == $this->payment->request_id) {
            $this->payment->state = $this->getPaymentSuccessState();
            $this->payment->reference_id = $this->payment->request_id;
        } else {
            $this->payment->state = $this->getPaymentErrorState();
        }

        if ($currentState != $this->payment->state) {
            $this->updateTransaction();
        }
        if (in_array($this->payment->state, $this->getPaymentSuccessStates())) {
            return $this->payment;
        }
        return false;
    }


    public function canPaymentRevalidate()
    {
        if (in_array($this->payment->state, $this->getPaymentSuccessStates())) {
            return false;
        }
        return true;
    }


    public function getPaymentRevalidate()
    {
        if ($this->canPaymentRevalidate()) {
            $this->payment->state = $this->getPaymentSuccessState();
            $this->payment->reference_id = $this->payment->request_id;
            $this->updateTransaction();
            return $this->payment;
        }
        return false;
    }
}