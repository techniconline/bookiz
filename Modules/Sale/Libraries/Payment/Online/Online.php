<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 9/8/18
 * Time: 3:04 PM
 */

namespace Modules\Sale\Libraries\Payment\Online;


use Modules\Sale\Libraries\Payment\Template\PaymentTemplate;

class Online extends PaymentTemplate
{
    private $name='online';
    private $payments=[
        'zarinpal',
        'asan',
        'fake',
    ];


    public function getName()
    {
        return $this->name;
    }


    public function getRenderView($template){
        $html='';
        foreach($this->payments as $payment){
            $html.=view('sale::payment.online.'.strtolower($payment).'.'.$template)->render();
        }
        return $html;
    }

    /**
     * @return array
     */
    public function getPayments()
    {
        $payments = [];
        foreach ($this->payments as $payment) {
            $key = $payment;
            $payment = $this->getPaymentObject($payment);
            if(!method_exists($payment, "getOptions")){
                continue;
            }

            $payment = $payment->getOptions();
            if(!$payment){
                continue;
            }
            $payments[$key] = $payment;
        }
        return $payments;
    }


    public function getConfigView(){

    }


    /** STEP 4 - payment
     * @param $payment
     * @return bool
     */
    public function getPaymentPage($payment){

        $options=$payment->options;
        if(!isset($options->payment)){
            return false;
        }
        if(!in_array($options->payment,$this->payments)){
            return false;
        }
        $onlinePay=$this->getPaymentObject($options->payment);
        return $onlinePay->setPayment($payment)->getPaymentRedirectPage();
    }

    public function getPaymentVerify($payment){
        $options=$payment->options;
        if(!isset($options->payment)){
            return false;
        }
        if(!in_array($options->payment,$this->payments)){
            return false;
        }
        $onlinePay=$this->getPaymentObject($options->payment);
        return $onlinePay->setPayment($payment)->getPaymentVerify();
    }

    public function getPaymentRevalidate($payment){
        $options=$payment->options;
        if(!isset($options->payment)){
            return false;
        }
        if(!in_array($options->payment,$this->payments)){
            return false;
        }
        $onlinePay=$this->getPaymentObject($options->payment);
        return $onlinePay->setPayment($payment)->getPaymentRevalidate();
    }

    public function getPaymentObject($name){
        $name=ucfirst($name);
        $paymentClass = "\\Modules\\Sale\\Libraries\\Payment\\Online\\Payments\\".$name."\\".$name;
        return new $paymentClass();
    }
}