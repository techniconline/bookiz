<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 9/8/18
 * Time: 3:05 PM
 */

namespace Modules\Sale\Libraries\Payment;

use Modules\Sale\Models\Payment;

class MasterPayment
{
    private $types = [
        'online'
    ];

    /** STEP 3 - payment
     * @param Payment $payment
     * @return mixed
     */
    public function getPaymentPage(Payment $payment)
    {
        $currentPayment = $this->getPaymentObject($payment->type);
        return $currentPayment->getPaymentPage($payment);
    }

    /**
     * @param Payment $payment
     * @return mixed
     */
    public function getPaymentVerify(Payment $payment)
    {
        $currentPayment = $this->getPaymentObject($payment->type);
        return $currentPayment->getPaymentVerify($payment);
    }


    /**
     * @param Payment $payment
     * @return mixed
     */
    public function getPaymentRevalidate(Payment $payment)
    {
        $currentPayment = $this->getPaymentObject($payment->type);
        return $currentPayment->getPaymentRevalidate($payment);
    }

    /**
     * @param string $template
     * @param bool $json
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View|mixed
     */
    public function getViewForm($template = 'payment', $json = false)
    {
        $list = [];
        foreach ($this->types as $type) {
            $list[$type] = $this->getPaymentObject($type);
            if ($json) {
                $list[$type] = $list[$type]->getPayments();
            }
        }
        if ($json) {
            return $list;
        }
        return view('sale::payment.payment', ['list' => $list, 'template' => $template]);
    }

    public function getPaymentObject($name)
    {
        $name = ucfirst($name);
        $paymentClass = "\\Modules\\Sale\\Libraries\\Payment\\" . $name . "\\" . $name;
        return new $paymentClass();
    }
}