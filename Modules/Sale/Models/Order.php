<?php

namespace Modules\Sale\Models;

use Modules\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Sale\Models\Traits\Order\OrderModelTrait;
use Modules\Sale\Models\Traits\Payment\OrderStatesTrait;
use Modules\Sale\Models\Traits\Payment\PaymentStatesTrait;

class Order extends BaseModel
{

    protected $table = 'orders';
    public $timestamps = true;

    use SoftDeletes;
    use OrderModelTrait;
    use PaymentStatesTrait;

    protected $dates = ['deleted_at'];

    protected $fillable = array('instance_id', 'reference_code', 'user_id', 'state', 'amount', 'payable_amount', 'currency_id');
    protected $appends = ["single_url", "cost", "total", "discount", "discount_percent", "discount_text"];

    public $api_fields = ['id', 'instance_id', 'reference_code', 'user_id', 'state', 'amount', 'payable_amount', 'currency_id', "cost", "total", "discount", "discount_percent", "discount_text"];
    public $list_fields = ['id', 'instance_id', 'reference_code', 'user_id', 'state', 'amount', 'payable_amount', 'currency_id'];
    public $api_append_fields = ['created_at_by_mini_format'];
    public $list_append_fields = ['created_at_by_mini_format', "single_url", "cost", "total", "discount", "discount_percent", "discount_text"];


}