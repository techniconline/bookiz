<?php

namespace Modules\Sale\Models\Discount\Coupen;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoupenUse extends Model 
{

    protected $table = 'coupen_used';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}