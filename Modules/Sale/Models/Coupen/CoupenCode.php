<?php

namespace Modules\Sale\Models\Discount\Coupen;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoupenCode extends Model 
{

    protected $table = 'coupen_codes';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}