<?php

namespace Modules\Sale\Models;

use Modules\Core\Models\BaseModel;
use Modules\Core\Models\Instance\Instance;
use Modules\User\Models\User;

class Credit extends BaseModel
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'user_id', 'model_id', 'model_type', 'type', 'amount', 'current_credit', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo(Instance::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
