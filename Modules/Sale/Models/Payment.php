<?php

namespace Modules\Sale\Models;

use Modules\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Sale\Models\Traits\Payment\PaymentModelTrait;

class Payment extends BaseModel
{

    use PaymentModelTrait;

    protected $table = 'payments';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = array('instance_id','user_id','action_id','action','amount','currency_id','type','bank','options','request_id','reference_id','description','state','info');

    public $api_fields = ['id', 'instance_id','user_id','action_id','action','amount','currency_id','type','options','request_id','reference_id','description','state'];
    public $list_fields = ['id', 'instance_id','user_id','action_id','action','amount','currency_id','type','state'];
    public $api_append_fields = ['created_at_by_mini_format', 'cost', 'type_text', 'status_text'];
    public $list_append_fields = ['created_at_by_mini_format', "cost",'type_text', 'status_text'];


}