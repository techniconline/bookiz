<?php

namespace Modules\Sale\Models\Cart;

use Modules\Core\Models\BaseModel;
use Modules\Sale\Models\Traits\Cart\CartItemModelTrait;

class CartItem extends BaseModel
{
    use CartItemModelTrait;

    protected $table = 'cart_items';
    public $timestamps = true;
    protected $fillable = array('cart_id', 'item_type', 'item_id', 'options', 'qty', 'amount', 'total_amount', 'currency_id');

    protected $appends = ["cost", "total", "discount", "discount_percent", "discount_text", "discount_items", "image", "url_item"];

}