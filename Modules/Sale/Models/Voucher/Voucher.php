<?php

namespace Modules\Sale\Models\Voucher;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Models\Instance\Instance;


class Voucher extends BaseModel
{

    protected $table = 'vouchers';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = array('instance_id', 'title', 'type', 'value', 'max_value', 'currency_id', 'generator_type', 'max_qty', 'use_type', 'use_qty', 'active', 'duration', 'conditions');


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo(Instance::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function voucherCodes()
    {
        return $this->hasMany(VoucherCodes::class);
    }

}