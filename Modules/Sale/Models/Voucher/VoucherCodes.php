<?php

namespace Modules\Sale\Models\Voucher;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Sale\Models\Order\OrderPromotions;


class VoucherCodes extends BaseModel
{

    protected $table = 'voucher_codes';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = array('voucher_id', 'code', 'used_qty', 'active_time', 'active', 'deleted_at', 'created_at', 'updated_at');

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function voucher()
    {
        return $this->belongsTo(Voucher::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function orderPromotions()
    {
        return $this->hasMany(OrderPromotions::class, 'type_id')->where('type', 'coupen');
    }

}