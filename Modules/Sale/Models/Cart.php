<?php

namespace Modules\Sale\Models;

use Modules\Core\Models\BaseModel;
use Modules\Sale\Models\Traits\Cart\CartModelTrait;

class Cart extends BaseModel
{

    use CartModelTrait;

    protected $table = 'cart';
    public $timestamps = true;

    protected $fillable = array('user_id', 'user_token', 'coupen', 'coupen_amount', 'promotion_id', 'promotion_amount', 'affiliate_id', 'affiliate_amount', 'amount', 'payable_amount', 'currency_id', 'data');

    protected $appends = ["cost", "total", "discount", "discount_percent", "discount_text","coupen_amount_text", "discount_items"];

}