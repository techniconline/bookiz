<?php

namespace Modules\Sale\Models\Owner;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Models\Currency\Currency;

class OwnerCalculateLog extends BaseModel
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'owner_calculate_log';

    /**
     * @var array
     */
    protected $fillable = ['owner_id', 'model_id', 'model_type', 'type', 'amount', 'current_credit', 'currency_id', 'active', 'created_at', 'updated_at', 'deleted_at'];

    const TYPE_MINUS = 'minus';
    const TYPE_PLUS = 'plus';

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    public $api_fields = [];
    public $list_fields = [];
    public $api_append_fields = [];
    public $list_append_fields = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

}
