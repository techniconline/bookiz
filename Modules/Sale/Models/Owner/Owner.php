<?php

namespace Modules\Sale\Models\Owner;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Sale\Traits\Owner\Owner as ModelTrait;

class Owner extends BaseModel
{
    use SoftDeletes;
    use ModelTrait;

    /**
     * @var array
     */
    protected $fillable = ['title', 'instance_id', 'currency_id', 'type', 'value', 'max_value', 'active', 'created_at', 'updated_at', 'deleted_at'];

    public $status_types = ['fixed', 'percent'];

    const TYPE_PERCENT = 'percent';
    const TYPE_FIXED = 'fixed';

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    public $api_fields = [];
    public $list_fields = [];
    public $api_append_fields = [];
    public $list_append_fields = [];

}
