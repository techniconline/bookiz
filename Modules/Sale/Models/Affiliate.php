<?php

namespace Modules\Sale\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Sale\Traits\Affiliate as TraitModel;

class Affiliate extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'user_id', 'owner_value', 'type', 'code', 'value', 'max_value', 'currency_id', 'max_qty', 'active_date', 'expire_date', 'conditions', 'active', 'created_at', 'updated_at', 'deleted_at'];

    use TraitModel;
    use SoftDeletes;

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
