<?php

namespace Modules\Sale\Models\Order;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;

class OrderExport implements FromCollection
{

    use Exportable;

    public function setCollection($collection)
    {
        $this->collection = $collection;
        return $this;
    }

    public function collection()
    {
        return $this->collection;
    }

}