<?php

namespace Modules\Sale\Models\Order;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Sale\Traits\Order\OrderItemsLog as ModelTrait;

class OrderItemsLog extends BaseModel
{

    use SoftDeletes;
    use ModelTrait;

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'order_items_log';

    /**
     * @var array
     */
    protected $fillable = ['order_id', 'order_item_id', 'item_id', 'item_type', 'instance_id', 'user_id', 'currency_id', 'payment_id', 'order_promotion_id', 'amount', 'discount', 'total_amount', 'payment_amount', 'promotion_amount', 'bank', 'active', 'created_at', 'updated_at', 'deleted_at'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    public $api_fields = [];
    public $list_fields = [];
    public $api_append_fields = [];
    public $list_append_fields = [];
}
