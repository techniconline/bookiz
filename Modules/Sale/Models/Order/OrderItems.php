<?php

namespace Modules\Sale\Models\Order;

use Modules\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Sale\Models\Traits\Order\OrderItemsTrait;

class OrderItems extends BaseModel
{

    use OrderItemsTrait;

    protected $table = 'order_items';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('order_id', 'item_type', 'item_id', 'name', 'options', 'qty', 'amount', 'has_shipment', 'total_amount', 'currency_id', 'created_at');
    protected $appends = ["cost", "total", "discount", "discount_percent", "discount_text", "url_item", "item_row",'item_extra_data'];

    public $api_fields = ['id', 'order_id', 'item_row','item_extra_data', 'item_type', 'item_id', 'name', 'options', 'qty', 'amount', 'has_shipment', 'total_amount', "cost", "total", "discount", "discount_percent", "discount_text", "url_item", 'currency_id', 'created_at'];
    public $list_fields = ['id', 'order_id', 'item_row','item_extra_data', 'item_type', 'item_id', 'name', 'options', 'qty', 'amount', 'has_shipment', 'total_amount', 'total', 'currency_id', 'created_at', 'updated_at', 'order_item_date'];
    public $api_append_fields = [''];
    public $list_append_fields = [''];


}