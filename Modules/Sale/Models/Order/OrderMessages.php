<?php

namespace Modules\Sale\Models\Order;

use Modules\Core\Models\BaseModel;

class OrderMessages extends BaseModel
{

    protected $table = 'order_messages';
    public $timestamps = false;

    protected $fillable = array('order_id','user_id','message','readed');

}