<?php

namespace Modules\Sale\Models\Order;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\User\Models\User;


class OrderPromotions extends BaseModel
{

    protected $table = 'order_promotions';
    public $timestamps = true;

    use SoftDeletes;
    protected $fillable = array('order_id', 'type', 'type_id', 'options', 'amount', 'user_id', 'deleted_at');

    public $types = ['coupen', 'promotion', 'affiliate'];

    const TYPE_COUPEN = 'coupen';
    const TYPE_PROMOTION = 'promotion';
    const TYPE_AFFILIATE = 'affiliate';

    protected $dates = ['deleted_at'];

    public function getOptionsAttribute()
    {
        if (!$this->attributes['options']) {
            return new \stdClass();
        }
        return json_decode($this->attributes['options']);
    }

    public function getOptionsArrayAttribute()
    {
        if (!$this->attributes['options']) {
            return [];
        }
        return json_decode($this->attributes['options'], true);
    }

    public function setOptionsAttribute($val)
    {
        if (!is_array($val) && !is_object($val)) {
            $this->attributes['options'] = $val;

        } else {
            $this->attributes['options'] = json_encode($val);
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}