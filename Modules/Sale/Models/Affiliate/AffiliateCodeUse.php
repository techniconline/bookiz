<?php

namespace Modules\Sale\Models\Affiliate;

use Modules\Core\Models\BaseModel;
use Modules\User\Models\User;

class AffiliateCodeUse extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['affiliate_code_id', 'user_id', 'model_id', 'model_type', 'amount', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function affiliateCode()
    {
        return $this->belongsTo(AffiliateCode::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
