<?php

namespace Modules\Sale\Models\Affiliate;

use Modules\Core\Models\BaseModel;
use Modules\Sale\Models\Affiliate;

class AffiliateCode extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['affiliate_id', 'code', 'active', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function affiliate()
    {
        return $this->belongsTo(Affiliate::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function affiliateCodeUses()
    {
        return $this->hasMany(AffiliateCodeUse::class);
    }
}
