<?php

namespace Modules\Sale\Models\Discount;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupen extends Model 
{

    protected $table = 'coupens';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}