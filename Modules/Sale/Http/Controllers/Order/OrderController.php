<?php

namespace Modules\Sale\Http\Controllers\Order;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class OrderController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('order.orderGrid')->setActionMethod("getViewListIndex")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['id' => $id], $request)->setViewModel('order.order')->setActionMethod("getView")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function payments($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['order_id' => $id,'system'=>'order'], $request)->setViewModel('order.paymentGrid')->setActionMethod("getViewListIndex")->response();
    }

    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
//        return $masterViewModel->setItemsRequest(['order_id' => $id], $request)->setViewModel('order')->setActionMethod("saveOrder")->response();
    }

    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
//        return $masterViewModel->setItemsRequest(['order_id' => $id], $request)->setViewModel('order')->setActionMethod("destroyOrder")->response();
    }

    public function accept($id, MasterViewModel $masterViewModel, Request $request)
    {
//        return $masterViewModel->setItemsRequest(['order_id' => $id], $request)->setViewModel('order')->setActionMethod("acceptOrder")->response();
    }


}
