<?php

namespace Modules\Sale\Http\Controllers\Payment;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class PaymentController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('order.paymentGrid')->setActionMethod("getViewListIndex")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['payment_id' => $id], $request)->setViewModel('order.paymentActions')->setActionMethod("editPayment")->response();
    }

    /**
     * @param $payment_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($payment_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['payment_id' => $payment_id], $request)->setViewModel('order.paymentActions')->setActionMethod("updatePayment")->response();
    }



    public function validate($payment_id, MasterViewModel $masterViewModel, Request $request){
        return $masterViewModel->setItemsRequest(['payment_id' => $payment_id], $request)->setViewModel('order.paymentActions')->setActionMethod("validatePayment")->response();
    }

}
