<?php

namespace Modules\Sale\Http\Controllers\Cart;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('cart')->setActionMethod("getCart")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getCartBooking(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('cart')->setActionMethod("getCartBooking")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getInvoiceBooking(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('cart')->setActionMethod("getInvoiceBooking")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveBooking(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('order.saveBooking')->setActionMethod("saveOrderBooking")->response();
    }

    /**
     * @param $item
     * @param $item_id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function add($item, $item_id, Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['item' => $item, 'item_id' => $item_id], $request)->setViewModel('cart')->setActionMethod("addToCart")->response();
    }

    /**
     * @param $type
     * @param $cart_id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function removeCodeFromCart($type, $cart_id, Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['type' => $type, 'cart_id' => $cart_id], $request)->setViewModel('cart')->setActionMethod("removeCodeFromCart")->response();
    }

    /**
     * @param $type
     * @param $cart_id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function removeVoucherCodeFromCart($type, $cart_id, Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['type' => $type, 'cart_id' => $cart_id], $request)->setViewModel('cart')->setActionMethod("removeVoucherCodeFromCart")->response();
    }

    /**
     * @param $item
     * @param $item_id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addCodeToCart(Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setRequest($request)->setViewModel('cart')->setActionMethod("addCodeToCart")->response();
    }

    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteItem($id, Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['id' => $id], $request)->setViewModel('cart')->setActionMethod("deleteItem")->response();
    }

    /**
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteCart(Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setRequest($request)->setViewModel('cart')->setActionMethod("deleteCart")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function invoice(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('cart')->setActionMethod("getInvoice")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('order.save')->setActionMethod("saveOrder")->response();
    }
}
