<?php

namespace Modules\Sale\Http\Controllers\Voucher;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class VoucherController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('voucher.vouchers_manage')->setActionMethod("getViewListIndex")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(MasterViewModel $masterViewModel, Request $request)
    {
        return $this->edit(0,$masterViewModel,$request);
    }




    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id,MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('voucher.edit')->setActionMethod("getForm")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save($id,MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('voucher.edit')->setActionMethod("save")->response();
    }


}
