<?php

namespace Modules\Sale\Http\Controllers\Voucher;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class VoucherCodesController extends Controller
{

    /**
     * @param $voucher_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($voucher_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['voucher_id' => $voucher_id], $request)->setViewModel('voucher.codes.list')->setActionMethod("getViewListIndex")->response();
    }


    /**
     * @param $voucher_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create($voucher_id,MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['voucher_id' => $voucher_id], $request)->setViewModel('voucher.codes.edit')->setActionMethod("getForm")->response();
    }

    /**
     * @param $voucher_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save($voucher_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['voucher_id' => $voucher_id], $request)->setViewModel('voucher.codes.edit')->setActionMethod("save")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['voucher_code_id' => $id], $request)->setViewModel('voucher.codes.edit')->setActionMethod("destroy")->response();
    }



}
