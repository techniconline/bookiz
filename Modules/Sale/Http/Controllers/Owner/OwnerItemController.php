<?php

namespace Modules\Sale\Http\Controllers\Owner;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class OwnerItemController extends Controller
{

    /**
     * @param $owner_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($owner_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['owner_id' => $owner_id], $request)->setViewModel('owner.ownerItemsGrid')->setActionMethod("getViewListIndex")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('owner.ownerItemsActions')->setActionMethod("createOwnerItem")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['owner_item_id'=>$id],$request)->setViewModel('owner.ownerItemsActions')->setActionMethod("createOwnerItem")->response();
    }

    /**
     * @param $owner_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('owner.ownerItemsActions')->setActionMethod("saveOwnerItem")->response();
    }

    /**
     * @param $owner_item_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($owner_item_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['owner_item_id' => $owner_item_id], $request)->setViewModel('owner.ownerItemsActions')->setActionMethod("saveOwnerItem")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['owner_item_id' => $id], $request)->setViewModel('owner.ownerItemsActions')->setActionMethod("destroyOwnerItem")->response();
    }

    /**
     * @param $model_type
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function searchInModel($model_type, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['model_type' => $model_type], $request)->setViewModel('owner.ownerItemsActions')->setActionMethod("searchInModel")->response();
    }

}
