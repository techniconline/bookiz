<?php

namespace Modules\Sale\Http\Controllers\Sale;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class OrderController extends Controller
{

    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function payment($id, Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('order.payment')->setActionMethod("getOrderPayment")->response();
    }


    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function callback($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('order.payment')->setActionMethod("getCallBack")->response();
    }


    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function complete($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('order.payment')->setActionMethod("getComplete")->response();
    }


    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function view($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('order.order')->setActionMethod("getView")->response();
    }

    /**
     * @param $id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function repay($id, Request $request, MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('order.payment')->setActionMethod("getRepay")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function myOrders(MasterViewModel $masterViewModel){
        return $masterViewModel->setViewModel('order.user')->setActionMethod("getList")->response();
    }
}
