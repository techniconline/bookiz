<?php

namespace Modules\Sale\Http\Controllers\Sale;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class CourseReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function courseDate(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('report_course.date')->setActionMethod("getViewListIndex")->response();
    }

    public function courseCategory(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('report_course.category')->setActionMethod("getViewListIndex")->response();
    }

}
