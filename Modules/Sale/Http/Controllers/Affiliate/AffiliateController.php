<?php

namespace Modules\Sale\Http\Controllers\Affiliate;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class AffiliateController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('affiliate.affiliate_manage')->setActionMethod("getViewListIndex")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(MasterViewModel $masterViewModel, Request $request)
    {
        return $this->edit(0,$masterViewModel,$request);
    }


    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id,MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('affiliate.edit')->setActionMethod("getForm")->response();
    }


    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save($id,MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['id'=>$id],$request)->setViewModel('affiliate.edit')->setActionMethod("save")->response();
    }


    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function listAffiliateUses($affiliate_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['affiliate_id'=>$affiliate_id],$request)->setViewModel('affiliate.affiliate_manage')->setActionMethod("getListAffiliateUses")->response();
    }


}
