<?php

Route::group(['middleware' => 'web', 'prefix' => 'sale', 'as' => 'sale.'], function () {

    Route::group(['namespace' => 'Modules\Sale\Http\Controllers\Cart'], function () {
        Route::get('cart/', 'CartController@index')->name('cart');

        Route::get('cart/invoice', 'CartController@invoice')->name('cart.invoice');
        Route::post('cart/add/{item}/{item_id}', 'CartController@add')->name('cart.add');
        Route::get('cart/delete/items/all', 'CartController@deleteCart')->name('cart.all.delete');
        Route::get('cart/delete/{id}', 'CartController@deleteItem')->name('cart.delete');

        Route::group(['middleware' => ['auth']], function () {
            Route::get('cart/booking', 'CartController@getCartBooking')->name('cart.booking');
            Route::get('cart/booking/invoice', 'CartController@getInvoiceBooking')->name('cart.booking.invoice');
            Route::post('cart/booking/save', 'CartController@saveBooking')->name('cart.booking.save');

            Route::get('cart/invoice', 'CartController@invoice')->name('cart.invoice');
            Route::post('cart/save', 'CartController@save')->name('cart.save');
            Route::post('cart/addCode', 'CartController@addCodeToCart')->name('cart.add_code');
            Route::any('cart/remove/{type}/{cart_id}', 'CartController@removeCodeFromCart')->name('cart.remove_code_affiliate');
            Route::any('cart/voucher/remove/{type}/{cart_id}', 'CartController@removeVoucherCodeFromCart')->name('cart.remove_code_voucher');
        });

    });

    Route::group(['prefix' => 'order', 'as' => 'order.', 'namespace' => 'Modules\Sale\Http\Controllers\Sale'], function () {
        Route::get('payment/{id}', 'OrderController@payment')->name('payment');
        Route::any('payment/callback/{id}', 'OrderController@callback')->name('payment.callback');
        Route::get('complete/{id}', 'OrderController@complete')->name('complete');
        Route::get('view/{id}', 'OrderController@view')->name('view');
        Route::get('repay/{id}', 'OrderController@repay')->name('repay');
    });

});


Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'sale', 'as' => 'sale.', 'namespace' => 'Modules\Sale\Http\Controllers\Sale'], function () {

    Route::group(['prefix' => 'order', 'as' => 'order.'], function () {
        Route::get('user/', 'OrderController@myOrders')->name('user');
    });

});

Route::group(['middleware' => ['web', 'auth', 'admin'], 'prefix' => 'sale', 'as' => 'sale.', 'namespace' => 'Modules\Sale\Http\Controllers'], function () {

    Route::group(['prefix' => 'order', 'as' => 'order.', 'namespace' => 'Order'], function () {
        Route::get('/', 'OrderController@index')->name('index');
        Route::get('/edit/{order_id}', 'OrderController@edit')->name('edit');
        Route::get('/payments/{order_id}', 'OrderController@payments')->name('payments');
        Route::delete('{order_id}/destroy', 'OrderController@destroy')->name('delete');
        Route::put('{order_id}/accept', 'OrderController@accept')->name('accept');
    });

    Route::group(['prefix' => 'payment', 'as' => 'payment.', 'namespace' => 'Payment'], function () {
        Route::get('/', 'PaymentController@index')->name('index');
        Route::get('/edit/{payment_id}', 'PaymentController@edit')->name('edit');
        Route::put('{payment_id}/update', 'PaymentController@update')->name('update');
        Route::get('{payment_id}/validate', 'PaymentController@validate')->name('validate');
    });

    Route::group(['prefix' => 'owner', 'as' => 'owner.', 'namespace' => 'Owner'], function () {
        Route::get('/', 'OwnerController@index')->name('index');
        Route::get('/create', 'OwnerController@create')->name('create');
        Route::get('/get_owners', 'OwnerController@getOwnersByApi')->name('get_owners_by_api');
        Route::post('/save', 'OwnerController@save')->name('save');
        Route::get('{owner_id}/edit', 'OwnerController@edit')->name('edit');
        Route::put('{owner_id}/update', 'OwnerController@update')->name('update');
        Route::delete('{owner_id}/delete', 'OwnerController@destroy')->name('delete');

        Route::group(['prefix' => 'items', 'as' => 'items.'], function () {
            Route::get('{owner_id}/list', 'OwnerItemController@index')->name('index');
            Route::get('/create', 'OwnerItemController@create')->name('create');
            Route::post('/save', 'OwnerItemController@save')->name('save');
            Route::get('{owner_item_id}/edit', 'OwnerItemController@edit')->name('edit');
            Route::put('{owner_item_id}/update', 'OwnerItemController@update')->name('update');
            Route::delete('{owner_item_id}/delete', 'OwnerItemController@destroy')->name('delete');
            Route::get('{model_type}/search', 'OwnerItemController@searchInModel')->name('search_model');
        });

    });

    Route::group(['prefix' => 'voucher', 'as' => 'voucher.', 'namespace' => 'Voucher'], function () {
        Route::get('/', 'VoucherController@index')->name('index');
        Route::get('/create', 'VoucherController@create')->name('create');
        Route::post('/save/{id}', 'VoucherController@save')->name('save');
        Route::get('/edit/{id}', 'VoucherController@edit')->name('edit');

        Route::group(['prefix' => 'code', 'as' => 'code.'], function () {
            Route::get('/{voucher_id}', 'VoucherCodesController@index')->name('index');
            Route::get('/create/{voucher_id}', 'VoucherCodesController@create')->name('create');
            Route::post('/save/{voucher_id}', 'VoucherCodesController@save')->name('save');
            Route::delete('/delete/{voucher_id}', 'VoucherCodesController@destroy')->name('delete');
        });
    });

    Route::group(['prefix' => 'affiliate', 'as' => 'affiliate.', 'namespace' => 'Affiliate'], function () {
        Route::get('/', 'AffiliateController@index')->name('index');
        Route::get('/create', 'AffiliateController@create')->name('create');
        Route::post('/save/{id}', 'AffiliateController@save')->name('save');
        Route::get('/edit/{id}', 'AffiliateController@edit')->name('edit');

        Route::group(['prefix' => 'code/uses', 'as' => 'code.'], function () {
            Route::get('/{affiliate_id}', 'AffiliateController@listAffiliateUses')->name('list_uses');
        });
    });

    Route::group(['prefix' => 'report/', 'as' => 'course.report.', 'namespace' => 'Sale'], function () {
        Route::get('/course_date', 'CourseReportController@courseDate')->name('course.date');
        Route::get('/course_category', 'CourseReportController@courseCategory')->name('course.category');
    });

    Route::group(['prefix' => 'config', 'as' => 'config.', 'namespace' => '\Config'], function () {
        Route::get('/instance/index', 'InstanceController@index')->name('instance.index');
        Route::post('/instance/save', 'InstanceController@save')->name('instance.save');
    });

});
