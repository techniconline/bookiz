<?php
namespace Modules\Sale\Bridge\SalesItemsHelper\Facades;

use Illuminate\Support\Facades\Facade;

class SalesItemsHelper extends Facade
{

    /**
     * get sms service
     * @return string
     */
    protected static function getFacadeAccessor() { return 'SalesItemsHelper'; }

}