<?php
namespace Modules\Sale\Bridge\SalesItemsHelper;

/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class SalesItemsHelper
{
    private $saleItems = [
        'Course' => "\\Modules\\Course\\Sale\\Course",
        'CourseActivity' => "\\Modules\\Course\\Sale\\CourseActivity",
        'EntityRelationServices' => "\\Modules\\Entity\\Sale\\EntityRelationService",
        'BookingDetails' => "\\Modules\\Booking\\Sale\\BookingDetail",
    ];
    private $cached=[];


    public function getItem($name)
    {
        $name = $this->getItemName($name);
        if (isset($this->saleItems[$name])) {
//            if(isset($this->cached[$name])){
//                return $this->cached[$name];
//            }
            $this->cached[$name] = new $this->saleItems[$name]();
            return $this->cached[$name];
        }
        return null;
    }

    public function getItemName($name)
    {
        $nameClass = get_uppercase_by_underscores($name);
        return $nameClass;
    }

    public function set($name, $namespace)
    {
        $this->saleItems[$name] = $namespace;
        return $this;
    }

    public function __call($method, $args)
    {
        if (strpos($method, 'get') == 0) {
            $method = str_replace('get', '', $method);
        }
        return $this->get($method);
    }
}