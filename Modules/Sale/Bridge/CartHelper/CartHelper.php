<?php
namespace Modules\Sale\Bridge\CartHelper;

use Modules\Sale\Bridge\SalesItemsHelper\SalesItemsHelper;
use Modules\Sale\Libraries\Cart\CartManager;
use Modules\Sale\Models\Traits\Cart\CartTrait;

/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class CartHelper
{

    use CartTrait;


    private $desingConfigs=[
        'parent_class'=>null,
        'form_class'=>null,
        'button_class'=>null,
        'button_title'=>null,
    ];
    private $itemManager=false;
    private $cartManager=false;

    public function getAddToCart($item_type,$item_id,$options=[],$desingConfigs=[]){
        $desingConfigs=array_merge($this->desingConfigs,$desingConfigs);
        $this->addAssetToTheme();
        return view('sale::cart.add_to_cart',['item_type'=>$item_type,'item_id'=>$item_id,'options'=>$options,'attributes'=>$desingConfigs]);
    }


    private function addAssetToTheme(){
        $assets=[];
        $assets[]=['container'=>'block_js','src'=>'assets/modules/sale/cart.js','name'=>'cart.js'];
        app('getInstanceObject')->setActionAssets($assets);
    }

    public function getItemManager(){
        if($this->itemManager){
            return $this->itemManager;
        }
        $this->itemManager=new SalesItemsHelper();
        return $this->itemManager;
    }


    public function getCurrentCart($create=true){
        if($this->cartManager){
            return $this->cartManager;
        }
        $this->cartManager=new CartManager($create);
        return $this->cartManager;
    }


    public function getCartInfo(){
        if(true || !($cartInfo=$this->getCartInfoFromCache())){
            $cartManager=$this->getCurrentCart(false);
            $cart=$cartManager->getCart();
            if($cart){
                $cartInfo=$cartManager->getCartInfo();
            }else{
                $cartInfo=collect([]);
            }
            $this->updateCartCache($cartInfo);
        }
        if($cartInfo){
            return $cartInfo->first();
        }
        return $cartInfo;
    }


}