<?php
namespace Modules\Sale\Bridge\CartHelper\Facades;

use Illuminate\Support\Facades\Facade;

class CartHelper extends Facade
{

    /**
     * get sms service
     * @return string
     */
    protected static function getFacadeAccessor() { return 'CartHelper'; }

}