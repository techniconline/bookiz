﻿{!! $blockViewModel->getConfig('before_html') !!}
@php $cartInfo= $blockViewModel->getCartInfo(); @endphp
 

 @if($blockViewModel->getConfig('is_mobile_box'))
  <a class="MyBasket_ btn a_plc_30  _nobrd "   href="{{ route('sale.cart') }}" >
                


						  @if($cartInfo && isset($cartInfo->cart_items) && count($cartInfo->cart_items))
         <span class="MyBasket_No _mbole1">{{ count($cartInfo->cart_items) }}</span>
		 @else 
         <span class="MyBasket_No _mbole1">0</span>
	    @endif



                        <i class="fas fa-shopping-cart "></i>
                    </a>
<a class="MyBasket_ btn   _nobrd mrguserleft"  href="{!! route("course.user.my.courses") !!}"> <i class="fas fa-user"></i>  </a> 
  @else
  
    <a class=" btn btn-default d-none d-sm-none d-md-block MyBasket_" href="{{ route('sale.cart') }}" >
   
        @if(!empty($blockViewModel->getConfig('title')))
		 {{$blockViewModel->getConfig('title')}}
        @endif
		
        @if($cartInfo && isset($cartInfo->cart_items) && count($cartInfo->cart_items))
         <span class="MyBasket_No">{{ count($cartInfo->cart_items) }}</span>
		 @else 
         <span class="MyBasket_No">0</span>
	    @endif


		 <i class="{{$blockViewModel->getConfig('icon')}}"></i>

    </a>
   @endif

 
{!! $blockViewModel->getConfig('after_html') !!}

 