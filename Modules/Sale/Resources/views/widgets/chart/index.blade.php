{!! $blockViewModel->getConfig('before_html') !!}
<div class="{{ $blockViewModel->getConfig('class_div') }}" id="{{ $blockViewModel->getConfig('id') }}">

 {!! $blockViewModel->getChart() !!}

</div>
{!! $blockViewModel->getConfig('after_html') !!}