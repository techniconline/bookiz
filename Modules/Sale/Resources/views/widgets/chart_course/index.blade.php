{!! $blockViewModel->getConfig('before_html') !!}
<div class="{{ $blockViewModel->getConfig('class_div') }}" id="{{ $blockViewModel->getConfig('id') }}">

 {!! $blockViewModel->getGridHtml()!!}

</div>
{!! $blockViewModel->getConfig('after_html') !!}