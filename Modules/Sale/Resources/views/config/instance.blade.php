<div class="col-md-12" id="core.instance.config">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cog"></i>
                {{ $BlockViewModel->getTitlePage() }}
            </div>
        </div>
        <div class="portlet-body form">
            <div class="clearfix"></div>

            <div class="clearfix"></div>

            {!! FormHelper::open(['role'=>'form','url'=>route('sale.config.instance.save'),'method'=>'POST','class'=>'form-horizontal','id'=>'config_form','enctype'=>"multipart/form-data"]) !!}
            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp


            {!! FormHelper::input('number','main_owner_id',old('main_owner_id',$BlockViewModel->getSetting('main_owner_id'))
                ,['required'=>'required','title'=>trans('sale::form.fields.main_owner_id'),'helper'=>trans('sale::form.helper.main_owner_id')]) !!}
            {!! FormHelper::checkbox('active_calculate_owner',1,old('active_calculate_owner',$BlockViewModel->getSetting('active_calculate_owner'))
                ,['title'=>trans('sale::form.fields.active_calculate_owner'),'label'=>trans('sale::form.helper.active_calculate_owner')]) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitOnly() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>