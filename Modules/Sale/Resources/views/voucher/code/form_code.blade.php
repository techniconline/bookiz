<div class="col-md-12">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i>{{ $BlockViewModel->getTitlePage() }}
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('sale.voucher.code.save',['id'=>$BlockViewModel->getModelData('id',0)]),'method'=>'POST','class'=>'form-horizontal']) !!}

            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            {!! FormHelper::input('number','count_create_voucher',old('count_create_voucher',1),['required'=>'required','title'=>trans('sale::form.fields.count_create_voucher'),'helper'=>trans('sale::form.fields.count_create_voucher')]) !!}

            {!! FormHelper::input('text','pre_code',old('pre_code',null),['title'=>trans('sale::form.fields.pre_code'),'helper'=>trans('sale::form.fields.pre_code')]) !!}

            {!! FormHelper::input('text','code',old('code',null),['required'=>'required','title'=>trans('sale::form.fields.voucher_code'),'helper'=>trans('sale::form.fields.voucher_code')]) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel([], ['title'=>trans("exam::exam.cancel"), 'url'=>route('sale.voucher.code.index',['voucher_id'=>$BlockViewModel->getModelData('id',0)])]) !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>

