<div class="col-md-12">
   <table class="table table-striped table-responsive table-bordered table-addition">
       <thead>
        <tr>
            <th class="bg-primary  text-center">ردیف</th>
            {{--<th class="bg-primary  text-center">شرط به قبل</th>--}}
            <th class="bg-primary  text-center">نوع</th>
            {{--<th class="bg-primary  text-center">شرط</th>--}}
            <th class="bg-primary  text-center">مقادیر</th>
            {{--<th class="bg-primary  text-center">شرط به بعد</th>--}}
            <th class="bg-primary  text-center">
                <button type="button" class="btn btn-success add-row"><i class="fa fa-plus"></i> اضافه کردن سطر جدید </button>
            </th>
        </tr>
       </thead>
       <tbody>
        <tr class="row-base hidden" id="row-#id">
            <td>#id</td>
            {{--<td>--}}
                {{--{!! FormHelper::select("condition[#id]['before_condition']",trans('sale::data.voucher_use_general_conditions'),null,['placeholder'=>trans("sale::form.placeholder.please_select")]) !!}--}}
            {{--</td>--}}
            <td>
                {!! FormHelper::select("condition[#id]['type']",trans('sale::data.voucher_condition_types'),null,['placeholder'=>trans("sale::form.placeholder.please_select"),'id'=>'condition_type_#id']) !!}
            </td>
            {{--<td>--}}
                {{--{!! FormHelper::select("condition[#id]['operate']",trans('sale::data.voucher_condition_operates'),'',['placeholder'=>trans("sale::form.placeholder.please_select")]) !!}--}}
            {{--</td>--}}
            <td>
                {!! FormHelper::selectTag("condition[#id]['values']",[],null,['data-ajax-url'=>route('core.feature.group.json'),'id'=>'condition_value_#id']) !!}
            </td>
            {{--<td>--}}
                {{--{!! FormHelper::select("condition[#id]['after_condition']",trans('sale::data.voucher_use_general_conditions'),null,['placeholder'=>trans("sale::form.placeholder.please_select")]) !!}--}}
            {{--</td>--}}
            <td class="text-center">
                <button type="button" class="btn btn-danger delete-row"><i class="fa fa-trash"></i> حذف شرط </button>
            </td>
        </tr>
       </tbody>
   </table>
</div>

<script>
    $(document).ready(function () {
        $(".add-row").click(function(e){
            $dataTable=$(this).closest('.table-addition');
            $row=$($dataTable).find('tr.row-base').html();
            $id=$($dataTable).find('tr.data_row').length+1;
            $row=$row.replace("#id", $id);
            $($dataTable).append('<tr class="data_row" id="row-'+$id+'">'+$row+'</tr>');
        });

        $('.table-addition').on('click',".delete-row",function(e){
            $dataTable=$(this).closest('.data_row');
            console.log($dataTable);
            $($dataTable).remove();
        });
    });
</script>
