<div class="col-md-12">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i>{{ $BlockViewModel->getTitlePage() }}
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('sale.voucher.save',['id'=>$BlockViewModel->getModelData('id',0)]),'method'=>'POST','class'=>'form-horizontal']) !!}

            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            {!! FormHelper::input('text','title',old('title',$BlockViewModel->getModelData('title')),['required'=>'required','title'=>trans('sale::form.fields.title'),'helper'=>trans('sale::form.helper.title')]) !!}
            {!! FormHelper::select('type',trans('sale::data.voucher_type'),old('type',$BlockViewModel->getModelData('type')),['title'=>trans('sale::form.fields.type'),'helper'=>trans('sale::form.helper.type'),'placeholder'=>trans("sale::form.placeholder.please_select")]) !!}
            {!! FormHelper::input('text','value',old('value',$BlockViewModel->getModelData('value')),['required'=>'required','title'=>trans('sale::form.fields.value'),'helper'=>trans('sale::form.helper.value')]) !!}
            {!! FormHelper::input('text','max_value',old('max_value',$BlockViewModel->getModelData('max_value')),['required'=>'required','title'=>trans('sale::form.fields.max_value'),'helper'=>trans('sale::form.helper.max_value')]) !!}
            {!! FormHelper::select('currency_id',BridgeHelper::getCurrencyHelper()->getCurrencyList(true),old('currency_id'
            ,$BlockViewModel->getModelData('currency_id',BridgeHelper::getCurrencyHelper()->getCurrentCurrency()->id))
            ,['required'=>'required','title'=>trans("course::enrollment.payments.currency"),'helper'=>trans("course::enrollment.payments.select_currency")
            ,'placeholder'=>trans("course::enrollment.payments.select_currency")]) !!}
            {!! FormHelper::input('number','max_qty',old('max_qty',$BlockViewModel->getModelData('max_qty')),['required'=>'required','title'=>trans('sale::form.fields.max_qty'),'helper'=>trans('sale::form.helper.max_qty')]) !!}
            {!! FormHelper::select('use_type',trans('sale::data.voucher_use_type'),old('use_type',$BlockViewModel->getModelData('use_type')),['title'=>trans('sale::form.fields.use_type'),'helper'=>trans('sale::form.helper.use_type'),'placeholder'=>trans("sale::form.placeholder.please_select")]) !!}
            {!! FormHelper::input('number','use_qty',old('use_qty',$BlockViewModel->getModelData('use_qty')),['required'=>'required','title'=>trans('sale::form.fields.use_qty'),'helper'=>trans('sale::form.helper.use_qty')]) !!}
            {!! FormHelper::select('active',trans("sale::data.statuses"),old('active',$BlockViewModel->getModelData('active',1))
            ,['title'=>trans("sale::form.fields.status"),'helper'=>trans("sale::form.fields.status")
            ,'placeholder'=>trans("sale::form.placeholder.please_select")]) !!}
            {!! FormHelper::input('duration','duration',old('duration',$BlockViewModel->getModelData('duration')),['required'=>'required','title'=>trans('sale::form.fields.duration_day'),'helper'=>trans('sale::form.helper.duration_day')]) !!}
            {!! FormHelper::legend(trans('sale::form.titles.voucher_conditions')) !!}
            @php  FormHelper::setCustomAttribute('element','class','col-md-12') @endphp
            @include('sale::voucher.condition')
            @php  FormHelper::setCustomAttribute('element','class','col-md-8') @endphp
            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>

