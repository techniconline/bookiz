<div class="col-md-12">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i>{{ $BlockViewModel->getTitlePage() }}
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('sale.affiliate.save',['id'=>$BlockViewModel->getModelData('id',0)]),'method'=>'POST','class'=>'form-horizontal']) !!}

            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            {!! FormHelper::selectTag('user_id',[],old('user_id',$BlockViewModel->getModelData('user_id')),['data-ajax-url'=>BridgeHelper::getUser()->getUrlSearchUser() , 'title'=>trans("sale::form.fields.user"),'helper'=>trans("sale::form.fields.select_user")]) !!}
            {!! FormHelper::select('type',trans('sale::data.voucher_type'),old('type',$BlockViewModel->getModelData('type')),['title'=>trans('sale::form.fields.type'),'helper'=>trans('sale::form.helper.type'),'placeholder'=>trans("sale::form.placeholder.please_select")]) !!}
            {!! FormHelper::input('float','owner_value',old('owner_value',$BlockViewModel->getModelData('owner_value',0.1)),['required'=>'required','title'=>trans('sale::form.fields.owner_value'),'helper'=>trans('sale::form.helper.owner_value')]) !!}
            {!! FormHelper::input('number','value',old('value',$BlockViewModel->getModelData('value',0)),['required'=>'required','title'=>trans('sale::form.fields.value'),'helper'=>trans('sale::form.helper.value')]) !!}
            {!! FormHelper::input('number','max_value',old('max_value',$BlockViewModel->getModelData('max_value',0)),['required'=>'required','title'=>trans('sale::form.fields.max_value'),'helper'=>trans('sale::form.helper.max_value')]) !!}

            {!! FormHelper::input('text','code',old('code',$BlockViewModel->getModelData('code',strtoupper(str_random(10)))),['required'=>'required','title'=>trans('sale::form.fields.affiliate_code'),'helper'=>trans('sale::form.helper.affiliate_code')]) !!}

            {!! FormHelper::select('currency_id',BridgeHelper::getCurrencyHelper()->getCurrencyList(true),old('currency_id'
            ,$BlockViewModel->getModelData('currency_id',BridgeHelper::getCurrencyHelper()->getCurrentCurrency()->id))
            ,['required'=>'required','title'=>trans("course::enrollment.payments.currency"),'helper'=>trans("course::enrollment.payments.select_currency")
            ,'placeholder'=>trans("course::enrollment.payments.select_currency")]) !!}

            {!! FormHelper::datetime('active_date',old('active_date',$BlockViewModel->getModelData('active_date')),['title'=>trans("sale::form.fields.active_date")
                ,'date-year-current'=>2,'date-year-before'=>2,'set-default'=>0]) !!}

            {!! FormHelper::datetime('expire_date',old('expire_date',$BlockViewModel->getModelData('expire_date')),['title'=>trans("sale::form.fields.expire_date")
                ,'date-year-current'=>2,'date-year-before'=>2,'set-default'=>0]) !!}

            {!! FormHelper::input('number','max_qty',old('max_qty',$BlockViewModel->getModelData('max_qty',0)),['required'=>'required','title'=>trans('sale::form.fields.max_qty'),'helper'=>trans('sale::form.helper.max_qty')]) !!}
            {!! FormHelper::select('active',trans("sale::data.statuses"),old('active',$BlockViewModel->getModelData('active',1))
            ,['title'=>trans("sale::form.fields.status"),'helper'=>trans("sale::form.fields.status")
            ,'placeholder'=>trans("sale::form.placeholder.please_select")]) !!}

            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>

