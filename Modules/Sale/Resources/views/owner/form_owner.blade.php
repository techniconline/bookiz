<div class="col-md-12">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-edit"></i>{{ $view_model->getTitlePage() }}
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>$view_model->getModelData('id')? route('sale.owner.update',['owner_id'=>$view_model->getModelData('id')]) : route('sale.owner.save'),'method'=>$view_model->getModelData('id')?'PUT':'POST','class'=>'form-horizontal']) !!}

            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

            {!! FormHelper::input('text','title',old('title',$view_model->getModelData('title')),['required'=>'required','title'=>trans('sale::owner.owner_title'),'helper'=>trans('sale::owner.owner_title')]) !!}

            {!! FormHelper::input('text','value',old('value',$view_model->getModelData('value')),['title'=>trans('sale::owner.value'),'helper'=>trans('sale::owner.value')]) !!}
            {!! FormHelper::input('text','max_value',old('max_value',$view_model->getModelData('max_value')),['title'=>trans('sale::owner.max_value'),'helper'=>trans('sale::owner.max_value')]) !!}
            
            {!! FormHelper::select('currency_id',BridgeHelper::getCurrencyHelper()->getCurrencyList(true),old('currency_id'
             ,$view_model->getModelData('currency_id',BridgeHelper::getCurrencyHelper()->getCurrentCurrency()->id))
            ,['required'=>'required','title'=>trans("course::enrollment.payments.currency"),'helper'=>trans("course::enrollment.payments.select_currency")
            ,'placeholder'=>trans("course::enrollment.payments.select_currency")]) !!}
            
            {!! FormHelper::select('type',trans('sale::owner.types'),old('type',$view_model->getModelData('type')),['title'=>trans('sale::owner.type'),'helper'=>trans('sale::owner.type'),'placeholder'=>trans("sale::form.placeholder.please_select")]) !!}

            {!! FormHelper::select('active',trans("sale::data.statuses"),old('active',$view_model->getModelData('active',1))
            ,['title'=>trans("sale::owner.status"),'helper'=>trans("sale::owner.status")
            ,'placeholder'=>trans("sale::form.placeholder.please_select")]) !!}
            
            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitByCancel(['title'=>trans("sale::owner.submit")], ['title'=>trans("sale::owner.cancel"), 'url'=>route('sale.owner.index')]) !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>

