@if($cart->affiliate_id)
<tr>
    <td class="text-primary"><a href="{!! route('sale.cart.remove_code_affiliate', ['type'=>'affiliate','cart_id'=>$cart->id]) !!}"><i class="fa fa-times-circle" style="color: red !important;"></i></a></td>
    <td class="text-primary" colspan="{{ $col }}"><strong>@lang('sale::cart.text_affiliate_code_price')</strong></td>
    <td class="text-primary"><strong> {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cart->affiliate_amount,$cart->currency_id,true,true) }}</strong></td>
</tr>
@endif