<div>
    <button type="button" class="swal2-close-custom btn btn-danger">@lang("sale::form.fields.close")</button>
    <a href="{!! route('sale.cart') !!}"  class="btn btn-success">@lang("sale::form.fields.go_cart")</a>
</div>