@php $currentCart=$blockViewModel->getCurrentCart();  @endphp

<div class="checkout-page">
    <div class="content">
        <div class="options">
            <div class="form-group">
                <label for="employees">Select Employee</label>
                <select class="form-control" id="employees">
                    <option value="1">Any employee</option>
                    <option value="2">Ahmad Khorshidi</option>
                    <option value="3">Ali Mortazavi</option>
                </select>
            </div>
            <div class="form-group">
                <label for="employees">Select start time for your appointment</label>
                <div id="date-calendar"  data-dates='["01/02/2019","02/02/2019","03/02/2019","05/02/2019"]' class="material-theme"></div>
                <ul class="select-times">
                    <li class="active">
                        <span class="time">10:45 AM</span>
                        <span class="prices">
                            <span class="old-price">$175</span>
                            <span class="new-price">$165</span>
                        </span>
                    </li>
                    <li>
                        <span class="time">11:00 AM</span>
                        <span class="prices">
                            <span class="old-price">$175</span>
                            <span class="new-price">$165</span>
                        </span>
                    </li>
                    <li>
                        <span class="time">11:15 AM</span>
                        <span class="prices">
                            <span class="old-price">$175</span>
                            <span class="new-price">$165</span>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="basket">
            <h4>@lang('sale::cart.cart')</h4>
            @if($cart=$currentCart->getCart())
                <ul>
                    @php $disountedOnProducts=0.00 @endphp
                    @foreach($cart->cartItems as $cartItem)
                        @php
                            $item=$blockViewModel->getSaleItem($cartItem->item_type,$cartItem->item_id,$cartItem->options_array);
                            $itemDiscountPrice=BridgeHelper::getCurrencyHelper()->getConvertPrice($cartItem->discount_amount,$cartItem->currency_id,$cart->currency_id);
                            $disountedOnProducts=$disountedOnProducts+$itemDiscountPrice;
                        @endphp
                        @if($item)
                            @php $itemExtraData=$item->getItemExtraData(); @endphp
                            <li>
                            <span class="detail">
                                <span class="title">{{ $cartItem->name }}</span>
                                @if($cartItem->discount_amount)
                                    <span class="discount" style="color: green; font-size: 12px">@lang('sale::cart.discount') {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cartItem->discount_amount,$cartItem->currency_id,true) }}</span>
                                @endif
                                @if($itemExtraData['calendar'])
                                <span class="duration">{!! $itemExtraData['calendar']->period_time  !!}</span>
                                @endif
                            </span>
                                <span class="right">
                                <span class="price">{{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cartItem->total_amount,$cartItem->currency_id,true) }}</span>

                                    {{--<a href="javascript:void(0)" class="delete">x</a>--}}
                                    <a class="text-danger delete"
                                       href="{{ route('sale.cart.delete',['id'=>$cartItem->id]) }}">
                                   <i class="fas fa-trash-alt"></i>
                                </a>
                            </span>
                            </li>
                        @else
                            @php
                                $deletedId=[$cartItem->id];
                                $cartItem->delete();
                                $currentCart->updateCart(null,$deletedId);
                            @endphp
                        @endif

                    @endforeach
                    <li>
                        <a class="add-more" href="{!! $itemExtraData['entity_url'] !!}"><i class="fas fa-plus-circle"></i> @lang('sale::cart.add_another_service')</a>
                    </li>
                </ul>
                <a href="{!! route('sale.cart.booking.invoice') !!}" type="button" class="btn btn-block">@lang('sale::cart.save_order')</a>
            @else
                <ul>
                    <li>
                        <a class="add-more" href="{!! request()->header('referer') !!}"><i class="fas fa-plus-circle"></i> @lang('sale::cart.add_another_service')</a>
                    </li>
                </ul>
            @endif

        </div>
    </div>
</div>
