@if($cart->promotion_id)
<tr>
    <td class="text-danger" colspan="{{ $col }}"><strong> تخفیف 20 درصد روی اولین محصول تحصیلی</strong></td>
    <td class="text-danger"><strong> {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cart->promotion_amount,$cart->currency_id,true,true) }}</strong></td>
</tr>
@endif