@if($cart->coupen)
    <tr>
        <td class="text-danger"><a href="{!! route('sale.cart.remove_code_voucher', ['type'=>'coupen','cart_id'=>$cart->id]) !!}"><i class="fa fa-times-circle" style="color: red !important;"></i></a></td>
        <td class="text-danger" colspan="{{ $col }}"><strong>@lang('sale::cart.text_voucher_code_price')</strong></td>
        <td class="text-danger"><strong> {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cart->coupen_amount,$cart->currency_id,true,true) }}</strong></td>
    </tr>
@endif