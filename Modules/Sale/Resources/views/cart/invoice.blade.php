<div class="container-fluid bg_1 floatright">
    <div class="container">
        <div class="_shop_basket2   ">
            @lang('sale::cart.invoice')
        </div>
        <div class="_shop_basket31 ">
            <div class=" col-xl-8 mx-auto col-md-10 col-sm-12 col-xs-12  _tbl_hover _shop_basket63">
                @php
                    $currentCart=$blockViewModel->getCurrentCart();
                $cart=$currentCart->getCart();
                @endphp
                @if($cart)
                    <table class="table table-hover Basket_tbl1">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">@lang('sale::cart.product_code')</th>
                            <th scope="col">@lang('sale::cart.title')</th>
                            <th scope="col">@lang('sale::cart.cost') </th>
                            <th scope="col">@lang('sale::cart.discount') </th>
                            <th scope="col">@lang('sale::cart.qty') </th>
                            <th scope="col">@lang('sale::cart.total') </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $disountedOnProducts=0.00 @endphp
                        @foreach($cart->cartItems as $cartItem)
                            @php
                                $item=$blockViewModel->getSaleItem($cartItem->item_type,$cartItem->item_id);
                                $itemDiscountPrice=BridgeHelper::getCurrencyHelper()->getConvertPrice($cartItem->discount_amount,$cartItem->currency_id,$cart->currency_id);
                                $disountedOnProducts=$disountedOnProducts+$itemDiscountPrice;
                            @endphp
                            @if($item)
                                @php $itemExtraData=$item->getItemExtraData(); @endphp
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>-</td>
                                    <td>
                                        @if($itemExtraData && isset($itemExtraData['url']))
                                            <a href="{!! $itemExtraData['url'] !!}">
                                                {{ $cartItem->name }}
                                            </a>
                                        @else
                                            {{ $cartItem->name }}
                                        @endif
                                    </td>
                                    <td class="color6">{{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cartItem->amount,$cartItem->currency_id,true) }}</td>
                                    <td>{{ ($itemDiscountPrice==0)?'-':BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($itemDiscountPrice,$cartItem->currency_id,true) }}</td>
                                    <td> {{ $cartItem->qty }}</td>
                                    <td class="color6">{{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cartItem->total_amount,$cartItem->currency_id,true) }}</td>
                                </tr>
                            @else
                                @php
                                    $deletedId=[$cartItem->id];
                                    $cartItem->delete();
                                    $currentCart->updateCart(null,$deletedId);
                                @endphp
                            @endif
                        @endforeach
                        <tr>
                            <td colspan="6"><strong>@lang('sale::cart.total_amount')</strong></td>
                            <td>
                                <strong>{{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cart->amount,$cart->currency_id,true) }}</strong>
                            </td>
                        </tr>
                        @include('sale::cart.promotion',['cart'=>$cart,'col'=>6])
                        @include('sale::cart.affiliate',['cart'=>$cart,'col'=>5])
                        {{--                        @include('sale::cart.discount',['cart'=>$cart,'col'=>5,'discount'=>$disountedOnProducts])--}}
                        @include('sale::cart.discount',['cart'=>$cart,'col'=>5])
                        <tr>
                            <td class="text-success" colspan="6"><strong>@lang('sale::cart.payable_amount')</strong>
                            </td>
                            <td class="text-success">
                                <strong> {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cart->payable_amount,$cart->currency_id,true) }}</strong>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                @else
                    {!! $blockViewModel->getComponent('message',['error'=>trans('sale::message.cart_empty')]) !!}
                @endif

                @if($cart && (!$cart->affiliate_id && !$cart->coupen))

                    {!! FormHelper::open(['role'=>'form','url'=>route('sale.cart.add_code'),'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                    <div class="row col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12  _tbl_hover " style="">
                        <div class="    _shop_basket3">
                            <input id="_code" type="text" name="code" class="form-control txt-controler "
                                   placeholder="@lang('sale::cart.add_code')"/>
                            <input id="_submit_code" type="submit" value="@lang('sale::cart.check_code')"
                                   class="btn bgcolor8 color5 _btn7 "/>
                        </div>
                    </div>

                    {!! FormHelper::close() !!}

                @endif

            </div>


            @if($cart)

                {!! FormHelper::open(['role'=>'form','url'=>route('sale.cart.save'),'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
                @if($cart->payable_amount)
                    {!! $blockViewModel->getMasterPayment()->getViewForm('payment') !!}
                @else
                    {!! FormHelper::input('hidden','payment','0') !!}
                @endif
                <div class="UnderTableLinks_">
                    <button type="submit" class="empty_btn">
                        <div class="btn btn1 bgcolor4 color5">

                        <span class="btn2 Fsize18">
                              @lang('sale::cart.save_order')
                        </span>
                            <span class="btn3 ">
                                @lang('sale::cart.pay_order')
                        </span>
                            <span class="btn4 Fsize18">
                           <i class="fas fa-arrow-left"></i>
                        </span>
                        </div>
                    </button>

                    <a class="" href="{{ route('sale.cart') }}">
                        <div class="btn btn1">
                    <span class="btn2">
                        @lang('sale::cart.edit_cart')
                    </span>
                            <span class="btn3">
                        @lang('sale::cart.add_new_product')
                    </span>
                            <span class="btn4 Fsize18 btn6">
                    <i class="fas fa-plus"></i>
                    </span>
                        </div>
                    </a>

                </div>
                {!! FormHelper::close() !!}
            @endif
        </div>
    </div>
</div>


