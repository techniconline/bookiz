<div class="{{ isset($attribute['parent_class'])?$attribute['parent_class']:'add-cart-box' }}">
{!! FormHelper::open(['role'=>'form','url'=>route('sale.cart.add',['item'=>$item_type,'item_id'=>$item_id]),'method'=>'POST','class'=>isset($attribute['form_class'])?$attribute['form_class']:'add-cart-form','']) !!}
    {!! FormHelper::hidden('item',$item_type) !!}
    {!! FormHelper::hidden('item_id',$item_id) !!}

    @foreach($options as $name=>$value)
    {!! FormHelper::hidden('options['.$name.']',$value) !!}
    @endforeach
<button type="submit" class="cart-add-submit btn btn-success core14-btn {{isset($attribute['button_class'])?$attribute['button_class']:''}} ">
    <i class="fa fa-cart-plus"></i>
    <span>{{isset($attribute['button_title'])?$attribute['button_title']:trans('sale::sale.add_to_cart')}}</span>
</button>
{!! FormHelper::close() !!}
</div>