@php $order=$viewModel->getOrder();  @endphp
<div class="col-xl-12 col-lg-12 col-md-12  floatright   dash6  "  style="text-align: center">
    <div class="dash7">
        <h2>
            @lang('sale::sale.order.view',['order_id'=>$order->reference_code])
        </h2>
    </div>
    <div>
        @if($viewModel->application == 'owner_panel')
            <a href="{!! $viewModel->getConfigMobile("back_url_to_owner_panel") !!}" class="btn btn-primary">
                @lang('sale::sale.back_to_app')
            </a>
        @else
            <a href="{!! $viewModel->getConfigMobile("prefix_back_url_to_app").$viewModel->getCurrentInstance()->app_name.$viewModel->getConfigMobile("post_back_url_to_app") !!}" class="btn btn-primary">
                @lang('sale::sale.back_to_app')
            </a>
        @endif

    </div>
    <div class=" col-xl-12 col-lg-12 col-md-12  _tbl_hover _shop_basket63">
        <table class="table table-hover  Basket_tbl1">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">@lang('sale::cart.product_code')</th>
                <th scope="col">@lang('sale::cart.title')</th>
                <th scope="col">@lang('sale::cart.cost') </th>
                <th scope="col">@lang('sale::cart.qty') </th>
                <th scope="col">@lang('sale::cart.total') </th>
            </tr>
            </thead>
            <tbody>
            @foreach($order->orderItems as $cartItem)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>-</td>
                    <td> {{ $cartItem->name }}</td>
                    <td class="color6">{{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cartItem->amount,$cartItem->currency_id,true) }}</td>
                    <td> {{ $cartItem->qty }}</td>
                    <td class="color6">{{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cartItem->total_amount,$cartItem->currency_id,true) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class=" col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12  _tbl_hover ">
            <div class=" row  _shop_basket4">
                <h5 class="color6">
                    <span>@lang('sale::cart.total_amount')</span>
                    {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($order->amount,$order->currency_id,true) }}
                </h5>
            </div>
            <div class=" row  _shop_basket4">
                <h5 class="color6">
                    <span>@lang('sale::cart.payable_amount')</span>
                    {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($order->payable_amount,$order->currency_id,true) }}
                </h5>
            </div>
        </div>
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12  _tbl_hover ">
            @php $payments=$viewModel->getOrderPayments($order->id); @endphp
            @include('sale::order.order_payments',['payments'=>$payments,'viewModel'=>$viewModel])
        </div>
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12  _tbl_hover ">
            @php $messages=$viewModel->getOrderMessages($order->id); @endphp
            @include('sale::order.order_messages',['messages'=>$messages,'viewModel'=>$viewModel])
        </div>
    </div>
</div>

