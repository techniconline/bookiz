@php $order=$viewModel->getOrder();  @endphp
<div class="col-xl-12 col-lg-12 col-md-12  floatright   dash6  ">
    <div class="dash7">
        <h2>
            @lang('sale::sale.order.view',['order_id'=>$order->reference_code])
        </h2>
    </div>
    <div class=" col-xl-12 col-lg-12 col-md-12  _tbl_hover _shop_basket63">
        @if($viewModel->canRePay($order))
            <div class="col-md-12">
                <a class="btn btn-success btn-large" href="{!! route('sale.order.repay',['id'=>$order->id,'reference_code'=>$order->reference_code]) !!}">
                    @lang('sale::sale.order.repay')
                </a>
            </div>
         @endif
        <table class="table table-hover  Basket_tbl1">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">@lang('sale::cart.product_code')</th>
                <th scope="col">@lang('sale::cart.title')</th>
                <th scope="col">@lang('sale::cart.cost') </th>
                <th scope="col">@lang('sale::cart.qty') </th>
                <th scope="col">@lang('sale::cart.total') </th>
            </tr>
            </thead>
            <tbody>
            @foreach($order->orderItems as $cartItem)
                @php
                    $itemExtraData=[];
                    $item=$viewModel->getSaleItem($cartItem->item_type,$cartItem->item_id,$cartItem->options_array);
                    if($item){
                        $itemExtraData=$item->getItemExtraData();
                    }
                @endphp
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>-</td>
                    <td>
                        @if($itemExtraData && isset($itemExtraData['url']))
                            <a href="{!! $itemExtraData['url'] !!}">
                                {{ $cartItem->name }}
                            </a>
                        @else
                            {{ $cartItem->name }}
                        @endif
                    </td>
                    <td class="color6">{{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cartItem->amount,$cartItem->currency_id,true) }}</td>
                    <td> {{ $cartItem->qty }}</td>
                    <td class="color6">{{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($cartItem->total_amount,$cartItem->currency_id,true) }}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="5"><strong>@lang('sale::cart.total_amount')</strong></td>
                <td><strong>{{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($order->amount,$order->currency_id,true) }}</strong></td>
            </tr>
            @include('sale::order.promotions',['order'=>$order,'col'=>5])
            <tr>
                <td class="text-success" colspan="5"><strong>@lang('sale::cart.payable_amount')</strong></td>
                <td class="text-success"><strong> {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($order->payable_amount,$order->currency_id,true) }}</strong></td>
            </tr>
            </tbody>
        </table>
        <div class=" col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12  _tbl_hover ">
        </div>
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12  _tbl_hover ">
            @php $payments=$viewModel->getOrderPayments($order->id); @endphp
            @include('sale::order.order_payments',['payments'=>$payments,'viewModel'=>$viewModel])
        </div>
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12  _tbl_hover ">
            @php $messages=$viewModel->getOrderMessages($order->id); @endphp
            @include('sale::order.order_messages',['messages'=>$messages,'viewModel'=>$viewModel])
        </div>
    </div>
</div>

