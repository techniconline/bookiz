@if($messages->count())
    <table class="table table-hover Basket_tbl1">
        <caption>@lang('sale::message.details')</caption>
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">@lang('sale::message.message')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($messages as $message)
            <tr>
                <td scope="row">{{ $loop->iteration }}</td>
                <td>{!! $message->message !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif