@if($payments->count())
    <table class="table table-hover Basket_tbl1">
        <caption>@lang('sale::payment.payments')</caption>
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">@lang('sale::payment.type')</th>
            <th scope="col">@lang('sale::payment.amount')</th>
            <th scope="col">@lang('sale::payment.reference_id') </th>
            <th scope="col">@lang('sale::payment.status') </th>
            <th scope="col">@lang('sale::payment.datetime') </th>
            <th scope="col">@lang('sale::payment.description') </th>
        </tr>
        </thead>
        <tbody>
        @foreach($payments as $payment)
            <tr>
                <td scope="row">{{ $loop->iteration }}</td>
                <td>@lang('sale::payment.types.'.$payment->type)</td>
                <td class="{{ (in_array($payment->state,$viewModel->getPaymentSuccessStates()))?'color6':'text-danger' }}"> {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($payment->amount,$payment->currency_id,true) }}</td>
                <td> {{ empty($payment->reference_id)?'-':$payment->reference_id }}</td>
                <td class="{{ (in_array($payment->state,$viewModel->getPaymentSuccessStates()))?'color6':'text-danger' }}">@lang('sale::payment.states.'.$payment->state)</td>
                <td> {{ DateHelper::setDateTime($payment->updated_at)->getLocaleFormat(trans('core::date.datetime.medium')) }}</td>
                <td>{{ $payment->description }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif