@if($order->orderPromotions)
    @foreach($order->orderPromotions as $promotion)
        @if($promotion->type=='promotion')
            <tr>
                <td class="text-danger" colspan="{{ $col }}"><strong> تخفیف 20 درصد روی اولین محصول تحصیلی</strong></td>
                <td class="text-danger">
                    <strong> {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($promotion->amount,$order->currency_id,true,true) }}</strong>
                </td>
            </tr>
        @endif

        @if($promotion->type=='affiliate')
            <tr>
                <td class="text-primary" colspan="{{ $col }}">
                    <strong>@lang('sale::cart.text_affiliate_code_price')</strong></td>
                <td class="text-primary">
                    <strong> {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($promotion->amount,$order->currency_id,true,true) }}</strong>
                </td>
            </tr>
        @endif

        @if($promotion->type=='coupen')
            <tr>
                <td class="text-primary" colspan="{{ $col }}">
                    <strong>@lang('sale::cart.text_voucher_code_price') ({!! isset($promotion->options->voucher_code)?$promotion->options->voucher_code:'-' !!})</strong></td>
                <td class="text-primary">
                    <strong> {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($promotion->amount,$order->currency_id,true,true) }}</strong>
                </td>
            </tr>
        @endif

    @endforeach
@endif