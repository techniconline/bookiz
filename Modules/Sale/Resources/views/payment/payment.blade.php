<div class="col-xl-8 mx-auto col-md-12 col-sm-12 col-xs-12     mrg_btn_20 pad_top_30 ">
    {!! FormHelper::input('hidden','payment','online') !!}
    <nav class="  navbar-default">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                @foreach($list as $payment)
                <div class="panel-heading" role="tab" id="heading{{$payment->getName()}}">
                    <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$payment->getName()}}" aria-expanded="true" aria-controls="collapse{{$payment->getName()}}">
                                @lang('sale::payment.'.$payment->getName())
                                <span class="pull-left">
                                     <i class="fas fa-angle-up _collaps_me"></i><i class="fas fa-angle-down "></i>
                                </span>
                            </a>
                    </h4>
                </div>
                @endforeach
            </div>
        </div>
        @foreach($list as $payment)
        <div id="collapse{{$payment->getName()}}" class="panel-collapse in collapse show" role="tabpanel" aria-labelledby="heading{{$payment->getName()}}">
            <div class="panel-body">
                <div class="Online_1">
                    {!! $payment->getRenderView($template) !!}
                </div>
            </div>
        </div>
        @endforeach
    </nav>


	<br><br>
</div>