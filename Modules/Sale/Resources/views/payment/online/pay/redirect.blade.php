<div class="payment-connect-info">
    @lang('sale::payment.connecting');
</div>
<form method="POST" action="{{ $action }}" id="payment_request">
</form>
<script type="text/javascript">
    window.onload = function(){
        document.forms["payment_request"].submit();
    }
</script>