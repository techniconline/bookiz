<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("sale::sale.payment.edit_by_order",['reference_code'=>isset($payment->order->reference_code)?$payment->order->reference_code:'--'])
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>
{{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>route('sale.payment.update', ['payment_id'=>$payment->id])
                        ,'method'=>'PUT','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">

                            <div class="tab-content">
                                <div class="tab-pane active" id="public">
                                    @php

                                    $arr_reference = ['required'=>'required','title'=>trans("sale::payment.reference_id"),'helper'=>trans("sale::payment.reference_id")];
                                    if ($payment->reference_id){
                                    $arr_reference = array_merge($arr_reference, ['disabled'=>'disabled']);
                                    }

                                    $arr_state = ['title'=>trans("sale::payment.state"),'helper'=>trans("sale::payment.state"),'placeholder'=>trans("sale::payment.state")];
                                    if ($payment->state==$view_model->getPaymentCompleteState()){
                                    $arr_state = array_merge($arr_state, ['disabled'=>'disabled']);
                                    }



                                    @endphp

                                    {!! FormHelper::input('text','reference_id',old('reference_id',$payment?$payment->reference_id:null)
                                        ,$arr_reference) !!}

                                    {!! FormHelper::select('state',trans('sale::payment.states'),old('state',$payment?$payment->state:null)
                                    ,$arr_state) !!}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("sale::sale.submit")], ['title'=>trans("sale::sale.cancel"), 'url'=>route('sale.order.payments',['order_id'=>isset($payment->order->id)?$payment->order->id:0])]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>
