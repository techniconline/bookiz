<?php
return [
    'module' => 'فروش',
    'cart_block' => 'سبد خرید',

    'chart' => 'چارت',
    'chart_block' => 'بلوک چارت',
    'chart_course' => 'چارت دوره ها',
    'chart_course_block' => 'چارت دوره ها',
    'advanced' => 'پیشرفته',
    'mobile' => 'موبایل',
    'report_course_block' => 'گزارش فروش محصولات',

    'chart_models' => [
        'orders' => 'سفارشات',
        'payments' => 'پرداختها',
    ],

    'chart_course_models' => [
        'sale_top_amount_courses'=>'پر فروش ترین دوره ها بر اساس  قیمت',
        'sale_top_qty_courses'=>'پر فروش ترین دوره ها بر اساس  تعداد',
    ],

    'report_course_models' => [
        'sale_top_amount_courses'=>'پر فروش ترین دوره ها بر اساس  قیمت',
        'sale_top_qty_courses'=>'پر فروش ترین دوره ها بر اساس  تعداد',
    ],

    'charts'=>[
        'title_order'=>'نمودار سفارشات',
        'title_order_pay'=>'نمودار مبلغ سفارشات',
        'title_order_not_complete'=>'نمودار سفارشات تکمیل نشده',
        'title_sale_top_amount_courses'=>'پر فروش ترین دوره ها بر اساس  قیمت',
        'title_sale_top_qty_courses'=>'پر فروش ترین دوره ها بر اساس تعداد',
        'title_payment'=>'ترکنشها',
        'title_payment_success'=>'تراکنشهای موفق',
        'title_payment_success_zarin'=>'تراکنشهای موفق‌ زرین پال',
        'title_payment_success_asan'=>'تراکنشهای موفق آسان پرداخت',
        'title_payment_fail'=>'تراکنشهای ناموفق',
    ],


];