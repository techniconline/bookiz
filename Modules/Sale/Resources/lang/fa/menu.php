<?php

return [
    'group' => [
        'orders' => 'مدیریت سفارشات',
        'sale'=>'مدیریت فروش',
    ],
    'orders_list' => 'مدیریت سفارشات',
    'payment_list' => 'مدیریت پرداختها',
    'owner_list' => 'مدیریت مالکین',
    'voucher_manage' => 'مدیریت کد های تخفیف',
    'affiliate_manage' => 'مدیریت کد های مشارکت',
    'config'=>[
        'sale_instance'=>'تنظیمات اصلی بخش فروش',
    ],
];