<?php
return [
    'active'=>[
        '1'=>'فعال',
        '0'=>'غیر فعال',
        '2'=>'حذف شده',
    ],
    'duration'=>[
        'day'=>'روز',
        'minute'=>'ماه',
    ],
    'voucher_type'=>[
        'percent'=>'درصد',
        'fixed'=>'مبلغ ثابت',
    ],
    'voucher_use_type'=>[
        'unlimited'=>'بصورت نامحدود',
        'disposable_per_user'=>'یک بار برای هر کاربر',
        'disposable_per_use_qty'=>'به تعداد تعیین شده',
    ],
    'statuses' => [
        "0" => "حذف شده",
        "1" => "فعال",
        "2" => "غیرفعال",
    ],
    'voucher_use_general_conditions'=>[
        'and'=>'و',
        'or'=>'یا',
    ],
    'voucher_condition_operates'=>[
        'equals'=>'برابر باشد',
        'not_equals'=>'برابر نباشد',
        'include'=>'شامل باشد',
        'not_include'=>'شامل نباشد',
        'equals_in'=>'برابر با یکی باشد',
        'greater_than'=>'بزرگتر باشد',
        'less_than'=>'کوچکتر باشد',
    ],
    'voucher_condition_types'=>[
        'category_id'=>'شناسه دسته بندی درس',
        'course_id'=>'شناسه درس',
        'user_id'=>'شناسه کاربر',
    ],
];