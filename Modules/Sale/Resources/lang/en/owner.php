<?php
return [

    'cancel' => 'انصراف',
    'submit' => 'ثبت',
    'delete' => 'حذف',

    'owner_title' => 'عنوان مالک',
    'owner_list' => 'لیست مالکین',
    'type' => 'نوع',
    'value' => 'مقدار',
    'max_value' => 'حداکثر مقدار',

    'items' => [
        'list' => 'لیست محصولات مالک',
        'model_title' => 'نام محصول',
        'model_id' => 'شناسه',
        'model_type' => 'مدل',
        'model_items' => 'محصولات',
        'model_types' => [
            'courses' => 'دوره',
        ],
    ],
    'types' => [
        'percent' => 'درصد',
        'fixed' => 'مبلغ ثابت',
    ],
    'export_to_excel' => 'خروجی به اکسل',

    'instance_id' => "سامانه",
    'user_id' => "کاربر",
    'state' => "وضعیت",
    'status' => "وضعیت",
    'add' => "ایجاد",
    'edit' => "ویرایش",
    'amount' => "قیمت",
    'create_owner' => "افزودن",

    'statuses' => [
        "0" => "حذف شده",
        "1" => "فعال",
        "2" => "غیرفعال",
    ],
    'created_at' => 'زمان ایجاد',
    'updated_at' => 'آخرین تغییر',
    'user_name' => "نام کاربر",

    'confirm' => 'آیااطمینان دارید؟',
    'confirm_delete' => 'آیا از حذف اطمینان دارید؟',
];