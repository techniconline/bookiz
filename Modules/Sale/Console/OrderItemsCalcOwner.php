<?php

namespace Modules\Sale\Console;

use Illuminate\Console\Command;
use Modules\Core\Bridge\CurrencyHelper\CurrencyHelper;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Owner\Owner;
use Modules\Sale\Models\Owner\OwnerCalculateLog;
use Modules\Sale\Models\Owner\OwnerItem;
use Modules\Sale\ViewModels\Config\InstanceViewModel;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class OrderItemsCalcOwner extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'sale:order-item-calc-owner';

    protected $signature = 'sale:order-item-calc-owner {--instance_id=} {--owner_id=} {--take=}';

    private $order_id;
    private $owner_id;
    private $instance_id;
    private $main_owner_id;
    private $take = 10;
    private $mod_amount = 0;
    private $mod_fix_amount = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate Order In {--instance_id=} {--owner_id=}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(" Start Calculate.");
        $this->instance_id = $this->option('instance_id');
//        $this->order_id = $this->option('order_id');
        $this->owner_id = $this->option('owner_id');
        $this->take = $this->option('take') ? $this->option('take') : 10;

        $configs = $this->getConfigs();

        if (empty($configs)) {
            $this->warn("Not Find Configs!");
            return false;
        }
        $this->main_owner_id = config('sale_owner.owner_id');

        if ($this->instance_id){

            $this->info(" Start Calculate For Instance :" . $this->instance_id);
            $this->info(" -------------------------------------------------- ");

            $this->calculateOrders();

            $this->info(" End Calculate For Instance :" . $this->instance_id);
            $this->info(" -------------------------------------------------- ");

        }else{

            foreach ($configs as $instance_id => $config) {
                if (isset($config->active_calculate_owner) && $config->active_calculate_owner) {
                    if ((isset($config->main_owner_id) && $config->main_owner_id)) {
                        $this->main_owner_id = $config->main_owner_id;
                    }
                    $this->instance_id = $instance_id;
                    $this->info(" Start Calculate For Instance :" . $instance_id);
                    $this->info(" -------------------------------------------------- ");

                    $this->calculateOrders();

                    $this->info(" End Calculate For Instance :" . $instance_id);
                    $this->info(" -------------------------------------------------- ");
                }

            }

        }



        return true;

    }

    private function calculateOrders()
    {
        $orders = $this->getOrders();
        $max_count = $orders->count();
        if (!$max_count) {
            $this->warn("Not Find Order for calculate!");
        }

        if ($max_count){
            $this->output->progressStart($max_count);
            foreach ($orders as $order) {
                try {
                    $this->calcOrder($order);
                } catch (\Exception $exception) {
                    report($exception);
                    $this->warn("Fatal Error! Order Id:" . $order->id);
                }

                $this->output->progressAdvance();
            }
            $this->output->progressFinish();
        }
    }

    /**
     * @return array
     */
    private function getConfigs()
    {
        $instanceViewModel = new InstanceViewModel();
        $configs = $instanceViewModel->getAllConfigs();
        $decorated = [];

        if (!$configs) {
            return $decorated;
        }

        foreach ($configs as $config) {
            $decorated[$config->instance_id] = $config->configs;
        }
        return $decorated;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['hash_key', InputArgument::REQUIRED, 'An hash_key argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

    /**
     * @return mixed
     */
    private function getOrders()
    {
        $lastCalculated = Order\OrderItemsLog::active()->where(function ($q) {
            if ($this->instance_id) {
                $q->where('instance_id', $this->instance_id);
            }
        })->orderBy("order_id", "DESC")->first();
        $orders = Order::active()->filterCompleted()
            ->where(function ($q) use ($lastCalculated) {

                if ($lastCalculated) {
                    $q->where("id", ">", $lastCalculated->order_id);
                }

                if ($this->instance_id) {
                    $q->where('instance_id', $this->instance_id);
                }

//                if ($this->order_id) {
//                    $q->where('id', $this->order_id);
//                }

            })
//            ->with(['orderPaymentSuccess' => function ($q) {
//                $q->where('bank', '!=', 'fake');
//            }])
            ->whereHas('orderPaymentSuccess', function ($q) {
                $q->where('bank', '!=', 'fake');
            })
            ->take($this->take)
            ->orderBy("id", "ASC")
            ->get();

        return $orders;
    }

    /**
     * @param $order
     */
    private function calcOrder($order)
    {
        $items = Order\OrderItems::where('order_id', $order->id)->with('order.orderPaymentSuccess', 'order.orderPromotion');
        $items = $items->get();

        foreach ($items as $item) {
            $this->calcItem($item);
            $this->warn("Order:" . $item->order_id . ", order_item:" . $item->id . ", finished!");
        }

        $order->save();
    }

    /**
     * @param $string
     * @return string
     */
    private function get_uppercase_by_underscores($string)
    {
        $arr = explode('_', $string);
        $string = '';
        foreach ($arr as $value) {
            if (!empty($value)) {
                $string .= ucfirst($value);
            }
        }
        return $string;
    }

    /**
     * @param $item
     * @return null
     */
    public function getItemRow($item)
    {
        $model = $this->get_uppercase_by_underscores($item->item_type);
        $class = null;
        if ($item->item_type && method_exists($item, "get" . $model)) {
            $class = $item->{"get" . $model}();
        }

        if (!$class) {
            return null;
        }

        return $item->belongsTo($class, 'item_id')->first();
    }

    /**
     * @param $item
     * @return bool
     */
    private function calcItem($item)
    {
        $modelRow = $this->getItemRow($item);
        if ($modelRow) {
            $currencyClass = new CurrencyHelper();

            $model_type = $modelRow->getTable();
            $model_id = $modelRow->id;
            $ownerRows = OwnerItem::active()->with('owner')->where("owner_id", "!=", $this->main_owner_id)->where("model_type", $model_type)->where("model_id", $model_id)->orderBy('type', 'DESC')->get();
            $this->mod_amount = $item->getAttributes()['total_amount'];
            $this->mod_fix_amount = $item->getAttributes()['total_amount'];
            $item_currency_id = $item->currency_id;
            $change_currency = false;
            foreach ($ownerRows as $ownerItem) {
                $value = $ownerItem->value;
                $currency_id = $ownerItem->currency_id;
                if (!$value) {
                    $currency_id = $ownerItem->owner->currency_id;
                }

                if ($item->currency_id != $currency_id) {
                    $this->mod_amount = $currencyClass->getConvertPrice($this->mod_amount, $item->currency_id, $currency_id);
                    $this->mod_fix_amount = $currencyClass->getConvertPrice($this->mod_fix_amount, $item->currency_id, $currency_id);
                    $item->currency_id = $currency_id;
                    $change_currency = true;
                }

                $this->calcOwner($ownerItem, $item);
            }
            $this->calcMainOwner($item, $model_type, $model_id);
            if ($change_currency) {
                $item->currency_id = $item_currency_id;
            }
            $this->logOrderItem($item);
        }
        return false;
    }

    private function calcMainOwner($item, $model_type, $model_id)
    {
        $owner_id = $this->main_owner_id;
        try {
            $ownerRow = OwnerItem::active()->with('owner')->where("owner_id", $owner_id)->where("model_type", $model_type)->where("model_id", $model_id)->orderBy('id', 'DESC')->first();
            if (!$ownerRow) {
                $ownerRow = Owner::active()->find($owner_id);
            }
            if (!$ownerRow) {
                $this->warn("Not find Owner!");
                return false;
            }
//            $type = $ownerRow->type;
//            $value = $ownerRow->value;
//            $max_value = $ownerRow->max_value;
            $currency_id = $ownerRow->currency_id;
            $currencyClass = new CurrencyHelper();

            if (!$currency_id) {
                $currency_id = $currencyClass->getCurrentCurrencyId();
            }
            $mod_amount = $this->mod_amount;

            $model_type = $item->getTable();
            $model_id = $item->id;
            $modelOwnerCalcLog = new OwnerCalculateLog();
            $lastLog = $modelOwnerCalcLog->active()->where('owner_id', $owner_id)->orderBy('id', 'DESC')->first();
            $current_credit = $lastLog ? $lastLog->current_credit : 0;

            if ($item->currency_id != $currency_id) {
                $mod_amount = $currencyClass->getConvertPrice($mod_amount, $item->currency_id, $currency_id);
            }

            $data = [
                'model_type' => $model_type,
                'model_id' => $model_id,
                'owner_id' => $owner_id,
                'currency_id' => $currency_id,
                'active' => 1,
                'type' => $modelOwnerCalcLog::TYPE_PLUS,
            ];

            $amount = $mod_amount;
            $data['amount'] = $amount;
            $data['current_credit'] = $data['amount'] + $current_credit;

            $modelOwnerCalcLog->fill($data);
            if ($modelOwnerCalcLog->save()) {
                $this->info("Calculate Item:" . $item->id . ', ItemType:' . $item->getTable() . ', OwnerItem Id:' . (isset($ownerRow->model_id) ? $ownerRow->model_id : 0) . ', OwnerItem Type:' . (isset($ownerRow->model_type) ? $ownerRow->model_type : 'PUBLIC'));
                return true;
            }

            $this->warn(" NOT INSERTED !!!! Item:" . $item->id . ', ItemType:' . $item->getTable());
            return false;
        } catch (\Exception $exception) {
            $this->warn("Error in calc Owner Item.! Item:" . $item->id . ", Owner:" . $owner_id);
            report($exception);
            return false;
        }

    }

    /**
     * @param $ownerItem
     * @param $item
     * @return bool
     */
    private function calcOwner($ownerItem, $item)
    {
        if (!isset($ownerItem->owner->id)) {
            $this->warn("Not find Owner!");
            return false;
        }

        try {
            $currencyClass = new CurrencyHelper();

            $type = $ownerItem->type;
            $value = $ownerItem->value;
            $max_value = $ownerItem->max_value;
            $currency_id = $ownerItem->currency_id;
            if (!$value) {
                $value = $ownerItem->owner->value;
                $max_value = $ownerItem->owner->max_value;
                $type = $ownerItem->owner->type;
                $currency_id = $ownerItem->owner->currency_id;
            }

            if (!$currency_id) {
                $currency_id = $currencyClass->getCurrentCurrencyId();
            }

            $model_type = $item->getTable();
            $model_id = $item->id;
            $modelOwnerCalcLog = new OwnerCalculateLog();
            $lastLog = $modelOwnerCalcLog->active()->where('owner_id', $ownerItem->owner_id)->orderBy('id', 'DESC')->first();
            $current_credit = $lastLog ? $lastLog->current_credit : 0;

            $data = [
                'model_type' => $model_type,
                'model_id' => $model_id,
                'owner_id' => $ownerItem->owner_id,
                'currency_id' => $currency_id,
                'active' => 1,
                'type' => $modelOwnerCalcLog::TYPE_PLUS,
            ];

            $total_amount = $this->mod_fix_amount;
            if ($item->currency_id != $currency_id) {
                $total_amount = $currencyClass->getConvertPrice($total_amount, $item->currency_id, $currency_id);
            }

            $amount = 0;
            if ($type == OwnerItem::TYPE_PERCENT) {

                $amount = $total_amount * ($value / 100);
                if (($max_value) && ($amount > $max_value)) {
                    $amount = $max_value;
                }

//                if ($amount > $this->mod_amount) {
//                    $amount = $this->mod_amount;
//                }

                $data['amount'] = $amount;
                $data['current_credit'] = $data['amount'] + $current_credit;

            } elseif ($type == OwnerItem::TYPE_FIXED) {

                $amount = $value;
//                if ($amount > $this->mod_amount) {
//                    $amount = $this->mod_amount;
//                }
                $data['amount'] = $amount;
                $data['current_credit'] = $data['amount'] + $current_credit;

            }

            $modelOwnerCalcLog->fill($data);
            if ($modelOwnerCalcLog->save()) {
                $this->info("Calculate Item:" . $model_id . ', ItemType:' . $model_type . ', OwnerItem Id:' . $ownerItem->model_id . ', OwnerItem Type:' . $ownerItem->model_type);
                $this->mod_amount -= $amount;
                if ($type == OwnerItem::TYPE_FIXED) {
                    $this->mod_fix_amount -= $amount;
                }
                return true;
            }

            $this->warn("Type Is not Valid! type:" . $ownerItem->type);
            return false;
        } catch (\Exception $exception) {
            $this->warn("Error in calc Owner Item.! Item:" . $item->id . ", OwnerItem:" . $ownerItem->id . ", Owner:" . $ownerItem->owner->id);
            report($exception);
            return false;
        }
    }

    /**
     * @param $item
     * @return bool
     */
    private function logOrderItem($item)
    {
        $modelOrderLog = new Order\OrderItemsLog();
        try {

            $data = [
                'order_id' => $item->order_id,
                'order_item_id' => $item->id,
                'item_id' => $item->item_id,
                'item_type' => $item->item_type,
                'instance_id' => $item->order->instance_id,
                'user_id' => $item->order->user_id,
                'currency_id' => $item->currency_id,
                'payment_id' => $item->order->orderPaymentSuccess ? $item->order->orderPaymentSuccess->id : null,
                'order_promotion_id' => $item->order->orderPromotion ? $item->order->orderPromotion->id : null,
                'promotion_amount' => $item->order->orderPromotion ? $item->order->orderPromotion->amoount : null,
                'amount' => $item->amount,
                'discount' => $item->discount_amount,
                'total_amount' => $item->getAttributes()['total_amount'],
                'payment_amount' => $item->order->orderPaymentSuccess ? $item->order->orderPaymentSuccess->amount : null,
                'bank' => $item->order->orderPaymentSuccess ? $item->order->orderPaymentSuccess->bank : null,
                'active' => 1,
            ];

            $modelOrderLog->fill($data);
            if ($modelOrderLog->save()) {
                $this->warn("Order Item Log inserted!!! Item Id: " . $item->id);
                return true;
            }
            $this->warn("Order Item Log Error!!! Item Id: " . $item->id);
            return false;
        } catch (\Exception $exception) {
            $this->warn("FATAL: Order Item Log Error!!! Item Id: " . $item->id);
            report($exception);
            return false;
        }

    }


}
