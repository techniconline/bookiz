<?php

namespace Modules\Sale\Console;

use Illuminate\Console\Command;
use Modules\Sale\Models\Order;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CalcPromotion extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'sale:calc-promotion-order';

    protected $signature = 'sale:calc-promotion-order {--instance_id=} {--order_id=}';

    private $order_id;
    private $instance_id;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate {--instance_id=} {--order_id=}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(" Start Calculate. ");
        $this->instance_id = $this->option('instance_id');
        $this->order_id = $this->option('order_id');
        $orders = $this->getOrders();

        $max_count = $orders->count();
        if (!$max_count) {
            $this->warn("Not Find Order for calculate!");
            return true;
        }

        $this->output->progressStart($max_count);
        foreach ($orders as $order) {
            try {
                $this->calcOrder($order);
            } catch (\Exception $exception) {
                report($exception);
            }

            $this->output->progressAdvance();
        }
        $this->output->progressFinish();

        return true;

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['hash_key', InputArgument::REQUIRED, 'An hash_key argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

    /**
     * @return mixed
     */
    private function getOrders()
    {
        $orders = Order::active()->filterCompleted()
            ->where(function ($q) {
                if ($this->instance_id) {
                    $q->where('instance_id', $this->instance_id);
                }
                if ($this->order_id) {
                    $q->where('id', $this->order_id);
                }
            })
            ->with(['orderItems', 'orderPromotions', 'orderPaymentSuccess'])
            ->get();
        return $orders;
    }

    /**
     * @param $order
     */
    private function calcOrder($order)
    {
        $items = Order\OrderItems::where('order_id',$order->id);
        $sumTotalAmount = $items->sum('total_amount');
        $items = $items->get();
        $countItems = count($items);
        $promotions = Order\OrderPromotions::where('order_id',$order->id);
        $sumPromotion = $promotions->sum('amount');
        $promotions = $promotions->get();
        $tDisc = 0;
        $counter = 0;
        foreach ($items as $item) {
            $counter++;
            if ($countItems == $counter){
                $dis_total_amount = (int)($sumPromotion - $tDisc);
            }else{
                $dis_total_amount = (int)(($item->getAttributes()['total_amount'] * $sumPromotion) / $sumTotalAmount);
                $tDisc += $dis_total_amount;
            }

            $item->total_amount = $item->getAttributes()['total_amount'] - $dis_total_amount;
            $item->has_shipment = $item->has_shipment + 10;
            $item->save();
            $this->warn("Order:".$item->order_id.", order_item:" . $item->id . ", Changed!");
        }

        $order->deleted_at = '2019-02-12 00:00:00';
        $order->save();

    }

}
