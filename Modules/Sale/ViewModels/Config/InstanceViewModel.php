<?php

namespace Modules\Sale\ViewModels\Config;

use Modules\Core\Models\Instance\InstanceConfigs;
use Modules\Core\Models\Module\Module;
use Modules\Core\ViewModels\Config\SettingViewModel;


class InstanceViewModel extends SettingViewModel
{

    public $setting_name = 'instance';
    public $module_name = 'sale';

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getConfigForm()
    {
        $BlockViewModel =& $this;
        $this->setTitlePage(trans('sale::menu.config.sale_instance'));
        return $this->renderedView('sale::config.instance', compact('BlockViewModel'));
    }

    /**
     * @return $this
     */
    public function save()
    {
        $rules = [];
        $this->requestValuesUpdate()->validate($rules);
        $data = $this->request->all();
        $result = $this->saveConfig($data);
        if ($result) {
            return $this->redirectBack()->setResponse($result, trans('sale::messages.alert.save_success'));
        }
        return $this->redirectBack()->setResponse($result, trans('sale::messages.alert.error'));

    }

    /**
     * @return mixed
     */
    public function getAllConfigs()
    {
        $module = Module::active()->where("name", $this->module_name)->first();
        $instanceConfigModel = new InstanceConfigs();
        $configs = $instanceConfigModel->active()->where("module_id",$module->id)->get();
        return $configs;
    }


}
