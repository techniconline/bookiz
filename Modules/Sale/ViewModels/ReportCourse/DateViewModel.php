<?php

namespace Modules\Sale\ViewModels\ReportCourse;

use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\Traits\Block\MasterBlock;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Sale\Models\Order\OrderItems;
use Modules\Sale\Models\Order\OrderPromotions;

class DateViewModel extends BaseViewModel
{
    use GridViewModel;

    private $block_name;
    private $chartjs_assets;

    public function setGridModel()
    {
        $instancId = get_instance()->getCurrentInstanceId();
        $this->sort_by = 'item_id';
        $this->Model = OrderItems::select(
            'item_id',
            'name',
            'item_type',
            DB::raw('count(item_id) as count_sale'),
            DB::raw('sum(total_amount) as total_amount'),
            DB::raw('Date(payments.updated_at) as date'),
            DB::raw('sum(if(payments.currency_id = 2 , order_items.total_amount , order_items.total_amount/10 ) ) as total_sum_amount')
//            DB::raw('CASE WHEN payments.currency_id = 1 THEN order_items.total_amount/10 WHEN payments.currency_id = 2 THEN order_items.total_amount ELSE 0 END) as total_sum_amount')
        )
            ->join('payments', 'payments.action_id', '=', 'order_items.order_id')
            ->where('item_type', 'course')
            ->where('payments.action', 'order')
            ->where(function ($query) {
                $query->where('payments.state', 'success')
                    ->orWhere('payments.state', 'complete');
            })
            ->where('payments.instance_id', $instancId)
            ->groupBy('name', DB::raw('Date(payments.updated_at)'), 'item_id', 'item_type');
//        dd($this->Model->toSql());
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->sort_by = 'count_sale';
        $this->can_chart = false;
        $this->hasIndex = false;
        $this->addColumn('item_id', trans('sale::sale.item_id'), true)
            ->addColumn('name', trans('sale::sale.course_title'), false)
            ->addColumn('count_sale', trans('sale::sale.count_sale'), false)
            ->addColumn('total_amount', trans('sale::sale.total_amount'), false)
            ->addColumn('date', trans('sale::sale.created_at'), false);
        // Add filter --------------------------------------------------------
        $config = ['title' => trans('sale::sale.order.created_at'), 'field' => 'payments.updated_at'];
        $this->addFilter('updated_at', 'date', $config);
        $this->addFilter('name', 'text', ['title' => 'عنوان']);
        // Chart -------------------------------------------------------------
        $this->setNameChart("sample_chart");
        //if (request()->use_chart == 1) {
           // $this->with_paginate = false;
        //}
        $this->setXField("name");
        $this->setYField("total_amount", trans('sale::sale.total_amount'));
        $this->setChartSize(["width" => 600, "height" => 400]);

        $this->can_export = 1;
        $this->export_file_name = 'sale_date_report';

        return $this;
    }

    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('sale::sale.report_course.list'));
        $this->generateGridList()->renderedView("sale::sale.index", ['view_model' => $this], "report_course");
        return $this;
    }

    public function getData($name = null, $default = null)
    {
        if ($name) {
            if (isset($this->data[$name])) {
                return $this->data[$name];
            }
            return $default;
        }
        return $this->data;
    }

    public function getAssets()
    {
        $chartjs_assets = [];
        $chartjs_assets [] = ['container' => 'general_style', 'src' => ("css/loading.css"), 'name' => 'loading'];
        $chartjs_assets [] = ['container' => 'plugin_general_2', 'src' => ("/plugins/chartjs/Chart.bundle.min.js"), 'name' => 'Chartjs.bundle'];
        $chartjs_assets [] = ['container' => 'plugin_general_2', 'src' => ("/plugins/chartjs/Chart.min.js"), 'name' => 'Chartjs'];
        return $chartjs_assets;
    }

    public function getExportHeader()
    {
        $header = [
            'item_id' => "شناسه",
            'name' => "نام محصول",
            'count_sale' => "تعدد فروش",
            'total_amount' => "مبلغ کل",
            'date' => "تاریخ فروش",
        ];
        return $header;
    }

    public function getExportRow($row)
    {
        $csvRow = [
            'item_id' => $row->item_id,
            'name' => $row->name,
            'count_sale' => $row->count_sale,
            'total_amount' => $row->total_amount,
            'date' => $row->date,
        ];
        return $csvRow;
    }
}
