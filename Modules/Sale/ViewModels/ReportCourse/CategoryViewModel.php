<?php

namespace Modules\Sale\ViewModels\ReportCourse;

use Illuminate\Support\Facades\DB;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Models\Course\CourseCategory;
use Modules\Course\Models\Course\CourseCategoryRelation;
use Modules\Sale\Models\Order\OrderItems;

class CategoryViewModel extends BaseViewModel
{
    use GridViewModel;

    private $block_name;
    private $chartjs_assets;

    public function setGridModel()
    {
        $instancId = get_instance()->getCurrentInstanceId();
        $this->sort_by = 'category_id';

        $this->Model = OrderItems::select(
            'course_category_relations.category_id',
            DB::raw('count(course_category_relations.category_id) as course_total'),
            DB::raw('sum(if(payments.currency_id = 1 , order_items.total_amount/10 , order_items.total_amount ) ) as total_sum_amount'),
            DB::raw('sum(order_items.discount_amount) as total_discount_amount')
        )
            ->join('course_category_relations', 'course_category_relations.course_id', '=', 'order_items.item_id')
            ->join('payments', 'payments.action_id', '=', 'order_items.order_id')
            ->where('order_items.item_type', 'course')
            ->where('payments.action', 'order')
            ->where(function ($query) {
                $query->where('payments.state', 'success')
                    ->orWhere('payments.state', 'complete');
            })
            ->where('payments.instance_id', $instancId)
            ->groupBy('course_category_relations.category_id');
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->sort_by = 'category_id';
        $this->hasIndex = false;
        $this->addColumn('category_id', trans('sale::sale.category_id'), true)
            ->addColumn('category_title', trans('sale::sale.category_title'), false)
            ->addColumn('course_total', trans('sale::sale.course_total'), true)
            ->addColumn('total_sum_amount', trans('sale::sale.total_sum_amount'), true)
            ->addColumn('total_discount_amount', trans('sale::sale.total_discount_amount'), true);

        $categories = $this->getListCategoryByChildes();
        $this->addFilter('updated_at', 'date', ['field' => 'payments.updated_at', 'title' => trans('sale::sale.order.created_at')]);
        $this->addFilter('category_id', 'select', ['relation' => 'courseCategories', 'options' => $categories, 'title' => trans('course::course.category')]);
        $this->addFilter('bank', 'select', ['title' => trans('sale::sale.bank'), 'field' => 'payments.bank', 'options' => trans('sale::payment.payment_way')]);

        $this->can_export = 1;
        $this->export_file_name = 'sale_category_report';

        return $this;
    }

    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('sale::sale.report_course.list'));
        $this->generateGridList()->renderedView("sale::sale.index", ['view_model' => $this], "report_course");

        return $this;
    }

    public function getData($name = null, $default = null)
    {
        if ($name) {
            if (isset($this->data[$name])) {
                return $this->data[$name];
            }

            return $default;
        }

        return $this->data;
    }

    public function getRowsUpdate($row)
    {
        $row->category_title = CourseCategoryRelation::getCategoryName($row->category_id);

        return $row;
    }

    private function getListCategoryByChildes()
    {
        $categories = CourseCategory::enable()->filterCurrentInstance()->orderBy('parent_id')->pluck('title', 'id');
        $list = [];
        $model = new CourseCategory();
        foreach ($categories as $cat_id => $title) {
            $childes = $model->getChildesId($cat_id);
            if ($childes->count() > 1) {
                $title = $title . ' + ';
            }
            $list[implode(",", $childes->toArray())] = $title;
        }
        return $list;
    }

    public function getExportHeader()
    {
        $header = [
            'category_id' => "شناسه",
            'category_title' => "عنوان دسته بندی",
            'course_total' => "تعدد دوره‌ها",
            'total_sum_amount' => "مبلغ کل هر دسته بندی",
            'total_discount_amount' => "مبلغ کل تخفیف‌ها",
        ];
        return $header;
    }

    public function getExportRow($row)
    {
        $csvRow = [
            'id' => $row->category_id,
            'category_title' => $row->category_title,
            'course_total' => $row->course_total,
            'total_sum_amount' => $row->total_sum_amount,
            'total_discount_amount' => $row->total_discount_amount,
        ];
        return $csvRow;
    }
}
