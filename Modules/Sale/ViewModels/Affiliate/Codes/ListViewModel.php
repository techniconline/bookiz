<?php

namespace Modules\Sale\ViewModels\Affiliate\Codes;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Sale\Models\Affiliate\AffiliateCodes;
use Modules\Sale\Models\Affiliate\Affiliate;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;
    protected $voucher=false;

    public function setGridModel()
    {
        $this->Model = AffiliateCodes::where('voucher_id',$this->voucher_id);
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('sale::form.fields.id'), true)
            ->addColumn('voucher_id', trans('sale::form.fields.voucher_id'), true)
            ->addColumn('voucher', trans('sale::form.fields.voucher_id'), false)
            ->addColumn('code', trans('sale::form.fields.voucher_code'), true)
            ->addColumn('used_qty', trans('sale::form.fields.used_qty'), true)
            ->addColumn('active_time', trans('sale::form.fields.active_time'), true)
            ->addColumn('created_at', trans('sale::form.fields.created_at'), true);


        $routeAdd=route('sale.voucher.code.create');
        $options=['class'=>"btn btn-primary"];
        $this->addButton('add',$routeAdd,trans('sale::form.titles.create_voucher_code'),'fa fa-plus',false,'before',$options);

        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->voucher=$this->voucher->title;
        return $row;
    }


    protected function getViewListIndex()
    {
        $voucher_id=$this->request->get('id');
        $this->voucher=Affiliate::find($voucher_id);
        if(!$this->voucher){
            abort('404');
        }
        $this->setTitlePage(trans('sale::menu.voucher_manage'));
        $this->generateGridList()->renderedView("sale::voucher.index", ['view_model' => $this], trans('sale::menu.voucher_manage'));
        return $this;
    }


}
