<?php

namespace Modules\Sale\ViewModels\Affiliate;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Sale\Models\Affiliate;
use Modules\Sale\Models\Order\OrderPromotions;

class AffiliateManageViewModel extends BaseViewModel
{

    use GridViewModel;
    private $list_uses = 'affiliate';
    private $affiliate_id;

    /**
     *
     */
    public function setGridModel()
    {
        $this->Model = Affiliate::enable()->with('user')->withCount('orderPromotions')->filterCurrentInstance();
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('sale::form.fields.id'), true)
            ->addColumn('user', trans('sale::form.fields.user_name_affiliate'), true)
            ->addColumn('type', trans('sale::form.fields.type'), true)
            ->addColumn('code', trans('sale::form.fields.affiliate_code'), true)
            ->addColumn('value', trans('sale::form.fields.value'), true)
            ->addColumn('max_value', trans('sale::form.fields.max_value'), true)
            ->addColumn('owner_value', trans('sale::form.fields.owner_value'), true)
            ->addColumn('count_uses', trans('sale::form.fields.count_uses'), false)
            ->addColumn('max_qty', trans('sale::form.fields.max_qty'), true)
            ->addColumn('active_date', trans('sale::form.fields.active_date'), true)
            ->addColumn('expire_date', trans('sale::form.fields.expire_date'), true)
            ->addColumn('active', trans('sale::form.fields.status'), true);

        $edit = array(
            'name' => 'sale.affiliate.edit',
            'parameter' => ['id']
        );
        $this->addAction('edit', $edit, trans('sale::form.titles.edit_affiliate'), 'fa fa-pencil', false, ['target' => '', 'class' => 'btn btn-sm btn-primary']);


        $codes = array(
            'name' => 'sale.affiliate.code.list_uses',
            'parameter' => ['id']
        );
        $this->addAction('codes', $codes, trans('sale::form.titles.affiliate_codes'), 'fa fa-list', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $this->action_text = false;

        $routeAdd = route('sale.affiliate.create');
        $options = ['class' => "btn btn-primary"];
        $this->addButton('add', $routeAdd, trans('sale::form.titles.create_affiliate'), 'fa fa-plus', false, 'before', $options);

        /*filter*/
        $this->addFilter('last_name', 'text', ['relation' => 'user']);
        $this->addFilter('first_name', 'text', ['relation' => 'user']);

        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        if ($this->list_uses == 'affiliate_uses') {
            $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
            $row->order_id = '<a target="_blank" href="'.route('sale.order.index',['filter'=>1, 'id'=>$row->order_id]).'" class="btn btn-success"><i class="fa fa-link"></i>'.$row->order_id.'</a>';
        }

        if ($this->list_uses == 'affiliate') {
            if ($row->type == 'percent') {
                $row->value = $row->value . ' ' . trans('sale::data.voucher_type.percent');
            } else {
                $row->value = BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($row->value, $row->currency_id, true);
            }

            $row->max_value = BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($row->max_value, $row->currency_id, true);
            $row->type = trans('sale::data.voucher_type.' . $row->type);
            $row->active = trans('sale::data.active.' . $row->active);
            $row->active_date = DateHelper::setDateTime($row->active_date)->getLocaleFormat(trans('core::date.datetime.medium'));
            $row->expire_date = DateHelper::setDateTime($row->expire_date)->getLocaleFormat(trans('core::date.datetime.medium'));
            $row->count_uses = $row->order_promotions_count;
        }
        $row->user = $row->user ? $row->user->full_name : $row->user_id;

        return $row;
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->list_uses = 'affiliate';
        $this->setTitlePage(trans('sale::menu.affiliate_manage'));
        $this->generateGridList()->renderedView("sale::affiliate.index", ['view_model' => $this], trans('sale::menu.affiliate_manage'));
        return $this;
    }

    public function setGridModelUses()
    {
        $this->Model = OrderPromotions::active()->where('type_id',$this->affiliate_id)->where('type', 'affiliate')->with('user');
    }

    /**
     * @return $this
     */
    private function generateGridListCodeUses()
    {
        $this->setGridModelUses();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('sale::form.fields.id'), true)
            ->addColumn('user', trans('sale::form.fields.user_name'), true)
            ->addColumn('type', trans('sale::form.fields.model_type'), true)
            ->addColumn('type_id', trans('sale::form.fields.model_id'), true)
            ->addColumn('order_id', trans('sale::form.fields.order'), true)
            ->addColumn('amount', trans('sale::form.fields.amount'), true)
            ->addColumn('created_at', trans('sale::form.fields.created_at'), true);

        $this->action_text = false;

        $routeAdd = route('sale.affiliate.index');
        $options = ['class' => "btn btn-primary"];
        $this->addButton('add', $routeAdd, trans('sale::form.titles.list_affiliate'), 'fa fa-list', false, 'before', $options);

        /*filter*/
        $this->addFilter('last_name', 'text', ['relation' => 'user']);
        $this->addFilter('first_name', 'text', ['relation' => 'user']);

        return $this;
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getListAffiliateUses()
    {
        $this->affiliate_id = $this->request->get('affiliate_id');
        $this->list_uses = 'affiliate_uses';
        $this->setTitlePage(trans('sale::form.titles.affiliate_codes'));
        $this->generateGridListCodeUses()->renderedView("sale::affiliate.index", ['view_model' => $this], trans('sale::form.titles.affiliate_codes'));
        return $this;
    }


}
