<?php

namespace Modules\Sale\ViewModels\Affiliate;

use Illuminate\Support\Facades\Validator;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Sale\Models\Affiliate;

class EditViewModel extends BaseViewModel
{
    public function boot()
    {
        $id = $this->requestValuesUpdate()->get('id');
        if ($id) {
            $this->modelData = Affiliate::find($id);
        }
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getForm()
    {
        $this->boot();
        $BlockViewModel =& $this;
        $this->setTitlePage(trans('sale::form.titles.create_affiliate'));
        return $this->renderedView('sale::affiliate.edit', compact('BlockViewModel'));
    }


    public function save()
    {
        $this->boot();
        $this->request = $this->requestValuesUpdate(['active_date' => 'Y-m-d H:i:s', 'expire_date' => 'Y-m-d H:i:s']);
        $rules = [
            'user_id' => ['required', 'integer'],
            'type' => ['required', 'in:fixed,percent'],
            'value' => ['required', 'numeric'],
            'max_value' => ['nullable', 'numeric'],
            'currency_id' => ['required', 'integer'],
            'max_qty' => ['nullable', 'integer'],
            'active' => ['required', 'in:0,1,2'],
        ];
        if ($this->request->get('type') == 'percent') {
            $rules['value'] = ['required', 'between:0,100'];
        }

        if (!$this->modelData){
            $rules['code'] = ['required', 'unique:affiliates,code'];
        }

        $this->request->validate($rules);
        $this->request->request->add(['instance_id' => app('getInstanceObject')->getCurrentInstanceId()]);

        $data = $this->request->all();
        if (!$this->modelData) {
            $this->modelData = new Affiliate;
        }
        $this->modelData->fill($data);
        $result = $this->modelData->save();
        if ($result) {
            return $this->redirect(route('sale.affiliate.index'))->setResponse(true, trans('sale::message.action.success'));
        }
        return $this->redirectBack()->setResponse($result, trans('sale::message..action.error'));
    }


}
