<?php

namespace Modules\Sale\ViewModels\Order;

use BridgeHelper;
use Illuminate\Support\Facades\DB;
use Modules\Booking\ViewModels\Booking\BookingViewModel;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Sale\Models\Affiliate;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Order\OrderItems;
use Modules\Sale\Models\Traits\Payment\OrderStatesTrait;
use Modules\Sale\Models\Traits\Payment\PaymentCloseTrait;
use Modules\Sale\Models\Traits\Payment\PaymentStatesTrait;
use Modules\Sale\Models\Traits\Payment\PaymentTrait;
use Modules\Sale\Models\Order\OrderPromotions;
use Modules\Sale\Models\Voucher\VoucherCodes;

class SaveBookingViewModel extends BaseViewModel
{
    use PaymentStatesTrait, OrderStatesTrait, PaymentTrait, PaymentCloseTrait;


    /** STEP 1 payment
     * @return $this
     */
    public function saveOrderBooking()
    {
        $owner_user_id = BridgeHelper::getAccess()->getUser()->id;
        if (!$this->loginClient()) {
            return $this->redirectBack()->setResponse(false, trans('sale::message.booking_item_add_error'));
        }
        $this->closeOpenedPayments();
        $cartManager = $this->getCurrentCart($create = false);
        $cart = $cartManager->validateCart()->getCart();
        if (!$cart || !isset($cart->cartItems) || !$cartManager->canUseDiscount() || !$cart->cartItems->count()) {
            BridgeHelper::getAccess()->loginById($owner_user_id);
            return $this->redirectBack()->setResponse(false, trans('sale::message.cart_not_find'));
        }
        if ($cart->payable_amount > 0) {
            $rules = [
                'payment' => 'required|in:online',
                'option' => 'required|array',
            ];
            $this->request->validate($rules);
        }
        DB::beginTransaction();
        try {
            $booking = $this->createBooking($cart);
            $booking_details = $booking ? $booking->bookingDetails : null;
            if ($booking_details->isEmpty()) {
                DB::rollBack();
                $error = trans('sale::message.booking_item_add_error');
                BridgeHelper::getAccess()->loginById($owner_user_id);
                return $this->redirectBack()->setResponse(false, $error);
            }

            $order = $this->createOrderByItems($cart, $booking);


            //accept booking by user
            $bookingViewModel = BridgeHelper::getBooking()->getBookingManagerViewModel();
            $bookingViewModel->setRequest($this->request)->acceptBookingUser($booking->id);

            if ($order->payable_amount > 0) {
                $options = ($this->request->has('option')) ? $this->request->get('option') : [];
                $type = ($this->request->has('payment')) ? $this->request->get('payment') : 'online';
                $this->createOrderPayment($order, $options, $type);
            }
            DB::commit();

            $app = $this->request->get('application');
            if (!$app){
                $app = app('getInstanceObject')->isApi() ? "mobile" : "web";
            }

            if ($order->payable_amount > 0) {
                $redirect = route('sale.order.payment', ['id' => $order->id, "reference_code" => $order->reference_code, "app" => $app]);
                $redirect = get_instance()->getCurrentUrl($redirect);
            } else {
                $redirect = route('sale.order.complete', ['id' => $order->id, "reference_code" => $order->reference_code, "app" => $app]);
                $redirect = get_instance()->getCurrentUrl($redirect);
            }
        } catch (\Exception $e) {
            report($e);
            DB::rollBack();
            $error = trans('sale::message.item_add_error');
            BridgeHelper::getAccess()->loginById($owner_user_id);
            return $this->redirectBack()->setResponse(false, $error);
        }
        BridgeHelper::getAccess()->loginById($owner_user_id);
        return $this->redirect($redirect)->setDataResponse($order)->setResponse(true, trans("sale::message.save_order"));
    }

    /**
     * @return bool
     */
    private function loginClient()
    {
        if ($this->request->get('entity_client_id') && $this->request->get('entity_id')) {
            $owner_user_id = BridgeHelper::getAccess()->getUser()->id;
            $entity_id = $this->request->get('entity_id');
            $entity_client_id = $this->request->get('entity_client_id');

            $entity = BridgeHelper::getEntity()->getEntityModel();
            $entity = $entity->isOwner()->active()->find($entity_id);

            if (!$entity) {
                return false;
            } else {
                $client = BridgeHelper::getEntity()->getEntityClientModel();
                $client = $client->active()->find($entity_client_id);
                if ($client) {
                    BridgeHelper::getAccess()->loginById($client->user_id);
                    return true;
                }
                return false;
            }
        }
        return true;
    }

    /**
     * @param $cart
     * @return mixed
     */
    private function createBooking($cart)
    {
        $booking_items = $this->request->get("booking_details");
        $booking_items_collection = collect($booking_items);
        $requestItems = [];
        foreach ($cart->cartItems as $index => $saleItem) {
            $item_id = $saleItem->item_id;
            $booking_item = $booking_items_collection->where('model_id', $item_id)->first();
            //$booking_item = isset($booking_items[$item_id]) ? $booking_items[$item_id] : null;

            if (!$booking_item) {
                continue;
            }

            //TODO , redesign booking ... remove entity_relation_service_id and entity_service_id and remove entity_id
            if ((isset($booking_item['time']) && $booking_item['time']) && (isset($booking_item['date']) && $booking_item['date']) && (isset($booking_item['entity']) && $booking_item['entity'])) {
                $requestItems['booking_details'][] = [
                    'model_id' => $item_id,
                    'model_type' => $saleItem->item_type,
                    'booking_calendar_id' => $booking_item['booking_calendar'],
//                    'entity_relation_service_id' => $booking_item['service'],
//                    'entity_service_id' => $booking_item['entity_service'],
                    'date' => $booking_item['date'],
                    'by_owner' => (int)boolval(request()->get('entity_client_id')),
                    'time' => date("H:i:s", strtotime($booking_item['time'])),
                    'entity_employee_id' => ($booking_item['employee'] > 0) ? $booking_item['employee'] : null,
                    'params' => ['description' => isset($booking_item['description']) ? $booking_item['description'] : null],
                ];
                $requestItems['entity_id'] = $booking_item['entity'];
                $requestItems['by_owner'] = (int)boolval(request()->get('entity_client_id'));

                $requestItems['model_id'] = $booking_item['booking_model_id'];
                $requestItems['model_type'] = $booking_item['booking_model_type'];
            }

        }

        $bookingViewModel = BridgeHelper::getBooking()->getBookingViewModel();
        $this->request->request->add($requestItems);
        $booking = $bookingViewModel->setRequest($this->request)->saveBooking(true);
        return $booking;
    }

    /**
     * @param $cart
     * @param $booking
     * @return Order
     */
    public function createOrderByItems($cart, $booking)
    {
        $booking_details = $booking ? $booking->bookingDetails : null;
        if ($booking_details->isEmpty()) {
            return false;
        }
        $instance = app('getInstanceObject')->getCurrentInstance();
        $order = new Order;
        $order->instance_id = $instance->id;
        $order->reference_code = $order->getReferenceCode();
        $order->user_id = $cart->user_id;
        $order->state = $this->getOrderNewState();
        $order->amount = $cart->amount;
        $order->payable_amount = $cart->payable_amount;
        $order->currency_id = $cart->currency_id;
        $order->save();

        if ($order->id) {
            $this->addCodeToOrder($cart, $order);
            //$bookingDetailsModel = BridgeHelper::getBooking()->getBookingDetailsModel();
            $amount = 0;
            $payable_amount = 0;
            foreach ($cart->cartItems as $saleItem) {

                $booking_detail = $booking_details->where("model_type", $saleItem->item_type)->where("model_id", $saleItem->item_id)->first();
                if (!$booking_detail) {
                    continue;
                }

                $bParams = $booking_detail->params_array;
                $addParams = [];
                $addParams['name'] = $saleItem->name;
                $params = array_merge($bParams, $addParams);

                $booking_detail->params = $params;
                $booking_detail->currency_id = $saleItem->currency_id;
                $booking_detail->save();

                $add_options = [
                    'model_id' => $saleItem->item_id,
                    'model_type' => $saleItem->item_type,
                    'date' => $booking_detail->date,
                    'time' => $booking_detail->time,
                    'amount' => $booking_detail->amount,
                    'discount_amount' => $booking_detail->discount_amount,
                    'payable_amount' => $booking_detail->payable_amount,
                    'entity_employee_id' => $booking_detail->entity_employee_id,
                    'bcd_id' => $booking_detail->bcd_id,
                    'booking_id' => $booking_detail->booking_id,
                    'params_json' => $booking_detail->params_array,
                ];

                $options = json_decode(json_encode($saleItem->options), true);
                $options = array_merge($options, $add_options);

                $item = new OrderItems;
                $item->order_id = $order->id;
                $item->item_type = $booking_detail->getTable();
                $item->item_id = $booking_detail->id;
                $item->name = $saleItem->name;
                $item->options = $options;
                $item->qty = $saleItem->qty;
                $item->amount = $saleItem->amount;
                $item->discount_amount = $saleItem->discount_amount;
                $item->total_amount = $saleItem->total_amount;
                $item->currency_id = $saleItem->currency_id;
                $item->save();

                $amount += $saleItem->amount;
                $payable_amount += $saleItem->total_amount;
                $saleItem->delete();
            }

            $order->amount = $amount;
            $order->payable_amount = $payable_amount;
            $order->save();

            //save order in booking
            $params = $booking->params_array;
            $params = $params ? array_merge($params, ['order_id' => $order->id, 'reference_code' => $order->reference_code]) : ['order_id' => $order->id, 'reference_code' => $order->reference_code];
            $params = json_encode($params);
            $booking->params = $params;
            $booking->save();


        }
        $cart->delete();
        return $order;
    }

    /**
     * @param $cart
     * @param $order
     */
    private function addCodeToOrder($cart, $order)
    {
        //promotion
        if ($cart->promotion_id) {
            $orderPromotion = new OrderPromotions;
            $orderPromotion->order_id = $order->id;
            $orderPromotion->user_id = $cart->user_id;
            $orderPromotion->type = 'promotion';
            $orderPromotion->type_id = $cart->promotion_id;
            $orderPromotion->amount = $cart->promotion_amount;
            $orderPromotion->save();
        }

        //affiliate
        if ($cart->affiliate_id) {
            $origin_payable_amount = $cart->payable_amount + $cart->affiliate_amount;
            $data = [
                'origin_payable_amount' => $origin_payable_amount,
                'amount' => $cart->amount,
            ];
            $result = $this->calcOwnerAffiliate($cart);
            if ($result['action']) {
                unset($result['action']);
                $data = array_merge($data, $result);
            }
            $orderPromotion = new OrderPromotions;
            $orderPromotion->order_id = $order->id;
            $orderPromotion->user_id = $cart->user_id;
            $orderPromotion->type = 'affiliate';
            $orderPromotion->type_id = $cart->affiliate_id;
            $orderPromotion->amount = $cart->affiliate_amount;
            $orderPromotion->options = json_encode($data);
            $orderPromotion->save();
        }

        //voucher or coupen
        if ($cart->coupen) {
            $voucher = VoucherCodes::with('voucher')->find($cart->coupen);
            $used_qty = $voucher->used_qty;
            $options = ['voucher_code' => $voucher->code, 'active_time' => $voucher->active_time, 'type' => $voucher->voucher->type, 'value' => $voucher->voucher->value, 'max_value' => $voucher->voucher->max_value, 'max_qty' => $voucher->voucher->max_qty];
            $voucher->used_qty = $used_qty + 1;
            $voucher->save();
            $orderPromotion = new OrderPromotions;
            $orderPromotion->order_id = $order->id;
            $orderPromotion->user_id = $cart->user_id;
            $orderPromotion->type = 'coupen';
            $orderPromotion->type_id = $cart->coupen;
            $orderPromotion->amount = $cart->coupen_amount;
            $orderPromotion->options = json_encode($options);
            $orderPromotion->save();
        }

    }

    /**
     * @param $cart
     * @return array
     */
    private function calcOwnerAffiliate($cart)
    {
        if ($cart->affiliate_id) {
            $affiliate = Affiliate::active()->find($cart->affiliate_id);

            if ($affiliate) {
                $owner_value = $affiliate->owner_value;
                $user_id = $affiliate->user_id;
                $origin_payable_amount = $cart->payable_amount + $cart->affiliate_amount;
                $owner_amount = $origin_payable_amount * $owner_value;
                return ['action' => true, 'owner_amount' => $owner_amount, 'owner_user_id' => $user_id];
            }
        }
        return ['action' => false];
    }

    /**
     * @param bool $create
     * @return mixed
     */
    public function getCurrentCart($create = false)
    {
        return BridgeHelper::getCartHelper()->getCurrentCart($create);
    }

}
