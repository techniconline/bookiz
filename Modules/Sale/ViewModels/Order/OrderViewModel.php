<?php

namespace Modules\Sale\ViewModels\Order;

use BridgeHelper;
use Illuminate\Support\Facades\DB;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Sale\Models\Traits\Order\OrderMessageTrait;
use Modules\Sale\Models\Traits\Order\OrderViewTrait;
use Modules\Sale\Models\Traits\Payment\OrderStatesTrait;
use Modules\Sale\Models\Traits\Payment\PaymentStatesTrait;
use Modules\Sale\Models\Traits\Payment\PaymentTrait;
use Modules\Sale\Models\Traits\Sale\ItemTrait;

class OrderViewModel extends BaseViewModel
{
    use ItemTrait;
    use OrderViewTrait;
    use PaymentStatesTrait;
    use OrderStatesTrait;
    use OrderMessageTrait;

    private $order = false;
    private $payment = false;
    private $instance = false;
    public $application = null;

    public function getView()
    {
        $this->boot();
        $viewModel =& $this;
        if (app('getInstanceObject')->isApi()) {
            $order = $this->getOrder();
            $order = $this->decorateAttributes($order);
            return $this->setDataResponse($order)->setResponse(true);
        }
//        $this->layout = 'user.layout';
        $this->setTitlePage(trans('sale::sale.order.view'));
        if ($this->request->get("app") != "web") {
            $this->application = $this->request->get('app');
            $this->layout = "EmptyColumn";
            return $this->renderedView('sale::order.view_mobile', compact('viewModel'));
        }
        return $this->renderedView('sale::order.view', compact('viewModel'));
    }

    /**
     * @param null $key
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getConfigMobile($key = null)
    {
        $configs = config("mobile");
        if ($key) {
            if (isset($configs[$key])) {
                return $configs[$key];
            }
        }
        return $configs;
    }

    /**
     * @return mixed
     */
    public function getCurrentInstance()
    {
        return app('getInstanceObject')->getCurrentInstance();
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function boot()
    {
        $id = $this->request->get('id');
        $reference_code = $this->request->get('reference_code');
        if (!$id) {
            return abort(404, 'NOT FIND PAGE');
        }
        $this->order = $this->getOrderByReference($id, $reference_code);
        if (!$this->order) {
            return abort(403, 'Access Denied');
        }

    }

}
