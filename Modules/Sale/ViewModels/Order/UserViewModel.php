<?php

namespace Modules\Sale\ViewModels\Order;

use BridgeHelper;
use DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Sale\Models\Traits\Order\OrderViewTrait;
use Modules\Sale\Models\Traits\Payment\OrderStatesTrait;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Traits\Payment\PaymentCloseTrait;

class UserViewModel extends BaseViewModel
{
    use OrderViewTrait;
    use OrderStatesTrait;
    use GridViewModel;
    use PaymentCloseTrait;


    public function setGridModel()
    {
        $this->closeOpenedPayments();
        $user = BridgeHelper::getAccess()->getUser();
        $this->Model = Order::where('user_id', $user->id);
    }

    /**
     * @return $this
     */
    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('sale::form.fields.id'), true)
            ->addColumn('date', trans('sale::form.fields.date'), true)
            ->addColumn('status', trans('sale::form.fields.status'), true)
            ->addColumn('amount', trans('sale::form.fields.amount'), true)
            ->addColumn('payable_amount', trans('sale::form.fields.payable_amount'), true);

        $show = array(
            'name' => 'sale.order.view',
            'parameter' => ['id']
        );
        $this->addAction('show', $show, trans('sale::sale.order.view'), 'fa fa-eye', false, ['target' => '', 'class' => 'btn btn-sm btn-default']);

        $this->action_text = false;

        return $this;
    }

    public function getRowsUpdate($row)
    {
        $row->date = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.short'));
        if($row->state){
            $row->status=trans('sale::sale.order.states.'.$row->state);
        }else{
            $row->status='-';
        }
        return $row;
    }

    public function getList()
    {
//        $this->layout = 'user.layout';
        if (app('getInstanceObject')->isApi()) {
            return $this->getOrderListData();
        }
        $this->setTitlePage(trans('sale::sale.my_orders'));
        $this->generateGridList();
        return $this->renderedView("sale::order.list", ['view_model' => $this]);
    }

    /**
     * @return $this
     */
    protected function getOrderListData()
    {
        $this->setGridModel();
        $data = $this->Model->orderBy('id', 'DESC')->paginate();
        $data = $this->decorateList($data);
        return $this->setDataResponse($data)->setResponse(true);
    }

}
