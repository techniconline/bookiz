<?php

namespace Modules\Sale\ViewModels\Order;


use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Payment;
use Modules\Sale\Models\Traits\Payment\PaymentStatesTrait;


class PaymentGridViewModel extends BaseViewModel
{

    use GridViewModel;
    use PaymentStatesTrait;

    private $payment_assets;
    private $order_id;
    private $system;

    public function __construct($order_id = null, $system = null)
    {

    }

    public function bootViewModel($order_id = null, $system = null)
    {
        $this->order_id = $order_id ?: $this->request->get('order_id');
        $this->system = $system ?: $this->request->get('system');
        return $this;
    }

    public function setGridModel()
    {
        $this->bootViewModel();
        $this->Model = Payment::where(function ($q) {
            if ($this->order_id) {
                $q->where('action_id', $this->order_id);
            }
            if ($this->system) {
                $q->where('action', $this->system);
            }
        })
            ->filterCurrentInstance()->with("user");
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $payment_assets = [];

        //use bested assets
        if ($this->payment_assets) {
            $payment_assets [] = ['container' => 'plugin_general_2', 'src' => ("/plugins/chartjs/Chart.bundle.min.js"), 'name' => 'Chartjs.bundle'];
            $payment_assets [] = ['container' => 'plugin_general_2', 'src' => ("/plugins/chartjs/Chart.min.js"), 'name' => 'Chartjs'];
        }

        return $payment_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('reference_id', trans('sale::sale.payment.reference_code'), true)
            ->addColumn('user', trans('sale::sale.payment.user_name'), false)
            ->addColumn('amount', trans('sale::sale.payment.amount'), false)
            ->addColumn('action', trans('sale::sale.payment.action'), false)
            ->addColumn('gateway', trans('sale::sale.payment.gateway'), false)
            ->addColumn('action_id', trans('sale::sale.payment.action_id'), false)
            ->addColumn('request_id', trans('sale::sale.payment.request_code'), false)
            ->addColumn('state', trans('sale::sale.payment.state'), true)
            ->addColumn('created_at', trans('sale::sale.payment.created_at'), true)
            ->addColumn('updated_at', trans('sale::sale.payment.updated_at'), false);

        /*back action*/
        if ($this->system) {
            $add = array(
                'name' => 'sale.order.index',
                'parameter' => []
            );
            $this->addButton('back_order', $add, trans('sale::sale.order.list'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);

        }

        $show = array(
            'name' => 'sale.payment.edit',
            'parameter' => ['id']
        );
        $this->addAction('payment_single', $show, trans('sale::sale.payment.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $validate = array(
            'name' => 'sale.payment.validate',
            'parameter' => ['id']
        );
        $this->addAction('payment_validate', $validate, trans('sale::sale.payment.validate'), 'fa fa-refresh', false, ['class' => 'btn btn-sm btn-danger']);

        $this->action_text = false;

        //chart --------------------------------
//        $this->payment_assets = true;
//        $this->can_chart = true;
//        $this->setNameChart("sample_chart");
//        $this->setXField("created_at");
//        $this->setYField("request_id", trans('sale::sale.payment.action_id'));
//        $this->setChartSize(["width"=>600, "height"=>400]);

//        $optionsChart['scales'] = [
//            'yAxes'=>[['display'=>true,'ticks'=>['beginAtZero'=>true,'steps'=>10,'stepValue'=>5,'max'=>200]]]
//        ];
//        $this->setChartOptions($optionsChart);
        //chart --------------------------------


        /*filter*/
        $config = ['title' => trans('sale::sale.payment.reference_code')];
        $this->addFilter('reference_id', 'text', $config);
        $config = ['title' => trans('sale::sale.payment.request_code')];
        $this->addFilter('request_id', 'text', $config);
        $config = ['title' => trans('sale::sale.payment.created_at')];
        $this->addFilter('created_at', 'date', $config);
        $this->addFilter('last_name', 'text', ['relation' => 'user']);
        $this->addFilter('first_name', 'text', ['relation' => 'user']);
        $this->addFilter('username', 'text', ['relation' => 'user']);
        $this->addFilter('national_code', 'text', ['relation' => 'user']);

        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->user = $row->user ? $row->user->full_name . "(" . $row->user->username . ")" : "-";
        $row->gateway = isset($row->options->payment) ? $row->options->payment : "-";
        $row->reference_id = $row->reference_id ? ltrim($row->reference_id, 0) : "-";
        $row->request_id = $row->request_id ? ltrim($row->request_id, 0) : "-";
        $order = $row->getActionModel();
        $row->action_id = $row->action && $row->action_id && $order ? '<a target="_blank" href="' . route('sale.order.index', ['filter' => 1, 'reference_code' => $order->reference_code]) . '">' . $row->action_id . '</a>' : $row->action_id;
        $row->state = $row->status_text;
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
        if ($row->state == $this->getPaymentCompleteState()) {
            unset($action['payment_single'], $action['payment_validate']);
        }
        return $action;
    }

    /**
     * @return $this
     */
    protected function setAssetsEditVideo()
    {
        $this->payment_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->boot();
        $order = Order::find($this->order_id);
        $this->setTitlePage(trans('sale::sale.payment.list_by_order', ['reference_code' => $order ? $order->reference_code : null]));
        $this->generateGridList()->renderedView("sale::payment.index", ['view_model' => $this], "payment_list");
        return $this;
    }


}
