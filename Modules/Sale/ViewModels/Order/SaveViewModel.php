<?php

namespace Modules\Sale\ViewModels\Order;

use BridgeHelper;
use Illuminate\Support\Facades\DB;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Sale\Models\Affiliate;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Order\OrderItems;
use Modules\Sale\Models\Traits\Payment\OrderStatesTrait;
use Modules\Sale\Models\Traits\Payment\PaymentCloseTrait;
use Modules\Sale\Models\Traits\Payment\PaymentStatesTrait;
use Modules\Sale\Models\Traits\Payment\PaymentTrait;
use Modules\Sale\Models\Order\OrderPromotions;
use Modules\Sale\Models\Voucher\VoucherCodes;

class SaveViewModel extends BaseViewModel
{
    use PaymentStatesTrait, OrderStatesTrait, PaymentTrait,PaymentCloseTrait;


    /** STEP 1 payment
     * @return $this
     */
    public function saveOrder()
    {
        $this->closeOpenedPayments();
        $cartManager = $this->getCurrentCart($create = false);
        $cart = $cartManager->validateCart()->getCart();
        if (!$cart || !isset($cart->cartItems) || !$cartManager->canUseDiscount() || !$cart->cartItems->count()) {
            return $this->redirectBack()->setResponse(false, trans('sale::message.cart_not_find'));
        }
        if ($cart->payable_amount > 0) {
            $rules = [
                'payment' => 'required|in:online',
                'option' => 'required|array',
            ];
            $this->request->validate($rules);
        }
        DB::beginTransaction();
        try {
            $order = $this->createOrderByItems($cart);
            if ($order->payable_amount > 0) {
                $options = ($this->request->has('option')) ? $this->request->get('option') : [];
                $type = ($this->request->has('payment')) ? $this->request->get('payment') : 'online';
                $this->createOrderPayment($order, $options, $type);
            }
            DB::commit();

            if ($order->payable_amount > 0) {
                $redirect = route('sale.order.payment', ['id' => $order->id, "reference_code" => $order->reference_code, "app" => app('getInstanceObject')->isApi() ? "mobile" : "web"]);
            } else {
                $redirect = route('sale.order.complete', ['id' => $order->id, "reference_code" => $order->reference_code, "app" => app('getInstanceObject')->isApi() ? "mobile" : "web"]);
            }
        } catch (\Exception $e) {
            report($e);
            DB::rollBack();
            $error = trans('sale::message.item_add_error');
            return $this->redirectBack()->setResponse(false, $error);
        }
        return $this->redirect($redirect)->setDataResponse($order)->setResponse(true, trans("sale::message.save_order"));
    }


    public function createOrderByItems($cart)
    {
        $instance = app('getInstanceObject')->getCurrentInstance();
        $order = new Order;
        $order->instance_id = $instance->id;
        $order->reference_code = $order->getReferenceCode();
        $order->user_id = $cart->user_id;
        $order->state = $this->getOrderNewState();
        $order->amount = $cart->amount;
        $order->payable_amount = $cart->payable_amount;
        $order->currency_id = $cart->currency_id;
        $order->save();
        if ($order->id) {
            $this->addCodeToOrder($cart, $order);
            foreach ($cart->cartItems as $saleItem) {
                $item = new OrderItems;
                $item->order_id = $order->id;
                $item->item_type = $saleItem->item_type;
                $item->item_id = $saleItem->item_id;
                $item->name = $saleItem->name;
                $item->options = $saleItem->options;
                $item->qty = $saleItem->qty;
                $item->amount = $saleItem->amount;
                $item->discount_amount = $saleItem->discount_amount;
                $item->total_amount = $saleItem->total_amount;
                $item->currency_id = $saleItem->currency_id;
                $item->save();
                $saleItem->delete();
            }
        }
        $cart->delete();
        return $order;
    }

    /**
     * @param $cart
     * @param $order
     */
    private function addCodeToOrder($cart, $order)
    {
        //promotion
        if ($cart->promotion_id) {
            $orderPromotion = new OrderPromotions;
            $orderPromotion->order_id = $order->id;
            $orderPromotion->user_id = $cart->user_id;
            $orderPromotion->type = 'promotion';
            $orderPromotion->type_id = $cart->promotion_id;
            $orderPromotion->amount = $cart->promotion_amount;
            $orderPromotion->save();
        }

        //affiliate
        if ($cart->affiliate_id) {
            $origin_payable_amount = $cart->payable_amount + $cart->affiliate_amount;
            $data = [
                'origin_payable_amount' => $origin_payable_amount,
                'amount' => $cart->amount,
            ];
            $result = $this->calcOwnerAffiliate($cart);
            if ($result['action']) {
                unset($result['action']);
                $data = array_merge($data, $result);
            }
            $orderPromotion = new OrderPromotions;
            $orderPromotion->order_id = $order->id;
            $orderPromotion->user_id = $cart->user_id;
            $orderPromotion->type = 'affiliate';
            $orderPromotion->type_id = $cart->affiliate_id;
            $orderPromotion->amount = $cart->affiliate_amount;
            $orderPromotion->options = json_encode($data);
            $orderPromotion->save();
        }

        //voucher or coupen
        if ($cart->coupen) {
            $voucher = VoucherCodes::with('voucher')->find($cart->coupen);
            $used_qty = $voucher->used_qty;
            $options = ['voucher_code' => $voucher->code, 'active_time' => $voucher->active_time, 'type' => $voucher->voucher->type, 'value' => $voucher->voucher->value, 'max_value' => $voucher->voucher->max_value, 'max_qty' => $voucher->voucher->max_qty];
            $voucher->used_qty = $used_qty + 1;
            $voucher->save();
            $orderPromotion = new OrderPromotions;
            $orderPromotion->order_id = $order->id;
            $orderPromotion->user_id = $cart->user_id;
            $orderPromotion->type = 'coupen';
            $orderPromotion->type_id = $cart->coupen;
            $orderPromotion->amount = $cart->coupen_amount;
            $orderPromotion->options = json_encode($options);
            $orderPromotion->save();
        }

    }

    /**
     * @param $cart
     * @return array
     */
    private function calcOwnerAffiliate($cart)
    {
        if ($cart->affiliate_id) {
            $affiliate = Affiliate::active()->find($cart->affiliate_id);

            if ($affiliate) {
                $owner_value = $affiliate->owner_value;
                $user_id = $affiliate->user_id;
                $origin_payable_amount = $cart->payable_amount + $cart->affiliate_amount;
                $owner_amount = $origin_payable_amount * $owner_value;
                return ['action' => true, 'owner_amount' => $owner_amount, 'owner_user_id' => $user_id];
            }
        }
        return ['action' => false];
    }

    /**
     * @param bool $create
     * @return mixed
     */
    public function getCurrentCart($create = false)
    {
        return BridgeHelper::getCartHelper()->getCurrentCart($create);
    }

}
