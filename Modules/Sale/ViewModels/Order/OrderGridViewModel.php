<?php

namespace Modules\Sale\ViewModels\Order;


use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Traits\Payment\OrderStatesTrait;
use Modules\Video\Models\Video\VideoUse;


class OrderGridViewModel extends BaseViewModel
{

    use GridViewModel;
    use OrderStatesTrait;

    private $order_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = Order::enable()->filterCurrentInstance()->with("user", "orderItems", "orderPromotions");
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $order_assets = [];

        //use bested assets
        if ($this->order_assets) {

        }

        return $order_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('reference_code', trans('sale::sale.order.reference_code'), true)
            ->addColumn('user', trans('sale::sale.order.user_name'), false)
            ->addColumn('payable_amount', trans('sale::sale.order.payable_amount'), false)
            ->addColumn('amount', trans('sale::sale.order.amount'), false)
            ->addColumn('coupen', trans('sale::sale.order.coupen'), false)
            ->addColumn('affiliate', trans('sale::sale.order.affiliate'), false)
            ->addColumn('promotion', trans('sale::sale.order.promotion'), false)
            ->addColumn('state', trans('sale::sale.order.state'), true)
            ->addColumn('created_at', trans('sale::sale.order.created_at'), true)
            ->addColumn('updated_at', trans('sale::sale.order.updated_at'), false);

        $show = array(
            'name' => 'sale.order.edit',
            'parameter' => ['id', 'reference_code']
        );
        $this->addAction('order_single', $show, trans('sale::sale.order.view'), 'fa fa-eye', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'sale.order.payments',
            'parameter' => ['id']
        );
        $this->addAction('order_payments', $show, trans('sale::sale.payment.list'), 'fa fa-credit-card', false, ['target' => '', 'class' => 'btn btn-sm btn-default']);


        $this->action_text = false;

        /*filter*/
        $config = ['title' => trans('sale::sale.order.reference_code')];
        $this->addFilter('reference_code', 'text', $config);
        $config = ['title' => trans('sale::sale.order.order_id')];
        $this->addFilter('id', 'text', $config);

        $config = ['title' => trans('sale::sale.order.created_at')];
        $this->addFilter('created_at', 'date', $config);

        /*filter*/
        $this->addFilter('last_name', 'text', ['relation' => 'user']);
        $this->addFilter('first_name', 'text', ['relation' => 'user']);
        $this->addFilter('username', 'text', ['relation' => 'user']);

        $this->can_export = 1;
        $this->setExportFileName('order_details');

        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->user = $row->user ? $row->user->full_name : "-";
        $row->state = $row->state_text;
        $row->promotion = '-';
        $row->affiliate = '-';
        $row->coupen = '-';
        if($orderPromotions = $row->orderPromotions){
            foreach ($orderPromotions as $orderPromotion) {
                $row->{$orderPromotion->type} = $orderPromotion->amount;
            }
        }
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
//        if ($row->state==$this->getOrderCompleteState()) {
//            unset($action['accept_order'],$action['delete']);
//        }
//
//        if(key_exists('accept_order', $action) && !$row->has_success_payment){
//            unset($action['accept_order']);
//        }
        return $action;
    }

    /**
     * @return $this
     */
    protected function setAssetsEditOrder()
    {
        $this->order_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('sale::sale.order.list'));
        $this->generateGridList()->renderedView("sale::order.index", ['view_model' => $this], "order_list");
        return $this;
    }

    public function exportToExcel()
    {
//        dd($this->request->all(), DateHelper::setLocaleDateTime('1397-07-01 10:10:10', 'Y-m-d H:i:s')->getDateTime('Y-m-d H:i:s'));

        $data = $this->generateGridList();
        $data->Model->with('orderItems');
        $data = $data->getGridJson();
//        dd($data->toArray());
        $data = $this->decorateExport($data->getCollection());
        $order = new Order\OrderExport();
        $file = $order->setCollection($data)->download('orders.xlsx');

//        $file = Excel::download($data, 'orders.xlsx');
        return $this->setFileResponse($file)->setResponse(true);

    }


    public function getExportHeader()
    {
        $header = [
            'reference_code' => trans("sale::sale.reference_code"),
            'instance_id' => trans("sale::sale.instance_id"),
            'user_id' => trans("sale::sale.user_id"),
            'state' => trans("sale::sale.state"),
            'amount' => trans("sale::sale.amount"),
            'payable_amount' => trans("sale::sale.payable_amount"),
            'created_at' => trans("sale::sale.created_at"),
            'updated_at' => trans("sale::sale.updated_at"),
            'cost' => trans("sale::sale.cost"),
            'total' => trans("sale::sale.total"),

            'coupen' => trans('sale::sale.order.coupen'),
            'affiliate' => trans('sale::sale.order.affiliate'),
            'promotion' => trans('sale::sale.order.promotion'),

            'discount' => trans("sale::sale.discount"),
            'discount_percent' => trans("sale::sale.discount_percent"),
            'discount_text' => trans("sale::sale.discount_text"),
            'user_name' => trans("sale::sale.user_name"),
            'connectors' => 'connectors',
            'row_item_type' => trans("sale::sale.row_item_type"),
            'row_item_id' => trans("sale::sale.row_item_id"),
            'row_name' => trans("sale::sale.row_name"),
            'row_qty' => trans("sale::sale.row_qty"),
            'row_amount' => trans("sale::sale.row_amount"),
            'row_discount_amount' => trans("sale::sale.row_discount_amount"),
            'row_has_shipment' => trans("sale::sale.row_has_shipment"),
            'row_total_amount' => trans("sale::sale.row_total_amount"),
            'row_cost' => trans("sale::sale.row_cost"),
            'row_total' => trans("sale::sale.row_total"),
            'row_discount' => trans("sale::sale.row_discount"),
            'row_discount_percent' => trans("sale::sale.row_discount_percent"),

        ];
        return $header;
    }

    public function getExportRow($row)
    {

        $csvRow = [];
        $counter = 0;

        if ($row->orderItems) {
            $userConnectors = $row->user ? $row->user->userConnectors : null;
            $connectors = null;
            if ($userConnectors) {
                $connectors = $userConnectors->pluck('value')->toArray();
                $connectors = implode(", ", $connectors);
            }

            $row->promotion = '-';
            $row->affiliate = '-';
            $row->coupen = '-';
            if($orderPromotions = $row->orderPromotions){
                foreach ($orderPromotions as $orderPromotion) {
                    $row->{$orderPromotion->type} = $orderPromotion->amount;
                }
            }

            foreach ($row->orderItems as $index => $order_item) {
                $csvRow[] = [
                    'reference_code' => $row->reference_code,
                    'instance_id' => $row->instance_id,
                    'user_id' => $row->user_id,
                    'state' => $row->state_text,
                    'amount' => $row->amount,
                    'payable_amount' => $row->payable_amount,
                    'created_at' => $row->created_at,
                    'updated_at' => $row->updated_at,
                    'cost' => $row->cost,
                    'total' => $row->total,

                    'coupen' => $row->coupen,
                    'affiliate' => $row->affiliate,
                    'promotion' => $row->promotion,

                    'discount' => $row->discount,
                    'discount_percent' => $row->discount_percent,
                    'discount_text' => $row->discount_text,
                    'user_name' => $row->user ? $row->user->full_name : '',
                    'connectors' => $connectors,
                    'row_item_type' => $order_item->item_type,
                    'row_item_id' => $order_item->item_id,
                    'row_name' => $order_item->name,
                    'row_qty' => $order_item->qty,
                    'row_amount' => $order_item->amount,
                    'row_discount_amount' => $order_item->discount_amount,
                    'row_has_shipment' => $order_item->has_shipment,
                    'row_total_amount' => $order_item->total_amount,
                    'row_cost' => $order_item->cost,
                    'row_total' => $order_item->total,
                    'row_discount' => $order_item->discount,
                    'row_discount_percent' => $order_item->discount_percent,
                ];

                $counter++;
            }
        }

        return $csvRow;
    }

}
