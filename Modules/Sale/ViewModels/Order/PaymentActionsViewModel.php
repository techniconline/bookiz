<?php

namespace Modules\Sale\ViewModels\Order;


use DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Payment;
use Modules\Sale\Models\Traits\Payment\PaymentStatesTrait;
use Modules\Sale\Models\Traits\Payment\PaymentTrait;


class PaymentActionsViewModel extends BaseViewModel
{

    use PaymentStatesTrait;
    use PaymentTrait;


    private $payment_assets;
    private $payment_id;

    public function __construct()
    {

    }

    public function boot($payment_id = null)
    {
        $this->payment_id = $payment_id ?: $this->request->get('payment_id');
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $payment_assets = [];

        //use bested assets
        if ($this->payment_assets) {

        }

        return $payment_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return $this
     */
    protected function setAssetsEditPayment()
    {
        $this->payment_assets = true;
        return $this;
    }

    public function editPayment()
    {
        $this->boot();
        $payment = Payment::with('user', 'order')->find($this->payment_id);
        $viewModel =& $this;
        $this->setTitlePage(trans('sale::sale.payment.edit'));
        return $this->renderedView("sale::payment.form", ['payment' => $payment,'view_model'=>$viewModel], "form");
    }

    public function updatePayment()
    {
        $this->boot();
        $model = new Payment();

        if($this->payment_id){
            $model = $model->find($this->payment_id);
        }

        if($model->fill($this->request->all())){
            $model->save();
            $message = trans("course::messages.alert.save_success");
            $action = true;
            if($this->request->get('state')==$this->getPaymentSuccessState()){
                $order = $this->getOrder($model);
                $paymentViewModel = new PaymentViewModel();
                $response = $paymentViewModel->getComplete($order)->getResponse();
                $message = isset($response['message'])?$response['message']:$message;
                $action = isset($response['action'])?$response['action']:$action;
            }
            return $this->redirect(route('sale.payment.edit', ['payment_id' => $model->id]))->setResponse($action, $message);
        }
        return $this->redirectBack()->setResponse(false, trans("course::messages.alert.mis_data"), $model->errors );
    }

    /**
     * @param $payment
     * @return bool
     */
    public function getOrder($payment)
    {
        if(!$payment){
            return false;
        }
        $action = $payment->action;
        if($action == 'order'){
            return Order::find($payment->action_id);
        }
        return false;
    }


    public function validatePayment(){
        $this->boot();
        if(!$this->payment_id){
            abort(404);
        }
        $paymentModel= Payment::with('order')->find($this->payment_id);
        if(!$paymentModel){
            abort(404);
        }
        if(in_array($paymentModel->state,$this->getPaymentSuccessStates())){
            return $this->redirect(route('sale.order.complete',['id' => $paymentModel->order->id, "app"=>'web', "reference_code"=>$paymentModel->order->reference_code]))->setResponse('true',trans('sale::message.payment_and_order_compelete'));
        }
        $masterPayment = $this->getMasterPayment();
        $payment=$masterPayment->getPaymentRevalidate($paymentModel);
        if ($payment) {
            return $this->redirect(route('sale.order.complete',['id' => $paymentModel->order->id, "app"=>'web', "reference_code"=>$paymentModel->order->reference_code]))->setResponse('true',trans('sale::message.payment_and_order_compelete'));
        } else {
            return $this->redirectBack()->setResponse('false',trans('sale::message.payment_can_not_revalidate'));
        }
    }
}
