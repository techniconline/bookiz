<?php

namespace Modules\Sale\ViewModels\Order;

use BridgeHelper;
use Illuminate\Support\Facades\DB;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Sale\Models\Credit;
use Modules\Sale\Models\Order\OrderPromotions;
use Modules\Sale\Models\Traits\Order\OrderMessageTrait;
use Modules\Sale\Models\Traits\Order\OrderViewTrait;
use Modules\Sale\Models\Traits\Payment\OrderPaymentTrait;
use Modules\Sale\Models\Traits\Payment\OrderStatesTrait;
use Modules\Sale\Models\Traits\Payment\PaymentStatesTrait;
use Modules\Sale\Models\Traits\Payment\PaymentTrait;
use Modules\Sale\Models\Traits\Sale\ItemTrait;
use Modules\User\Traits\Credit\CreditTrait;

class PaymentViewModel extends BaseViewModel
{
    use OrderViewTrait;
    use PaymentTrait;
    use PaymentStatesTrait;
    use OrderStatesTrait;
    use ItemTrait;
    use OrderMessageTrait;
    use CreditTrait;

    private $order = false;
    private $payment = false;
    protected $order_id=null;
    protected $app='web';
    protected $reference_code=null;


    /**
     * STEP 2 payment
     * @return $this
     */
    public function getOrderPayment()
    {
        $this->boot();
        if ($this->order->payable_amount > 0) {
            $this->payment = $this->getOrderCurrentPayment($this->order, $this->getPaymentNewState());
            if (!$this->payment) {
                if (app('getInstanceObject')->isApi()) {
                    return $this->setResponse(false, trans('sale::payment.order_payment_not_find'));
                }
                return $this->getRedirectOrder()->setResponse(false);
            }
            $masterPayment = $this->getMasterPayment();
            $this->layout = 'EmptyColumn';
            return $this->setDataResponse($masterPayment->getPaymentPage($this->payment), 'content')->setResponse(true);
        } else {
            return $this->getComplete();
        }
    }


    public function getCallBack()
    {
        DB::beginTransaction();
        try{
            $this->boot();
            $this->payment = $this->getOrderCurrentPayment($this->order, $this->getPaymentSendState());
            if (!$this->payment) {
                DB::rollBack();
                return $this->getRedirectOrder()->setResponse(false);
            }
            $this->payment->state=$this->getPaymentBackState();
            $this->payment->save();
            DB::commit();
        }catch(\Exception $e) {
            report($e);
            DB::rollBack();
        }
        $masterPayment = $this->getMasterPayment();
        $this->payment = $masterPayment->getPaymentVerify($this->payment);
        if ($this->payment && $this->payment->state == $this->getPaymentSuccessState()) {
            return $this->getComplete();
        } else {
            return $this->getComplete();
        }
    }

    public function getClosePayment()
    {
        if(!$this->payment){
            return $this->getRedirectOrder()->setResponse('false');
        }
        if (!in_array($this->payment->state, $this->getPaymentSuccessStates())) {
            $masterPayment = $this->getMasterPayment();
            $this->payment=$masterPayment->getPaymentRevalidate($this->payment);
        }
        if ($this->payment && $this->payment->state == $this->getPaymentSuccessState()) {
            return $this->getComplete();
        } else {
            return $this->getComplete();
        }
    }

    public function getComplete($order = null)
    {
        if (!$this->order && !$order) {
            $this->boot(false);
        }
        if ($order) {
            $this->order = $order;
        }
        if (in_array($this->order->state, $this->getOrderCompleteStates())) {
            return $this->getRedirectOrder()->setResponse(false, '');
        }
        $canComplete = false;
        if ($this->order->payable_amount > 0) {
            if (!$this->payment) {
                $this->payment = $this->getOrderCurrentPayment($this->order, $this->getPaymentSuccessState());
                if (!$this->payment) {
                    return $this->getRedirectOrder()->setResponse(false, trans('sale::payment.order_payment_not_find'));
                }
            }
            if ($this->payment->state == $this->getPaymentSuccessState() && $this->order->state != $this->getOrderReviewState()) {
                $this->order->state = $this->getOrderReviewState();
                $this->setOrderMessage($this->order->id, trans('sale::payment.order_message', ['reference_id' => $this->payment->reference_id]));
                $this->setOrderMessage($this->order->id, trans('sale::sale.order.order_message', ['reference_code' => $this->order->reference_code]));
                $this->order->save();
                $canComplete = true;
            } elseif ($this->payment->state != $this->getPaymentSuccessState()) {
                $this->order->state = $this->getOrderCancelState();
                $this->setOrderMessage($this->order->id, trans('sale::payment.fail'));
                $this->setOrderMessage($this->order->id, trans('sale::sale.order.order_message', ['reference_code' => $this->order->reference_code]));
                $this->order->save();
                $canComplete = false;
            } elseif ($this->order->state == $this->getOrderReviewState()) {
                $canComplete = true;
            }
        } else {
            $canComplete = true;
        }
        /*run item Success*/
        if ($canComplete) {
            try {
                DB::beginTransaction();
                $order_error_messsage=trans('course::enrollment.enroll_error_order_message');
                foreach ($this->order->orderItems as $item) {
                    if ($saleItem = $this->getSaleItem($item->item_type, $item->item_id, $item->options_array)) {
                        $result = $saleItem->setOrderComplete($this->order, $item);
                        if ($result && !is_bool($result)) {
                            $this->setOrderMessage($this->order->id, $result);
                        }else{
                            $this->setOrderMessage($this->order->id, $order_error_messsage.'(150)');
                        }
                    }else{
                        $this->setOrderMessage($this->order->id, $order_error_messsage.'(153)');
                    }
                }
                $this->order->state = $this->getOrderCompleteState();
                $this->order->save();
                if ($this->order->payable_amount > 0) {
                    $this->payment->state = $this->getPaymentCompleteState();
                    $this->payment->save();
                }
                $this->addCredit();
                DB::commit();
            } catch (\Exception $e) {
                report($e);
                DB::rollBack();
                $error = trans('sale::payment.order_complete_error');
                return $this->getRedirectOrder()->setResponse(false, $error);
            }
        } else {
            $error = trans('sale::payment.fail');
            return $this->getRedirectOrder()->setResponse(false, $error);
        }
        return $this->getRedirectOrder()->setResponse(true, trans('sale::payment.order_completed'));
    }

    /**
     * void
     */
    private function addCredit()
    {
        if ($this->order) {
            $orderPromotion = OrderPromotions::active()->where('order_id', $this->order->id)->where('type', OrderPromotions::TYPE_AFFILIATE)->orderBy('id', 'DESC')->first();
            if ($orderPromotion) {
                $options = $orderPromotion->options;
                if (isset($options->owner_user_id) && isset($options->owner_amount) && $options->owner_user_id) {
                    $this->initCredit($options->owner_amount, $options->owner_user_id, 'order', $this->order->id);
                }

            }
        }
    }

    public function boot($needPayment = false)
    {
        $this->setOrderBoot();
        if (!$this->order_id || !$this->reference_code) {
            return abort(404, 'NOT FIND PAGE');
        }
        $this->order = $this->getOrderByReference($this->order_id, $this->reference_code);
        if (!$this->order) {
            return abort(403, 'Access Denied');
        }
        if ($needPayment && $this->order && $this->order->payable_amount > 0) {
            $this->payment = $this->getOrderLastPayment($this->order->id);
        }
    }


    public function getRepay()
    {
        $this->boot();
        $lastPayment = $this->getOrderLastPayment($this->order->id);
        if (!$this->canRePay($this->order) && $lastPayment) {
            $error = trans('sale::message.payment_error');
            return $this->redirectBack()->setResponse(false, $error);
        }
        foreach ($this->order->orderItems as $item) {
            if ($saleItem = $this->getSaleItem($item->item_type, $item->item_id, $item->options_array)) {
                if (!$saleItem->canSaleItem()) {
                    $error = trans('sale::message.payment_error');
                    return $this->redirectBack()->setResponse(false, $error);
                }
            }
        }
        DB::beginTransaction();
        try {
            $this->order->state = $this->getOrderPaymentState();
            $options = $lastPayment->options_array;
            $type = $lastPayment->type;
            $this->createOrderPayment($this->order, $options, $type);
            $this->order->save();
            DB::commit();
            $redirect = route('sale.order.payment', ['id' => $this->order->id, "reference_code" => $this->order->reference_code, "app" => app('getInstanceObject')->isApi() ? "mobile" : "web"]);
        } catch (\Exception $e) {
            report($e);
            DB::rollBack();
            $error = trans('sale::message.payment_error');
            return $this->redirectBack()->setResponse(false, $error);
        }
        return $this->redirect($redirect)->setResponse(true);
    }


    public function setOrderBoot($id=null,$reference_code=null,$app='web'){
        if($this->order_id){
            return $this;
        }
        if(is_null($id) || is_null($reference_code)){
            $this->order_id = $this->request->get('id');
            $this->reference_code = $this->request->get('reference_code');
            $this->app = $this->request->get("app", "web");
        }else{
            $this->order_id=$id;
            $this->reference_code=$reference_code;
            $this->app=$app;
        }
        return $this;
    }


    public function getRedirectOrder($url=null){
        if(is_null($url)){
            $url=route('sale.order.view', ['id' => $this->order_id, "app" => $this->app, "reference_code" => $this->reference_code]);
        }
        return $this->redirect($url);
    }

    public function setPayment($payment){
        $this->payment=$payment;
        if($this->payment->action=='order'){
            $this->order=$this->getOrderById($payment->action_id);
        }
        return $this;
    }
}
