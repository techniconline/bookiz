<?php

namespace Modules\Sale\ViewModels\Owner;

use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Sale\Models\Owner\Owner;

class OwnerGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $order_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = Owner::enable()->filterCurrentInstance();
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $order_assets = [];

        //use bested assets
        if ($this->order_assets) {

        }

        return $order_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('sale::owner.owner_title'), true);
        $this->addColumn('value', trans('sale::owner.value'), true);
        $this->addColumn('type', trans('sale::owner.type'), true);
        $this->addColumn('state', trans('sale::owner.state'), true);
        $this->addColumn('updated_at', trans('sale::owner.updated_at'), true);
        $this->addColumn('created_at', trans('sale::owner.created_at'), true);


        $routeAdd=route('sale.owner.create');
        $options=['class'=>"btn btn-primary"];
        $this->addButton('add',$routeAdd,trans('sale::owner.create_owner'),'fa fa-plus',false,'before',$options);


        $show = array(
            'name' => 'sale.owner.items.index',
            'parameter' => ['id']
        );
        $this->addAction('create_item', $show, trans('sale::owner.items.list'), 'fa fa-list', false, ['target' => '', 'class' => 'btn btn-sm btn-primary']);

        $show = array(
            'name' => 'sale.owner.edit',
            'parameter' => ['id']
        );
        $this->addAction('edit_item', $show, trans('sale::owner.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('sale::owner.confirm_delete')];

        $delete = array(
            'name' => 'sale.owner.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('sale::owner.delete'), 'fa fa-trash', false, $options);

        $this->action_text = false;

        /*filter*/
        $config = ['title' => trans('sale::owner.owner_title')];
        $this->addFilter('title', 'text', $config);

        $config = ['title' => trans('sale::owner.created_at')];
        $this->addFilter('created_at', 'date', $config);
        
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {

        $row->type = trans('sale::owner.types.'.$row->type);
        $row->state = $row->status_text;
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
//        if ($row->state==$this->getOwnerCompleteState()) {
//            unset($action['accept_order'],$action['delete']);
//        }
//
//        if(key_exists('accept_order', $action) && !$row->has_success_payment){
//            unset($action['accept_order']);
//        }
        return $action;
    }

    /**
     * @return $this
     */
    protected function setAssetsEditOwner()
    {
        $this->order_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('sale::owner.owner_list'));
        $this->generateGridList()->renderedView("sale::owner.index", ['view_model' => $this], "owner_list");
        return $this;
    }

}
