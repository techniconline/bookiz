<?php

namespace Modules\Sale\ViewModels\Owner;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\Sale\Models\Owner\Owner;

class OwnerActionsViewModel extends BaseViewModel
{

    private $access_assets;
    private $filter_items = [];

    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {
//            $access_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/exam/exam.js"), 'name' => 'exam-backend'];
        }

        return $access_assets;
    }


    /**
     * @return $this
     */
    protected function setAssetsCreateOwner()
    {
        $this->access_assets = true;
        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->filter_items = $params ? $params : request()->except(array_keys($this->filter_items));
        return $this;
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createOwner()
    {
        $this->init();
        $model = new Owner();
        $model = $model->find($this->request->get('owner_id'));
        $viewModel =& $this;

        $this->setModelData($model);
        $this->setTitlePage(trans('sale::owner.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("sale::owner.form_owner", ['view_model' => $viewModel], "form");
    }

    /**
     * @return $this
     */
    protected function destroyOwner()
    {
        $owner_id = $this->request->get('owner_id');
        $owner = Owner::enable()->find($owner_id);

        if ($owner->delete()) {
            return $this->setResponse(true, trans("sale::message.alert.del_success"));
        }
        return $this->setResponse(false, trans("sale::message.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveOwner()
    {

        $data = $this->request->all();
        $response = $this->serviceSaveOwner($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('sale.owner.edit', ['owner_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @param $data
     * @return array
     */
    public function serviceSaveOwner($data)
    {
        $model = new Owner();

        $owner_id = isset($data['owner_id']) ? $data['owner_id'] : 0;
        if ($owner_id) {
            $model = $model->enable()->find($owner_id);
            if (!$model) {
                return ['action' => false, 'message' => trans("sale::message.alert.not_find_data")];
            }
        }

        if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
            $model->fill($data);
            $model->instance_id = get_instance()->getCurrentInstanceId();

            if ($model->save()) {
                return ['action' => true, 'message' => trans("sale::message.alert.save_success"), 'data' => $model];
            } else {
                return ['action' => false, 'message' => trans("sale::message.alert.save_un_success")];
            }
        }
        return ['action' => false, 'message' => trans("sale::message.alert.mis_data"), 'errors' => $this->errors];
    }

    /**
     * @return $this
     */
    public function getOwnersByApi()
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }
        $model = Owner::select('id', 'title')->filterCurrentInstance();
        if ($q) {
            $model->where('title', 'like', '%' . $q . '%');
            $items = $model->get();
        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $model = Owner::select('id', 'title')->active()->filterCurrentInstance()->orderBy('id', 'DESC')->take(10);
            $items = $model->get();
        }
        $items = $items->map(function ($item) {
            $item->text = $item->title;
            return $item;
        });
        return $this->setDataResponse($items)->setResponse(true);
    }

}
