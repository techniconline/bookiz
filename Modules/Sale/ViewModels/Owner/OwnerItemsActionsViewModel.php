<?php

namespace Modules\Sale\ViewModels\Owner;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Sale\Models\Owner\OwnerItem;

class OwnerItemsActionsViewModel extends BaseViewModel
{

    private $access_assets;
    private $filter_items = [];

    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {
//            $access_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/exam/exam.js"), 'name' => 'exam-backend'];
        }

        return $access_assets;
    }


    /**
     * @return $this
     */
    protected function setAssetsCreateOwnerItem()
    {
        $this->access_assets = true;
        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function init($params = [])
    {
        $this->filter_items = $params ? $params : request()->except(array_keys($this->filter_items));
        return $this;
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createOwnerItem()
    {
        $this->init();
        $model = new OwnerItem();
        $model = $model->find($this->request->get('owner_item_id'));

        $viewModel =& $this;

        $this->setModelData($model);
        $this->setTitlePage(trans('sale::owner.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("sale::owner.form_owner_item", ['view_model' => $viewModel], "form");
    }

    /**
     * @return $this
     */
    protected function destroyOwnerItem()
    {
        $owner_item_id = $this->request->get('owner_item_id');
        $owner_item = OwnerItem::enable()->find($owner_item_id);

        if ($owner_item->delete()) {
            return $this->setResponse(true, trans("sale::message.alert.del_success"));
        }
        return $this->setResponse(false, trans("sale::message.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveOwnerItem()
    {

        $data = $this->request->all();
        $model_ids = $this->request->get('model_ids');
        $response['action'] = false;
        $response['message'] = trans("sale::message.alert.mis_data");
        if (!empty($model_ids) && is_array($model_ids)){
            foreach ($model_ids as $model_id) {
                $data['model_id'] = $model_id;
                $response[] = $this->serviceSaveOwnerItem($data);
            }

        }else{
            $response = $this->serviceSaveOwnerItem($data);
        }

        if (isset($response['action'])){
            if (!$response['action']) {
                $this->redirectBack();
            } else {
                $this->redirect(route('sale.owner.items.edit', ['owner_item_id' => $response['data']->id]));
            }
        }else{
            $this->redirect(route('sale.owner.items.index', ['owner_id' =>$this->request->get('owner_id')]));
        }

        return $this->setDataResponse(isset($response) ? $response : [])
            ->setResponse(true, trans("sale::message.alert.save_success"));
    }

    /**
     * @param $data
     * @return array
     */
    public function serviceSaveOwnerItem($data)
    {
        $model = new OwnerItem();

        $owner_item_id = isset($data['owner_item_id']) ? $data['owner_item_id'] : 0;
        if ($owner_item_id) {
            $model = $model->enable()->find($owner_item_id);
            if (!$model) {
                return ['action' => false, 'message' => trans("sale::message.alert.not_find_data")];
            }
        }

        if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {
            $model->fill($data);

            if ($model->save()) {
                return ['action' => true, 'message' => trans("sale::message.alert.save_success"), 'data' => $model];
            } else {
                return ['action' => false, 'message' => trans("sale::message.alert.save_un_success")];
            }
        }
        return ['action' => false, 'message' => trans("sale::message.alert.mis_data"), 'errors' => $this->errors];
    }

    public function searchInModel()
    {
        $model = new OwnerItem();

        $method = 'getViewModel'.get_uppercase_by_underscores($this->request->get('model_type'));
        if (method_exists($this, $method)){
            return $this->{$method}();
        }

        return $this->setResponse(false);
    }

    /**
     * @return mixed
     */
    private function getViewModelCourses()
    {
        return BridgeHelper::getCourse()->getSearchCourseViewModel()->setRequest($this->request)->getCourseByApi();
    }

}
