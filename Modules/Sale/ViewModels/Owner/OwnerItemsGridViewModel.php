<?php

namespace Modules\Sale\ViewModels\Owner;

use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Sale\Models\Owner\Owner;
use Modules\Sale\Models\Owner\OwnerItem;

class OwnerItemsGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $order_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = OwnerItem::enable()->with('owner')->where('owner_id', $this->request->get('owner_id'));
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $order_assets = [];

        //use bested assets
        if ($this->order_assets) {

        }

        return $order_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('model_title', trans('sale::owner.items.model_title'), true);
        $this->addColumn('model_type', trans('sale::owner.items.model_type'), true);
        $this->addColumn('model_id', trans('sale::owner.items.model_id'), true);
        $this->addColumn('value', trans('sale::owner.value'), true);
        $this->addColumn('type', trans('sale::owner.type'), true);
        $this->addColumn('title', trans('sale::owner.owner_title'), true);
        $this->addColumn('state', trans('sale::owner.state'), true);
        $this->addColumn('updated_at', trans('sale::owner.updated_at'), true);
        $this->addColumn('created_at', trans('sale::owner.created_at'), true);


        $routeAdd = route('sale.owner.items.create', ['owner_id' => $this->request->get('owner_id')]);
        $options = ['class' => "btn btn-primary"];
        $this->addButton('add', $routeAdd, trans('sale::owner.create_owner'), 'fa fa-plus', false, 'before', $options);

        $routeAdd = route('sale.owner.index');
        $options = ['class' => "btn btn-success"];
        $this->addButton('list', $routeAdd, trans('sale::owner.owner_list'), 'fa fa-list', false, 'before', $options);


//        $show = array(
//            'name' => 'sale.owner.items.index',
//            'parameter' => ['id']
//        );
//        $this->addAction('create_item', $show, trans('sale::owner.items.list'), 'fa fa-list', false, ['target' => '', 'class' => 'btn btn-sm btn-primary']);

        $show = array(
            'name' => 'sale.owner.items.edit',
            'parameter' => ['id']
        );
        $this->addAction('edit_item', $show, trans('sale::owner.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('sale::owner.confirm_delete')];

        $delete = array(
            'name' => 'sale.owner.items.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('sale::owner.delete'), 'fa fa-trash', false, $options);

        $this->action_text = false;

        /*filter*/

        $this->addFilter('model_id', 'text', ['title' => trans('sale::owner.items.model_id')]);

        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $item_data = $row->item_data;
        $row->model_title = $item_data ? $item_data->title : '-';
        $row->type = trans('sale::owner.types.' . $row->type);
        $row->title = $row->owner->title;
        $row->state = $row->status_text;
        $row->created_at = DateHelper::setDateTime($row->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        $row->updated_at = DateHelper::setDateTime($row->updated_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
//        if ($row->state==$this->getOwnerCompleteState()) {
//            unset($action['accept_order'],$action['delete']);
//        }
//
//        if(key_exists('accept_order', $action) && !$row->has_success_payment){
//            unset($action['accept_order']);
//        }
        return $action;
    }

    /**
     * @return $this
     */
    protected function setAssetsEditOwner()
    {
        $this->order_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('sale::owner.items.list'));
        $this->generateGridList()->renderedView("sale::owner.index", ['view_model' => $this], "owner_items_list");
        return $this;
    }

}
