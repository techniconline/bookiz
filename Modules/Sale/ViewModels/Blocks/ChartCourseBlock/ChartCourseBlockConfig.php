<?php

namespace Modules\Sale\ViewModels\Blocks\ChartCourseBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;

class ChartCourseBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='ChartCourseBlock';

    public function __construct(){
        $this->setDefaultName(trans('sale::block.chart_course'))
        ->addTab('chart',trans('sale::block.chart_course'),'sale::widgets.chart_course.config.form');
    }

    /**
     * @return array
     */
    public function getBlockRules(){
        return [];
    }

    public function getChartTypeList(){
        $list = trans('core::chart.chart_types');
        return $list;
    }

    public function getChartModelList(){
        $list = trans('sale::block.chart_course_models');
        return $list;
    }

}
