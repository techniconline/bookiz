<?php

namespace Modules\Sale\ViewModels\Blocks\ChartCourseBlock;

use BridgeHelper;
use Illuminate\Support\Facades\DB;
use Modules\Core\Traits\Block\MasterBlock;
use Modules\Core\ViewModels\Chart\ChartViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Models\Course\CourseCategory;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Payment;
use Modules\Sale\Models\Traits\Payment\OrderStatesTrait;

class ChartCourseBlock
{
    use MasterBlock;
    use GridViewModel;
    use OrderStatesTrait;

    private $block_name;

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $this->setAssetsBlock();
        $this->block_name = $block_name = get_lowercase_separated_by_underscores(class_basename($this));
        $chart_models = $this->getConfig('chart_models', ['sale_top_amount_courses']);
        $models_string = implode("_", $chart_models);
        $models_string = sha1($models_string);
        $used = sha1(request()->route()->getName()) . "_" . $models_string;
        $cache_key = $block_name . '_' . get_instance()->getCurrentInstanceId() . '_' . $used;
        $this->setCacheConfigs($cache_key, $this->getConfigs());
        $this->setActionUrl(route("core.dashboard.render_chart_block", ["block_name" => $block_name, 'used' => $used]));
        $this->generateChart();
        $blockViewModel =& $this;
        return view('sale::widgets.chart_course.index', compact('blockViewModel'))->render();
    }

    /**
     * @return array|null
     * @throws \Throwable
     */
    public function renderJson()
    {
        $this->block_name = $block_name = get_lowercase_separated_by_underscores(class_basename($this));
        $cache_key = $block_name . '_' . get_instance()->getCurrentInstanceId() . '_' . request()->get('used') ;
        $configs = $this->getCacheConfig($cache_key);
        $this->setConfigs($configs);
        $this->generateChart();
        $this->boot();
        $data = $this->initChartGrid()->getChartRenderer();
//        $block = $this->setDataBlock($data)->getBlock();
        return $data;
    }

    /**
     *
     */
    private function setAssetsBlock()
    {
        $chartjs_assets = [];
        $chartjs_assets [] = ['container' => 'general_style', 'src' => ("css/loading.css"), 'name' => 'loading'];
        $chartjs_assets [] = ['container' => 'plugin_general_2', 'src' => ("/plugins/chartjs/Chart.bundle.min.js"), 'name' => 'Chartjs.bundle'];
        $chartjs_assets [] = ['container' => 'plugin_general_2', 'src' => ("/plugins/chartjs/Chart.min.js"), 'name' => 'Chartjs'];
        $this->setAssetsOnRoute($chartjs_assets);
    }

    /**
     * @return $this
     */
    private function generateChart()
    {
        $this->setTitleChart($this->getData('alias', trans('core::form.titles.dashboard')));
        $configs = $this->getConfigs();
        $strConfigs = json_encode($configs);
        $this->in_widget = true;

        $this->setNameChart($this->block_name . (rand(10000, 99999)));
        $this->setChartSize(["width" => 200, "height" => 100]);
        $this->setChartType("bar");
        $this->sort_by = 'count_sale';
        $this->use_chart = true;
        $this->hasIndex = false;

        $this->setModel();

        $this->addColumn('item_type', trans('sale::sale.item_type'), false)
            ->addColumn('item_id', trans('sale::sale.item_id'), false)
            ->addColumn('course_title', trans('sale::sale.course_title'), false)
            ->addColumn('count_sale', trans('sale::sale.count_sale'), false);


        $chart_models = $this->getConfig('chart_models', ['sale_top_amount_courses']);

        foreach ($chart_models as $chart_model) {
            $chart_model = get_uppercase_by_underscores($chart_model);
            $methodName = "setChartModel" . $chart_model;
            if (method_exists($this, $methodName)) {
                $this->{$methodName}();
            }
        }

        $this->with_paginate = false;

        $this->can_chart = true;
        $this->setXField("course_title");
        $this->setChartSize(["width" => 600, "height" => 400]);

//        $this->initChartGrid();

        return $this;
    }

    public function getRowsUpdate($row)
    {
        $row->total_amount = $row->getAttributes()['total_amount'];
        $row->total_discount_amount = $row->getAttributes()['total_discount_amount'];
//        $row->course_title = isset($row->item_row->title) ? $row->item_row->title : '-';
        $row->course_title = $row->name ? $row->name : '-';
        return $row;
    }

    private function setModel()
    {
        $this->Model = Order\OrderItems::whereNotNull(DB::raw('(select id from orders where id=order_id and state in ("' . $this->getOrderCompleteState() . '") and instance_id=' . get_instance()->getCurrentInstanceId() . ')'))
            ->select(['item_type', 'item_id', 'name', DB::raw('count(id) as count_sale')])
            ->with('courseCategories')
            ->groupBy(['item_type', 'item_id', 'name'])->take($this->getConfig('take_row', 15));

    }

    /**
     * @return array
     */
    private function getListCategoryByChildes()
    {
        $categories = CourseCategory::enable()->filterCurrentInstance()->orderBy('parent_id')->pluck('title', 'id');
        $list = [];
        $model = new CourseCategory();
        foreach ($categories as $cat_id => $title) {
            $childes = $model->getChildesId($cat_id);
            if ($childes->count() > 1) {
                $title = $title . ' + ';
            }
            $list[implode(",", $childes->toArray())] = $title;
        }
        return $list;
    }

    /**
     *
     */
    private function setChartModelSaleTopAmountCourses()
    {
        $this->addColumn('total_amount', trans('sale::sale.total_amount'), false)
            ->addColumn('total_discount_amount', trans('sale::sale.total_discount_amount'), false);

        $this->setYField('total_amount', trans('sale::sale.total_amount'));
        $this->setYField('total_discount_amount', trans('sale::sale.total_discount_amount'));
        $this->Model = $this->Model->addSelect([DB::raw('sum(total_amount) as total_amount'), DB::raw('sum(discount_amount) as total_discount_amount')]);
        $this->sort_by = 'total_amount';

        if ($this->getConfig("show_filter", 0)) {

//        $this->addFilter('item_type', 'text',['title' => trans('course::course.item_type')]);
//        $this->addFilter('item_id', 'text',['title' => trans('course::course.item_id')]);
            $config = ['title' => trans('sale::sale.order.created_at')];
            $this->addFilter('created_at', 'date', $config);

            $categories = $this->getListCategoryByChildes();
            $this->addFilter('category_id', 'select', ['relation' => 'courseCategories', 'options' => $categories, 'title' => trans('course::course.category')]);



        }

    }

    /**
     *
     */
    private function setChartModelSaleTopQtyCourses()
    {
        $this->addColumn('total_qty', trans('sale::sale.total_qty'), false);
        $this->setYField("total_qty", trans('sale::sale.total_qty'));
        $this->Model = $this->Model->addSelect([DB::raw('sum(qty) as total_qty')]);
        $this->sort_by = 'total_qty';

        if ($this->getConfig("show_filter", 0)) {

//        $this->addFilter('item_type', 'text',['title' => trans('course::course.item_type')]);
//        $this->addFilter('item_id', 'text',['title' => trans('course::course.item_id')]);

            $categories = $this->getListCategoryByChildes();
            $this->addFilter('category_id', 'select', ['relation' => 'courseCategories', 'options' => $categories, 'title' => trans('course::course.category')]);

            $config = ['title' => trans('sale::sale.order.created_at')];
            $this->addFilter('created_at', 'date', $config);


        }

    }


}
