<?php

namespace Modules\Sale\ViewModels\Blocks\ChartBlock;

use BridgeHelper;
use Illuminate\Support\Facades\DB;
use Modules\Core\Traits\Block\MasterBlock;
use Modules\Core\ViewModels\Chart\ChartViewModel;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Payment;

class ChartBlock
{
    use MasterBlock;
    use ChartViewModel;
    private $block_name;

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $this->setAssetsBlock();
        $this->block_name = $block_name = get_lowercase_separated_by_underscores(class_basename($this));

        $chart_models = $this->getConfig('chart_models', ['orders']);
        $models_string = implode("_", $chart_models);
        $models_string = sha1($models_string);
        $used = sha1(request()->route()->getName()) . "_" . $models_string;
        $cache_key = $block_name . '_' . get_instance()->getCurrentInstanceId() . '_' . $used;

        $this->setCacheConfigs($cache_key, $this->getConfigs());
        $this->setActionUrl(route("core.dashboard.render_chart_block", ["block_name" => $block_name, 'used'=>$used]));
        $this->generateChart();
        $blockViewModel =& $this;
        return view('sale::widgets.chart.index', compact('blockViewModel'))->render();
    }


    /**
     * @return array|null
     * @throws \Throwable
     */
    public function renderJson()
    {
        $this->block_name = $block_name = get_lowercase_separated_by_underscores(class_basename($this));
        $cache_key = $block_name.'_'.get_instance()->getCurrentInstanceId().'_'.request()->get('used');
        $configs = $this->getCacheConfig($cache_key);
        $this->setConfigs($configs);
        $this->generateChart();
        $data = $this->getChart();
//        $block = $this->setDataBlock($data)->getBlock();
        return $data;
    }

    /**
     *
     */
    private function setAssetsBlock()
    {
        $chartjs_assets = [];
        $chartjs_assets [] = ['container' => 'general_style', 'src' => ("css/loading.css"), 'name' => 'loading'];
        $chartjs_assets [] = ['container' => 'plugin_general_2', 'src' => ("/plugins/chartjs/Chart.bundle.min.js"), 'name' => 'Chartjs.bundle'];
        $chartjs_assets [] = ['container' => 'plugin_general_2', 'src' => ("/plugins/chartjs/Chart.min.js"), 'name' => 'Chartjs'];
        $this->setAssetsOnRoute($chartjs_assets);
    }

    /**
     * @return $this
     */
    private function generateChart()
    {
        $this->setTitleChart($this->getData('alias', trans('core::form.titles.dashboard')));
        $configs = $this->getConfigs();
        $strConfigs = json_encode($configs);

        $this->setNameChart($this->block_name . (rand(10000, 99999)));
        $this->setChartSize(["width" => 200, "height" => 100]);
        $this->setChartType("bar");
        if ($this->getConfig("show_filter", 0)) {
            $this->filter_models = true;
            $this->setFilter("created_at", "date", ["title_to" => trans("core::chart.title_to"), "title_from" => trans("core::chart.title_from")]);
            $chart_types = $this->getConfig('chart_types', ["bar"]);

            $chart_types = collect(trans('core::chart.chart_types'))->filter(function ($item, $key) use ($chart_types) {

                if (in_array($key, $chart_types)) {
                    return [$key => $item];
                }

            })->toArray();

            $this->setFilterTypeChart($chart_types);
        }

        $chart_models = $this->getConfig('chart_models', ['orders']);
        foreach ($chart_models as $chart_model) {
            $chart_model = get_uppercase_by_underscores($chart_model);
            $methodName = "setChartModel" . $chart_model;
            if (method_exists($this, $methodName)) {
                $this->{$methodName}();
            }
        }

        $this->setGroupByItems("full_date", ["type" => "date", "format" => "Y-m-d", "value" => 'DATE_FORMAT(created_at, "%Y-%m-%d")', "field_name" => "created_time", "title" => trans("core::chart.full_date")]);
        $this->setGroupByItems("by_month", ["type" => "date", "format" => "Y-m", "value" => 'DATE_FORMAT(created_at, "%Y-%m")', "field_name" => "created_time", "title" => trans("core::chart.by_month")]);
        $this->setGroupByItems("by_hours", ["type" => "date", "format" => "Y-m-d H", "value" => 'DATE_FORMAT(created_at, "%Y-%m-%d %H")', "field_name" => "created_time", "title" => trans("core::chart.by_hours")]);

        $this->initChart();

        return $this;
    }

    /**
     *
     */
    private function setChartModelOrders()
    {
        $model = Order::active()->filterCurrentInstance()->filterCompleted()->where(function ($q){
            if (!$this->hasFilterColumn("created_at", "date")){
                $q->where("created_at",">=",date("Y-m-d",strtotime(date("Y-m-d")." - ".$this->getConfig('last_days', 14)." days")));
            }
        });
        $this->setChartModel("orderspay", trans('sale::block.charts.title_order_pay'), $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "sum_payable_amount", "selected" => "sum(if(currency_id=1,payable_amount,payable_amount*10)) as sum_payable_amount"]], ["full_date"]);

        $model = Order::active()->filterCurrentInstance()->filterCompleted()->where(function ($q){
            if (!$this->hasFilterColumn("created_at", "date")){
                $q->where("created_at",">=",date("Y-m-d",strtotime(date("Y-m-d")." - ".$this->getConfig('last_days', 14)." days")));
            }
        });
        $this->setChartModel("orders", trans('sale::block.charts.title_order'), $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);

        $model = Order::active()->filterCurrentInstance()->filterNotCompleted()->where(function ($q){
            if (!$this->hasFilterColumn("created_at", "date")){
                $q->where("created_at",">=",date("Y-m-d",strtotime(date("Y-m-d")." - ".$this->getConfig('last_days', 14)." days")));
            }
        });
        $this->setChartModel("not_complete_orders", trans('sale::block.charts.title_order_not_complete'), $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);
        //        $this->setChartModel("orders", "order list", $model, ["x" => ["field_name" => "created_time", "selected" => "DATE_FORMAT(created_at, '%Y-%m-%d') as created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);
    }

    /**
     *
     */
    private function setChartModelPayments()
    {
        $model = Payment::active()->filterCurrentInstance()->filterCompleted()->where(function ($q){
            if (!$this->hasFilterColumn("created_at", "date")){
                $q->where("created_at",">=",date("Y-m-d",strtotime(date("Y-m-d")." - ".$this->getConfig('last_days', 14)." days")));
            }
        });
        $this->setChartModel("payments", trans('sale::block.charts.title_payment'), $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);
//        $this->setChartModel("payments", "payment list", $model, ["x" => ["field_name" => "created_time", "selected" => "DATE_FORMAT(created_at, '%Y-%m-%d') as created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);

        $model = Payment::active()->filterCompleted()->filterCurrentInstance()->where(function ($q){
            if (!$this->hasFilterColumn("created_at", "date")){
                $q->where("created_at",">=",date("Y-m-d",strtotime(date("Y-m-d")." - ".$this->getConfig('last_days', 14)." days")));
            }
        });
        $this->setChartModel("payments_success", trans('sale::block.charts.title_payment_success'), $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);

        $model = Payment::active()->filterCompleted()->where("bank", "zarinpal")->filterCurrentInstance()->where(function ($q){
            if (!$this->hasFilterColumn("created_at", "date")){
                $q->where("created_at",">=",date("Y-m-d",strtotime(date("Y-m-d")." - ".$this->getConfig('last_days', 14)." days")));
            }
        });
        $this->setChartModel("payments_success_zarin", trans('sale::block.charts.title_payment_success_zarin'), $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);

        $model = Payment::active()->filterCompleted()->where("bank", "asan")->filterCurrentInstance()->where(function ($q){
            if (!$this->hasFilterColumn("created_at", "date")){
                $q->where("created_at",">=",date("Y-m-d",strtotime(date("Y-m-d")." - ".$this->getConfig('last_days', 14)." days")));
            }
        });
        $this->setChartModel("payments_success_asan", trans('sale::block.charts.title_payment_success_asan'), $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);

        $model = Payment::active()->whereNotIn("state", ["success", "complete"])->filterCurrentInstance()->where(function ($q){
            if (!$this->hasFilterColumn("created_at", "date")){
                $q->where("created_at",">=",date("Y-m-d",strtotime(date("Y-m-d")." - ".$this->getConfig('last_days', 14)." days")));
            }
        });
        $this->setChartModel("payments_fail", trans('sale::block.charts.title_payment_fail'), $model, ["x" => ["field_name" => "created_time"], "y" => ["field_name" => "count", "selected" => "count(id) as count"]], ["full_date"]);

    }


}
