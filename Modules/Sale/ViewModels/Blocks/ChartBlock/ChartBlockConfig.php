<?php

namespace Modules\Sale\ViewModels\Blocks\ChartBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;

class ChartBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='ChartBlock';

    public function __construct(){
        $this->setDefaultName(trans('sale::block.chart'))
        ->addTab('chart',trans('sale::block.chart'),'sale::widgets.chart.config.form');
    }

    /**
     * @return array
     */
    public function getBlockRules(){
        return [];
    }

    public function getChartTypeList(){
        $list = trans('core::chart.chart_types');
        return $list;
    }

    public function getChartModelList(){
        $list = trans('sale::block.chart_models');
        return $list;
    }

}
