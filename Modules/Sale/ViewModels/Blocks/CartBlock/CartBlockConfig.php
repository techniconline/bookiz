<?php

namespace Modules\Sale\ViewModels\Blocks\CartBlock;


use Modules\Core\Traits\Block\MasterBlockConfig;

class CartBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='CartBlock';

    public function __construct(){
        $this->setDefaultName(trans('sale::block.cart_block'));
        $this->addTab('cart_block',trans('sale::block.cart_block'),'sale::widgets.cart.config.form');
    }


}