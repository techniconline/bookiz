<?php

namespace Modules\Sale\ViewModels\Blocks\CartBlock;

use BridgeHelper;
use Modules\Core\Traits\Block\MasterBlock;


class CartBlock
{

    use MasterBlock;
    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel=&$this;

        return view('sale::widgets.cart.index',compact('blockViewModel'))->render();
    }

    public function getCartInfo(){
        return BridgeHelper::getCartHelper()->getCartInfo();
    }

}
