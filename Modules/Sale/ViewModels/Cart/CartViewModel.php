<?php

namespace Modules\Sale\ViewModels\Cart;

use BridgeHelper;
use Modules\Core\Traits\ModelTrait;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Sale\Models\Affiliate;
use Modules\Sale\Models\Traits\Payment\PaymentTrait;
use Modules\Sale\Models\Traits\Sale\ItemTrait;
use Modules\Sale\Models\Voucher\Voucher;
use Modules\Sale\Models\Voucher\VoucherCodes;
use Modules\User\Http\Middleware\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class CartViewModel extends BaseViewModel
{

    use ItemTrait;
    use PaymentTrait;
    use ModelTrait;
    private $access_assets;

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {
            $access_assets[] = ['container' => 'up_theme_js', 'src' => ("assets/js/main/jsCalendar.js"), 'name' => 'calendar-js'];
            $access_assets[] = ['container' => 'theme_js', 'src' => ("assets/js/checkout.js"), 'name' => 'checkout-js'];
        }

        return $access_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getCart()
    {
        $blockViewModel =& $this;
        if (app('getInstanceObject')->isApi()) {
            return $this->getCartData();
        }
        $this->layout = '1Column';
        $this->setTitlePage(trans('sale::form.titles.cart'));
        return $this->renderedView('sale::cart.index', compact('blockViewModel'));
    }

    /**
     * @return CartViewModel
     * @throws \Throwable
     */
    protected function getCartBooking()
    {
        $this->access_assets = true;
        $blockViewModel =& $this;
        if (app('getInstanceObject')->isApi()) {
            $dataCart = $this->getCartData(true);
            $cart = $this->decorateBasketBooking($dataCart);
            if ($cart) {
                $cart['gateways'] = null;
                if ($dataCart->payable_amount) {
                    $cart['gateways'] = $this->getMasterPayment()->getViewForm('payment', true);
                }
            }
            return $this->setDataResponse($cart)->setResponse($cart ? true : false, $cart ? null : trans('sale::message.cart_empty'));
        }
        $this->layout = '1Column';
        $this->setTitlePage(trans('sale::form.titles.cart'));
        return $this->renderedView('sale::cart.index_booking', compact('blockViewModel'));
    }

    /**
     * @param bool $json
     * @return $this
     */
    public function getCartData($json = false)
    {
        $owner_user_id = BridgeHelper::getAccess()->getUser()->id;
        if (!$this->loginClientCart()){
            return $json? false :$this->setDataResponse(null)->setResponse(false, trans('sale::message.cart_empty'));
        }
        $currentCart = $this->getCurrentCart();
        $cart = $currentCart->getCart();
        BridgeHelper::getAccess()->loginById($owner_user_id);
        if ($json) {
            return $cart;
        }
        return $this->setDataResponse($cart)->setResponse($cart ? true : false, $cart ? null : trans('sale::message.cart_empty'));
    }

    /**
     * @param $cart
     * @return mixed
     */
    private function decorateBasketBooking($cart)
    {

        if (isset($cart->cartItems) && $cart->cartItems) {

            $discountOnProducts = 0.00;
            foreach ($cart->cartItems as $cartItem) {

                $item = $this->getSaleItem($cartItem->item_type, $cartItem->item_id, $cartItem->options_array);
                $itemDiscountPrice = BridgeHelper::getCurrencyHelper()->getConvertPrice($cartItem->discount_amount, $cartItem->currency_id, $cart->currency_id);
                $discountOnProducts = $discountOnProducts + $itemDiscountPrice;
                $itemExtraData = null;
                if ($item) {
                    $itemExtraData = $item->getItemExtraData();
                }

                $cartItem->item = $item;
                $cartItem->item_discount = $itemDiscountPrice;
                $cartItem->extera_data = $itemExtraData;

            }

//            $cart->total_discount = $discountOnProducts;

        }
        return $cart;
    }

    /**
     * @return CartViewModel
     * @throws \Throwable
     */
    protected function getInvoice()
    {
        $blockViewModel =& $this;
        if (app('getInstanceObject')->isApi()) {
            return $this->getInvoiceData();
        }
        $this->layout = '1Column';
        $this->setTitlePage(trans('sale::cart.invoice'));
        return $this->renderedView('sale::cart.invoice', compact('blockViewModel'));
    }

    /**
     * @return CartViewModel
     * @throws \Throwable
     */
    protected function getInvoiceBooking()
    {
        $blockViewModel =& $this;
        if (app('getInstanceObject')->isApi()) {
            return $this->getInvoiceData();
        }
        $this->layout = '1Column';
        $this->setTitlePage(trans('sale::cart.invoice'));
        return $this->renderedView('sale::cart.invoice_booking', compact('blockViewModel'));
    }

    /**
     * @param $items
     * @return mixed
     */
    private function renderCartItems($items)
    {
        foreach ($items as &$item) {
            if ($this->getSaleItem($item->item_type, $item->item_id)) {
                $item->cost = BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($item->amount, $item->currency_id, true);
                $item->total = BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($item->total_amount, $item->currency_id, true);
            }
        }

        return $items;
    }

    /**
     * @return $this
     */
    public function getInvoiceData()
    {
        $currentCart = $this->getCurrentCart();
        $cart = $currentCart->getCart();
        if (isset($cart->cartItems) && $cart->cartItems) {
//            $cart->cart_items = $this->renderCartItems($cart->cartItems);
            if ($cart->payable_amount) {
                $cart->payments = $this->getMasterPayment()->getViewForm('payment', true);
            } else {
                $cart->payments = null;
            }
        }
        return $this->setDataResponse($cart)->setResponse($cart ? true : false, $cart ? null : trans('sale::message.cart_empty'));
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function addToCart()
    {
        $owner_user_id = BridgeHelper::getAccess()->getUser()->id;
        if (!$this->loginClient()) {
            return $this->redirectBack()->setResponse(false, trans('sale::message.item_error'));
        }
        $cart = $this->getCurrentCart(true);
        $item = $this->request->get('item');
        $item_id = $this->request->get('item_id');
        $qty = $this->request->has('qty') ? $this->request->get('qty') : 1;
        if (!$item_id || !$item) {
            BridgeHelper::getAccess()->loginById($owner_user_id);
            return $this->redirectBack()->setResponse(false, trans('sale::message.item_error'));
        }
        $options = $this->request->has('options') ? $this->request->get('options') : [];

        $item = $this->getSaleItem($item, $item_id, $options);
        if (!$item) {
            BridgeHelper::getAccess()->loginById($owner_user_id);
            return $this->redirectBack()->setResponse(false, trans('sale::message.item_error'));
        }
        if (!$item->canSaleItem()) {
            BridgeHelper::getAccess()->loginById($owner_user_id);
            return $this->redirectBack()->setResponse(false, trans('sale::message.item_error'));
        }
        $result = $cart->addItemToCart($item);
        $data = [];
        if ($result) {
            if (!app('getInstanceObject')->isApi()) {
                $data["buttons"] = view('sale::cart.buttons')->render();
            }
            $data["count"] = $cart->getItemsCount() + $qty;
            BridgeHelper::getAccess()->loginById($owner_user_id);
            return $this->redirectBack()->setDataResponse($data)->setResponse(true, trans('sale::message.item_add_success'));
        } else {
            BridgeHelper::getAccess()->loginById($owner_user_id);
            return $this->redirectBack()->setDataResponse($cart->getCartItemRemoved(), 'remove_cart_item')->setResponse(false, $cart->getError());
        }
    }

    /**
     * @return bool
     */
    private function loginClient()
    {
        if ($this->request->get('entity_client_id') && $this->request->get('entity_id')) {
            $owner_user_id = BridgeHelper::getAccess()->getUser()->id;
            $entity_id = $this->request->get('entity_id');
            $entity_client_id = $this->request->get('entity_client_id');
            $item = $this->request->get('item');
            $item_id = $this->request->get('item_id');
            $model = $this->getModelObject($item);
            $item_row = $model->active()->isOwner($owner_user_id, $entity_id)->find($item_id);
            if (!$item_row) {
                $this->request->offsetUnset('item_id');
                $this->request->offsetUnset('item');
            } else {
                $client = BridgeHelper::getEntity()->getEntityClientModel();
                $client = $client->active()->find($entity_client_id);
                if ($client) {
                    BridgeHelper::getAccess()->loginById($client->user_id);
                    return true;
                }
                return false;
            }
            return false;
        }
        return true;
    }

    /**
     * @return $this
     */
    public function addCodeToCart()
    {
        $code = $this->request->get('code');
        if (!$code) {
            return $this->redirectBack()->setResponse(false, trans('sale::message.miss_code'));
        }

        $error = null;
        $cart = $this->getCurrentCart();
        if ($cart->cart && !$cart->cart->coupen && !$cart->cart->affiliate_id && !$cart->cart->promotion_id) {
            $affiliate = Affiliate::active()->filterCurrentInstance()->where('code', $code)->first();
            if (!$affiliate) {
                $voucher_code = VoucherCodes::active()->with('voucher')
                    ->whereHas('voucher', function ($q) {
                        return $q = $q->active()->filterCurrentInstance();
                    })
                    ->where('code', $code)->first();
                if ($voucher_code) {
                    $cart->voucher_code = $voucher_code;
                } else {
                    return $this->redirectBack()->setResponse(false, trans('sale::message.code_is_not_valid'));
                }
            } else {
                $cart->affiliate = $affiliate;
            }

            $cart = $cart->addCodeToCart();
        } else {

            $error = trans('sale::message.you_has_another_code');

        }


        return $this->redirectBack()->setResponse($error ? false : $cart->is_valid, $error ?: $cart->getError());
    }

    /**
     * @return $this
     */
    public function removeCodeFromCart()
    {
        $type = $this->request->get('type');
        $cart_id = $this->request->get('cart_id');

        if (!$cart_id || !$type) {
            return $this->redirectBack()->setResponse(false, trans('sale::message.miss_code'));
        }

        $cart = $this->getCurrentCart();

        $cart = $cart->removeCodeFromCart();
        return $this->redirectBack()->setResponse(true, trans('sale::message.remove_success_code'));

    }

    /**
     * @return $this
     */
    public function removeVoucherCodeFromCart()
    {
        $type = $this->request->get('type');
        $cart_id = $this->request->get('cart_id');

        if (!$cart_id || !$type) {
            return $this->redirectBack()->setResponse(false, trans('sale::message.miss_code'));
        }

        $cart = $this->getCurrentCart();

        $cart = $cart->removeVoucherCodeFromCart();
        return $this->redirectBack()->setResponse(true, trans('sale::message.remove_success_code'));

    }

    /**
     * @return $this
     */
    public function deleteItem()
    {
        $cart_item_id = $this->request->get('id');
        $cart = $this->getCurrentCart(false);
        $result = $cart->delete($cart_item_id);
        if ($result) {
            return $this->redirectBack()->setResponse(true, trans('sale::message.item_delete_success'));
        } else {
            return $this->redirectBack()->setResponse(false, trans('sale::message.item_delete_fail'));
        }
    }

    /**
     * @return CartViewModel
     */
    public function deleteCart()
    {
        $owner_user_id = BridgeHelper::getAccess()->getUser()->id;
        if (!$this->loginClientCart()){
            return $this->setDataResponse(null)->setResponse(false, trans('sale::message.cart_empty'));
        }
        $cart = $this->getCurrentCart(false);
        $result = $cart->deleteCart();
        BridgeHelper::getAccess()->loginById($owner_user_id);
        if ($result) {
            return $this->redirectBack()->setResponse(true, trans('sale::message.item_delete_success'));
        } else {
            return $this->redirectBack()->setResponse(false, trans('sale::message.item_delete_fail'));
        }
    }

    /**
     * @return bool
     */
    private function loginClientCart()
    {
        if ($this->request->get('entity_client_id') && $this->request->get('entity_id')) {
            $entity_id = $this->request->get('entity_id');
            $entity_client_id = $this->request->get('entity_client_id');

            $entity = BridgeHelper::getEntity()->getEntityModel();
            $entity = $entity->isOwner()->active()->find($entity_id);

            if (!$entity) {
                return false;
            } else {
                $client = BridgeHelper::getEntity()->getEntityClientModel();
                $client = $client->active()->find($entity_client_id);
                if ($client) {
                    BridgeHelper::getAccess()->loginById($client->user_id);
                    return true;
                }
                return false;
            }
        }
        return true;
    }

    public function getCurrentCart($create = false)
    {
        return BridgeHelper::getCartHelper()->getCurrentCart($create);
    }
}
