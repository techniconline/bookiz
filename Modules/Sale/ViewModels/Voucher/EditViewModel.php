<?php

namespace Modules\Sale\ViewModels\Voucher;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\Sale\Models\Voucher\Voucher;

class EditViewModel extends BaseViewModel
{
    public function boot()
    {
        $id=$this->requestValuesUpdate()->get('id');
        if($id){
            $this->modelData=Voucher::find($id);
        }
    }

    public function getForm(){
        $this->boot();
        $BlockViewModel=&$this;
        $this->setTitlePage(trans('sale::form.titles.create_voucher'));
        return $this->renderedView('sale::voucher.edit',compact('BlockViewModel'));
    }


    public function save(){
        $this->boot();
        $rules=[
            'title'=>['required','string','min:2','max:150'],
            'type'=>['required','in:fixed,percent'],
            'value'=>['required','numeric'],
            'max_value'=>['required','numeric'],
            'currency_id'=>['required','integer'],
            'max_qty'=>['required','integer'],
            'use_type'=>['required','string'],
            'duration'=>['required','integer'],
            'active'=>['required','in:0,1,2'],
        ];
        if($this->request->get('type')=='percent'){
            $rules['value']=['required','between:0,100'];
        }
        $this->request->validate($rules);
        $data=$this->request->all();
        if(!$this->modelData){
            $instance=app('getInstanceObject')->getCurrentInstance();
            $this->modelData=new Voucher;
            $this->modelData->instance_id=$instance->id;
        }
        $this->modelData->title=$data['title'];
        $this->modelData->type=$data['type'];
        $this->modelData->value=$data['value'];
        $this->modelData->max_value=$data['max_value'];
        $this->modelData->currency_id=$data['currency_id'];
        $this->modelData->max_qty=$data['max_qty'];
        $this->modelData->use_type=$data['use_type'];
        $this->modelData->duration=$data['duration'];
        $this->modelData->active=$data['active'];
        $result=$this->modelData->save();
        if($result){
            return $this->redirect(route('sale.voucher.index'))->setResponse(true,trans('sale::message.action.success'));
        }
        return $this->redirectBack()->setResponse($result,trans('sale::message..action.error'));
    }



}
