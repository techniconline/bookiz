<?php

namespace Modules\Sale\ViewModels\Voucher;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Sale\Models\Voucher\Voucher;

class VouchersManageViewModel extends BaseViewModel
{

    use GridViewModel;

    public function setGridModel()
    {
        $this->Model = Voucher::enable()->filterCurrentInstance();
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('sale::form.fields.id'), true)
            ->addColumn('title', trans('sale::form.fields.title'), true)
            ->addColumn('type', trans('sale::form.fields.type'), true)
            ->addColumn('value', trans('sale::form.fields.value'), true)
            ->addColumn('max_value', trans('sale::form.fields.max_value'), true)
            ->addColumn('max_qty', trans('sale::form.fields.max_qty'), true)
            ->addColumn('duration', trans('sale::form.fields.duration_day'), true)
            ->addColumn('active', trans('sale::form.fields.status'), true);


        $edit = array(
            'name' => 'sale.voucher.edit',
            'parameter' => ['id']
        );
        $this->addAction('edit', $edit, trans('sale::form.titles.edit_voucher'), 'fa fa-pencil', false, ['target' => '', 'class' => 'btn btn-sm btn-primary']);


        $codes = array(
            'name' => 'sale.voucher.code.index',
            'parameter' => ['id']
        );
        $this->addAction('codes', $codes, trans('sale::form.titles.voucher_codes'), 'fa fa-credit-card', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);


        $this->action_text = false;

        $routeAdd=route('sale.voucher.create');
        $options=['class'=>"btn btn-primary"];
        $this->addButton('add',$routeAdd,trans('sale::form.titles.create_voucher'),'fa fa-plus',false,'before',$options);

        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        if($row->type=='percent'){
            $row->value=$row->value.' '.trans('sale::data.voucher_type.percent');
        }else{
            $row->value=BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($row->value,$row->currency_id,true);
        }
        $row->max_value=BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($row->max_value,$row->currency_id,true);
        $row->type=trans('sale::data.voucher_type.'.$row->type);
        $row->active=trans('sale::data.active.'.$row->active);
        $row->duration=$row->duration.' '.trans('sale::data.duration.day');
        return $row;
    }


    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('sale::menu.voucher_manage'));
        $this->generateGridList()->renderedView("sale::voucher.index", ['view_model' => $this], trans('sale::menu.voucher_manage'));
        return $this;
    }


}
