<?php

namespace Modules\Sale\ViewModels\Voucher\Codes;

use Illuminate\Support\Facades\Validator;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Sale\Models\Voucher\Voucher;
use Modules\Sale\Models\Voucher\VoucherCodes;

class EditViewModel extends BaseViewModel
{
    public function boot()
    {
        $id = $this->requestValuesUpdate()->get('voucher_id');
        if ($id) {
            $this->modelData = Voucher::find($id);
        }
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getForm()
    {
        $this->boot();
        $BlockViewModel =& $this;
        $this->setTitlePage(trans('sale::form.titles.create_voucher_code'));
        return $this->renderedView('sale::voucher.code.form_code', compact('BlockViewModel'));
    }


    /**
     * @return $this
     */
    public function save()
    {
        $this->boot();
        $rules = [
            'voucher_id' => ['required', 'integer'],
            'code' => ['required', 'string', 'unique:voucher_codes,code'],
        ];


        $count_create_voucher = $this->request->get('count_create_voucher');
        $action = false;
        $voucher_id = $this->request->get('voucher_id');
        $pre_code = $this->request->get('pre_code');
        $code = $this->request->get('code');
        $loop = $count_create_voucher > 1 ? true : false;
        $duration = isset($this->modelData->duration) ? $this->modelData->duration : 1;
        $active_date = date('Y-m-d', strtotime(now() . ' + ' . $duration . ' days')).' 23:59:59';
        for ($i = 0; $count_create_voucher > $i; $i++) {
            $data = [
                'voucher_id' => $voucher_id,
                'pre_code' => $pre_code,
                'code' => $code,
                'active_time' => $active_date,
                'active' => 1,
            ];
            $res[] = $result = $this->serviceCode($data, $rules, $loop);
            if ($result['action'] && !$action) {
                $action = true;
            }
        }

        if ($action) {
            return $this->redirect(route('sale.voucher.code.index', ['voucher_id' => $voucher_id]))->setResponse(true, trans('sale::message.alert.save_success'));
        }
        return $this->redirectBack()->setResponse($result, trans('sale::message.alert.save_un_success'));
    }

    /**
     * @param $data
     * @param $rules
     * @param bool $loop
     * @return array
     */
    public function serviceCode($data, $rules, $loop = false)
    {
        $this->request = $this->requestValuesUpdate();
        $this->useModel = new VoucherCodes();
        $pre_code = isset($data['pre_code']) ? $data['pre_code'] : null;
        $code = isset($data['code']) ? $data['code'] : (str_random(4));
        if ($loop) {
            $code .= (str_random(4));
        }
        $data['code'] = $pre_code ? $pre_code : '';
        $data['code'] .= $code;
        $data['code'] = strtoupper($data['code']);


        $this->isValidRequest($data, $rules, null, 'POST');
        if ($this->isValidRequest) {
            $this->useModel->fill($data);
            $result = $this->useModel->save();
            return ['action' => boolval($result), 'message' => boolval($result) ? trans('sale::message..action.save_success') : trans('sale::message.alert.save_un_success'), 'data' => boolval($result) ? $this->useModel : []];
        } else {
            return ['action' => $this->isValidRequest, 'message' => trans('sale::message.alert.mis_date'), 'errors' => $this->errors];
        }
    }

    /**
     * @return $this
     */
    public function destroy()
    {
        $voucher_code_id = $this->request->get("voucher_code_id");
        $model = new VoucherCodes();
        $voucher_code = $model->enable()->find($voucher_code_id);

        if (!$voucher_code){
            return $this->redirectBack()->setResponse(false, trans('sale::message.alert.not_find_data'));
        }

        if ($voucher_code->delete()){
            return $this->redirectBack()->setResponse(true, trans('sale::message.alert.del_success'));
        }

        return $this->redirectBack()->setResponse(false, trans('sale::message.alert.del_un_success'));
    }


}
