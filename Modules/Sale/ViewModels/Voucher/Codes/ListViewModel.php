<?php

namespace Modules\Sale\ViewModels\Voucher\Codes;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Sale\Models\Voucher\VoucherCodes;
use Modules\Sale\Models\Voucher\Voucher;

class ListViewModel extends BaseViewModel
{

    use GridViewModel;
    protected $voucher;
    protected $voucher_id;

    public function setGridModel()
    {
        $this->Model = VoucherCodes::active()->where('voucher_id', $this->voucher_id)->with('voucher');
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('sale::form.fields.id'), true)
            ->addColumn('voucher_id', trans('sale::form.fields.voucher_id'), true)
            ->addColumn('voucher', trans('sale::form.fields.voucher_id'), false)
            ->addColumn('code', trans('sale::form.fields.voucher_code'), true)
            ->addColumn('used_qty', trans('sale::form.fields.used_qty'), true)
            ->addColumn('active_time', trans('sale::form.fields.active_time'), true)
            ->addColumn('created_at', trans('sale::form.fields.created_at'), true);

        $routeAdd = route('sale.voucher.code.create',['voucher_id'=>$this->voucher_id]);
        $options = ['class' => "btn btn-primary"];
        $this->addButton('add', $routeAdd, trans('sale::form.titles.create_voucher_code'), 'fa fa-plus', false, 'before', $options);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('sale::sale.confirm')];
        $delete = array(
            'name' => 'sale.voucher.code.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('sale::sale.delete'), 'fa fa-trash', false, $options);


        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->voucher = $row->voucher->title;
        return $row;
    }


    protected function getViewListIndex()
    {
        $this->voucher_id = $this->request->get('voucher_id');
//        $this->voucher = Voucher::find($voucher_id);
        if (!$this->voucher_id) {
            abort('404');
        }
        $this->setTitlePage(trans('sale::menu.voucher_manage'));
        $this->generateGridList()->renderedView("sale::voucher.index", ['view_model' => $this], trans('sale::menu.voucher_manage'));
        return $this;
    }


}
