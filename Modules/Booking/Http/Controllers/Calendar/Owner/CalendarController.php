<?php

namespace Modules\Booking\Http\Controllers\Calendar\Owner;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class CalendarController extends Controller
{

    /**
     * @param $entity_id
     * @param $entity_relation_service_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function list($entity_id, $entity_relation_service_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_id'=>$entity_id, 'entity_relation_service_id'=>$entity_relation_service_id],$request)
            ->setViewModel('calendar.owner.bookingCalendar')
            ->setActionMethod("listCalendar")->response();
    }

    /**
     * @param $entity_id
     * @param $entity_relation_service_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($entity_id, $entity_relation_service_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_id'=>$entity_id, 'entity_relation_service_id'=>$entity_relation_service_id],$request)
            ->setViewModel('calendar.owner.bookingCalendar')
            ->setActionMethod("saveCalendar")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['booking_calendar_id' => $id], $request)
            ->setViewModel('calendar.owner.bookingCalendar')
            ->setActionMethod("showCalendar")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getCalendar($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['booking_calendar_id' => $id], $request)
            ->setViewModel('calendar.owner.bookingCalendar')
            ->setActionMethod("getCalendar")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['booking_calendar_id' => $id], $request)
            ->setViewModel('calendar.owner.bookingCalendar')
            ->setActionMethod("saveCalendar")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['booking_calendar_id' => $id], $request)
            ->setViewModel('calendar.owner.bookingCalendar')
            ->setActionMethod("destroyCalendar")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function blockTimes(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('calendar.owner.bookingCalendar')->setActionMethod("blockTimes")->response();
    }


}
