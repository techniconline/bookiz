<?php

namespace Modules\Booking\Http\Controllers\Booking;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class BookingController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function list(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)
            ->setViewModel('booking.booking')
            ->setActionMethod("listBooking")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getUserBasketBooking(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)
            ->setViewModel('booking.booking')
            ->setActionMethod("getUserBasketBooking")->response();
    }

    /**
     * @param $entity_id
     * @param $entity_relation_service_id
     * @param $entity_service_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($entity_id, $entity_relation_service_id, $entity_service_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_id' => $entity_id,'entity_relation_service_id' => $entity_relation_service_id,'entity_service_id' => $entity_service_id], $request)
            ->setViewModel('booking.booking')
            ->setActionMethod("saveBooking")->response();
    }

    /**
     * @param $entity_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeMultiDetails($entity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['entity_id' => $entity_id], $request)
            ->setViewModel('booking.booking')
            ->setActionMethod("saveBooking")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['booking_id' => $id], $request)
            ->setViewModel('booking.booking')
            ->setActionMethod("showBooking")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['booking_id' => $id], $request)
            ->setViewModel('booking.booking')
            ->setActionMethod("saveBooking")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['booking_id' => $id], $request)
            ->setViewModel('booking.booking')->setActionMethod("destroyBooking")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyBookingDetails(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)
            ->setViewModel('booking.booking')->setActionMethod("destroyBookingDetails")->response();
    }


}
