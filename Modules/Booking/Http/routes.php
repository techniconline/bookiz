<?php

Route::group(['middleware' => 'web', 'prefix' => 'booking', 'as' => 'booking.', 'namespace' => 'Modules\Booking\Http\Controllers'], function()
{

    Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

        Route::group(['prefix' => 'calendar', 'as' => 'calendar.', 'namespace' => '\Calendar\Owner'], function () {
            Route::get('/{entity_id}/{entity_relation_service_id}/list', 'CalendarController@list')->name('list');
            Route::post('{entity_id}/{entity_relation_service_id}/save', 'CalendarController@store')->name('save');
            Route::post('/blockTimes', 'CalendarController@blockTimes')->name('block_times');
            Route::get('/show/{booking_calendar_id}', 'CalendarController@show')->name('show');
            Route::get('/show/{booking_calendar_id}/get', 'CalendarController@getCalendar')->name('get');
            Route::put('/{booking_calendar_id}/update', 'CalendarController@update')->name('update');
            Route::delete('/{booking_calendar_id}/delete', 'CalendarController@destroy')->name('delete');
        });

        Route::group(['prefix' => 'booking', 'as' => 'booking.', 'namespace' => '\Booking'], function () {

            Route::group(['namespace' => '\Owner'], function () {
                Route::get('/{model_id}/{model_type}/list', 'BookingController@list')->name('list');
                Route::get('/{model_id}/{model_type}/list_booking_chart', 'BookingController@listBookingChart')->name('listBookingChart');
                Route::get('/{model_id}/{model_type}/list_booking_details_chart', 'BookingController@listBookingDetailsChart')->name('listBookingDetailsChart');
                Route::get('/show/{booking_id}', 'BookingController@show')->name('show');
                Route::put('/{booking_id}/update', 'BookingController@update')->name('update');
                //Route::delete('/{booking_id}/delete', 'BookingController@destroy')->name('delete');
            });

            Route::group(['prefix' => 'manager', 'as' => 'manager.'], function () {
                Route::post('/{booking_id}/acceptEntity', 'BookingManagerController@acceptEntity')->name('acceptEntity');
                Route::post('/{booking_id}/doneBooking', 'BookingManagerController@doneBookingEntity')->name('doneBookingEntity');
                Route::post('/{booking_id}/doingBooking', 'BookingManagerController@doingBookingEntity')->name('doingBookingEntity');
                Route::post('/{booking_id}/rejectEntity', 'BookingManagerController@rejectEntity')->name('rejectEntity');
                Route::post('/{booking_detail_id}/rejectBookingDetailsEntity', 'BookingManagerController@rejectBookingDetailsEntity')->name('rejectBookingDetailsEntity');
            });

        });


    });

    Route::group(['middleware' => ['auth'], 'prefix' => 'user', 'as' => 'user.'], function () {

        Route::group(['prefix' => 'calendar', 'as' => 'calendar.', 'namespace' => '\Calendar\Owner'], function () {
            Route::get('/show/{booking_calendar_id}/get', 'CalendarController@getCalendar')->name('get');
        });


        Route::group(['prefix' => 'booking', 'as' => 'booking.', 'namespace' => '\Booking'], function () {

            Route::group([], function () {
                Route::get('/list', 'BookingController@list')->name('list');
                Route::get('/basket', 'BookingController@getUserBasketBooking')->name('basket');
                Route::post('{entity_id}/{entity_relation_service_id}/{entity_service_id}/save', 'BookingController@store')->name('save');
                Route::post('{entity_id}/details/save', 'BookingController@storeMultiDetails')->name('storeMultiDetails');
                Route::get('/show/{booking_id}', 'BookingController@show')->name('show');
                Route::delete('/details/delete', 'BookingController@destroyBookingDetails')->name('delete_details');
                Route::delete('/{booking_id}/delete', 'BookingController@destroy')->name('delete');
            });

            Route::group(['prefix' => 'manager', 'as' => 'manager.'], function () {
                Route::post('/{booking_id}/acceptUser', 'BookingManagerController@acceptUser')->name('acceptUser');
                Route::post('/{booking_id}/rejectUser', 'BookingManagerController@rejectUser')->name('rejectUser');
                Route::post('/{booking_detail_id}/rejectBookingDetailsUser', 'BookingManagerController@rejectBookingDetailsUser')->name('rejectBookingDetailsUser');
            });

        });

    });

});

Route::group(['middleware' => ['web', 'auth', 'admin'], 'prefix' => 'booking', 'as' => 'booking.', 'namespace' => 'Modules\Booking\Http\Controllers'], function () {

});