<?php

return [
    'tabs'=>[
        'public'=>'عمومی',
    ],

    'reasons_reject_entity'=>[
        'other'=>'غیره',
        'time_is_completed'=>'ظرفیت تکمبل شده',
        'request_of_customer'=>'درخواست مشتری',
        'absence_customer'=>'عدم حضور مشتری',
        'absence_employee'=>'عدم حضور متخصص',
    ],
    'complete_order'=>[
        'send_order_to_entity'=>'درخواست برای صاحب کسب و کار ارسال شد. :entity_name ',
    ],
    'reasons_reject_user'=>[
        'other'=>'غیره',
        'request_of_customer'=>'درخواست سرویس دهنده',
        'absence_employee'=>'عدم حضور متخصص',
        'entity_is_not_on_time'=>'سرویس دهنده به موقع سرویس نداد',
    ],
    'actions_text'=>[
        'new'=>'جدید',
        'do'=>'در انتظار انجام شدن',
        'doing'=>'در حال انجام',
        'done'=>'انجام شده',
        'cancel'=>'لغو شده',
    ],
    'list' => 'لیست رزروها',
    'user_basket' => 'سبد سفارشات',
    'this_user_basket' => 'سفارش کاربر :user_name',
    'user_basket_is_empty' => 'سبد سفارشات خالی است!',
    'instances' => 'سامانه های سیستم',
    'languages' => 'زبانهای سیستم',
    'select_language' => 'انتخاب زبان',
    'select_instance' => 'انتخاب سامانه',
    'select_status' => 'انتخاب وضعیت',
    'confirm_delete' => 'آیا از حذف اطمینان دارید؟',
    'confirm' => 'آیا از عملیات مربوطه اطمینان دارید؟',
    'reservation_status_text'=> ["1" => "تایید نهایی", "2" => "منتظر تایید سرویس دهنده", "3" => "منتظر تایید کاربر", "4" => "رد شده توسط خدمت دهنده", "5" => "کنسل شده از طرف کاربر"],
    'owner' => 'مالک',
    'configs' => 'تنظیمات',
    'delete' => 'حذف',
    'delete_file' => 'حذف فایل',
    'action' => 'عملیات',
    'add' => 'افزودن ',
    'edit' => 'ویرایش ',
    'show' => 'نمایش ',
    'title' => 'عنوان',

    'created_at' => 'تاریخ ایجاد',
    'updated_at' => 'آخرین بروزرسانی',
    'active_date' => 'تاریخ فعال شدن',
    'expire_date' => 'تاریخ غیر فعال شدن',
    'sortable' => 'مرتب سازی',
    'categories' => 'دسته بندی ها',
    'all_categories' => 'همه دسته بندی ها',
    'all_enrollments' => 'همه روش های ثبت نام',
    'category' => 'دسته بندی',
    'select_category' => 'انتخاب دسته بندی',

    'instance' => 'مشخصه سامانه (instance)',
    'cancel' => 'انصراف',
    'submit' => 'ثبت',
    'status' => 'وضعیت',
    'statuses' => [
        "0" => "حذف شده",
        "1" => "فعال",
        "2" => "غیرفعال",
    ],


];