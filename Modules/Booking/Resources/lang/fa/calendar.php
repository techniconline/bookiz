<?php

return [
    'tabs' => [
        'public' => 'عمومی',
    ],
    'list' => 'لیست تقویمها',
    'instances' => 'سامانه های سیستم',
    'languages' => 'زبانهای سیستم',
    'select_language' => 'انتخاب زبان',
    'select_instance' => 'انتخاب سامانه',
    'select_status' => 'انتخاب وضعیت',
    'confirm_delete' => 'آیا از حذف اطمینان دارید؟',
    'confirm' => 'آیا از عملیات مربوطه اطمینان دارید؟',

    'owner' => 'مالک',
    'configs' => 'تنظیمات',
    'delete' => 'حذف',
    'delete_file' => 'حذف فایل',
    'action' => 'عملیات',
    'add' => 'افزودن ',
    'edit' => 'ویرایش ',
    'show' => 'نمایش ',
    'title' => 'عنوان',
    'gender' => 'جنسیت',
    'min' => 'دقیقه',
    'hr' => 'ساعت',

    'all_days' => [
        'mon' => 'دوشنبه',
        'tue' => 'سه شنبه',
        'wed' => 'چهارشنبه',
        'thu' => 'پنجشنبه',
        'fri' => 'جمعه',
        'sat' => 'شنبه',
        'sun' => 'یکشنبه',
    ],

    'genders' => [
        'all' => 'همه',
        'female' => 'زن',
        'male' => 'مرد',
    ],

    'created_at' => 'تاریخ ایجاد',
    'updated_at' => 'آخرین بروزرسانی',
    'active_date' => 'تاریخ فعال شدن',
    'expire_date' => 'تاریخ غیر فعال شدن',
    'sortable' => 'مرتب سازی',
    'categories' => 'دسته بندی ها',
    'all_categories' => 'همه دسته بندی ها',
    'all_enrollments' => 'همه روش های ثبت نام',
    'category' => 'دسته بندی',
    'select_category' => 'انتخاب دسته بندی',

    'instance' => 'مشخصه سامانه (instance)',
    'cancel' => 'انصراف',
    'submit' => 'ثبت',
    'status' => 'وضعیت',
    'statuses' => [
        "0" => "حذف شده",
        "1" => "فعال",
        "2" => "غیرفعال",
    ],


];