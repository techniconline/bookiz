<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("entity::entity.".($view_model->getModelData('id')?'edit':'add'))
                </div>
                <div class="tools">
                    <a href="" class="collapse"> </a>
                </div>
            </div>
            {{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>($view_model->getModelData('id')? route('entity.product.update', ['entity_id'=>$view_model->getModelData('id')]) : route('entity.product.save'))
                        ,'method'=>($view_model->getModelData('id')?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("entity::entity.tabs.public") </a>
                                </li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::input('text','title',old('title',$view_model->getModelData('title'))
                                        ,['required'=>'required','title'=>trans("entity::entity.products.title"),'helper'=>trans("entity::entity.products.title")]) !!}

                                    {!! FormHelper::input('text','quantity',old('quantity',$view_model->getModelData('quantity'))
                                        ,['required'=>'required','title'=>trans("entity::entity.products.quantity"),'helper'=>trans("entity::entity.products.quantity")]) !!}

                                    {!! FormHelper::selectTag('unit',trans("entity::entity.products.units"),old('unit',$view_model->request->get("unit")),['title'=>trans("entity::entity.products.unit"),'helper'=>trans("entity::entity.products.unit")]) !!}

                                    {!! FormHelper::input('text','order_point',old('order_point',$view_model->getModelData('order_point'))
                                    ,['required'=>'required','title'=>trans("entity::entity.products.order_point"),'helper'=>trans("entity::entity.products.order_point")]) !!}

                                    {!! FormHelper::checkbox('notify',1,old('notify',$view_model->getModelData('notify')) ,['title'=>trans("entity::entity.products.notify")]) !!}

                                    {!! FormHelper::selectTag('entity_id',[],old('entity_id',$view_model->getModelData('entity_id')?$view_model->getModelData('entity_id'):$view_model->request->get("entity_id")),['data-ajax-url'=>route('entity.get_entity_by_api')
                                      ,'title'=>trans("entity::entity.products.title_entity"),'helper'=>trans("entity::entity.products.title_entity")]) !!}

                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("entity::entity.submit")], ['title'=>trans("entity::entity.cancel"), 'url'=>route('entity.product.index',["entity_id"=>$view_model->getModelData('entity_id')?$view_model->getModelData('entity_id'):$view_model->request->get("entity_id")])]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>

        </div>

    </div>

</div>
