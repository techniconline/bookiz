<?php

namespace Modules\Booking\ViewModels\Calendar\Owner;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Booking\Models\Calendar\BookingCalendar;
use Modules\Booking\Models\Calendar\BookingCalendarDetails;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Category\EntityCategoryRelation;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\Models\Service\EntityServices;
use Morilog\Jalali\Facades\jDateTime;

class BookingCalendarViewModel extends BaseViewModel
{

    private $access_assets;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @param bool $json
     * @param null $entity_id
     * @param null $entity_relation_service_id
     * @return \Illuminate\Support\Collection
     * @throws \Throwable
     */
    protected function listCalendar($json = false, $entity_id = null, $entity_relation_service_id = null)
    {
        $model = BridgeHelper::getEntity()->getEntityModel();
        $model = $model->enable()->isOwner()->find($this->request->get('entity_id', $entity_id));

        if (!$model) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.access_error"));
        }

        $calendars = BookingCalendar::active()->where('entity_id', $model->id)->where(function ($q) use ($entity_relation_service_id) {

            if ($entity_relation_service_id = $this->request->get('entity_relation_service_id', $entity_relation_service_id)) {
                return $q->where('entity_relation_service_id', $entity_relation_service_id);
            }

        })->with('entityRelationService')->get();
        $calendars = $this->decorateList($calendars);

        if ($json){
            return $calendars;
        }

        $this->setTitlePage(trans('booking::calendar.list'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($calendars)->setResponse(true);
        }
        $this->setModelData($model);
        $viewModel =& $this;
        return $this->renderedView("booking::calendar.form_calendar", ['view_model' => $viewModel], "view");

    }

    /**
     * @return $this
     */
    protected function showCalendar()
    {
        $booking_calendar_id = $booking_calendar_id?:$this->request->get('booking_calendar_id');
        $booking_calendar = BookingCalendar::enable()->with('entityRelationService')->find($booking_calendar_id);

        if (!$booking_calendar) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.not_find_data"));
        }

        $entity = BridgeHelper::getEntity()->getEntityModel()->enable()->isOwner()->find($booking_calendar->entity_id);

        if (!$entity) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.access_error"));
        }

        return $this->setDataResponse($booking_calendar)->setResponse(false, trans("booking::messages.alert.del_un_success"));

    }

    /**
     * @param null $booking_calendar_id
     * @param null $entity_employee_id
     * @param bool $json
     * @param bool $check_now_time
     * @param null $date
     * @param null $days_active_reserve
     * @return $this|array
     */
    public function getCalendar($booking_calendar_id = null, $entity_employee_id = null, $json = false, $check_now_time = false, $date = null, $days_active_reserve = null)
    {
        $booking_calendar_id = $booking_calendar_id ?: $this->request->get('booking_calendar_id');
        $entity_employee_id = $entity_employee_id ?: $this->request->get('entity_employee_id');
        $date = $date ?: ($json ? null : $this->request->get('date'));
        $date = $date ? DateHelper::setLocaleDateTime($date, trans('core::date.datetime.date_medium'))->getDateTime(trans('core::date.datetime.medium')) : null;
        $booking_calendar = BookingCalendar::enable()->with('entityRelationService')
            ->where(function ($q) use ($date) {
                if ($date) {
                    $q->where('start_date', "<=", $date);
                    $q->where('end_date', ">=", $date);
                }
            })
            ->find($booking_calendar_id);

        if (!$booking_calendar) {
            if ($json) {
                return false;
            }
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.not_find_data"));
        }

        $sDate = $booking_calendar->start_date;
        $eDate = $booking_calendar->end_date;

        if (time() >= strtotime($eDate)) {
            if ($json) {
                return false;
            }
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.not_find_data"));
        }

        $days_active_reserve = $days_active_reserve ?: $booking_calendar->days_active_reserve;
        $count_reservation_in_time = $booking_calendar->count_reservation_in_time ?: 1;
        $period = $booking_calendar->period;
        $params = $booking_calendar->params_json;
        $open_time = isset($params->open_time) ? $params->open_time : '09:00';
        $close_time = isset($params->close_time) ? $params->close_time : '20:00';
        $days_off = isset($params->closed) ? $params->closed : null;
        $days = isset($params->days) ? $params->days : [];

        if ($date) {

            $dayName = strtolower(date("D", strtotime($date)));
            if (isset($days->$dayName) && isset($days->$dayName->open) && isset($days->$dayName->close)) {
                $open_time = $days->$dayName->open;
                $close_time = $days->$dayName->close;
            }
            $days_active_reserve = 1;
            $sDate = date("Y-m-d", strtotime($date));

        }

        $tsDate = $sDate ? strtotime($sDate) : strtotime(date("Y-m-d"));
        $now = strtotime(date("Y-m-d"));

        if ($tsDate < $now) {
            $sDate = date("Y-m-d");
        }

        $used = $this->getTimesUsed($booking_calendar->id, $sDate, $eDate, $count_reservation_in_time, $entity_employee_id);
        $result = $this->generateCalendar($sDate, $days_active_reserve, $open_time, $close_time, $period, $days_off, $used, $check_now_time, $eDate, $days);


        if ($json) {
            return $result;
        }
        return $this->redirectBack()->setDataResponse($result)->setResponse(boolval($result));

    }

    /**
     * @param $start_date
     * @param int $count_days
     * @param string $open_time
     * @param string $close_time
     * @param int $period
     * @param array $days_off
     * @param array $used
     * @param bool $check_now_time
     * @param null $eDate
     * @param null $days
     * @return array
     */
    public function generateCalendar($start_date, $count_days = 7, $open_time = '09:00', $close_time = '20:00', $period = 15, $days_off = [], $used = [], $check_now_time = false, $eDate = null, $days = null)
    {
        $end_date = strtotime($start_date . " + " . $count_days . " days ");
        $end_date = date("Y-m-d", $end_date);
        $end_date .= " " . $close_time;
        $used = collect($used);

        $default_dayEnd = $dayEnd = $start_date . " " . $close_time;
        $default_start_date = $start_date .= " " . $open_time;

        if ($days_off) {
            $days_off = array_map(function ($item) {
                return strtolower($item);
            }, $days_off);
        }

        $dates = [];
        date_default_timezone_set(config('app.timezone'));
        for ($i = 0; $i < $count_days; $i++) {
            $start_date = $default_start_date;
            $dayEnd = $default_dayEnd;

            $sdate = date("Y-m-d H:i", strtotime($start_date . " + " . $i . " days "));
            $edate = date("Y-m-d H:i", strtotime($dayEnd . " + " . $i . " days "));
            $day_name = strtolower(date("D", strtotime($sdate)));

            // set custom time table
            if ($days) {
                if (isset($days->$day_name)) {

                    if (isset($days->$day_name->open) && $days->$day_name->open) {
                        $arr = explode(" ", $sdate, 2);
                        $sdate = array_first($arr) . " " . $days->$day_name->open;
                    }

                    if (isset($days->$day_name->close) && $days->$day_name->close) {
                        $arr = explode(" ", $edate, 2);
                        $edate = array_first($arr) . " " . $days->$day_name->close;
                    }

                }
            }
            //---------------------------


            if ($eDate && strtotime($sdate) > strtotime($eDate)) {
                return $dates;
            }

            if (!empty($days_off) && in_array($day_name, $days_off)) {
                continue;
            }

            $tsDate = strtotime($sdate);
            $teDate = strtotime($edate);
            $time = ($tsDate);
            $duration = 0;

            while ($teDate > $time) {
                $is_valid = true;
                $time = date("Y-m-d H:i", strtotime($sdate . " + " . $duration . " minutes "));

                if (strtotime($time) <= time()) {
                    $duration += $period;
                    continue;
                }
                if ($used->isNotEmpty() && $used->contains($time)) {
                    $is_valid = false;
                }

                $timeArr = explode(" ", $time);
                $dateN = $timeArr[0];
                $timeN = $timeArr[1];

                $dates[$dateN][$timeN] = [
//                    "date_time"=>DateHelper::setCalender("miladi")->setDateTime($time)->getLocaleFormat(trans('core::date.datetime.long')),
                    "valid" => $is_valid
                ];
                $time = strtotime($time);
                $duration += $period;
            };

        }
        date_default_timezone_set(config('app.timezone'));
        return $dates;
    }

    /**
     * @return $this
     */
    public function blockTimes()
    {
        $booking_calendar_id = $this->request->get('booking_calendar_id');
        $entity_employee_id = $this->request->get('entity_employee_id');
        $start_datetime = $this->request->get('start_date_time');
        $end_datetime = $this->request->get('end_date_time');

        try {

            $sd = explode(" ", $start_datetime, 2);
            $ed = explode(" ", $end_datetime, 2);

            $sDate = array_first($sd);
            $eDate = array_first($ed);

            $sTime = end($sd);
            $eTime = end($ed);

            $sDate = DateHelper::setLocaleDateTime($sDate, trans('core::date.datetime.date_medium'))->getDateTime(trans('core::date.datetime.medium'));
            $eDate = DateHelper::setLocaleDateTime($eDate, trans('core::date.datetime.date_medium'))->getDateTime(trans('core::date.datetime.medium'));

            $booking_calendar = BookingCalendar::enable()
                ->with('entityRelationService')
                ->where(function ($q) use ($eDate, $sDate) {
                    if ($sDate) {
                        $q->where('start_date', "<=", $sDate);
                    }
                    if ($eDate) {
                        $q->where('end_date', ">=", $eDate);
                    }
                })
                ->find($booking_calendar_id);

            if (!$booking_calendar) {
                return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.not_find_data"));
            }

            $date1 = date_create($sDate);
            $date2 = date_create($eDate);
            $diff = date_diff($date1, $date2);

            if ($diff->invert) {
                return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.mis_data"));
            }
            $count_days = $diff->days;

            $resultGenerateCalendar = [];
            for ($i = 0; $i <= $count_days; $i++) {
                $date = date("Y-m-d", strtotime($sDate . " + " . $i . " days "));
                $date = DateHelper::setDateTime($date)->getLocaleFormat(trans('core::date.datetime.date_medium'));
                $res = $this->getCalendar($booking_calendar_id, $entity_employee_id, true, false, $date);
                if ($resultGenerateCalendar && $res) {
                    $resultGenerateCalendar = array_merge($res, $resultGenerateCalendar);
                } else {
                    $resultGenerateCalendar = $res;
                }
            }


            if ($resultGenerateCalendar) {
                $sDateTime = DateHelper::setLocaleDateTime($start_datetime, trans('core::date.datetime.medium'))->getDateTime(trans('core::date.datetime.medium'));
                $eDateTime = DateHelper::setLocaleDateTime($end_datetime, trans('core::date.datetime.medium'))->getDateTime(trans('core::date.datetime.medium'));

                $sTimeStamp = strtotime($sDateTime);
                $eTimeStamp = strtotime($eDateTime);

                foreach ($resultGenerateCalendar as $dateKey => $times) {

                    foreach ($times as $time => $valid) {

                        if (isset($valid['valid']) && !$valid['valid']) {
                            continue;
                        }

                        $thisTimeStamp = strtotime($dateKey . " " . $time);
                        $thisDate = date("Y-m-d H:i:s", $thisTimeStamp);

                        if ($thisTimeStamp >= $sTimeStamp && $thisTimeStamp <= $eTimeStamp) {
                            $model = new BookingCalendarDetails();
                            list($dateIns, $timeIns) = explode(" ", $thisDate);
                            $insItems = [
                                'date' => $dateIns,
                                'time' => $timeIns,
                                'booking_calendar_id' => $booking_calendar_id,
                                'entity_employee_id' => $entity_employee_id,
                                'count_reserved' => $booking_calendar->count_reservation_in_time,
                                'reservation_status' => $model::RESERVATION_STATUS_BLOCK,
                                'active' => 1,
                            ];

                            $model->fill($insItems);
                            $model->save();
                        }

                    }

                }

            }
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.save_success"));
        } catch (\Exception $exception) {
            report($exception);
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.error"));
        }
    }

    /**
     * @param $booking_calendar_id
     * @param $start_date
     * @param $end_date
     * @param int $count_reservation_in_time
     * @param null $employee_id
     * @return null|static
     */
    protected function getTimesUsed($booking_calendar_id, $start_date, $end_date, $count_reservation_in_time = 1, $employee_id = null)
    {
        $bookingCalendarDetails = BookingCalendarDetails::active()
            ->where("booking_calendar_id", $booking_calendar_id)
            ->where(function ($q) use ($employee_id, $count_reservation_in_time) {
                if ($employee_id) {
                    $q->where("entity_employee_id", $employee_id);
                }
                if ($count_reservation_in_time) {
                    $q->where("count_reserved", ">=", $count_reservation_in_time);
                }
            })
            ->where("date", ">=", $start_date)
            ->where("date", "<=", $end_date)
            ->get();

        if ($bookingCalendarDetails) {
            $used = collect($bookingCalendarDetails)->mapWithKeys(function ($item, $index) {
                return [date("Y-m-d", strtotime($item->date)) . " " . date("H:i", strtotime($item->time)) => date("Y-m-d", strtotime($item->date)) . " " . date("H:i", strtotime($item->time))];
            });
            return $used;
        }
        return null;
    }

    /**
     * @param bool $check_access_owner
     * @return $this
     */
    public function destroyCalendar($check_access_owner = true)
    {
        $booking_calendar_id = $this->request->get('booking_calendar_id');
        $booking_calendar = BookingCalendar::enable()->find($booking_calendar_id);

        if (!$booking_calendar) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.not_find_data"));
        }

        if ($check_access_owner) {
            $entity = BridgeHelper::getEntity()->getEntityModel()->enable()->isOwner()->find($booking_calendar->entity_id);

            if (!$entity) {
                return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.access_error"));
            }
        }

        if ($booking_calendar->delete()) {
            return $this->setResponse(true, trans("booking::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("booking::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    public function saveCalendar()
    {
        $data = $this->request->all();
        $response = $this->serviceSaveCalendar($data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('booking.admin.calendar.show', ['booking_calendar_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }


    /**
     * @param $data
     * @param bool $check_access_owner
     * @return array
     */
    public function serviceSaveCalendar($data, $check_access_owner = true)
    {
        try {

            $model = new BookingCalendar();
            $booking_calendar_id = isset($data['booking_calendar_id']) ? $data['booking_calendar_id'] : 0;
            if ($booking_calendar_id) {
                $model = $model->find($booking_calendar_id);
                if (!$model) {
                    return ['action' => false, 'message' => trans("booking::messages.alert.not_find_data")];
                }

                $entity_id = $model->entity_id;
                $entity_relation_service_id = $model->entity_relation_service_id;

                $model_id = $model->model_id;
                $model_type = $model->model_type;

            } else {
                $entity_id = isset($data['entity_id']) ? $data['entity_id'] : 0;
                $entity_relation_service_id = isset($data['entity_relation_service_id']) ? $data['entity_relation_service_id'] : null;
            }

            if ($entity_relation_service_id) {
                $entityRelationService = EntityRelationServices::enable()->find($entity_relation_service_id);
                if (!$entityRelationService) {
                    return ['action' => false, 'message' => trans("booking::messages.alert.not_find_data")];
                }
            }

            if ($check_access_owner) {
                $entity = BridgeHelper::getEntity()->getEntityModel()->enable()->isOwner()->find($entity_id);
                if (!$entity) {
                    return ['action' => false, 'message' => trans("booking::messages.alert.access_error")];
                }
            }


            $data['start_date'] = isset($data['start_date']) ? DateHelper::setLocaleDateTime($data['start_date'], trans('core::date.datetime.date_medium'))->getDateTime(trans('core::date.datetime.medium')) : date("Y-m-d 00:00:00", strtotime(date("Y-m-d") . "+ 1 days"));
            $data['end_date'] = isset($data['end_date']) ? DateHelper::setLocaleDateTime($data['end_date'], trans('core::date.datetime.date_medium'))->getDateTime(trans('core::date.datetime.medium')) : date("Y-m-d 00:00:00", strtotime(date("Y-m-d") . "+ 366 days"));

            if (strtotime($data['start_date']) >= strtotime($data['end_date'])) {
                return ['action' => false, 'message' => trans("booking::messages.alert.mis_data")];
            }

//            if (!$booking_calendar_id) {
//                $calendars = BookingCalendar::active()->where('entity_id', $entity_id)->where(function ($q) use ($entity_relation_service_id) {
//                    if ($entity_relation_service_id) {
//                        return $q->where('entity_relation_service_id', $entity_relation_service_id);
//                    }
//                })->with('entityRelationService')->first();
//                if ($calendars) {
//                    return ['action' => false, 'message' => trans("booking::messages.alert.duplicate_data")];
//                }
//            }

            $days_off = $data["params"]["closed"] = isset($data["closed"]) ? $data["closed"] : null;
            $open_time = $data["params"]["open_time"] = isset($data["open_time"]) ? $data["open_time"] : '09:00';
            $close_time = $data["params"]["close_time"] = isset($data["close_time"]) ? $data["close_time"] : '20:00';
            $data["params"] = json_encode($data["params"]);

            if ($this->isValidRequest($data, $model->rules, $model->messages, "POST")->isValidRequest) {

                $model->fill($data);
//                if ($days_off){
//                    $off = $this->disableDaysOfWeek($model, $days_off);
//                }
                if ($model->save()) {
                    return ['action' => true, 'message' => trans("booking::messages.alert.save_success"), 'data' => $model];
                } else {
                    return ['action' => false, 'message' => trans("booking::messages.alert.save_un_success")];
                }
            }
            return ['action' => false, 'message' => trans("booking::messages.alert.mis_data"), 'errors' => $this->errors];
        } catch (\Exception $exception) {
            report($exception);
            return ['action' => false, 'message' => trans("booking::messages.alert.mis_data")];
        }
    }

    /**
     * @param BookingCalendar $calendar
     * @param array $days_off
     * @param null $now_time
     * @return array|bool
     */
    protected function disableDaysOfWeek(BookingCalendar $calendar, $days_off = ["fri"], $now_time = null)
    {
        $start_date = $now_time ?: $calendar->start_date;
        $end_date = $calendar->end_date;

        if (!$start_date || !$end_date || empty($days_off)) {
            return false;
        }

        $start_date = strtotime($start_date);
        $end_date = strtotime($end_date);

        if ($start_date > $end_date) {
            return false;
        }

        $days_off = array_map(function ($item) {
            return strtolower($item);
        }, $days_off);

        $total_time = $end_date - $start_date;
        $days = (int)($total_time / 86400);

        $start_date = date("Y-m-d", $start_date);
        $dates_off = [];
        for ($i = 0; $i < $days; $i++) {
            $date = date("Y-m-d", strtotime($start_date . " + " . $i . " days "));
            $day_name = strtolower(date("D", strtotime($date)));
            if (in_array($day_name, $days_off)) {
                $dates_off[] = $date;
            }
        }
        return $dates_off;
    }

    public function dataValidity($model_id, $model_type)
    {



    }


}
