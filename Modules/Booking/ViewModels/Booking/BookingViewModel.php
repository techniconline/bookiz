<?php

namespace Modules\Booking\ViewModels\Booking;

use Illuminate\Support\Facades\DB;
use Modules\Booking\Models\Booking;
use Modules\Booking\Models\Calendar\BookingCalendar;
use Modules\Booking\Models\Calendar\BookingCalendarDetails;
use Modules\Booking\Traits\Booking\BookingTrait;
use Modules\Booking\ViewModels\Calendar\Owner\BookingCalendarViewModel;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Entity;

class BookingViewModel extends BaseViewModel
{

    use BookingTrait;
    private $access_assets;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function listBooking()
    {

        $bookings = Booking::active()
            ->owner()->with(
                'bookingDetails.entityRelationService'
                , 'bookingDetails.entityEmployee.user'
                , 'entity'
            )
            ->orderBy("id", "DESC")
            ->paginate();
        $bookings = $this->decorateList($bookings);

        $this->setTitlePage(trans('booking::booking.list'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($bookings)->setResponse(true);
        }
        $this->setModelData($bookings);
        $viewModel =& $this;
        return $this->renderedView("booking::calendar.form_calendar", ['view_model' => $viewModel], "view");

    }

    /**
     * @return $this
     */
    protected function showBooking()
    {
        $booking_id = $this->request->get('booking_id');
        $booking = Booking::enable()->owner()->with(
            'bookingDetails.entityRelationService'
            , 'bookingDetails.entityEmployee.user'
            , 'entity', 'user'
        )->find($booking_id);

        if (!$booking) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.not_find_data"));
        }

        if (get_instance()->isAjax()) {
            $booking = $this->decorateAttributes($booking);
        }

        return $this->setDataResponse($booking)->setResponse(true);

    }

    /**
     * @return BookingViewModel
     * @throws \Throwable
     */
    public function getUserBasketBooking()
    {
        $booking = Booking::owner()
            ->active()
            ->where(function ($q) {
                $q->where('reservation_status', Booking::RESERVATION_STATUS_USER_PEND);
            })
            ->with(
                'bookingDetails.entityRelationService'
                , 'bookingDetails.entityEmployee.user'
                , 'entity', 'user'
            )
            ->orderBy("id", "DESC")
            ->first();

        $booking = $this->decorateAttributes($booking);
        $action = boolval($booking);
        $this->setTitlePage(trans('booking::booking.user_basket'));
        if (get_instance()->isAjax()) {
            $user = $booking ? $booking['user'] : null;
            $user_name = $user ? $user["full_name"] : null;
            return $this->setDataResponse($booking)->setResponse($action, $action ? trans('booking::booking.this_user_basket', ['user_name' => $user_name]) : trans('booking::booking.user_basket_is_empty'));
        }
        $this->setModelData($booking);
        $viewModel =& $this;
        return $this->renderedView("booking::booking.basket", ['view_model' => $viewModel], "view");

    }

    /**
     * @return $this
     */
    protected function destroyBooking()
    {
        $booking_id = $this->request->get('booking_id');
        $booking = Booking::enable()->isOpenUser()->owner()->find($booking_id);

        if (!$booking) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.not_find_data"));
        }

        if ($booking->delete()) {
            $bookingDetails = Booking\BookingDetails::active()->where("booking_id", $booking->id)->get();
            foreach ($bookingDetails as $bookingDetail) {
                $this->removeBookingCalendarDetails($bookingDetail);
                $bookingDetail->delete();
            }
            return $this->setResponse(true, trans("booking::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("booking::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function destroyBookingDetails()
    {
        try {
            $booking_detail_id = $this->request->get('booking_detail_id');
            $booking_detail_id = is_array($booking_detail_id) ? $booking_detail_id : [$booking_detail_id];
            $bookingDetails = Booking\BookingDetails::active()->whereIn("id", $booking_detail_id)->get();

            if (!$bookingDetails) {
                return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.not_find_data"));
            }

            $booking_id = $bookingDetails[0]->booking_id;
            $booking = Booking::enable()->isOpenUser()->owner()->find($booking_id);

            if (!$booking) {
                return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.access_error"));
            }

            foreach ($bookingDetails as $bookingDetail) {
                $bookingDetail->delete();
                $this->removeBookingCalendarDetails($bookingDetail);
            }
            return $this->setResponse(true, trans("booking::messages.alert.del_success"));
        } catch (\Exception $exception) {
            report($exception);
            return $this->setResponse(false, trans("booking::messages.alert.error"));
        }


    }

    /**
     * @param bool $json
     * @return $this
     */
    public function saveBooking($json = false)
    {
        $result = $this->setEntity()->saveBookingItem();
        $data = $this->booking ? $this->booking : null;

        if ($json){
            return $data;
        }

        return $this->redirectBack()->setDataResponse($data)->setResponse(boolval($result), $this->message_booking, $this->errors_validation);
    }

}
