<?php

namespace Modules\Booking\ViewModels\Booking;

use Modules\Booking\Traits\Booking\BookingTrait;
use Modules\Core\ViewModels\BaseViewModel;

class BookingManagerViewModel extends BaseViewModel
{

    use BookingTrait;
    private $access_assets;

    public function __construct()
    {

    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @param null $booking_id
     * @return BookingManagerViewModel
     */
    public function acceptBookingUser($booking_id = null)
    {
        $booking_id = $booking_id?:$this->request->get("booking_id");
        $res = $this->acceptUserBooking($booking_id);
        return $this->redirectBack()->setResponse($res, $this->message_booking);

    }

    /**
     * @param null $booking_id
     * @return BookingManagerViewModel
     */
    public function rejectBookingUser($booking_id = null)
    {
        $booking_id = $booking_id?:$this->request->get("booking_id");
        $res = $this->rejectUserBooking($booking_id);
        return $this->redirectBack()->setResponse($res, $this->message_booking);
    }

    /**
     * @return BookingManagerViewModel
     */
    public function acceptBookingEntity()
    {
        $booking_id = $this->request->get("booking_id");
        $res = $this->acceptEntityBooking($booking_id);
        return $this->redirectBack()->setResponse($res, $this->message_booking);
    }

    /**
     * @return BookingManagerViewModel
     */
    public function doneBookingEntity()
    {
        $booking_id = $this->request->get("booking_id");
        $res = $this->doneEntityBooking($booking_id);
        return $this->redirectBack()->setResponse($res, $this->message_booking);
    }

    /**
     * @return BookingManagerViewModel
     */
    public function doingBookingEntity()
    {
        $booking_id = $this->request->get("booking_id");
        $res = $this->doingEntityBooking($booking_id);
        return $this->redirectBack()->setResponse($res, $this->message_booking);
    }

    /**
     * @return BookingManagerViewModel
     */
    public function rejectBookingEntity()
    {
        $booking_id = $this->request->get("booking_id");
        $res = $this->rejectEntityBooking($booking_id);
        return $this->redirectBack()->setResponse($res, $this->message_booking);
    }

    /**
     * @return BookingManagerViewModel
     */
    public function rejectBookingDetailsEntity()
    {
        $booking_detail_id = $this->request->get("booking_detail_id");
        $res = $this->rejectEntityBookingDetails($booking_detail_id);
        return $this->redirectBack()->setResponse($res, $this->message_booking);
    }

    /**
     * @return BookingManagerViewModel
     */
    public function rejectBookingDetailsUser()
    {
        $booking_detail_id = $this->request->get("booking_detail_id");
        $res = $this->rejectUserBookingDetails($booking_detail_id);
        return $this->redirectBack()->setResponse($res, $this->message_booking);
    }


}
