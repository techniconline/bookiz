<?php

namespace Modules\Booking\ViewModels\Booking\Owner;

use Illuminate\Support\Facades\DB;
use Modules\Booking\Models\Booking;
use Modules\Booking\Models\Calendar\BookingCalendar;
use Modules\Booking\Traits\Booking\BookingTrait;
use Modules\Booking\ViewModels\Calendar\Owner\BookingCalendarViewModel;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\Traits\ModelTrait;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Entity\Models\Entity;

class BookingViewModel extends BaseViewModel
{

    use BookingTrait;

    private $access_assets;


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $access_assets = [];

        if ($this->access_assets) {

        }

        return $access_assets;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function listBooking()
    {
        $model = $this->getModelObject($this->request->get('model_type'));
        $model = $model->enable()->isOwner()->find($this->request->get('model_id'));

        if (!$model) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.access_error"));
        }

        $bookings = Booking::active()->where('model_type', $model->getTable())->where('model_id', $model->id)
            ->with(['bookingDetails','bookingDetails.entityEmployee.user', 'user'])
            ->whereHas('bookingDetails',function($q){
                $q->filterDate('date',$this->request->get('start_date'),'Y-m-d','>=')
                    ->filterDate('date',$this->request->get('end_date'),'Y-m-d','<=')
                    ->filterTimeDetails($this->request->get('start_time'),$this->request->get('end_time'));
            })
            ->whereIn("reservation_status", [Booking::RESERVATION_STATUS_ENTITY_PEND, Booking::RESERVATION_STATUS_ACCEPT])
            ->orderBy("reservation_status", "DESC")
            ->orderBy("id", "DESC");

        if ($this->request->get('with_out_paginate')){
            $bookings = $bookings->get();
        }else{
            $bookings = $bookings->paginate();
        }

        $bookings = $this->decorateList($bookings);

        $this->setTitlePage(trans('booking::booking.list'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($bookings)->setResponse(true);
        }
        $this->setModelData($model);
        $viewModel =& $this;
        return $this->renderedView("booking::calendar.xxx", ['view_model' => $viewModel], "view");

    }

    /**
     * @param null $start_date
     * @param null $end_date
     * @param null $start_time
     * @param null $end_time
     * @param string $group_by
     * @param null $counter
     * @param bool $for_model
     * @param bool $for_employee
     * @return BookingViewModel
     * @throws \Throwable
     */
    public function listBookingDetailsChart($start_date = null, $end_date = null, $start_time = null, $end_time = null, $group_by = null, $counter = null, $for_model = false, $for_employee = false)
    {
        $model = $this->getModelObject($this->request->get('model_type'));
        $model = $model->enable()->isOwner()->find($this->request->get('model_id'));

        if (!$model) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.access_error"));
        }

        $start_date = $start_date ?: $this->request->get('start_date', date("Y-m-d", strtotime(now() . " -3 days")));
        $end_date = $end_date ?: $this->request->get('end_date', date("Y-m-d"));

        $start_time = $start_time ?: $this->request->get('start_time', '00:00:00');
        $end_time = $end_time ?: $this->request->get('end_time', '23:59:59');
        $group_by = $group_by ?: $this->request->get('group_by', 'date');
        $for_model = $for_model ?: $this->request->get('for_model');
        $counter = $counter ?: $this->request->get('counter');
        $for_employee = $for_employee ?: $this->request->get('for_employee');

        $selectCounter = 'count(id) as counter';
        if ($counter == 'amount') {
            $selectCounter = 'sum(amount) as counter';
        }

        $list = Booking\BookingDetails::active()->forEntityOwner()->with(["booking" => function ($q) use ($model) {
            $q->forEntityOwner()->where('model_type', $model->getTable())->where('model_id', $model->id);
        }])->where('date', '>=', $start_date)
            ->where('date', '<=', $end_date)
            ->where(function ($q) use ($start_time, $end_time) {
                if ($start_time) {
                    $q->where('time', '>=', $start_time);
                }
                if ($end_time) {
                    $q->where('time', '<=', $end_time);
                }
            })->select(DB::raw('DATE_FORMAT(date, "%Y-%m-%d") as date'), DB::raw($selectCounter));

        $list->orderBy('date', 'DESC');

        if ($for_model) {
            $list->orderBy('counter', 'DESC');
            $list->groupBy(['model_id', 'model_type']);
            $list->addSelect(['model_id', 'model_type']);
        }

        if ($for_employee) {
            $list->with(['entityEmployee.user']);
            $list->orderBy('counter', 'DESC');
            $list->groupBy(['entity_employee_id']);
            $list->addSelect(['entity_employee_id']);
        }

        if ($group_by == 'hours') {
            $list->groupBy(DB::raw('DATE_FORMAT(date, "%Y-%m-%d")'));
            $list->groupBy(DB::raw('DATE_FORMAT(time, "%H")'));
            $list->addSelect(DB::raw('DATE_FORMAT(time, "%H") as time_hours'));
            $list->orderBy('time_hours', 'DESC');
        } else {
            $list->groupBy(DB::raw('DATE_FORMAT(date, "%Y-%m-%d")'));
        }


        $list = $list->get();

        $list = $this->decorateForChart($list);

        $this->setTitlePage(trans('booking::booking.list'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($list)->setResponse(true);
        }
        $this->setModelData($list);
        $viewModel =& $this;
        return $this->renderedView("booking::calendar.xxx", ['view_model' => $viewModel], "view");

    }

    /**
     * @param null $start_date
     * @param null $end_date
     * @param null $start_time
     * @param null $end_time
     * @param string $group_by
     * @return BookingViewModel
     * @throws \Throwable
     */
    public function listBookingChart($start_date = null, $end_date = null, $start_time = null, $end_time = null, $group_by = null)
    {
        $model = $this->getModelObject($this->request->get('model_type'));
        $model = $model->enable()->isOwner()->find($this->request->get('model_id'));

        if (!$model) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.access_error"));
        }

        $start_date = $start_date ?: $this->request->get('start_date', date("Y-m-d", strtotime(now() . " -7 days")));
        $end_date = $end_date ?: $this->request->get('end_date', date("Y-m-d"));

        $start_time = $start_time ?: $this->request->get('start_time', '00:00:00');
        $end_time = $end_time ?: $this->request->get('end_time', '23:59:59');

        $start_date .= " " . $start_time;
        $end_date .= " " . $end_time;

        $group_by = $group_by ?: $this->request->get('group_by', 'date');

        $list = Booking::active()->forEntityOwner()
            ->where('model_type', $model->getTable())
            ->where('model_id', $model->id)
            ->where('created_at', '>=', $start_date)
            ->where('created_at', '<=', $end_date)
            ->select(DB::raw('count(id) as count'));

        if ($group_by == 'hours') {
            $list->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H")'));
            $list->addSelect(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H") as date'));
        } else {
            $list->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d")'));
            $list->addSelect(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as date'));
        }

        $list = $list->get();

        $list = $this->decorateForChart($list);

        $this->setTitlePage(trans('booking::booking.list'));
        if (get_instance()->isAjax()) {
            return $this->setDataResponse($list)->setResponse(true);
        }
        $this->setModelData($list);
        $viewModel =& $this;
        return $this->renderedView("booking::calendar.xxx", ['view_model' => $viewModel], "view");

    }

    /**
     * @param $list
     * @return array
     */
    public function decorateForChart($list)
    {
        $list = collect($list)->mapWithKeys(function ($row, $index) {

            $new = [];
            foreach ($row->toArray() as $key => $value) {
                if ($value) {
                    $new[$index][$key] = $value;
                }
            }
            return $new;

        })->all();
        return $list;
    }

    /**
     * @return $this
     */
    protected function showBooking()
    {
        $booking_id = $this->request->get('booking_id');
        $booking = Booking::enable()->with(
            'bookingDetails.entityRelationService'
            , 'bookingDetails.entityEmployee.user'
            , 'entity', 'user'
        )->find($booking_id);

        if (!$booking) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.not_find_data"));
        }

        $model = $this->getModelObject($booking->model_type);
        $entity = $model->enable()->isOwner()->find($booking->model_id);

        if (!$entity) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.access_error"));
        }

        if (get_instance()->isAjax()) {
            $booking = $this->decorateAttributes($booking);
        }

        return $this->setDataResponse($booking)->setResponse(true);

    }

    /**
     * @return $this
     */
    protected function saveBooking()
    {

        if (!($date = $this->request->get("date")) || !($time = $this->request->get("time")) || !($booking_calendar_id = $this->request->get("booking_calendar_id"))) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.mis_data"));
        }
        $date = DateHelper::setLocaleDateTime($date, trans('core::date.datetime.date_medium'))->getDateTime(trans('core::date.datetime.date_medium'));
        $this->request->offsetSet("date", $date);
        $bookingCalendar = BookingCalendar::active()->where("start_date", "<=", $date)->where("end_date", ">=", $date)
            ->where("entity_relation_service_id", $this->request->get("entity_relation_service_id"))->find($booking_calendar_id);
        if (!$bookingCalendar) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.not_find_data"));
        }

        $bookingCalendarViewModel = new BookingCalendarViewModel();
        $validDates = $bookingCalendarViewModel->setRequest($this->request)->getCalendar($booking_calendar_id, $this->request->get("entity_employee_id"), true);

        if (!$validDates) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.this_time_is_not_valid"));
        }

        $dateTime = date("Y-m-d H:i", strtotime($date . " " . $time));
        if (!isset($validDates[$dateTime])) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.this_time_is_not_valid"));
        }

        if (!($validDates[$dateTime]["valid"])) {
            return $this->redirectBack()->setResponse(false, trans("booking::messages.alert.this_time_is_completed"));
        }

        $result = $this->setEntity()->saveBookingItem();

        return $this->redirectBack()->setDataResponse($this->booking->bookingDetails)->setResponse(boolval($result), $this->message_booking, $this->errors_validation);
    }

}
