<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBookingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('bookings', function (Blueprint $table) {
            $table->enum('action', ["new", "do", "doing", "done", "cancel"])->after('accept_term')->nullable()->default("new");
        });

        Schema::table('booking_details', function (Blueprint $table) {
            $table->enum('action', ["new", "do", "doing", "done", "cancel"])->after('accept_term')->nullable()->default("new");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
