<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('booking_calendars', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_id');
            $table->unsignedInteger('entity_relation_service_id')->nullable();
            $table->enum('for_gender', ['female', 'male', 'all'])->default('all')->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->unsignedTinyInteger('days_active_reserve')->nullable()->comment('days of active for reserve after date');
            $table->unsignedTinyInteger('period')->nullable()->comment('time of service, minutes');
            $table->unsignedTinyInteger('count_reservation_in_time')->nullable()->comment('this value is true for reserve in period time');
            $table->mediumText('params')->nullable();
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('booking_calendars', function (Blueprint $table) {

            $table->foreign('entity_id')->references('id')->on('entities')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('entity_relation_service_id')->references('id')->on('entity_relation_services')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_calendars', function (Blueprint $table) {
            $table->dropForeign('booking_calendars_entity_relation_service_id_foreign');
            $table->dropForeign('booking_calendars_entity_id_foreign');
        });

        Schema::dropIfExists('booking_calendars');
    }
}
