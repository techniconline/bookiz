<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::table('bookings', function (Blueprint $table) {

            $table->unsignedTinyInteger('reservation_status')->after('accept_term')->nullable()->comment('1: accept, 2:entity pend, 3:user pend, 4:reject by entity, 5:reject by user');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
