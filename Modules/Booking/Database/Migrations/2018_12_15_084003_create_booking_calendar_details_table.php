<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingCalendarDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('booking_calendar_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('booking_calendar_id');
            $table->unsignedInteger('entity_employee_id')->nullable();
            $table->unsignedTinyInteger('count_reserved')->nullable()->comment('count of reserved in this period');
            $table->date('date')->nullable()->index();
            $table->time('time')->nullable()->index();
            $table->unsignedTinyInteger('reservation_status')->nullable()->comment('1: accept, 2:entity pend, 3:user pend, 4:reject by entity, 5:reject by user');
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('booking_calendar_details', function (Blueprint $table) {
            $table->foreign('entity_employee_id')->references('id')->on('entity_employees')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('booking_calendar_id')->references('id')->on('booking_calendars')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_calendar_details', function (Blueprint $table) {
            $table->dropForeign('booking_calendar_details_entity_employee_id_foreign');
            $table->dropForeign('booking_calendar_details_booking_calendar_id_foreign');
        });

        Schema::dropIfExists('booking_calendar_details');
    }
}
