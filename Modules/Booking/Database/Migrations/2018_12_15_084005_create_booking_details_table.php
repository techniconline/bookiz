<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('booking_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('booking_id');
            $table->unsignedInteger('bcd_id')->nullable();
            $table->unsignedInteger('entity_employee_id')->nullable();
            $table->unsignedInteger('entity_service_id')->nullable();
            $table->unsignedInteger('entity_relation_service_id')->nullable();
            $table->unsignedTinyInteger('by_owner_entity')->nullable()->comment('reserve by owner of entity');
            $table->float('amount',14,2)->nullable();
            $table->float('discount_amount',14,2)->nullable();
            $table->float('payable_amount',14,2)->nullable();
            $table->unsignedInteger('currency_id')->nullable();
            $table->date('date')->nullable()->index();
            $table->time('time')->nullable()->index();
            $table->mediumText('params')->nullable();
            $table->boolean('accept_term')->nullable();
            $table->boolean('viewed')->nullable();
            $table->tinyInteger('priority')->nullable();
            $table->unsignedTinyInteger('reservation_status')->nullable()->comment('1: accept, 2:entity pend, 3:user pend, 4:reject by entity, 5:reject by user');
            $table->tinyInteger('active')->default('1')->index()->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('booking_details', function (Blueprint $table) {

            $table->foreign('entity_service_id','bd_entity_service_id_foreign')->references('id')->on('entity_services')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('entity_relation_service_id','bd_entity_relation_service_id_foreign')->references('id')->on('entity_relation_services')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('bcd_id')->references('id')->on('booking_calendar_details')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('currency_id')->references('id')->on('currencies')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('booking_id')->references('id')->on('bookings')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('entity_employee_id')->references('id')->on('entity_employees')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_details', function (Blueprint $table) {
            $table->dropForeign('bd_entity_relation_service_id_foreign');
            $table->dropForeign('bd_entity_service_id_foreign');
            $table->dropForeign('booking_details_currency_id_foreign');
            $table->dropForeign('booking_details_bcd_id_foreign');
            $table->dropForeign('booking_details_booking_id_foreign');
            $table->dropForeign('booking_details_entity_employee_id_foreign');
        });

        Schema::dropIfExists('booking_details');
    }
}
