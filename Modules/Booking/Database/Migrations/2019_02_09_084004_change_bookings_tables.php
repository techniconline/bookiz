<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBookingsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropForeign('bookings_entity_id_foreign');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->unsignedInteger('model_id')->after('id')->nullable()->index();
            $table->string('model_type')->after('model_id')->nullable()->index()->comment('for entity or...');
        });

        Schema::table('booking_details', function (Blueprint $table) {
            $table->dropForeign('bd_entity_service_id_foreign');
            $table->dropForeign('bd_entity_relation_service_id_foreign');
            $table->dropForeign('booking_details_entity_employee_id_foreign');
        });

        Schema::table('booking_details', function (Blueprint $table) {
            $table->unsignedInteger('model_id')->after('booking_id')->nullable()->index();
            $table->string('model_type')->after('model_id')->nullable()->index()->comment('for entity_relation_service or...');
        });

        Schema::table('booking_calendars', function (Blueprint $table) {
            $table->dropForeign('booking_calendars_entity_relation_service_id_foreign');
            $table->dropForeign('booking_calendars_entity_id_foreign');
        });

        Schema::table('booking_calendars', function (Blueprint $table) {
            $table->unsignedInteger('model_id')->after('id')->nullable()->index();
            $table->string('model_type')->after('model_id')->nullable()->index()->comment('for entity_relation_service or...');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
