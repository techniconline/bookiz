<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBookingsTables1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('booking_details', function (Blueprint $table) {
            $table->dropColumn('by_owner_entity');
        });

        Schema::table('booking_details', function (Blueprint $table) {
            $table->boolean('by_owner')->after('model_type')->index()->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
