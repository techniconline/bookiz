<?php

namespace Modules\Booking\Sale;

use Modules\Booking\Models\Booking\BookingDetails;
use Modules\Booking\Traits\Booking\BookingDetailSaleTrait;
use Modules\Core\Patterns\Sale\Item\SaleItemPattern;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Traits\Decorate\DecorateData;
use Modules\Entity\ViewModels\Entity\Owner\EntityEmployeeViewModel;
use Modules\Sale\Models\Cart;

class BookingDetail extends SaleItemPattern
{

    use DecorateData;
    use BookingDetailSaleTrait;

    protected $type = 'booking_detail';
    protected $itemID = false;

    private $booking_detail = false;
    private $options = [];

    /**
     * @param $item_id
     * @return $this|mixed
     */
    public function setItemId($item_id)
    {
        $this->itemID = $item_id;
        $this->booking_detail = BookingDetails::active()->with('booking', 'bookingCalendarDetail', 'currency', 'entityEmployee.user')->find($item_id);
        return $this;
    }

    /**
     * @param $options
     * @return $this|mixed
     */
    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return bool
     */
    public function checkItemSale()
    {
        return true;
    }

    /**
     * @param $cart
     * @return bool
     */
    public function canSaleItemInCart($cart)
    {
//        if ($cart && $cart->cartItems) {
//            $cartItems = $cart->cartItems;
//            $items = $cartItems->pluck('item_id', 'item_type');
//            $values = $items->values();
//            $entityRelationServices = EntityRelationServices::active()->whereIn('id', $values)->get();
//            foreach ($entityRelationServices as $item) {
//                if ($this->booking_detail && $this->booking_detail->entity_id != $item->entity_id) {
//                    return false;
//                }
//            }
//        }
        return true;
    }

    /**
     * @return bool|mixed
     */
    public function canSaleItem()
    {
        if (!$this->booking_detail) {
            return false;
        }
        return $this->checkItemSale();
    }

    /**
     * @return int|mixed
     */
    public function getMaxQtyForSale()
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function getSaleItem()
    {
        $data = $this->getCartData($this->booking_detail);
        //$data['options']['cat_ids']=$this->getCategoryIds();
        return $data;
    }

    /**
     * @return int|mixed
     */
    public function getItemQty()
    {
        return 1;
    }

    /**
     * @param $order
     * @param $item
     * @return bool|mixed
     */
    public function setOrderComplete($order, $item)
    {
        if ($order && $item) {
            try {
                return $this->setOrderForUser($order);
            } catch (\Exception $exception) {
                report($exception);
            }

        }
        return false;
    }


    /**
     * @return array|mixed
     */
    public function getCategoryIds()
    {
        $cats = [];
        return $cats;
    }

    /**
     * @param $order
     * @return array
     */
    public function setOrderForUser($order)
    {
        $messages = [];
        $messages[] = trans('booking::booking.complete_order.send_order_to_entity', ['entity_name' => isset($this->booking_detail->booking->entity->title)?$this->booking_detail->booking->entity->title:'-']);

        return $messages;
    }

    /**
     * TODO
     * @return mixed
     * @throws \Throwable
     */
    public function getItemExtraData()
    {
        if (!$this->booking_detail) {
            return false;
        }

        if (!$this->booking_detail->booking->model_row) {
            return false;
        }

        $url = $this->booking_detail->booking->model_row->single_link;
        $service = $this->booking_detail->model_row;
        $employee = [];
        if (isset($this->booking_detail->entityEmployee->user) && $this->booking_detail->entityEmployee->user){
            $employee = [
                'entity_employee_id'=>$this->booking_detail->entityEmployee->id,
                'full_name'=>$this->booking_detail->entityEmployee->user->full_name,
                'user_id'=>$this->booking_detail->entityEmployee->user->id,
                'small_avatar_url'=>$this->booking_detail->entityEmployee->user->small_avatar_url,
            ];
        }

        $data = [
            'image' => $service->image,
            'icon' => $service->icon,
            'model_row_url' => $url,
            'model_row' => $this->booking_detail->booking->model_row,
            'employee' => $employee,
            'booking_detail' => $this->booking_detail,
            'booking' => $this->decorateAttributes($this->booking_detail->booking),
            'service' => $service,
            'calendar' => $this->booking_detail->bookingCalendarDetail,
            'currency' => $this->booking_detail->currency,
        ];
        return $data;
    }

}