<?php

namespace Modules\Booking\Traits\Booking;

use BridgeHelper;
use DateHelper;

trait BookingDetailSaleTrait
{
    /**
     * @param $booking_detail
     * @return array
     */
    public function getCartData($booking_detail)
    {
        $item = [
            'item_type' => 'booking_details',
            'item_id' => $booking_detail->id,
            'options' => [
                'booking_id' => $booking_detail->booking_id,
                'entity_service_id' => $booking_detail->entity_service_id,
                'entity_relation_service_id' => $booking_detail->entity_relation_service_id,
                'entity_employee_id' => $booking_detail->entity_employee_id,
            ],
            'cost' => $booking_detail->amount,
            'sale_cost' => $booking_detail->payable_amount,
            'text' => $this->getPriceText($booking_detail->price, $booking_detail->currency_id),
            'percent_discount' => $booking_detail->discount_amount ? (($booking_detail->discount_amount * 100) / $booking_detail->amount) : 0,
            'currency' => $booking_detail->currency_id,
        ];
        $item['name'] = $booking_detail->title;
        $item['options']['product_key'] = sha1('entity_' . $booking_detail->entity_id . '_booking_detail_' . $booking_detail->id);
        $item['qty'] = 1;
        return $item;
    }

    /**
     * @param $price
     * @param $currency
     * @return mixed
     */
    public function getPriceText($price, $currency)
    {
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($price, $currency, true);
    }

    /**
     * @param $user_id
     * @param $booking_detail_id
     * @param $course_id
     * @return mixed
     */
    public function addUserToActivity($user_id, $booking_detail_id, $course_id)
    {
//        $time = time() + 157680000;
//        $expireDate = DateHelper::setDateTime(date(trans('core::date.database.datetime'), $time), null, trans('core::date.database.datetime'))->getDateTime(trans('core::date.database.datetime'));
//        $userEnroll = new CourseUserEnrollment();
//        $userEnroll->user_id = $user_id;
//        $userEnroll->course_id = $course_id;
//        $userEnroll->booking_detail_id = $booking_detail_id;
//        $userEnroll->type = CourseUserEnrollment::TYPE_ACTIVITY;
//        $userEnroll->active_date = date(trans('core::date.database.datetime'), time());
//        $userEnroll->expire_date = $expireDate;
//        $userEnroll->active = 1;
//        return $userEnroll->save();
    }
}
