<?php

namespace Modules\Booking\Traits\Booking;

use BridgeHelper;
use Illuminate\Support\Facades\Lang;
use Modules\Booking\Models\Booking;
use Modules\Booking\Models\Calendar\BookingCalendarDetails;
use Modules\Core\Models\Currency\Currency;
use Modules\Entity\Models\Entity\EntityEmployee;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\Models\Service\EntityServices;

trait BookingDetails
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [

        ];
        return $this;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsOwner($query)
    {
        return $query->join('bookings as b', 'b.id', $this->getTable() . '.booking_id')
            ->where('b.user_id', auth()->user()->id)->where('b.active', 1)
            ->whereNull('b.deleted_at')->select($this->getTable() . ".*");
    }

    /**
     * @param $query
     * @param $from_date
     * @param $to_date
     * @return mixed
     */
    public function scopeFilterDateDetails($query, $from_date, $to_date)
    {
        if ($from_date) {
            $query->where('date', '>=', $from_date);
        }

        if ($to_date) {
            $query->where('date', '<=', $to_date);
        }
        return $query;
    }

    /**
     * @param $query
     * @param string $from_time
     * @param string $to_time
     * @return mixed
     */
    public function scopeFilterTimeDetails($query, $from_time = '00:00:00', $to_time = '23:59:59')
    {
        if ($from_time) {
            $query->where('time', '>=', $from_time);
        }

        if ($to_time) {
            $query->where('time', '<=', $to_time);
        }
        return $query;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getReservationStatusTextAttribute()
    {
        if ($this->reservation_status && Lang::has("booking::booking.reservation_status_text." . $this->getAttribute("reservation_status"))) {
            return trans("booking::booking.reservation_status_text." . $this->getAttribute("reservation_status"));
        }
        return $this->reservation_status;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getActionTextAttribute()
    {
        if ($this->action) {
            return trans("booking::booking.actions_text." . $this->getAttribute("action"));
        }
        return $this->action;
    }

    /**
     * @return array|string
     */
    public function getBookTimeAttribute()
    {
        $bookTime = [];
        if ($this->date) {
            $bookTime[] = $this->date;
        }

        if ($this->time) {
            $bookTime[] = $this->time;
        }
        $bookTime = implode(" ", $bookTime);
        $bookTime = $bookTime ? date(trans('core::date.datetime.short'), strtotime($bookTime)) : null;
        return $bookTime;
    }

    /**
     * @return false|null|string
     */
    public function getEndBookTimeAttribute()
    {
        $period = isset($this->params_json->period) ? $this->params_json->period : 0;

        if (!$period && $this->bcd_id) {
            $bookingCalendar = $this->bookingCalendarDetail->bookingCalendar;
            $period = $bookingCalendar->period;
        }

        if ($period && $this->book_time) {
            $date = date(trans('core::date.datetime.short'), strtotime($this->book_time . ' +' . $period . ' minutes'));
            return $date;
        }
        return null;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsOpen($query)
    {
        return $query->whereIn($this->getTable() . '.reservation_status', [self::RESERVATION_STATUS_ENTITY_PEND, self::RESERVATION_STATUS_USER_PEND]);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsOpenUser($query)
    {
        return $query->whereIn($this->getTable() . '.reservation_status', [self::RESERVATION_STATUS_USER_PEND]);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsPendEntity($query)
    {
        return $query->whereIn($this->getTable() . '.reservation_status', [self::RESERVATION_STATUS_ENTITY_PEND]);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsAcceptEntity($query)
    {
        return $query->whereIn($this->getTable() . '.reservation_status', [self::RESERVATION_STATUS_ACCEPT]);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeForEntityOwner($query)
    {
        return $query->whereIn($this->getTable() . '.reservation_status', [self::RESERVATION_STATUS_ACCEPT, self::RESERVATION_STATUS_ENTITY_PEND]);
    }

    /**
     * @return mixed
     */
    public function getParamsJsonAttribute()
    {
        $value = $this->params;
        if ($value) {
            $value = json_decode($value);
        }
        return $value;
    }

    /**
     * @return mixed
     */
    public function getParamsArrayAttribute()
    {
        $value = $this->params;
        if ($value) {
            $value = json_decode($value, true);
        }
        return $value;
    }


    /**
     * @param $val
     */
    public function setParamsAttribute($val)
    {
        if (!is_array($val) && !is_object($val)) {
            $this->attributes['params'] = $val;

        } else {
            $this->attributes['params'] = json_encode($val);
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | null
     */
    public function getModelRowAttribute()
    {
        $class = $this->getClassObject($this->model_type);
        if (!$class) {
            return null;
        }

        return $this->belongsTo($class, 'model_id')->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityRelationService()
    {
        return $this->belongsTo(EntityRelationServices::class, 'model_id');
    }

//    /**
//     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
//     */
//    public function entityService()
//    {
//        return $this->belongsTo(EntityServices::class);
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bookingCalendarDetail()
    {
        return $this->belongsTo(BookingCalendarDetails::class, 'bcd_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityEmployee()
    {
        return $this->belongsTo(EntityEmployee::class);
    }

}
