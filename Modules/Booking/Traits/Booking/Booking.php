<?php

namespace Modules\Booking\Traits\Booking;

use BridgeHelper;
use Illuminate\Support\Facades\Lang;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Entity\Models\Entity;
use Modules\Sale\Models\Order;
use Modules\User\Models\UserAddress;
use Modules\User\Models\User;
use Modules\Booking\Models\Booking\BookingDetails;

trait Booking
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [

        ];
        return $this;
    }

    /**
     * @param null $key
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getReasonsRejectByEntity($key = null)
    {
        if (!$key) {
            return trans("booking::booking.reasons_reject_entity");
        }
        if (Lang::has("booking::booking.reasons_reject_entity." . $key)) {
            return trans("booking::booking.reasons_reject_entity." . $key);
        }
        return null;
    }

    /**
     * @param null $key
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getReasonsRejectByUser($key = null)
    {
        if (!$key) {
            return trans("booking::booking.reasons_reject_user");
        }
        if (Lang::has("booking::booking.reasons_reject_user." . $key)) {
            return trans("booking::booking.reasons_reject_user." . $key);
        }
        return null;
    }

    /**
     * @param $query
     * @param null $user
     * @return mixed
     */
    public function scopeOwner($query, $user = null)
    {
        if (!$user) {
            $user = BridgeHelper::getAccess()->getUser();
        }

        $query = $query->where(function ($q) use ($user) {
            if ($user) {
                $q->where($this->getTable() . ".user_id", $user->id);
            } else {
                $q->where($this->getTable() . ".user_id", 0);
            }
        });
        return $query;
    }

    /**
     * @param $query
     * @param $entity_id
     * @return mixed
     */
    public function scopeOwnerEntity($query, $entity_id)
    {
        $entity = BridgeHelper::getEntity()->getEntityModel()->enable()->isOwner()->find($entity_id);
        $entity_id = $entity ? $entity->id : 0;
        $query = $query->where($this->getTable() . ".entity_id", $entity_id);
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsOpen($query)
    {
        return $query->whereIn($this->getTable() . '.reservation_status', [self::RESERVATION_STATUS_ENTITY_PEND, self::RESERVATION_STATUS_USER_PEND]);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsOpenUser($query)
    {
        return $query->whereIn($this->getTable() . '.reservation_status', [self::RESERVATION_STATUS_USER_PEND]);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsAccept($query)
    {
        return $query->whereIn($this->getTable() . '.reservation_status', [self::RESERVATION_STATUS_ACCEPT]);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeForEntityOwner($query)
    {
        return $query->whereIn($this->getTable() . '.reservation_status', [self::RESERVATION_STATUS_ACCEPT,self::RESERVATION_STATUS_ENTITY_PEND]);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsPendEntity($query)
    {
        return $query->whereIn($this->getTable() . '.reservation_status', [self::RESERVATION_STATUS_ENTITY_PEND]);
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getReservationStatusTextAttribute()
    {
        if ($this->reservation_status && Lang::has("booking::booking.reservation_status_text." . $this->getAttribute("reservation_status"))) {
            return trans("booking::booking.reservation_status_text." . $this->getAttribute("reservation_status"));
        }
        return $this->reservation_status;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getActionTextAttribute()
    {
        if ($this->action) {
            return trans("booking::booking.actions_text." . $this->getAttribute("action"));
        }
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function getParamsJsonAttribute()
    {
        $value = $this->params;
        if ($value) {
            $value = json_decode($value);
        }
        return $value;
    }

    /**
     * @return mixed
     */
    public function getParamsArrayAttribute()
    {
        $value = $this->params;
        if ($value) {
            $value = json_decode($value, true);
        }
        return $value;
    }

    /**
     * @return |null
     */
    public function getOrderAttribute()
    {
        if (isset($this->params_json->order_id)){
            $order = Order::find($this->params_json->order_id);
            return $order;
        }
        return null;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | null
     */
    public function getModelRowAttribute()
    {
        $class = $this->getClassObject($this->model_type);
        if (!$class) {
            return null;
        }

        return $this->belongsTo($class, 'model_id')->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class, 'model_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userAddress()
    {
        return $this->belongsTo(UserAddress::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookingDetails()
    {
        return $this->hasMany(BookingDetails::class);
    }

}
