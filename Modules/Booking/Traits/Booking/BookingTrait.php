<?php

namespace Modules\Booking\Traits\Booking;

use BridgeHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Booking\Jobs\BookingJob;
use Modules\Booking\Models\Calendar\BookingCalendar;
use Modules\Booking\Models\Calendar\BookingCalendarDetails;
use Modules\Booking\ViewModels\Calendar\Owner\BookingCalendarViewModel;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Entity\Models\Entity;
use Modules\Booking\Models\Booking\BookingDetails;
use Modules\Entity\Models\Service\EntityRelationServices;
use Modules\Entity\Traits\Entity\EntityProductTrait;
use Modules\Core\Traits\ModelTrait;

trait BookingTrait
{
    use EntityProductTrait;
    use ModelTrait;

    public $booking;
    public $booking_details;
    public $user;
    public $owner_entity;
    public $entity;
    public $errors_validation;
    public $message_booking;
    private $request_items = [];

    /**
     * set items in request
     */
    private function initBookingTrait()
    {
        request()->request->add($this->request_items);
    }

    /**
     * @param array $request_items
     * @return $this
     */
    public function setDataRequest($request_items = [])
    {
        if (!$this->request_items) {
            $this->request_items = $request_items;
        }
        return $this;
    }

    /**
     * @param null $key
     * @return array|null
     */
    private function getRequestItem($key = null)
    {
        if ($key) {
            if (isset($this->request_items[$key])) {
                return $this->request_items[$key];
            }
            return null;
        }
        return $this->request_items;
    }

    /**
     * @param null $entity
     * @return $this
     */
    public function setEntity($entity = null)
    {
        $this->initBookingTrait();
        if ($this->entity) {
            return $this;
        }

        if ($entity) {
            $this->entity = $entity;
        } else {
            $entity_id = request()->get("model_id");
            $model_type = request()->get("model_type");

            //TODO remove
//            $entity_id = $entity_id?:request()->get("entity_id");

            if (!$entity_id) {
                return $this;
            }

            $model = $this->getModelObject($model_type);
            $this->entity = $model->enable()->find($entity_id);
        }

        return $this;
    }

    /**
     * @param null $user_id
     * @return $this
     */
    public function getOpenBooking($user_id = null)
    {
        $this->setEntity();
        $this->setUserBooking($user_id);
        if (!$this->user) {
            return $this;
        }


        $this->booking = \Modules\Booking\Models\Booking::active()->isOpenUser()->where("user_id", $this->user->id)->where(function ($q) {
            if ($this->entity) {
//                $q->where("entity_id", $this->entity->id);
                $q->where("model_id", $this->entity->id);
                $q->where("model_type", $this->entity->getTable());
            }
        })->orderBy("id", "DESC")->first();

        return $this;
    }

    /**
     * @param null $user_id
     * @return $this
     */
    private function setUserBooking($user_id = null)
    {
        if ($this->user) {
            return $this;
        }

        if ($user_id) {
            $user = BridgeHelper::getUser()->getUserModel()->enable()->find($user_id);
        } else {
            $user = BridgeHelper::getAccess()->getUser();
        }

        $this->user = $user;
        return $this;
    }

    /**
     * @param $user_id
     * @param null $entity_id
     * @return bool
     */
    public function saveBookingItem($entity_id = null, $user_id = null)
    {
        $this->getOpenBooking($user_id);
        DB::beginTransaction();
        if ($this->booking) {
            $res = $this->saveBookingDetails();
        } else {
            if (!$entity_id) {
                $entity_id = $this->entity->id;
            }
            if (!$user_id) {
                $user_id = $this->user->id;
            }
            $res = $this->storeBooking($user_id, $entity_id);
        }
        DB::commit();

        if (!$res) {
            DB::rollBack();
        }
        return $res;
    }

    /**
     * @param $model_id
     * @param $model_type
     * @param $booking_calendar_id
     * @param $date
     * @param $time
     * @param null $entity_employee_id
     * @return bool
     */
    private function checkBookAction($model_id, $model_type, $booking_calendar_id, $date, $time, $entity_employee_id = null)
    {
        $orgDate = $date;
        $date = DateHelper::setLocaleDateTime($date, trans('core::date.datetime.date_medium'))->getDateTime(trans('core::date.datetime.date_medium'));
        $bookingCalendar = BookingCalendar::active()->where("start_date", "<=", $date)->where("end_date", ">=", $date)
            ->where("model_id", $model_id)
            ->where("model_type", $model_type)
            ->find($booking_calendar_id);

        if (!$bookingCalendar) {
            $this->message_booking = trans("booking::messages.alert.not_find_data");
            return false;
        }

        $bookingCalendarViewModel = new BookingCalendarViewModel();
        $validDates = $bookingCalendarViewModel->setRequest(request())->getCalendar($booking_calendar_id, $entity_employee_id, true, false, $orgDate);
        if (!$validDates) {
            $this->message_booking = trans("booking::messages.alert.this_time_is_not_valid");
            return false;
        }

        $dateTime = date("Y-m-d H:i", strtotime($date . " " . $time));
        $dateArr = explode(" ", $dateTime);
        $date = $dateArr[0];
        $time = $dateArr[1];
        if (!isset($validDates[$date][$time])) {
            $this->message_booking = trans("booking::messages.alert.this_time_is_not_valid");
            return false;
        }

        if (!($validDates[$date][$time]["valid"])) {
            $this->message_booking = trans("booking::messages.alert.this_time_is_completed");
            return false;
        }

        return true;
    }

    /**
     * @param null $model_type
     * @param null $model_id
     * @param null $booking_calendar_id
     * @param null $entity_relation_service_id
     * @param null $entity_service_id
     * @param null $date
     * @param null $time
     * @param null $entity_employee_id
     * @param null $params
     * @return bool
     */
    public function saveBookingDetail($model_type = null, $model_id = null, $booking_calendar_id = null, $date = null, $time = null, $entity_employee_id = null, $params = null)
    {
        if ($this->booking) {

            try {

                $model = new BookingDetails();
                $data = request()->all();
                $data['booking_id'] = $this->booking->id;
                $data['by_owner'] = request()->get("by_owner");

                if ($model_id && $model_type) {
                    $data['model_id'] = $model_id;
                    $data['model_type'] = $model_type;
                    $data['entity_employee_id'] = $entity_employee_id;
//                    $data['entity_service_id'] = $entity_service_id;
//                    $data['entity_relation_service_id'] = $entity_relation_service_id;
                    $data['date'] = $date;
                } else {
                    $entity_employee_id = $data['entity_employee_id'] = $entity_employee_id ?: request()->get("entity_employee_id");

//                    $entity_service_id = $data['entity_service_id'] = request()->get("entity_service_id");
//                    $entity_relation_service_id = $data['entity_relation_service_id'] = request()->get("entity_relation_service_id");

                    $model_id = $data['model_id'] = $model_id ?: request()->get("model_id");
                    $model_type = $data['model_type'] = $model_type ?: request()->get("model_type");
                    $date = $data['date'] = $date ?: request()->get("date");
                    $time = $time ?: request()->get("time");
                }

                $booking_calendar_id = $booking_calendar_id ?: request()->get("booking_calendar_id");
                $bookAction = $this->checkBookAction($model_id, $model_type, $booking_calendar_id, $date, $time, $entity_employee_id);

                if (!$bookAction) {
                    return false;
                }

                $date = DateHelper::setLocaleDateTime($date, trans('core::date.datetime.date_medium'))->getDateTime(trans('core::date.datetime.date_medium'));
                $data['date'] = $date;
                $data['time'] = date("H:i:s", strtotime($time));

                if ($this->booking) {
                    $lastRow = BookingDetails::active()->where("booking_id", $this->booking->id)->orderBy("id", "DESC")->first();
                    if ($lastRow) {
                        $lastDate = $lastRow->date;
                        if ($lastDate != $date) {
                            $value = DateHelper::setDateTime($lastDate)->getLocaleFormat(trans('core::date.datetime.date_medium'));
                            $this->errors_validation = ["date" => [trans("booking::messages.alert.not_valid_date", ["date" => $value])]];
                            return false;
                        }
                    }
                }

                $data["action"] = $model::ACTION_NEW;
                $params = $params ?: request()->get("params");
                $data['params'] = $params ? json_encode($params) : null;
                $data['reservation_status'] = BookingDetails::RESERVATION_STATUS_USER_PEND;

                if (!$model_type || !$model_id || !$booking_calendar_id || !$date || !$time) {
                    $this->message_booking = trans("booking::messages.alert.mis_data");
                    return false;
                }

                $resAmount = $this->getAmountOfService($model_type, $model_id);

                $data = array_merge($data, $resAmount);
                $data['payable_amount'] = $data['amount'] - $data['discount_amount'];

                $rules = $model->rules;
                $messages = $model->messages;
                $validation = Validator::make($data, $rules, $messages);
                if ($validation->fails()) {
                    $this->errors_validation = $validation->messages();
                    return false;
                }

                $data['bcd_id'] = $this->saveBookingCalendarTime($booking_calendar_id, $data['date'], $data['time'], $data['entity_employee_id']);
                if (!$data['bcd_id']) {
                    return false;
                }

                $res = $model->fill($data)->save();
                $this->message_booking = trans("booking::messages.alert.save_success");
                return boolval($res);
            } catch (\Exception $exception) {
                report($exception);
                $this->message_booking = trans("booking::messages.alert.save_un_success");
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * request booking_details=[
     *      0=>[
     *          booking_calendar_id=>value,
     *          entity_relation_service_id =>value,
     *          entity_service_id=>value,
     *          date=>value,
     *          time=>value,
     *          entity_employee_id=>value,
     *          params=>value,
     *          ]
     * ]
     *
     */
    public function saveBookingDetails()
    {
        $booking_details = request()->get("booking_details");
        if (!$booking_details) {
            return $this->saveBookingDetail();
        }

        $book = false;
        foreach ($booking_details as $booking_detail) {
            if ($this->checkValidationBookingsDetailData($booking_detail)) {
                $params = isset($booking_detail["params"]) ? $booking_detail["params"] : null;

//                $entity_relation_service_id = isset($booking_detail["entity_relation_service_id"]) ? $booking_detail["entity_relation_service_id"] : null;
//                $entity_service_id = isset($booking_detail["entity_service_id"]) ? $booking_detail["entity_service_id"] : null;

                $booking_calendar_id = isset($booking_detail["booking_calendar_id"]) ? $booking_detail["booking_calendar_id"] : null;
                $entity_employee_id = isset($booking_detail["entity_employee_id"]) ? $booking_detail["entity_employee_id"] : null;
                $date = isset($booking_detail["date"]) ? $booking_detail["date"] : null;
                $time = isset($booking_detail["time"]) ? $booking_detail["time"] : null;

                $model_id = isset($booking_detail["model_id"]) ? $booking_detail["model_id"] : null;
                $model_type = isset($booking_detail["model_type"]) ? $booking_detail["model_type"] : null;

                $res = $this->saveBookingDetail($model_type, $model_id, $booking_calendar_id, $date, $time, $entity_employee_id, $params);
                if ($res && !$book) {
                    $book = true;
                }
            }
        }

        if ($book) {
            $this->message_booking = trans("booking::messages.alert.save_success");
            return true;
        }

        $this->message_booking = trans("booking::messages.alert.save_un_success");
        return false;
    }

    /**
     * @param $booking_detail
     * @return bool
     */
    private function checkValidationBookingsDetailData($booking_detail)
    {
        $required = ['model_id', 'model_type', 'time', 'date'
//            , 'entity_service_id', 'entity_relation_service_id'
            , 'booking_calendar_id'];
        $keys = array_keys($booking_detail);
        foreach ($required as $item) {
            if (!in_array($item, $keys)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $model_type
     * @param $model_id
     * @return array
     */
    private function getAmountOfService($model_type, $model_id)
    {
        $model = $this->getModelObject($model_type);
        $model_item = $model->active()->find($model_id);
        $amount = $model_item ? $model_item->price : 0;
        $discount = $this->getDiscount($model_item);
        return ['amount' => $amount, 'discount_amount' => $discount];
    }

    /**
     * @param $model_item
     * @return int
     */
    private function getDiscount($model_item)
    {
        $discount = $model_item ? $model_item->discount : 0;
        return $discount;
    }

    /**
     * @param $booking_calendar_id
     * @param $date
     * @param $time
     * @param null $entity_employee_id
     * @return mixed
     */
    private function saveBookingCalendarTime($booking_calendar_id, $date, $time, $entity_employee_id = null)
    {
        $bookingCalendarDetails = BookingCalendarDetails::active()->where('booking_calendar_id', $booking_calendar_id)->where('date', $date)
            ->where('time', $time)->where(function ($q) use ($entity_employee_id) {
                if ($entity_employee_id) {
                    $q->where('entity_employee_id', $entity_employee_id);
                } else {
                    $q->whereNull('entity_employee_id');
                }
            })->first();

        if (!$bookingCalendarDetails) {
            $bookingCalendarDetails = new BookingCalendarDetails();
            $bookingCalendarDetails->booking_calendar_id = $booking_calendar_id;
            $bookingCalendarDetails->entity_employee_id = $entity_employee_id;
            $bookingCalendarDetails->date = $date;
            $bookingCalendarDetails->time = $time;
            $count = 1;
        } else {
            $count = $bookingCalendarDetails->count_reserved;
            $count += 1;
        }

        $bookingCalendarDetails->count_reserved = $count;
        $res = $bookingCalendarDetails->save();
        if ($res) {
            return $bookingCalendarDetails->id;
        }

        return false;

    }

    /**
     * @param $user_id
     * @param $entity_id
     * @return bool
     */
    public function storeBooking($user_id, $entity_id)
    {
        try {
            $data = request()->all();
            if (!$this->booking) {
                $model = new \Modules\Booking\Models\Booking();
                $data["user_id"] = request()->get("user_id", $user_id);

//                $data["entity_id"] = request()->get("entity_id", $entity_id);

                $data["model_id"] = request()->get("model_id", $entity_id);
                $data["model_type"] = request()->get("model_type", $entity_id);

                $data["reservation_status"] = $model::RESERVATION_STATUS_USER_PEND; //user_pend
                $data["action"] = $model::ACTION_NEW;
                $data["active"] = 1;
            } else {
                $model = $this->booking;
            }
            $data["user_address_id"] = request()->get("user_address_id");
            $params = request()->get("params");
            $data["params"] = json_encode($params);

            if (!$this->booking) {
                $rules = $model->rules;
                $messages = $model->messages;
                $validation = Validator::make($data, $rules, $messages);
                if ($validation->fails()) {
                    $this->errors_validation = $validation->messages();
                    return false;
                }
            }

            $existBooking = \Modules\Booking\Models\Booking::active()->isOpenUser()->where("user_id", $this->user->id)->orderBy("id", "DESC")->first();

            if ($existBooking && ($data["model_id"] != $existBooking->entity_id)) {
                $this->errors_validation = ["entity" => [trans("booking::messages.alert.has_open_booking")]];
                return false;
            }

            $model->fill($data);
            $res = $model->save();
            $this->booking = $model;
            $res = $this->saveBookingDetails();
            return boolval($res);
        } catch (\Exception $exception) {
            report($exception);
            $this->message_booking = trans("booking::messages.alert.error");
            return false;
        }

    }

    /**
     * @param $booking_id
     * @return bool
     */
    public function acceptUserBooking($booking_id)
    {

        try {
            $booking = (new \Modules\Booking\Models\Booking())->active()->isOpenUser()->owner()->with("user")->find($booking_id);
            if (!$booking) {
                $this->message_booking = trans("booking::messages.alert.not_find_data");
                return false;
            }

            $params = [];
            $params["note"] = request()->get("note", '');
            $params_array = $booking->params_array;
            $params = is_array($params_array) && $params_array ? array_merge($params, $params_array) : $params;
            $params = json_encode($params);

            $data = [
                'reservation_status' => \Modules\Booking\Models\Booking::RESERVATION_STATUS_ENTITY_PEND,
                'params' => $params,
                'accept_term' => 1,
            ];

            $booking->fill($data);
            $booking->save();
            $this->updateBookingDetails($booking, \Modules\Booking\Models\Booking::RESERVATION_STATUS_ENTITY_PEND);
            $this->message_booking = trans("booking::messages.alert.accept_booking_by_user");
            dispatch(new BookingJob($booking->toArray()))->onQueue('high');
            return true;
        } catch (\Exception $exception) {
            report($exception);
            $this->message_booking = trans("booking::messages.alert.error");
            return false;
        }

    }

    /**
     * @param $booking_id
     * @return bool
     */
    public function acceptEntityBooking($booking_id)
    {
        try {
            $booking = (new \Modules\Booking\Models\Booking())->active()->isPendEntity()->with("user")->find($booking_id);
            if (!$booking) {
                $this->message_booking = trans("booking::messages.alert.not_find_data");
                return false;
            }

            $model = $this->getModelObject($booking->model_type);
            $model = $model->isOwner()->active()->find($booking->model_id);
            if (!$model) {
                $this->message_booking = trans("booking::messages.alert.access_error");
                return false;
            }

            $params = [];
            $params["note_entity"] = request()->get("note");
            $params_array = $booking->params_array;
            $params = is_array($params_array) && $params_array ? array_merge($params, $params_array) : $params;
            $params = json_encode($params);
            $data = [
                'reservation_status' => \Modules\Booking\Models\Booking::RESERVATION_STATUS_ACCEPT,
                'params' => $params,
            ];
            $data["action"] = \Modules\Booking\Models\Booking::ACTION_DO;

            $booking->fill($data);
            $booking->save();
            $this->updateBookingDetails($booking, \Modules\Booking\Models\Booking::RESERVATION_STATUS_ACCEPT, 1, null, \Modules\Booking\Models\Booking::ACTION_DO);
            dispatch(new BookingJob($booking->toArray()))->onQueue('high');
            $this->message_booking = trans("booking::messages.alert.accept_booking_by_entity", ["user_name" => $booking->user->full_name]);
            return true;
        } catch (\Exception $exception) {
            $this->message_booking = trans("booking::messages.alert.error");
            report($exception);
            return false;
        }
    }

    /**
     * @param $booking_id
     * @return bool
     */
    public function doneEntityBooking($booking_id)
    {
        try {
            $booking = (new \Modules\Booking\Models\Booking())->active()->isAccept()->with("user")->find($booking_id);
            if (!$booking) {
                $this->message_booking = trans("booking::messages.alert.not_find_data");
                return false;
            }

            $model = $this->getModelObject($booking->model_type);
            $model = $model->isOwner()->active()->find($booking->model_id);
            if (!$model) {
                $this->message_booking = trans("booking::messages.alert.access_error");
                return false;
            }

            $params = [];
            $params["note_done_booking"] = request()->get("note");
            $params_array = $booking->params_array;
            $params = is_array($params_array) && $params_array ? array_merge($params, $params_array) : $params;
            $params = json_encode($params);
            $data = [
                'params' => $params,
            ];
            $data["action"] = \Modules\Booking\Models\Booking::ACTION_DONE;

            $booking->fill($data);
            $booking->save();
            $this->updateBookingDetails($booking, \Modules\Booking\Models\Booking::RESERVATION_STATUS_ACCEPT, 1, null, \Modules\Booking\Models\Booking::ACTION_DONE);
            dispatch(new BookingJob($booking->toArray()))->onQueue('high');
            $this->message_booking = trans("booking::messages.alert.done_booking_by_entity", ["user_name" => $booking->user->full_name]);
            return true;
        } catch (\Exception $exception) {
            $this->message_booking = trans("booking::messages.alert.error");
            report($exception);
            return false;
        }
    }

    /**
     * @param $booking_id
     * @return bool
     */
    public function doingEntityBooking($booking_id)
    {
        try {
            $booking = (new \Modules\Booking\Models\Booking())->active()->isAccept()->with("user")->find($booking_id);
            if (!$booking) {
                $this->message_booking = trans("booking::messages.alert.not_find_data");
                return false;
            }

            $model = $this->getModelObject($booking->model_type);
            $model = $model->isOwner()->active()->find($booking->model_id);
            if (!$model) {
                $this->message_booking = trans("booking::messages.alert.access_error");
                return false;
            }

            $params = [];
            $params["note_doing_booking"] = request()->get("note");
            $params_array = $booking->params_array;
            $params = is_array($params_array) && $params_array ? array_merge($params, $params_array) : $params;
            $params = json_encode($params);
            $data = [
                'params' => $params,
            ];
            $data["action"] = \Modules\Booking\Models\Booking::ACTION_DOING;

            $booking->fill($data);
            $booking->save();
            $this->updateBookingDetails($booking, \Modules\Booking\Models\Booking::RESERVATION_STATUS_ACCEPT, 1, null, \Modules\Booking\Models\Booking::ACTION_DOING);
            dispatch(new BookingJob($booking->toArray()))->onQueue('high');
            $this->message_booking = trans("booking::messages.alert.doing_booking_by_entity", ["user_name" => $booking->user->full_name]);
            return true;
        } catch (\Exception $exception) {
            $this->message_booking = trans("booking::messages.alert.error");
            report($exception);
            return false;
        }
    }

    /**
     * @param $booking
     * @param int $reservation_status
     * @param int $active
     * @param null $booking_detail_id
     * @param null $action
     * @return mixed
     */
    public function updateBookingDetails($booking, $reservation_status = 2, $active = 1, $booking_detail_id = null, $action = null)
    {

        $reservation_statuses = [BookingDetails::RESERVATION_STATUS_ACCEPT, BookingDetails::RESERVATION_STATUS_ENTITY_PEND, BookingDetails::RESERVATION_STATUS_USER_PEND];

        $bookingDetails = BookingDetails::active()
            ->whereIn('reservation_status', $reservation_statuses)
            ->where("booking_id", $booking->id)
            ->where(function ($q) use ($booking_detail_id) {
                if ($booking_detail_id) {
                    $q->where("id", $booking_detail_id);
                }
            })->get();

        $data = [
            "reservation_status" => $reservation_status,
            "active" => $active,
        ];

        $arrRejectStatus = [BookingDetails::RESERVATION_STATUS_ENTITY_REJECT, BookingDetails::RESERVATION_STATUS_USER_REJECT];
        foreach ($bookingDetails as $bookingDetail) {
            $bookingDetail->fill($data);
            if ($action) {
                $bookingDetail->action = $action;
            }
            $bookingDetail->save();
            if ($action && BookingDetails::ACTION_DONE && $bookingDetail->action != BookingDetails::ACTION_DONE) {
                $this->updateProductService($bookingDetail);
            }
            if (in_array($bookingDetail->reservation_status, $arrRejectStatus)) {
                $this->removeBookingCalendarDetails($bookingDetail);
            }
        }

        return $bookingDetails;
    }

    /**
     * @param $bookingDetail
     * @return bool
     */
    public function updateProductService($bookingDetail)
    {
        $entity_relation_service_id = $bookingDetail->model_id;
        $productsService = Entity\EntityProductService::active()->with('entityProduct')->where("entity_relation_service_id", $entity_relation_service_id)->get();

        if (!$productsService){
            return false;
        }

        $used_data = [];
        foreach ($productsService as $item) {
            $entityProduct = $item->entityProduct;
            if ($item->used && $entityProduct) {
                $resProduct = $this->usedProduct($item->entity_product_id, $item->used);
                if ($resProduct) {
                    $used_data['products_used'][] = ["entity_product_id" => $item->entity_product_id, "used" => $item->used];
                }
            }
        }

        if (empty($used_data)) {
            return false;
        }

        $params = $bookingDetail->params_array;
        $params = array_merge($params, $used_data);
        $bookingDetail->params = json_encode($params);
        return $bookingDetail->save();
    }

    /**
     * @param $booking_details_id
     * @return bool
     */
    public function rejectEntityBookingDetails($booking_details_id)
    {
        try {
            $bookingDetail = (new BookingDetails())->active()->isOpen()->with("booking.user")->find($booking_details_id);
            if (!$bookingDetail) {
                $this->message_booking = trans("booking::messages.alert.not_find_data");
                return false;
            }

            $model = $this->getModelObject($bookingDetail->booking->model_type);
            $model = $model->isOwner()->active()->find($bookingDetail->booking->model_id);
            if (!$model) {
                $this->message_booking = trans("booking::messages.alert.access_error");
                return false;
            }

            $params = [];
            $params["reason_reject_entity"] = request()->get("reason_reject");
            $params["reason_note_reject_entity"] = request()->get("reason_note_reject");
            $params_array = $bookingDetail->params_array;
            $params = is_array($params_array) && $params_array ? array_merge($params, $params_array) : $params;
            $params = json_encode($params);
            $data = [
                'reservation_status' => BookingDetails::RESERVATION_STATUS_ENTITY_REJECT,
                'params' => $params,
                'active' => 2,
            ];
            $data["action"] = BookingDetails::ACTION_CANCEL;

            $bookingDetail->fill($data);
            $bookingDetail->save();
            $this->removeBookingCalendarDetails($bookingDetail);
            dispatch(new BookingJob($bookingDetail->toArray()))->onQueue('high');
            $this->message_booking = trans("booking::messages.alert.reject_booking_details_by_entity", [
                "service" => $bookingDetail->model_row->title,
                "user_name" => $bookingDetail->booking->user->full_name]);
        } catch (\Exception $exception) {
            report($exception);
            $this->message_booking = trans("booking::messages.alert.error");
            return false;
        }
    }

    /**
     * @param $booking_details_id
     * @return bool
     */
    public function rejectUserBookingDetails($booking_details_id)
    {
        try {
            $bookingDetail = (new BookingDetails())->active()->isOwner()->isOpen()->with("booking.user")->find($booking_details_id);
            if (!$bookingDetail) {
                $this->message_booking = trans("booking::messages.alert.not_find_data");
                return false;
            }

            $params = [];
            $params["reason_reject"] = request()->get("reason_reject");
            $params["reason_note_reject"] = request()->get("reason_note_reject");
            $params_array = $bookingDetail->params_array;
            $params = is_array($params_array) && $params_array ? array_merge($params, $params_array) : $params;
            $params = json_encode($params);
            $data = [
                'reservation_status' => BookingDetails::RESERVATION_STATUS_USER_REJECT,
                'params' => $params,
                'active' => 2,
            ];
            $data["action"] = BookingDetails::ACTION_CANCEL;

            $bookingDetail->fill($data);
            $bookingDetail->save();
            $this->removeBookingCalendarDetails($bookingDetail);
            dispatch(new BookingJob($bookingDetail->toArray()))->onQueue('high');
            $this->message_booking = trans("booking::messages.alert.reject_booking_details_by_entity", [
                "service" => $bookingDetail->model_row->title,
                "user_name" => $bookingDetail->booking->user->full_name
            ]);
        } catch (\Exception $exception) {
            report($exception);
            $this->message_booking = trans("booking::messages.alert.error");
            return false;
        }
    }

    /**
     * @param $bookingDetail
     * @return bool
     */
    public function removeBookingCalendarDetails($bookingDetail)
    {
        $bcd_id = $bookingDetail->bcd_id;
        $bookingCalendarDetails = BookingCalendarDetails::active()->where("count_reserved", ">", 0)->find($bcd_id);
        if ($bookingCalendarDetails) {
            $count_reserved = $bookingCalendarDetails->count_reserved;
            $bookingCalendarDetails->count_reserved = $count_reserved - 1;
            $bookingCalendarDetails->save();
            return true;
        }
        return false;
    }

    /**
     * @param $booking_id
     * @return bool
     */
    public function rejectUserBooking($booking_id)
    {
        try {
            $booking = (new \Modules\Booking\Models\Booking())->active()->owner()->with("user")->find($booking_id);
            if (!$booking) {
                $this->message_booking = trans("booking::messages.alert.not_find_data");
                return false;
            }

            $params = [];
            $params["reason_reject"] = request()->get("reason_reject");
            $params["reason_note_reject"] = request()->get("reason_note_reject");
            $params_array = $booking->params_array;
            $params = is_array($params_array) && $params_array ? array_merge($params, $params_array) : $params;
            $params = json_encode($params);
            $data = [
                'reservation_status' => \Modules\Booking\Models\Booking::RESERVATION_STATUS_USER_REJECT,
                'params' => $params,
            ];
            $data["action"] = \Modules\Booking\Models\Booking::ACTION_CANCEL;

            $booking->fill($data);
            $booking->save();
            $this->updateBookingDetails($booking, \Modules\Booking\Models\Booking::RESERVATION_STATUS_USER_REJECT, 1, null, \Modules\Booking\Models\Booking::ACTION_CANCEL);
            dispatch(new BookingJob($booking->toArray()))->onQueue('high');
            $this->message_booking = trans("booking::messages.alert.reject_booking_by_user");
            return true;
        } catch (\Exception $exception) {
            report($exception);
            $this->message_booking = trans("booking::messages.alert.error");
            return false;
        }
    }

    /**
     * @param $booking_id
     * @return bool
     */
    public function rejectEntityBooking($booking_id)
    {
        try {
            $booking = (new \Modules\Booking\Models\Booking())->active()->isPendEntity()->with("user")->find($booking_id);
            if (!$booking) {
                $this->message_booking = trans("booking::messages.alert.not_find_data");
                return false;
            }

            $model = $this->getModelObject($booking->model_type);
            $model = $model->isOwner()->active()->find($booking->model_id);
            if (!$model) {
                $this->message_booking = trans("booking::messages.alert.access_error");
                return false;
            }

            $params = [];
            $params["reason_reject_entity"] = request()->get("reason_reject");
            $params["reason_note_reject_entity"] = request()->get("reason_note_reject");
            $params_array = $booking->params_array;
            $params = is_array($params_array) && $params_array ? array_merge($params, $params_array) : $params;
            $params = json_encode($params);
            $data = [
                'reservation_status' => \Modules\Booking\Models\Booking::RESERVATION_STATUS_ENTITY_REJECT,
                'params' => $params,
            ];
            $data["action"] = \Modules\Booking\Models\Booking::ACTION_CANCEL;

            $booking->fill($data);
            $booking->save();
            $this->updateBookingDetails($booking, \Modules\Booking\Models\Booking::RESERVATION_STATUS_ENTITY_REJECT, 1, null, \Modules\Booking\Models\Booking::ACTION_CANCEL);
            dispatch(new BookingJob($booking->toArray()))->onQueue('high');
            $this->message_booking = trans("booking::messages.alert.reject_booking_by_entity", ["user_name" => $booking->user->full_name]);
            return true;
        } catch (\Exception $exception) {
            report($exception);
            $this->message_booking = trans("booking::messages.alert.error");
            return false;
        }
    }

}
