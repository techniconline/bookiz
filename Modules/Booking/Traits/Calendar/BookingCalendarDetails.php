<?php

namespace Modules\Booking\Traits\Calendar;

use BridgeHelper;

use Modules\Booking\Models\Booking\BookingDetails;
use Modules\Entity\Models\Entity\EntityEmployee;

trait BookingCalendarDetails
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [

        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bookingCalendar()
    {
        return $this->belongsTo(\Modules\Booking\Models\Calendar\BookingCalendar::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityEmployee()
    {
        return $this->belongsTo(EntityEmployee::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookingDetails()
    {
        return $this->hasMany(BookingDetails::class, 'bcd_id');
    }

}
