<?php

namespace Modules\Booking\Traits\Calendar;

use BridgeHelper;
use Modules\Booking\Models\Calendar\BookingCalendarDetails;
use Modules\Entity\Models\Entity;
use Modules\Entity\Models\Service\EntityRelationServices;

trait BookingCalendar
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [

        ];
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParamsJsonAttribute()
    {
        $value = $this->params;
        if ($value) {
            return json_decode($value);
        }
        return $value;
    }

    /**
     * @return string
     */
    public function getPeriodTimeAttribute()
    {
        $value = $this->period;
        if ($value>60){
            $mod = fmod($value, 60);
            $min = $mod;
            $h = (int)($value/60);
            $str = $h .' '. trans("booking::calendar.hr").', ';
            if ($min){
                $str .= $min .' '.trans("booking::calendar.min");
            }

        }else{
            $str = $value .' '. trans("booking::calendar.min");
        }
        return $str;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        $query = parent::scopeActive($query);
        $query = $query->where('start_date', '<=', date('Y-m-d H:i:s'));
        $query = $query->where('end_date', '>=', date('Y-m-d H:i:s'));
        return $query;
    }

    /**
     * @param $query
     * @param null $gender
     * @return mixed
     */
    public function scopeFilterGender($query, $gender = null)
    {
        if (!$gender) {
            $user = BridgeHelper::getAccess()->getUser();
            $gender = $user ? $user->gender_key : "all";
            $gender = $gender?:"all";
        }

        if ($gender) {
            $query->where("for_gender", $gender)->orWhere("for_gender", "all");
        }
        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | null
     */
    public function getModelRowAttribute()
    {
        $class = $this->getClass($this->model_type);
        if (!$class) {
            return null;
        }
        return $this->belongsTo($class, 'model_id')->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entityRelationService()
    {
        return $this->belongsTo(EntityRelationServices::class, "model_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookingCalendarDetails()
    {
        return $this->hasMany(BookingCalendarDetails::class);
    }
}
