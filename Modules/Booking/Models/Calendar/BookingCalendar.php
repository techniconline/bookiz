<?php

namespace Modules\Booking\Models\Calendar;

use Modules\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Booking\Traits\Calendar\BookingCalendar as TraitModel;

class BookingCalendar extends BaseModel
{
    use SoftDeletes;
    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['model_id','model_type', 'for_gender', 'start_date', 'end_date', 'days_active_reserve', 'period', 'count_reservation_in_time', 'params', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $api_fields = ['id','for_gender', 'start_date', 'end_date', 'days_active_reserve', 'period','count_reservation_in_time'];
    public $list_fields = ['id','for_gender', 'start_date', 'end_date', 'days_active_reserve', 'period','count_reservation_in_time'];
    public $api_append_fields = ['params_json'];
    public $list_append_fields = ['params_json'];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
