<?php

namespace Modules\Booking\Models\Calendar;

use Modules\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Booking\Traits\Calendar\BookingCalendarDetails as TraitModel;

class BookingCalendarDetails extends BaseModel
{
    use SoftDeletes;
    use TraitModel;

    /**
     * @var array
     */
    protected $fillable = ['booking_calendar_id', 'entity_employee_id', 'count_reserved', 'date', 'time', 'reservation_status', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $statues = [
        0 => "deleted",
        1 => "active",
        2 => "disabled",
        3 => "block",
        4 => "off",
    ];

    const RESERVATION_STATUS_ACCEPT = 1;
    const RESERVATION_STATUS_ENTITY_PEND = 2;
    const RESERVATION_STATUS_USER_PEND = 3;
    const RESERVATION_STATUS_ENTITY_REJECT = 4;
    const RESERVATION_STATUS_USER_REJECT = 5;
    const RESERVATION_STATUS_BLOCK = 6;

    public $api_fields = [];
    public $list_fields = [];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
