<?php

namespace Modules\Booking\Models;

use Modules\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Booking\Traits\Booking\Booking as TraitModel;


class Booking extends BaseModel
{
    use SoftDeletes;
    use TraitModel;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['model_id','model_type','user_id', 'user_address_id', 'params', 'accept_term', 'action', 'reservation_status', 'viewed', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $api_fields = ["id", 'accept_term', 'reservation_status', 'order', 'viewed', 'action'];
    public $list_fields = ["id", 'accept_term', 'reservation_status', 'order', 'viewed', 'action'];
    public $api_append_fields = ["reservation_status_text", "action_text", "created_date", "updated_date", "params_json"];
    public $list_append_fields = ["reservation_status_text", "action_text", "created_date", "updated_date", "params_json"];

    protected $appends=['order'];

    const SYSTEM_NAME = 'booking';
    public $messages = [];
    public $rules = [];
    public $reservation_status_list = ["1" => "accept", "2" => "entity_pend", "3" => "user_pend", "4" => "reject_entity", "5" => "reject_user"];
    public $actions_states = ["new", "do", "doing", "done", "cancel"];

    public $params_variables = ["note", "note_entity", "reason_reject", "reason_reject_entity",
        "reason_note_reject", "reason_note_reject_entity", "order_reference", "payment_reference"];

    const RESERVATION_STATUS_ACCEPT = 1;
    const RESERVATION_STATUS_ENTITY_PEND = 2;
    const RESERVATION_STATUS_USER_PEND = 3;
    const RESERVATION_STATUS_ENTITY_REJECT = 4;
    const RESERVATION_STATUS_USER_REJECT = 5;

    const ACTION_NEW = 'new';
    const ACTION_DO = 'do';
    const ACTION_DOING = 'doing';
    const ACTION_DONE = 'done';
    const ACTION_CANCEL = 'cancel';

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
