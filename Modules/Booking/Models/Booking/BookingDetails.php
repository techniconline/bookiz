<?php

namespace Modules\Booking\Models\Booking;

use Modules\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Booking\Traits\Booking\BookingDetails as TraitModel;

class BookingDetails extends BaseModel
{
    use SoftDeletes;
    use TraitModel;


    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['model_id','model_type','booking_id', 'bcd_id', 'entity_employee_id', 'currency_id', 'by_owner', 'amount', 'discount_amount',
        'payable_amount', 'date', 'time', 'params', 'accept_term', 'action',
        'viewed', 'priority', 'reservation_status', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $api_fields = ['id', 'by_owner', 'amount', 'discount_amount','model_id','model_type',
        'payable_amount', 'date', 'time', 'accept_term',
        'viewed', 'priority', 'reservation_status'];
    public $list_fields = ['id', 'by_owner', 'amount', 'discount_amount','model_id','model_type',
        'payable_amount', 'date', 'time', 'accept_term',
        'viewed', 'priority', 'reservation_status'];
    public $api_append_fields = ['params_json', 'book_time','end_book_time','model_row', "action_text"];
    public $list_append_fields = ['params_json', 'book_time','end_book_time','model_row', "action_text"];

    public $messages = [];
    public $rules = [];
    protected $appends = ["params_json", "model_row"];
    public $reservation_status_list = ["1" => "accept", "2" => "entity_pend", "3" => "user_pend", "4" => "reject_entity", "5" => "reject_user"];
    public $actions_states = ["new", "do", "doing", "done", "cancel"];
    public $params_variables = ["description", "note", "note_entity", "reason_reject", "reason_reject_entity", "reason_note_reject", "reason_note_reject_entity"];

    const RESERVATION_STATUS_ACCEPT = 1;
    const RESERVATION_STATUS_ENTITY_PEND = 2;
    const RESERVATION_STATUS_USER_PEND = 3;
    const RESERVATION_STATUS_ENTITY_REJECT = 4;
    const RESERVATION_STATUS_USER_REJECT = 5;

    const ACTION_NEW = 'new';
    const ACTION_DO = 'do';
    const ACTION_DOING = 'doing';
    const ACTION_DONE = 'done';
    const ACTION_CANCEL = 'cancel';

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
