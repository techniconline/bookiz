<?php
namespace Modules\Booking\Bridge\Booking;

use Illuminate\Support\Facades\Request;
use Modules\Booking\Models\Booking\BookingDetails;
use Modules\Booking\Models\Calendar\BookingCalendar;
use Modules\Booking\ViewModels\Booking\BookingManagerViewModel;
use Modules\Booking\ViewModels\Booking\BookingViewModel;
use Modules\Booking\ViewModels\Calendar\Owner\BookingCalendarViewModel;

/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class Booking
{

    /**
     * @return \Modules\Booking\Models\Booking
     */
    public function getBookingModel()
    {
        return new \Modules\Booking\Models\Booking();
    }

    /**
     * @return BookingDetails
     */
    public function getBookingDetailsModel()
    {
        return new BookingDetails();
    }

    /**
     * @return string
     */
    public function getBookingDetailsClass()
    {
        return BookingDetails::class;
    }

    /**
     * @return BookingCalendar
     */
    public function getBookingCalendarModel()
    {
        return new BookingCalendar();
    }

    /**
     * @return BookingCalendarViewModel
     */
    public function getBookingCalendarViewModel()
    {
        return new BookingCalendarViewModel();
    }

    /**
     * @return BookingViewModel
     */
    public function getBookingViewModel()
    {
        return new BookingViewModel();
    }

    /**
     * @return BookingManagerViewModel
     */
    public function getBookingManagerViewModel()
    {
        return new BookingManagerViewModel();
    }

}