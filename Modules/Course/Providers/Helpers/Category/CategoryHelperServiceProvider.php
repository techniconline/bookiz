<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Course\Providers\Helpers\Category;

use Illuminate\Support\ServiceProvider;

class CategoryHelperServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->registerThisHelperBuilder();
        $this->app->alias("category", "Modules\Course\Providers\Helpers\Category");
    }

    protected function registerThisHelperBuilder(){
        $this->app->singleton("category", function (){
            return new CategoryHelper();
        });
    }

}