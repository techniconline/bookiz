<?php
/**
 * Created by PhpStorm.
 * Date: 6/30/18
 * Time: 11:58 AM
 */

namespace Modules\Course\Providers\Helpers\Category;

use Illuminate\Database\Eloquent\Collection;
use Modules\Core\Traits\Decorate\DecorateData;
use Modules\Course\Models\Course\CourseCategory;

class CategoryHelper
{

    use DecorateData;
    private $html = '';

    public function createTreeByNestable($categories)
    {

        $this->html = '
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bubble font-purple"></i>
                        <span class="caption-subject font-purple sbold uppercase">' . trans("course::category.list_category", ["root_category" => isset($categories[0]) ? ' ( ' . $categories[0]->title . ' ) ' : '...']) . '</span>
                    </div>
                    <div class="actions">
                        <!--<div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                                <input type="radio" name="options" class="toggle" id="option1">New</label>
                            <label class="btn btn-transparent grey-salsa btn-circle btn-sm">
                                <input type="radio" name="options" class="toggle" id="option2">Returning</label>
                        </div>
                        -->
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="dd" id="nestable_list_category_tree" data-token="' . csrf_token() . '">
                        <ol class="dd-list">
        ';
        $this->html .= '<script>';
        $this->html .= 'var $json_data = {};';
        $this->html .= '</script>';

        self::renderNestableChildes($categories, true);

        $this->html .= '
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        ';

        return $this->html;

    }

    private  function renderNestableChildes($categories, $is_root = false)
    {

        foreach ($categories as $category) {
//            dd($category);
            $this->html .= '<script>';
            $this->html .= '$json_data[' . $category->id . '] = ' . (json_encode($category->toArray())) . ';';
            $this->html .= 'console.log($json_data[' . $category->id . ']);';
            $this->html .= '</script>';
            $this->html .= '<li class="dd-item dd3-item" data-id="' . $category->id . '" data-is-root="' . ($is_root ? 1 : 0) . '">';
            $this->html .= '
                    <div class="dd-handle dd3-handle _change_position" change-position-url="' . route('course.category.change_position', ['category_id' => $category->id]) . '"></div>
                    <div class="dd3-content">  
                        <a href="' . route('course.category.update', ['category_id' => $category->id]) . '" class="btn btn-xs btn-default blue _edit" data-json=""><i class="fa fa-edit"></i></a> 
                        <a href="' . route('course.category.add_child', ['category_id' => $category->id]) . '" class="btn btn-xs btn-default green _add_child"><i class="fa fa-plus-circle"></i></a> 
                        <a href="' . route('course.category.delete', ['category_id' => $category->id]) . '" class="btn btn-xs btn-default red _delete" data-text-confirm="' . trans("course::category.confirm_delete") . '"><i class="fa fa-trash"></i></a> 
                    ' . $category->title . ' 
                    </div>
                ';
            if ($category->childes->count()) {

                $this->html .= '
                    <ol class="dd-list">
                ';

                self::renderNestableChildes($category->childes);

                $this->html .= '
                    </ol>
                ';
            }

            $this->html .= '</li>';
        }

    }

    /**
     * @param $id
     * @param int $deep
     * @param array $append_fields
     * @return \Illuminate\Support\Collection
     */
    public function getNestableCourseCategory($id, $deep = 1, $append_fields = [])
    {
        $courseCategoryModel = new CourseCategory();
        $childes = $courseCategoryModel->getChildesId($id);
        $courseCategory = $courseCategoryModel->active()->filterLanguage()->filterCurrentInstance()->find($id);
        if (!$courseCategory) {
            return collect([]);
        }
        $root_id = $courseCategory["root_id"];
        $roots = $courseCategoryModel->active()->whereIn('id', $childes)->get();
        $courseCategories = self::getNestable($roots, $courseCategory["parent_id"], $root_id, $deep + 1, $append_fields)->first();
        if ($courseCategories) {
            return $courseCategories["childes"];
        }

        return $courseCategories;
    }

    /**
     * @param int $deep
     * @param array $append_fields
     * @return \Illuminate\Support\Collection
     */
    public function getNestableCourseCategories($deep = 999999, $append_fields = [])
    {
        $courseCategoryModel = new CourseCategory();
        $listRoots = $courseCategoryModel->whereNull("parent_id")->active()->filterLanguage()->filterCurrentInstance()->get();
        $list = collect($listRoots)->map(function ($item, $index) use ($deep, $append_fields) {
            $item = $this->decorateAttributes($item);
            $item->put("childes",self::getNestableCourseCategory($item["id"], $deep, $append_fields));
            return $item;
        });
        return $list;
    }

    /**
     * @param Collection $courseCategories
     * @param null $parent_id
     * @param null $root_id
     * @param int $deep
     * @param array $append_fields
     * @return Collection
     */
    protected  function getNestable(Collection $courseCategories, $parent_id = null, $root_id = null, $deep = 10, $append_fields = [])
    {
        $subCourseCategories = $courseCategories->where('parent_id', $parent_id);
        if ($root_id) {
            $subCourseCategories = $subCourseCategories->where('root_id', $root_id);
        }
        $subCourseCategories = $subCourseCategories->sortBy('sort');
        $newArr = [];

        if ($deep) {
            foreach ($subCourseCategories as $courseCategory) {
                $courseCategory = $this->decorateAttributes($courseCategory);
                $newdeep = $deep - 1;

                $courseCategory->put("childes", self::getNestable($courseCategories, $courseCategory["id"], null, $newdeep));
                $newArr[] = $courseCategory;
            }
        }
        return collect($newArr);
    }

}