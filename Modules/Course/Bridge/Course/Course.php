<?php

namespace Modules\Course\Bridge\Course;
use Modules\Course\Models\Course\CourseActivity;

/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class Course
{
    /**
     * @return string
     */
    public function getUrlCourseList()
    {
        return route('course.get_course_by_api');
    }

    /**
     * @return string
     */
    public function getUrlCourseCategoryList()
    {
        return route('course.category.get_category_by_api');
    }

    /**
     * @return CourseActivity
     */
    public function getCourseActivityModel()
    {
        return new CourseActivity();
    }

    /**
     * @return \Modules\Course\Models\Course\Course
     */
    public function getCourseModel()
    {
        return new \Modules\Course\Models\Course\Course();
    }

    /**
     * @return string
     */
    public function getCourseClass()
    {
        return \Modules\Course\Models\Course\Course::class;
    }

}