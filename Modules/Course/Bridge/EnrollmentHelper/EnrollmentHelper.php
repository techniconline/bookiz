<?php

namespace Modules\Course\Bridge\EnrollmentHelper;


/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class EnrollmentHelper
{

    private $desingConfigs = [
        'parent_class' => null,
        'form_class' => null,
        'button_class' => null,
        'button_title' => null,
    ];

    public function getAddToEnrollment($item_type, $item_id, $options = [], $designConfigs = [])
    {
        $designConfigs = array_merge($this->desingConfigs, $designConfigs);
        $this->addAssetToTheme();
        return view('course::enrollment.add_to_enrollment', ['item_type' => $item_type, 'item_id' => $item_id, 'options' => $options, 'attributes' => $designConfigs]);
    }


    private function addAssetToTheme()
    {
//        $assets = [];
//        $assets[] = ['container' => 'block_js', 'src' => 'assets/modules/sale/cart.js', 'name' => 'cart.js'];
//        get_instance()->setActionAssets($assets);
    }


}