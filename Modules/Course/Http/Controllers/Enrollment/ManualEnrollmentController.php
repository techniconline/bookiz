<?php

namespace Modules\Course\Http\Controllers\Enrollment;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class ManualEnrollmentController extends Controller
{

    /**
     * @param $course_id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($course_id, Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(["course_id"=>$course_id], $request)
            ->setViewModel('enrollment.listEnrollment')
            ->setActionMethod("getViewListIndex")->response();
    }


    /**
     * @param $course_id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create($course_id, Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(["course_id" => $course_id], $request)
            ->setViewModel('enrollment.manualEnrollment')
            ->setActionMethod("createManualEnrollment")->response();
    }

    /**
     * @param $course_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($course_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(["course_id" => $course_id], $request)
            ->setViewModel('enrollment.manualEnrollment')
            ->setActionMethod("saveManualEnrollment")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatus($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_user_enrollment_id' => $id], $request)
            ->setViewModel('enrollment.manualEnrollment')
            ->setActionMethod("changeStatusEnrollment")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_user_enrollment_id' => $id], $request)
            ->setViewModel('enrollment.manualEnrollment')
            ->setActionMethod("destroyUserEnrollment")->response();
    }

}
