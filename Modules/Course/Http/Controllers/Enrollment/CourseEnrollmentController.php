<?php

namespace Modules\Course\Http\Controllers\Enrollment;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class CourseEnrollmentController extends Controller
{

    /**
     * @param $course_id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($course_id, Request $request, MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(["course_id"=>$course_id], $request)
            ->setViewModel('enrollment.courseEnrollment')
            ->setActionMethod("getViewListIndex")->response();
    }


    /**
     * @param $course_id
     * @param Request $request
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create($course_id, Request $request,MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setItemsRequest(["course_id"=>$course_id], $request)
            ->setViewModel('enrollment.courseEnrollment')
            ->setActionMethod("createCourseEnrollment")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)
            ->setViewModel('enrollment.courseEnrollment')
            ->setActionMethod("saveCourseEnrollment")->response();
    }

    /**
     * Show the specified resource.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_enrollment_id' => $id], $request)
            ->setViewModel('enrollment.courseEnrollment')
            ->setActionMethod("editCourseEnrollment")->response();
    }


    /**
     * @param $enrollment_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function showEnrollment($enrollment_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['enrollment_id' => $enrollment_id], $request)
            ->setViewModel('enrollment.courseEnrollment')
            ->setActionMethod("getViewEnrollment")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_enrollment_id' => $id], $request)
            ->setViewModel('enrollment.courseEnrollment')
            ->setActionMethod("saveCourseEnrollment")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_enrollment_id' => $id], $request)
            ->setViewModel('enrollment.courseEnrollment')
            ->setActionMethod("destroyCourseEnrollment")->response();
    }

}
