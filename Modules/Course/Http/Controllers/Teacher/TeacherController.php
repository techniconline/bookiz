<?php

namespace Modules\Course\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class TeacherController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setViewModel('teacher')
            ->setActionMethod("getViewListIndex")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setViewModel('teacher')
            ->setActionMethod("createTeacher")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getUserByApi(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['is_ajax'=>1],$request)
            ->setViewModel('teacher')
            ->setActionMethod("getUserByApi")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getTeacherByApi(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['is_ajax'=>1],$request)->setViewModel('teacher')
            ->setActionMethod("getTeacherByApi")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('teacher')
            ->setActionMethod("saveTeacher")->response();
    }

    /**
     * Show the specified resource.
     * @param $teacher_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($teacher_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['teacher_id' => $teacher_id], $request)->setViewModel('teacher')
            ->setActionMethod("editTeacher")->response();
    }


    /**
     * Update the specified resource in storage.
     * @param $teacher_id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($teacher_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['teacher_id' => $teacher_id], $request)->setViewModel('teacher')
            ->setActionMethod("saveTeacher")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $teacher_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($teacher_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['teacher_id' => $teacher_id], $request)->setViewModel('teacher')
            ->setActionMethod("destroyTeacher")->response();
    }

}
