<?php

namespace Modules\Course\Http\Controllers\Course;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class CourseRelationsController extends Controller
{

    /**
     * @param $course_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($course_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_id' => $course_id], $request)->setViewModel('course.relations')->setActionMethod("indexCourseRelations")->response();
    }

    /**
     * @param $course_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($course_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_id' => $course_id], $request)->setViewModel('course.relations')->setActionMethod("saveCourseRelations")->response();
    }

    /**
     * @param $course_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($course_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_relation_id' => $course_id], $request)->setViewModel('course.relations')->setActionMethod("destroyCourseRelation")->response();
    }

}
