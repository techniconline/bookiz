<?php

namespace Modules\Course\Http\Controllers\Course;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class CourseCopyController extends Controller
{

    /**
     * @param $course_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($course_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_id' => $course_id], $request)->setViewModel('course.copy')->setActionMethod("indexCourseCopy")->response();
    }

    /**
     * @param $course_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($course_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_id' => $course_id], $request)->setViewModel('course.copy')->setActionMethod("saveCourseCopy")->response();
    }

}
