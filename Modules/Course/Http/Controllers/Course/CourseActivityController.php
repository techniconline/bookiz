<?php

namespace Modules\Course\Http\Controllers\Course;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class CourseActivityController extends Controller
{

    /**
     * @param $course_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($course_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_id' => $course_id], $request)->setViewModel('course.courseActivity')
            ->setActionMethod("getViewListIndex")->response();
    }


    /**
     * @param $course_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create($course_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_id' => $course_id], $request)->setViewModel('course.courseActivity')
            ->setActionMethod("createCourseActivity")->response();
    }


    /**
     * @param $course_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('course.courseActivity')
            ->setActionMethod("saveCourseActivity")->response();
    }

    /**
     * Show the specified resource.
     * @param $course_activity_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($course_activity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_activity_id' => $course_activity_id], $request)->setViewModel('course.courseActivity')
            ->setActionMethod("editCourseActivity")->response();
    }


    /**
     * Update the specified resource in storage.
     * @param $course_activity_id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($course_activity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_activity_id' => $course_activity_id], $request)->setViewModel('course.courseActivity')
            ->setActionMethod("saveCourseActivity")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $course_activity_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($course_activity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_activity_id' => $course_activity_id], $request)->setViewModel('course.courseActivity')
            ->setActionMethod("destroyCourseActivity")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeItem(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('course.courseActivity')
            ->setActionMethod("saveCourseActivityItem")->response();
    }

    /**
     * @param $course_activity_item_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyItem($course_activity_item_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_activity_item_id' => $course_activity_item_id], $request)->setViewModel('course.courseActivity')
            ->setActionMethod("destroyCourseActivityItem")->response();
    }

}
