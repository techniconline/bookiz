<?php

namespace Modules\Course\Http\Controllers\Course;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class ActivityUploaderController extends Controller
{
    /**
     * @param $course_activity_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function uploadFile($course_activity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_activity_id' => $course_activity_id], $request)->setViewModel('course.activityUploader')
            ->setActionMethod("uploadFile")->response();
    }

    /**
     * @param $course_activity_id
     * @param $file_index
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteFile($course_activity_id, $file, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_activity_id' => $course_activity_id, "file" => $file], $request)->setViewModel('course.activityUploader')
            ->setActionMethod("deleteFile")->response();
    }
}
