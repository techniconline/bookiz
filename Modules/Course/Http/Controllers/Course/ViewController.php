<?php

namespace Modules\Course\Http\Controllers\Course;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class ViewController extends Controller
{

    /**
     * Show the specified resource.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function view($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_id' => $id], $request)->setViewModel('course.page')
            ->setActionMethod("viewCourse")->response();
    }

    /**
     * @param $category_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function listCourses($category_id, $alias, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(["category_id" => $category_id], $request)->setViewModel("course.categoryByCourses")
            ->setActionMethod("listCourses")->response();
    }


    public function activity($course_id, $activity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_id' => $course_id, 'activity_id' => $activity_id], $request)->setViewModel('course.page')
            ->setActionMethod("viewActivity")->response();
    }

    /**
     * @param $course_id
     * @param $activity_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getActivity($course_id, $activity_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['course_id' => $course_id, 'activity_id' => $activity_id], $request)->setViewModel('course.page')
            ->setActionMethod("getActivity")->response();
    }


    /**
     * @param $teacher_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function showTeacherDetails($teacher_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(["teacher_id" => $teacher_id], $request)->setViewModel("teacher.page")
            ->setActionMethod("viewTeacherDetails")->response();
    }

    public function json(Request $request,MasterViewModel $masterViewModel){
        return $masterViewModel->setItemsRequest(['is_ajax'=>1],$request)->setViewModel('course.json')->setActionMethod("getCourseByApi")->response();
    }
}
