<?php

namespace Modules\Course\Http\Controllers\Course;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class UserController extends Controller
{
    /**
     * Show the specified resource.
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function myCourses(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('course.user')->setActionMethod("myCourses")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function myActivity(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('course.user')->setActionMethod("myActivity")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function myCoursesAndActivities(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('course.user')->setActionMethod("myCoursesAndActivities")->response();
    }

    /**
     * @param $item_type
     * @param $item_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enrollFreeItem($item_type, $item_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['item_type' => $item_type, 'item_id' => $item_id], $request)->setViewModel('course.user')->setActionMethod("enrollFreeItem")->response();
    }

}
