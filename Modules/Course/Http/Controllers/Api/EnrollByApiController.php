<?php

namespace Modules\Course\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class EnrollByApiController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enrollOneStep($api,MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['api'=>$api],$request)->setViewModel('api.enroll')->setActionMethod("enroll")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enrollScore($api,MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['api'=>$api],$request)->setViewModel('api.enroll')->setActionMethod("score")->response();
    }

}
