<?php
Route::group(['middleware' => ['web'], 'prefix' => 'course', 'as' => 'course.', 'namespace' => 'Modules\Course\Http\Controllers'], function () {

    Route::get('search', 'SearchController@search')->name('search');
    Route::get('search/parameters/get', 'SearchController@parameters')->name('search.parameters');
    Route::get('search/get', 'SearchController@getCourseByApi')->name('get_course_by_api');

    Route::group(['namespace' => '\Course'], function () {
        Route::get('/view/{id}', 'ViewController@view')->name('view');
        Route::get('/teacher/view/{teacher_id}', 'ViewController@showTeacherDetails')->name('show_teacher_details');
        Route::get('/view/{course_id}/activity/{activity_id}', 'ViewController@activity')->name('activity');
        Route::get('/view/{course_id}/activity/{activity_id}/get', 'ViewController@getActivity')->name('getActivity');
        Route::get('category/{category_id}/courses/{alias}', 'ViewController@listCourses')->name('category.list.courses');
        Route::get('/json/', 'ViewController@json')->name('json');

    });

    Route::group(['prefix' => 'category', 'as' => 'category.', 'namespace' => '\Category'], function () {

        Route::group(['middleware' => 'api', 'as' => 'api.'], function () {
            Route::get('/get/root/list', 'CategoryController@getRootCategories')->name('get_root_categories');
            Route::get('/get/{parent_id}/childes/list', 'CategoryController@getChildesCategories')->name('get_childes_categories');
        });
    });

    Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'user', 'as' => 'user.', 'namespace' => '\Course'], function () {
//        Route::get('/', 'UserController@myCoursesAndActivities')->name('my.courses_and_activities');
        Route::get('/', 'UserController@myCourses')->name('my.courses');
        Route::get('/activity', 'UserController@myActivity')->name('my.activity');

        Route::post('/enroll/{item_type}/{item_id}', 'UserController@enrollFreeItem')->name('enroll.free.item');
    });

    Route::group([ 'as' => 'api.', 'namespace' => '\Api'], function () {
        Route::any('/on/enroll/{name}/', 'EnrollByApiController@enrollOneStep')->name('on.enroll');
        Route::any('/on/enroll/score/{name}/', 'EnrollByApiController@enrollScore')->name('on.enroll.score');
    });

});
Route::group(['middleware' => ['web', 'auth', 'admin'], 'prefix' => 'course', 'as' => 'course.', 'namespace' => 'Modules\Course\Http\Controllers'], function () {

    Route::group(['prefix' => 'config', 'as' => 'config.', 'namespace' => '\Config'], function () {
        Route::get('/instance/index', 'InstanceController@index')->name('instance.index');
        Route::post('/instance/save', 'InstanceController@save')->name('instance.save');
    });

    Route::group(['namespace' => '\Course'], function () {
        Route::get('/', 'CourseController@index')->name('index');
        Route::get('/create', 'CourseController@create')->name('create');
        Route::get('/{course_id}/edit', 'CourseController@edit')->name('edit');
        //Route::get('/{course_id}/show', 'CourseController@show')->name('show');
        Route::post('/save', 'CourseController@store')->name('save');
        Route::put('/{course_id}/update', 'CourseController@update')->name('update');
        Route::delete('/{course_id}/delete', 'CourseController@destroy')->name('delete');

        Route::group(['prefix' => 'section', 'as' => 'section.'], function () {
            Route::get('/{course_id}', 'CourseSectionController@index')->name('index');
            Route::get('/{course_id}/create', 'CourseSectionController@create')->name('create');
            Route::post('/{course_id}/store', 'CourseSectionController@store')->name('save');
            Route::get('/{course_section_id}/edit', 'CourseSectionController@edit')->name('edit');
            Route::put('/{course_section_id}/update', 'CourseSectionController@update')->name('update');
            Route::post('/sorting', 'CourseSectionController@sortingSection')->name('sorting');
            Route::delete('/{course_section_id}/delete', 'CourseSectionController@destroySection')->name('delete');
        });

        Route::group(['prefix' => 'copy', 'as' => 'copy.'], function () {
            Route::get('/{course_id}', 'CourseCopyController@index')->name('index');
            Route::post('/{course_id}/store', 'CourseCopyController@store')->name('save');
        });

        Route::group(['prefix' => 'relations', 'as' => 'relations.'], function () {
            Route::get('/{course_id}', 'CourseRelationsController@index')->name('index');
            Route::post('/{course_id}/store', 'CourseRelationsController@store')->name('save');
            Route::delete('/{course_relation_id}/delete', 'CourseRelationsController@destroy')->name('delete');
        });

        Route::group(['middleware' => 'api', 'as' => 'api.',], function () {
            Route::get('/get/{category_id}/list', 'CourseController@getCoursesByCategory')->name('get_courses_by_category');
        });


    });

    Route::group(['prefix' => 'meta', 'as' => 'meta.', 'namespace' => '\Course'], function () {

        Route::post('/{course_id}/{meta_group_id}/features/value/save', 'CourseController@saveFeaturesValue')->name('features.value.save');
        Route::get('/{course_id}/tags/edit', 'CourseController@editMetaTags')->name('editMetaTags');
        Route::post('/{course_id}/{course_meta_group_id}/copy', 'CourseController@copyCourseMetaGroup')->name('copy');
        Route::delete('/{course_id}/{course_meta_group_id}/delete', 'CourseController@deleteCourseMetaGroup')->name('delete');

    });

    Route::group(['prefix' => 'activity', 'as' => 'activity.', 'namespace' => '\Course'], function () {
        Route::get('/{course_id}', 'CourseActivityController@index')->name('index');
        Route::get('/{course_id}/create', 'CourseActivityController@create')->name('create');
        Route::get('/{activity_id}/edit', 'CourseActivityController@edit')->name('edit');
        Route::post('/save', 'CourseActivityController@store')->name('save');
        Route::put('/{activity_id}/update', 'CourseActivityController@update')->name('update');
        Route::delete('/{activity_id}/delete', 'CourseActivityController@destroy')->name('delete');
        Route::post('/{activity_id}/file/upload', 'ActivityUploaderController@uploadFile')->name('file.upload');
        Route::delete('/{activity_id}/{url_file}/file/delete', 'ActivityUploaderController@deleteFile')->name('file.delete');

        Route::group(['prefix' => 'items', 'as' => 'items.'], function () {
            Route::post('/save', 'CourseActivityController@storeItem')->name('save');
            Route::delete('/{activity_items_id}/delete', 'CourseActivityController@destroyItem')->name('delete');
        });

    });

    Route::group(['prefix' => 'category', 'as' => 'category.', 'namespace' => '\Category'], function () {
        Route::get('/', 'CategoryController@index')->name('index');
        Route::get('/create', 'CategoryController@create')->name('create');
        Route::get('/api/get', 'CategoryController@getCategoryByApi')->name('get_category_by_api');
        Route::post('/save', 'CategoryController@store')->name('save');
        Route::post('/add/child/{parent_id}', 'CategoryController@addChild')->name('add_child');
        Route::put('/{category_id}/change_position', 'CategoryController@changePosition')->name('change_position');
        Route::put('/{category_id}/update', 'CategoryController@update')->name('update');
        Route::get('/{category_id}/show', 'CategoryController@show')->name('show');
        Route::delete('/{category_id}/delete', 'CategoryController@destroy')->name('delete');

    });

    Route::group(['prefix' => 'teacher', 'as' => 'teacher.', 'namespace' => '\Teacher'], function () {
        Route::get('/', 'TeacherController@index')->name('index');
        Route::get('/create', 'TeacherController@create')->name('create');
        Route::get('/user/get', 'TeacherController@getUserByApi')->name('get_user_by_api');
        Route::get('/teacher/get', 'TeacherController@getTeacherByApi')->name('get_teacher_by_api');
        Route::post('/save', 'TeacherController@store')->name('save');
        Route::put('/{teacher_id}/update', 'TeacherController@update')->name('update');
        Route::get('/{teacher_id}/edit', 'TeacherController@edit')->name('edit');
        Route::delete('/{teacher_id}/delete', 'TeacherController@destroy')->name('delete');
    });

    Route::group(['prefix' => 'enrollment', 'as' => 'enrollment.', 'namespace' => '\Enrollment'], function () {
        Route::get('/', 'EnrollmentController@index')->name('index');
        Route::get('/create', 'EnrollmentController@create')->name('create');
        Route::get('/get', 'EnrollmentController@getEnrollmentByApi')->name('get_enrollment_by_api');
        Route::post('/save', 'EnrollmentController@store')->name('save');
        Route::put('/{enrollment_id}/update', 'EnrollmentController@update')->name('update');
        Route::get('/{enrollment_id}/edit', 'EnrollmentController@edit')->name('edit');
        Route::delete('/{enrollment_id}/delete', 'EnrollmentController@destroy')->name('delete');
    });

    //course enrollments routes
    Route::group(['prefix' => 'enrollments', 'as' => 'enrollments.', 'namespace' => '\Enrollment'], function () {
        Route::get('/{course_id}', 'CourseEnrollmentController@index')->name('index');
        Route::get('{course_id}/create', 'CourseEnrollmentController@create')->name('create');
        Route::get('/{course_enrollment_id}/edit', 'CourseEnrollmentController@edit')->name('edit');

        Route::get('/get/view/{enrollment_id}', 'CourseEnrollmentController@showEnrollment')->name('showEnrollment');

        Route::post('/save', 'CourseEnrollmentController@store')->name('save');
        Route::put('/{course_enrollment_id}/update', 'CourseEnrollmentController@update')->name('update');
        Route::delete('/{course_enrollment_id}/delete', 'CourseEnrollmentController@destroy')->name('delete');

        Route::get('users/list/{course_id}', 'ManualEnrollmentController@index')->name('user.list.index');
        Route::group(['prefix' => 'manual', 'as' => 'manual.'], function () {
            Route::get('{course_id}/create', 'ManualEnrollmentController@create')->name('create');
            Route::get('{course_user_enrollment_id}/edit', 'ManualEnrollmentController@edit')->name('edit');

            Route::post('{course_id}/save', 'ManualEnrollmentController@store')->name('save');
            Route::delete('{course_user_enrollment_id}/changeStatus', 'ManualEnrollmentController@changeStatus')->name('changeStatus');
            Route::delete('/{course_user_enrollment_id}/delete', 'ManualEnrollmentController@destroy')->name('delete');
        });

        Route::group(['prefix' => 'package', 'as' => 'package.'], function () {
            Route::get('{course_id}/list', 'PackageEnrollmentController@index')->name('index');
            Route::get('{course_id}/create', 'PackageEnrollmentController@create')->name('create');

            Route::post('{course_id}/save', 'PackageEnrollmentController@store')->name('save');
            Route::delete('{course_package_enrollment_id}/changeStatus', 'PackageEnrollmentController@changeStatus')->name('changeStatus');
            Route::delete('/{course_package_enrollment_id}/delete', 'PackageEnrollmentController@destroy')->name('delete');
        });


    });


});
