<?php

namespace Modules\Course\Traits;


trait Enrollment
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'name.required' => trans('course::errors.enrollment.name_required'),
            'alias.required' => trans('course::errors.enrollment.alias_required'),
        ];
        return $this;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotLoadEnrolls($query)
    {
        return $query->whereNotIn("alias", $this->not_public_enrolls);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseEnrollments()
    {
        return $this->hasMany('Modules\Course\Models\Enrollment\CourseEnrollment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseUserEnrollments()
    {
        return $this->hasMany('Modules\Course\Models\Enrollment\CourseUserEnrollment');
    }

    public static function getAllEnrollment(){
        $key='AllEnrollMent';
        $items=cache($key);
        if(!$items){
            $all=Self::Where('active',1)->get();
            $items=new \stdClass();
            foreach ($all as $enrollment){
                $items->{$enrollment->id}=(object)$enrollment->toArray();
            }
            cache([$key=>$items],1440);
        }
        return $items;
    }
}
