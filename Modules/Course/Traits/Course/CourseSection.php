<?php

namespace Modules\Course\Traits\Course;


trait CourseSection
{

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterCourse($query)
    {
        if($course_id = $this->getRequestItem("course_id")){
            return $query->where("course_id", $course_id);
        }
        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseActivities()
    {
        return $this->hasMany('Modules\Course\Models\Course\CourseActivity')->active()->orderBy("sort");
    }
}
