<?php
namespace Modules\Course\Traits\Course;

trait CourseRelation
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseRelation()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course', 'relation_course_id');
    }

}
