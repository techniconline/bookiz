<?php
namespace Modules\Course\Traits\Course;

use BridgeHelper;
use Modules\Core\Models\Feature\FeatureGroupRelation;
trait CourseInfo
{



    public function getFieldsList(){
        $fields=[
            'duration'=>trans('course::course.info.duration'),
            'students'=>trans('course::course.info.students'),
            'section'=>trans('course::course.info.section'),
            'sessions'=>trans('course::course.info.sessions'),
            'quiz'=>trans('course::course.info.quiz'),
            'teachers'=>trans('course::course.info.teachers'),
        ];
        return $this->getCourseFeatureField($fields);
    }

    public function getCourseFeatureField($fields){
        $featureGroupId=BridgeHelper::getConfig()->getSettings('default_feature_group','instance','course');
        if(!$featureGroupId){
            return [];
        }
        $featureGroup=FeatureGroupRelation::with('feature')->where('feature_group_id',$featureGroupId)->orderBy('position')->get();
        if($featureGroup->count()){
            foreach($featureGroup as $field){
                $fields[$field->feature_group_id.'_'.$field->feature_id]=trans('course::course.meta').': '.$field->feature->title;
            }
        }
        return $fields;
    }

}
