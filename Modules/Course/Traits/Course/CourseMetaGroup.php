<?php

namespace Modules\Course\Traits\Course;


trait CourseMetaGroup
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [

        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function metaGroup()
    {
        return $this->belongsTo('Modules\Core\Models\Meta\MetaGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course');
    }

    /** //TODO test
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function featureValueRelations()
    {
        return $this->hasMany('Modules\Core\Models\Feature\FeatureValueRelation', 'item_id')
            ->where('system', $this->getTable())->active()->filterLanguage();
    }

}
