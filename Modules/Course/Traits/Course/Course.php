<?php

namespace Modules\Course\Traits\Course;


use BridgeHelper;
use Illuminate\Support\Facades\Cache;
use Modules\Course\Models\Enrollment;
use Modules\Course\ViewModels\Course\CourseViewModel;

trait Course
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'title.required' => trans('course::errors.course.title_required'),
            'language_id.required' => trans('course::errors.course.language_id_required'),
            'instance_id.required' => trans('course::errors.course.instance_id_required'),
            'category_id.required' => trans('course::errors.course.category_id_required'),
            'type.required' => trans('course::errors.course.instance_id_required'),

        ];
        return $this;
    }

    /**
     * @return string
     */
    public function getGenerateCourseCode()
    {
        $instance_id = $this->instance_id ?: app('getInstanceObject')->getCurrentInstanceId();
        return "CRS" . $instance_id . "-" . strtoupper(str_random(10));
    }

//    /**
//     * @param $value
//     * @return string
//     */
//    public function setCodeAttribute($value)
//    {
//        if (!$value) {
//            return $this->getGenerateCourseCode();
//        }
//        return $value;
//    }

    /**
     * @return mixed|null
     */
    public function getParamsJsonAttribute()
    {
        if ($this->params) {
            return json_decode($this->params);
        }
        return null;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeSearching($query)
    {
        $query = $query->filterCurrentInstance();
        $query = $query->filterLanguage();
        $query = $query->filterByCategories();
        $query = $query->filterByEnrollments();
        if ($term = $this->getRequestItem('text')) {
            $query = $query->where(function ($q) use ($term) {
                $q->orWhere('code', $term);
                $q->orWhere('title', 'LIKE', '%' . $term . '%');
                $q->orWhere('short_description', 'LIKE', '%' . $term . '%');
            });
        }

        return $query;
    }

    public function getInfoCourseAttribute()
    {
        $attrs = [
            "duration" => $this->getDurationAttribute(),
            "count_students" => $this->getCountStudentsAttribute(),
            "sessions" => $this->getSessionsAttribute(),
            "course_sections_count" => $this->getCourseSectionsCountAttribute(),
            "quiz" => $this->getQuizAttribute(),
        ];
        return $attrs;
    }

    /**
     * @return mixed
     */
    public function getRateAttribute()
    {
        return BridgeHelper::getRate()->getRateObject()->setModel($this->getTable(), $this->id)->setWithData(true)->init()->getData();
    }

    /**
     * @return mixed
     */
    public function getIsEnrollAttribute()
    {
        $this->setUserRoleAndAccessOnCourse($this->id);
        return $this->isEnroll();
    }

    /**
     * @return CourseViewModel
     * @throws \Throwable
     */
    public function getMetaCourseAttribute()
    {
        $viewModel = new CourseViewModel();
        $result = $viewModel->editMetaTags($this->id);
        return $result;
    }

    /**
     * @return mixed
     */
    public function getEnrollmentsAttribute()
    {
        return $this->getCourseEnrollments($this->id, 'list', true);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getImageAttribute($value)
    {
        return strlen($value) ? url($value).'?_v='.strtotime($this->updated_at?$this->updated_at:$this->created_at) : ((BridgeHelper::getConfig()->getSettings('default_poster', 'instance', 'course')) ? url(BridgeHelper::getConfig()->getSettings('default_poster', 'instance', 'course')) : $value);
    }

    /**
     * @return null
     */
    public function getImageInAdminAttribute()
    {
        if ($this->attributes["image"]) {
            return $this->attributes["image"];
        }
        return null;
    }


    /**
     * @return array
     */
    public function getDurationAttribute()
    {
        $duration = $this->courseActivities()->active()->sum('duration');
        $unit = BridgeHelper::getConfig()->getSettings('default_time_unit', 'instance', 'course');
        if (!$unit) {
            $unit = 'minute';
        }
        if ($unit != 'minute' && $duration > 0) {
            if ($unit == 'hour') {
                $duration = round($duration / 60, 0);
            }
        }

        $data = [
            "label" => trans('course::course.info.duration'),
            "value" => $duration,
            "unit" => trans('course::course.info_unit.' . $unit),
            "icon" => "icon-rewind-time",
        ];
        return $data;
    }


    /**
     * @return array
     */
    public function getCountStudentsAttribute()
    {

        $students = $this->courseUsersEnrollment()->distinct()->count("user_id");

        //TODO TEST MOLAIE
        if (isset($this->params_json->min_show_item_enrollment_user) && ((int)$this->params_json->min_show_item_enrollment_user > $students)) {
            $students =0;
        }

//        if(Cache::has('course_students_'.$this->id)){
//            $rand_count = Cache::get('course_students_'.$this->id);
//        }else{
//            $rand_count =  rand(300, 700);
//            Cache::forever('course_students_'.$this->id, $rand_count);
//        }
//
//        $students += $rand_count;

        $data = [
            "label" => trans('course::course.info.students'),
            "value" => $students,
            "unit" => trans('course::course.info_unit.students'),
            "icon" => "icon-graduate",
        ];
        return $data;

    }

    /**
     * @return mixed
     */
    public function getSessionsAttribute()
    {
        $data = [
            "label" => trans('course::course.info.sessions'),
            "value" => $this->courseActivities()->active()->count('duration'),
            "unit" => trans('course::course.info_unit.sessions'),
            "icon" => "icon-computer",
        ];
        return $data;
    }

    /**
     * @return mixed
     */
    public function getQuizAttribute()
    {
        $data = [
            "label" => trans('course::course.info.quiz'),
            "value" => 0,
            "unit" => trans('course::course.info_unit.quiz'),
            "icon" => "icon-notepad",
        ];
        return $data;
    }

    /**
     * @return mixed
     */
    public function getTeachersAttribute()
    {
        $teachers = $this->teachers()->with("user")->get();
        $data = [
            "label" => trans('course::course.info.teacher' . ($teachers->count() > 1 ? "s" : "")),
            "value" => $this->teachers()->with("user")->get(),
            "unit" => "",
            "icon" => "icon-teachers",
        ];
        return $data;
    }

    /**
     * @return mixed
     */
    public function getCourseSectionsCountAttribute()
    {
        $data = [
            "label" => trans('course::course.info.section'),
            "value" => $this->courseSections()->active()->count(),
            "unit" => trans('course::course.info_unit.section'),
            "icon" => "icon-computer",
        ];
        return $data;
    }

    /**
     * @return string
     */
    public function getUrlSingleCourseAttribute()
    {
        return route("course.view", ["course_id" => $this->id]);
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        $newTypes = [];

        foreach ($this->types as $type) {
            $newTypes[$type] = trans("course::course." . $type);
        }
        return $newTypes;
    }

    public function scopeSortBy($query)
    {
        $sorted = false;
        if ($sort_by = $this->getRequestItem("sort_by")) {
            $sArr = explode("_", $sort_by);
            foreach ($sArr as &$item) {
                $item = ucfirst($item);
            }
            $methodName = implode("", $sArr);
            if (method_exists($this, "getSortBy" . $methodName)) {
                $sorted = true;
                $query = $this->{"getSortBy" . $methodName}($query);
            }

        }
        if (!$sorted) {
            return $query->orderBy("created_at", "DESC");;
        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    private function getSortByNew($query)
    {
        return $query->orderBy("created_at", "DESC");
    }

    /**
     * @param $query
     * @return mixed
     */
    private function getSortByViewCount($query)
    {
        return $query->orderBy("view_count", "DESC");
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByCategories($query)
    {
        if ($category = $this->getRequestItem("category_id")) {
            $query = $query->whereHas("courseCategories", function ($q) use ($category) {
                if (is_array($category) && $category) {
                    $q = $q->whereIn("category_id", $category);
                } else {
                    $q = $q->where("category_id", $category);
                }
                return $q;
            });


        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByEnrollments($query)
    {
        if ($enrollment = $this->getRequestItem("enrollment_id")) {
            $query = $query->whereHas("courseEnrollments", function ($q) use ($enrollment) {

                if (is_array($enrollment) && $enrollment) {
                    $q = $q->whereIn("enrollment_id", $enrollment);
                } else {
                    $q = $q->where("enrollment_id", $enrollment);
                }
                return $q;
            });
        }
        return $query;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getActiveTextAttribute()
    {
        return trans('course::course.statuses.' . $this->active);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseActivities()
    {
        return $this->hasMany('Modules\Course\Models\Course\CourseActivity')->active()->orderBy("sort");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseCategory()
    {
        return $this->belongsTo('Modules\Course\Models\Course\CourseCategory', 'category_id')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseFormatConfigs()
    {
        return $this->hasMany('Modules\Course\Models\Course\CourseFormatConfig')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseSections()
    {
        return $this->hasMany('Modules\Course\Models\Course\CourseSection')->active()->orderBy('sort');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseTeachers()
    {
        return $this->hasMany('Modules\Course\Models\Course\CourseTeacher', "course_id")->active();
    }

    /**
     * @return mixed
     */
    public function teachers()
    {
        return $this->belongsToMany('Modules\Course\Models\Teacher\Teacher', 'course_teachers')->wherePivot('active', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courseCategories()
    {
        return $this->belongsToMany('Modules\Course\Models\Course\CourseCategory', 'course_category_relations', null, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function metaGroups()
    {
        return $this->belongsToMany('Modules\Core\Models\Meta\MetaGroup', 'course_meta_groups');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseMetaGroups()
    {
        return $this->hasMany('Modules\Course\Models\Course\CourseMetaGroup')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseEnrollments()
    {
        return $this->hasMany('Modules\Course\Models\Enrollment\CourseEnrollment')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseUsersEnrollment()
    {
        return $this->hasMany('Modules\Course\Models\Enrollment\CourseUserEnrollment')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseUsersManualEnrollment()
    {
        $enrollment = Enrollment::active()->where("alias", "manual")->first();
        return $this->hasMany('Modules\Course\Models\Enrollment\CourseUserEnrollment')->active()->where(function ($q)use($enrollment){
            if($enrollment){
                $q->where("enrollment_id", $enrollment->id);
            }else{
                $q->where("enrollment_id", 0);
            }
        })->with("user");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseUsersPackageEnrollment()
    {
        $enrollment = Enrollment::active()->where("alias", "package")->first();
        return $this->hasMany('Modules\Course\Models\Enrollment\CourseUserEnrollment')->active()->where(function ($q)use($enrollment){
            if($enrollment){
                $q->where("enrollment_id", $enrollment->id);
            }else{
                $q->where("enrollment_id", 0);
            }
        })->with("user");

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseRelations()
    {
        return $this->hasMany('Modules\Course\Models\Course\CourseRelation', "course_id")->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function coursePackageEnrollment()
    {
        return $this->hasMany(Enrollment\CoursePackageEnrollment::class)->active();
    }

}
