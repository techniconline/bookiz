<?php

namespace Modules\Course\Traits\Course;



trait CourseFormatConfig
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course');
    }
}
