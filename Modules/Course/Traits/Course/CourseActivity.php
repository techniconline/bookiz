<?php

namespace Modules\Course\Traits\Course;


use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Course\Traits\Page\ActivityViewModelTrait;
use Modules\Exam\Models\ExamAttempt;

trait CourseActivity
{
    use ActivityViewModelTrait;

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'show_type.required' => trans('course::errors.course.show_type_required'),
            'course_id.required' => trans('course::errors.course.course_id_required'),
            'type.required' => trans('course::errors.course.type_required'),
            'title.required' => trans('course::errors.course.title_required'),

        ];
        return $this;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getActiveDateByMiniFormatAttribute()
    {
        $value = $this->active_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.medium'));
        }
        return $value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getExpireDateByMiniFormatAttribute()
    {
        $value = $this->expire_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.medium'));
        }
        return $value;
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        $newTypes = [];
        foreach ($this->types as $key => $type) {
            $newTypes[$key] = trans("course::course.activity." . $key);
        }
        return $newTypes;
    }

    /**
     * @return array
     */
    public function getShowTypes()
    {
        $newTypes = [];
        foreach ($this->show_types as $key => $type) {
            $newTypes[$key] = trans("course::course.activity." . $key);
        }
        return $newTypes;

    }

    /**
     * @return null|string
     */
    public function getUrlActivityAttribute()
    {
        if ($this->hasActivityAccess($this)) {
            return route('course.getActivity', ['course_id' => $this->course_id, 'activity_id' => $this->id]);
        }
        return null;
    }

    /**
     * @return null|string
     */
    public function getUrlPageActivityAttribute()
    {
        if ($this->hasActivityAccess($this)) {
            $url = route('course.activity', ['course_id' => $this->course_id, 'activity_id' => $this->id]);
            return app("getInstanceObject")->getCurrentUrl($url);
        }
        return null;
    }

    /**
     * @return bool
     */
    public function getHasAccessAttribute()
    {
        $this->userRole = $this->isUserEnroll();

        return $this->hasActivityAccess($this);
    }

    /**
     * @return null
     */
    public function getSingleAmountTextAttribute()
    {
        if ($this->single_currency && $this->single_amount) {
            return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->single_amount, $this->single_currency, true);
        }
        return null;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getShowTypeTextAttribute()
    {
        return trans('course::course.activity.' . ($this->show_type));
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getIsPublicTextAttribute()
    {
        return trans('course::course.activity.' . ($this->is_public ? "is_public_true" : "is_public_false"));
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getParamsAttribute($value)
    {
        if ($value) {
            $value = json_decode($value);
        }
        return $value;
    }

    public function getParametersAttribute()
    {
        $params = $this->params;
        if ($this->type == "file") {

            foreach ($params->files as $file) {
                unset($file->src);
                $file->size = $file->size ? ($file->size / 1024) / 1024 : $file->size;
                $file->size_type = "Mb";
            }

        }

        return $params;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getParamsArrayAttribute()
    {
        if ($value = $this->attributes['params']) {
            $value = json_decode($value, true);
        }
        return $value;
    }

    /**
     * @return null|string
     */
    public function getParamsTextAttribute()
    {
        $value = null;
        if ($value = $this->params) {
            $value = json_encode($value);
        }
        return $value;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseActivityItems()
    {
        return $this->hasMany('Modules\Course\Models\Course\CourseActivityItem', 'ca_id')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseSection()
    {
        return $this->belongsTo('Modules\Course\Models\Course\CourseSection');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function examAttempts()
    {
        return $this->hasMany(ExamAttempt::class)->enable();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function examAttemptsPassed()
    {
        return $this->hasMany(ExamAttempt::class)->enable();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function examAttemptsLive()
    {
        return $this->hasMany(ExamAttempt::class)->enable()->whereIn('status', [ExamAttempt::STATUS_NEW, ExamAttempt::STATUS_OPEN])->where('end_time', '>=', date("Y-m-d H:i:s"));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function examAttemptsNotAction()
    {
        return $this->hasMany(ExamAttempt::class)->enable()->whereIn('status', [ExamAttempt::STATUS_NEW, ExamAttempt::STATUS_OPEN])->where('end_time', '<', date("Y-m-d H:i:s"));
    }

}
