<?php

namespace Modules\Course\Traits\Course;


trait CourseActivityItem
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'item_id.required' => trans('course::errors.course.item_id_required'),
            'ca_id.required' => trans('course::errors.course.course_id_required'),
            'type.required' => trans('course::errors.course.type_required'),

        ];
        return $this;
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        $newTypes = [];
        foreach ($this->types as $key => $type) {
            $newTypes[$key] = trans("course::course.activity.items." . $key);
        }
        return $newTypes;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseActivity()
    {
        return $this->belongsTo('Modules\Course\Models\Course\CourseActivity', 'ca_id');
    }

}
