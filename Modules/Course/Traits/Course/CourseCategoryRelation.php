<?php

namespace Modules\Course\Traits\Course;


trait CourseCategoryRelation
{

    public function scopeIsVisible($query)
    {
        $query = $query->join("courses", function ($join) {
            $join->on('courses.id', '=', $this->getTable() . '.course_id')->where("courses.active", 1);
        })->join("course_categories", function ($join) {
            $join->on('course_categories.id', '=', $this->getTable() . '.category_id')->where("course_categories.active", 1);
        });
        return $query;
    }

    public function scopeSortBy($query)
    {
        if ($sort_by = $this->getRequestItem("sort_by")) {
            $sArr = explode("_", $sort_by);
            foreach ($sArr as &$item) {
                $item = ucfirst($item);
            }
            $methodName = implode("", $sArr);
            if(method_exists($this, "getSortBy".$methodName)){
                $query = $this->{"getSortBy".$methodName}($query);
            }

        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    private function getSortByNew($query)
    {
        $query = $query->whereHas("course", function ($q){
            return $q->orderBy("created_at", "DESC");
        });
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    private function getSortByViewCount($query)
    {
        $query = $query->whereHas("course", function ($q){
            return $q->orderBy("view_count", "DESC");
        });
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByCategories($query)
    {

        if ($category = $this->getRequestItem("category_id")) {

            $query = $query->whereHas("courseCategories", function ($q)use($category){
                if (is_array($category) && $category) {
                    $q = $q->whereIn("category_id", $category);
                } else {
                    $q = $q->where("category_id", $category);
                }
                return $q;
            });


        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByEnrollments($query)
    {
        if ($enrollment = $this->getRequestItem("enrollment_id")) {
            $query = $query->whereHas("course.courseEnrollments", function ($q)use($enrollment){

                if (is_array($enrollment) && $enrollment) {
                    $q = $q->whereIn("enrollment_id", $enrollment);
                } else {
                    $q = $q->where("enrollment_id", $enrollment);
                }
                return $q;
            });
        }
        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseCategory()
    {
        return $this->belongsTo('Modules\Course\Models\Course\CourseCategory', 'category_id')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseCategoryByFilter()
    {
        return $this->belongsTo('Modules\Course\Models\Course\CourseCategory', 'category_id')->active()->filterLanguage()->filterCurrentInstance();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseByFilter()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course')->active()->filterLanguage()->filterCurrentInstance();
    }
}
