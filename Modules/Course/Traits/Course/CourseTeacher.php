<?php

namespace Modules\Course\Traits\Course;

trait CourseTeacher
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'teacher_id.required' => trans('course::errors.course.teacher_id_required'),
            'course_id.required' => trans('course::errors.course.course_id_required'),

        ];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teacher()
    {
        return $this->belongsTo('Modules\Course\Models\Teacher\Teacher');
    }
}
