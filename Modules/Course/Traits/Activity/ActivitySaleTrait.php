<?php

namespace Modules\Course\Traits\Activity;

use BridgeHelper;
use DateHelper;
use Modules\Course\Models\Enrollment\CourseUserEnrollment;

trait ActivitySaleTrait
{
    public function getCartData($activity,$course_id){
        $item = [
            'item_type' => 'course_activity',
            'item_id' => $activity->id,
            'options' => [
                'course_id' => $course_id,
            ],
            'cost' => null,
            'sale_cost' => $activity->single_amount,
            'text' => $this->getPriceText($activity->single_amount,$activity->single_currency),
            'percent_discount' => 0,
            'currency' => $activity->single_currency,
        ];
        $item['name']=$activity->title;
        $item['options']['product_key']='course_'.$course_id.'_activity_'.$activity->id;
        $item['qty']=1;
        return $item;
    }

    public function getPriceText($price,$currency){
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($price,$currency,true);
    }

    public function addUserToActivity($user_id,$activity_id,$course_id){
        $time = time() + 157680000;
        $expireDate = DateHelper::setDateTime(date(trans('core::date.database.datetime'), $time), null, trans('core::date.database.datetime'))->getDateTime(trans('core::date.database.datetime'));
        $userEnroll = new CourseUserEnrollment();
        $userEnroll->user_id = $user_id;
        $userEnroll->course_id = $course_id;
        $userEnroll->activity_id = $activity_id;
        $userEnroll->type = CourseUserEnrollment::TYPE_ACTIVITY;
        $userEnroll->active_date = date(trans('core::date.database.datetime'), time());
        $userEnroll->expire_date = $expireDate;
        $userEnroll->active = 1;
        return $userEnroll->save();
    }
}
