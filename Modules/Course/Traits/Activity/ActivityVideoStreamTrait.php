<?php

namespace Modules\Course\Traits\Activity;

use BridgeHelper;
use DateHelper;
use Modules\Course\Models\Activity\CourseVideoStreamUsers;
use Modules\User\Models\User;

trait ActivityVideoStreamTrait
{
    private $API_URL = 'https://www.skyroom.online/skyroom/api/';
    private $API_KEY = 'apikey-23998-735-f84bfe395e3b3d68e00ade61217297c7';

    /**
     * @param $students
     * @return array
     * @throws HttpException
     * @throws JsonException
     * @throws NetworkException
     */
    public function savePersonals($students)
    {
        if (empty($students)) {
            return ['action' => false, 'result' => []];
        }

        $existsUsers = CourseVideoStreamUsers::active()->whereIn('user_id', $students)->get();
        $currentUsersIds = $existsUsers ? $existsUsers->pluck("user_id")->toArray() : [];
        $users = collect($students)->diff($currentUsersIds);
        $users = User::active()
//            ->filterCurrentInstance()
            ->whereIn("id", $users->toArray())->get();

        if ($users->isEmpty()) {
            return ['action' => false, 'result' => []];
        }

        $result = [];
        foreach ($users as $user) {
            $user_name = rand(123456789, 987654321);
            $data = [
                "username" => "std_" . $user_name,
                "password" => $user_name,
                "nickname" => $user->full_name,
                "status" => 1,
                "is_public" => false
            ];

            $res = $this->callApi('createUser', $data);

            if (isset($res["ok"]) && $res["ok"]) {
                $out_user_id = $res["result"];
                $data = [
                    'user_id' => $user->id,
                    'out_user_id' => $out_user_id,
                    'user_name' => "std_" . $user_name,
                    'password' => $user_name,
                    'active' => 1
                ];
                $model = new CourseVideoStreamUsers();
                if ($model->fill($data)->save()) {
                    $result[$user->id] = $data;
                    $result['out_user_ids'][] = $out_user_id;
                }
            }
        }
        $result = ['action' => $result ? true : false, 'result' => $result];
        return $result;
    }


    /**
     * @param $class_data
     * @return array
     * @throws HttpException
     * @throws JsonException
     * @throws NetworkException
     */
    public function saveClassRoom($class_data)
    {

        if (empty($class_data)) {
            return ['action' => false, 'result' => []];
        }

        $ext = $this->getDataParams('external_params');
        $room_id = isset($ext->room_id)?$ext->room_id:null;

        if ($class_data['room_id'] = $room_id) {
            $result = $this->callApi('updateRoom', $class_data);
            if ($result["ok"]) {
                return ['action' => true, 'result' => $result, 'room_id' => $room_id];
            }
        } else {
            $result = $this->callApi('createRoom', $class_data);
            if ($result["ok"]) {
                $this->updateActivity(['room_id' => $result['result']]);
                return ['action' => true, 'result' => $result, 'room_id' => $result['result']];
            }
        }

        return ['action' => false, 'result' => $result];
    }

    /**
     * @param $params
     * @return bool
     */
    private function updateActivity($params)
    {
        if ($activity = $this->activity) {
            $pAttr = $activity->params_array;
            $extParams['external_params'] = $params;
            $pAttr = array_merge($extParams, $pAttr);
            $activity->params = json_encode($pAttr);
            return $activity->save();
        }
        return false;
    }


    /**
     * @param array $users
     * @param $class_room
     * @param int $access
     * @return array
     * @throws HttpException
     * @throws JsonException
     * @throws NetworkException
     */
    public function addUsersToClass(array $users, $class_room, $access = 1)
    {
        if (empty($users) || !$class_room) {
            return ['action' => false, 'result' => []];
        }

        $usersData = CourseVideoStreamUsers::active()->whereIn('user_id', $users)->get()->pluck('out_user_id');

        $data = [];
        $data['room_id'] = $class_room;
        foreach ($usersData as $user) {
            $data['users'][] = ['user_id' => $user, "access" => $access];
        }

        $result = $this->callApi('addRoomUsers', $data);
        if ($result["ok"]) {
            return ['action' => true, 'result' => $result];
        }
        return ['action' => false, 'result' => $result];
    }

    /**
     * @param int $ttl
     * @return null
     * @throws HttpException
     * @throws JsonException
     * @throws NetworkException
     */
    public function getAutoLoginLink($ttl = 300)
    {
        $user = BridgeHelper::getAccess()->getUser();
        if(!$user){
            return false;
        }
        $usersData = CourseVideoStreamUsers::active()->where('user_id', $user->id)->first();
        $ext = $this->getDataParams('external_params');
        $room_id = isset($ext->room_id)?$ext->room_id:null;

        if(!$usersData || !$room_id){
            return false;
        }

        $data = [
            'user_id' => $usersData->out_user_id,
            'room_id' => $room_id,
            'ttl' => $ttl ?: 300,
        ];

        $result = $this->callApi('getLoginUrl', $data);
        if ($result["ok"]) {
            return $result["result"];
        }
        return null;
    }

    /**
     * @param $action_name
     * @param array $action_params
     * @param string $format
     * @return mixed
     * @throws HttpException
     * @throws JsonException
     * @throws NetworkException
     */
    public function callApi($action_name, $action_params = [], $format = 'json')
    {
        $contentType = 'application/json';

        $params = [
            'action' => $action_name,
        ];

        if (count($params)) {
            $params['params'] = $action_params;
        }

//        $fields_string = http_build_query($params);
        $fields_string = json_encode($params);

        $url = $this->API_URL . $this->API_KEY;

        // set request options
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            "Content-Type: $contentType",
        ));

        // make the request
        $response = curl_exec($curl);
        $errNo = curl_errno($curl);
        if ($errNo !== 0) {
            throw new NetworkException(curl_error($curl), $errNo);
        }

        // check HTTP status code
        $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if ($http_code !== 200) {
            throw new HttpException('HTTP Error', $http_code);
        }

        // decode JSON response
        $response = json_decode($response, true);
        if ($response === null) {
            throw new JsonException('Invalid Response', json_last_error());
        }

        return $response;
    }


}

// --------------
class HttpError
{
    private $code;
    private $message;

    function __construct($message, $code = 0)
    {
        $this->code = $code;
        $this->message = $message;
    }

    function getCode()
    {
        return $this->code;
    }

    function getMessage()
    {
        return $this->message;
    }

    static function IsError(&$input)
    {
        return is_object($input) && (get_class($input) === 'Skyroom\HttpError');
    }
}

class NetworkException extends \Exception
{
    public function __toString()
    {
        return __CLASS__ . ": {$this->message}\n";
    }
}

class HttpException extends \Exception
{
    public function __toString()
    {
        return __CLASS__ . ": {$this->message}\n";
    }
}

class JsonException extends \Exception
{
    public function __toString()
    {
        return __CLASS__ . ": {$this->message}\n";
    }
}
