<?php

namespace Modules\Course\Traits\Page;


use BridgeHelper;
use Modules\Course\Libraries\Activity\MasterActivity;
use Modules\Course\Models\Enrollment\CourseUserEnrollment;

trait ActivityViewModelTrait
{

    private $accessActivity=false;
    private $activityIcons=[];
    private $activityClass=[];

    public function getActivityDuration($duration){

        return $duration.' '.trans('course::course.times.minute');
    }

    public function getActivityPrice($activity,$formated=false){
        if($activity->single_sales=='disabled'){
            return false;
        }
        return BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($activity->single_amount,$activity->single_currency,$formated);
    }

    public function getActivityCartBtn($activity){
        if($this->canSaleActivity($activity)){
            $data=[
                'item_type'=>'course_activity',
                'item_id'=>$activity->id,
                'options'=>[
                    'course_id'=>$activity->course_id,
                ],
            ];
            return BridgeHelper::getCartHelper()->getAddToCart($data['item_type'],$data['item_id'],$data['options']);
        }
        return '';
    }

    public function canSaleActivity($activity){
        if($activity->single_sales=='disabled') {
            return false;
        }
        if($activity->single_sales=='only_students' && $this->isEnroll()){
            return true;
        }elseif($activity->single_sales=='only_users' && !$this->isEnroll()){
            return true;
        }else{
            return true;
        }
    }

    /**
     * @param $activity
     * @return bool
     */
    public function hasActivityAccess($activity){

        if(!$this->checkDateActivity($activity)){
            return false;
        }

        if($activity->show_type=='free'){
            return true;
        }
        if($activity->single_sales=='only_users' || $activity->single_sales=='all_user'){
            $accessActivity=$this->getUserActivityAccess(null,$activity->course_id);
            if(isset($accessActivity[$activity->id])){
                return true;
            }
            return false;
        }
        if($this->userRole){
            if($activity->single_sales=='only_student'){
                $accessActivity=$this->getUserActivityAccess(null,$activity->course_id);
                if(isset($accessActivity[$activity->id])){
                    return true;
                }
                return false;
            }else{
                return true;
            }
        }
        return false;
    }

    /**
     * @param $activity
     * @return bool
     */
    public function checkDateActivity($activity)
    {
        $active_date = $activity->active_date;
        $expire_date = $activity->expire_date;

        if($active_date && (time() < strtotime($active_date))){
            return false;
        }elseif ($expire_date && (time() > strtotime($expire_date))){
            return false;
        }else{
            return true;
        }
    }

    /**
     * @return bool
     */
    public function isUserEnroll()
    {
        if(isset($this->userRole)){
            return $this->userRole;
        }else{
            $course = app('getInstanceObject')->getRelationalData('course');
            if(isset($course->userRole)){
                return $course->userRole;
            }
            return false;
        }
    }

    public function getUserActivityAccess($user_id=null,$course_id=null){
        if($this->accessActivity){
            return $this->accessActivity;
        }
        $this->accessActivity=[];
        if (is_null($course_id)) {
            $course_id = $this->course->id;
        }
        if (is_null($user_id)) {
            $user = BridgeHelper::getAccess()->getUser();
            if (!$user) {
                return $this->accessActivity;
            }
            $user_id= $user->id;
        }
        $courseActivity=CourseUserEnrollment::where('course_id',$course_id)->where('user_id',$user_id)
            ->where('type', CourseUserEnrollment::TYPE_ACTIVITY)->select('activity_id')->get();
        if($courseActivity){
            foreach($courseActivity as $activity){
                $this->accessActivity[$activity->activity_id]=true;
            }
        }
        return $this->accessActivity;
    }

    public function getActivityIcon($activity){
        if(!isset($this->activityIcons[$activity->type])){
            $activityClass=$this->getActivityObject($activity);
            $this->activityIcons[$activity->type]=$activityClass->getIcon();
        }
        return $this->activityIcons[$activity->type];
    }

    public function getActivityObject($activity){
        if(!isset($this->activityClass[$activity->id])){
            $masterActivity=new MasterActivity();
            $this->activityClass[$activity->id]=$masterActivity->setActivity($activity);
        }
        return $this->activityClass[$activity->id];
    }

}
