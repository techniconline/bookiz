<?php

namespace Modules\Course\Traits\Page;

use Illuminate\Support\Facades\Cache;
use BridgeHelper;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Course\CourseTeacher;
use Modules\Course\Models\Enrollment\CourseUserEnrollment;
use Modules\Course\Models\Course\CourseActivity;
use Modules\Course\Models\Course\CourseSection;

trait InfoViewModelTrait
{
    private $course_id = null;
    private $use_course;
    private $params_config;

    public function getCourseInfoArray($fields = [], $course_id = null)
    {
        if (!is_array($fields) || !count($fields)) {
            return collect([]);
        }
        if (is_null($course_id)) {
            $course_id = $this->course->id;
        }
        $this->course_id = $course_id;
        $key = 'course_info_by_id_' . $course_id;
        $info = Cache::get($key);
        if (true || !$info) {
            $info = [];
            foreach ($fields as $field) {
                $data = $this->fieldHandler($field);
                if ($data && is_array($data)) {
                    list($title, $value) = $data;
                    $info[] = (object)['label' => $title, 'value' => $value];
                }
            }
            $info = collect($info);
            Cache::put($key, $info, 1440);
        }
        return $info;
    }


    private function fieldHandler($field)
    {
        $functionName = 'get' . ucfirst($field);
        if (method_exists($this, $functionName)) {
            return $this->$functionName();
        } else {
            //return $this->getFromFeatureGroup($field);
        }
        return false;
    }

    public function setUseCourse($course_id)
    {
        if($this->use_course && $this->use_course->id == $course_id){
            return $this;
        }
        if (isset($this->course) && $this->course) {
            $this->use_course = $this->course;
            return $this;
        }
        $this->use_course = Course::find($course_id);
        return $this->setParamsConfig();
    }

    public function setParamsConfig()
    {
        if($this->use_course){
            $this->params_config = $this->use_course->params_json;
        }
        return $this;
    }

    public function getStudents($course_id = null, $onlyvalue = false)
    {
        if (!$course_id) {
            $course_id = $this->course_id;
        }
        $this->setUseCourse($course_id);
//        $students = CourseUserEnrollment::active()->where('course_id', $course_id)->count();

        $resCountStudents = $this->use_course->count_students;

        if(!isset($this->params_config->min_show_item_enrollment_user)){
            if($this->params_config){
                $this->params_config->min_show_item_enrollment_user = 5;
            }else{
                $this->params_config = new \stdClass();
                $this->params_config->min_show_item_enrollment_user = 5;
            }
        }

        if(((int)$this->params_config->min_show_item_enrollment_user > $resCountStudents['value'])){
            return null;
        }

        $students = $resCountStudents['value']. ' ' .$resCountStudents['unit'];

        if ($onlyvalue) {
            return $students;
        }
        $title = $resCountStudents['label'];
        return [$title, $students];
    }

    public function getDuration($course_id = null, $onlyvalue = false)
    {
        if (!$course_id) {
            $course_id = $this->course_id;
        }

        $this->setUseCourse($course_id);

        //return sections
        if (isset($this->params_config->default_show_session_time) && $this->params_config->default_show_session_time == 'session') {
            return $this->getSessions($course_id, $onlyvalue);
        }

        $duration = CourseActivity::active()->where('active', 1)->where('course_id', $course_id)->sum('duration');
        $unit = BridgeHelper::getConfig()->getSettings('default_time_unit', 'instance', 'course');
        if (!$unit) {
            $unit = 'minute';
        }
        if ($unit != 'minute' && $duration > 0) {
            if ($unit == 'hour') {
                $duration = round($duration / 60, 0);
            }
        }
        $duration = $duration . ' ' . trans('course::course.info_unit.' . $unit);
        if ($onlyvalue) {
            return $duration;
        }
        $title = trans('course::course.info.duration');
        return [$title, $duration];
    }


    public function getTeachers($course_id = null, $onlyvalue = false)
    {
        if (!$course_id) {
            $course_id = $this->course_id;
        }
        $teachers = CourseTeacher::active()->with(['teacher.user'])->where('course_id', $course_id)->get();
        $text = '';
        foreach ($teachers as $key => $teacher) {
            if ($key > 0) {
                $text .= trans('course::teacher.text_separator');
            }
            $text .= $teacher->teacher->user->first_name . ' ' . $teacher->teacher->user->last_name;
        }

        if ($onlyvalue) {
            return $text;
        }
        if ($teachers->count() == 1) {
            $title = trans('course::course.info.teacher');
        } else {
            $title = trans('course::course.info.teachers');
        }
        return [$title, $text];
    }

    public function getSessions($course_id = null, $onlyvalue = false)
    {
        if (!$course_id) {
            $course_id = $this->course_id;
        }
        $sessions = CourseActivity::active()->where('active', 1)->where('course_id', $course_id)->count();
        $sessions = $sessions . ' ' . trans('course::course.info_unit.sessions');
        if ($onlyvalue) {
            return $sessions;
        }
        $title = trans('course::course.info.sessions');
        return [$title, $sessions];
    }

    public function getQuiz($course_id = null, $onlyvalue = false)
    {
        if (!$course_id) {
            $course_id = $this->course_id;
        }
        //$duration=CourseActivity::where('active',1)->where('course_id',$this->course_id)->count();
        $quiz = 0;
        $quiz = $quiz . ' ' . trans('course::course.info_unit.quiz');
        if ($onlyvalue) {
            return $quiz;
        }
        $title = trans('course::course.info.quiz');
        return [$title, $quiz];
    }

    public function getSection($course_id = null, $onlyvalue = false)
    {
        if (!$course_id) {
            $course_id = $this->course_id;
        }
        $sections = CourseSection::active()->where('active', 1)->where('course_id', $course_id)->count();
        $sections = $sections . ' ' . trans('course::course.info_unit.section');
        if ($onlyvalue) {
            return $sections;
        }
        $title = trans('course::course.info.section');
        return [$title, $sections];
    }

}
