<?php
namespace Modules\Course\Traits\Page;

use Modules\Course\Models\Course\CourseTeacher;
trait TeachersViewModelTrait
{

    public function getCourseTeachers($course_id=null,$view=null){
        if(is_null($course_id)){
            $course_id=$this->course->id;
        }
        $teachers=$this->getAllCourseTeachers($course_id);
        if(is_null($view)){
            $view='course::teacher.course.list';
        }
        return view($view,['teachers'=>$teachers]);
    }

   public function getAllCourseTeachers($course_id){
        $teachers=CourseTeacher::with(['teacher.user'])->where('course_id',$course_id)->get();
        return $teachers;
   }

}
