<?php

namespace Modules\Course\Traits\Page;


use BridgeHelper;

trait ParamsViewModelTrait
{
    private $params_default=[
        'access_comment'=>'only_user',
        'confirm_comment'=>'by_admin',
        'access_rate'=>'only_user',
        'show_section_title'=>'enable',
        'show_sections'=>'enable',
        'show_default_section_title'=>'enable',
        'view_status_sections'=>'first_open',
    ];

    public function getSectionTitle($sectionTitle,$index){
        $title=trans('course::course.activity.section').' '.$index;
        if($this->canShowSectionTitle()){
            if(isset($this->course->params_json->show_default_section_title)){
                $value=$this->course->params_json->show_default_section_title;
            }else{
                $value=$this->params_default['show_default_section_title'];
            }
            $value=($value=='enable')?true:false;
            if($value){
                $title=$title.' - ';
            }else{
                $title='';
            }
            return $title.$sectionTitle;
        }
        return $title;
    }
    public function canShowSectionTitle(){
        if(isset($this->course->params_json->show_section_title)){
            $value=$this->course->params_json->show_section_title;
        }else{
            $value=$this->params_default['show_section_title'];
        }
        return ($value=='enable')?true:false;
    }

    public function canShowSections(){
        if(isset($this->course->params_json->show_sections)){
            $value=$this->course->params_json->show_sections;
        }else{
            $value=$this->params_default['show_sections'];
        }
        return ($value=='enable')?true:false;
    }

    public function canShowQtoaSection(){
        if(!$this->isEnroll()){
            return false;
        }
        if(isset($this->course->params_json->active_qtoa)){
            $value=$this->course->params_json->active_qtoa;
        }else{
            $value=isset($this->params_default['active_qtoa'])?$this->params_default['active_qtoa']:'disable';
        }
        return ($value=='enable')?true:false;
    }


    public function canOpenSection($section_pos){
        if(isset($this->course->params_json->view_status_sections)){
            $value=$this->course->params_json->view_status_sections;
        }else{
            $value=$this->params_default['view_status_sections'];
        }
        if($value=='all_open'){
            return true;
        }elseif($value=='first_open' && $section_pos==1){
            return true;
        }
        return false;
    }


    public function canAccessComment(){
        if(isset($this->course->params_json->access_comment)){
            $value=$this->course->params_json->access_comment;
        }else{
            $value=$this->params_default['access_comment'];
        }
        if($value==='disable'){
            return false;
        }
        if($value==='only_user'){
            if(BridgeHelper::getAccess()->isLogin()){
                return true;
            }
            return false;
        }
        return true;
    }


    public function canAccessRate(){
        if(isset($this->course->params_json->access_rate)){
            $value=$this->course->params_json->access_rate;
        }else{
            $value=$this->params_default['access_rate'];
        }
        if($value==='disable'){
            return false;
        }
        if($value==='only_user'){
            if(BridgeHelper::getAccess()->isLogin()){
                return true;
            }
            return false;
        }
        return true;
    }
}
