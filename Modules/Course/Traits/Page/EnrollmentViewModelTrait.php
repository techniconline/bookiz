<?php

namespace Modules\Course\Traits\Page;


use BridgeHelper;
use DateHelper;
use Modules\Course\Libraries\Enrollment\MasterEnrollment;
use Modules\Course\Models\Enrollment\CourseEnrollment;
use Modules\Course\Models\Enrollment\CourseUserEnrollment;
use Modules\Course\Models\Enrollment;
use Illuminate\Support\Facades\Cache;

trait EnrollmentViewModelTrait
{

    private $enrollmentPriority = null;
    private $courseEnrollment = false;
    private $userRole = null;

    public function getCourseEnrollment($course_id, $enrollment_id)
    {
        if (is_null($course_id)) {
            $course_id = $this->course->id;
        }
        if ($this->userRole) {
            return false;
        }
        $key = "course_enrollment_".$enrollment_id."_course_".$course_id;

        if(Cache::has($key)){
            $courseEnrollment = Cache::get($key);
        }else{
            $courseEnrollment = CourseEnrollment::with(['enrollment'])->where('enrollment_id', $enrollment_id)->where('course_id', $course_id)->active()->first();
            Cache::put($key, $courseEnrollment, 360);
        }

        $key_enrollment = "enrollment_".$enrollment_id;
        if(Cache::has($key_enrollment)){
            $enrollment = Cache::get($key_enrollment);
        }else{
            $enrollment = Enrollment::active()->find($courseEnrollment->enrollment_id);
            Cache::put($key_enrollment, $enrollment, 360);
        }

        if ($this->onEnrollTime($courseEnrollment)) {
            $master = new MasterEnrollment();
            $master->setCourseId($course_id);
            $data = ($courseEnrollment) ? $courseEnrollment->params_json : [];
            $this->courseEnrollment =& $courseEnrollment;
            return $master->getEnrollment($enrollment, $data);
        }
        return false;
    }

    public function getCourseEnrollments($course_id = null, $viewType = 'content', $returnJson = false)
    {
        if ($this->userRole) {
            return null;
        }
        if (is_null($course_id)) {
            $course_id = $this->course->id;
        }
        $enrollments = $this->getAvailableEnrollment($course_id);
        if ($enrollments->count()) {
            $master = new MasterEnrollment();
            $master->setCourseId($course_id);
            $html = [];
            foreach ($enrollments as $courseEnrollment) {
                $data = ($courseEnrollment) ? $courseEnrollment->params_json : [];
                $html[] = $master->getEnrollment($courseEnrollment->enrollment, $data)->getRenderView($viewType, null, $returnJson);
            }
            return $returnJson ? array_filter($html) : implode(" ", $html);
        } else {
            return $this->getCloseEnrollment($course_id, $viewType, $returnJson);
        }
    }

    /**
     * @param $course_id
     * @param $viewType
     * @param $returnJson
     * @return mixed
     */
    private function getCloseEnrollment($course_id, $viewType, $returnJson)
    {
        $master = new MasterEnrollment();
        $master->setCourseId($course_id);
        $enrolment = new \stdClass();
        $enrolment->alias = "close";
        $html[] = $master->getEnrollment($enrolment, [])->getRenderView($viewType, null, $returnJson);
        return $returnJson ? $html : implode(" ", $html);
    }

    public function getAvailableEnrollment($course_id)
    {
        $enrollments = CourseEnrollment::with(['enrollment'])->where('course_id', $course_id)->active()->get();
        $list = [];
        foreach ($enrollments as $enrollment) {
            if ($this->onEnrollTime($enrollment)) {
                $enrollment->position = $enrollment->enrollment->priority;
                $list[] = $enrollment;
            }
        }
        return collect($list)->sortByDesc('position');
    }


    public function onEnrollTime($enrollment)
    {
        if (is_null($enrollment->start_date) && is_null($enrollment->end_date)) {
            return true;
        }
        $now = time();
        $start = ($enrollment->start_date) ? strtotime($enrollment->start_date) : 0;
        $end = ($enrollment->end_date) ? strtotime($enrollment->end_date) : 0;

        if ($start && $end) {
            if ($now >= $start && $now <= $end) {
                return true;
            }
        } elseif ($start && !$end) {
            if ($now >= $start) {
                return true;
            }
        } elseif (!$start && $end) {
            if ($now <= $end) {
                return true;
            }
        }
        return false;
    }


    public function getEnrollmentOrder($enrollment_id)
    {
        $list = $this->getAllEnrollmentByPosition();
        if (isset($list[$enrollment_id])) {
            return $list[$enrollment_id];
        }
        return 0;
    }

    public function getAllEnrollmentByPosition()
    {
        if ($this->enrollmentPriority) {
            return $this->enrollmentPriority;
        }
        $all = Enrollment::getAllEnrollment();
        $this->enrollmentPriority = [];
        foreach ($all as $enrollment) {
            $this->enrollmentPriority[$enrollment->id] = $enrollment->priority;
        }
        return $this->enrollmentPriority;
    }

    public function setUserRoleAndAccessOnCourse($course_id,$user_id=null)
    {
        if (!is_null($this->userRole)) {
            return $this->userRole;
        }
        if (is_null($course_id)) {
            $course_id = $this->course->id;
        }
        if(is_null($user_id)){
            $user = BridgeHelper::getAccess()->getUser();
            if($user){
                $user_id=$user->id;
            }
        }
        if (!$user_id) {
            $this->userRole = false;
            return $this->userRole;
        }

        $cacheKey='course_enroll_check_'.$course_id.'_'.$user_id;
        if(Cache::has($cacheKey)){
            $this->userRole=Cache::get($cacheKey);
            return  $this->userRole;
        }

        $userEnrollment = CourseUserEnrollment::with('role')
            ->where('active', 1)
            ->where('user_id', $user_id)
            ->where('course_id', $course_id)
            ->active()
            ->first();
        if ($userEnrollment) {
            $now = time();
            $end = ($userEnrollment->expire_date) ? strtotime($userEnrollment->expire_date) : $now + 86400;
            if ($now <= $end) {
                $this->userRole = $userEnrollment->role;
                Cache::put($cacheKey,$this->userRole,60);
                return $this->userRole;
            }
        }
        $this->userRole = false;
        return $this->userRole;
    }

    /**
     * @param string $role
     * @return bool
     */
    public function hasAccess($role = 'teacher')
    {
        if($this->userRole){
            if($this->userRole->name == $role){
                return true;
            }
            return false;
        }
        return false;
    }


    public function enrollUserOnCourse($user_id, $enrollment_id, $course_id, $params = null)
    {
        if (!$this->courseEnrollment) {
            $this->courseEnrollment = CourseEnrollment::with(['enrollment'])->where('enrollment_id', $enrollment_id)->where('course_id', $course_id)->first();
        }
        if ($this->courseEnrollment) {
            $expireType = $this->courseEnrollment->duration_type;
            if ($expireType == 'expierd_date') {
                $expireDate = $this->courseEnrollment->end_date;
            } elseif ($expireType == 'expierd_duration' && $this->courseEnrollment->duration > 0) {
                $time = time() + ($this->courseEnrollment->duration * 86400);
                $expireDate = DateHelper::setDateTime(date(trans('core::date.database.datetime'), $time), null, trans('core::date.database.datetime'))->getDateTime(trans('core::date.database.datetime'));
            } else {
                $time = time() + 157680000;
                $expireDate = DateHelper::setDateTime(date(trans('core::date.database.datetime'), $time), null, trans('core::date.database.datetime'))->getDateTime(trans('core::date.database.datetime'));
            }

            if(!$expireDate){
                $time = time() + 157680000;
                $expireDate = DateHelper::setDateTime(date(trans('core::date.database.datetime'), $time), null, trans('core::date.database.datetime'))->getDateTime(trans('core::date.database.datetime'));
            }

            $userEnroll = new CourseUserEnrollment;
            $userEnroll->user_id = $user_id;
            $userEnroll->course_id = $course_id;
            $userEnroll->enrollment_id = $enrollment_id;
            $userEnroll->params = $params;
            $userEnroll->course_enrollment_id = $this->courseEnrollment->id;
            $userEnroll->role_id = $this->courseEnrollment->role_id;
            $userEnroll->active_date = date(trans('core::date.database.datetime'), time());
            $userEnroll->expire_date = $expireDate;
            $userEnroll->active = 1;
            return $userEnroll->save();
        }
        return false;
    }

    public function enrollUserOnCoursePackage($user_id, $enrollment_id, $course_id, $package_id)
    {
        $enrollment = Enrollment::active()->where("alias", "package")->first();
        if(!$enrollment){
            return false;
        }

        $p_enrollment_id = $enrollment->id;
        $whereOfPackage = '"course_package_id":"'.$package_id.'"';
        $courseEnrollment = CourseEnrollment::with(['enrollment'])->where('params','LIKE','%'.$whereOfPackage.'%')->where('enrollment_id', $p_enrollment_id)->where('course_id', $course_id)->first();
        $this->courseEnrollment = CourseEnrollment::with(['enrollment'])->where('enrollment_id', $enrollment_id)->where('course_id', $package_id)->first();

        if ($this->courseEnrollment && $courseEnrollment) {
            $expireType = $this->courseEnrollment->duration_type;
            if ($expireType == 'expierd_date') {
                $expireDate = $this->courseEnrollment->end_date;
            } elseif ($expireType == 'expierd_duration' && $this->courseEnrollment->duration > 0) {
                $time = time() + ($this->courseEnrollment->duration * 86400);
                $expireDate = DateHelper::setDateTime(date(trans('core::date.database.datetime'), $time), null, trans('core::date.database.datetime'))->getDateTime(trans('core::date.database.datetime'));
            } else {
                $time = time() + 157680000;
                $expireDate = DateHelper::setDateTime(date(trans('core::date.database.datetime'), $time), null, trans('core::date.database.datetime'))->getDateTime(trans('core::date.database.datetime'));
            }
            if(!$expireDate){
                $time = time() + 157680000;
                $expireDate = DateHelper::setDateTime(date(trans('core::date.database.datetime'), $time), null, trans('core::date.database.datetime'))->getDateTime(trans('core::date.database.datetime'));
            }
            $userEnroll = new CourseUserEnrollment;
            $userEnroll->user_id = $user_id;
            $userEnroll->course_id = $course_id;
            $userEnroll->enrollment_id = $p_enrollment_id;
            $userEnroll->course_enrollment_id = $courseEnrollment->id;
            $userEnroll->role_id = $this->courseEnrollment->role_id;
            $userEnroll->active_date = date(trans('core::date.database.datetime'), time());
            $userEnroll->expire_date = $expireDate;
            $userEnroll->active = 1;
            return $userEnroll->save();
        }
        return false;
    }

    public function getUserEnrollmentCourses($user_id = null, $limit = 20, $page = 0, $order = 'id')
    {
        if (is_null($user_id)) {
            $user_id = BridgeHelper::getAccess()->getUser()->id;
        }
        $userCourses = CourseUserEnrollment::with(['course'])->active()->where('expire_date', '>=', date(trans('core::date.database.datetime')))->where('user_id', $user_id)->orderBy($order)->get();
        if ($userCourses->count()) {
            return $userCourses;
        }
        return null;
    }

    public function isEnroll()
    {
        if ($this->userRole) {
            return true;
        }
        return false;
    }

}
