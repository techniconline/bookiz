<?php

namespace Modules\Course\Traits\Enrollment;


use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;

trait CoursePackageEnrollment
{

    public $format_date;

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'course_id.required' => trans('course::errors.enrollment.course_id_required'),
            'package_course_id.required' => trans('course::errors.enrollment.package_course_id_required'),
        ];
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getParamsJsonAttribute()
    {
        if ($this->params) {
            return json_decode($this->params);
        }
        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coursePackage()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course', 'package_course_id');
    }
}
