<?php

namespace Modules\Course\Traits\Enrollment;


use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;

trait CourseUserEnrollment
{

    public $format_date;

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'enrollment_id.required' => trans('course::errors.enrollment.enrollment_id_required'),
            'course_id.required' => trans('course::errors.enrollment.course_id_required'),
            'role_id.required' => trans('course::errors.enrollment.role_id_required'),
            'user_id.required' => trans('course::errors.enrollment.user_id_required'),
        ];
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getParamsJsonAttribute()
    {
        if ($this->params) {
            return json_decode($this->params);
        }
        return null;
    }

    /**
     * @param $format
     * @return $this
     */
    public function setFormatOutputDate($format)
    {
        $this->format_date = $format;
        return $this;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getActiveDateByMiniFormatAttribute()
    {
        $value = $this->active_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.medium'));
        }
        return $value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getExpireDateByMiniFormatAttribute()
    {
        $value = $this->expire_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.medium'));
        }
        return $value;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function enrollment()
    {
        return $this->belongsTo('Modules\Course\Models\Enrollment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseEnrollment()
    {
        return $this->belongsTo(\Modules\Course\Models\Enrollment\CourseEnrollment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('Modules\User\Models\Role');
    }
}
