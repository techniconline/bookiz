<?php

namespace Modules\Course\Traits\Enrollment;


trait CourseEnrollment
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'enrollment_id.required' => trans('course::errors.enrollment.enrollment_id_required'),
            'course_id.required' => trans('course::errors.enrollment.course_id_required'),
            'duration_type_id.required' => trans('course::errors.enrollment.duration_type_id_required'),
            'role_id.required' => trans('course::errors.enrollment.role_id_required'),
        ];
        return $this;
    }

    public function getParamsJsonAttribute()
    {
        if($this->params){
            return json_decode($this->params);
        }
        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function enrollment()
    {
        return $this->belongsTo('Modules\Course\Models\Enrollment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('Modules\User\Models\Role');
    }

}
