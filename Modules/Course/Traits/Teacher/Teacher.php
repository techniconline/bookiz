<?php

namespace Modules\Course\Traits\Teacher;


trait Teacher
{

    public function scopeFilterUser($query)
    {
        $q = isset($this->requestItems['q']) ? $this->requestItems['q'] : null;
        $query = $query->whereHas('user', function ($query) use ($q) {
            $query->where(function ($query) use ($q) {

                if ($q){
                    $query->orWhere('username', 'LIKE', '%' . $q . '%');
                    $query->orWhere('last_name', 'LIKE', '%' . $q . '%');
                    $query->orWhere('first_name', 'LIKE', '%' . $q . '%');
                }

            })->active();
        })->filterCurrentInstance();

        return $query;
    }

    /**
     * @return mixed
     */
    public function getEducationKeyAttribute()
    {
        return $this->searchEnumItem($this->education, $this->educations);
    }

    /**
     * @return mixed
     */
    public function getEducationCertificateAttribute()
    {
        return trans("course::teacher.educations.".$this->education_key);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function teacherCourses()
    {
        return $this->hasMany('Modules\Course\Models\Course\CourseTeacher', "teacher_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance');
    }


}
