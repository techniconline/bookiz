<?php

namespace Modules\Course\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Modules\Core\Notifications\Channels\SmsChannel;
use Modules\Core\Notifications\Channels\Sms\SmsMessage;

class AttemptCount extends Notification
{
    use Queueable;

    private $attemptCount=null;
    private $mobile='9147795065';


    public function __construct($attemptCount)
    {
        $this->attemptCount=$attemptCount;
    }

    public function via($notifiable)
    {
            return [SmsChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toSms($notifiable)
    {
        $message=new SmsMessage();
        $values=['token1'=>$this->attemptCount,'token2'=>'attempt count','template'=>'verify'];
        $content=trans('user::message.sms.verify',$values);
        return $message->setContent($content)->setTo($this->mobile)->setType('verify')->setCustomFields($values)->getMessage();
    }

}
