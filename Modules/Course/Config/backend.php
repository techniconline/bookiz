<?php

return [
    'version' => '20180619001',
    'blocks' => [
        'left' => [],
        'top' => []
    ],
    'menus' => [
        'management' => [],
        'management_admin' => [
            [
                'alias' => '', //**
                'route' => 'course.category.index', //**
                'key_trans' => 'course::menu.categories_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-sitemap', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_courses',
            ],

            [
                'alias' => '', //**
                'route' => 'course.index', //**
                'key_trans' => 'course::menu.courses_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-institution', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_courses',
            ],
            [
                'alias' => '', //**
                'route' => 'course.teacher.index', //**
                'key_trans' => 'course::menu.teacher_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-users', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_courses',
            ],
            [
                'alias' => '', //**
                'route' => 'course.enrollment.index', //**
                'key_trans' => 'course::menu.enrollment_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-bell-o', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_courses',
            ],
            ['alias' => 'course.config.instance.index', //**
                'route' => 'course.config.instance.index', //**
                'key_trans' => 'course::menu.config.instance', //**
                'order' => null, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-cog', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_manage_settings',
            ],

        ],
    ],
    'group_menu' => [
        'management' => [],
        'management_admin' => [
            ['alias' => 'admin_courses', //**
                'key_trans' => 'course::menu.group.courses', //**
                'icon_class' => 'fa fa-graduation-cap',
                'before' => '',
                'after' => '',
                'order' => 10,
            ],
        ]
    ],

];
