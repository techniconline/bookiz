<?php

namespace Modules\Course\ViewModels\Course;


use Illuminate\Http\Request;
use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureValueRelation;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\Traits\Feature\FormFeatureBuilder;
use Modules\Core\Models\Language\Language;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Libraries\Activity\MasterActivity;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Course\CourseActivity;
use Modules\Course\Models\Course\CourseActivityItem;
use Modules\Course\Models\Course\CourseCategory;
use Modules\Course\Models\Course\CourseSection;
use Modules\Video\Models\Video\Video;


class CourseActivityViewModel extends BaseViewModel
{

    use GridViewModel;
    use FormFeatureBuilder;

    private $course_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = CourseActivity::enable()->where('course_id', $this->request->get('course_id'))->with(['course', 'courseSection']);
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $course_assets = [];

        //use bested assets
        if ($this->course_assets) {

        }

        if ($this->course_assets) {
            $course_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/course/activity.js"), 'name' => 'course-activity-backend'];
        }

        return $course_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('course::course.activity.title'), false);
        $this->addColumn('sort', trans('course::course.activity.sort'), true);
        $this->addColumn('section', trans('course::course.activity.section'), false);
        $this->addColumn('type', trans('course::course.activity.type'), true);
        $this->addColumn('show_type_text', trans('course::course.activity.show_type'), false);
        $this->addColumn('single_amount', trans('course::course.activity.single_amount'), false);
        $this->addColumn('active_date', trans('course::course.activity.active_date'), false);
        $this->addColumn('expire_date', trans('course::course.activity.expire_date'), false);
        $this->addColumn('is_public_text', trans('course::course.activity.is_public'), false);
        $this->addColumn('active', trans('course::course.activity.status'), false);


        /*add action*/
        $add = array(
            'name' => 'course.activity.create',
            'parameter' => ["course_id" => ['value' => $this->request->get("course_id")]]
        );
        $this->addButton('create_course', $add, trans('course::course.activity.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        /*back action*/
        $add = array(
            'name' => 'course.index',
            'parameter' => []
        );
        $this->addButton('back_course', $add, trans('course::course.list'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);


        $show = array(
            'name' => 'course.activity.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('course::course.activity.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('course::course.confirm_delete')];
        $delete = array(
            'name' => 'course.activity.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('course::course.activity.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('title', 'text');
        $this->addFilter('active_date', 'text');
        $this->addFilter('expire_date', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->section = $row->courseSection->title;
        $row->active = trans("course::course.statuses." . $row->active);
        return $row;
    }


    /**
     * @return $this
     */
    protected function setAssetsEditCourseActivity()
    {
        $this->course_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('course::course.activity.list'));
        $this->generateGridList()->renderedView("course::course_activity.index", ['view_model' => $this], "course_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createCourseActivity()
    {
        $courseActivity = new CourseActivity();

        $types = $courseActivity->getTypes();
        $showTypes = $courseActivity->getShowTypes();

        $courseActivity = $courseActivity->enable()->with(['course'])->find($this->request->get('course_activity_id'));
        $course_id = $courseActivity ? $courseActivity->course_id : $this->request->get('course_id');
        $courseSections = CourseSection::active()->where('course_id', $course_id)->get();
        $courseSections = $courseSections ? $courseSections->pluck('title', 'id')->toArray() : [];
        $course = Course::enable()->find($course_id);

        $viewCourseActivity = $this->renderTypeCourseActivity($courseActivity);

        $courseActivityItem = new CourseActivityItem();
        $itemTypes = $courseActivityItem->getTypes();

        $this->setTitlePage(trans('course::course.activity.' . ($courseActivity ? 'edit' : 'add')));
        $course_id = $this->request->get("course_id") ?: ($courseActivity ? $courseActivity->course_id : 0);
        return $this->renderedView("course::course_activity.form", ['courseActivity' => $courseActivity, 'course' => $course
            , 'viewCourseActivity' => $viewCourseActivity, 'courseSections' => $courseSections, 'types' => $types
            , 'showTypes' => $showTypes, 'itemTypes' => $itemTypes, 'course_id' => $course_id], "form");
    }

    /**
     * @param $courseActivity
     * @return bool
     */
    public function renderTypeCourseActivity($courseActivity)
    {

        if (!$courseActivity) {
            return false;
        }

        $masterActivity = new MasterActivity();
        $responseActivity = $masterActivity->setActivity($courseActivity)->render();
        return $responseActivity;
    }

    /**
     * @return CourseActivityViewModel
     * @throws \Throwable
     */
    protected function editCourseActivity()
    {
        return $this->createCourseActivity();
    }

    /**
     * @return $this
     */
    protected function destroyCourseActivity()
    {
        $course_activity_id = $this->request->get('course_activity_id');
        $courseActivity = CourseActivity::enable()->find($course_activity_id);

        if ($courseActivity && $courseActivity->delete()) {
            return $this->setResponse(true, trans("course::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function destroyCourseActivityItem()
    {
        $course_activity_item_id = $this->request->get('course_activity_item_id');
        $courseActivityItem = CourseActivityItem::enable()->find($course_activity_item_id);

        if ($courseActivityItem && $courseActivityItem->delete()) {
            return $this->setResponse(true, trans("course::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveCourseActivityItem()
    {
        $courseActivityItem = new CourseActivityItem();
        $this->validate($this->request, $courseActivityItem->rules);
        if (($courseActivityItem->fill($this->request->all()))) {
            $courseActivityItem->save();
        } else {
            $courseActivityItem = null;
        }

        return $this->setDataResponse($courseActivityItem)->setResponse($courseActivityItem ? true : false
            , trans("course::messages.alert." . ($courseActivityItem ? "save_success" : "save_un_success")), isset($courseActivityItem->errors) ? $courseActivityItem->errors : null);

    }

    /**
     * @return $this
     */
    protected function saveCourseActivity()
    {
        $response = $this->saveCourseActivityService();
        $courseActivity = $response['courseActivity'];
        if ($response['action']) {
            return $this->redirect(route('course.activity.edit', ['course_activity_id' => $courseActivity->id]))
                ->setDataResponse($courseActivity)->setResponse(true, trans("course::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, trans("course::messages.alert.mis_data"), isset($courseActivity->errors) ? $courseActivity->errors : null);
    }

    /**
     * @return $this|array
     */
    private function saveCourseActivityService()
    {
        $this->request = $this->requestValuesUpdate(['active_date' => 'Y-m-d H:i:s', 'expire_date' => 'Y-m-d H:i:s']);
        $courseActivity = new CourseActivity();
        if ($course_activity_id = $this->request->get("course_activity_id")) {
            $courseActivity = $courseActivity->enable()->find($course_activity_id);
            if (!$courseActivity) {
                return ['action' => false, 'courseActivity' => $courseActivity, 'redirect' => route('course.activity.edit', ['course_activity_id' => $this->request->get('course_activity_id')])];
            }

        }

        if (!$this->request->get("is_public")) {
            $this->request->request->set("is_public", 0);
        }

        $data = $this->request->all();
        if ($params = $this->request->get("params")) {
            $params = $this->convertDate($params);
            if($courseActivity && $course_activity_id && isset($courseActivity->params_array['external_params'])){
                //merge with database
                $ext_params['external_params'] = $courseActivity->params_array['external_params'];
                $params = array_merge($params, $ext_params);
            }
            $data["params"] = json_encode($params);
        }

        $this->request->request->add($data);
        $this->validate($this->request, $courseActivity->rules);
        if (($courseActivity->fill($data))) {
            $courseActivity->save();
            //$courseActivity->updated = $course_activity_id ? true : false;
            if ($course_activity_id) {
                // add to video use
                $res = $this->saveVideoUse($course_activity_id, $params);
                $resAction = $this->callActionTypeCourseActivity($courseActivity, $params);
            }
            $response = ['action' => true, 'courseActivity' => $courseActivity];
        } else {
            $response = ['action' => false, 'courseActivity' => $courseActivity];
        }

        return $response;
    }

    private function saveExternalParams($activity, $ext_params = [])
    {

    }

    /**
     * @param $courseActivity
     * @param array $params
     * @return bool
     */
    public function callActionTypeCourseActivity($courseActivity, $params = [])
    {

        if (!$courseActivity) {
            return false;
        }

        $masterActivity = new MasterActivity();
        $masterActivity = $masterActivity->setActivity($courseActivity);
        $responseActivity = ['action' => false];
        if (method_exists($masterActivity, 'callAction')) {
            $responseActivity = $masterActivity->callAction($params);
        }
        return $responseActivity;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function convertDate($params)
    {
        foreach ($params as $key => $param) {
            if (strpos($key, "_date") !== false) {
                $arr = explode(" ", $param);
                if (count($arr) == 2) {
                    $format = trans('core::date.datetime.medium');
                } else {
                    $format = trans('core::date.datetime.date_medium');
                }
                $p = $this->requestDataUpdate([$key => $param], [$key => $format]);
                $params[$key] = $p[$key];
            }
        }
        return $params;
    }

    /**
     * @param $course_activity_id
     * @param $params
     * @return array
     */
    private function saveVideoUse($course_activity_id, $params)
    {
        $courseActivity = new CourseActivity();
        $courseActivity = $courseActivity->enable()->with("course")->find($course_activity_id);

        $video_id = isset($params["video_id"]) && $params["video_id"] ? $params["video_id"] : null;
        if ($video_id && $courseActivity && $courseActivity->course) {
            $video = Video::find($video_id);
            $duration = $this->request->get('duration');
            $duration = $duration ?: $video->video_time_minutes;
            $this->saveDuration($courseActivity, $duration);
            $videoUse = BridgeHelper::getVideoUse()->getVideoUseObject();
            $instance_id = $courseActivity->course->instance_id;
            $res = $videoUse->setModel($courseActivity->getTable(), $course_activity_id)
                ->setVideoId($video_id)->setInstanceId($instance_id)->init()->save();
            return $res;
        }
        return ["action" => false];
    }

    /**
     * @param $courseActivity
     * @param $duration
     * @return bool
     */
    public function saveDuration($courseActivity, $duration)
    {
        if ((!$courseActivity->duration || ($courseActivity->duration != $duration))) {
            $courseActivity->duration = $duration;
            return $courseActivity->save();
        }
        return false;
    }


}
