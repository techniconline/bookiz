<?php

namespace Modules\Course\ViewModels\Course;


use Illuminate\Http\Request;
use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureValueRelation;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Meta\MetaGroup;
use Modules\Core\Traits\Feature\FormFeatureBuilder;
use Modules\Course\Models\Course\Course;
use Modules\Core\Models\Language\Language;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Models\Course\CourseMetaGroup;
use Modules\Course\Models\Course\CourseSection;
use Modules\Course\Models\Course\CourseTeacher;
use Modules\Course\Models\Enrollment;
use Modules\Course\Models\Enrollment\CourseEnrollment;
use Modules\Course\Models\Teacher\Teacher;
use Modules\Course\Models\Course\CourseCategoryRelation;


class SectionViewModel extends BaseViewModel
{

    use GridViewModel;
    use FormFeatureBuilder;

    private $course_assets;
    private $course_id = 0;

    public function __construct()
    {
//        $this->setModel(new Course());
    }

    /**
     * @return $this
     */
    public function boot()
    {
        $this->course_id = $this->request->get("course_id");
        $this->setModel(new CourseSection());
        return $this;
    }

    public function setGridModel()
    {
        $this->boot();
        $this->Model = $this->useModel;
        $this->Model = $this->Model->setRequestItems($this->request->all())->active()->filterCourse()->with("course");
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $course_assets = [];
        $course_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/course/course.js"), 'name' => 'course-backend'];

        return $course_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('course::course.section.title'), true);
        $this->addColumn('course', trans('course::course.course'), false);
        $this->addColumn('sort', trans('course::course.sort'), false);
        $this->addColumn('active_date', trans('course::course.section.active_date'), false);
        $this->addColumn('expire_date', trans('course::course.section.expire_date'), false);

        /*add action*/
        $add = array(
            'name' => 'course.section.create',
            'parameter' => ["course_id" => ["value" => $this->course_id]]
        );
        $this->addButton('create_course_section', $add, trans('course::course.section.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        /*back action*/
        $add = array(
            'name' => 'course.index',
            'parameter' => []
        );
        $this->addButton('back_course', $add, trans('course::course.list'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);


        $show = array(
            'name' => 'course.section.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('course::course.section.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);


        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('course::course.confirm_delete')];
        $delete = array(
            'name' => 'course.section.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('course::course.section.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('title', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->course = $row->course->title;
        return $row;
    }


    /**
     * @return $this
     */
    protected function setAssetsCreateCourseSection()
    {
        $this->course_assets = true;
        return $this;
    }

    protected function setAssetsEditCourseSection()
    {
        $this->course_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('course::course.list'));
        $this->generateGridList()->renderedView("course::course.section.index", ['view_model' => $this], "section_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createCourseSection()
    {
        $this->boot();
        $viewModel =& $this;
        $courseSection = null;
        if($course_section_id = $this->request->get("course_section_id")){
            $courseSection = $this->useModel->enable()->with("course")->find($course_section_id);
            $this->course_id = $courseSection?$courseSection->course_id:0;
        }

        $this->setTitlePage(trans('course::course.section.' . ($courseSection ? 'edit' : 'add')));
        return $this->renderedView("course::section.form_section", ['courseSection' => $courseSection,'viewModel' => $viewModel], "form");
    }

    /**
     * @return mixed
     */
    public function getCourseId()
    {
        return $this->course_id;
    }

    /**
     * @throws \Throwable
     */
    protected function editCourseSection()
    {
        return $this->createCourseSection();
    }

    /**
     * @return $this
     */
    protected function destroySection()
    {
        $course_section_id = $this->request->get('course_section_id');
        $courseSection = new CourseSection();
        $courseSection = $courseSection->enable()->find($course_section_id);

        if ($courseSection && $courseSection->delete()) {
            return $this->setResponse(true, trans("course::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveCourseSection()
    {
        $courseSection = new CourseSection();
        $this->request = $this->requestValuesUpdate(['active_date' => 'Y-m-d H:i:s', 'expire_date' => 'Y-m-d H:i:s']);
        if ($course_section_id = $this->request->get("course_section_id")) {
            $courseSection = $courseSection->enable()->find($course_section_id);
            if (!$courseSection) {
                return $this->redirectBack()->setResponse(false, trans("course::messages.alert.not_find_data"));
            }

        }

        if (!$this->request->get("sort")) {
            $this->request->offsetSet("sort", 0);
        }


        if (($courseSection->fill($this->request->all())->isValid())) {
            $courseSection->save();
            $courseSection->updated = $course_section_id ? true : false;
            return $this->redirect(route("course.section.edit",["id"=>$courseSection->id]))->setResponse(true, trans("course::messages.alert.save_success"));
        } else {
            return $this->redirectBack()->setResponse(false, trans("course::messages.alert.mis_data"), $courseSection->errors);
        }

    }

    /**
     * @return $this
     */
    protected function sortingSection()
    {
        $sorts = $this->request->get('sort_section');
        foreach ($sorts as $section => $sort) {
            CourseSection::where('id', $section)->update(['sort' => (int)$sort]);
        }
        return $this->setResponse(true, trans("course::messages.alert.save_success"));
    }

}
