<?php

namespace Modules\Course\ViewModels\Course;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Course\CourseCategory;
use Modules\Course\Models\Course\CourseCategoryRelation;
use Modules\Course\Models\Enrollment;
use Modules\Course\Models\Enrollment\CourseEnrollment;
use Modules\Course\Providers\Helpers\Category\CategoryHelperFacade as CategoryHelper;
use Modules\Course\Traits\Page\EnrollmentViewModelTrait;
use Modules\Course\Traits\Page\InfoViewModelTrait;


class CategoryByCoursesViewModel extends BaseViewModel
{

    use InfoViewModelTrait;
    use EnrollmentViewModelTrait;

    public $listCourses;

    /**
     * @param $course_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getCourseEnrollmentsHtml($course_id)
    {
        return $this->getCourseEnrollments($course_id, 'list');
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function listCourses()
    {
        $viewModel =& $this;
        $current_category = $this->request->get("category_id");
        $current_enrollment = $this->request->get("enrollment_id");
        $model = new CourseCategory();
        if ($current_category) {
            $categories = $model->getChildesId($current_category);
            $this->request->offsetSet("category_id", $categories->toArray());
        }
        $model = new Course();
        $this->listCourses = $model->setRequestItems($this->request->all())
            ->filterByCategories()
            ->filterByEnrollments()
            ->filterCurrentInstance()
            ->with(["courseCategories", "courseEnrollments"])
            //->filterRelations(["course", "courseCategory"])
            ->sortBy()
            ->active()
            ->paginate($this->perPage);

        if (app("getInstanceObject")->isApi()) {
            $this->listCourses = $this->decorateList($this->listCourses);
            return $this->setDataResponse($this->listCourses)->setResponse(true);
        }
        $this->setTitlePage(trans('course::course.list'));
        return $this->renderedView('course::course.courses_list', compact('viewModel', 'current_category', 'current_enrollment'));
    }

    public function getCourseCategories()
    {
        $courseCategories = CategoryHelper::getNestableCourseCategories();
        return $courseCategories;
    }

    /**
     * @param null $selected
     * @return string
     * @throws \Throwable
     */
    public function renderCategoriesView($selected = null)
    {
        $viewModel =& $this;
        return view('course::category.categories', compact("selected", "viewModel"))->render();
    }

    /**
     * @param null $selected
     * @return string
     * @throws \Throwable
     */
    public function renderSortableView($selected = null)
    {
        $viewModel =& $this;
        $this->request->offsetUnset("category_id");
        return view('course::category.sortable', compact("selected", "viewModel"))->render();
    }

    /**
     * @return mixed
     */
    public function getEnrolmentList()
    {
        $listEnrolls = Enrollment::active()->notLoadEnrolls()->get();
        return $listEnrolls;
    }

    /**
     * @param null $selected
     * @return string
     * @throws \Throwable
     */
    public function renderEnrollmentsView($selected = null)
    {
        $viewModel =& $this;
        return view('course::enrollment.enrollments', compact("selected", "viewModel"))->render();
    }

}
