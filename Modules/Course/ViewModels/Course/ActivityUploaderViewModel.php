<?php

namespace Modules\Course\ViewModels\Course;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Modules\Code\Libraries\Uploader\UploaderLibrary;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Course\Models\Course\CourseActivity;

class ActivityUploaderViewModel extends BaseViewModel
{

    protected function deleteFile()
    {
        $courseActivity = CourseActivity::find($this->request->get("course_activity_id"));
        $file_delete = $this->request->get("file");

        if(!$courseActivity){
            return $this->setResponse(false, trans("course::messages.alert.not_find_data"));
        }

        $params = $courseActivity->params;


        if(isset($params->files) && $params->files){
            $new_params = [];
            foreach ($params->files as $file) {
                $md5 = md5($file->src);
                if($file_delete != $md5){
                    $new_params[] = $file;
                }else{
                    // delete file

                }
            }

            $params->files = $new_params;
            $courseActivity->params = json_encode($params);
            $courseActivity->save();

        }
        return $this->setResponse(true, trans("course::messages.alert.del_success"));
    }

    /**
     * @return $this
     */
    protected function uploadFile()
    {
        $courseActivity = CourseActivity::find($this->request->get("course_activity_id"));
        if (!$courseActivity) {
            return $this->setResponse(false, trans("course::messages.alert.not_find_data"));
        }

        $result = $this->uploadFileService($courseActivity);

        return $this->setResponse(isset($result['action']) ? $result['action'] : null, isset($result['message']) ? $result['message'] : null
            , isset($result['errors']) ? $result['errors'] : null, isset($result['items']) ? $result['items'] : null);

    }

    /**
     * @param CourseActivity $courseActivity
     * @return array
     */
    private function uploadFileService(CourseActivity $courseActivity)
    {
        $libUploader = new UploaderLibrary();

        $course = $courseActivity?$courseActivity->course:null;
        $activity_id = $courseActivity?$courseActivity->id:0;

        $folderUpload = "files".DIRECTORY_SEPARATOR."instance".DIRECTORY_SEPARATOR.($course?$course->instance_id:0);
        $folderUpload .= DIRECTORY_SEPARATOR."course".DIRECTORY_SEPARATOR.($course?$course->id:0);
        $folderUpload .= DIRECTORY_SEPARATOR."activity".DIRECTORY_SEPARATOR.$activity_id;
        $file_name = md5(time());
        $result = $libUploader->setStorage($libUploader::STORAGE_NFS_PUBLIC)->setFolderUploadName($folderUpload)
            ->setFileName($file_name)
            ->setTypeFile('doc')
            ->setUrlUpload(null, false)->upload();

        if (isset($result['action']) && $result['action']) {

            $src = $result['items']['src'] ? $result['items']['src'] : null;
            $name = $result['items']['name'] ? $result['items']['name'] : null;
            $size = $result['items']['size'] ? $result['items']['size'] : null;
            $ext = $result['items']['extension'] ? $result['items']['extension'] : null;

            $exist = Storage::disk($libUploader::STORAGE_PUBLIC_MINIO)->exists($src);
            $params = $courseActivity->params_text;
            $params = $params?(json_decode($params, true)):[];

            $params['files'] = isset($params['files'])?collect($params['files'])->values()->toArray():[];
            $counter = count($params['files']);
            $params["files"][$counter]['src'] = $src;
//            $params["files"][$counter]['mime'] = $this->request->file('file')->getClientMimeType();
            $params["files"][$counter]['ext'] = $ext;
            $params["files"][$counter]['name'] = $name;
            $params["files"][$counter]['size'] = $size;
            $params_text = json_encode($params);
            $courseActivity->params = $params_text;
            $courseActivity->save();

        }

        return $result;
    }


}
