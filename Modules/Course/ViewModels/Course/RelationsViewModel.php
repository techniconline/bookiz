<?php

namespace Modules\Course\ViewModels\Course;


use Illuminate\Http\Request;
use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureValueRelation;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Meta\MetaGroup;
use Modules\Core\Traits\Feature\FormFeatureBuilder;
use Modules\Course\Models\Course\Course;
use Modules\Core\Models\Language\Language;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Models\Course\CourseActivity;
use Modules\Course\Models\Course\CourseMetaGroup;
use Modules\Course\Models\Course\CourseRelation;
use Modules\Course\Models\Course\CourseSection;
use Modules\Course\Models\Course\CourseTeacher;
use Modules\Course\Models\Enrollment;
use Modules\Course\Models\Enrollment\CourseEnrollment;
use Modules\Course\Models\Teacher\Teacher;
use Modules\Course\Models\Course\CourseCategoryRelation;


class RelationsViewModel extends BaseViewModel
{

    use GridViewModel;

    private $course_assets;
    private $course_id = 0;
    private $title;

    public function __construct()
    {
//        $this->setModel(new Course());
    }

    /**
     * @return $this
     */
    public function boot()
    {
        $this->course_id = $this->request->get("course_id");
        $this->setModel(new Course());
        return $this;
    }

    public function setGridModel()
    {

        $this->boot();
        $this->Model = CourseRelation::active()->with('course', 'courseRelation')->where('course_id', $this->course_id);
//        dd($this->Model->get());
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('course::course.course_title'), true);
        $this->addColumn('type', trans('course::course.type'), false);
        $this->addColumn('course_relation', trans('course::course.relations'), false);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('course::course.confirm_delete')];
        $delete = array(
            'name' => 'course.relations.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('course::course.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
//        $this->addFilter('title', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->title = $row->course->title;
        $row->type = trans('course::course.course_relation.'.$row->type);
        $row->course_relation = $row->courseRelation->title;
        return $row;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $course_assets = [];
//        $course_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/course/course.js"), 'name' => 'course-backend'];

        return $course_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return $this
     */
    protected function setAssetsCreateCourseSection()
    {
        $this->course_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function indexCourseRelations()
    {
        $this->boot();
        $viewModel =& $this;
        $this->generateGridList();
        $relation_course_ids = $this->getCourseRelationIds()->toArray();

        $this->setTitlePage($this->getTitle());
        return $this->renderedView("course::course_relations.form_course_relation", ['viewModel' => $viewModel, 'course'=>$this->getCourse(), 'relation_course_ids' => $relation_course_ids], "form");
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getTitle()
    {
        $course = $this->getCourse();
        $title = $course ? $course->title : '--';
        $title = trans('course::course.course_relation.title', ['course_title' => $title]);
        return $title;
    }

    /**
     * @return mixed
     */
    public function getCourseId()
    {
        return $this->course_id;
    }

    /**
     * @return mixed
     */
    public function getCourse()
    {
        return $this->useModel->with('courseRelations')->find($this->getCourseId());
    }

    /**
     * @return mixed
     */
    public function getCourseRelationIds()
    {
        return $this->useModel->courseRelations->pluck('relation_course_id');
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return [
            'normal'=>trans('course::course.course_relation.normal'),
            'before_course'=>trans('course::course.course_relation.before_course'),
            'after_course'=>trans('course::course.course_relation.after_course'),
        ];
    }

    /**
     * @return $this
     */
    public function saveCourseRelations()
    {
        $this->boot();

        $course = $this->getCourse();
        if (!$course) {
            return $this->redirectBack()->setResponse(false, trans("course::messages.alert.not_find_data"));
        }

        $relation_course_id = $this->request->get('relation_course_id');
        $type = $this->request->get('type');

        $relations = CourseRelation::active()->where('type', $type)->where('course_id',$this->course_id)->pluck('relation_course_id');
        $del_ids = collect($relations)->diff($relation_course_id);
        $new_ids = collect($relation_course_id)->diff($relations);

//        if($del_ids->isNotEmpty()){
//            CourseRelation::active()->where('course_id', $this->course_id)->where('relation_course_id', $del_ids)->delete();
//        }

        $insertItems = [];
        foreach ($new_ids as $rel_course_id) {
            $insertItems[] = ['course_id'=>$this->course_id, 'relation_course_id'=>$rel_course_id,'type'=>$type, 'created_at'=>date("Y-m-d H:i:s")];
        }


        if($insertItems){
            $model = new CourseRelation();
            $model->insert($insertItems);
        }

        return $this->redirect(route('course.relations.index',['course_id'=>$this->course_id]))->setResponse(true, trans("course::messages.alert.save_success"));
    }

    /**
     * @return $this
     */
    protected function destroyCourseRelation()
    {
        $course_relation_id = $this->request->get('course_relation_id');
        $courseRelation = CourseRelation::enable()->find($course_relation_id);

        if ($courseRelation && $courseRelation->delete()) {
            return $this->setResponse(true, trans("course::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.del_un_success"));

    }

}
