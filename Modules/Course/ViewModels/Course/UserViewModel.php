<?php

namespace Modules\Course\ViewModels\Course;

use BridgeHelper;
use Illuminate\Support\Facades\DB;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Enrollment\CourseUserEnrollment;
use Modules\Course\Traits\Page\EnrollmentViewModelTrait;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Order\OrderItems;
use Modules\Sale\Models\Traits\Payment\OrderStatesTrait;
use Modules\User\Models\Role;

class UserViewModel extends BaseViewModel
{

    use EnrollmentViewModelTrait;
    use OrderStatesTrait;

    /**
     * @param null $type
     * @return $this
     * @throws \Throwable
     */
    public function myCourses($type = null)
    {
        $viewModel =& $this;
        $user = BridgeHelper::getAccess()->getUser();
        $userCourses = new CourseUserEnrollment();

        $userCourses = $userCourses->where('user_id', $user->id)->active()->with('course')
            ->whereHas('course', function ($q) {
                $q->active();
            })
            ->where(function ($q) use ($type) {
                if ($type) {
                    return $q->where('type', $type);
                }
            })
            ->orderBy('id', 'DESC')->paginate(12);

        if (app("getInstanceObject")->isApi()) {
            $userCourses = $this->decorateList($userCourses);
            return $this->setDataResponse($userCourses)->setResponse($userCourses ? true : false, $userCourses ? null : trans("course::messages.alert.not_find_data"));
        }

        $this->layout = 'user.layout';
        $this->setTitlePage(trans('course::course.my_courses'));
        return $this->renderedView('course::user.my_courses', ['viewModel' => $viewModel, 'userCourses' => $userCourses]);
    }

    /**
     * @return UserViewModel
     * @throws \Throwable
     */
    public function myActivity()
    {
        return $this->myCourses(CourseUserEnrollment::TYPE_ACTIVITY);
    }

    /**
     * @return $this
     */
    public function enrollFreeItem()
    {
        $item_id = $this->request->get('item_id');
        $item_type = $this->request->get('item_type');
        $data = $this->request->all();
        $enrollments = $this->getAvailableEnrollment($item_id);
        $courseEnrollment = $enrollments->first();

        if (isset($courseEnrollment['enrollment']) && $courseEnrollment['enrollment']->alias == 'free') {
            $course_enrollment_id = $courseEnrollment['id'];
            $enrollment = $courseEnrollment['enrollment'];
            $user = BridgeHelper::getAccess()->getUser();
            $courseUserEnroll = new CourseUserEnrollment();

            $courseUserEnrollData = $courseUserEnroll->enable()->where('course_id',$item_id)->where('user_id', $user->id)->first();

            if ($courseUserEnrollData) {
                return $this->redirectBack()->setResponse(false, trans("course::messages.alert.enroll_exist"));
            }

            $role = Role::active()->where('name', 'student')->where('type', 'course')->first();

            if (!$role) {
                return $this->redirectBack()->setResponse(false, trans("course::messages.alert.role_error"));
            }

            $course = Course::active()->with('instance')->find($item_id);
            if (!$course) {
                return $this->redirectBack()->setResponse(false, trans("course::messages.alert.mis_data"));
            }

            $order = $this->makeOrder($user, $course, $courseEnrollment);
            if (!$order) {
                return $this->redirectBack()->setResponse(false, trans("course::messages.alert.process_error"));
            }
            $data['order_id'] = $order->id;
            $params = json_encode($data);

            $response = $this->enrollUserOnCourse($user->id, $enrollment->id, $item_id, $params);
            if ($response) {
                return $this->redirectBack()->setDataResponse($courseUserEnroll)->setResponse(true, trans("course::messages.alert.enroll_success"));
            }
        }
        return $this->redirectBack()->setResponse(false, trans("course::messages.alert.enroll_error"));
    }

    public function makeOrder($user, $info, $courseEnrollment)
    {
        DB::beginTransaction();
        try {
            $currentCurrency = BridgeHelper::getCurrencyHelper()->getCurrentCurrency();
            $itemParams = $courseEnrollment->params_json;
            $cost = 0;
            if(isset($itemParams->sale_cost) && isset($itemParams->currency_id)){
                $cost = BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($itemParams->sale_cost, $itemParams->currency_id, false);
            }
            $order = new Order;
            $order->instance_id = $info->instance->id;
            $order->reference_code = $order->getReferenceCode();
            $order->user_id = $user->id;
            $order->state = $this->getOrderCompleteState();
            $order->amount = $cost;
            $order->payable_amount = $cost;
            $order->currency_id = $currentCurrency->id;
            $order->save();
            $item = new OrderItems;
            $item->order_id = $order->id;
            $item->item_type = 'course';
            $item->item_id = $info->id;
            $item->name = $info->title;
            $item->options = ["enrollment_id" => $courseEnrollment->enrollment_id, "product_key" => $info->code];
            $item->qty = 1;
            $item->amount = isset($itemParams->sale_cost) ? $itemParams->sale_cost : 0;
            $item->total_amount = isset($itemParams->sale_cost) ? $itemParams->sale_cost : 0;
            $item->currency_id = isset($itemParams->currency_id) ? $itemParams->currency_id : $currentCurrency->id;
            $item->save();
            DB::commit();
            return $order;
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return false;
        }
    }

}
