<?php

namespace Modules\Course\ViewModels\Course;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Course\Models\Course\Course;
use Modules\Course\Traits\Page\ActivityViewModelTrait;
use Modules\Course\Traits\Page\EnrollmentViewModelTrait;
use Modules\Course\Traits\Page\ParamsViewModelTrait;
use Modules\Course\Traits\Page\TeachersViewModelTrait;
use Modules\Course\Models\Course\CourseActivity;
use Illuminate\Support\Facades\Cache;

class PageViewModel extends BaseViewModel
{
    use ActivityViewModelTrait;
    use EnrollmentViewModelTrait;
    use TeachersViewModelTrait;
    use ParamsViewModelTrait;

    private $course;
    private $activity_id = false;


    /**
     * @return $this|void
     * @throws \Throwable
     */
    public function viewCourse()
    {
        $blockViewModel =& $this;
        $course_id = $this->request->get('course_id');
        $this->setUserRoleAndAccessOnCourse($course_id);
        $cacheKey = $this->getCourseCacheKey($course_id);
        if (!($this->course = Cache::get($cacheKey))) {
            $courseModel = Course::with(['courseCategories', 'courseSections.courseActivities', 'courseTeachers', 'coursePackageEnrollment.coursePackage'])
                ->active();
            if ($this->userRole) {
                $this->course = $courseModel->find($course_id);
            } else {
                $this->course = $courseModel->filterCurrentInstance()->find($course_id);
            }
            Cache::put($cacheKey, $this->course, 60);
        }
        if (!$this->course) {
            return abort(404, 'Page Not Find');
        }

        $this->course->userRole = $this->userRole;
        $this->setRelationalData('course', $this->course);
        $activity_view = false;
        if (!$this->activity_id) {
            $this->activity_id = $this->getFreeActivity();
        }
        if ($this->activity_id) {
            $activity_view = $this->getActivity();
            if (!$activity_view) {
                return $this->redirect(route('course.view', ['id' => $this->course->id]))->setResponse(false, trans('course::messages.alert.activity_can_not_access'));
            }
        }
        if (app("getInstanceObject")->isApi()) {
            $this->course = $this->decorateAttributes($this->course);
            return $this->setDataResponse($this->course)->setResponse(true);
        }
        $this->setTitlePage($this->course->title);
        return $this->renderedView('course::course.page', ['blockViewModel' => $blockViewModel, 'activity_view' => $activity_view, 'current_activity' => $this->activity_id]);
    }

    public function getActivity()
    {
        if (app("getInstanceObject")->isApi()) {
            $this->activity_id = $this->request->get("activity_id");
        }

        $activity = CourseActivity::active()->find($this->activity_id);

        if (!$activity) {
            if (app("getInstanceObject")->isApi()) {
                return $this->setResponse(false, trans('course::course.activity_not_find'));
            }
            return $this->renderedView('core::component.message', ['error' => trans('course::course.activity_not_find')]);
        }
        $this->setUserRoleAndAccessOnCourse($activity->course_id);
        if ($this->hasActivityAccess($activity)) {
            $activityObject = $this->getActivityObject($activity);
            if (app("getInstanceObject")->isApi()) {
                $activity = $this->decorateAttributes($activity);
                $output = $activityObject->getOutput();
                if ($output instanceof \Symfony\Component\HttpFoundation\Response || $output instanceof \Illuminate\Contracts\Routing\ResponseFactory) {
                    return $output;
                } else {
                    $activity->put('out_put', $output);
//                    $activity->out_put=$output;
                    return $this->setDataResponse($activity)->setResponse(true);
                }
            }
            return $activityObject->getOutput();
        } else {
            if (app("getInstanceObject")->isApi()) {
                return $this->setResponse(false, trans('course::course.activity_not_access'));
            }
            return $this->renderedView('core::component.message', ['error' => trans('course::course.activity_not_access')]);
        }
    }

    public function viewActivity()
    {
        $activity_id = $this->request->get('activity_id');
        if (!$activity_id) {
            return abort(404, 'Page Not Find');
        }
        $this->activity_id = $activity_id;
        if ($this->isAjaxRequest()) {
            $data = $this->getActivity();
            if (!is_array($data)) {
                $data = ['content' => $data];
            }
            return $this->setDataResponse($data)->setResponse(true);
        } else {
            return $this->getActivity();
        }
        //return $this->viewCourse();
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getJsonData()
    {
        $this->course->rate = BridgeHelper::getRate()->getRateObject()->setModel($this->course->getTable(), $this->course->id)->setWithData(true)->init()->getData();
        $this->course->image = strlen($this->course->image) ? $this->course->image : BridgeHelper::getConfig()->getSettings('default_poster', 'instance', 'course');
        $this->course->enrollements = BridgeHelper::getConfig()->getSettings('show_enrollment_on_course', 'instance', 'course') ? $this->getCourseEnrollments($this->course->id, null, true) : null;
        $this->course->teachers = $this->course->courseTeachers->count() ? $this->getCourseTeachers($this->course->id) : $this->getComponent('message', ['info' => trans('course::course.message.no_teachers')], 'core');
        return $this;
    }

    public function getCourse()
    {
        return $this->course;
    }


    public function getFreeActivity()
    {
        $activity = $this->course->courseActivities->where('show_type', 'free')->where('type', 'video')->first();
        if ($activity && $this->hasActivityAccess($activity)) {
            return $activity->id;
        }
        return false;
    }


    public function getCourseCacheKey($course_id)
    {
        $key = 'course_model_by_' . $course_id;
        return $key;
    }
}
