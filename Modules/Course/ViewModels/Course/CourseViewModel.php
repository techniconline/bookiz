<?php

namespace Modules\Course\ViewModels\Course;


use Illuminate\Http\Request;
use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureValueRelation;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Meta\MetaGroup;
use Modules\Core\Traits\Feature\FormFeatureBuilder;
use Modules\Course\Models\Course\Course;
use Modules\Core\Models\Language\Language;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Models\Course\CourseCategory;
use Modules\Course\Models\Course\CourseMetaGroup;
use Modules\Course\Models\Course\CourseSection;
use Modules\Course\Models\Course\CourseTeacher;
use Modules\Course\Models\Enrollment;
use Modules\Course\Models\Enrollment\CourseEnrollment;
use Modules\Course\Models\Teacher\Teacher;
use Modules\Course\Models\Course\CourseCategoryRelation;


class CourseViewModel extends BaseViewModel
{

    use GridViewModel;
    use FormFeatureBuilder;

    private $course_assets;
    private $course_meta_assets;

    public function __construct()
    {
//        $this->setModel(new Course());
    }

    public function setGridModel()
    {
        $this->Model = Course::enable()->filterCurrentInstance()->with('courseCategory', 'instance', 'courseCategories');
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $course_assets = [];

        //use bested assets
        if ($this->course_assets) {
            $course_assets [] = ['container' => 'up_theme_js_2', 'src' => ("assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"), 'name' => 'ui.widget'];

            $course_assets [] = ['container' => 'plugin_general_2', 'src' => ("jquery-ui/jquery-ui.min.js"), 'name' => 'jquery-ui'];
            $course_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/plupload.full.min.js"), 'name' => 'plupload.full'];
            $course_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/jquery.ui.plupload.js"), 'name' => 'jquery.ui.plupload'];
            $course_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/i18n/fa.js"), 'name' => 'fa.js'];

            $course_assets [] = ['container' => 'general_style', 'src' => ("jquery-ui/jquery-ui.min.css"), 'name' => 'jquery-ui'];
            $course_assets [] = ['container' => 'general_style', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css"), 'name' => 'jquery.ui.plupload.css'];

        }

        if ($this->course_assets || $this->course_meta_assets) {
            $course_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/course/course.js"), 'name' => 'course-backend'];
        }

        return $course_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('course::course.title'), true);
        $this->addColumn('code', trans('course::course.code'), false);
        $this->addColumn('active_text', trans('course::course.status'), false);
        $this->addColumn('category', trans('course::course.category'), false);
        $this->addColumn('instance', trans('course::course.instance'), false);
        $this->addColumn('type', trans('course::course.type'), false);

        /*add action*/
        $add = array(
            'name' => 'course.create',
            'parameter' => null
        );
        $this->addButton('create_course', $add, trans('course::course.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);


        $show = array(
            'name' => 'course.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('course::course.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'course.activity.index',
            'parameter' => ['id']
        );
        $this->addAction('activity_edit', $show, trans('course::course.activity.edit'), 'fa fa-list', false, ['target' => '', 'class' => 'btn btn-sm btn-primary']);

        $show = array(
            'name' => 'course.section.index',
            'parameter' => ['id']
        );
        $this->addAction('section_list', $show, trans('course::course.sections'), 'fa fa-calendar', false, ['target' => '', 'class' => 'btn btn-sm btn-default']);

        $show = array(
            'name' => 'course.meta.editMetaTags',
            'parameter' => ['id']
        );
        $this->addAction('edit_meta_tags', $show, trans('course::course.edit_meta_tags'), 'fa fa-tags', false, ['target' => '', 'class' => 'btn btn-sm btn-warning']);

        $show = array(
            'name' => 'course.enrollments.index',
            'parameter' => ['id']
        );
        $this->addAction('enrollments_edit', $show, trans('course::course.enrolls.edit'), 'fa fa-bell-o', false, ['target' => '', 'class' => 'btn btn-sm btn-info']);

        $show = array(
            'name' => 'course.enrollments.user.list.index',
            'parameter' => ['id']
        );
        $this->addAction('enrollments_list', $show, trans('course::course.enrolls.list'), 'fa fa-users', false, ['target' => '', 'class' => 'btn btn-sm btn-default']);

        //package enroll
        $show = array(
            'name' => 'course.enrollments.package.index',
            'parameter' => ['id']
        );
        $this->addAction('course_package_enrollments_list', $show, trans('course::enrollment.package.list'), 'fa fa-file-archive-o', false, ['target' => '', 'class' => 'btn btn-sm btn-default']);

        $show = array(
            'name' => 'course.copy.index',
            'parameter' => ['id']
        );
        $this->addAction('course_copy', $show, trans('course::course.copy'), 'fa fa-copy', false, ['target' => '', 'class' => 'btn btn-sm btn-primary']);


        $show = array(
            'name' => 'course.relations.index',
            'parameter' => ['id']
        );
        $this->addAction('course_relations', $show, trans('course::course.relations'), 'fa fa-link', false, ['target' => '', 'class' => 'btn btn-sm btn-warning']);


        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('course::course.confirm_delete')];
        $delete = array(
            'name' => 'course.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('course::course.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('title', 'text');
        $this->addFilter('code', 'text');
//        $categories = CourseCategory::enable()->pluck('title', 'id');
        $categories = $this->getListCategoryByChildes();
        $this->addFilter('category_id', 'select', ['relation' => 'courseCategories', 'options' => $categories, 'title' => trans('course::course.category')]);

        $this->can_export = 1;
        $this->setExportFileName('course_list');

        return $this;
    }


    public function getExportHeader()
    {
        $header = [
            'title' => trans("course::course.title"),
            'code' => trans("course::course.code"),
            'active_text' => trans("course::course.status"),
            'category' => trans("course::course.category"),
            'instance' => trans("course::course.instance"),
            'type' => trans("course::course.type"),
            'link' => trans("course::course.course_link"),
        ];
        return $header;
    }

    /**
     * @param $row
     * @return array
     */
    public function getExportRow($row)
    {
        $csvRow = [];
        if ($row) {

            $csvRow[] = [
                'title' => $row->title,
                'code' => $row->code,
                'active_text' => $row->active_text,
                'category' => $row->courseCategory->title,
                'instance' => $row->instance->name,
                'type' => trans('course::course.' . $row->type),
                'link' => get_instance()->getCurrentUrl(route('course.view',['course_id'=>$row->id])),
            ];

        }

        return $csvRow;
    }

    /**
     * @return array
     */
    private function getListCategoryByChildes()
    {
        $categories = CourseCategory::enable()->filterCurrentInstance()->orderBy('parent_id')->pluck('title', 'id');
        $list = [];
        $model = new CourseCategory();
        foreach ($categories as $cat_id => $title) {
            $childes = $model->getChildesId($cat_id);
            if ($childes->count() > 1) {
                $title = $title . ' + ';
            }
            $list[implode(",", $childes->toArray())] = $title;
        }
        return $list;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->category = $row->courseCategory->title;
        $row->instance = $row->instance->name;
        $row->type = trans('course::course.' . $row->type);
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $actions = $this->actions;
        if ($row->type == 'package') {
            unset($actions['enrollments_list']);
        } else {
            // add package route
            unset($actions['course_package_enrollments_list']);
        }
        return $actions;
    }

    /**
     * @return $this
     */
    protected function setAssetsCreateCourse()
    {
        $this->course_assets = true;
        return $this;
    }

    protected function setAssetsEditCourse()
    {
        $this->course_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('course::course.list'));
        $this->generateGridList()->renderedView("course::course.index", ['view_model' => $this], "course_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createCourse()
    {
        $instances = Instance::active()->get();

        $course = new Course();

        $types = $course->getTypes();

        $course = $course->enable()->with([
            'language'
            , 'courseSections'
            , 'instance'
            , 'courseCategory'
            , 'courseTeachers'
            , 'courseEnrollments'
        ])->find($this->request->get('course_id'));

        $teachers = $activeCategories = $activeTeachers = null;
        $metaGroups = null;
        $activeEnrollments = $metaGroupsActive = [];
        if ($course) {
            $teachers = Teacher::active()->with('user')->get();
            $activeTeachers = $course->courseTeachers->pluck('teacher_id')->toArray();
            $activeEnrollments = $course->courseEnrollments->pluck('enrollment_id')->toArray();

            $activeCategories = CourseCategoryRelation::where('course_id', $course->id)->pluck('category_id')->toArray();

            $metaGroups = new MetaGroup();
            $metaGroups = $metaGroups->active()->filterLanguage()->filterSystem(Course::SYSTEM_NAME)->get();
            $metaGroups = $metaGroups ? $metaGroups->pluck('title', 'id') : [];

            $courseMetaGroups = $course->courseMetaGroups()->get();

            if ($courseMetaGroups) {
                $metaGroupsActive = $courseMetaGroups->pluck('meta_group_id')->toArray();
            }


        }


        $this->setTitlePage(trans('course::course.' . ($course ? 'edit' : 'add')));
        return $this->renderedView("course::course.form_course", ['course' => $course, 'teachers' => $teachers, 'activeTeachers' => $activeTeachers
            , 'types' => $types, 'instances' => $instances, 'activeCategories' => $activeCategories
            , 'metaGroups' => $metaGroups, 'activeEnrollments' => $activeEnrollments, 'metaGroupsActive' => $metaGroupsActive], "form");
    }

    /**
     * @return CourseViewModel
     * @throws \Throwable
     */
    protected function editCourse()
    {
        return $this->createCourse();
    }

    /**
     * @return $this
     */
    protected function saveCourse()
    {
        $response = $this->saveCourseService();
        $course = isset($response['course']) ? $response['course'] : null;
        if ($response['action']) {
            return $this->redirect(route('course.edit', ['course_id' => $course->id]))->setDataResponse($course)->setResponse(true, trans("course::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, isset($response['message']) ? $response['message'] : trans("course::messages.alert.mis_data"), isset($course->errors) ? $course->errors : null);
    }

    /**
     * @return $this|array
     */
    private function saveCourseService()
    {
        $course = new Course();

        $course_id = $this->request->get('course_id');
//        if (!$this->checkCourseCode()) {
//            return ['action' => false, 'message' => trans('course::messages.alert.duplicate_course'), 'redirect' => $course_id ? route('course.edit', ['course_id' => $course_id]) : route("course.create")];
//        }

        $this->request = $this->requestValuesUpdate(['active_date' => 'Y-m-d H:i:s', 'expire_date' => 'Y-m-d H:i:s']);
        if ($course_id) {
            $course = $course->enable()->find($course_id);
            if (!$course) {
                return ['action' => false, 'course' => $course, 'redirect' => route('course.edit', ['course_id' => $this->request->get('course_id')])];
            }
        }

        $description = $this->request->get('description');
        $short_description = $this->request->get('short_description');

        if (!$short_description && $description) {
            $description = strip_tags($description);
            $description = str_replace("&nbsp;", " ", $description);
            $descArr = explode(".", $description);
            $short = $descArr[0];
            $short = str_limit($short, 250);
            $this->request->offsetSet('short_description', $short);
        }

        $categories = $this->request->get('categories');
        $category_id = $categories ? $categories[0] : null;
        $this->request->offsetSet('category_id', $category_id);
        $this->request->offsetSet('params', json_encode($this->request->get("params")));

//        dd($this->request->all(),$course->rules,$course->fill($this->request->all())->isValid());
        $this->validate($this->request, $course->rules);
        if (($course->fill($this->request->all()))) {

            $course->save();

            // add or edit sections
            $this->registerSectionRecords($course->id);

            $this->saveTeachers($course->id);
//            $this->saveEnrollments($course->id);
            $this->saveCategories($course->id);

            $course->updated = $course_id ? true : false;
            //set Default Meta in Course and change
            $course_id = $course->id;
            $defaultMetaGroup = MetaGroup::where("default", 1)->where("system", Course::SYSTEM_NAME)->get();
            $defaultMetaGroup = $defaultMetaGroup ? $defaultMetaGroup->pluck("id")->toArray() : [];
            $requestMetaGroup = array_map('intval', $this->request->get('meta_group_ids', []));
            $metaGroupIds = array_unique(array_merge($defaultMetaGroup, $requestMetaGroup));
            $this->saveMetaGroups($course_id, $metaGroupIds);

            $response = ['action' => true, 'course' => $course];
        } else {
            $response = ['action' => false, 'course' => $course];
        }

        return $response;
    }

    /**
     * @return bool
     */
    private function checkCourseCode()
    {
        $course = new Course();
        $course = $course->filterCurrentInstance()->where("code", $this->request->get("code"))->first();
        if ($course_id = $this->request->get("course_id")) {
            if ($course && $course->id != $course_id) {
                return false;
            }
        } else {
            if ($course) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return $this
     */
    protected function destroyCourse()
    {
        $course_id = $this->request->get('course_id');
        $course = Course::enable()->find($course_id);

        if ($course && $course->delete()) {
            return $this->setResponse(true, trans("course::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.del_un_success"));

    }

    /**
     * @param $course_id
     * @return mixed
     */
    protected function saveTeachers($course_id)
    {
        $teachers = $this->request->get('teachers');
        CourseTeacher::where('course_id', $course_id)->delete();

        if (empty($teachers)) {
            return false;
        }

        $insertItems = [];
        foreach ($teachers as $teacher) {
            $insertItems[] = ['course_id' => $course_id, 'teacher_id' => $teacher, 'created_at' => date("Y-m-d H:i:s")];
        }
        $courseTeacher = new CourseTeacher();
        $result = $courseTeacher->insert($insertItems);
        return $result;
    }

    /**
     * @param $course_id
     * @return mixed
     */
    protected function saveEnrollments($course_id)
    {
        $enrollments = $this->request->get('enrollments');
        $enrolls = CourseEnrollment::where('course_id', $course_id)->get();
        $enrolls = $enrolls ? $enrolls->pluck('enrollment_id')->toArray() : [];

        $enrolls = array_intersect($enrollments, $enrolls);

        if (count($enrolls)) {
            CourseEnrollment::where('course_id', $course_id)->whereNotIn('enrollment_id', $enrolls)->delete();
        }

        $enrollments = array_diff($enrollments, $enrolls);

        if (empty($enrollments)) {
            return false;
        }

        $insertItems = [];
        foreach ($enrollments as $enrollment) {
            $enroll = Enrollment::find($enrollment);

            if (!$enroll) {
                continue;
            }

            $insertItems[] = ['course_id' => $course_id
                , 'enrollment_id' => $enrollment
                , 'created_at' => date("Y-m-d H:i:s")];
        }
        $courseEnrollment = new CourseEnrollment();
        if (empty($insertItems)) {
            return false;
        }
        $result = $courseEnrollment->insert($insertItems);
        return $result;
    }

    /**
     * @param $course_id
     * @return bool
     */
    protected function saveCategories($course_id)
    {
        $categories = $this->request->get('categories');
        CourseCategoryRelation::where('course_id', $course_id)->delete();

        if (empty($categories)) {
            return false;
        }

        $insertItems = [];
        foreach ($categories as $category) {
            $insertItems[] = ['course_id' => $course_id, 'category_id' => $category];
        }
        $courseCR = new CourseCategoryRelation();
        $result = $courseCR->insert($insertItems);
        return $result;
    }

    /**
     * @param $course_id
     * @param $meta_group_ids
     * @return bool
     */
    protected function saveMetaGroups($course_id, $meta_group_ids)
    {

        $cmgModel = new CourseMetaGroup();
        $system = $cmgModel->getTable();
        $mgIds = $cmgModel->where('course_id', $course_id)->pluck('meta_group_id')->toArray();
        $cmgIdsDelete = array_diff($meta_group_ids, $mgIds);

        if ($cmgIdsDelete) {
//            FeatureValueRelation::where('system', $system)->whereIn('item_id', $cmgIds)->delete();
            CourseMetaGroup::whereIn('id', $cmgIdsDelete)->where('course_id', $course_id)->delete();
        }

        if (empty($meta_group_ids) || !$course_id || ($mgIds == $meta_group_ids)) {
            return false;
        }

        $insertItems = [];
        foreach ($meta_group_ids as $meta_group_id) {
            $insertItems[] = ['meta_group_id' => $meta_group_id, 'course_id' => $course_id];
        }

        $result = CourseMetaGroup::insert($insertItems);
        return $result;
    }

    /**
     * @param $course_id
     * @return bool
     */
    public function registerSectionRecords($course_id)
    {
        $items = [
            'course_id' => $course_id,
            'active' => 1
        ];

        //insert
        $title_new = $this->request->get('title_new');
        $sort_new = $this->request->get('sort_new');
        if ($title_new) {
            foreach ($title_new as $key => $value) {
                if (!$value) {
                    continue;
                }
                $courseSection = new CourseSection();
                $items['title'] = $value;
                $items['sort'] = isset($sort_new[$key]) ? $sort_new[$key] : $key;
                $courseSection->fill($items)->save();
            }
        }

        $items = [
            'course_id' => $course_id,
            'active' => 1
        ];
        //edit
        $title_section = $this->request->get('title_section');
        $sort_section = $this->request->get('sort_section');
        if ($title_section) {
            foreach ($title_section as $key => $value) {
                $courseSection = new CourseSection();
                $courseSection = $courseSection->find($key);
                if ($courseSection) {
                    $items['title'] = $value;
                    $items['sort'] = isset($sort_section[$key]) ? $sort_section[$key] : 0;
                    $courseSection->fill($items)->save();
                }
            }
        }

        return true;
    }

    public function registerSectionRecords_old($course_id)
    {
        $count_sections = $this->request->get('count_sections', 1);
        $items = [
            'course_id' => $course_id,
            'active' => 1
        ];

        for ($i = 0; $count_sections > $i; $i++) {
            $courseSection = new CourseSection();
            $items['title'] = "section " . ($i + 1);
            $items['sort'] = $i;
            $courseSection->fill($items)->save();
        }
        return true;
    }

    /**
     * @return $this
     */
    protected function saveFeaturesValue()
    {
        $course_meta_group_id = $this->request->get('course_meta_group_id');
        $course_id = $this->request->get('course_id');
        $courseMetaGroup = CourseMetaGroup::find($course_meta_group_id);

        if (!$courseMetaGroup) {
            return $this->redirectBack()->setResponse(false, trans("course::messages.alert.not_find_data"));
        }
        $courseMetaGroup->save();
        //save time

        //insert feature values
        $features = $this->request->get('feature');

        if (!$features) {
            return $this->redirectBack()->setResponse(false);
        }

        $feature_ids = array_keys($features);

        $featureModel = new Feature();
        $useFeatures = $featureModel->active()->whereIn('id', $feature_ids)->get();

        if (!$useFeatures) {
            return $this->redirectBack()->setResponse(false);
        }

        $featuresType = $useFeatures->pluck('type', 'id')->toArray();

        $customTypes = $featureModel->customTypes;

        $insertItems = [];
        $language_id = app('getInstanceObject')->getLanguageId();
        $item_id = $courseMetaGroup->id;
        $system = 'course_meta_groups';
        foreach ($features as $f_id => $value) {
            $type = isset($featuresType[$f_id]) ? $featuresType[$f_id] : null;

            if (!$type || !$value)
                continue;

            if (in_array($type, $customTypes)) {
                //insert value
                $insertItems[] = ['language_id' => $language_id, 'item_id' => $item_id, 'system' => $system, 'feature_id' => $f_id, 'feature_value' => $value, 'feature_value_id' => null, 'created_at' => date("Y-m-d H:i:s")];
            } else {
                //insert value_id
                if (is_array($value)) {

                    foreach ($value as $item) {
                        $insertItems[] = ['language_id' => $language_id, 'item_id' => $item_id, 'system' => $system, 'feature_id' => $f_id, 'feature_value' => null, 'feature_value_id' => $item, 'created_at' => date("Y-m-d H:i:s")];
                    }

                } else {
                    $insertItems[] = ['language_id' => $language_id, 'item_id' => $item_id, 'system' => $system, 'feature_id' => $f_id, 'feature_value' => null, 'feature_value_id' => $value, 'created_at' => date("Y-m-d H:i:s")];
                }
            }


        }

        $featureValueRelation = new FeatureValueRelation();
        $resultDel = $featureValueRelation->active()->where('system', $system)->where('item_id', $item_id)->delete();

        if (!$insertItems) {
            return $this->redirectBack()->setResponse(false);
        }

        $result = $featureValueRelation->insert($insertItems);
        //insert feature values

        if ($result) {
            return $this->redirect(route('course.meta.editMetaTags', ['course_id' => $course_id]))->setResponse(true, trans("course::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, trans("course::messages.alert.save_un_success"));

    }


    /**
     * @return $this
     */
    protected function setAssetsEditMetaTags()
    {
        $this->course_meta_assets = true;
        return $this;
    }

    /**
     * @param null $course_id
     * @return $this
     * @throws \Throwable
     */
    public function editMetaTags($course_id = null)
    {
        $course = new Course();
        $course = $course->enable()->with(['language'
            , 'instance'
            , 'metaGroups.featureGroups.featureGroupRelations.feature.featureValues'
        ])->find($course_id ?: $this->request->get('course_id'));

        if (!$course) {
            return $this->redirect(route('course.meta.editMetaTags', ['course_id' => $this->request->get('course_id')]))->setResponse(false, trans("course::messages.alert.not_find_data"));
        }

        $courseMetaGroups = $metaGroups = null;
        $metaFeatureDataList = $metaGroupsActive = [];
        $metaGroups = MetaGroup::active()->filterLanguage()->filterSystem(Course::SYSTEM_NAME)->get();

        $courseMetaGroups = $course->courseMetaGroups()->get();

        if ($courseMetaGroups) {
            $metaGroupsActive = $courseMetaGroups->pluck('meta_group_id')->toArray();

            $listMetaGroup = $metaGroups ? $metaGroups->pluck('title', 'id') : null;

            $courseMetaGroups = ($courseMetaGroups)->map(function ($item) use ($listMetaGroup) {
                $item['group_name'] = isset($listMetaGroup[$item->meta_group_id]) ? $listMetaGroup[$item->meta_group_id] : null;
                return $item;
            });

            $courseMetaGroupList = $courseMetaGroups->pluck('meta_group_id', 'id');
            $metaFeatureData = FeatureValueRelation::active()->where('system', 'course_meta_groups')->whereIn('item_id', ($courseMetaGroupList->keys()))->get();

            foreach ($metaFeatureData as $item) {
                $metaFeatureDataList[$item->item_id][$item->feature_id][] = $item->feature_value_id ? $item->feature_value_id : $item->feature_value;
            }

        }

        if (app("getInstanceObject")->isApi()) {
            return $this->editMetaTagsJson($course, $courseMetaGroups, $metaGroupsActive, $metaFeatureDataList);
        }

        $this->setTitlePage(trans('course::course.edit_meta_tags') . ':' . $course->title);

        return $this->renderedView("course::course.form_course_meta_tags", ['course' => $course, 'metaGroups' => $metaGroups
            , 'metaGroupsActive' => $metaGroupsActive, 'courseMetaGroups' => $courseMetaGroups
            , 'metaFeatureDataList' => $metaFeatureDataList, 'view_model' => $this], "form");


    }

    protected function editMetaTagsJson($course, $courseMetaGroups, $metaGroupsActive, $metaFeatureDataList)
    {
        $result = [];
        if (isset($course) && $course && !empty($metaGroupsActive)) {

            foreach ($courseMetaGroups as $courseMetaGroup) {

                foreach ($courseMetaGroup->metaGroup->featureGroups as $featureGroup) {

                    foreach ($featureGroup->featureGroupRelations as $featureGroupRelation) {

                        if ($featureGroupRelation->feature) {

                            $activeValue = null;
                            if ($metaFeatureDataList && isset($metaFeatureDataList[$courseMetaGroup->id][$featureGroupRelation->feature->id])) {
                                $activeValue = $metaFeatureDataList[$courseMetaGroup->id][$featureGroupRelation->feature->id];
                            }

                            $result[] = $this->setFeatureGroup($featureGroup->title)
                                ->setFeatureModel($featureGroupRelation->feature)
                                ->setActiveValueFeature($activeValue)
                                ->get();

                        }

                    }

                }


            }

        }

        return $result;
    }


    /**
     * @return $this
     */
    protected function copyCourseMetaGroup()
    {
        $course_meta_group_id = $this->request->get('course_meta_group_id');
        $course_id = $this->request->get('course_id');
        $courseMetaGroup = CourseMetaGroup::find($course_meta_group_id);

        $resultCopy = null;
        if ($courseMetaGroup) {
            $newCourseMetaGroup = $courseMetaGroup->replicate();
            $resultCopy = $newCourseMetaGroup->save();
            if ($resultCopy) {
                $system = 'course_meta_groups';
                $this->copyFeatureValueRelation($course_meta_group_id, $system, $newCourseMetaGroup->id);
            }

        }
        return $this->redirectBack()->setResponse($resultCopy ? true : false, trans("course::messages.alert." . ($resultCopy ? 'save_success' : 'save_un_success')));

    }

    /**
     * @param $item_id
     * @param $system
     * @param $new_item_id
     * @return bool
     */
    protected function copyFeatureValueRelation($item_id, $system, $new_item_id)
    {
        $featureValueRelation = new FeatureValueRelation();
        $fvrs = $featureValueRelation->active()->where('system', $system)->where('item_id', $item_id)->get();

        if (!$fvrs) {
            return false;
        }

        $insertItem = [];
        foreach ($fvrs as $fvr) {
            $insertItem[] = ['language_id' => $fvr->language_id, 'system' => $fvr->system, 'item_id' => $new_item_id
                , 'feature_id' => $fvr->feature_id, 'feature_value_id' => $fvr->feature_value_id
                , 'feature_value' => $fvr->feature_value, 'active' => $fvr->active];
        }

        $featureValueRelation->insert($insertItem);
        return true;
    }

    /**
     * @return $this
     */
    protected function deleteCourseMetaGroup()
    {
        $course_meta_group_id = $this->request->get('course_meta_group_id');
        $course_id = $this->request->get('course_id');
        $courseMetaGroup = CourseMetaGroup::find($course_meta_group_id);
        $resultDelete = null;
        if ($courseMetaGroup) {
            $resultDelete = $courseMetaGroup->delete();
            if ($resultDelete) {
                $system = 'course_meta_groups';
                $this->deleteFeatureValueRelation($course_meta_group_id, $system);
            }

        }
        return $this->redirectBack()->setResponse($resultDelete ? true : false, trans("course::messages.alert." . ($resultDelete ? 'del_success' : 'del_un_success')));
    }

    /**
     * @param $item_id
     * @param $system
     * @return bool
     */
    protected function deleteFeatureValueRelation($item_id, $system)
    {
        $featureValueRelation = new FeatureValueRelation();
        $fvrs = $featureValueRelation->active()->where('system', $system)->where('item_id', $item_id)->delete();
        if (!$fvrs) {
            return false;
        }
        return true;
    }

    /**
     * @return CourseViewModel
     */
    protected function getCoursesByCategory()
    {
        $model = new CourseCategoryRelation();
        $list = $model->setRequestItems($this->request->all())->filters()->isVisible()->with('course')->simplePaginate();
        $list = $this->decorateList($list, 'course');
        return $this->setDataResponse($list)->setResponse($list ? true : false);
    }

    /**
     * @return CourseViewModel
     */
    protected function show()
    {
        $course = new Course();
        $course = $course->enable()->with([
            'language'
            , 'courseSections'
            , 'instance'
            , 'courseCategory'
            , 'courseTeachers'
            , 'courseEnrollments'
        ])->find($this->request->get('course_id'));
        $course = $this->decorateAttributes($course);
        return $this->setDataResponse($course)->setResponse($course ? true : false);

    }

}
