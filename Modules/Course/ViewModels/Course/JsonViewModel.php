<?php

namespace Modules\Course\ViewModels\Course;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\Course\Models\Course\Course;

class JsonViewModel extends BaseViewModel
{
    public function getCourseByApi(){
        $q=false;
        $selected=false;
        if($this->request->has('q')){
            $q=$this->request->get('q');
        }elseif($this->request->has('selected')){
            $inputs=explode(',',$this->request->get('selected'));
            $selected=[];
            foreach($inputs as $select){
                $select=trim($select);
                if(strlen($select)){
                    $selected[]=$select;
                }
            }
        }
        $model=Course::select('id','title')->filterCurrentInstance();
        if($q){
            $model->where('title', 'like', '%'.$q.'%');
            $items=$model->get();
        }elseif($selected && count($selected)){
            $model->whereIN('id', $selected);
            $items=$model->get();

        }else{
            $model=Course::select('id','title')->active()->filterCurrentInstance()->orderBy('id','DESC')->take(10);
            $items=$model->get();
        }
        $items=$items->map(function ($item) {
            $item->text=$item->title;
            return $item;
        });
        return $this->setDataResponse($items)->setResponse(true) ;
    }
}
