<?php

namespace Modules\Course\ViewModels\Course;


use Illuminate\Http\Request;
use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureValueRelation;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Meta\MetaGroup;
use Modules\Core\Traits\Feature\FormFeatureBuilder;
use Modules\Course\Models\Course\Course;
use Modules\Core\Models\Language\Language;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Models\Course\CourseActivity;
use Modules\Course\Models\Course\CourseMetaGroup;
use Modules\Course\Models\Course\CourseSection;
use Modules\Course\Models\Course\CourseTeacher;
use Modules\Course\Models\Enrollment;
use Modules\Course\Models\Enrollment\CourseEnrollment;
use Modules\Course\Models\Teacher\Teacher;
use Modules\Course\Models\Course\CourseCategoryRelation;


class CopyViewModel extends BaseViewModel
{

    use GridViewModel;

    private $course_assets;
    private $course_id = 0;
    private $instance_id;
    private $title;
    private $copy_parts = [];

    public function __construct()
    {
//        $this->setModel(new Course());
    }

    /**
     * @return $this
     */
    public function boot()
    {
        $this->course_id = $this->request->get("course_id");
        $this->copy_parts = $this->request->get("copy_parts");
        $this->instance_id = $this->request->get("instance_id");
        $this->title = $this->request->get("title");
        $this->setModel(new Course());
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $course_assets = [];
        $course_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/course/course.js"), 'name' => 'course-backend'];

        return $course_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return $this
     */
    protected function setAssetsCreateCourseSection()
    {
        $this->course_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function indexCourseCopy()
    {
        $this->boot();
        $viewModel =& $this;
        $instances = Instance::active()->get();
        $instances = $instances ? $instances->pluck('name', 'id') : [];

        $this->setTitlePage($this->getTitle());
        return $this->renderedView("course::copy.form_copy", ['viewModel' => $viewModel, 'instances' => $instances], "form");
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getTitle()
    {
        $course = $this->getCourse();
        $title = $course ? $course->title : '--';
        $title = trans('course::course.course_copy.title', ['course_title' => $title]);
        return $title;
    }

    /**
     * @return mixed
     */
    public function getCourseId()
    {
        return $this->course_id;
    }

    /**
     * @return mixed
     */
    public function getCourse()
    {
        return $this->useModel->find($this->getCourseId());
    }

    /**
     * @return array
     */
    public function getCopyParts()
    {
        $parts = [
            'enrollment',
            'activity',
            'section',
            'category',
            'teacher',
        ];
        return $parts;

    }

    private $copy_section = false;
    private $copy_activity = false;

    /**
     * @return $this
     */
    public function saveCourseCopy()
    {
        $this->boot();

        $result = [];
        $result["course"] = $newCourse = $this->copyCourse();
        $new_course_id = $newCourse ? $newCourse->id : 0;
        if (!$new_course_id) {
            return $this->redirectBack()->setResponse(false, trans("course::messages.alert.mis_data"));
        }

        $this->copy_section = in_array("section", $this->copy_parts) ? true : false;
        $this->copy_activity = in_array("activity", $this->copy_parts) ? true : false;

        foreach ($this->copy_parts as $copy_part) {
            if (method_exists($this, "copy" . ucfirst($copy_part))) {
                $result[$copy_part] = $this->{"copy" . ucfirst($copy_part)}($new_course_id);
            }
        }

        return $this->redirect(route('course.index'))->setResponse(true, trans("course::messages.alert.save_success"));
    }

    /**
     * @return mixed
     */
    public function copyCourse()
    {
        $course = $this->useModel->find($this->getCourseId());
        $newCourse = $course->replicate();
        $newCourse->title = $this->title;
        $newCourse->instance_id = $this->instance_id;
        $newCourse->code = $newCourse->getGenerateCourseCode();
        $newCourse->save();
        return $newCourse;
    }

    /**
     * @param $new_course_id
     * @return bool
     */
    public function copyEnrollment($new_course_id)
    {
        $model = new CourseEnrollment();

        $rows = $model->active()->where('course_id', $this->course_id)->get();
        if (!$rows) {
            return false;
        }

        foreach ($rows as $row) {
            $newRow = $row->replicate();
            $newRow->course_id = $new_course_id;
            $newRow->save();
        }
        return true;
    }

    private $copied_activity = false;
    /**
     * @param $new_course_id
     * @return bool
     */
    public function copyActivity($new_course_id)
    {

        if($this->copy_section && !$this->copied_section){
           return $this->copySection($new_course_id);
        }

        if(!$this->copied_activity){
            $section = CourseSection::active()->where('course_id', $this->course_id)->first();
            $newSecion = $section->replicate();
            $newSecion->course_id = $new_course_id;
            $newSecion->sort = 0;
            $newSecion->title = trans('course::course.section_default') ;
            if($newSecion->save()){
                $new_course_section_id = $newSecion->id;
            }else{
                return false;
            }

            $model = new CourseActivity();
            $rows = $model->active()->where('course_id', $this->course_id)->get();
            if (!$rows) {
                return false;
            }

            foreach ($rows as $row) {
                $newRow = $row->replicate();
                $newRow->course_id = $new_course_id;
                $newRow->course_section_id = $new_course_section_id;
                $newRow->save();
            }
            return true;
        }else{
            return false;
        }

    }

    /**
     * @param $course_section_id
     * @param $old_course_section_id
     * @param $new_course_id
     * @return bool
     */
    public function copySingleActivity($course_section_id, $old_course_section_id, $new_course_id)
    {
        $model = new CourseActivity();
        $rows = $model->active()->where('course_id', $this->course_id)->where('course_section_id', $old_course_section_id)->get();
        if (!$rows) {
            return false;
        }

        foreach ($rows as $row) {
            $newRow = $row->replicate();
            $newRow->course_id = $new_course_id;
            $newRow->course_section_id = $course_section_id;
            $newRow->save();
        }
        return true;

    }

    private $copied_section = false;
    /**
     * @param $new_course_id
     * @return bool
     */
    public function copySection($new_course_id)
    {
        if($this->copied_section){
            return false;
        }

        $model = new CourseSection();
        $rows = $model->active()->where('course_id', $this->course_id)->get();
        if (!$rows) {
            return false;
        }

        foreach ($rows as $row) {
            $newRow = $row->replicate();
            $newRow->course_id = $new_course_id;
            $newRow->save();

            if ($this->copy_activity && $newRow->id) {
                $this->copySingleActivity($newRow->id, $row->id, $new_course_id);
                $this->copied_activity = true;
            }

        }
        $this->copied_section = true;
        return true;
    }

    /**
     * @param $new_course_id
     * @return bool
     */
    public function copyCategory($new_course_id)
    {
        $model = new CourseCategoryRelation();

        $rows = $model->where('course_id', $this->course_id)->get();
        if (!$rows) {
            return false;
        }

        foreach ($rows as $row) {
            $newModel = new CourseCategoryRelation();
            $newModel->insert(['category_id'=>$row->category_id, 'course_id'=>$new_course_id]);
        }
        return true;
    }

    /**
     * @param $new_course_id
     * @return bool
     */
    public function copyTeacher($new_course_id)
    {
        $model = new CourseTeacher();

        $rows = $model->active()->where('course_id', $this->course_id)->get();
        if (!$rows) {
            return false;
        }

        foreach ($rows as $row) {
            $newRow = $row->replicate();
            $newRow->course_id = $new_course_id;
            $newRow->save();
        }
        return true;
    }


}
