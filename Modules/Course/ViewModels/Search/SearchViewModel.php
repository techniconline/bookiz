<?php

namespace Modules\Course\ViewModels\Search;


use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Modules\Course\Models\Course\Course;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Course\Models\Enrollment;
use Modules\Course\Traits\Page\EnrollmentViewModelTrait;
use Modules\Course\Traits\Page\InfoViewModelTrait;
use Modules\Course\Providers\Helpers\Category\CategoryHelperFacade as CategoryHelper;


class SearchViewModel extends BaseViewModel
{
    public $listCourses;
    use InfoViewModelTrait;
    use EnrollmentViewModelTrait;

    public function __construct()
    {
        parent::__construct();
        $this->setModel(new Course());
    }

    /**
     * @param $course_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getCourseEnrollmentsHtml($course_id)
    {
        return $this->getCourseEnrollments($course_id, 'list');
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function searchCourses()
    {
        $viewModel =& $this;
        $model = new Course();
        $this->perPage=12;
        $this->listCourses = $model->setRequestItems($this->request->all())->active()->with('courseCategories','courseEnrollments')->searching()->paginate($this->perPage);
        if (app("getInstanceObject")->isApi()) {
            $this->listCourses = $this->decorateList($this->listCourses, null, ['students','duration']);
            return $this->setDataResponse($this->listCourses, "result")
                ->setResponse($this->listCourses ? true : false, $this->listCourses ? null : trans("course::messages.alert.not_find_data"));
        }
        return $this->renderedView('course::course.list', compact('viewModel'));
//        return $this->setDataResponse($list)->setResponse(!$list->isEmpty(), $list->isEmpty() ? trans("course::messages.alert.not_find_data") : '' );
    }

    /**
     * @return $this
     */
    protected function parameters()
    {

        $response = [];
        if (isset($this->config["config"]["search"]["parameters"]) && $parameters = $this->config["config"]["search"]["parameters"]) {

            foreach ($parameters as $key => $parameter) {
                if (isset($parameter['active']) && $parameter['active']) {
                    if (method_exists($this, "get" . ucfirst($key))) {
                        $response[$key]['result'] = $this->{"get" . ucfirst($key)}();
                        $response[$key]['filter_by'] = $parameter['filter_by'];
                    }
                }
            }

        }

        return $this->setDataResponse($response, 'result')
            ->setResponse($response ? true : false, $response ? null : trans("course::messages.alert.not_find_data"));
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function getCategory()
    {
        return CategoryHelper::getNestableCourseCategories();
    }

    /**
     * @return mixed
     */
    public function getEnrollment()
    {
        $enrollments = Enrollment::active()->notLoadEnrolls()->get();
        $enrollments = $this->decorateList($enrollments);
        return $enrollments;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getSorting()
    {
        $sortings = trans("course::course.sort_items");
        return collect($sortings)->map(function ($item, $key) {
            return ['key' => $key, 'value' => $item];
        })->values();
    }

    /**
     * @return array
     */
    public function getText()
    {
        return ['key' => 'text', 'value' => trans('course::course.course_title')];
    }

    /**
     * @return $this
     */
    public function getCourseByApi()
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }

        $model = new Course();
        $model = $model->enable()->filterCurrentInstance()->select('id', DB::raw('concat(title,"(",type,")") as text'));
        if ($q) {

            $model->where(function ($query) use ($q) {
                $query->where('title', 'LIKE', '%' . $q . '%');
            });
            $items = $model->get();

        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $items = $this->request->has('selected')?[]:$model->orderBy('id','DESC')->take(20)->get();
        }

        return $this->setDataResponse($items)->setResponse(true);
    }


}
