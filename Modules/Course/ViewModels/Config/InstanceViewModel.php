<?php

namespace Modules\Course\ViewModels\Config;

use Illuminate\Support\Facades\Route;
use Modules\Core\ViewModels\Config\SettingViewModel;
use Modules\Course\Traits\Course\CourseInfo;

class InstanceViewModel extends SettingViewModel
{

    use CourseInfo;
    public $setting_name='instance';
    public $module_name='course';

    public function getConfigForm(){
        $BlockViewModel=&$this;
        $this->setTitlePage(trans('course::menu.config.instance'));
        return $this->renderedView('course::config.instance',compact('BlockViewModel'));
    }


    public function save(){
        $rules=[];
        $this->requestValuesUpdate();
        $data=$this->request->all();
        $result=$this->saveConfig($data);
        if($result){
            return $this->redirectBack()->setResponse($result,trans('core::messages.alert.save_success'));
        }
        return $this->redirectBack()->setResponse($result,trans('core::messages.alert.error'));

    }

}
