<?php

namespace Modules\Course\ViewModels\Blocks\CourseInfoBlock;


use Modules\Core\Traits\Block\MasterBlockConfig;
use Modules\Course\Traits\Course\CourseInfo;

class CourseInfoBlockConfig
{
    use MasterBlockConfig,CourseInfo;

    public $baseClass='CourseInfoBlock';

    public function __construct(){
        $this->setDefaultName(trans('course::block.course_info_block'));
        $this->addTab('course_info_block',trans('course::block.course_info_block'),'course::widgets.info.config.form');
        $this->addTab('advanced_block',trans('course::block.advanced_block'),'course::widgets.info.config.advanced');
    }




}