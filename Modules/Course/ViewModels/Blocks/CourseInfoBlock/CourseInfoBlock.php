<?php

namespace Modules\Course\ViewModels\Blocks\CourseInfoBlock;

use Modules\Core\Traits\Block\MasterBlock;

use Modules\Course\Traits\Page\EnrollmentViewModelTrait;
use Modules\Course\Traits\Page\InfoViewModelTrait;

class CourseInfoBlock
{

    use MasterBlock;
    use InfoViewModelTrait;
    use EnrollmentViewModelTrait;
    public $relationalData=['course'];
    public $course=false;

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        if(!$this->hasRelationalData()){
            return '';
        }
        $this->course=$this->getRelationalDataOnInstance('course');
        if(isset($this->course->userRole)){
            $this->userRole=$this->course->userRole;
        }else{
            $this->setUserRoleAndAccessOnCourse($this->course->id);
        }
        $blockViewModel=& $this;
        return view('course::widgets.info.index',compact('blockViewModel'))->render();
    }


    public function getFields(){
        return $this->getCourseInfoArray($this->getConfig('fields'),$this->course->id);
    }


    public function getCourseEnrollmentsHtml(){
        return $this->getCourseEnrollments($this->course->id,'widget');
    }
}
