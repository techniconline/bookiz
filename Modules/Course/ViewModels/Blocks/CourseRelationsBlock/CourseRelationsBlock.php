<?php

namespace Modules\Course\ViewModels\Blocks\CourseRelationsBlock;

use Modules\Core\Traits\Block\MasterBlock;

use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Course\CourseCategoryRelation;
use Modules\Course\Models\Course\CourseRelation;
use Modules\Course\Traits\Page\EnrollmentViewModelTrait;
use Modules\Course\Traits\Page\InfoViewModelTrait;
use Modules\Course\ViewModels\Course\RelationsViewModel;

class CourseRelationsBlock
{

    use MasterBlock;
    use EnrollmentViewModelTrait;
    use InfoViewModelTrait;

    public $relationalData = ['course'];
    public $course = false;

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        if (!$this->hasRelationalData()) {
            return '';
        }
        $this->course = $this->getRelationalDataOnInstance('course');
        if (isset($this->course->userRole)) {
            $this->userRole = $this->course->userRole;
        }
        $blockViewModel =& $this;
        return view('course::widgets.course_relations.slider', compact('blockViewModel'))->render();
    }

    /**
     * @return null
     */
    public function getCourseRelations()
    {
        if ($this->course) {
            $key='course_relation_id_'.$this->course->id;
            if(!($relations=cache($key))){
                $model = new CourseRelation();
                $relations = $model->active()
                    ->where('relation_course_id', '!=', $this->course->id)
                    ->where(function ($q) {
                        if ($type = $this->getConfig('type')) {
                            $q->where('type', $type);
                        }
                    })
                    ->where('course_id', $this->course->id)->with('courseRelation')
                    ->take($this->getConfig('take_number', 6))
                    ->get();

                $relations = $this->decorateByType($relations);

                if (!$relations || count($relations)==0) {
                    $relations['normal'] = $this->getCourseRelationRandom();
                    if(!count($relations['normal'])){
                        $relations=[];
                    }
                }
                cache($key,$relations,1440);
            }
            return $relations;
        }
        return null;
    }

    /**
     * @return mixed
     */
    private function getCourseRelationRandom()
    {
        $category_id = 0;
        if ($this->course) {
            $category = CourseCategoryRelation::where('course_id', $this->course->id)->orderBy('category_id','DESC')->first();
            $category_id = $category ? $category->category_id : 0;
        }
        return $this->getCourseRelationByCategory($category_id);
    }

    /**
     * @param $category_id
     * @return mixed
     */
    private function getCourseRelationByCategory($category_id)
    {
        $model = new Course();
        return $model->setRequestItems(['category_id' => $category_id])
            ->filterByCategories()->where('id','!=',$this->course->id)->with('courseCategories')->filterCurrentInstance()
            ->active()
            ->inRandomOrder()
            ->take($this->getConfig('take_number', 6))
            ->get();

    }

    /**
     * @param $courseRelations
     * @return array|null
     */
    private function decorateByType($courseRelations)
    {
        if (!$courseRelations) {
            return null;
        }
        $newResult = [];
        foreach ($courseRelations as $item) {
            if($item->courseRelation->id !=$this->course->id){
                $newResult[$item->type][] = $item->courseRelation;
            }
        }
        return $newResult;
    }

    /**
     * @return RelationsViewModel
     */
    public function getRelationViewModel()
    {
        return new RelationsViewModel();
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->getRelationViewModel()->getTypes();
    }

    /**
     * @param $course_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getCourseEnrollmentsHtml($course_id)
    {
        return $this->getCourseEnrollments($course_id, 'list');
    }


}
