<?php

namespace Modules\Course\ViewModels\Blocks\CourseRelationsBlock;


use Modules\Core\Traits\Block\MasterBlockConfig;
use Modules\Course\ViewModels\Course\RelationsViewModel;

class CourseRelationsBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='CourseRelationsBlock';

    public function __construct(){
        $this->setDefaultName(trans('course::block.course_relations_block'));
        $this->addTab('course_relations_block',trans('course::block.course_relations_block'),'course::widgets.course_relations.config.form');
        $this->addTab('advanced_block',trans('course::block.advanced_block'),'course::widgets.course_relations.config.advanced');
    }


    /**
     * @return RelationsViewModel
     */
    public function getRelationViewModel()
    {
        return new RelationsViewModel();
    }

    /**
     * @return array
     */
    public function getTypes(){
        return $this->getRelationViewModel()->getTypes();
    }


}