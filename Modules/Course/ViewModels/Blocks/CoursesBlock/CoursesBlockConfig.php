<?php

namespace Modules\Course\ViewModels\Blocks\CoursesBlock;


use Modules\Core\Traits\Block\MasterBlockConfig;
use Modules\Course\Models\Course\CourseCategory;

class CoursesBlockConfig
{
    use MasterBlockConfig;

    public $baseClass = 'CoursesBlock';

    public function __construct()
    {
        $this->setDefaultName(trans('course::block.courses_block'));
        $this->addTab('courses_block', trans('course::block.courses_block'), 'course::widgets.courses.config.form');
        $this->addTab('advanced_block', trans('course::block.advanced_block'), 'course::widgets.courses.config.advanced');
    }


    /**
     * @return array
     */
    public function getTemplateList()
    {
        $list = [
            'slider' => trans("course::form.fields.templates.slider"),
            'list_box' => trans("course::form.fields.templates.list_box"),
        ];
        return $list;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getSortList()
    {
        $list = trans("course::course.sort_items");
        return $list;
    }

    /**
     * @return array
     */
    public function getCourseCategoriesList()
    {
        $course_categoryList = CourseCategory::filterLanguage()->filterCurrentInstance()->with("parentRecursive")->get();
        $list = [];
        $list[0] = "...";
        foreach ($course_categoryList as $course_category) {
            $this->parents = [];
            $this->getParents($course_category);
            $list[$course_category->id] = implode(" > ", $this->parents) . " > " . $course_category->title;
        }
        return $list;
    }

    private $parents = [];

    /**
     * @param $row
     * @return mixed
     */
    public function getParents($row)
    {
        if ($row->parentRecursive) {
            $this->parents[] = $this->getParents($row->parentRecursive, false);
        }
        return $row->title;
    }


}