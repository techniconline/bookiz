<?php

namespace Modules\Course\ViewModels\Blocks\CoursesBlock;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Traits\Block\MasterBlock;

use Modules\Core\Traits\Decorate\DecorateData;
use Modules\Course\Models\Course\Course;
use Modules\Course\Traits\Page\EnrollmentViewModelTrait;
use Modules\Course\Traits\Page\InfoViewModelTrait;

class CoursesBlock
{

    use MasterBlock;
    use InfoViewModelTrait;
    use EnrollmentViewModelTrait;
    use DecorateData;

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel =& $this;
        return view('course::widgets.courses.' . $this->getConfig("template", "slider"), compact('blockViewModel'))->render();
    }

    /**
     * @return array
     */
    public function renderJson()
    {
        $courses = $this->renderDataCourses($this->getCoursesList());
        $block = $this->setDataBlock($courses)->getBlock();
        return $block;
    }

    /**
     * @param $courses
     * @return mixed
     */
    private function renderDataCourses($courses)
    {
        return $this->decorateList($courses, null, ['students']);
    }

    /**
     * @param bool $paginate
     * @return mixed
     */
    public function getCoursesList($paginate = false)
    {
        $model = new Course();
        $take = $this->getConfig("course_count_show", 8);
        $items = [
            "category_id" => $this->getConfig("course_category_id", 0),
            "sort_by" => $this->getConfig("sort_by", "new"),
        ];
        $data = $model->setRequestItems($items)
            ->filterLanguage()->filterCurrentInstance()
            ->filterByCategories()->active()
            ->sortBy();

        if ($this->getConfig("is_random")) {
            $take = $take * 3;
        }

        if ($paginate) {
            $data = $data->paginate($take);
        } else {
            if ($take) {
                $data = $data->take($take);
            }
            $data = $data->get();

            if ($this->getConfig("is_random")) {
                $data = $this->getRandomData($data);
            }
        }
        return $data;
    }

    /**
     * @param $data
     * @return \Illuminate\Support\Collection|static
     */
    public function getRandomData($data)
    {
        try {
            $data = collect($data);
            $count = $data->count();
            $data = $data->map(function ($row) use ($count) {
                $row['_sort_row'] = rand(0, $count);
                return $row;
            })->sortBy('_sort_row');
            return $data;
        } catch (\Exception $exception) {
            return $data;
        }
    }

    /**
     * @param $course_id
     * @return array|\Illuminate\Support\Collection
     */
    public function getFields($course_id)
    {
        return $this->getCourseInfoArray($this->getConfig('fields'), $course_id);
    }


    /**
     * @param $course_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getCourseEnrollmentsHtml($course_id)
    {
        return $this->getCourseEnrollments($course_id, 'list');
    }


    /**
     * @return mixed
     */
    public function getCoursesListPaginate()
    {
        return $this->getCoursesList(true);
    }


}
