<?php

namespace Modules\Course\ViewModels\Blocks\CourseCategoryBlock;


use BridgeHelper;
use Modules\Core\Traits\Block\MasterBlock;
use Modules\Course\Models\Course\CourseCategory;
use Modules\Course\Providers\Helpers\Category\CategoryHelperFacade as CategoryHelper;

class CourseCategoryBlock
{
    use MasterBlock;

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel =& $this;
        return view('course::widgets.course_category.index', compact('blockViewModel'))->render();
    }

    /**
     * @return array
     */
    public function renderJson()
    {
        $categories = $this->getCourseCategories();
        $block = $this->setDataBlock($categories)->getBlock();
        return $block;
    }

    /**
     * @param $categories
     * @return mixed
     */
    private function getDataCategories($categories)
    {
        foreach ($categories as $category) {
            $category->url = $this->getCourseCategoryUrl($category);
            if ($category->childes->count()) {
                $this->getDataCategories($category->childes);
            }
        }
        return $categories;
    }

    /**
     * @param $course_category
     * @return string
     */
    public function getCourseCategoryUrl($course_category)
    {
        return $course_category["url_courses"];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getCourseCategories()
    {
        $id = $this->getConfig('course_category_id');
        if ($this->getConfig("root_course_category_id")) {
            $id = $this->getConfig("root_course_category_id");
        }
        if ($id){
            $courseCategories = CategoryHelper::getNestableCourseCategory($id, $this->getConfig('deep'),["courses_count"]);
        }else{
            $courseCategories = CategoryHelper::getNestableCourseCategories(0,["courses_count"]);
        }
//        dd($courseCategories);
        return $courseCategories;
    }


}
