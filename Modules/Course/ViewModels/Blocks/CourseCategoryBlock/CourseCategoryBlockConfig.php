<?php

namespace Modules\Course\ViewModels\Blocks\CourseCategoryBlock;

use Modules\Core\Traits\Block\MasterBlockConfig;
use Modules\Course\Models\Course\CourseCategory;

class CourseCategoryBlockConfig
{
    use MasterBlockConfig;

    public $baseClass = 'CourseCategoryBlock';

    public function __construct()
    {
        $this->setDefaultName(trans('course::block.course_category'))
            ->addTab('course_category', trans('course::block.course_category'), 'course::widgets.course_category.config.form')
            ->addTab('course_category_advanced', trans('course::block.advanced'), 'course::widgets.course_category.config.advanced')
            ->addTab('course_category_mobile', trans('course::block.mobile'), 'course::widgets.course_category.config.mobile');
    }

    /**
     * @return array
     */
    public function getBlockRules()
    {
        return [
//            'course_category_id'=>'nullable|exists:course_categories,id'
        ];
    }

    /**
     * @return array
     */
    public function getShowList()
    {
        $list = [
            'normal' => trans("course::form.fields.list_types.normal"),
            'box_list' => trans("course::form.fields.list_types.box_list"),
        ];
        return $list;
    }

    /**
     * @return array
     */
    public function getCourseCategoryRootList()
    {
        $course_categoryList = CourseCategory::filterLanguage()->filterCurrentInstance()->whereNull("parent_id")->get();
        $list = [];
        $list[0] = "...";
        foreach ($course_categoryList as $course_category) {
            $list[$course_category->id] = $course_category->title;
        }
        return $list;
    }

    /**
     * @return array
     */
    public function getCourseCategoriesList()
    {
        $course_categoryList = CourseCategory::filterLanguage()->filterCurrentInstance()->with("parentRecursive")->get();
//        dd($course_categoryList->toArray());
        $list = [];
        $list[0] = "...";
        foreach ($course_categoryList as $course_category) {
            $this->parents = [];
            $this->getParents($course_category);
            $list[$course_category->id] = implode(" > ",$this->parents) ." > ". $course_category->title;
        }
        return $list;
    }

    private $parents = [];

    /**
     * @param $row
     * @param bool $first
     * @return mixed
     */
    public function getParents($row)
    {
        if ($row->parentRecursive) {
            $this->parents[] = $this->getParents($row->parentRecursive, false);
        }
        return $row->title;
    }

}
