<?php

namespace Modules\Course\ViewModels\Enrollment;


use Illuminate\Http\Request;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Libraries\Enrollment\MasterEnrollment;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Enrollment;
use Modules\User\Models\Role;


class CourseEnrollmentViewModel extends BaseViewModel
{

    use GridViewModel;

    private $enrollment_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = Enrollment\CourseEnrollment::enable()
            ->where('course_id', $this->request->get('course_id'))
            ->with(["course", "enrollment"]);
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $enrollment_assets = [];

        //use bested assets
        if ($this->enrollment_assets) {

        }

        if ($this->enrollment_assets) {
            $enrollment_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/course/enrollment.js"), 'name' => 'enrollment-backend'];
        }

        return $enrollment_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('course', trans('course::course.title'), false);
        $this->addColumn('enrollment', trans('course::enrollment.name'), false);
        $this->addColumn('active', trans('course::enrollment.status'), false);

        /*add action*/
        $add = array(
            'name' => 'course.enrollments.create',
            'parameter' => ["course_id" => ['value' => $this->request->get("course_id")]]
        );
        $this->addButton('create_enrollment', $add, trans('course::enrollment.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);


        /*back action*/
        $add = array(
            'name' => 'course.index',
            'parameter' => []
        );
        $this->addButton('back_course', $add, trans('course::course.list'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);


        $show = array(
            'name' => 'course.enrollments.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('course::enrollment.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);


        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('course::enrollment.confirm_delete')];
        $delete = array(
            'name' => 'course.enrollments.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('course::enrollment.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
//        $this->addFilter('name', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->course = $row->course->title;
        $row->enrollment = $row->enrollment->name;
        $row->active = trans("course::enrollment.statuses." . $row->active);
        return $row;
    }

    public function getActionsUpdate($row)
    {
        $actions = $this->actions;
        if ($row->enrollment->alias == 'package') {
            unset($actions['content'], $actions['delete']);
        } else {
            //
        }
        return $actions;
    }

    /**
     * @return $this
     */
    protected function setAssetsEditCourseEnrollment()
    {
        $this->enrollment_assets = true;
        return $this;
    }

    /**
     * @return $this
     */
    protected function setAssetsCreateCourseEnrollment()
    {
        $this->enrollment_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('course::enrollment.list'));
        $this->generateGridList()->renderedView("course::enrollment.index_course_enrollments", ['view_model' => $this], "enrollment_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createCourseEnrollment()
    {
        $courseEnrollment = new Enrollment\CourseEnrollment();


        $enrollments = Enrollment::active()->notLoadEnrolls()->get();
        $enrollments = $enrollments ? $enrollments->pluck("name", "id")->toArray() : [];

        $courseEnrollment = $courseEnrollment->enable()->with(['course', 'enrollment'])->find($this->request->get('course_enrollment_id'));

        $course_id = $courseEnrollment ? $courseEnrollment->course_id : $this->request->get('course_id');

        $roles = Role::active()->filterInstance()->get();
        $roles = $roles ? $roles->pluck("title", "id")->toArray() : [];

        $course = Course::find($course_id);

        $viewCourseEnrollment = $this->renderEnrollment($courseEnrollment);


        $this->setTitlePage(trans('course::enrollment.' . ($courseEnrollment ? 'edit' : 'add')));
        return $this->renderedView("course::enrollment.form_course_enrollment", ['courseEnrollment' => $courseEnrollment
            , 'course_id' => $course_id, 'course' => $course
            , 'enrollments' => $enrollments, 'roles' => $roles, 'viewCourseEnrollment' => $viewCourseEnrollment], "form");
    }


    /**
     * @return $this
     */
    protected function getViewEnrollment()
    {
        $enrollment = Enrollment::active()->find($this->request->get('enrollment_id'));
        $courseEnrollment = Enrollment\CourseEnrollment::active()->find($this->request->get('course_enrollment_id'));

        $viewRendered = $this->renderEnrollment($courseEnrollment, $enrollment);
        return $this->setDataResponse($viewRendered)->setResponse(true);
    }

    /**
     * @param $enrollment
     * @param $courseEnrollment
     * @return bool
     */
    public function renderEnrollment($courseEnrollment = null, $enrollment = null)
    {
        if (is_null($enrollment)) {
            if ($courseEnrollment && $courseEnrollment->enrollment) {
                $enrollment = $courseEnrollment->enrollment;
            }
        }
        if (!$courseEnrollment || !$enrollment) {
            return false;
        }
        $masterEnrollment = new MasterEnrollment();
        $data = ($courseEnrollment) ? $courseEnrollment->params_json : [];
        $responseEnrollment = $masterEnrollment->getEnrollment($enrollment, $data)->getForm();
        return $responseEnrollment;
    }

    /**
     * @throws \Throwable
     */
    protected function editCourseEnrollment()
    {
        return $this->createCourseEnrollment();
    }

    /**
     * @return $this
     */
    protected function destroyCourseEnrollment()
    {
        $course_enrollment_id = $this->request->get('course_enrollment_id');
        $courseEnrollment = Enrollment\CourseEnrollment::enable()->find($course_enrollment_id);

        if ($courseEnrollment && $courseEnrollment->delete()) {
            return $this->setResponse(true, trans("course::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveCourseEnrollment()
    {
        $response = $this->saveCourseEnrollmentService();
        $courseEnrollment = $response['course_enrollment'];
        if ($response['action']) {
            return $this->redirect(route('course.enrollments.edit', ['course_enrollment_id' => $courseEnrollment->id]))
                ->setDataResponse($courseEnrollment)->setResponse(true, trans("course::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, trans("course::messages.alert.mis_data"), isset($courseEnrollment->errors) ? $courseEnrollment->errors : null);
    }

    /**
     * @return $this|array
     */
    private function saveCourseEnrollmentService()
    {
        $courseEnrollment = new Enrollment\CourseEnrollment();
        $this->request = $this->requestValuesUpdate(['start_date' => 'Y-m-d H:i:s', 'end_date' => 'Y-m-d H:i:s']);
        if ($course_enrollment_id = $this->request->get("course_enrollment_id")) {
            $courseEnrollment = $courseEnrollment->enable()->find($course_enrollment_id);
            if (!$courseEnrollment) {
                return ['action' => false, 'course_enrollment' => $courseEnrollment, 'redirect' => route('course.enrollments.edit'
                    , ['course_enrollment_id' => $this->request->get('course_enrollment_id')])];
            }
        }

        $configs = $this->request->get("configs");
        $configs = json_encode($configs);
        $this->request->offsetSet("params", $configs);
        $this->validate($this->request, $courseEnrollment->rules);
        if (($courseEnrollment->fill($this->request->all()))) {
            $courseEnrollment->save();
            $courseEnrollment->updated = $course_enrollment_id ? true : false;
            $response = ['action' => true, 'course_enrollment' => $courseEnrollment];
        } else {
            $response = ['action' => false, 'course_enrollment' => $courseEnrollment];
        }

        return $response;
    }

    /**
     * @param $course_id
     * @param $course_package_id
     * @return array
     */
    public function savePackageEnrollment($course_id, $course_package_id)
    {
        $courseEnrollment = new Enrollment\CourseEnrollment();
        $coursePackage = Course::enable()->find($course_package_id);
        $course = Course::enable()->find($course_id);
        $enrollment = Enrollment::active()->where("alias", "package")->first();

        if(!$course || !$coursePackage || !$enrollment){
            return ['action' => false, 'message' => trans("course::messages.alert.mis_data")];
        }

        $config_p = [
            'course_package_id'=>$course_package_id,
        ];

        $configs = json_encode($config_p);

        $data = [
            'course_id' => $course_id,
            'enrollment_id' => $enrollment->id,
            'params' => $configs,
        ];

        if ($courseEnrollment->fill($data)) {
            $courseEnrollment->save();
            $response = ['action' => true, 'course_enrollment' => $courseEnrollment];
        } else {
            $response = ['action' => false, 'course_enrollment' => $courseEnrollment];
        }

        return $response;
    }

}
