<?php

namespace Modules\Course\ViewModels\Enrollment;


use Illuminate\Http\Request;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Libraries\Enrollment\Package\PackageEnrollment;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Enrollment;
use Modules\User\Models\User;


class PackageEnrollmentViewModel extends BaseViewModel
{

    use GridViewModel;

    private $enrollment_assets;
    public $packageEnrollment;

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $enrollment_assets = [];

        return $enrollment_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }


    /**
     * @return $this
     */
    protected function setAssetsEditCourseEnrollment()
    {
        $this->enrollment_assets = true;
        return $this;
    }

    /**
     * @return $this
     */
    protected function setAssetsCreateCourseEnrollment()
    {
        $this->enrollment_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createPackageEnrollment()
    {
        $viewEnrollment =& $this;
        $this->packageEnrollment = new PackageEnrollment();
        $course = Course::enable()->find($this->request->get("course_id"));
        if (!$course) {
            return $this->redirectBack()->setResponse(false, trans('course::messages.alert.not_find_course'));
        }

        $this->packageEnrollment->setCourseId($course->id)->setDataEnrollment($course, []);

        $this->setTitlePage(trans('course::enrollment.package.title'));
        return $this->renderedView("course::enrollment.form_package_enrollment", ['viewEnrollment' => $viewEnrollment], "form");
    }

    /**
     * @return $this
     */
    protected function savePackageEnrollment()
    {
        $response = $this->savePackageEnrollmentService();
        $coursePackageEnrollment = $response['course_package_enrollment'];
        if ($response['action']) {
            return $this->redirectBack()->setDataResponse($coursePackageEnrollment)->setResponse(true, trans("course::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, trans("course::messages.alert.mis_data"));
    }

    /**
     * @return $this|array
     */
    private function savePackageEnrollmentService()
    {

        $coursePackageEnroll = new Enrollment\CoursePackageEnrollment();

        $courses = $this->request->get("courses");
        $description = $this->request->get("description");
        $course_id = $this->request->get("course_id");
        $course = Course::enable()->find($course_id);
        $enrollment = Enrollment::active()->where("alias", "package")->first();

        $currentCoursePackage = $enrollment ? $coursePackageEnroll->enable()->where("course_id", $course_id)->get() : [];
        $currentCourseIds = $currentCoursePackage ? $currentCoursePackage->pluck("package_course_id") : [];

        $courses = collect($courses)->diff($currentCourseIds);
        $courses = Course::enable()->where('type', 'normal')->whereIn("id", $courses)->get()->pluck("id");

        if (!$enrollment || empty($courses)) {
            return ['action' => false, "course_package_enrollment" => null];
        }

        $params = json_encode(["description" => $description]);

        $insertItem = [];
        foreach ($courses as $course) {
            $insertItem[] = [
                "package_course_id" => $course,
                "course_id" => $course_id,
                "active" => 1,
                "params" => $params,
                "created_at" => date("Y-m-d H:i:s"), // five years
            ];
        }

        if ($coursePackageEnroll->insert($insertItem)) {
            $courseEnrollmentViewModel = new CourseEnrollmentViewModel();

            foreach ($insertItem as $item) {
                $courseEnrollmentViewModel->savePackageEnrollment($item['package_course_id'],$course_id);
            }

            $response = ['action' => true, 'course_package_enrollment' => $coursePackageEnroll];
        } else {
            $response = ['action' => false, 'course_package_enrollment' => $coursePackageEnroll];
        }

        return $response;
    }

    /**
     * @return $this
     */
    protected function destroyCoursePackageEnrollment()
    {
        $course_package_enrollment_id = $this->request->get('course_package_enrollment_id');
        $coursePackageEnroll = Enrollment\CoursePackageEnrollment::enable()->find($course_package_enrollment_id);

        if ($coursePackageEnroll && $coursePackageEnroll->delete()) {
            return $this->setResponse(true, trans("course::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function changeStatusEnrollment()
    {
        $course_package_enrollment_id = $this->request->get('course_package_enrollment_id');
        $coursePackageEnroll = Enrollment\CoursePackageEnrollment::enable()->find($course_package_enrollment_id);

        if ($coursePackageEnroll) {
            $coursePackageEnroll->active = $coursePackageEnroll->active == 1 ? 2 : 1;
            $coursePackageEnroll->save();
            return $this->setResponse(true, trans("course::messages.alert.update_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.update_un_success"));

    }

}
