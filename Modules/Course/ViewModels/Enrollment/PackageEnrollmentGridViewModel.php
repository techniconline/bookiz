<?php

namespace Modules\Course\ViewModels\Enrollment;


use Illuminate\Http\Request;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Models\Enrollment;


class PackageEnrollmentGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $course_id;
    public $packageEnrollment;


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $enrollment_assets = [];

        return $enrollment_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->course_id = $this->request->get("course_id");
        $this->Model = Enrollment\CoursePackageEnrollment::enable()->where("course_id", $this->course_id)->with("coursePackage.courseCategory");
    }

    /**
     * @return $this
     */
    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('course_package', trans('course::enrollment.package.course'), true);
        $this->addColumn('category', trans('course::course.category'), false);

        /*add action*/
        $add = array(
            'name' => 'course.enrollments.package.create',
            'parameter' => ["course_id" => ["value" => $this->course_id]]
        );
        $this->addButton('create_package_course', $add, trans('course::enrollment.package.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);


        /*add action*/
        $add = array(
            'name' => 'course.index',
            'parameter' => null
        );
        $this->addButton('list_course', $add, trans('course::course.list'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);

        $show = array(
            'name' => 'course.enrollments.package.changeStatus',
            'parameter' => ['id']
        );
        $this->addAction('changeStatus', $show, trans('course::enrollment.package.changeStatus'), 'fa fa-toggle-off', false
            , ['target' => '', 'class' => 'btn btn-sm btn-success _ajax_delete', 'data-message-confirm' => trans('course::enrollment.confirm')]);


        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('course::enrollment.package.confirm_delete')];
        $delete = array(
            'name' => 'course.enrollments.package.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('course::enrollment.package.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('title', 'text');
        return $this;
    }

    public function getRowsUpdate($row)
    {
        $row->course_package = $row->coursePackage->title;
        $row->category = isset($row->coursePackage->courseCategory->title)?$row->coursePackage->courseCategory->title:'-';
        return $row;
    }

    public function getActionsUpdate($row)
    {
        $actions = $this->actions;
        if ($row->active == 2) {
            $actions["changeStatus"]->icon = "fa fa-toggle-on";
        }else{
            $actions["changeStatus"]->icon = "fa fa-toggle-off";
        }
        return $actions;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function getViewListIndex()
    {
        $this->setTitlePage(trans('course::enrollment.package.list'));
        $this->generateGridList()->renderedView("course::enrollment.package.index", ['view_model' => $this], "course_package_list");
        return $this;
    }

}
