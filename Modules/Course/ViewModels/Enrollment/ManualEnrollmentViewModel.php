<?php

namespace Modules\Course\ViewModels\Enrollment;


use Illuminate\Http\Request;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Libraries\Enrollment\Manual\ManualEnrollment;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Enrollment;
use Modules\User\Models\User;


class ManualEnrollmentViewModel extends BaseViewModel
{

    use GridViewModel;

    private $enrollment_assets;
    public $manualEnrollment;


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $enrollment_assets = [];

        return $enrollment_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }


    /**
     * @return $this
     */
    protected function setAssetsEditCourseEnrollment()
    {
        $this->enrollment_assets = true;
        return $this;
    }

    /**
     * @return $this
     */
    protected function setAssetsCreateCourseEnrollment()
    {
        $this->enrollment_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createManualEnrollment()
    {

        $viewEnrollment =& $this;
        $this->manualEnrollment = new ManualEnrollment();
        $course = Course::enable()->find($this->request->get("course_id"));
        if(!$course){
            return $this->redirectBack()->setResponse(false, trans('course::messages.alert.not_find_course'));
        }
        $this->manualEnrollment->setCourseId($course->id)->setDataEnrollment($course, []);

        $this->setTitlePage(trans('course::enrollment.manual'));
        return $this->renderedView("course::enrollment.form_manual_enrollment", ['viewEnrollment' => $viewEnrollment], "form");
    }


    /**
     * @return $this
     */
    protected function saveManualEnrollment()
    {
        $response = $this->saveManualEnrollmentService();
        $courseUserEnrollment = $response['course_user_enrollment'];
        if ($response['action']) {
            return $this->redirectBack()->setDataResponse($courseUserEnrollment)->setResponse(true, trans("course::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, trans("course::messages.alert.mis_data"));
    }

    /**
     * @return $this|array
     */
    private function saveManualEnrollmentService()
    {

        $courseUserEnroll = new Enrollment\CourseUserEnrollment();

        $users = $this->request->get("users");
        $course_id = $this->request->get("course_id");
        $role_id = $this->request->get("role_id");
        $course = Course::enable()->find($course_id);
        $description = $this->request->get("description");
        $enrollment = Enrollment::active()->where("alias", "manual")->first();

        $currentUsers = $enrollment ? $courseUserEnroll->active()->where("course_id", $course_id)->where("enrollment_id", $enrollment->id)->get() : [];
        $currentUsersIds = $currentUsers ? $currentUsers->pluck("user_id") : [];

        $users = collect($users)->diff($currentUsersIds);
        $users = User::active()->whereIn("id", $users)->get()->pluck("id");

        if (!$enrollment || !$course || empty($users) || !$role_id) {
            return ['action' => false, "course_user_enrollment" => null];
        }

        $params = json_encode(["description" => $description]);

        $insertItem = [];
        foreach ($users as $user) {
            $insertItem[] = [
                "user_id" => $user,
                "enrollment_id" => $enrollment->id,
                "role_id" => $role_id,
                "course_id" => $course_id,
                "active" => 1,
                "params" => $params,
                "created_at" => date("Y-m-d H:i:s"),
                "active_date" => date("Y-m-d H:i:s"),
                "expire_date" => date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s") . ' + 1825 days')) // five years
            ];
        }

        if ($courseUserEnroll->insert($insertItem)) {
            $response = ['action' => true, 'course_user_enrollment' => $courseUserEnroll];
        } else {
            $response = ['action' => false, 'course_user_enrollment' => $courseUserEnroll];
        }

        return $response;
    }

    /**
     * @return $this
     */
    protected function destroyUserEnrollment()
    {
        $course_user_enrollment_id = $this->request->get('course_user_enrollment_id');
        $courseUserEnroll = Enrollment\CourseUserEnrollment::enable()->find($course_user_enrollment_id);

        if ($courseUserEnroll && $courseUserEnroll->delete()) {
            return $this->setResponse(true, trans("course::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function changeStatusEnrollment()
    {
        $course_user_enrollment_id = $this->request->get('course_user_enrollment_id');
        $courseUserEnroll = Enrollment\CourseUserEnrollment::enable()->find($course_user_enrollment_id);

        if ($courseUserEnroll) {
            $courseUserEnroll->active = $courseUserEnroll->active == 1 ? 2 : 1;
            $courseUserEnroll->save();
            return $this->setResponse(true, trans("course::messages.alert.update_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.update_un_success"));

    }

}
