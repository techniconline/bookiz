<?php

namespace Modules\Course\ViewModels\Enrollment;


use Illuminate\Http\Request;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Models\Enrollment;


class EnrollmentViewModel extends BaseViewModel
{

    use GridViewModel;

    private $enrollment_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = Enrollment::enable();
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $enrollment_assets = [];

        //use bested assets
        if ($this->enrollment_assets) {

        }

        if ($this->enrollment_assets) {
            $enrollment_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/course/enrollment.js"), 'name' => 'enrollment-backend'];
        }

        return $enrollment_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('name', trans('course::enrollment.name'), false);
        $this->addColumn('alias', trans('course::enrollment.alias'), false);

        /*add action*/
        $add = array(
            'name' => 'course.enrollment.create',
            'parameter' => null
        );
        $this->addButton('create_enrollment', $add, trans('course::enrollment.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);


        $show = array(
            'name' => 'course.enrollment.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('course::enrollment.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);


        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('course::enrollment.confirm_delete')];
        $delete = array(
            'name' => 'course.enrollment.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('course::enrollment.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('name', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        return $row;
    }


    /**
     * @return $this
     */
    protected function setAssetsEditEnrollment()
    {
        $this->enrollment_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('course::enrollment.list'));
        $this->generateGridList()->renderedView("course::enrollment.index", ['view_model' => $this], "enrollment_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createEnrollment()
    {
        $enrollment = new Enrollment();

        $enrollment = $enrollment->enable()->find($this->request->get('enrollment_id'));

        $this->setTitlePage(trans('course::enrollment.' . ($enrollment ? 'edit' : 'add')));
        return $this->renderedView("course::enrollment.form_enrollment", ['enrollment' => $enrollment], "form");
    }

    /**
     * @return EnrollmentViewModel
     * @throws \Throwable
     */
    protected function editEnrollment()
    {
        return $this->createEnrollment();
    }

    /**
     * @return $this
     */
    protected function destroyEnrollment()
    {
        $enrollment_id = $this->request->get('enrollment_id');
        $enrollment = Enrollment::enable()->find($enrollment_id);

        if ($enrollment && $enrollment->delete()) {
            return $this->setResponse(true, trans("course::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveEnrollment()
    {
        $response = $this->saveEnrollmentService();
        $enrollment = $response['enrollment'];
        if ($response['action']) {
            return $this->redirect(route('course.enrollment.edit', ['enrollment_id' => $enrollment->id]))->setDataResponse($enrollment)->setResponse(true, trans("course::messages.alert.save_success"));
        }
        return $this->redirect(isset($response['redirect']) ? $response['redirect'] : route('course.enrollment.create'))->setResponse(false, trans("course::messages.alert.mis_data"), isset($enrollment->errors) ? $enrollment->errors : null);
    }

    /**
     * @return $this|array
     */
    private function saveEnrollmentService()
    {
        $enrollment = new Enrollment();
        $arrDate = [];
        if ($this->request->get("start_date")){
            $arrDate["start_date"] = 'Y-m-d H:i:s';
        }

        if ($this->request->get("end_date")){
            $arrDate["end_date"] = 'Y-m-d H:i:s';
        }

        $this->request = $this->requestValuesUpdate($arrDate);
        if ($enrollment_id = $this->request->get("enrollment_id")) {
            $enrollment = $enrollment->enable()->find($enrollment_id);
            if (!$enrollment) {
                return ['action' => false, 'enrollment' => $enrollment, 'redirect' => route('course.enrollment.edit', ['enrollment_id' => $this->request->get('enrollment_id')])];
            }
        }

        if(!$this->request->get("concurrent")){
            $this->request->offsetSet("concurrent", 0);
        }

        $this->validate($this->request, $enrollment->rules);
        if (($enrollment->fill($this->request->all()))) {

            $enrollment->save();
            $enrollment->updated = $enrollment_id ? true : false;
            $response = ['action' => true, 'enrollment' => $enrollment];
        } else {
            $response = ['action' => false, 'enrollment' => $enrollment];
        }

        return $response;
    }

    /**
     * @return $this
     */
    public function getEnrollmentByApi()
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }
        $model = Enrollment::select('id', 'name');
        if ($q) {
            $model->where(function ($query) use ($q) {

                $query->where('name', 'LIKE', '%' . $q . '%');

            })->active();
            $items = $model->get();
        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $items = collect([]);
        }
        $items = $items->map(function ($item) {
            $item->text = $item->name;
            return $item;
        });
        return $this->setDataResponse($items)->setResponse(true);
    }


}
