<?php

namespace Modules\Course\ViewModels\Enrollment;


use Illuminate\Http\Request;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Course\Libraries\Enrollment\Manual\ManualEnrollment;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Enrollment;
use Modules\Course\Models\Enrollment\CourseUserEnrollment;
use Modules\User\Models\User;


class ListEnrollmentViewModel extends BaseViewModel
{

    use GridViewModel;

    private $course_id;
    public $manualEnrollment;


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $enrollment_assets = [];

        return $enrollment_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->course_id = $this->request->get("course_id");
        $this->Model = CourseUserEnrollment::enable()->where("course_id", $this->course_id)->with("user", "enrollment", "role");
    }

    /**
     * @return $this
     */
    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('user', trans('course::enrollment.manuals.user'), true);
        $this->addColumn('enrollment', trans('course::enrollment.name'), true);
        $this->addColumn('role', trans('course::enrollment.role'), true);
        $this->addColumn('active', trans('course::enrollment.status'), true);
        $this->addColumn('active_date', trans('course::enrollment.manuals.active_date'), false);
        $this->addColumn('expire_date', trans('course::enrollment.manuals.expire_date'), false);

        /*add action*/
        $add = array(
            'name' => 'course.enrollments.manual.create',
            'parameter' => ["course_id" => ["value" => $this->course_id]]
        );
        $this->addButton('create_manual_course', $add, trans('course::enrollment.manuals.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);


        /*add action*/
        $add = array(
            'name' => 'course.index',
            'parameter' => null
        );
        $this->addButton('list_course', $add, trans('course::course.list'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);

        $show = array(
            'name' => 'course.enrollments.manual.changeStatus',
            'parameter' => ['id']
        );
        $this->addAction('changeStatus', $show, trans('course::enrollment.manuals.changeStatus'), 'fa fa-toggle-off', false
            , ['target' => '', 'class' => 'btn btn-sm btn-success _ajax_delete', 'data-message-confirm' => trans('course::enrollment.confirm')]);


        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('course::enrollment.manuals.confirm_delete')];
        $delete = array(
            'name' => 'course.enrollments.manual.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('course::enrollment.manuals.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('last_name', 'text', ['relation'=>'user']);
        $this->addFilter('first_name', 'text', ['relation'=>'user']);
        $this->addFilter('username', 'text', ['relation'=>'user']);
        $this->addFilter('active', 'select', ['options'=>trans('core::data.active'),'title'=>trans('course::enrollment.status')]);

        return $this;
    }

    public function getRowsUpdate($row)
    {
        $row->user = $row->user->full_name;
        $row->active = trans('core::data.active.'.$row->active);
        $row->enrollment = $row->enrollment?$row->enrollment->name:'-';
        $row->role = $row->role?$row->role->title:'-';
        return $row;
    }

    public function getActionsUpdate($row)
    {
        $actions = $this->actions;
        if ($row->active == 2) {
            $actions["changeStatus"]->icon = "fa fa-toggle-on";
        }else{
            $actions["changeStatus"]->icon = "fa fa-toggle-off";
        }
        return $actions;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function getViewListIndex()
    {
        $this->setTitlePage(trans('course::enrollment.list'));
        $this->generateGridList()->renderedView("course::enrollment.index", ['view_model' => $this], "enrollment_list");
        return $this;
    }

}
