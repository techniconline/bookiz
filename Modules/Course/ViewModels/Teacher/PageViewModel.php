<?php

namespace Modules\Course\ViewModels\Teacher;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Course\Models\Course\CourseTeacher;
use Modules\Course\Models\Teacher\Teacher;
use Modules\Course\Traits\Page\EnrollmentViewModelTrait;
use Modules\Course\Traits\Page\InfoViewModelTrait;


class PageViewModel extends BaseViewModel
{
    use InfoViewModelTrait;
    use EnrollmentViewModelTrait;

    private $teacher;


    /**
     * @return $this|void
     * @throws \Throwable
     */
    public function viewTeacherDetails()
    {
        $blockViewModel =& $this;
        $this->teacher = Teacher::active()->with(["user","teacherCourses.course"])
            ->whereHas("teacherCourses.course", function ($q){
                $q->active();
            })
            ->find($this->request->get('teacher_id'));

        if (!$this->teacher) {
            return abort(404, 'Page Not Find');
        }

        if (app("getInstanceObject")->isApi()) {
            $this->teacher = $this->decorateAttributes($this->teacher);
            return $this->setDataResponse($this->teacher)->setResponse(true);
        }
        $this->setTitlePage($this->teacher->user->full_name);
        return $this->renderedView('course::teacher.page', ['blockViewModel'=>$blockViewModel]);
    }

    /**
     * @return mixed
     */
    public function getCourses()
    {
        return CourseTeacher::active()->where('teacher_id', $this->teacher->id)->with('course')->whereHas('course', function ($q){
            $q->filterCurrentInstance()->active();
        })->get();
    }

    /**
     * @return mixed
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

}
