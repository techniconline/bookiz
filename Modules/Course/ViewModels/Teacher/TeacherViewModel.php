<?php

namespace Modules\Course\ViewModels\Teacher;


use Illuminate\Http\Request;
use Modules\Course\Models\Teacher\Teacher;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\User\Models\User;


class TeacherViewModel extends BaseViewModel
{

    use GridViewModel;

    private $teacher_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = Teacher::active()->filterCurrentInstance()->with('user')->whereHas("user",function ($q){
            return $q->active();
        });
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $teacher_assets = [];

        //use bested assets
        if ($this->teacher_assets) {
            $teacher_assets [] = ['container' => 'up_theme_js_2', 'src' => ("assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"), 'name' => 'ui.widget'];

            $teacher_assets [] = ['container' => 'plugin_general_2', 'src' => ("jquery-ui/jquery-ui.min.js"), 'name' => 'jquery-ui'];
        }

        if ($this->teacher_assets) {
            $teacher_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/course/teacher.js"), 'name' => 'course-backend'];
        }

        return $teacher_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('name', trans('course::teacher.name'), true);
        $this->addColumn('education', trans('course::teacher.education'), false);
        $this->addColumn('teaching_time', trans('course::teacher.teaching_time'), false);

        /*add action*/
        $add = array(
            'name' => 'course.teacher.create',
            'parameter' => null
        );
        $this->addButton('create_teacher', $add, trans('course::teacher.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);


        $show = array(
            'name' => 'course.teacher.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('course::teacher.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('course::teacher.confirm_delete')];
        $delete = array(
            'name' => 'course.teacher.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('course::teacher.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('name', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->name = $row->user->full_name;
        $row->education = trans('course::teacher.educations.' . $row->education_key);
        return $row;
    }


    /**
     * @return $this
     */
    protected function setAssetsEditTeacher()
    {
        $this->teacher_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('course::teacher.list'));
        $this->generateGridList()->renderedView("course::teacher.index", ['view_model' => $this], "teacher_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createTeacher()
    {

        $teacher = new Teacher();
        //$users = User::active()->filterCurrentInstance()->get();
        $teacher = $teacher->active()->with([
            'user'
        ])->whereHas("user",function ($q){
            return $q->where('id','!=',0);
        })->find($this->request->get('teacher_id'));

        $this->setTitlePage(trans('course::teacher.' . ($teacher ? 'edit' : 'add')));
        return $this->renderedView("course::teacher.form_teacher", ['teacher' => $teacher], "form");
    }

    /**
     * @return TeacherViewModel
     * @throws \Throwable
     */
    protected function editTeacher()
    {
        return $this->createTeacher();
    }

    /**
     * @return $this
     */
    protected function destroyTeacher()
    {
        $teacher_id = $this->request->get('teacher_id');
        $teacher = Teacher::enable()->find($teacher_id);

        if ($teacher && $teacher->delete()) {
            return $this->setResponse(true, trans("course::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("course::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveTeacher()
    {
        $response = $this->saveTeacherService();
        $teacher = $response['teacher'];
        if ($response['action']) {
            return $this->redirect(route('course.teacher.edit', ['teacher_id' => $teacher->id]))->setDataResponse($teacher)->setResponse(true, trans("course::messages.alert.save_success"));
        }
        return $this->redirect(isset($response['redirect']) ? $response['redirect'] : route('course.teacher.create'))->setResponse(false, trans("course::messages.alert.mis_data"), isset($teacher->errors) ? $teacher->errors : null);
    }

    /**
     * @return $this|array
     */
    private function saveTeacherService()
    {
        $teacher = new Teacher();
        if ($teacher_id = $this->request->get("teacher_id")) {
            $teacher = $teacher->enable()->find($teacher_id);
            if (!$teacher) {
                return ['action' => false, 'teacher' => $teacher, 'redirect' => route('course.teacher.edit', ['teacher_id' => $this->request->get('teacher_id')])];
            }

        }

        $education = $teacher->searchEnumItem($this->request->get('education'), $teacher->educations, 'value');
        $this->request->offsetSet('education', $education);
        $this->request->offsetSet('instance_id', app('getInstanceObject')->getCurrentInstanceId());

        if (($teacher->fill($this->request->all())->isValid()) || ($teacher_id && $teacher)) {
            $teacher->save();
            $teacher->updated = $teacher_id ? true : false;
            $response = ['action' => true, 'teacher' => $teacher];
        } else {
            $response = ['action' => false, 'teacher' => $teacher];
        }

        return $response;
    }


    /**
     * @return $this
     */
    public function getUserByApi()
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }
        $model = User::select('id', 'username', 'first_name', 'last_name')->filterCurrentInstance();
        if ($q) {
            $model->where(function ($query) use ($q) {

                $query->orWhere('username', 'LIKE', '%' . $q . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $q . '%');
                $query->orWhere('first_name', 'LIKE', '%' . $q . '%');

            })
//                ->filterCurrentInstance()
                ->active();

            $items = $model->get();
        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $items = collect([]);
        }
        $items = $items->map(function ($item) {
            $item->text = $item->full_name;
            return $item;
        });
        return $this->setDataResponse($items)->setResponse(true);
    }

    /**
     * @return $this
     */
    public function getTeacherByApi()
    {
        $q = false;
        $selected = false;
        $type = $this->request->get('_type');
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }
        $model = new Teacher();
        $model = $model->setRequestItems($this->request->all())->active()->with('user');
        if ($q) {
            $items = $model->filterUser()->get();
        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->filterUser()->get();

        } else {
            $items = $type=='query'?$model->filterUser()->take(20)->orderBy('id','DESC')->get():collect([]);

        }
        $items = $items->map(function ($item) {
            $item->text = $item->user->full_name;
            return $item;
        });
        return $this->setDataResponse($items)->setResponse(true);
    }


}
