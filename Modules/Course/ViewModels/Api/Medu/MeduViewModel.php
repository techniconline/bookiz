<?php

namespace Modules\Course\ViewModels\Api\Medu;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use BridgeHelper;
use Modules\Course\ViewModels\Api\EnrollViewModel;
use Modules\Course\Models\Enrollment\CourseEnrollment;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Order\OrderItems;
use Modules\Sale\Models\Traits\Payment\OrderStatesTrait;
use Modules\Sale\Models\Traits\Payment\PaymentStatesTrait;
use Modules\Sale\Models\Traits\Payment\PaymentTrait;

class MeduViewModel extends EnrollViewModel
{
    use PaymentStatesTrait,OrderStatesTrait,PaymentTrait;

    public function enrollUser($request,$info,$user){
        $this->request=$request;
        if($user->id) {
            $rules = [
                'first_name' => ['required', 'string', 'max:50'],
                'last_name' => ['required', 'string', 'max:50'],
                'national_code' => 'required|digits:10',
                'enrollment_id' => ['required'],
            ];
        }else{
            $rules = [
                'first_name' => ['required', 'string', 'max:50'],
                'last_name' => ['required', 'string', 'max:50'],
                'national_code' => 'required|digits:10',
                'enrollment_id' => ['required'],
            ];
        }
        $this->requestValuesUpdate();
        $this->request->validate($rules);
        try{
            if(!$user->id){
                $info->first_name=$request->get('first_name');
                $info->last_name=$request->get('last_name');
                $info->national_code=$request->get('national_code');
                $info->password=$info->medu;
                $user=$this->getUser($info);
                $user = Auth::loginUsingId($user->id);
            }else{
                $user->first_name=$request->get('first_name');
                $user->last_name=$request->get('last_name');
                $user->national_code=$request->get('national_code');
                $user->office_code=$info->office_code;
                $user->save();
            }
            if(!$user || $user->id==0){
                return $this->getErrorView(trans("course::messages.alert.connector_error"));
            }
            if($info->enroll->enroll=='api'){
                $userEnroll=$this->userEnroll($info,$user);
            }else{
                $courseEnrollment=CourseEnrollment::with('enrollment')->where('enrollment_id',$request->get('enrollment_id'))->where('course_id',$info->course->id)->active()->first();
                if(!$courseEnrollment){
                    return $this->getErrorView(trans("course::messages.alert.enroll_error"));
                }
                if($courseEnrollment->enrollment->alias=='free'){
                    $userEnroll=$this->userEnroll($info,$user,$courseEnrollment->enrollment);
                }elseif($courseEnrollment->enrollment->alias=='payment'){
                    $order=$this->makeOrder($request,$user,$info, $courseEnrollment);
                    if($order){
                        if($order->payable_amount>0){
                            $redirect=route('sale.order.payment',['id'=>$order->id, "reference_code"=>$order->reference_code, "app" => app('getInstanceObject')->isApi() ? "mobile" : "web"]);
                        }else{
                            $redirect=route('sale.order.complete',['id'=>$order->id, "reference_code"=>$order->reference_code, "app" => app('getInstanceObject')->isApi() ? "mobile" : "web"]);
                        }
                        return $this->redirect($redirect)->setDataResponse($order)->setResponse(true, trans("sale::message.save_order"));
                    }
                }else{
                    return $this->getErrorView(trans("course::messages.alert.enroll_error"));
                }
            }
            if($userEnroll){
                $url=route('course.view',['id'=>$userEnroll->course_id]);
                return $this->redirect($url);
            }else{
                return $this->getErrorView(trans("course::messages.alert.enroll_error"));
            }
        }catch (\Exception $e){
            report($e);
            return $this->getErrorView(trans("course::messages.alert.error_payment_order") .' - '. $e->getCode());
        }
    }


    public function makeOrder($request,$user,$info, $courseEnrollment)
    {
        DB::beginTransaction();
        try{
            $currentCurrency=BridgeHelper::getCurrencyHelper()->getCurrentCurrency();
            $itemParams=$courseEnrollment->params_json;
            $cost = BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($itemParams->sale_cost, $itemParams->currency_id, false);
            $reference_code = "INS";
            $reference_code .= $info->instance->id * 3;
            $reference_code .= "-" . strtoupper(str_random(10));
            $order = new Order;
            $order->instance_id = $info->instance->id;
            $order->reference_code = $reference_code;
            $order->user_id = $user->id;
            $order->state = $this->getOrderNewState();
            $order->amount = $cost;
            $order->payable_amount = $cost;
            $order->currency_id = $currentCurrency->id;
            $order->save();
            $item = new OrderItems;
            $item->order_id = $order->id;
            $item->item_type = 'course';
            $item->item_id = $info->course->id;
            $item->name = $info->course->title;
            $item->options = ["enrollment_id" => $courseEnrollment->enrollment_id, "product_key" => $info->course->code];
            $item->qty = 1;
            $item->amount = $itemParams->sale_cost;
            $item->total_amount = $itemParams->sale_cost;
            $item->currency_id = $itemParams->currency_id;
            $item->save();
            if($order->payable_amount>0) {
                $bankid=rand(0,4);
                $bank=['asan','asan','zarinpal'];
//                $bank=['zarinpal'];
                $options = ($this->request->has('option'))?$this->request->get('option'):["payment" => $bank[$bankid], "reference_code" => $order->reference_code];
                $type = 'online';
                $this->createOrderPayment($order, $options, $type);
            }
            DB::commit();
            return $order;
        }catch (\Exception $e){
            DB::rollBack();
            report($e);
            return false;
        }
    }


}
