<?php

namespace Modules\Course\ViewModels\Api;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use DateHelper;
use Illuminate\Support\Facades\Notification;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Course\Models\Enrollment;
use Modules\Course\Models\Enrollment\CourseUserEnrollment;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\User\Models\User;
use Modules\Exam\Models\ExamAttempt;
use Modules\Course\Notifications\AttemptCount;


class EnrollViewModel extends BaseViewModel
{

    private $apiLib=false;
    private $attemptCount=0;

    public function enroll(){
        $api=$this->request->get('api');
        if($api && $this->setApiLib($api) && $this->apiLib && $this->apiLib->isValidRequest($this->request)){
//            $result=$this->request->get('result');
//            $show=true;
//            $error=null;
//            if($result){
//                $result=$this->persianNumberReplace($result);
//                $savedResult=session('qResult');
//                if($savedResult==$result && $this->canLogin()){
//                    $show=false;
//                }else{
//                    $error='پاسخ شما اشتباه بود. لطفا دقت نمایید.';
//                }
//                //session()->forget('qResult');
//            }
//            if($show){
//                $hasForm=$this->canLogin();
//                $q='';
//                if($hasForm){
//                    $q=$this->makeQ();
//                }else{
//                    $error='کاربر گرامی هم اینک '.$this->attemptCount.' نفر از کاربران در حال آزمون هستند. لطفا در ساعت های کم بار تر مراجعه فرمایید.';
//                }
//                $this->setLayout('None');
//                return $this->renderedView('course::api.medu.capcha',['q'=>$q,'hasForm'=>$hasForm,'error'=>$error]);
//            }
            if($this->apiLib->byEnroll()){
                $info=$this->apiLib->getUserInfo($this->request,[]);
                if(!$info){
                    return $this->getErrorView($this->apiLib->getError());
                }
                return $this->enrollUserByOneStep($info);
            }else{
                $result=$this->apiLib->enrollUserBySteps($this->request,[]);
                if($result){
                    return $result;
                }
                return $this->getErrorView(trans('course::messages.api.ltms_not_access'));
            }
        }else{
            return $this->getErrorView(trans('course::messages.api.not_find_page'));
        }
    }

    public function score(){
        $api=$this->request->get('api');
        $result=['action'=>false,'data'=>null,'message'=>'null'];
        if($api && $this->setApiLib($api) && $this->apiLib && $this->apiLib->isValidRequest($this->request)) {
            $info=$this->apiLib->getUserInfo($this->request,[]);
            if(!$info){
                $result=['action'=>false,'data'=>null,'message'=>$this->apiLib->getError()];
            }else{
                $user=$this->getUser($info);
                $data=$this->apiLib->getUserScore($info->course,$user);
                if($data!=false){
                    $result=['action'=>true,'data'=>$data];
                }
            }
        }
        return response()->json($result,200);
    }

    public function enrollUserByOneStep($info){
        try{
            $user=$this->getUser($info);
            if(!$user){
                return $this->getErrorView(trans("course::messages.alert.user_error"));
            }
            $user = Auth::loginUsingId($user->id);
            $result=$this->userEnroll($info,$user);
            if($result && isset($result->course_id)){
                if($result->course_id==300){
                    $url=url('/course/view/300/activity/4859');
                }else{
                    $url=route('course.view',['id'=>$result->course_id]);
                }

                return $this->redirect($url);
            }else{
                return $this->getErrorView(trans("course::messages.alert.enroll_error"));
            }
        }catch (\Exception $e){
            report($e);
        }
        return $this->getErrorView(trans("course::messages.alert.action_error"));
    }

    public function getErrorView($error){
        return $this->renderedView('course::api.error',['error'=>$this->getComponent('error',['error'=>$error])]);
    }

    protected function getUser($info){
        $userConnector=UserRegisterKey::with('user')->where('connector_id',$info->connector->id)->where('value',$info->{$info->connector->code})->first();
        if($userConnector){
            return $userConnector->user;
        }
        $value=$info->{$info->connector->code};
        DB::beginTransaction();
        try{
            /*add user*/
            $instance_id=$info->instance->id;
            $user = new User;
            $user->instance_id=$instance_id;
            $user->username=$this->getUsername($info->connector,$value);
            $user->password=bcrypt($info->password);
            $user->first_name=$info->first_name;
            $user->last_name=$info->last_name;
            $user->confirm=$info->confirm;
            if(isset($info->national_code)){
                $user->national_code=$info->national_code;
            }
            if(isset($info->office_code)){
                $user->office_code=$info->office_code;
            }
            $user->save();

            $user->addDefaultMeta();

            /*add connector*/
            $userConnector=new UserRegisterKey;
            $userConnector->user_id=$user->id;
            $userConnector->connector_id=$info->connector->id;
            $userConnector->value=$value;
            if($info->confirm){
                $userConnector->status='active';
                $userConnector->token=null;
                $userConnector->hash=null;
            }else{
                $userConnector->status='deactive';
                $userConnector->token=random_int(1000,100000);
                $userConnector->hash=md5($info->connector->id.$user->id);
            }
            $userConnector->save();
            /*add default Role*/

            DB::commit();
        }catch (Exception $e){
            DB::rollBack();
            return false;
        }
        return $user;
    }

    protected function userEnroll($info,$user,$enrollmentModel=null, $course_enrollment_id = null){
        $isEnrol=CourseUserEnrollment::where('user_id',$user->id)->where('course_id',$info->course->id)->first();
        if($isEnrol){
            return $isEnrol;
        }
        if(is_null($enrollmentModel)){
            $enrollmentModel = Enrollment::where('alias','api')->first();
        }
        if($enrollmentModel){
            $time = time() + 25720000;
            $expireDate = DateHelper::setDateTime(date(trans('core::date.database.datetime'), $time), null, trans('core::date.database.datetime'))->getDateTime(trans('core::date.database.datetime'));
            $userEnroll = new CourseUserEnrollment;
            $userEnroll->user_id = $user->id;
            $userEnroll->course_id = $info->course->id;
            $userEnroll->enrollment_id = $enrollmentModel->id;
            $userEnroll->course_enrollment_id = $course_enrollment_id;
            $userEnroll->role_id = $info->role->id;
            $userEnroll->type = 'enrollment';
            $userEnroll->active_date = date(trans('core::date.database.datetime'), time());
            $userEnroll->expire_date = $expireDate;
            $userEnroll->active = 1;
            $userEnroll->save();
            return $userEnroll;
        }
        return false;
    }

    public function getUsername($connector,$value){
        return $connector->code.'_'.$value.'_'.rand(1000,100000);
    }



    public function setApiLib($name){
        if (empty($name)) {
            return false;
        }
        try {
            $enrollmentClass = "\\Modules\\Course\\Libraries\\Api";
            $type = ucfirst($name);
            $enrollmentClass .= "\\" . $type . "\\" . $type . "Enrollment";
            $this->apiLib = new $enrollmentClass();
            return true;
        } catch (\Exception $exception) {
            abort(404);
            return false;
        }
    }

    public function canLogin(){
        $key='open_attempt_sms_send';
        $send=Cache::get($key);
        if(!$send){
            $send=['send'=>2000];
            Cache::forever($key,$send);
        }
        $attempts=$this->getOpenAttemptCount();
        //$attempts=3050;
        $attempts=$attempts+1000;
        $this->attemptCount=$attempts;
        if($attempts > 1000){
            try{
                if(($send['send']+1000) < $attempts){
                    $send['send']=$attempts;
                    Cache::forever($key,$send);
                    Notification::sendNow($attempts, new AttemptCount($attempts-1000));
                }
            }catch (\Exception $e){
            }
        }
        $max_attempt=3500;
        if($attempts > $max_attempt){
            return false;
        }
        return true;
    }

    public function getOpenAttemptCount(){
        $key='open_attempt_count';
        if(Cache::has($key)){
            return Cache::get($key);
        }
        $date=date('Y-m-d H:i:s');
        $result=ExamAttempt::where('end_time','>=',$date)->whereIn('status',['new','open'])->count();
        Cache::put($key,$result,3);
        return $result;
    }


    public function makeQ(){
        $rand=rand(0,1);
        if($rand){
            $num1=rand(10,rand(15,40));
            $num2=rand(1,9);
            $data=['qResult'=>($num1-$num2)];
            session($data);
            $q='اگر از عدد '.$num1.' عدد '.$num2.' را کم کنیم نتیجه چقدر می شود؟ ';
        }else{
            $num1=rand(1,rand(10,40));
            $num2=rand(1,10);
            $data=['qResult'=>($num1+$num2)];
            session($data);
            $q='نتیجه جمع دو عدد '.$num1.' و '.$num2.' چقدر می شود؟ ';
        }
        return $q;
    }


    /**
     * @param $value
     * @return mixed
     */
    private function persianNumberReplace($value)
    {
        $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
        $english = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        return str_replace($persian, $english, $value);
    }

}
