<?php

namespace Modules\Course\Models\Teacher;


use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Teacher\Teacher as TraitModel;

class Teacher extends BaseModel
{
    use TraitModel;
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'user_id', 'education', 'field_study', 'location_study', 'teaching_time', 'short_description', 'description', 'active', 'deleted_at', 'created_at', 'updated_at'];
    public $messages = [];
    public $rules = [
        'user_id' => 'required',
        'education' => 'required',
    ];

    public $api_fields = ['id', 'education', 'field_study', 'location_study', 'teaching_time', 'short_description', 'description'];
    public $list_fields = ['id', 'education', 'field_study', 'location_study', 'teaching_time', 'short_description', 'description'];
    public $api_append_fields = ["education_key", "education_certificate"];
    public $list_append_fields = ["education_key", "education_certificate"];

    public $educations = [1 => 'diploma', 2 => 'bachelor', 3 => 'master', 4 => 'doctor'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $appends = ["education_key"];
}
