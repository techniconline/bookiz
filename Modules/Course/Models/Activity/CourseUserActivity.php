<?php

namespace Modules\Course\Models\Activity;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;

class CourseUserActivity extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'course_id', 'activity_id', 'active_date', 'expire_date', 'params', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;

    public $messages = [];
    public $rules = [
        'user_id' => 'required',
        'course_id' => 'required',
        'activity_id' => 'required',
    ];

    public $api_fields = ['id', 'user_id', 'course_id', 'activity_id', 'active_date', 'expire_date', 'params'];
    public $list_fields = ['id', 'user_id', 'course_id', 'activity_id'];
    public $api_append_fields = ['active_date_by_mini_format','expire_date_by_mini_format'];
    public $list_append_fields = ['active_date_by_mini_format','expire_date_by_mini_format'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];


    /**
     * @param $format
     * @return $this
     */
    public function setFormatOutputDate($format)
    {
        $this->format_date = $format;
        return $this;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getActiveDateByMiniFormatAttribute()
    {
        $value = $this->active_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.medium'));
        }
        return $value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getExpireDateByMiniFormatAttribute()
    {
        $value = $this->expire_date;
        if ($value) {
            $value = DateHelper::setDateTime($value)->getLocaleFormat(trans('core::date.datetime.medium'));
        }
        return $value;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('Modules\Course\Models\Course\Course');
    }
}
