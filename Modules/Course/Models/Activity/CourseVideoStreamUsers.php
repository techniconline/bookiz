<?php

namespace Modules\Course\Models\Activity;

use Modules\Core\Models\BaseModel;
use Modules\User\Models\User;

class CourseVideoStreamUsers extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'out_user_id', 'user_name', 'password', 'params', 'active', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @param $value
     * @return mixed
     */
    public function getParamsAttribute($value)
    {
        if($value){
            return json_decode($value);
        }
        return $value;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
