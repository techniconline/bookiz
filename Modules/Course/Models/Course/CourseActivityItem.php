<?php

namespace Modules\Course\Models\Course;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Course\CourseActivityItem as TraitModel;

class CourseActivityItem extends BaseModel
{
    use TraitModel;
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['ca_id', 'type', 'item_id', 'deleted_at', 'created_at', 'updated_at'];

    public $messages = [];
    public $rules = [
        'type' => 'required',
        'item_id' => 'required',
        'ca_id' => 'required',
    ];

    public $types = ['video' => 'video', 'file' => 'file'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
