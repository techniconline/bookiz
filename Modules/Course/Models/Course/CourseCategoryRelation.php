<?php

namespace Modules\Course\Models\Course;

use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Course\CourseCategoryRelation as TraitModel;

class CourseCategoryRelation extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['course_id', 'category_id'];

    public $timestamps = false;
    protected $primaryKey = ['course_id', 'category_id'];
    public $incrementing = false;
}
