<?php

namespace Modules\Course\Models\Course;


use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Course\CourseCategory as TraitModel;

class CourseCategory extends BaseModel
{

    use TraitModel;
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['parent_id', 'instance_id', 'language_id', 'root_id', 'title', 'short_description', 'alias', 'key_trans', 'attributes', 'sort', 'active', 'deleted_at', 'created_at', 'updated_at'];
    public $api_fields = ['id', 'parent_id', 'root_id', 'title', 'short_description', 'alias', 'key_trans', 'attributes', 'sort'];
    public $list_fields = ['id', 'parent_id', 'root_id', 'title', 'short_description', 'alias', 'key_trans', 'attributes', 'sort'];

    public $api_append_fields = ['url_get_childes', 'url_courses', 'courses_count'];
    public $list_append_fields = ['url_get_childes', 'url_courses', 'courses_count', 'childes_count'];

    public $messages = [];
    public $rules = [
        'title' => 'required',
        'instance_id' => 'required',
        'language_id' => 'required',
//        'root_id' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $appends = ['url_get_childes'];
}
