<?php

namespace Modules\Course\Models\Course;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Course\Course as TraitModel;
use Modules\Course\Traits\Page\EnrollmentViewModelTrait;
use Modules\Course\Traits\Page\InfoViewModelTrait;

class Course extends BaseModel
{
    use TraitModel;
    use SoftDeletes;
    use EnrollmentViewModelTrait;
    use InfoViewModelTrait;

    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'language_id', 'category_id', 'title', 'code', 'short_description', 'description'
        , 'type', 'image', 'quantity', 'active_date', 'expire_date', 'params', 'view_count', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $api_fields = ['id', 'title', 'code', 'short_description', 'description', 'type', 'image'];
    public $list_fields = ['id', 'title', 'code', 'short_description', 'type', 'image'];
    public $api_append_fields = ['url_single_course', 'rate', 'enrollments', 'info_course', 'teachers', 'is_enroll', 'params_json'];
    public $list_append_fields = ['url_single_course', 'rate', 'enrollments', 'info_course', 'teachers', 'course_sections_count'];

    const SYSTEM_NAME = 'course';
    public $messages = [];
    public $rules = [
        'title' => 'required',
        'language_id' => 'required',
        'instance_id' => 'required',
        'category_id' => 'required',
        'active' => 'required',
        'type' => 'required',
    ];

    public $types = ["package", "normal"];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $appends = ['active_text'];

}
