<?php

namespace Modules\Course\Models\Course;


use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Course\CourseActivity as TraitModel;

class CourseActivity extends BaseModel
{
    use TraitModel;
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['title', 'description', 'course_id', 'course_section_id', 'type', 'active_date', 'expire_date', 'is_public', 'show_type', 'single_sales', 'single_amount', 'single_currency', 'duration', 'params', 'sort', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $api_fields = ['id', 'title', 'description', 'course_id', 'course_section_id', 'type', 'active_date', 'expire_date'
        , 'is_public', 'show_type', 'single_sales', 'single_amount', 'single_currency', 'duration'];

    public $list_fields = ['id', 'title', 'description', 'course_id', 'course_section_id', 'type', 'active_date', 'expire_date'
        , 'is_public', 'show_type', 'single_sales', 'single_amount', 'single_currency', 'duration', 'params'];

    public $api_append_fields = ["url_activity","url_page_activity", "has_access", "single_amount_text",'parameters'];
    public $list_append_fields = [];


    public $messages = [];
    public $rules = [
        'title' => 'required',
        'type' => 'required',
        'course_id' => 'required',
        'course_section_id' => 'required',
        'show_type' => 'required',
    ];

    public $show_types = ['free' => 'free', 'pay' => 'pay'];
    public $types = ['video' => 'video', 'file' => 'file', 'exam' => 'exam', 'video_stream' => 'video_stream'];

    protected $appends = ["is_public_text"];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
