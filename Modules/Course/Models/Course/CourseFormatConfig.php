<?php

namespace Modules\Course\Models\Course;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Course\CourseFormatConfig as TraitModel;

class CourseFormatConfig extends BaseModel
{
    use TraitModel;
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['course_id', 'title', 'configs', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $messages = [];
    public $rules = [
        'title' => 'required',
        'course_id' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
