<?php

namespace Modules\Course\Models\Course;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Course\CourseSection as TraitModel;

class CourseSection extends BaseModel
{
    use TraitModel;
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['course_id', 'title', 'description', 'active_date', 'expire_date', 'sort', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $api_fields = ['course_id', 'title', 'description', 'active_date', 'expire_date', 'sort'];
    public $list_fields = ['course_id', 'title', 'active_date', 'expire_date', 'sort'];

    public $messages = [];
    public $rules = [
        'title' => 'required',
        'course_id' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
