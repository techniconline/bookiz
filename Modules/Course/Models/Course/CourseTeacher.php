<?php

namespace Modules\Course\Models\Course;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Course\CourseTeacher as TraitModel;

class CourseTeacher extends BaseModel
{
    use TraitModel;
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['course_id', 'teacher_id','active', 'deleted_at', 'created_at', 'updated_at'];


    public $messages = [];
    public $rules = [
        'course_id' => 'required',
        'teacher_id' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
