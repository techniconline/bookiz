<?php

namespace Modules\Course\Models\Course;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Course\CourseRelation as TraitModel;

class CourseRelation extends BaseModel
{

    use SoftDeletes;
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['course_id', 'relation_course_id', 'type', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $api_fields = ['id', 'course_id', 'relation_course_id', 'type', 'active'];
    public $list_fields = ['id', 'course_id', 'relation_course_id', 'type', 'active'];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [
        'course_id' => 'required',
        'relation_course_id' => 'required',
        'type' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
