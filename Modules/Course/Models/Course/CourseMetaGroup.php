<?php

namespace Modules\Course\Models\Course;

use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Course\CourseMetaGroup as TraitModel;

class CourseMetaGroup extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['course_id', 'meta_group_id'];
    public $messages = [];
    public $rules = [
        'course_id' => 'required',
        'meta_group_id' => 'required',
    ];

    public $timestamps = false;

}
