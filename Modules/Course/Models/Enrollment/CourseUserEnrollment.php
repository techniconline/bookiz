<?php

namespace Modules\Course\Models\Enrollment;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Enrollment\CourseUserEnrollment as TraitModel;

class CourseUserEnrollment extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'course_id', 'enrollment_id', 'course_enrollment_id', 'activity_id', 'role_id', 'type', 'active_date', 'expire_date', 'params', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use TraitModel;
    use SoftDeletes;

    public $messages = [];
    public $rules = [
        'user_id' => 'required',
        'course_id' => 'required',
        'enrollment_id' => 'required',
        'role_id' => 'required',
    ];

    const TYPE_ACTIVITY = 'activity';
    const TYPE_ENROLLMENT = 'enrollment';

    public $api_fields = ['id', 'user_id', 'course_id', 'enrollment_id', 'activity_id', 'role_id', 'type', 'active_date', 'expire_date', 'params'];
    public $list_fields = ['id', 'user_id', 'course_id', 'enrollment_id', 'activity_id', 'type'];
    public $api_append_fields = ['active_date_by_mini_format', 'expire_date_by_mini_format'];
    public $list_append_fields = ['active_date_by_mini_format', 'expire_date_by_mini_format'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
