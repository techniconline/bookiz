<?php

namespace Modules\Course\Models\Enrollment;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Enrollment\CourseEnrollment as TraitModel;

class CourseEnrollment extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['course_id', 'enrollment_id','role_id', 'duration_type', 'start_date', 'end_date', 'duration', 'params', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use TraitModel;
    use SoftDeletes;

    public $messages = [];
    public $rules = [
        'duration_type' => 'required|in:no_expierd,expierd_date,expierd_duratio',
        'course_id' => 'required',
        'enrollment_id' => 'required',
        'role_id' => 'required',
    ];

    public $timestamps = true;
    public $dates = ['deleted_at'];

}
