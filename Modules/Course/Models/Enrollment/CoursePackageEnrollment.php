<?php

namespace Modules\Course\Models\Enrollment;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Enrollment\CoursePackageEnrollment as TraitModel;

class CoursePackageEnrollment extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['course_id', 'package_course_id', 'price', 'params', 'active', 'deleted_at', 'created_at', 'updated_at'];

    use TraitModel;
    use SoftDeletes;

    public $messages = [];
    public $rules = [
        'course_id' => 'required',
        'package_course_id' => 'required',
    ];


    public $api_fields = ['id', 'course_id', 'package_course_id', 'price'];
    public $list_fields = ['id','course_id', 'package_course_id', 'price'];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
