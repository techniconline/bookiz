<?php

namespace Modules\Course\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Course\Traits\Enrollment as TraitModel;

class Enrollment extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'alias', 'concurrent', 'priority', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $api_fields = ['id', 'name', 'alias', 'concurrent', 'priority', 'active'];
    public $list_fields = ['id', 'name', 'alias', 'concurrent', 'priority'];
    public $api_append_fields = [];
    public $list_append_fields = [];

    use TraitModel;
    use SoftDeletes;

    public $messages = [];
    public $rules = [
        'name' => 'required',
        'alias' => 'required',
    ];

    public $not_public_enrolls = ["manual","package","api"];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
