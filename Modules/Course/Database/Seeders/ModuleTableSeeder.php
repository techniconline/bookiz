<?php

namespace Modules\Course\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Module\Module;

class ModuleTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('currencies')->delete();
		// CurrenciesSeeder
        $module=new Module();
        $module->fill(array(
				'name' =>'course',
				'version' => '2017010101',
			))->save();

	}
}