<?php

namespace Modules\Course\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureGroup;
use Modules\Core\Models\Feature\FeatureGroupRelation;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;
use Modules\Core\Models\Meta\MetaGroup;

class FeaturesTableSeeder extends Seeder
{

    /**
     * @throws \Throwable
     */
    public function run()
    {
        $feature = new Feature();
        $language = Language::where("code", "fa")->first();
        $language_id = $language ? $language->id : null;

        $feature_ids = $items = [];
        $items[] = [
            "title" => "تعداد جلسات",
            "type" => "text",
            "language_id" => $language_id,
            "custom" => 1,
        ];

        $items[] = [
            "title" => "تعداد آزمون",
            "type" => "text",
            "language_id" => $language_id,
            "custom" => 1,
        ];

        $items[] = [
            "title" => "مدت دوره",
            "type" => "text",
            "language_id" => $language_id,
            "custom" => 1,
        ];

        foreach ($items as $item) {
            $feature->fill($item);
            if ($feature->save()) {
                $feature_ids[] = $feature->id;
            }
            $feature = new Feature();
        }
        // insert GROUP
        $instances = Instance::all();
        $fgIds = [];
        foreach ($instances as $instance) {
            $featureGroup = new FeatureGroup();
            $items = [
                "title" => "گروه ویژگی دوره",
                "instance_id" => $instance->id,
                "language_id" => $instance->language_id,
            ];
            $featureGroup->fill($items);
            if ($featureGroup->save()) {
                $fgIds[] = $feature_group_id = $featureGroup->id;
                foreach ($feature_ids as $index => $feature_id) {
                    $featureGroupRelation = new FeatureGroupRelation();
                    $itemsFGR = ["feature_id" => $feature_id, "feature_group_id" => $feature_group_id, "position" => $index];
                    $featureGroupRelation->insert($itemsFGR);
                }
            }
        }

        //insert Meta Group
        $metaGroup = new MetaGroup();
        $items = [
            "title"=>"متا دوره ها",
            "language_id"=>$language_id,
            "system"=>"course",
        ];
        $metaGroup->fill($items);
        $meta_group_id = null;
        if($metaGroup->save()){
            $meta_group_id = $metaGroup->id;
        }


    }
}
