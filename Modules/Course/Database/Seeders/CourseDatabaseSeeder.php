<?php

namespace Modules\Course\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CourseDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();
        $this->call('Modules\Course\Database\Seeders\ModuleTableSeeder');
        $this->call('Modules\Course\Database\Seeders\BlockTableSeeder');
        $this->call("Modules\Course\Database\Seeders\FeaturesTableSeeder");
        $this->call("Modules\Course\Database\Seeders\EnrollmentsTableSeeder");
    }
}
