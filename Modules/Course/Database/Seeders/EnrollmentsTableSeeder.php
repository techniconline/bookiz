<?php

namespace Modules\Course\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Models\Feature\Feature;
use Modules\Core\Models\Feature\FeatureGroup;
use Modules\Core\Models\Feature\FeatureGroupRelation;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;
use Modules\Core\Models\Meta\MetaGroup;
use Modules\Course\Models\Enrollment;

class EnrollmentsTableSeeder extends Seeder
{

    /**
     * @throws \Throwable
     */
    public function run()
    {

        $enrollmodel = new Enrollment();
        $exist = Enrollment::where('alias', 'free')->first();
        if(!$exist){
            $enrollmodel->fill(array(
                'name' => 'رایگان',
                'alias' => 'free',
                'priority' => 1,
                'active' => 1
            ))->save();
        }

        $enrollmodel = new Enrollment();
        $exist = Enrollment::where('alias', 'payment')->first();
        if(!$exist){
            $enrollmodel->fill(array(
                'name' => 'با هزینه',
                'alias' => 'payment',
                'priority' => 100,
                'active' => 1,
            ))->save();
        }

        $enrollmodel = new Enrollment();
        $exist = Enrollment::where('alias', 'manual')->first();
        if(!$exist){
            $enrollmodel->fill(array(
                'name' => 'دستی',
                'alias' => 'manual',
                'priority' => 0,
                'active' => 1,
            ))->save();
        }


        $enrollmodel = new Enrollment();
        $exist = Enrollment::where('alias', 'api')->first();
        if(!$exist){
            $enrollmodel->fill(array(
                'name' => 'با API',
                'alias' => 'api',
                'priority' => 0,
                'active' => 1,
            ))->save();
        }

        $enrollmodel = new Enrollment();
        $exist = Enrollment::where('alias', 'package')->first();
        if(!$exist){
            $enrollmodel->fill(array(
                'name' => 'پکیج',
                'alias' => 'package',
                'priority' => 0,
                'active' => 1,
            ))->save();
        }


    }
}
