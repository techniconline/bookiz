<?php

namespace Modules\Course\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Block\Block;
use Modules\Core\Models\Module\Module;


class BlockTableSeeder extends Seeder
{

    public function run()
    {
        //DB::table('currencies')->delete();

        $module_id = Module::where('name', 'course')->first()->id;

        if (!Block::active()->where('module_id', $module_id)->where('name', 'courses_block')->count()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'courses_block',
                'namespace' => 'Modules\Course\ViewModels\Blocks\CoursesBlock',
                'active' => 1,
            ))->save();
        }

        if (!Block::active()->where('module_id', $module_id)->where('name', 'course_info_block')->count()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'course_info_block',
                'namespace' => 'Modules\Course\ViewModels\Blocks\CourseInfoBlock',
                'active' => 1,
            ))->save();
        }


        if (!Block::active()->where('module_id', $module_id)->where('name', 'course_category_block')->count()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'course_category_block',
                'namespace' => 'Modules\Course\ViewModels\Blocks\CourseCategoryBlock',
                'active' => 1,
            ))->save();
        }


        if (!Block::active()->where('module_id', $module_id)->where('name', 'course_relations_block')->count()) {
            $block = new Block();
            $block->fill(array(
                'module_id' => $module_id,
                'name' => 'course_relations_block',
                'namespace' => 'Modules\Course\ViewModels\Blocks\CourseRelationsBlock',
                'active' => 1,
            ))->save();
        }


    }
}