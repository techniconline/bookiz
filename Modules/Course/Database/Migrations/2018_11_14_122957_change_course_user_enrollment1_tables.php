<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCourseUserEnrollment1Tables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('course_user_enrollments', function (Blueprint $table) {
            $table->unsignedInteger('course_enrollment_id')->after('enrollment_id')->nullable()->index();
        });

        Schema::table('course_user_enrollments', function (Blueprint $table) {
            $table->foreign('course_enrollment_id')->references('id')->on('course_enrollments')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_user_enrollments', function (Blueprint $table) {
            $table->dropForeign('course_user_enrollments_course_enrollment_id_foreign');
        });
        Schema::table('course_user_enrollments', function (Blueprint $table) {
            $table->dropColumn(['course_enrollment_id']);
        });

    }
}
