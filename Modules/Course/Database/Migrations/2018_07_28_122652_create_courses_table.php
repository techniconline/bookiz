<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('instance_id');
            $table->unsignedInteger('language_id');
            $table->unsignedInteger('category_id');
            $table->string('title');
            $table->mediumText('short_description')->nullable();
            $table->longText('description')->nullable();
            $table->string('type')->default('normal')->comment('normal, bind and ...');
            $table->unsignedInteger('quantity')->nullable();
            $table->timestamp('active_date')->nullable();
            $table->timestamp('expire_date')->nullable();
            $table->tinyInteger('active')->default('2')->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('courses', function (Blueprint $table) {
            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->dropForeign('courses_instance_id_foreign');
            $table->dropForeign('courses_language_id_foreign');
        });

        Schema::dropIfExists('courses');
    }
}
