<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEnrollTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('enrollments', function (Blueprint $table) {
            $table->tinyInteger("priority")->default(0)->after("alias");
            $table->boolean("concurrent")->default(0)->after("priority");
        });

        Schema::table('course_enrollments', function (Blueprint $table) {
            $table->unsignedInteger("role_id")->nullable()->after("enrollment_id");
        });

        Schema::table('course_enrollments', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enrollments', function (Blueprint $table) {
            $table->dropColumn(['priority', 'concurrent']);
        });

        Schema::table('course_enrollments', function (Blueprint $table) {
            $table->dropForeign('course_enrollments_role_id_foreign');
        });

        Schema::table('course_enrollments', function (Blueprint $table) {
            $table->dropColumn(['role_id']);
        });
    }
}
