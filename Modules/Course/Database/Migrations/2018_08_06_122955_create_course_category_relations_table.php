<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseCategoryRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_category_relations', function (Blueprint $table) {
            $table->unsignedInteger('course_id')->index();
            $table->unsignedInteger('category_id')->index();
        });

        Schema::table('course_category_relations', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');

             $table->foreign('category_id')->references('id')->on('course_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_category_relations', function (Blueprint $table) {
            $table->dropForeign('course_category_relations_course_id_foreign');
            $table->dropForeign('course_category_relations_category_id_foreign');
        });

        Schema::dropIfExists('course_category_relations');
    }
}
