<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseActivityItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_activity_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ca_id')->comment('course_activity_id');
            $table->string('type')->default('video')->comment('video, file and ...');
            $table->unsignedInteger('item_id')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('course_activity_items', function (Blueprint $table) {
            $table->foreign('ca_id')->references('id')->on('course_activities')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_activity_items', function (Blueprint $table) {
            $table->dropForeign('course_activity_items_ca_id_foreign');
        });

        Schema::dropIfExists('course_activity_items');
    }
}
