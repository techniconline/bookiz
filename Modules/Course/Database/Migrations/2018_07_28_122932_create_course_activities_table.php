<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->default('video')->comment("type : video and ...");
            $table->unsignedInteger('course_id');
            $table->dateTime('active_date')->nullable();
            $table->dateTime('expire_date')->nullable();
            $table->boolean('is_public')->default(1);
            $table->string('show_type')->default('free')->comment("free and pay");
            $table->tinyInteger('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('course_activities', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_activities', function (Blueprint $table) {
            $table->dropForeign('course_activities_course_id_foreign');
        });
        Schema::dropIfExists('course_activities');
    }
}
