<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseMetaGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_meta_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('course_id');
            $table->unsignedInteger('meta_group_id');

        });

        Schema::table('course_meta_groups', function (Blueprint $table) {
            $table->foreign('meta_group_id')->references('id')->on('meta_groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_meta_groups', function (Blueprint $table) {
            $table->dropForeign('course_meta_groups_meta_group_id_foreign');
            $table->dropForeign('course_meta_groups_course_id_foreign');
        });

        Schema::dropIfExists('course_meta_groups');
    }
}
