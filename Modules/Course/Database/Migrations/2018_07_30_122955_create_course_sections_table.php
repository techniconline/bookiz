<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('course_id')->index();
            $table->string('title');
            $table->text('description')->nullable();
            $table->dateTime('active_date')->nullable();
            $table->dateTime('expire_date')->nullable();
            $table->tinyInteger('sort')->default(0);
            $table->tinyInteger('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('course_sections', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('course_activities', function (Blueprint $table) {
            $table->unsignedInteger('course_section_id')->after("course_id")->nullable();
            $table->unsignedInteger('sort')->after("show_type")->default(0);
            $table->text('params')->after("show_type")->nullable()->comment("json");
        });

        Schema::table('course_activities', function (Blueprint $table) {
            $table->foreign('course_section_id')->references('id')->on('course_sections')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_sections', function (Blueprint $table) {
            $table->dropForeign('course_sections_course_id_foreign');
        });

        Schema::table('course_activities', function (Blueprint $table) {
            $table->dropForeign('course_activities_course_section_id_foreign');
        });

        Schema::table('course_activities', function (Blueprint $table) {
            $table->dropColumn(['course_section_id', 'sort', 'params']);
        });

        Schema::dropIfExists('course_sections');
    }
}
