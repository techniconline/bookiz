<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursePackageEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_package_enrollments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("course_id")->nullable()->index();
            $table->unsignedInteger("package_course_id")->nullable()->index();
            $table->float("price",14,2)->nullable();
            $table->longText("params")->nullable();
            $table->tinyInteger("active")->default(1)->index();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('course_package_enrollments', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('package_course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_package_enrollments', function (Blueprint $table) {
            $table->dropForeign('course_package_enrollments_package_course_id_foreign');
            $table->dropForeign('course_package_enrollments_course_id_foreign');

        });

        Schema::dropIfExists('course_package_enrollments');
    }
}
