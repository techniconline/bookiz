<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('course_id')->index();
            $table->unsignedInteger('relation_course_id')->index();
            $table->string('type')->deffault('normal')->nullable()->index();
            $table->tinyInteger('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('course_relations', function (Blueprint $table) {

            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('relation_course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_relations', function (Blueprint $table) {
            $table->dropForeign('course_relations_relation_course_id_foreign');
            $table->dropForeign('course_relations_course_id_foreign');
        });

        Schema::dropIfExists('course_relations');
    }
}
