<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_enrollments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("course_id")->nullable();
            $table->unsignedInteger("enrollment_id")->nullable();

            $table->dateTime("start_date")->nullable();
            $table->dateTime("end_date")->nullable();
            $table->unsignedInteger("duration")->default(0);
            $table->enum("duration_type",['no_expierd','expierd_date','expierd_duration'])->default('no_expierd');
            $table->longText("params")->nullable();

            $table->tinyInteger("active")->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('course_enrollments', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('enrollment_id')->references('id')->on('enrollments')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_enrollments', function (Blueprint $table) {
            $table->dropForeign('course_enrollments_enrollment_id_foreign');
            $table->dropForeign('course_enrollments_course_id_foreign');

        });

        Schema::dropIfExists('course_enrollments');
    }
}
