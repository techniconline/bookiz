<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCourseActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('course_activities', function (Blueprint $table) {
            $table->text('title')->after("id");
            $table->text('description')->after("title")->nullable();
            $table->integer('duration')->defualt(0)->after("show_type");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_activities', function (Blueprint $table) {
            $table->dropColumn(['description', 'title','duration']);
        });

    }
}
