<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCourseUserEnrollmentTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('course_user_enrollments', function (Blueprint $table) {
            $table->enum("type",['enrollment','activity'])->after("role_id")->default('enrollment')->nullable()->inedx();
            $table->unsignedInteger("activity_id")->nullable()->after("enrollment_id")->index();
            $table->dateTime("active_date")->nullbale()->change();
            $table->dateTime("expire_date")->nullbale()->change();
        });

        Schema::table('course_user_enrollments', function (Blueprint $table) {
            $table->foreign('activity_id')->references('id')->on('course_activities')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_user_enrollments', function (Blueprint $table) {
            $table->dropForeign('course_user_enrollments_activity_id_foreign');
        });
        Schema::table('course_user_enrollments', function (Blueprint $table) {
            $table->dropColumn(['type','activity_id']);
        });

    }
}
