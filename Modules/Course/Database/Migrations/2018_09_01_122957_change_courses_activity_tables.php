<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCoursesActivityTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('course_activities', function (Blueprint $table) {
            $table->enum("single_sales",['disabled','only_students','only_users','all_user'])->after("show_type");
            $table->float("single_amount",10,2)->nullable()->after("single_sales");
            $table->unsignedInteger("single_currency")->nullable()->after("single_amount");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_activities', function (Blueprint $table) {
            $table->dropColumn(['single_sales']);
            $table->dropColumn(['single_amount']);
            $table->dropColumn(['single_currency']);
        });

    }
}
