<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTeachersInstanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('teachers', function (Blueprint $table) {
            $table->unsignedInteger('instance_id')->nullable()->after("id");
        });

        Schema::table('teachers', function (Blueprint $table) {
            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teachers', function (Blueprint $table) {
            $table->dropForeign('teachers_instance_id_foreign');
        });
        Schema::table('teachers', function (Blueprint $table) {
            $table->dropColumn(['instance_id']);
        });

    }
}
