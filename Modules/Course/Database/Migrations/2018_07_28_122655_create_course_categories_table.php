<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_categories', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger("root_id")->nullable()->index();
            $table->unsignedInteger("parent_id")->nullable()->index();
            $table->unsignedInteger("instance_id")->nullable();
            $table->unsignedInteger("language_id")->nullable();
            $table->string("title")->nullable();
            $table->string("alias")->nullable();
            $table->string("key_trans")->nullable();
            $table->mediumText("attributes")->nullable();
            $table->unsignedInteger("sort")->default(0);
            $table->tinyInteger("active")->default(1);

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('course_categories', function (Blueprint $table) {
            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('parent_id')->references('id')->on('course_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('courses', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('course_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_categories', function (Blueprint $table) {
            $table->dropForeign('course_categories_instance_id_foreign');
            $table->dropForeign('course_categories_language_id_foreign');
            $table->dropForeign('course_categories_parent_id_foreign');
        });

        Schema::table('courses', function (Blueprint $table) {
            $table->dropForeign('courses_category_id_foreign');
        });

        Schema::dropIfExists('course_categories');
    }
}
