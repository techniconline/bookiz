<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseUserActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_user_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("user_id");
            $table->unsignedInteger("course_id");
            $table->unsignedInteger("activity_id");
            $table->dateTime("active_date")->nullbale();
            $table->dateTime("expire_date")->nullbale();
            $table->tinyInteger("active")->default(1);
            $table->softDeletes();
            $table->timestamps();
            $table->unique(['user_id','course_id','activity_id']);
        });

        Schema::table('course_user_activities', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('activity_id')->references('id')->on('course_activities')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_user_activities', function (Blueprint $table) {
            $table->dropForeign('course_user_activities_activity_id_foreign');
            $table->dropForeign('course_user_activities_course_id_foreign');
            $table->dropForeign('course_user_activities_user_id_foreign');
        });

        Schema::dropIfExists('course_user_activities');
    }
}
