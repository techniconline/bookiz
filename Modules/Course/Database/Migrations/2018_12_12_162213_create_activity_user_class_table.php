<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityUserClassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_video_stream_users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("user_id");
            $table->unsignedInteger("out_user_id")->nullable()->index();
            $table->string("user_name")->nullable()->index();
            $table->string("password")->nullable();
            $table->text("params")->nullable();
            $table->tinyInteger("active")->default(1)->index();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('course_video_stream_users', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_video_stream_users', function (Blueprint $table) {
            $table->dropForeign('course_video_stream_users_user_id_foreign');
        });

        Schema::dropIfExists('course_package_enrollments');
    }
}
