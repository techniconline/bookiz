<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseUserEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_user_enrollments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("user_id")->nullable();
            $table->unsignedInteger("course_id")->nullable();
            $table->unsignedInteger("enrollment_id")->nullable();
            $table->unsignedInteger("role_id")->nullable();
            $table->dateTime("active_date")->nullbale();
            $table->dateTime("expire_date")->nullbale();

            $table->tinyInteger("active")->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('course_user_enrollments', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('enrollment_id')->references('id')->on('enrollments')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('role_id')->references('id')->on('roles')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_user_enrollments', function (Blueprint $table) {
            $table->dropForeign('course_user_enrollments_enrollment_id_foreign');
            $table->dropForeign('course_user_enrollments_course_id_foreign');
            $table->dropForeign('course_user_enrollments_user_id_foreign');
            $table->dropForeign('course_user_enrollments_role_id_foreign');
        });

        Schema::dropIfExists('course_user_enrollments');
    }
}
