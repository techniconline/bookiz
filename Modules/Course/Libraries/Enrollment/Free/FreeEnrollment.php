<?php

namespace Modules\Course\Libraries\Enrollment\Free;

use Modules\Course\Libraries\Enrollment\Template\Enrollment;

class FreeEnrollment extends Enrollment
{
    protected $name = 'free';
    protected $polarEnrollment = ['payment', 'free'];
    protected $hasOutput = true;

    public function getCartData()
    {
        $data = [
            'item_type' => 'course',
            'item_id' => $this->course_id,
            'options' => [
                'enrollment_id' =>  $this->enrollment->id,
            ],
            'attributes' => [
                'button_title' => trans('course::enrollment.enroll_to_course'),
            ],
            'cost' => 0,
            'sale_cost' => 0,
            'text' => trans('course::enrollment.free'),
            'percent_discount' => 0,
            'currency' => 0,
        ];
        return $data;
    }

}