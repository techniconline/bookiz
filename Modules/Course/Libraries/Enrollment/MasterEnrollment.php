<?php

namespace Modules\Course\Libraries\Enrollment;

class MasterEnrollment
{
    private $onRunEnrollment = [];
    private $course_id = null;


    public function getEnrollment($enrollment, $data = null)
    {
        if (is_null($data)) {
            $data = [];
        }
        try {
            $enrollmentClass = "\\Modules\\Course\\Libraries\\Enrollment";
            $type = ucfirst($enrollment->alias);
            $enrollmentClass .= "\\" . $type . "\\" . $type . "Enrollment";
            $enrollmentClass = new $enrollmentClass();
            $enrollmentClass->setDataEnrollment($data, $this->onRunEnrollment)->setCourseId($this->course_id)->setEnrollment($enrollment);
            $this->onRunEnrollment[] = strtolower($type);
        } catch (\Exception $exception) {
            //
        }
        return $enrollmentClass;
    }


    public function setCourseId($course_id)
    {
        $this->course_id = $course_id;
        return $this;
    }


}