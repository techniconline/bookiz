<?php

namespace Modules\Course\Libraries\Enrollment\Manual;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Course\Libraries\Enrollment\Template\Enrollment;
use Modules\Course\Models\Course\Course;

class ManualEnrollment extends Enrollment
{

    protected $name = 'manual';
    protected $polarEnrollment = [];
    protected $hasOutput = false;

    /**
     * @return string
     */
    public function getUrlSearchUser()
    {
        return BridgeHelper::getUser()->getUrlSearchUser();
    }

    /**
     * @return null
     */
    public function getUserInCourseEnrollment()
    {
        if ($this->course_id) {
            return Course::enable()->with("courseUsersManualEnrollment")->find($this->course_id);
        }
        return null;
    }


}