<?php

namespace Modules\Course\Libraries\Enrollment\Payment;

use Modules\Course\Libraries\Enrollment\Template\Enrollment;

class PaymentEnrollment extends Enrollment
{
    protected $name = 'payment';
    protected $polarEnrollment = ['payment', 'free'];
    protected $hasOutput = true;


    public function getCartData()
    {
        $data = [
            'item_type' => 'course',
            'item_id' => $this->course_id,
            'options' => [
                'enrollment_id' => $this->enrollment->id
            ],
            'cost' => (int)$this->getData('cost'),
            'sale_cost' => (int)$this->getData("sale_cost"),
            'text' => $this->getData("sale_cost"),
            'percent_discount' => $this->getDiscount(),
            'currency' => $this->getData('currency_id'),
        ];
        return $data;
    }

    public function getDiscount()
    {
        $cost = $this->getData('cost');
        $sale_cost = $this->getData('sale_cost');
        if($sale_cost > 0 && $cost >0){
            $percent_sale_cost=(($sale_cost * 100) / $cost);
        }else{
            return 0;
        }
        $discount = 100 - $percent_sale_cost;
        return (ceil($discount));
    }

}