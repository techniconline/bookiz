<?php

namespace Modules\Course\Libraries\Enrollment\Template;

use BridgeHelper;

abstract Class Enrollment
{
    protected $name = '';
    protected $data = [];
    protected $onRunEnrollment = [];
    protected $polarEnrollment = [];
    protected $hasOutput = true;
    protected $viewTypes = ['content', 'list', 'widget'];
    protected $course_id = null;
    protected $enrollment = null;


    public function setCourseId($course_id)
    {
        $this->course_id = $course_id;
        return $this;
    }


    public function setEnrollment($enrollment)
    {
        $this->enrollment = $enrollment;
        return $this;
    }

    public function setDataEnrollment($data, $onRunEnrollment)
    {
        if (is_array($data)) {
            $data = (object)$data;
        }
        $this->data = $data;
        $this->onRunEnrollment = $onRunEnrollment;
        return $this;
    }

    public function getData($name = null, $default = null)
    {
        if (is_null($name)) {
            return $this->data;
        }
        if (isset($this->data->{$name})) {
            return $this->data->{$name};
        }
        return $default;
    }


    public function canMakeOutput()
    {
        if (!$this->hasOutput) {
            return false;
        }
        foreach ($this->polarEnrollment as $enrollmentName) {
            if (in_array($enrollmentName, $this->onRunEnrollment)) {
                return false;
            }
        }
        return true;
    }


    public function getForm($view = null)
    {
        $blockViewModel =& $this;
        if (is_null($view)) {
            $view = 'course::enrollment.enrolls.' . $this->name . '.form';
        }
        return view($view, compact('blockViewModel'))->render();
    }


    public function getRenderView($viewType = 'content', $view = null, $returnJson = false)
    {
        if ($this->canMakeOutput()) {
            $blockViewModel =& $this;

            if ($returnJson) {
                return $this->getCartDataJson();
            }

            if (is_null($view)) {
                $view = 'course::enrollment.enrolls.' . $this->name . '.' . $viewType;
            }
            return view($view, compact('blockViewModel'))->render();
        }
        return '';
    }

    /**
     * @return array
     */
    public function getCartDataJson()
    {
        $cartData = $this->getCartData();

        if (isset($cartData["cost"]) && $cartData["cost"]) {
            $cartData["cost"] = (int)BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->getData('cost'), $this->getData('currency_id'));
        }

        if (isset($cartData["sale_cost"]) && $cartData["sale_cost"]) {
            $cartData["sale_cost"] = (int)BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->getData('sale_cost'), $this->getData('currency_id'));
        }

        if (isset($cartData["sale_cost"]) && $cartData["sale_cost"]) {
            $cartData["text"] = BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($this->getData('sale_cost'), $this->getData('currency_id'), true);
        }

        return $cartData;
    }

    /**
     * @return array
     */
    public function getCartData()
    {
        $data = [
            'item_type' => 'course',
            'item_id' => $this->course_id,
            'options' => [
                'enrollment_id' => $this->enrollment->id
            ],
            'cost' => 0,
            'sale_cost' => 0,
            'text' => '',
            'percent_discount' => 0,
            'currency' => BridgeHelper::getCurrencyHelper()->getCurrentCurrencyId(),
        ];
        return $data;
    }

    public function getName()
    {
        return $this->name;
    }


}