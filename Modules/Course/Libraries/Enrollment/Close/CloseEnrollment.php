<?php

namespace Modules\Course\Libraries\Enrollment\Close;

use Modules\Course\Libraries\Enrollment\Template\Enrollment;

class CloseEnrollment extends Enrollment
{
    protected $name = 'close';
    protected $polarEnrollment = [];
    protected $hasOutput = true;

    public function getCartData()
    {
        $data = [
            'item_type' => 'course',
            'item_id' => $this->course_id,
            'options' => [
                'enrollment_id' => 0
            ],
            'cost' => 0,
            'sale_cost' => 0,
            'text' => trans('course::enrollment.closed'),
            'percent_discount' => 0,
            'currency' => 0,
        ];
        return $data;
    }


}