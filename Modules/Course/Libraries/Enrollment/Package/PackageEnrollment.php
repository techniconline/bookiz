<?php

namespace Modules\Course\Libraries\Enrollment\Package;

use Modules\Course\Libraries\Enrollment\Template\Enrollment;
use Modules\Course\Models\Course\Course;

class PackageEnrollment extends Enrollment
{

    protected $name = 'package';
    protected $polarEnrollment = [];
    protected $hasOutput = false;

    /**
     * @return string
     */
    public function getUrlSearchCourse()
    {
        return route("course.get_course_by_api");
    }

    /**
     * @return null
     */
    public function getUserInCourseEnrollment()
    {
        if ($this->course_id) {
            return Course::enable()->with("courseUsersPackageEnrollment")->find($this->course_id);
        }
        return null;
    }


}