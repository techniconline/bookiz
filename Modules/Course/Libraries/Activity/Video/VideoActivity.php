<?php

namespace Modules\Course\Libraries\Activity\Video;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Course\Libraries\Activity\BaseActivity;
use Modules\Course\Libraries\Activity\MasterActivity;
use Modules\Video\Models\Video\Video;

class VideoActivity extends BaseActivity
{
    private $videos;
    private $optionAttributes;
    protected $useAjax=true;

    /**
     * @return $this
     * @throws \Throwable
     */
    public function init()
    {
        $this->check()->setTypeActivity();
        return $this;
    }

    /**
     * @return array|mixed
     * @throws \Throwable
     */
    public function render()
    {
        return $this->call();
    }

    /**
     * @return array|mixed
     * @throws \Throwable
     */
    public function call()
    {
        return $this->init()->view()->getResponse();
    }

    /**
     * @return $this
     */
    private function setTypeActivity()
    {
        $this->typeActivity = $this->activity ? $this->activity->type : null;
        return $this;
    }


    /**
     * @return $this
     */
    private function getVideos()
    {
        if ($this->valid) {
            $course = $this->activity->course()->first();
            $video = new Video();
            $videos = $video->setRequestItems(['instance_id' => isset($course->instance_id) ? $course->instance_id : 0])->active()
                ->filterInstance()->get();

            $optionAttributes = [];
            if ($videos) {
                $optionAttributes = collect($videos)->mapWithKeys(function ($video) {
                    $video_url = $video->video_url;
                    $poster_url = $video->poster_url;
                    return [$video->id => ['data-video' => $video_url, 'data-poster' => $poster_url]];
                })->toArray();
            }

            $videos = $videos ? $videos->pluck('title', 'id') : [];
            $this->videos = $videos;
            $this->optionAttributes = $optionAttributes;


        }
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    private function view()
    {
        $videoActivity =& $this;
        $view = view('course::course_activity.activities.' . strtolower($this->typeActivity) . '.form', ['data' => $this->activity->params, "videoActivity" => $videoActivity])->render();
        $this->view = $view;
        return $this;
    }
    /**
     * @return array|mixed
     */
    public function getResponse()
    {
        $this->response = $this->view;
        return $this->response;
    }


    public function getIcon()
    {
        return 'fa-play-circle';
    }


    /**
     * @return string
     * @throws \Throwable
     */
    public function getOutput(){
        $this->init();
        $videoActivity =& $this;
        if($this->isApi()){
            return BridgeHelper::getVideoUse()->setModel($videoActivity->activity->getTable(), $videoActivity->activity->id)->getPlayer(true);
        }
        $content=view('course::course_activity.activities.' . strtolower($this->typeActivity) . '.output', ['data' => $this->activity->params, "videoActivity" => $videoActivity])->render();
        if($this->isAjax()){
            return [
                'content'=>$content,
                'assets'=>BridgeHelper::getVideoUse()->getVideoPlayerAssets(),
                ];
        }
        return $content;
    }

}