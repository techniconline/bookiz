<?php

namespace Modules\Course\Libraries\Activity\Exam;

use Illuminate\Support\Facades\Response;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Course\Libraries\Activity\BaseActivity;

class ExamActivity extends BaseActivity
{
    use ExamActivityTrait;

    private $exam;
    private $optionAttributes;
    protected $useAjax = false;

    /**
     * @return $this
     * @throws \Throwable
     */
    public function init()
    {
        $this->check()->setTypeActivity();
        return $this;
    }

    /**
     * @return $this
     */
    private function addAssetToTheme()
    {

        $assets = []; //block_js
        $assets [] = ['container' => 'up_theme_js_2', 'src' => ("assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"), 'name' => 'ui.widget'];
        $assets [] = ['container' => 'plugin_general_2', 'src' => ("jquery-ui/jquery-ui.min.js"), 'name' => 'jquery-ui'];
        app('getInstanceObject')->setActionAssets($assets);
        return $this;
    }

    /**
     * @return array|mixed
     * @throws \Throwable
     */
    public function render()
    {
        return $this->call();
    }

    /**
     * @return array|mixed
     * @throws \Throwable
     */
    public function call()
    {
        return $this->init()->addAssetToTheme()->view()->getResponse();
    }

    /**
     * @return $this
     */
    private function setTypeActivity()
    {
        $this->typeActivity = $this->activity ? $this->activity->type : null;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    private function view()
    {
        $viewActivity =& $this;
        $view = view('course::course_activity.activities.' . strtolower($this->typeActivity) . '.form', ['data' => $this->activity->params, "viewActivity" => $viewActivity])->render();
        $this->view = $view;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultShowScore()
    {
        return $this->getDataParams('show_final_score', 'not_show');
    }

    /**
     * @return array|mixed
     */
    public function getResponse()
    {
        $this->response = $this->view;
        return $this->response;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function getOutput()
    {
        $this->init();
        $data = $this->getUserExam();
        $content = $this->renderViewModel($data);
        return $content;
    }



    /**
     * @param $data
     * @return mixed
     */
    public function renderViewModel($data)
    {
        $userExamViewModel = BridgeHelper::getExam()->getUserExamViewModel();
        return $userExamViewModel->renderUserExam($data);
    }

    /**
     * @return $this|array
     */
    private function getUserExam()
    {

        $response = $this
            ->setExamRepeat(boolval(request()->get('exam_repeat')))
            ->setExamStart(boolval(request()->get('exam_start')))
            ->initExam();
        if ($response['action']) {
            $response['exam_data'] = $this->initAttempt()->getExam();
        }
        return $response;
    }

}