<?php

namespace Modules\Course\Libraries\Activity\Exam;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\Traits\Decorate\DecorateData;
use Modules\Exam\Models\ExamAttempt;
use Modules\Exam\Models\ExamQuestion;
use Modules\Exam\Models\ExamQuestionBank;
use Modules\Exam\Models\ExamQuestionUser;

trait ExamActivityTrait
{
    use DecorateData;

    private $course;
    private $user;
    private $courseCategory;
    private $attempt;
    private $end_date;
    private $start_date;
    private $exam_repeat = false;
    private $exam_start = false;
    private $attempt_id;
    private $count_attempt;
    private $attempts = [];
    private $examQuestionBanks = [];
    private $examQuestions = [];
    private $examQuestionsCacheKey = 'exam_question'; //first section key ---- pattern : ..._user_USERID_attempt_ATTEMPTID
    private $userExamQuestions;

    /**
     * @param bool $repeat
     * @return $this
     */
    public function setExamRepeat($repeat = false)
    {
        $this->exam_repeat = $repeat;
        return $this;
    }

    /**
     * @param bool $start
     * @return $this
     */
    public function setExamStart($start = false)
    {
        $this->exam_start = $start;
        return $this;
    }

    /**
     * @return $this|array
     */
    public function initExam()
    {
        if (!$this->valid) {
            return $this;
        }
        DB::beginTransaction();
        try {
            $this->setCourse()->checkValidate()->initUser()->createAttempt()->setExamQuestionBanks();
            DB::commit();
        } catch (\Exception $exception) {
            report($exception);
            DB::rollBack();
        }
        return $this->getExamResponse();
    }

    /**
     * @return $this
     */
    protected function checkValidate()
    {
        $start_date = DateHelper::setDateTime($this->getDataParams('start_date'))->getDateTime();
        $end_date = DateHelper::setDateTime($this->getDataParams('end_date'))->getDateTime();

        if ($start_date) {
            $start_date = strtotime($start_date) - 12600;
        }

        if ($end_date) {
            $end_date = strtotime($end_date) - 12600;
        }

        if ($start_date >= time()) {
            $start_date = DateHelper::setDateTime($this->getDataParams('start_date'))->getLocaleFormat(trans('core::date.datetime.medium'));
            $this->valid = false;
            $this->messages[] = trans('course::course.activity.exam_activity.alerts.exam_not_start_date', ['date' => $start_date]);
            $this->messages_json[] = trans('course::course.activity.exam_activity.alerts.exam_not_start_date_with_out_param', ['date' => $start_date]);
        }

        if ($this->valid && $end_date < time()) {
            $end_date = DateHelper::setDateTime($this->getDataParams('end_date'))->getLocaleFormat(trans('core::date.datetime.medium'));
            $this->valid = false;
            $this->messages[] = trans('course::course.activity.exam_activity.alerts.exam_ended_date', ['date' => $end_date]);
            $this->messages_json[] = trans('course::course.activity.exam_activity.alerts.exam_ended_date_with_out_param', ['date' => $end_date]);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getExamResponse()
    {
        return ['action' => $this->valid
            , 'count_exam' => $this->count_attempt
            , 'start_date' => DateHelper::setDateTime($this->getDataParams('start_date'))->getLocaleFormat(trans('core::date.datetime.medium'))
            , 'end_date' => DateHelper::setDateTime($this->getDataParams('end_date'))->getLocaleFormat(trans('core::date.datetime.medium'))
            , 'exam_question_count' => array_sum($this->getDataParams('exam_question_count'))
            , 'exam_repeat' => $this->exam_repeat
            , 'exam_repeat_link' => $this->exam_repeat ? route('course.activity', ['course_id' => $this->course ? $this->course->id : 0, 'activity_id' => $this->activity->id, 'exam_repeat' => 1, 'user'=>$this->user->id, 'timestamp'=>time()]) : null
            , 'exam_start' => $this->exam_start
            , 'exam_start_link' => $this->exam_start ? route('course.activity', ['course_id' => $this->course ? $this->course->id : 0, 'activity_id' => $this->activity->id, 'exam_start' => 1, 'user'=>$this->user->id, 'timestamp'=>time()]) : null
            , 'messages' => $this->messages
            , 'messages_json' => $this->messages_json
            , 'data' => $this->attempt, 'activity' => $this->activity, 'user' => $this->user];
    }

    /**
     * @return int
     */
    public function getCourseId()
    {
        return $this->activity ? $this->activity->course_id : 0;
    }

    /**
     * @return $this
     */
    public function setCourse()
    {
        $this->course = $this->activity->course;
        return $this;
    }

    /**
     * @param null $bank_id
     * @return array
     */
    public function getQuestions($bank_id = null)
    {
        if ($bank_id && isset($this->examQuestions[$bank_id])) {
            return $this->examQuestions[$bank_id];
        }
        return $this->examQuestions;
    }

    /**
     * @param array $bank_ids
     * @return mixed
     */
    private function getQuestionBanks(array $bank_ids)
    {
        $table = 'exam_question_banks_';
        $key = implode("_", $bank_ids);

        $cacheKey = $table . $key;
        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }
        $model = new ExamQuestionBank();
        $result = $model->active()
//            ->filterCurrentInstance()
            ->whereIn('id', $bank_ids)->get();
        if ($result->count()) {
            Cache::forever($cacheKey, $result);
            return $result;
        }
        return $result;
    }


    /**
     * @param $bank_id
     * @param null $take
     * @param bool $is_random
     * @return mixed
     */
    private function getExamQuestionsOfBank($bank_id, $take = null, $is_random = false)
    {
        $table = 'exam_questions_' . $bank_id;
        $cacheKey = $table;
        if (Cache::has($cacheKey)) {
            $result = Cache::get($cacheKey);
            if ($is_random) {
                $result = $this->getRandomQuestion($result);
            }
            if ($take) {
                $result = $result->take($take)->all();
            }
            return $result;
        }

        $model = new ExamQuestion();
        $result = $model->active()->where('exam_question_bank_id', $bank_id)->get();
        if ($result->count()) {
            Cache::forever($cacheKey, $result);
            if ($is_random) {
                $result = $this->getRandomQuestion($result);
            }
            if ($take) {
                $result = $result->take($take)->all();
            }
            return $result;
        }
        return $result;
    }

    /**
     * @param $questions
     * @return mixed
     */
    private function getRandomQuestion($questions)
    {
        if ($questions->count()) {
            $questions = $questions->shuffle();
        }
        return $questions;
    }

    /**
     * @return $this
     */
    public function setExamQuestionBanks()
    {
        if (!$this->valid) {
            return $this;
        }
        $eqb_ids = $this->getDataParams('exam_question_bank_id');
        $eqb_count = $this->getDataParams('exam_question_count');
        $is_random = $this->getDataParams('exam_is_random');

        if ($this->hasCache($this->user->id)) {
            return $this;
        }

        if (!$eqb_ids) {
            $this->valid = false;
//            $this->messages[] = trans('course::course.activity.exam_activity.alerts.not_find_bank_data', ['link' => route('course.view', ['course_id' => $this->course ? $this->course->id : 0])]);
            return $this;
        }

        $banks = $this->getQuestionBanks($eqb_ids);

        if (!$banks->count()) {
            $this->valid = false;
//            $this->messages[] = trans('course::course.activity.exam_activity.alerts.not_find_bank_data', ['link' => route('course.view', ['course_id' => $this->course ? $this->course->id : 0])]);
            return $this;
        }

        foreach ($banks as $bank) {
            $key = array_search($bank->id, $eqb_ids);
            $question_count = isset($eqb_count[$key]) ? $eqb_count[$key] : 0;
            $bank->exam_question_count = $question_count;
            $bank->exam_questions = null;
            if ($question_count) {
                $exam_questions = $this->getExamQuestionsOfBank($bank->id, $question_count, boolval($is_random));
                $this->exam_questions = $exam_questions;
            }

            $this->examQuestions[$bank->id] = $this->exam_questions;
        }

        $this->examQuestionBanks = $banks;
        return $this->createAttemptQuestion($this->attempt, $this->examQuestions, $this->user->id);
    }

    /**
     * @return $this
     */
    public function initUser()
    {
        $this->user = $user = BridgeHelper::getAccess()->getUser();
        if (!$user) {
            $this->valid = false;
            $this->messages[] = trans('course::course.activity.exam_activity.alerts.user_not_find');
            return $this;
        }
        return $this;
    }

    /**
     * @param $attempt
     * @return null
     */
    private function checkValidAttempt($attempt)
    {
        $start_date = $attempt->start_time;
        $end_date = $attempt->end_time;
        $this->attempt_id = $attempt->id;

        if (strtotime($end_date) < time()) {
            $userExamViewModel = BridgeHelper::getExam()->getUserExamViewModel();
            $resultSave = $userExamViewModel->saveAnswer($this->user, $this->attempt_id);

            $this->messages[] = trans('course::course.activity.exam_activity.alerts.exam_is_timeout');
            //$attempt = isset($resultSave->response['result_attempt']) && $resultSave->response['result_attempt'] ? $resultSave->response['result_attempt'] : $attempt;
//            if(isset($attempt->courseActivity)){
//                unset($attempt->courseActivity);
//            }

            //$this->count_attempt = ExamAttempt::enable()->where('course_activity_id', $this->activity->id)->where('user_id', $this->user->id)->whereIn('status', [ExamAttempt::STATUS_CLOSE, ExamAttempt::STATUS_EXPIRE_TIME])->count();

            if (isset($resultSave->response['action']) && $resultSave->response['action']) {
                $attempt->status = ExamAttempt::STATUS_CLOSE;
                $attempt->active = 5; //save after open page
            } else {
                $attempt->status = ExamAttempt::STATUS_CLOSE;
                //$attempt->score = 0;
                //$attempt->relative_score = 0;
                $attempt->active = 4;
            }

            $attempt->save();
            $this->valid = false;
        } else {
            $this->createCacheUserExam($this->user->id);
        }
        return $attempt;
    }

    /**
     * @return $this
     */
    public function createAttempt()
    {
        $model = new ExamAttempt();
        //user init
        if (!$this->valid) {
            $attempt = $model->enable()->where('course_activity_id', $this->activity->id)->where('user_id', $this->user->id)->orderBy('id', 'DESC')->first();
            $this->attempt = $attempt;
            return $this;
        }

        $exam_repeat = $this->getDataParams('exam_repeat', 1);
        $attempt = $model->enable()->where('course_activity_id', $this->activity->id)->where('user_id', $this->user->id)->whereIn('status', [$model::STATUS_NEW, $model::STATUS_OPEN])->first();
        if ($attempt) {
            $this->attempt = $attempt;
            $attempt = $this->checkValidAttempt($attempt);
            $this->attempt = $attempt;
            //$this->valid = false;
//            $this->messages[] = trans('course::course.activity.exam_activity.alerts.user_has_' . $attempt->status . '_exam');
            if ($attempt->status != $model::STATUS_CLOSE) {
                return $this;
            }
        }

        $attempts = $model->enable()->where('course_activity_id', $this->activity->id)->where('user_id', $this->user->id)->whereIn('status', [$model::STATUS_CLOSE, $model::STATUS_EXPIRE_TIME])->get();
        $attm = collect($attempts)->last();
        $this->count_attempt = $countAttempt = $attempts->count();
        $exam_repeat_condition = $this->getDataParams('exam_repeat_condition', 'all');
        //TODO not Completed
        if ($exam_repeat_condition == 'only_fail_exam' && $countAttempt) {
            $last_score = $attm->score;
            $exam_passing_score = $this->getDataParams('exam_passing_score', 12);
            if ($last_score >= $exam_passing_score) {
                $this->valid = false;
                $this->attempt = $attempts->last();
//                $this->messages[] = trans('course::course.activity.exam_activity.alerts.max_repeat_exam', ['max_repeat' => $exam_repeat]);
//                $this->messages_json[] = ['text' => trans('course::course.activity.exam_activity.alerts.max_repeat_exam_with_out_param'), 'params' => ['max_repeat' => $exam_repeat]];
                return $this;
            }
        }

        if ($countAttempt >= $exam_repeat) {
            $this->valid = false;
            $this->attempt = $attempts->last();
//            $this->messages[] = trans('course::course.activity.exam_activity.alerts.max_repeat_exam', ['max_repeat' => $exam_repeat]);
            $this->messages_json[] = trans('course::course.activity.exam_activity.alerts.max_repeat_exam_with_out_param', ['max_repeat' => $exam_repeat]);
            return $this;
        }

        if ($countAttempt && !$this->exam_repeat) {
            $this->valid = false;
            $this->attempt = $attempts->last();
            $this->exam_repeat = true;
//            $this->messages[] = trans('course::course.activity.exam_activity.alerts.confirm_repeat_exam', ['max_repeat' => $exam_repeat, 'exam_count' => $countAttempt, 'link' => route('course.view', ['course_id' => $this->course ? $this->course->id : 0])]);
            $this->messages_json[] = trans('course::course.activity.exam_activity.alerts.confirm_repeat_exam_with_out_param', ['max_repeat' => $exam_repeat, 'exam_count' => $countAttempt]);
            return $this;
        }

        if (!$countAttempt && !$this->exam_start) {
            $this->valid = false;
            $this->exam_start = true;
//            $this->messages[] = trans('course::course.activity.exam_activity.alerts.confirm_start_exam', ['max_repeat' => $exam_repeat, 'exam_count' => 0, 'link' => route('course.view', ['course_id' => $this->course ? $this->course->id : 0])]);
            $this->messages_json[] = trans('course::course.activity.exam_activity.alerts.confirm_start_exam_with_out_param', ['max_repeat' => $exam_repeat, 'exam_count' => $countAttempt]);
            return $this;
        }

        return $this->serviceCreateAttempt();
    }

    /**
     * @return $this
     */
    public function serviceCreateAttempt()
    {
        if (!$this->valid) {
            return $this;
        }

        $model = new ExamAttempt();
        $duration = $this->getDataParams('exam_total_time', 30);
        $data = [
            'user_id' => $this->user->id,
            'course_activity_id' => $this->activity->id,
            'start_time' => date("Y-m-d H:i:s"),
            'end_time' => date("Y-m-d H:i:s", now()->addMinutes($duration)->getTimestamp()),
            'status' => $model::STATUS_NEW,
        ];

        if ($model->fill($data)->save()) {
            $this->attempt = $model;
            //generate cache key
//            $this->messages[] = trans('course::course.activity.exam_activity.alerts.success_create_attempt');
        } else {
            $this->valid = false;
//            $this->messages[] = trans('course::course.activity.exam_activity.alerts.un_success_create_attempt');
        }

        return $this;
    }

    /**
     * @param $attempt
     * @param $examQuestions
     * @param $user_id
     * @return $this
     */
    private function createAttemptQuestion($attempt, $examQuestions, $user_id)
    {
        if (!$this->valid) {
            return $this;
        }
        if ($examQuestions) {
            $this->attempt = $attempt;
            $attempt_id = $attempt->id;
            $this->attempt_id = $attempt_id;
            $questionIds = [];
            foreach ($examQuestions as $bank_id => $questions) {
                if (!$questions) {
                    continue;
                }

                foreach ($questions as $question) {
                    $questionIds['list_questions'][] = $question->id;
                }
            }

            $model = new ExamQuestionUser();
            $data = [
                'user_id' => $user_id,
                'questions' => json_encode($questionIds),
                'exam_attempt_id' => $attempt_id,
                'status' => $model::STATUS_NEW
            ];

            if ($model->fill($data)->save()) {
                // created

            } else {
                $model->where('exam_attempt_id', $attempt_id)->where('user_id', $user_id)->update(['active' => 0, 'updated_at' => date("Y-m-d H:i:s")]);
                $attempt->delete();
                $this->valid = false;
//                        $this->messages[] = trans('course::course.activity.exam_activity.alerts.un_success_create_attempt_question');
                return $this;
            }

            //run exam
//            $this->messages[] = trans('course::course.activity.exam_activity.alerts.user_exam_created');
            return $this->createCacheUserExam($user_id, $questionIds);
        } else {
            $attempt->delete();
            $this->valid = false;
//            $this->messages[] = trans('course::course.activity.exam_activity.alerts.not_find_question_data');
        }

        return $this;
    }

    /**
     * @param $user_id
     * @param array $questionIds
     * @return $this
     */
    private function createCacheUserExam($user_id, $questionIds = [])
    {
        $this->examQuestionsCacheKey = $this->examQuestionsCacheKey . '_user_' . $user_id . '_attempt_' . $this->attempt->id;
        $duration = $this->getDataParams('exam_total_time', 30);

        if (empty($questionIds)) {
            $model = new ExamQuestionUser();
            $userQuestions = $model->active()->where('exam_attempt_id', $this->attempt_id)->where('user_id', $user_id)->where('status', $model::STATUS_NEW)->orderBy('id', 'DESC')->first();
//            ->with(['examQuestion.examAnswers'])
//            ->get();

            if (!$userQuestions) {
                $this->valid = false;
                return $this;
            }
            $question_ids = isset($userQuestions->questions->list_questions) ? $userQuestions->questions->list_questions : [];
        } else {
            $question_ids = isset($questionIds['list_questions']) ? $questionIds['list_questions'] : null;
        }

        if (!$question_ids) {
            $this->valid = false;
            return $this;
        }

        $data = $this->getCacheExamUserQuestion($this->examQuestionsCacheKey);

        if (!$data->count()) {
            $data = ExamQuestion::active()->whereIn('id', $question_ids)->with(['examAnswers'])->get();
        }

        if ($data->count() && $this->createCacheExam($data->toArray(), $duration)) {
            //change active attempt
            $this->attempt->active = 1;
            $this->attempt->save();

//            $this->messages[] = trans('course::course.activity.exam_activity.alerts.user_exam_running');
        } else {
            $this->valid = false;
//            $this->messages[] = trans('course::course.activity.exam_activity.alerts.user_exam_is_not_running');
        }

        return $this;
    }

    /**
     * @param $data
     * @param int $duration
     * @return mixed
     */
    private function createCacheExam($data, $duration = 30)
    {
        $duration = $duration + (isset($this->activity->params->exam_tolerance_time) ? $this->activity->params->exam_tolerance_time : 3);
        $keyCacheExpTime = $this->examQuestionsCacheKey . '_time';
        if (Cache::has($this->examQuestionsCacheKey)) {
            Cache::forget($keyCacheExpTime);
            Cache::forget($this->examQuestionsCacheKey);
        }
        Cache::put($this->examQuestionsCacheKey, $data, $duration);
        Cache::put($keyCacheExpTime, $this->attempt->end_time, $duration);
        return Cache::has($this->examQuestionsCacheKey);
    }

    /**
     * @param $key
     * @return mixed
     */
    private function getCacheExamUserQuestion($key)
    {
        return collect(Cache::get($key));
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function hasCache($user_id)
    {
        $this->initUser()->examQuestionsCacheKey = 'exam_question';
        $cacheKey = $this->examQuestionsCacheKey . '_user_' . $user_id . '_attempt_' . $this->attempt->id;
        return Cache::has($cacheKey);
    }


    /**
     * @return $this
     */
    public function reset()
    {
        $this->examQuestionsCacheKey = 'exam_question';
        $this->messages = [];
        return $this;
    }

    /**
     * @return $this
     */
    public function initAttempt()
    {
        $this->reset()->initUser();
        if (!$this->valid) {
            return $this;
        }

        $model = new ExamAttempt();
        $model = $model->enable()->where('course_activity_id', $this->activity->id)->where('user_id', $this->user->id)->whereIn('status', [$model::STATUS_NEW])->orderBy('id', 'DESC')->first();
        if (!$model) {
            $this->valid = false;
//            $this->messages[] = trans('course::course.activity.exam_activity.alerts.not_find_data_attempt');
            return $this;
        }

        $this->attempt = $model;
        //generate cache key
        $this->examQuestionsCacheKey = $this->examQuestionsCacheKey . '_user_' . $this->user->id . '_attempt_' . $model->id;
        $this->userExamQuestions = $this->getCacheExam($this->examQuestionsCacheKey);
        if ($this->userExamQuestions['action']) {
            $this->messages[] = trans('course::course.activity.exam_activity.alerts.exam_can_running');
            return $this;
        }

        $this->valid = false;
        $this->messages[] = trans('course::course.activity.exam_activity.alerts.exam_cannot_running');
        return $this;
    }

    /**
     * @param $key
     * @return mixed
     */
    protected function getCacheExam($key)
    {
        $keyCacheExpTime = $this->examQuestionsCacheKey . '_time';
        $examCache = Cache::get($key);
        if (!$examCache) {
            $this->createCacheUserExam($this->user->id);
            $examCache = Cache::get($key);
        }
        $examCacheExpTime = Cache::get($keyCacheExpTime);
        return ['action' => $examCache ? true : false, 'exam' => $examCache, 'now_time' => date("Y-m-d H:i:s"), 'exp_time' => $examCacheExpTime];
    }

    /**
     * @return mixed
     */
    public function getExam()
    {
        return ['action' => $this->valid, 'messages' => $this->messages, 'data' => $this->userExamQuestions];
    }


}