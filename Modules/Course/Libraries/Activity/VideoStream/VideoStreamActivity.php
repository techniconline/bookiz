<?php

namespace Modules\Course\Libraries\Activity\VideoStream;

use Illuminate\Support\Facades\Response;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Course\Libraries\Activity\BaseActivity;
use Modules\Course\Models\Teacher\Teacher;
use Modules\Course\Traits\Activity\ActivityVideoStreamTrait;

class VideoStreamActivity extends BaseActivity
{

    use ActivityVideoStreamTrait;

    private $accesses = [1 => 'student', 2 => 'presenter', 3 => 'teacher', 4 => 'manager'];
    public $base_room_url = 'https://www.skyroom.online/ch/farayad/';

    /**
     * @return $this
     * @throws \Throwable
     */
    public function init()
    {
        $this->check()->setTypeActivity();
        return $this;
    }

    /**
     * @return $this
     */
    private function addAssetToTheme()
    {
//        var_dump("AHMAD");
        $assets = []; //block_js
//        app('getInstanceObject')->setActionAssets($assets);
        return $this;
    }

    /**
     * @return array|mixed
     * @throws \Throwable
     */
    public function render()
    {
        return $this->call();
    }

    /**
     * @return array|mixed
     * @throws \Throwable
     */
    public function call()
    {
        return $this->init()->addAssetToTheme()->view()->getResponse();
    }

    /**
     * @return $this
     */
    private function setTypeActivity()
    {
        $this->typeActivity = $this->activity ? $this->activity->type : null;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    private function view()
    {
        $viewActivity =& $this;
        $view = view('course::course_activity.activities.' . get_lowercase_separated_by_underscores($this->typeActivity) . '.form', ['data' => $this->activity->params, "viewActivity" => $viewActivity])->render();
        $this->view = $view;
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getResponse()
    {
        $this->response = $this->view;
        return $this->response;
    }

    public function getIcon()
    {
        return 'fa-play-circle';
    }

    /**
     * @return string
     */
    public function getActiveIcon()
    {
        return 'fa-play-circle';
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function getOutput()
    {
        $this->init();
        $viewActivity =& $this;
        $isApi = app("getInstanceObject")->isApi();
        if($this->getDataParams('automatic_add_user')){
            $this->addUserAutomatic();
        }
        if (!$isApi) {
            $link = $this->getAutoLoginLink(600);
            $view = view('course::course_activity.activities.' . get_lowercase_separated_by_underscores($this->typeActivity) . '.output', ['data' => $this->activity->params, 'link' => $link, "viewActivity" => $viewActivity]);
            $response = $this->renderViewModel($view);
            return $response;
        } else {
            return $this->getJson();
        }
    }

    /**
     * @return bool
     * @throws \Modules\Course\Traits\Activity\HttpException
     * @throws \Modules\Course\Traits\Activity\JsonException
     * @throws \Modules\Course\Traits\Activity\NetworkException
     */
    private function addUserAutomatic()
    {
        $user = BridgeHelper::getAccess()->getUser();
        if ($user) {
            $result['students'] = $this->savePersonals([$user->id]);
            $ext = $this->getDataParams('external_params');
            $room_id = isset($ext->room_id)?$ext->room_id:null;
            $result['room_users'] = $this->addUsersToClass([$user->id], $room_id, 1);
            return true;
        }
        return false;
    }

    /**
     * @return array
     * @throws \Modules\Course\Traits\Activity\HttpException
     * @throws \Modules\Course\Traits\Activity\JsonException
     * @throws \Modules\Course\Traits\Activity\NetworkException
     */
    private function getJson()
    {
        $link = $this->getAutoLoginLink(600);
        $json = [
            'source' => $link
        ];
        return $json;
    }


    /**
     * @param $view
     * @return mixed
     */
    public function renderViewModel($view)
    {
        $videoStreamViewModel = BridgeHelper::getVideo()->getVideoStreamViewModel();
        $title = trans("course::course.activity.video_stream_activity.video_stream_title", ['activity_name' => $this->activity->title]);
        return $videoStreamViewModel->renderVideoStream($view, $title);
    }

    /**
     * @param array $data
     * @return array
     * @throws \Modules\Course\Traits\Activity\HttpException
     * @throws \Modules\Course\Traits\Activity\JsonException
     * @throws \Modules\Course\Traits\Activity\NetworkException
     */
    public function callAction(array $data)
    {
        $result = [];

        if (isset($data['name_class_room']) && isset($data['title_class_room'])) {
            $data_class = [
                'name' => isset($data['name_class_room']) ? $data['name_class_room'] : null,
                'title' => isset($data['title_class_room']) ? $data['title_class_room'] : null,
                'guest_login' => isset($data['guest_login']) ? boolval($data['guest_login']) : false,
                'op_login_first' => isset($data['op_login_first']) ? boolval($data['op_login_first']) : false,
                'max_users' => isset($data['max_users']) ? $data['max_users'] : 50,
                'session_duration' => isset($data['session_duration']) ? $data['session_duration'] : 60,
                'description' => isset($data['description']) ? $data['description'] : null,
            ];
            $result['class_room'] = $this->saveClassRoom($data_class);
        }

        if (isset($data['students'])) {
            $result['students'] = $this->savePersonals($data['students']);
        }

        $ext = $this->getDataParams('external_params');
        $room_id = isset($ext->room_id)?$ext->room_id:null;
        if ($room_id) {
            $user_ids = isset($data['students']) ? $data['students'] : [];
            $ext = $this->getDataParams('external_params');
            $room_id = isset($ext->room_id)?$ext->room_id:null;
            $result['room_users'] = $this->addUsersToClass($user_ids, $room_id, 1);
        }

        if ($this->getDataParams('teacher')) {
            $teacher = Teacher::find($this->getDataParams('teacher'));
            $user_id = $teacher ? $teacher->user_id : null;
            $result['teacher'] = $this->savePersonals([$user_id]);
            $user_ids = [$user_id];
            $ext = $this->getDataParams('external_params');
            $room_id = isset($ext->room_id)?$ext->room_id:null;
            $result['room_teacher'] = $this->addUsersToClass($user_ids, $room_id, 3);
        }

        return $result;
    }

}