<?php

namespace Modules\Course\Libraries\Activity;


abstract class BaseActivity
{
    public $typeActivity;
    public $activity;
    public $response = [];
    public $valid = true;
    public $view;
    public $messages = [];
    public $messages_json = [];
    public $data;
    public $data_params;
    public $course_id;
    protected $useAjax = false;

    public function setCourseId($course_id)
    {
        $this->course_id = $course_id;
        return $this;
    }

    public function setActivity($activity)
    {
        $this->activity = $activity;
        $this->data_params = $this->activity ? $this->activity->params : null;
    }

    /**
     * @return bool
     */
    protected function isDownloadable()
    {
        $downloadable = request()->get('download',0);
        return boolval($downloadable);
    }

    public function canUseAjax()
    {
        return $this->useAjax;
    }

    /**
     * @return mixed
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param null $key
     * @param null $default
     * @return null
     */
    public function getDataParams($key = null, $default = null)
    {
        if ($key) {
            if (isset($this->data_params->$key)) {
                return $this->data_params->$key;
            }
            return $default;
        } else {
            return $this->data_params;
        }
    }

    public function getIcon()
    {
        return 'fa-play-circle';
    }

    public function getActiveIcon()
    {
        return 'fa-play';
    }

    /**
     * @return $this
     */
    public function check()
    {
        if (!$this->activity) {
            $this->valid = false;
        }
        return $this;
    }

    public function isApi()
    {
        return app("getInstanceObject")->isApi();
    }


    public function isAjax()
    {
        if (request()->ajax() || request()->is_ajax) {
            return true;
        } elseif (app('getInstanceObject')->isAjax()) {
            return true;
        } else {
            return false;
        }
    }

}