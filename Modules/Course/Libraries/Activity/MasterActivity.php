<?php

namespace Modules\Course\Libraries\Activity;


class MasterActivity
{
    protected $activityClass;
    protected $course_id;


    /**
     * @param $activity
     * @return bool
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;
        $activityClass = "\\Modules\\Course\\Libraries\\Activity";
        if($this->activity){
            $this->typeActivity = $type = get_uppercase_by_underscores($this->activity->type);
            $activityClass .=  "\\".$type."\\".$type."Activity";
        }
        $this->activityClass = new $activityClass();

        try{
            $this->activityClass->setActivity($activity);
            return $this->activityClass;
        }catch (\Exception $exception){
            report($exception->getMessage());
            return false;
        }

    }

    /**
     * @param $course_id
     * @return $this
     */
    public function setCourseId($course_id)
    {
        $this->course_id = $course_id;
        return $this;
    }



}