<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 8/1/18
 * Time: 12:33 PM
 */

namespace Modules\Course\Libraries\Activity;


interface InterfaceActivity
{

    public function getResponse();
    public function getIcon();

}