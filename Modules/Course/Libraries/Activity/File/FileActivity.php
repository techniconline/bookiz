<?php

namespace Modules\Course\Libraries\Activity\File;

use Illuminate\Support\Facades\Response;
use Modules\Course\Libraries\Activity\BaseActivity;

class FileActivity extends BaseActivity
{
    private $file;
    private $optionAttributes;


    /**
     * @return $this
     * @throws \Throwable
     */
    public function init()
    {
        $this->check()->setTypeActivity();
        return $this;
    }

    /**
     * @return $this
     */
    private function addAssetToTheme(){
//        var_dump("AHMAD");
        $assets=[]; //block_js
        $assets [] = ['container' => 'up_theme_js_2', 'src' => ("assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"), 'name' => 'ui.widget'];
        $assets [] = ['container' => 'plugin_general_2', 'src' => ("jquery-ui/jquery-ui.min.js"), 'name' => 'jquery-ui'];
        $assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/plupload.full.min.js"), 'name' => 'plupload.full'];
        $assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/jquery.ui.plupload.js"), 'name' => 'jquery.ui.plupload'];
        $assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/i18n/fa.js"), 'name' => 'fa.js'];

        $assets [] = ['container' => 'general_style', 'src' => ("jquery-ui/jquery-ui.min.css"), 'name' => 'jquery-ui'];
        $assets [] = ['container' => 'general_style', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css"), 'name' => 'jquery.ui.plupload.css'];
        app('getInstanceObject')->setActionAssets($assets);
        return $this;
    }

    /**
     * @return array|mixed
     * @throws \Throwable
     */
    public function render()
    {
        return $this->call();
    }

    /**
     * @return array|mixed
     * @throws \Throwable
     */
    public function call()
    {
        return $this->init()->addAssetToTheme()->view()->getResponse();
    }

    /**
     * @return $this
     */
    private function setTypeActivity()
    {
        $this->typeActivity = $this->activity ? $this->activity->type : null;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    private function view()
    {
        $viewActivity =& $this;
        $view = view('course::course_activity.activities.' . strtolower($this->typeActivity) . '.form', ['data' => $this->activity->params, "viewActivity" => $viewActivity])->render();
        $this->view = $view;
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getResponse()
    {
        $this->response = $this->view;
        return $this->response;
    }


    public function getIcon()
    {
        return 'fa-cloud-download-alt';
    }

    public function getActiveIcon()
    {
        return 'fa-download';
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function getOutput(){
        $this->init();
        $all_files=[];
        if(isset($this->activity->params->files)){
            $all_files=(array)$this->activity->params->files;
        }
        $first = current($all_files);
        $downloadUrl='/storage'.$first->src;
        $isApi = app("getInstanceObject")->isApi();
        if (!$isApi || $this->isDownloadable()){
            $response = Response::make(null, 200);
            $response->header('X-Accel-Redirect', $downloadUrl);
            $response->header('Content-Disposition','attachment; filename='.$first->name);
            return $response;
        }else{
            return $this->getJson();
        }
    }

    private function getJson()
    {
        $json = [
            'source'=>route('course.activity',['course_id'=>$this->activity->course_id, 'activity_id'=>$this->activity->id,'download'=>1])
        ];
        return $json;
    }


}