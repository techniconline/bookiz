<?php

namespace Modules\Course\Libraries\Api\Quran;

use Illuminate\Http\Request;

use Modules\Course\Libraries\Api\Template\Enrollment;
use Modules\Exam\Models\ExamAttempt;
use Modules\User\Models\Role;
use Illuminate\Support\Facades\Cache;

class QuranEnrollment extends Enrollment
{

    protected $instance='quran';
    protected $connector='mobile';
    protected $config=[
        'referer'=>'gov.quranedu.ir',
        'url'=>'https://gov.quranedu.ir/api/e7f6b36b729e2509046906e4fd23ed3ccf2450a5/',
        'secret'=>'004c617d1dfc8498d755c3b944a2714576d7ba90',
    ];

    public function getInstanceName()
    {
        // TODO: Implement getInstanceName() method.
        return $this->instance;
    }


    public function getConnectorName()
    {
        // TODO: Implement getInstanceName() method.
        return $this->connector;
    }

    public function isValidRequest(Request $request)
    {
        return true;
        // TODO: Implement isValidRequest() method.
        $referer = $request->headers->get('referer');
        if($referer) {
            $referer = parse_url($referer);
        }else{
            $referer['host']=$this->config['referer'];
        }
        if($referer['host']==$this->config['referer']){
            if($request->has('user') && $request->has('course')){
                return true;
            }
        }
        return false;
    }


    public function getUserInfo(Request $request,$options = [])
    {
        $userInfo=$this->canAccess($request);
        if(!$userInfo){
            return false;
        }
        // TODO: Implement getUserInfo() method.
        $instance=$this->getInstance();
        if(!$instance){
            $this->setError(trans("course::messages.alert.instance_error"));
            return false;
        }
        $course=$this->getCourse($userInfo->course_code);
        if(!$course){
            $this->setError(trans("course::messages.alert.course_error"));
            return false;
        }
        $connector=$this->getConnector();
        if(!$connector){
            $this->setError(trans("course::messages.alert.connector_error"));
            return false;
        }
        $role=$this->getDefaultRole();
        if(!$role){
            $this->setError(trans("course::messages.alert.role_error"));
            return false;
        }

        $info=[
            'first_name'=>$userInfo->first_name,
            'last_name'=>$userInfo->last_name,
            $this->connector=>$userInfo->mobile,
            'national_code'=>$userInfo->national_code,
            'confirm'=>'1',
            'password'=>$userInfo->national_code,
            'instance'=>$instance,
            'course'=>$course,
            'connector'=>$connector,
            'role'=>$role,
        ];
        return (object) $info;
    }


    public function getDefaultRole(){
        $key='role_api_'.'student';
        if(!($role=Cache::get($key))){
            $role=Role::where('name','student')->where('type','course')->first();
            Cache::put($key,$role,600);
        }
        return $role;
    }

    public function canAccess(Request $request){
        $data=[];
        $data['secret']=$this->config['secret'];
        $data['user']=$request->get('user');
        $data['course']=$request->get('course');
        $config=[];
        $config['HTTP_AUTHENTICATION']=[];
        $config['HTTP_AUTHENTICATION']['USER']='quran';
        $config['HTTP_AUTHENTICATION']['PASSWORD']='987654';
        $result=$this->getUrlData($this->config['url'],$data,$config);
        if($result['status']!=200){
            $this->setError(trans("course::messages.alert.error_api"));
            return false;
        }
        if(isset($result['content']->error)){
            $this->setError($result['content']->error);
            return false;
        }
        return $result['content']->info;
    }



    public function getUserScore($course,$user){
        $scores=ExamAttempt::enable()->where('course_activity_id',4859)->where('user_id',$user->id)->get();
        $userScore=false;
        $count=count($scores);
        if($count){
            $userScore=['open'=>false,'scores'=>[]];
            foreach($scores as $score){
               $userScore['scores'][$score->id]=$score->score;
            }
            if($count<2){
                $userScore['open']=true;
            }
        }
        return  $userScore;
    }

}