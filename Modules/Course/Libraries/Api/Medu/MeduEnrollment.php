<?php

namespace Modules\Course\Libraries\Api\Medu;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Modules\Course\Libraries\Api\Template\Enrollment;
use Modules\Course\Models\Enrollment\CourseUserEnrollment;
use Modules\Course\ViewModels\Api\Medu\MeduViewModel;
use Modules\Exam\Models\ExamAttempt;
use Modules\Sale\Models\Traits\Payment\PaymentCloseTrait;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\User\Models\Role;
use Modules\User\Models\User;

class MeduEnrollment extends Enrollment
{

    use PaymentCloseTrait;

    protected $instance='ltms';
    protected $connector='medu';
    protected $byEnroll=false;
    protected $userScore=0;
    protected $config=[
        'referer'=>'www.ltms.medu.ir',
        'url'=>'http://service.ltms.medu.ir/LtmsService.asmx?WSDL.0x?Go',
        'keys'=>[
            '99506430'=>[
                'api'=>[
                    0=>'KLozHM1s95nDCElxnir7OMRaPcNuXs437EIK2VSIirE=',
                    1=>'KLozHM1s95n26Rhp1NImI4mw9S92WY1G7+saRk92Rn4=',
                ],
                'enroll'=>'pay',
                'landing'=>'quran',
            ],
        ],
    ];

    public function getInstanceName()
    {
        // TODO: Implement getInstanceName() method.
        return $this->instance;
    }


    public function getConnectorName()
    {
        // TODO: Implement getInstanceName() method.
        return $this->connector;
    }

    public function isValidRequest(Request $request)
    {
        // TODO: Implement isValidRequest() method.
        return true;
    }


    public function getUserInfo(Request $request,$options = []){
        $userInfo=$this->canAccess($request);
        if(!$userInfo){
            return false;
        }

        $instance=$this->getInstance();
        if(!$instance){
            $this->setError(trans("course::messages.alert.instance_error"));
            return false;
        }
        $course=$this->getCourse($userInfo->course_code);
        if(!$course){
            $this->setError(trans("course::messages.alert.course_error"));
            return false;
        }
        $connector=$this->getConnector();
        if(!$connector){
            $this->setError(trans("course::messages.alert.connector_error"));
            return false;
        }
        $role=$this->getDefaultRole();
        if(!$role){
            $this->setError(trans("course::messages.alert.role_error"));
            return false;
        }

        $info=[
            'enroll'=>$userInfo->enroll,
            $this->connector=>$userInfo->personalID,
            'confirm'=>'1',
            'password'=>$userInfo->personalID,
            'office_code'=>$userInfo->office_code,
            'instance'=>$instance,
            'course'=>$course,
            'connector'=>$connector,
            'role'=>$role,
        ];
        return (object) $info;
    }


    public function enrollUserBySteps(Request $request,$options = []){
        $meduViewModel=new MeduViewModel();
        $info=$this->getUserInfo($request,$options);
        if(!$info){
            return $meduViewModel->getErrorView($this->error);
        }
        $step=$request->get('step','info');
/*      if($step=='info'){
            $meduViewModel->setLayout('None');
            return $meduViewModel->renderedView('course::api.medu.info');
        }*/
        $user=$this->isUser($info);
        if($user && $this->getUserScore(null,$user)){
            $meduViewModel->setLayout('None');
            $args=explode('_',$user->username);
            $personalCode=$args[1];
            return $meduViewModel->renderedView('course::api.medu.score',['score'=>$this->userScore,'user'=>$user,'personalCode'=>$personalCode]);
        }
        if($user && $this->isEnroll($user->id,$info->course->id)){
                $url=route('course.view',['id'=>$info->course->id]);
                //$url=url('/course/view/304/activity/4858');
                return $meduViewModel->redirect($url);
        }
//        if($user->id != 75637){
//            return $meduViewModel->getErrorView('متاسفانه مهلت پرداخت و ثبت نام در دوره پایان یافته است.');
//        }
        $enrllmentsItem=$info->course->enrollments;
        if(!$enrllmentsItem || !count($enrllmentsItem)){
            return $meduViewModel->getErrorView('not set enrollment');
        }
        if(!$user){
            $user=new \stdClass();
            $user->first_name='';
            $user->last_name='';
            $user->national_code='';
            $user->id=0;
        }
        if($step=='enroll'){
            return $meduViewModel->enrollUser($request,$info,$user);
        }
        $url=$request->fullUrl();
        $enrollmentsList=[];
        foreach($enrllmentsItem as $enrollemntItem){
            $enrollmentsList[$enrollemntItem['options']['enrollment_id']]=$enrollemntItem['text'];
        }
        $infoData=['info'=>$info,'user'=>$user,'url'=>$url,'enrollmentsList'=>$enrollmentsList];
        $enrollment=view('course::api.medu.enrollment',$infoData)->render();
        $view=isset($info->enroll->landing)?$info->enroll->landing:'general';
        $meduViewModel->setLayout('None');
        $infoData['enrollment']=$enrollment;
        return $meduViewModel->renderedView('course::api.medu.'.$view,$infoData);
    }


    public function getErrorView($error){
        return $this->renderedView('course::api.error',['error'=>$this->getComponent('error',['error'=>$error])]);
    }

    public function getDefaultRole(){
        return Role::where('name','student')->where('type','course')->first();
    }


    public function canAccess($request,$api_index=0){
        $personalID=$request->get('q');
        $course_code=$request->get('Co');
        if(!$course_code || !isset($this->config['keys'][$course_code])){
            $this->setError(trans('course::messages.api.input_data_wrong'));
            return false;
        }
        $userInfo=new \stdClass();
        $userInfo->personalID=$personalID;
        $userInfo->course_code=$course_code;
        $userInfo->office_code=$api_index;
        $userInfo->enroll=(object)$this->config['keys'][$course_code];
        return $userInfo;
        try{
            $client = new \SoapClient("http://service.ltms.medu.ir/LtmsService.asmx?WSDL",array("trace" => 1, "exception" => 0));
            $has_other_api=false;
            if(is_array($this->config['keys'][$course_code]['api'])){
                $has_other_api=true;
                $params=array(
                    'CheckIsValid'=>array(
                        'SecurityCode'=>$this->config['keys'][$course_code]['api'][$api_index],
                        'PersID'=>$personalID
                    )
                );
            }else{
                $params=array(
                    'CheckIsValid'=>array(
                        'SecurityCode'=>$this->config['keys'][$course_code]['api'],
                        'PersID'=>$personalID
                    )
                );
            }

            $vem = $client->__soapCall('CheckIsValid',$params);
            if($vem->CheckIsValidResult=="0#0"){
                $this->setError(trans('course::messages.api.ltms_not_access'));
                if($has_other_api){
                    return $this->canAccess($request,$api_index+1);
                }
                return false;
            }else{
                $args=explode('#',$vem->CheckIsValidResult);
                if(!is_array($args) || $args[1]!=$course_code){
                    $this->setError(trans('course::messages.api.ltms_not_access'));
                    if($has_other_api){
                        return $this->canAccess($request,$api_index+1);
                    }
                    return false;
                }
                $userInfo=new \stdClass();
                $userInfo->personalID=$personalID;
                $userInfo->course_code=$course_code;
                $userInfo->office_code=$api_index;
                $userInfo->enroll=(object)$this->config['keys'][$course_code];
                return $userInfo;
            }
        }catch (Exception $e){
            report($e);
            $this->setError($e->getMessage());
            return false;
        }
        return false;
    }



    public function isEnroll($user_id,$course_id){
        $this->closeOpenedPayments();
        $isEnrol=CourseUserEnrollment::where('user_id',$user_id)->where('course_id',$course_id)->first();
        if($isEnrol){
            return $isEnrol;
        }
        return false;
    }


    public function isUser($info){
        $instance_id=$info->instance->id;
        $personal_id=trim($info->medu);
        $connector_id=$info->connector->id;
        $connector=UserRegisterKey::where('value', '=', $personal_id)->where('connector_id','=',$connector_id)->select(['user_id'])->first();
        if($connector){
            $user=User::where('id',$connector->user_id)->first();
            if($user){
                $user = Auth::loginUsingId($user->id);
                return $user;
            }
        }

        return false;
    }


    public function getUserScore($course,$user){
        $scores=ExamAttempt::enable()->where('course_activity_id',4858)->whereIn("status",["close", "expire"])->whereIn("active",[1,2,3,4,5])->where('user_id',$user->id)->get();
        $count=count($scores);
        if($count){
            foreach($scores as $score){
                if($score->score >=12){
                    $this->userScore=$score->score;
                    return true;
                }
            }
        }
        return  false;
    }


}