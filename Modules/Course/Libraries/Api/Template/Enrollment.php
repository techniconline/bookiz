<?php

namespace Modules\Course\Libraries\Api\Template;

use BridgeHelper;
use Illuminate\Http\Request;
use Modules\Core\Models\Instance\Instance;
use Modules\Course\Models\Course\Course;
use Modules\User\Models\Connector;
use Illuminate\Support\Facades\Cache;

abstract Class Enrollment
{

    protected $error=false;
    protected $byEnroll=true;
    abstract public function isValidRequest(Request $request);
    abstract public function getInstanceName();
    abstract public function getConnectorName();

    abstract public function getUserInfo(Request $request,$options=[]);

    public function getConnector(){
        $key='connector_api_'.$this->getConnectorName();
        if(!($connector=Cache::get($key))){
            $connector=Connector::where('code',$this->getConnectorName())->first();
            Cache::put($key,$connector,600);
        }
        return $connector;
    }

    public function getCourse($key){
        $cachekey='course_api_'.$key;
        if(!($course=Cache::get($cachekey))) {
            $course = Course::where('code', $key)->first();
            Cache::put($cachekey,$course,600);
        }
        return $course;
    }

    public function getInstance(){
        $key='instance_api_'.$this->getInstanceName();
        if(!($instance=Cache::get($key))) {
            $instance=Instance::where('name',$this->getInstanceName())->first();
            Cache::put($key,$instance,600);
        }
        return $instance;
    }

    public function setError($error){
        $this->error=$error;
        return $this;
    }


    public function getUrlData($url,$data,$config=[]) {
        try{
            $data_string = json_encode($data);
            $ch = curl_init($url);
            if(isset($config['HTTP_AUTHENTICATION'],$config['HTTP_AUTHENTICATION']['USER'],$config['HTTP_AUTHENTICATION']['PASSWORD'])){
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($ch, CURLOPT_USERPWD, $config['HTTP_AUTHENTICATION']['USER'] . ":" . $config['HTTP_AUTHENTICATION']['PASSWORD']);
            }
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );
            $content = curl_exec($ch);
            $status=curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if($status==200){
                $content=json_decode($content);
            }
            curl_close($ch);
            return array('status'=>$status,'content'=>$content);
        }catch(Exception $e){
            return false;
        }
    }

    public function getError(){
        if($this->error===false){
            $this->error='';
        }
        return $this->error;
    }

    public function byEnroll(){
        return $this->byEnroll;
    }



}