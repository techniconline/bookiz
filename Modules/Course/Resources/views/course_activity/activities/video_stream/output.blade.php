<div class="row">
    <div class="col-md-12">
        @if($link)
            {{--<iframe id="sky" frameborder="0" width="100%" height="100%" allowFullScreen="true" allow="microphone; camera" src="{!! $viewActivity->getDataParams('link_class_room') !!}"></iframe>--}}
            <iframe id="sky" frameborder="0" width="100%" height="100%" allowFullScreen="true" allow="microphone; camera" src="{!! $link !!}"></iframe>
        @else
            <div class="alert alert-danger">
                <strong>{{trans('error.error')}}: </strong> @lang('course::activity.video_stream.not_find')
            </div>
        @endif
    </div>
</div>
<style>
    body{
        margin: 0px !important;
    }
</style>