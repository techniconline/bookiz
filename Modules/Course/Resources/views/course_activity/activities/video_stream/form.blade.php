<div class="row margin-top-10">
    <div class="col-md-12">

        <div class="portlet box green _list_course_activity_items">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-video-camera"></i>@lang("course::course.activity.video_stream")
                </div>
                <div class="tools">
                    <a href="javascript:;" class="expand" data-original-title=""
                       title=""> </a>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">

                    <div class="portlet-body form">
                        @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                        <div class="col-lg-10">

                            {{--{!! FormHelper::input('text','params[url_stream]',old('params[url_stream]',$viewActivity->getDataParams('url_stream','http://')),['required'=>'required'--}}
                            {{--,'style'=>'direction: ltr;'--}}
                                {{--,'title'=>trans("course::course.activity.video_stream_activity.url_stream"),'helper'=>trans("course::course.activity.video_stream_activity.url_stream")]) !!}--}}

                            {!! FormHelper::input('text','params[title_class_room]',old('params[title_class_room]',$viewActivity->getDataParams('title_class_room','')),['required'=>'required'
                                ,'title'=>trans("course::course.activity.video_stream_activity.title_class_room"),'helper'=>trans("course::course.activity.video_stream_activity.title_class_room")]) !!}

                            {!! FormHelper::input('text','params[name_class_room]',old('params[name_class_room]',$viewActivity->getDataParams('name_class_room','')),['required'=>'required'
                                ,'style'=>'direction: ltr;'
                                ,'title'=>trans("course::course.activity.video_stream_activity.name_class_room"),'helper'=>trans("course::course.activity.video_stream_activity.name_class_room")]) !!}

                            {!! FormHelper::input('text','params[link_class_room]',old('params[link_class_room]',$viewActivity->getDataParams('link_class_room', $viewActivity->base_room_url.$viewActivity->getDataParams('name_class_room','') )),['readonly'=>'readonly'
                                ,'style'=>'direction: ltr;'
                                ,'title'=>trans("course::course.activity.video_stream_activity.link_class_room"),'helper'=>trans("course::course.activity.video_stream_activity.link_class_room")]) !!}



                            {!! FormHelper::checkbox('params[automatic_add_user]',1,old('params[automatic_add_user]',$viewActivity->getDataParams('automatic_add_user',0)) ,['title'=>trans("course::course.activity.video_stream_activity.automatic_add_user")]) !!}

                            {!! FormHelper::checkbox('params[guest_login]',1,old('params[guest_login]',$viewActivity->getDataParams('guest_login',0)) ,['title'=>trans("course::course.activity.video_stream_activity.guest_login")]) !!}

                            {!! FormHelper::checkbox('params[op_login_first]',1,old('params[op_login_first]',$viewActivity->getDataParams('op_login_first',0)) ,['title'=>trans("course::course.activity.video_stream_activity.op_login_first")]) !!}

                            {!! FormHelper::selectTag('params[teacher]',[],old('params[teacher]',$viewActivity->getDataParams('teacher')),['data-ajax-url'=>route('course.teacher.get_teacher_by_api')
                              , 'title'=>trans("course::course.info.teacher"),'helper'=>trans("course::course.select_teacher")]) !!}

                            {{--{!! FormHelper::datetime('params[start_date]',old('params[start_date]',$viewActivity->getDataParams('start_date')),['title'=>trans("course::course.activity.exam_activity.start_date")--}}
                                {{--,'date-year-current'=>2,'date-year-before'=>2,'set-default'=>0]) !!}--}}

                            {{--{!! FormHelper::datetime('params[end_date]',old('params[end_date]',$viewActivity->getDataParams('end_date')),['title'=>trans("course::course.activity.exam_activity.end_date")--}}
                                {{--,'date-year-current'=>2,'date-year-before'=>2,'set-default'=>0]) !!}--}}

                            {!! FormHelper::input('number','params[max_users]',old('params[max_users]',$viewActivity->getDataParams('max_users',30)),['required'=>'required'
                                ,'title'=>trans("course::course.activity.video_stream_activity.max_users"),'helper'=>trans("course::course.activity.video_stream_activity.max_users")]) !!}

                            {!! FormHelper::input('number','params[session_duration]',old('params[session_duration]',$viewActivity->getDataParams('session_duration',60)),['required'=>'required'
                                ,'title'=>trans("course::course.activity.video_stream_activity.session_duration"),'helper'=>trans("course::course.activity.video_stream_activity.session_duration")]) !!}

                            {!! FormHelper::selectTag('params[students][]',[],old('params[students][]',$viewActivity->getDataParams('students','')),['multiple'=>'multiple','data-ajax-url'=>route('course.teacher.get_user_by_api')
                                 , 'title'=>trans("course::course.activity.video_stream_activity.students"),'helper'=>trans("course::course.activity.video_stream_activity.students")]) !!}

                            {!! FormHelper::textarea('params[description]',old('params[description]',$viewActivity->getDataParams('description',''))
                                ,['title'=>trans("course::course.activity.video_stream_activity.description"),'helper'=>trans("course::course.activity.video_stream_activity.description")]) !!}



                        </div>


                    </div>

                </div>

            </div>
        </div>

    </div>
</div>


