<div class="row">
    <div class="col-md-12">
        @if(count($files))
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th class="bg-warning">@lang('course::activity.file.index')</th>
                        <th class="bg-warning">@lang('course::activity.file.name')</th>
                        <th class="bg-warning">@lang('course::activity.file.size')</th>
                        <th class="bg-warning">@lang('course::activity.file.link')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($files as $file)
                        <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ empty($file->name)?'-':$file->name }}</td>
                        <td>{{ empty($file->size)?'-':$file->size }}</td>
                        <td></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <div class="alert alert-danger">
                <strong>{{trans('error.error')}}: </strong> @lang('course::activity.file.not_file')
            </div>
        @endif
    </div>
</div>
