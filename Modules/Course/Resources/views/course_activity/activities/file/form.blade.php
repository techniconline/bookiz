<div class="row margin-top-10">
    <div class="col-md-12">

        <div class="portlet box green _list_course_activity_items">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-video-camera"></i>@lang("course::file.list")
                </div>
                <div class="tools">
                    <a href="javascript:;" class="expand" data-original-title=""
                       title=""> </a>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">

                    <div class="portlet-body form">

                        @php $validUpload = true @endphp
                        @if($courseActivity = $viewActivity->getActivity())
                            @if(isset($courseActivity->params->files))
                                <table class="table table-hover">
                                    <tr>
                                        <td>*</td>
                                        <td align="center">
                                            {!! trans("course::course.file_name") !!}
                                        </td>
                                        <td>
                                            {!! trans("course::course.action") !!}
                                        </td>
                                    </tr>
                                    @foreach($courseActivity->params->files as $index => $file)
                                        @php $validUpload = false @endphp

                                        <tr>
                                            <td>{!! $index+1 !!}</td>
                                            <td dir="ltr">{!! isset($file->name)?$file->name:$file->src !!} ({!! isset($file->ext)?$file->ext:null !!}) {!! isset($file->size)?(number_format(($file->size/1024))) .' kb':null !!} </td>
                                            <td>
                                                <a data-confirm-message="{!! trans("course::course.confirm_delete") !!}" href="{!! route('course.activity.file.delete', ['activity_id'=>$viewActivity->getActivity()->id,"file"=>md5($file->src)]) !!}"
                                                   class="btn btn-danger _delete_file">
                                                    <i class="fa fa-trash"></i>
                                                    {!! trans("course::course.delete_file") !!}
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            @endif
                        @endif

                        @if($validUpload)
                        <div class="portlet-body form">
                            <!-- Instantiating: -->
                            <div id="uploader_file">
                                {{--<p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>--}}
                            </div>

                            <script>
                                $('#uploader_file').plupload({
                                    runtimes: 'html5,flash,silverlight,html4',
                                    url: '{!! route('course.activity.file.upload', ['activity_id'=>$viewActivity->getActivity()->id]) !!}',
                                    filters: [
                                        {title: "File type", extensions: "doc,xls,docx,docx,pdf,zip,rar"},
                                    ],
                                    headers: {
                                        "Accept": "application/json",
                                        "X-CSRF-TOKEN": "{!! csrf_token() !!}"
                                    },
                                    views: {
                                        list: false,
                                        thumbs: true, // Show thumbs
                                        active: 'thumbs'
                                    },
                                    multipart_params: {"is_ajax": true},
                                    chunk_size: "1000kb",
                                    file_data_name: "file",
                                    multipart: true,
                                    multi_selection: true,
                                    dragdrop: true,

                                    // rename: true,
                                    // sortable: true,
                                    {{--flash_swf_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.swf") !!}',--}}
                                            {{--silverlight_xap_url : '{!! asset("vendor/jildertmiedema/laravel-plupload/js/Moxie.xap") !!}',--}}
                                    init: {
                                        FilesAdded: function (up, files) {
                                            // console.log('FilesAdded');

                                            var max_files = 1;
                                            plupload.each(files, function (file) {
                                                if (up.files.length > max_files) {
                                                    alert('{!! trans("core::messages.alert.maxFileUpload",['max_upload'=>1]) !!}');
                                                    up.removeFile(file);
                                                }
                                            });
                                            if (up.files.length >= max_files) {
                                                $('#pickfiles').hide('slow');
                                            }


                                        },
                                        FileUploaded: function (up, file, response) {
                                            // console.log('FileUploaded');
                                        },
                                        UploadComplete: function (up, files) {
                                            // console.log('UploadComplete');
                                        },
                                        FilesRemoved: function (up, files) {
                                            // console.log('Destroy');
                                        },
                                        BeforeUpload: function (up, files) {
                                            // console.log('BeforeUpload',files);
                                        },
                                        Init: function ($img_object, file, response) {

                                        }
                                    }
                                });
                            </script>
                        </div>
                        @endif
                    </div>

                </div>

            </div>
        </div>

    </div>
</div>


