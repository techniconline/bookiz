<div class="row margin-top-10">
    <div class="col-md-12">

        <div class="portlet box blue-dark _list_course_activity_items">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-question-circle"></i>@lang("course::exam.title")
                </div>
                <div class="tools">
                    <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                    <a class="_question_bank_plus"
                       title="{!! trans("course::course.activity.exam_activity.add_bank") !!}" data-index="0"><i
                                style="color: white; font-size: 2em" class="fa fa-plus-circle"></i></a>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">

                    <div class="portlet-body form">
                        @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                        <div class="_list_bank" data-rows="0">
                            @php $dataParams = $viewActivity->getDataParams(); @endphp
                            @php $banks = $viewActivity->getDataParams('exam_question_bank_id');@endphp
                            @php $questionBanks = $viewActivity->getDataParams('exam_question_count');@endphp
                            @php $countBank = $banks?count($banks):0; $countBank = $countBank?$countBank-1:0 @endphp
                            @for($i=0; $i<10; $i++)
                                <div class="_row" style="display: {!! $i>$countBank?'none':'' !!}">
                                    <div class="form-group _question_bank_select_box" data-index="{!! $i !!}">
                                        <div class="col-lg-6 col-lg-offset-1">
                                            <span class="_number">{!! $i+1 !!}.</span>
                                            <span>{!! trans("course::course.activity.exam_activity.bank_info") !!}</span>
                                            {!! FormHelper::selectTag('params[exam_question_bank_id]['.$i.']',[],old('params[exam_question_bank_id]['.$i.']',isset($banks[$i])?$banks[$i]:null),[
                                            $i>$countBank?'disabled':''=>$i>$countBank?'disabled':''
                                            ,$i==0?'required':''=>$i==0?'required':''
                                            ,'data-ajax-url'=>BridgeHelper::getExam()->getUrlSearchQuestionBank()
                                            ,'class'=>'_exam_question', 'title'=>trans("course::course.activity.exam_activity.title_bank"),'helper'=>trans("course::course.activity.exam_activity.title_bank")]) !!}

                                            {!! FormHelper::input('text','params[exam_question_count]['.$i.']',old('params[exam_question_count]['.$i.']',isset($questionBanks[$i])?$questionBanks[$i]:1),['required'=>'required', $i>$countBank?'disabled':''=>$i>$countBank?'disabled':''
                                            ,'class'=>'_exam_question_count','ajax-url'=> isset($banks[$i]) && $banks[$i] ? BridgeHelper::getExam()->getUrlSingleQuestionBank($banks[$i]):null
                                            ,'title'=>trans("course::course.activity.exam_activity.question_count"),'helper'=>trans("course::course.activity.exam_activity.question_count")]) !!}
                                        </div>
                                        @if($i>0)
                                            <a class="_question_bank_trash"
                                               title="{!! trans("course::course.activity.exam_activity.remove_bank") !!}"
                                               data-index="{!! $i !!}"><i style="color: red; font-size: 1.5em"
                                                                          class="fa fa-trash"></i></a>
                                        @endif
                                    </div>
                                    <hr>
                                </div>

                            @endfor


                        </div>
                        <div class="col-lg-10">

                            {!! FormHelper::input('number','params[exam_total_score]',old('params[exam_total_score]',$viewActivity->getDataParams('exam_total_score',20)),['required'=>'required'
                                ,'title'=>trans("course::course.activity.exam_activity.exam_total_score"),'helper'=>trans("course::course.activity.exam_activity.exam_total_score")]) !!}

                            {!! FormHelper::input('number','params[exam_passing_score]',old('params[exam_passing_score]',$viewActivity->getDataParams('exam_passing_score',12)),['required'=>'required'
                                ,'title'=>trans("course::course.activity.exam_activity.exam_passing_score"),'helper'=>trans("course::course.activity.exam_activity.exam_passing_score")]) !!}

                            {!! FormHelper::datetime('params[start_date]',old('params[start_date]',$viewActivity->getDataParams('start_date')),['title'=>trans("course::course.activity.exam_activity.start_date")
                                ,'date-year-current'=>2,'date-year-before'=>2,'set-default'=>0]) !!}

                            {!! FormHelper::datetime('params[end_date]',old('params[end_date]',$viewActivity->getDataParams('end_date')),['title'=>trans("course::course.activity.exam_activity.end_date")
                                ,'date-year-current'=>2,'date-year-before'=>2,'set-default'=>0]) !!}

                            {!! FormHelper::input('text','params[exam_total_time]',old('params[exam_total_time]',$viewActivity->getDataParams('exam_total_time',90)),['required'=>'required'
                               ,'title'=>trans("course::course.activity.exam_activity.exam_total_time"),'helper'=>trans("course::course.activity.exam_activity.exam_total_time")]) !!}

                            {!! FormHelper::input('number','params[exam_tolerance_time]',old('params[exam_tolerance_time]',$viewActivity->getDataParams('exam_tolerance_time',3)),['required'=>'required'
                                ,'title'=>trans("course::course.activity.exam_activity.exam_tolerance_time"),'helper'=>trans("course::course.activity.exam_activity.exam_tolerance_time")]) !!}

                            {!! FormHelper::input('number','params[exam_repeat]',old('params[exam_repeat]',$viewActivity->getDataParams('exam_repeat',1)),['required'=>'required'
                                ,'title'=>trans("course::course.activity.exam_activity.exam_repeat"),'helper'=>trans("course::course.activity.exam_activity.exam_repeat")]) !!}

                            {!! FormHelper::input('number','params[exam_per_page]',old('params[exam_per_page]',$viewActivity->getDataParams('exam_per_page',5)),['required'=>'required'
                                ,'title'=>trans("course::course.activity.exam_activity.exam_per_page"),'helper'=>trans("course::course.activity.exam_activity.exam_per_page")]) !!}

                            {!! FormHelper::select('params[exam_repeat_condition]',trans('course::course.activity.exam_activity.exam_repeat_conditions')
                                   ,old('params[exam_repeat_condition]',$viewActivity->getDataParams('exam_repeat_condition','all'))
                                   ,['title'=>trans("course::course.activity.exam_activity.exam_repeat_condition"),'helper'=>trans("course::course.activity.exam_activity.exam_repeat_condition")
                                   ,'placeholder'=>trans("course::course.activity.exam_activity.exam_repeat_condition")]) !!}

                            {!! FormHelper::select('params[show_final_score]',trans('course::course.activity.exam_activity.show_final_score_statuses'),old('params[show_final_score]',$viewActivity->getDefaultShowScore())
                                   ,['title'=>trans("course::course.activity.exam_activity.show_final_score"),'helper'=>trans("course::course.activity.exam_activity.show_final_score") ,'placeholder'=>trans("course::course.activity.exam_activity.show_final_score_select")]) !!}

                            {!! FormHelper::checkbox('params[exam_is_random]',1,old('params[exam_is_random]',$viewActivity->getDataParams('exam_is_random',0)) ,['title'=>trans("course::course.activity.exam_activity.exam_is_random")]) !!}

                            {!! FormHelper::checkbox('params[exam_answer_is_random]',1,old('params[exam_answer_is_random]',$viewActivity->getDataParams('exam_answer_is_random',0)) ,['title'=>trans("course::course.activity.exam_activity.exam_answer_is_random")]) !!}

                            {!! FormHelper::select('params[mines_score]',trans('course::course.activity.exam_activity.mines_scores'),old('params[mines_score]',$viewActivity->getDataParams('mines_score',0))
                                ,['title'=>trans("course::course.activity.exam_activity.exam_mines_score")
                                ,'helper'=>trans("course::course.activity.exam_activity.exam_mines_score"),'placeholder'=>trans("course::course.activity.exam_activity.exam_mines_score") ]) !!}

                            {!! FormHelper::select('params[score_notebook_status]',trans('course::course.activity.exam_activity.score_notebook_statuses'),old('params[score_notebook_status]',$viewActivity->getDataParams('score_notebook_status','enable'))
                                            ,['title'=>trans("course::course.activity.exam_activity.exam_impacted_in_score_notebook"), 'class'=>'_select_status_notebook'
                                            ,'helper'=>trans("course::course.activity.exam_activity.exam_impacted_in_score_notebook"),'placeholder'=>trans("course::course.activity.exam_activity.exam_impacted_in_score_notebook") ]) !!}

                            {!! FormHelper::input('number','params[exam_impacted_value]',old('params[exam_impacted_value]',$viewActivity->getDataParams('exam_impacted_value',1))
                                ,['required'=>'required', 'class'=>'_exam_impacted_value', 'disabled'=>'disabled'
                                ,'title'=>trans("course::course.activity.exam_activity.exam_impacted_value_in_score_notebook")
                                ,'helper'=>trans("course::course.activity.exam_activity.exam_impacted_value_in_score_notebook")]) !!}

                        </div>


                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<script>

    jQuery(document).ready(function () {


        $('div.form').on('change', 'select._select_status_notebook', function (event) {
            event.preventDefault();
            var $clicked = $(this);
            var $value = $clicked.find("option:selected").val();
            var $input_value = $('body').find('input._exam_impacted_value').first();
            if($value=='enable'){
                $input_value.prop('disabled', false);
                $input_value.parents('.form-group').fadeIn();

            }else{
                $input_value.prop('disabled', true);
                $input_value.parents('.form-group').fadeOut();
            }
        });

        $('div.form').on('click', 'a._question_bank_plus', function (event) {
            event.preventDefault();
            var $clicked = $(this);
            var $list = $('div._list_bank');
            var $counter = $list.attr("data-rows");
            var $index = $clicked.attr("data-index");

            $counter++;
            $index++;

            $list.attr("data-rows", $counter);
            // var $source = $list.find('div._question_bank_select_box[data-index="'+$counter+'"]').parents('._row').first();
            var $source = $list.find('div._question_bank_select_box:hidden:first').parents('._row').first();
            $source.fadeIn();
            $source.find('select, input').prop('disabled', false);

        });

        $('div.form').on('click', 'a._question_bank_trash', function (event) {
            event.preventDefault();
            var $clicked = $(this);
            var $list = $('div._list_bank');
            var $counter = $list.attr("data-rows");
            // $counter--;
            $list.attr("data-rows", $counter);

            var $source = $clicked.parents('._row').first();
            $source.fadeOut();
            $source.find('select, input').prop('disabled', true);

        });

        $('div.form').on('focusout', 'input._exam_question_count', function (event) {
            event.preventDefault();
            var $clicked = $(this);
            var $parent = $clicked.parents('div._row').first();
            var $selectBox = $parent.find('._exam_question');
            var $optionValue = $selectBox.find(':selected').val();
            var $url = $clicked.attr('ajax-url');
            if (!$url && $optionValue) {
                $url = '{!! BridgeHelper::getExam()->getUrlSingleQuestionBank() !!}/' + $optionValue + '/show';
            }
            if ($url) {
                ajaxGetSingleBank($clicked, $url, 'GET');
            }
        });


    });


    var $callAjax = false;
    var ajaxGetSingleBank = function ($this, $url, $method) {
        if ($callAjax) {
            return false;
        }
        $callAjax = true;
        $.ajax({
            method: $method,
            headers: {
                'X-CSRF-TOKEN': $('body input[name="_token"]').val() || $('meta[name="csrf-token"]').attr('content'),
                'ajax': 1
            },
            url: $url,
            data: {is_ajax: 1},
            success: function ($result) {

                if ($result.action) {
                    var $parent = $this.parents('._row').first();
                    var $input = $parent.find('input._exam_question_count');
                    var $inputValue = $input.val();
                    var $maxQuestions = $result.data.exam_questions_count;

                    if ($inputValue > $maxQuestions) {
                        alert('{!! trans("course::course.activity.exam_activity.max_question_validation") !!} {!! trans("course::course.activity.exam_activity.max_question") !!}' + $maxQuestions);
                        $input.val($maxQuestions);
                    }

                }

                $callAjax = false;
            },
            error: function (e) {
                alert('Error In Process!');
                $callAjax = false;
            }
        });

    };
</script>

