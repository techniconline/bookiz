<div class="row">
    <div class="col-md-12">
        @if(($data['action']))
            @if(isset($data['exam_data']['messages']) && $data['exam_data']['messages'])
                @foreach($data['exam_data']['messages'] as $message)
                    <div class="alert alert-warrning">
                        <strong> {!! $message !!} </strong>
                    </div>
                @endforeach
            @endif
            <table class="table table-responsive table-striped">
                <thead>
                <tr>
                    <th class="bg-warning">@lang('course::activity.file.index')</th>
                    <th class="bg-warning">@lang('course::activity.file.name')</th>
                    <th class="bg-warning">@lang('course::activity.file.size')</th>
                    <th class="bg-warning">@lang('course::activity.file.link')</th>
                </tr>
                </thead>
                <tbody>
                {{--@foreach($files as $file)--}}
                    {{--<tr>--}}
                        {{--<td>{{ $loop->iteration }}</td>--}}
                        {{--<td>{{ empty($file->name)?'-':$file->name }}</td>--}}
                        {{--<td>{{ empty($file->size)?'-':$file->size }}</td>--}}
                        {{--<td></td>--}}
                    {{--</tr>--}}
                {{--@endforeach--}}
                </tbody>
            </table>
        @else
            @if(isset($data['messages']) && $data['messages'])
                @foreach($data['messages'] as $message)
                    <div class="alert alert-danger">
                        <strong> {!! $message !!} </strong>
                    </div>
                @endforeach
            @endif
        @endif
    </div>
</div>
