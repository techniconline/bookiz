<div class="row margin-top-10">
    <div class="col-md-12">

        <div class="portlet box green _list_course_activity_items">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-video-camera"></i>@lang("course::video.list")
                </div>
                <div class="tools">
                    {{--<a title="@lang("course::course.activity.items.add")" class="_add_row_course_activity">--}}
                    {{--<i class="fa fa-plus-circle"--}}
                    {{--style="font-size: 20px; color: white"></i>--}}
                    {{--</a>--}}
                    <a href="javascript:;" class="expand" data-original-title=""
                       title=""> </a>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">

                    <div class="col-md-5">
                        {{--{{  session()->getId() }}--}}
                        {!! FormHelper::selectTag('params[video_id]',[],old('params[video_id]',isset($data->video_id)?$data->video_id:null),['data-ajax-url'=>BridgeHelper::getVideoUse()->getUrlVideoList()
                            , 'title'=>trans("course::video.list"),'helper'=>trans("course::video.list"),'class'=>'_video_select']) !!}

                            {{--{!! FormHelper::selectTag('params[video_id]',$videos,old('params[video_id]',isset($data->video_id)?$data->video_id:null)--}}
                                   {{--,['title'=>trans("course::video.list"),'helper'=>trans("course::video.list")--}}
                                   {{--,'placeholder'=>trans("course::video.select_video"),'class'=>'_video_select']--}}
                                   {{--,$optionAttributes) !!}--}}


                    </div>

                    <div class="col-md-6 col-md-offset-1">
                        {!! BridgeHelper::getVideoUse()->setModel($videoActivity->activity->getTable(), $videoActivity->activity->id)->getPlayer() !!}
                    </div>

                </div>

            </div>
        </div>

    </div>
</div>


