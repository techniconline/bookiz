<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("course::course.activity.".($courseActivity?'edit':'add')):
                    {!! "<b>".$course->title."</b>" !!}
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">

                {!! FormHelper::open(['role'=>'form','url'=>(isset($courseActivity) && $courseActivity? route('course.activity.update', ['instance_id'=>$courseActivity->id]) : route('course.activity.save'))
                        ,'method'=>(isset($courseActivity) && $courseActivity?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp
                <input type="hidden" name="course_id" value="{!! isset($course_id) && $course_id?$course_id :0 !!}">

                <div class="col-md-6">
                    {!! FormHelper::input('text','title',old('title',$courseActivity?$courseActivity->title:null)
                    ,['required'=>'required','title'=>trans("course::course.activity.title"),'helper'=>trans("course::course.activity.title")]) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::input('text','sort',old('sort',$courseActivity?$courseActivity->sort:null)
                    ,['required'=>'required','title'=>trans("course::course.activity.sort"),'helper'=>trans("course::course.activity.sort")]) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::select('course_section_id',$courseSections,old('course_section_id',$courseActivity?$courseActivity->course_section_id:null)
                           ,['title'=>trans("course::course.activity.section"),'helper'=>trans("course::course.activity.section")
                           ,'placeholder'=>trans("course::course.activity.select_section")]) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::checkbox('is_public',1,old('is_public',$courseActivity?$courseActivity->is_public:null)
                      ,['title'=>trans("course::course.activity.is_public"),'helper'=>trans("course::course.activity.is_public")]) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::select('type',$types,old('type',$courseActivity?$courseActivity->type:null)
                           ,['title'=>trans("course::course.activity.type"),'helper'=>trans("course::course.activity.type")
                           ,'placeholder'=>trans("course::course.activity.select_type")]) !!}
                </div>
                <div class="col-md-6">
                    {!! FormHelper::select('show_type',$showTypes,old('show_type',$courseActivity?$courseActivity->show_type:null)
                           ,['title'=>trans("course::course.activity.show_type"),'helper'=>trans("course::course.activity.show_type")
                           ,'placeholder'=>trans("course::course.activity.select_show_type")]) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::select('active',trans("course::course.statuses"),old('active',$courseActivity?$courseActivity->active:null)
                           ,['title'=>trans("course::course.activity.status"),'helper'=>trans("course::course.activity.status")
                           ,'placeholder'=>trans("course::course.activity.select_status")]) !!}
                </div>
                <div class="col-md-6">
                    {!! FormHelper::select('single_sales',trans('course::data.single_sales'),old('single_sales',$courseActivity?$courseActivity->single_sales:'disabled')
       ,['title'=>trans("course::course.activity.single_sales"),'helper'=>trans("course::course.activity.single_sales")]) !!}
                </div>
                <div class="col-md-6">
                    {!! FormHelper::input('text','single_amount',old('single_amount',$courseActivity?$courseActivity->single_amount:0.00)
,['title'=>trans("course::course.activity.single_amount"),'helper'=>trans("course::course.activity.single_amount")]) !!}
                </div>
                <div class="col-md-6">
                    {!! FormHelper::select('single_currency',BridgeHelper::getCurrencyHelper()->getCurrencyList(true),old('single_currency',$courseActivity?$courseActivity->single_currency:'')
,['title'=>trans("course::course.activity.single_currency"),'helper'=>trans("course::course.activity.single_currency"),'placeholder'=>'']) !!}
                </div>
                <div class="col-md-6">
                    {!! FormHelper::datetime('active_date',old('active_date',$courseActivity?$courseActivity->active_date:null)
                    ,['title'=>trans("course::course.activity.active_date"),'helper'=>trans("course::course.activity.active_date")
                        ,'date-year-current'=>1,'date-year-before'=>2,'set-default'=>0]) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::datetime('expire_date',old('expire_date',$courseActivity?$courseActivity->expire_date:null)
                    ,['title'=>trans("course::course.activity.expire_date"),'helper'=>trans("course::course.activity.expire_date")
                        ,'date-year-current'=>2,'date-year-before'=>2,'set-default'=>0]) !!}
                </div>

                <div class="col-md-12">
                    {!! FormHelper::input('number','duration',old('duration',$courseActivity?$courseActivity->duration:0)
,['title'=>trans("course::course.activity.duration"),'helper'=>trans("course::course.activity.duration_minute")]) !!}
                </div>


                <div class="col-md-12">
                    {!! FormHelper::editor('description',old('description',$courseActivity?$courseActivity->description:null)
                    ,['title'=>trans("course::course.activity.description"),'helper'=>trans("course::course.activity.description")]) !!}
                </div>

                @if($viewCourseActivity)
                    {!! $viewCourseActivity !!}
                @endif

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("course::course.activity.submit")], ['title'=>trans("course::course.activity.cancel"), 'url'=>route('course.activity.index',['course_id'=>isset($course_id)?$course_id:0])], ['class'=>'btn form-control input-small _language_list']) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}


            </div>


        </div>

    </div>

</div>


