<div class="row _copy_course">
    <div class="col-md-12">

        <div class="portlet box green ">
            <div class="portlet-title">
                <div class="caption">

                    <i class="fa fa-list"></i>{!! $viewModel->getTitle() !!}
                </div>
            </div>
            <div class="portlet-body">
                {!! FormHelper::open(['role'=>'form','url'=>route('course.copy.save', ['course_id'=>$viewModel->getCourseId()])
                        ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data","id"=>"form_copy"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp


                <div class="form-body" style="padding-right: 5%;">
                    <div class="form-group">

                        <div class="row">
                            <div class="col-md-12">
                                {!! FormHelper::input('text','title',old('title',$viewModel->getCourse()->title) ,['required'=>'required','title'=>trans("course::course.title"),'helper'=>trans("course::course.title")]) !!}
                            </div>
                            {{--<div class="col-md-6">--}}
                                {{--{!! FormHelper::input('text','code',old('code',$viewModel->getCourse()->getGenerateCourseCode()) ,['required'=>'required','title'=>trans("course::course.code"),'helper'=>trans("course::course.code")]) !!}--}}
                            {{--</div>--}}
                            <div class="col-md-12">
                                {!! FormHelper::select('instance_id',$instances,old('instance_id',app('getInstanceObject')->getCurrentInstanceId()) ,['title'=>trans("course::category.instance"),'helper'=>trans("course::category.instance"), 'placeholder'=>trans("course::category.instance")]) !!}
                            </div>

                            @foreach($viewModel->getCopyParts() as $part)
                                <div class="col-md-6 margin-top-10">
                                    {!! FormHelper::checkbox('copy_parts[]',$part,1,['title'=>trans("course::course.course_copy.".$part), 'id'=>'copy_parts_'.$part]) !!}
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("course::course.submit")], ['title'=>trans("course::course.cancel") , 'url'=>route('course.index')]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}


            </div>
        </div>

    </div>
</div>

