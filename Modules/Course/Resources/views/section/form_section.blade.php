<div class="row _section">
    <div class="col-md-12">

        <div class="portlet box green ">
            <div class="portlet-title">
                <div class="caption">

                    <i class="fa fa-list"></i>@lang("course::course.section.".($courseSection?"edit":"add"))
                </div>
            </div>
            <div class="portlet-body">
                {!! FormHelper::open(['role'=>'form','url'=>(isset($courseSection) && $courseSection? route('course.section.update', ['course_section_id'=>$courseSection->id]) : route('course.section.save', ['course_id'=>$viewModel->getCourseId()]))
                        ,'method'=>(isset($courseSection) && $courseSection?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data","id"=>"form_section"]) !!}

                <input type="hidden" name="course_id" value="{!! $viewModel->getCourseId() !!}">

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp


                <div class="form-body" style="padding-right: 5%;">
                    <div class="form-group">

                        <div class="row">
                            <div class="col-md-6 margin-top-10">

                                {!! FormHelper::input('text','title',old('title',$courseSection?$courseSection->title:null) ,['required'=>'required','title'=>trans("course::course.section.title"),'helper'=>trans("course::course.section.title")]) !!}

                            </div>

                            <div class="col-md-6 margin-top-10">

                                {!! FormHelper::input('text','sort',old('sort',$courseSection?$courseSection->sort:null)
                                    ,['required'=>'required','title'=>trans("course::course.section.sort"),'helper'=>trans("course::course.section.sort")]) !!}

                            </div>

                            <div class="col-md-6 margin-top-10">


                                {!! FormHelper::datetime('active_date',old('active_date',$courseSection?$courseSection->active_date:null)
                                ,['title'=>trans("course::course.section.active_date"),'helper'=>trans("course::course.section.active_date")
                                    ,'date-year-current'=>1,'date-year-before'=>3,'set-default'=>0]) !!}

                            </div>
                            <div class="col-md-6 margin-top-10">

                                {!! FormHelper::datetime('expire_date',old('expire_date',$courseSection?$courseSection->expire_date:null)
                                ,['title'=>trans("course::course.section.expire_date"),'helper'=>trans("course::course.section.expire_date")
                                    ,'date-year-current'=>2,'date-year-before'=>2,'set-default'=>0]) !!}

                            </div>


                            <div class="col-md-12 margin-top-10">
                                {!! FormHelper::textarea('description',old('description',$courseSection?$courseSection->description:null)
                                ,['title'=>trans("course::course.description"),'helper'=>trans("course::course.description")]) !!}

                            </div>

                        </div>
                    </div>
                </div>

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("course::course.submit")], ['title'=>trans("course::course.cancel")
                                            , 'url'=>route('course.section.index',["course_id"=>$viewModel->getCourseId()])]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}


            </div>
        </div>

    </div>
</div>

