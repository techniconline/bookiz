

    <div class="row margin-top-10 _section">
        <hr>
        <div class="col-md-10 col-md-offset-1">

            <div class="portlet box green _simple_list_course_sections">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i>@lang("course::course.sections")
                    </div>
                    <div class="tools">
                        <a title="@lang("course::course.section.add")" class="_add_row_course_section_simple">
                            <i class="fa fa-plus-circle"
                               style="font-size: 20px; color: white"></i>
                        </a>
                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    </div>
                </div>

                <div class="portlet-body">
                    <hr>

                    <table class="table table-striped table-hover" id="simple_list_course_sections">
                        <thead>
                        <tr>
                            <th width="10%"> #</th>
                            <th> @lang("course::course.section.title") </th>
                            <th> @lang("course::course.section.sort")
                                <a class="_edit_sort"><i class="fa fa-edit"></i></a>
                                <a data-confirm-message="@lang("course::course.section.confirm")"
                                   style="display: none" href="{!! route('course.section.sorting') !!}"
                                   class="_save_sort"><i class="fa fa-save" style="color: red"></i></a>
                            </th>
                            <th width="10%"> @lang("course::course.section.action") </th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr data-title="@lang("course::course.section_template")"
                            data-index-static="{!! isset($course->courseSections)?count($course->courseSections):1 !!}"
                            data-index="{!! isset($course->courseSections)?count($course->courseSections):1 !!}"
                            class="_template_row" style="display: none">
                            <td class="_row_number">1</td>
                            <td class="_title">
                                {!! FormHelper::input('text','title_new[0]',null ,[ 'class'=>'_title _new_item']) !!}
                            </td>
                            <td class="_sort">
                                {!! FormHelper::input('text','sort_new[0]',null ,['class'=>'input-xsmall _sort']) !!}
                            </td>
                            <td>
                                <a class="_delete_row_course_section_simple" title="@lang("course::course.section.delete")">
                                    <i class="fa fa-times-circle" style="font-size: 20px; color: red"></i>
                                </a>
                            </td>
                        </tr>

                        @if(isset($course) && $course && $course->courseSections)

                            @foreach($course->courseSections as $index => $section)

                                <tr>
                                    <td> {!! $index+1 !!}</td>
                                    <td class="_title">
                                        <input type="text" disabled style="text-align: center"
                                               class="form-control input _title" value="{!! $section->title !!}"
                                               name="title_section[{!! $section->id !!}]">
                                    </td>
                                    <td class="_sort">
                                        <input type="text" disabled style="text-align: center"
                                               class="form-control input-xsmall _sort" value="{!! $section->sort !!}"
                                               name="sort_section[{!! $section->id !!}]">
                                    </td>
                                    <td>
                                        <a href="{!! route('course.section.delete', ['course_section_id'=>$section->id]) !!}"
                                           class="_delete_row_course_section"
                                           title="@lang("course::course.section.delete")"
                                           data-confirm-message="@lang("course::course.confirm_delete")">
                                            <i class="fa fa-times-circle"
                                               style="font-size: 20px; color: red"></i> </a>
                                        <a href="{!! route('course.section.update', ['course_section_id'=>$section->id]) !!}"
                                           class="_edit_row_course_section_simple"
                                           title="@lang("course::course.section.edit")">
                                            <i class="fa fa-edit"
                                               style="font-size: 20px; color: blue"></i> </a>
                                    </td>
                                </tr>

                            @endforeach

                        @else
                            <tr>
                                <td>1</td>
                                <td class="_title">
                                    {!! FormHelper::input('text','title_new[0]',trans("course::course.section_template").' 1' ,['required'=>'required', 'class'=>'_title ']) !!}
                                </td>
                                <td class="_sort">
                                    {!! FormHelper::input('text','sort_new[0]',1 ,['required'=>'required','class'=>'input-xsmall _sort']) !!}
                                </td>
                                <td>
                                    {{--<a--}}
                                       {{--class="_delete_row_course_section"--}}
                                       {{--title="@lang("course::course.section.delete")">--}}
                                        {{--<i class="fa fa-times-circle"--}}
                                           {{--style="font-size: 20px; color: red"></i>--}}
                                    {{--</a>--}}
                                </td>
                            </tr>
                        @endif

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

