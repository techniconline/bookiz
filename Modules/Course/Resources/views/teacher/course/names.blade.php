@foreach($teachers as $courseTeacher)
    @if($loop->iteration>1), @endif
    {{ $courseTeacher->teacher->user->first_name.' '.$courseTeacher->teacher->user->last_name }}
@endforeach