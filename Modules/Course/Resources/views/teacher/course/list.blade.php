﻿@foreach($teachers as $courseTeacher)
<div class="col-xl-12 col-lg-12 col-md-12 box_course5 floatright ">
    @php $avatar=$courseTeacher->teacher->user->small_avatar_url @endphp
    
	  <img  src="{{ $avatar }}"  class="_coach_avatar" />
    <i class="fas fa-user-tie fltr mrg_10"></i>

	 <h3>
         <a href="{!! route("course.show_teacher_details",["teacher_id"=>$courseTeacher->teacher->id]) !!}">{{ $courseTeacher->teacher->user->full_name }}</a>
                        </h3>
                      
  <div class="col-xl-12 col-lg-12 col-md-12 box_course5 floatright ">
        {!!  $courseTeacher->teacher->short_description !!}
   </div>
</div>
@endforeach