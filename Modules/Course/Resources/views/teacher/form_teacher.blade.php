<div class="row" id="form_teacher">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("course::teacher.".($teacher?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">
                {!! FormHelper::open(['role'=>'form','url'=> (isset($teacher) && $teacher? route('course.teacher.update', ['teacher_id'=>$teacher->id]) : route('course.teacher.save'))
                ,'method'=>(isset($teacher) && $teacher?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                {!! FormHelper::selectTag('user_id',[],old('user_id',$teacher?$teacher->user_id:null),['data-ajax-url'=>route('course.teacher.get_user_by_api')
                     , 'title'=>trans("course::teacher.name"),'helper'=>trans("course::teacher.select_user")]) !!}

                {!! FormHelper::select('education',trans('course::teacher.educations'),old('education',$teacher?$teacher->education_key:null)
                ,['title'=>trans("course::teacher.education"),'helper'=>trans("course::teacher.select_education")
                ,'placeholder'=>trans("course::teacher.select_education")]) !!}

                {{--{!! FormHelper::input('text','teaching_time',old('teaching_time',$teacher?$teacher->teaching_time:null)--}}
                {{--,['title'=>trans("course::teacher.teaching_time").'('.trans("course::teacher.unit_time").')','helper'=>trans("course::teacher.teaching_time")]) !!}--}}

                {!! FormHelper::input('text','location_study',old('location_study',$teacher?$teacher->location_study:null)
                ,['title'=>trans("course::teacher.location_study"),'helper'=>trans("course::teacher.location_study")]) !!}

                {!! FormHelper::input('text','field_study',old('field_study',$teacher?$teacher->field_study:null)
                ,['title'=>trans("course::teacher.field_study"),'helper'=>trans("course::teacher.field_study")]) !!}

                {!! FormHelper::select('active',trans('course::teacher.statuses'),old('active',$teacher?$teacher->active:null)
                ,['title'=>trans("course::teacher.status"),'helper'=>trans("course::teacher.select_status")
                ,'placeholder'=>trans("course::teacher.select_status")]) !!}

                {!! FormHelper::editor('short_description',old('short_description',$teacher?$teacher->short_description:null)
                ,['title'=>trans("course::teacher.short_description"),'helper'=>trans("course::teacher.short_description")]) !!}

                {!! FormHelper::editor('description',old('description',$teacher?$teacher->description:null)
                ,['title'=>trans("course::teacher.description"),'helper'=>trans("course::teacher.description")]) !!}

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("course::teacher.submit")], ['title'=>trans("course::teacher.cancel"), 'url'=>route('course.teacher.index')]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>

