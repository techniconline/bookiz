@php $teacher=$blockViewModel->getTeacher() @endphp
 
	
	
	<div class="FullWidth _course1 floatright ">
    <div class="container  ">
        <h1>
              {{ $teacher->user->full_name }}
        </h1>
    </div>
</div>

<div class="FullWidth teacher100 floatright ">
    <div class="container teacher101 ">
	
	<a class="btn btn-success mybtn_intearch" href='#courseList_' >دوره‌های برگزارشده این استاد</a>
	
	  <img src="{{ $teacher->user->avatar }}" class="Avatar_teacher" >
  <div class="   teacher104 ">
	 <span>
          نام استاد :        {{ $teacher->user->full_name }}
         </span>
		<br>
       <span>
            @lang('course::teacher.education') :  {!! trans("course::teacher.educations.".$teacher->education_key) !!}
          </span>
		  <br>
       <span>
           @lang('course::teacher.field_study') :    {!! $teacher->field_study !!}
        </span>
<br>
		 <span>
          @lang('course::teacher.location_study'):   {!! $teacher->location_study !!}
       </span>
  
    </div>

   <div class="   teacher105 ">
   
	 

	

	  
 
	  {!! $teacher->description !!}
	
	    
	
      </div>
	
   
        </div>
</div>

 

 
<!-- COURSE LIST -->
@php $courses=$blockViewModel->getCourses() @endphp
@if($courses && $courses->count())

	<div class="FullWidth _course1 floatright teacher107">
    <div class="container  " id="courseList_">
        <h4>
       			دور‌‌ه‌های      {{ $teacher->user->full_name }}
        </h4>
    </div>
</div>



    <div class="FullWidth teacher103 floatright ">
        <div class="container    ">
            
			 
           
                    @php $listCourses = $courses @endphp
                    @foreach($listCourses as $item)
					
					
					
				 
                              
									
									 <div class=" col-xl-3 col-md-6 col-sm-6 col-12 floatright mrg10_1 course-box"  >

					  <div class="item col-xl-12 col-md-12 col-sm-12 col-12 floatright">
                        <a href="{{ route('course.view',['id'=>$item->course->id]) }}">
                            <img src="{{ ($item->course->image) }}"  class="_Full100precent2"/>
                        </a>
                        <h3>
                                   <a href="{{ route('course.view',['id'=>$item->course->id]) }}">   {{ $item->course->title }}  </a>
                        </h3>

                        <div class="FullWidth text-align-right fontSize18">
                     
                            <span class="fontSize12 _box008">
                                    <i class="fas fa-user-graduate"></i>   {!! $blockViewModel->getStudents($item->course->id,true) !!}
									
									</span>
						 
									
									 <span class="fontSize12   floatleft  box_008 "> 
								  <i class="fas fa-clock "></i> 
                                   {!! $blockViewModel->getDuration($item->course->id,true) !!}
								  </span>
								  
								  
                                
                            <div class="floatigt fontSize12 FullWidth btn_home_01  ">
                                <a href="{{ route('course.view',['id'=>$item->course->id]) }}">
                                    <i class="fas fa-shopping-cart "></i>
                                    <span >
                              {!! $blockViewModel->getCourseEnrollments($item->course->id, "list") !!}
                                    </span>
                                </a>
                            </div>
                        </div>
                        <br class="CLR"/>
                  
				  
				    </div>
				  
				  
				  </div>
				  
				  
					
              
						
                       {!! BridgeHelper::getRate()->getRateObject()
                                                ->setModel($item->course->getTable(), $item->course->id)
                                                ->setWithData(true)
                                                ->setWithForm(true)
                                                ->setWithSaveRate(false)
                                                ->setWithTextRate(false)
                                                ->setConfigs(["container_js" => "block_js", "container_css" => "block_style"])
                                                ->init()
                                                ->getForm()
                                                !!}
                    @endforeach
          


          
        </div>
    
</div>
@endif

<!-- COURSE LIST -->

