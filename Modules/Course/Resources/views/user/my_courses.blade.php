<div class="col-xl-12 col-lg-12 col-md-12  floatright   dash6  ">
    <div class="dash7">
        <h2>
            @lang('course::course.my_courses')
        </h2>
    </div>
    <div class=" col-xl-12 col-lg-12 col-md-12  _tbl_hover _shop_basket63">

        @if($userCourses->count())
            @foreach($userCourses as $userCourse)
                <div class="col-xl-4 col-lg-4 col-md-6 floatright   ">
                    <div class="col-xl-12 col-lg-12 col-md-12   item">
                        <div class="UnBoxItem">
                            <a href="{{ route('course.view',['id'=>$userCourse->course->id]) }}">
                                <img src="{!! $userCourse->course->image !!}" class="NoRadius with100"/>
                            </a>
                        </div>
                        <h3>
                            <a href="{{ route('course.view',['id'=>$userCourse->course->id]) }}">
                                {{ $userCourse->course->title }}
                            </a>
                        </h3>
                        <div class="FullWidth text-align-right">
                                        <span class="  _box002  ">

                                            <span class="strat_date">
@lang('course::enrollment.active_date')
                                                <br/>
                                                <span>
{{ DateHelper::setDateTime($userCourse->active_date)->getLocaleFormat(trans('core::date.datetime.medium')) }}
                                                </span>

                                            </span>
                                        </span>
                            <span class="  _box001">
                                            <span class="strat_date">
@lang('course::enrollment.expire_date')
                                                <br/>
                                                <span>
{{ DateHelper::setDateTime($userCourse->expire_date)->getLocaleFormat(trans('core::date.datetime.medium')) }}
                                                </span>
                                            </span>
                                        </span>
                            <div class="floatigt   FullWidth btn_home_052 ">
                                <a href="{{ route('course.view',['id'=>$userCourse->course->id]) }}">
                                    @lang('course::course.course_view')
                                </a>
                            </div>
                        </div>
                        <br style="clear:both;"/>
                    </div>
                </div>
            @endforeach
            {!! $userCourses->links() !!}
        @else
            {!! $viewModel->getComponent('message',['info'=>trans('course::course.my_courses_not_find')]) !!}
        @endif
    </div>
</div>
