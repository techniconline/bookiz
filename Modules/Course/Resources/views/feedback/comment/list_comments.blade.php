@if($comments = $viewModel->getDataComments())
    <div id="list_comments">
        @foreach($comments as $index => $comment)
            <div class="  col-lg-12 col-md-12 col-sm-12 col-xs-12 pad_btn_10 pad_top_10  mrg_05 fltr text-justify dirr core9 lineH1-5">
                <!--<a href="#" class="btn_ywllo fltl">پاسخ </a>-->
                <h5 class="fltr">
                    {!! $comment->name !!}
                </h5>
                <h6 class="fltr">
                    {!! DateHelper::setDateTime($comment->created_at)->getLocaleFormat(trans('core::date.datetime.medium')); !!}
                </h6>
                <hr>
                {!! $comment->comment !!}
            </div>
        @endforeach
        <div class="pagination">{!! $comments->links() !!}</div>
    </div>

@endif