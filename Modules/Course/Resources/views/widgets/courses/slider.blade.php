{!! $blockViewModel->getConfig('before_html') !!}

<div class="FullWidth">
    <div class="container titlelogo ">
        <h6>
            {{ $blockViewModel->getConfig('title') }}
        </h6>
    </div>
    <div class="container-fluid">
        <section class="demos">
            <div class="container">
                <div class="owl-carousel owl-theme">
                    @foreach($blockViewModel->getCoursesList() as $course)
                        <div class="item">
                            <a href="{{ route('course.view',['id'=>$course->id]) }}">
                                <img src="{!! $course->image !!}"/>
                            </a>
                            <h3>
                                <a href="{{ route('course.view',['id'=>$course->id]) }}">
                                    {{ $course->title }}
                                </a>
                            </h3>

                            <div class="FullWidth text-align-right">
                          
                            <span class="fontSize12 _box008">

                                @if($students = $blockViewModel->getStudents($course->id,true))
                                    <i class="fas fa-user-graduate"></i>
                                    {{ $students  }}
                                @endif

                            </span>


                                <span class="fontSize12   floatleft  box_008 ">
								  <i class="fas fa-clock "></i>
                                    {{ $blockViewModel->getDuration($course->id,true) }}
								  </span>

                                <div class="floatigt fontSize12 FullWidth btn_home_01  ">
                                    <a href="{{ route('course.view',['id'=>$course->id]) }}">
                                        <i class="fas fa-shopping-cart "></i>
                                        <span>
                                   {!! $blockViewModel->getCourseEnrollmentsHtml($course->id) !!}
                                    </span>
                                    </a>
                                </div>
                            </div>
                            <br style="clear:both;"/>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
</div>
{!! $blockViewModel->getConfig('after_html') !!}