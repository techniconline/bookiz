@if($coursesOnRelation=$blockViewModel->getCourseRelations())
    {!! $blockViewModel->getConfig('before_html') !!}
    @if(isset($coursesOnRelation['before_course']) || isset($coursesOnRelation['after_course']) )
        <div class="floatright col-xl-12 col-lg-12 col-md-12 box_course4 noborarde box_course41  _course_relations">

            <div class="FullWidth ">
        @if(isset($coursesOnRelation['before_course']) && !isset($coursesOnRelation['after_course']))
                   <div class=" col-xl-12 col-md-12 col-sm-12  floatright  bx-04 ">
                          @php $course=$coursesOnRelation['before_course'][0]  @endphp
                        <div class=" _course_relation FullWidth">
                            <div class=" floatright NExt_  Yellow floatright ">
                            <img src="/themes/default/assets/img/icon11.png"  class="NExt_img imgno-2 ">
                        </div>
                                    <div class="col-xl-4 col-md-5 col-sm-5  floatright  Relation_css_text  d-none  d-sm-block">
                                        <a href="{{ route('course.view',['id'=>$course->id]) }}">
                                            <img src="{!! $course->image !!}"/>
                                        </a>
                                    </div>

                                    <div class="col-xl-7 col-md-5 col-sm-6 col-9 floatright  flex-maker ">
                                        <a href="{{ route('course.view',['id'=>$course->id]) }}">
                                    {{ $blockViewModel->text_truncate($course->title,90) }}  
						 
                                        </a>

                                    </div>
                               
                            
                       
                      
                    </div>
                    </div>
        @elseif(isset($coursesOnRelation['after_course']) && !isset($coursesOnRelation['before_course']))
                    <div class=" col-xl-12 col-md-12 col-sm-12  floatright  bx-04 ">
                          @php $course=$coursesOnRelation['after_course'][0]  @endphp
               <div class=" _course_relation FullWidth">
                         
                                  <div class=" floatleft Pre_ ">
                            <img src="/themes/default/assets/img/icon12.png"  class="NExt_img img2">
                        </div>

                                   <div class="col-xl-4 col-md-5 col-sm-5  floatright  Relation_css_text  d-none  d-sm-block">
                                        <a href="{{ route('course.view',['id'=>$course->id]) }}">
                                            <img src="{!! $course->image !!}"/>
                                        </a>
                                    </div>

                                         <div class="col-xl-7 col-md-5 col-sm-6 col-9 floatright  flex-maker ">
                                        <a href="{{ route('course.view',['id'=>$course->id]) }}">
                                            {{ $blockViewModel->text_truncate($course->title,90) }}
                                        </a>

                                    </div>
                                  
                               
                            
                        </div>
                    </div>
        @elseif(isset($coursesOnRelation['after_course']) && isset($coursesOnRelation['before_course']))
                    <div class=" col-xl-6 col-md-6 col-sm-12 col-12 floatright  bx-04    ">
                       
                        @php $course=$coursesOnRelation['before_course'][0]  @endphp
                        <div class=" _course_relation FullWidth">
                            <div class=" floatright NExt_  Yellow floatright ">
                            <img src="/themes/default/assets/img/icon11.png"  class="NExt_img imgno-2 ">
                        </div>
                                    <div class="col-xl-4 col-md-5 col-sm-5  floatright  Relation_css_text  d-none  d-sm-block">
                                        <a href="{{ route('course.view',['id'=>$course->id]) }}">
                                            <img src="{!! $course->image !!}"/>
                                        </a>
                                    </div>

                                    <div class="col-xl-7 col-md-5 col-sm-6 col-9 floatright  flex-maker ">
                                        <a href="{{ route('course.view',['id'=>$course->id]) }}">
                                    {{ $blockViewModel->text_truncate($course->title,90) }}  
						 
                                        </a>

                                    </div>
                               
                            
                       
                      
                    </div>
					    </div>
                    <div class=" col-xl-6 col-md-6 col-sm-12 col-12 floatright bx-03 ">
                     
                        @php $course=$coursesOnRelation['after_course'][0]  @endphp
               <div class=" _course_relation FullWidth">
                         
                                  <div class=" floatleft Pre_ ">
                            <img src="/themes/default/assets/img/icon12.png"  class="NExt_img img2">
                        </div>

                                   <div class="col-xl-4 col-md-5 col-sm-5  floatright  Relation_css_text  d-none  d-sm-block">
                                        <a href="{{ route('course.view',['id'=>$course->id]) }}">
                                            <img src="{!! $course->image !!}"/>
                                        </a>
                                    </div>

                                         <div class="col-xl-7 col-md-5 col-sm-6 col-9 floatright  flex-maker ">
                                        <a href="{{ route('course.view',['id'=>$course->id]) }}">
                                            {{ $blockViewModel->text_truncate($course->title,90) }}
                                        </a>

                                    </div>
                                  
                               
                            
                        </div>
                    </div>
        @endif
            </div>
        </div>
    @endif

    @if(isset($coursesOnRelation['normal']))
        <div class="col-xl-12 col-lg-12 col-md-12  floatright    box_course10 merg_20">
            <div class="floatright box_course31">
                <h6> @lang('course::course.course_relation.course_list')‌ </h6>
            </div>
        </div>

        <div class="floatright col-xl-12 col-lg-12 col-md-12 box_course4 box_course41  _course_relations">
            <div class="FullWidth _course_relations">
                @foreach($coursesOnRelation['normal'] as $course)
                    <div class=" _cou	rse_relation     col-xl-4 col-md-6 col-sm-6  floatright   ">
                        <div class="row">
                            <div class=" Relation_css ">
 
                                <div class="col-xl-6 col-md-6 col-sm-6  floatright  Relation_css_text ">
                                    <a href="{{ route('course.view',['id'=>$course->id]) }}">
                                        <img src="{!! $course->image !!}"/>
                                    </a>
                                </div>

                                <div class="col-xl-6 col-md-6 col-sm-6  floatright Pad_left5px ">
                                        <a href="{{ route('course.view',['id'=>$course->id]) }}">
                                            {{ $blockViewModel->text_truncate($course->title,90) }}
                                        </a>

                                </div>
                                <br style="clear:both;"/>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
    {!! $blockViewModel->getConfig('after_html') !!}
@endif

