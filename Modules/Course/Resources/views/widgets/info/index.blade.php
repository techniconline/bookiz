﻿@if($blockViewModel->isEnroll())
    @php $userEnrollments=$blockViewModel->getUserEnrollmentCourses() @endphp
        @if($userEnrollments)
            <ul class="list-group">
                <li class="items-activity-title">
                    <i class="fas fa-bars bc_file2 "></i>
                    <span> @lang('course::enrollment.my_courses') ‌ </span>
                </li>
                @foreach( $userEnrollments as $enrollment)
                    <li class="list-group-item {{ $blockViewModel->getConfig('row_class') }}">
                        <a class="" href="{{ route('course.view',['id'=>$enrollment->course->id]) }}">
                            {{ $enrollment->course->title }}
                            <span></span>
                        </a>
                    </li>
                @endforeach
            </ul>
        @else
            @include('core::component.message',['info'=>trans('course::enrollment.empty_my_courses')])
        @endif
        <div class="clearfix"><hr></div>

@else
    <div class="{{ $blockViewModel->getConfig('class_div') }} info-block">
        {!! $blockViewModel->getConfig('before_html') !!}
        <span class="{{ $blockViewModel->getConfig('icon_parent_class') }}"><i
                    class="{{ $blockViewModel->getConfig('title_icon') }}"></i></span>
        <h5 class="{{ $blockViewModel->getConfig('title_class') }}">{{ $blockViewModel->getConfig('title') }}</h5>
        <div class="{{ $blockViewModel->getConfig('class_sub_div') }}">
            @foreach($blockViewModel->getFields()  as $field)
                @if(!empty($field->value))
                <h6 class="{{ $blockViewModel->getConfig('row_class') }}">
                    {{ $field->label }}
                    <span class="{{$blockViewModel->getConfig('row_label_class')}}">{!! $field->value !!}</span>
                </h6>
                @endif
            @endforeach
        </div>

        <div class="{{ $blockViewModel->getConfig('separator_class') }}">&nbsp;</div>
        @if($blockViewModel->getConfig('show_enrollment'))
            <div class="{{ $blockViewModel->getConfig('enrollment_class') }} ">
                {!! $blockViewModel->getCourseEnrollmentsHtml() !!}
            </div>
        @endif
        <div class="info-content">
            {!! $blockViewModel->getConfig('after_html') !!}
        </div>
    </div>
	
@endif
