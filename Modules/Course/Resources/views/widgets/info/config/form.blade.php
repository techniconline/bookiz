{!! FormHelper::input('text','title',old('title',$blockViewModel->getData('title')),['title'=>trans('course::form.fields.title'),'helper'=>trans('course::form.helper.title')]) !!}
{!! FormHelper::selectTag('fields[]',$blockViewModel->getFieldsList(),old('fields',$blockViewModel->getData('fields'))
,['title'=>trans("course::form.fields.fields"),'multiple'=>'multiple','helper'=>trans("course::form.fields.fields")]) !!}
{!! FormHelper::checkbox('show_enrollment',1,old('show_enrollment',$blockViewModel->getData('show_enrollment',1)),['title'=>trans('core::form.fields.show_enrollment'),'label'=>trans('core::form.helper.show_enrollment')]) !!}
