<li class="{{ $blockViewModel->getConfig($pre.'class_li') }}">
    <a class="{{ $blockViewModel->getConfig($pre.'class_link') }}"
       href="{{ $blockViewModel->getCourseCategoryUrl($course_category) }}">
        {{ $course_category["title"] }}
    </a>
    @if(isset($course_category["childes"]) && $course_category["childes"]->count())
        <ul class="submenu">
            @foreach($course_category["childes"] as $course_category)
                @include('course::widgets.course_category.subcourse_category',['pre'=>$pre])
            @endforeach
        </ul>
    @endif
</li>