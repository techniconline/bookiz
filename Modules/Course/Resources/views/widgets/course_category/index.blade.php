{!! $blockViewModel->getConfig('before_html') !!}
<div class="{{ $blockViewModel->getConfig('class_div') }}" id="{{ $blockViewModel->getConfig('id') }}">
    <ul class="{{ $blockViewModel->getConfig('class_ul') }}">
	 
        @foreach($blockViewModel->getCourseCategories() as $course_category)
            @include('course::widgets.course_category.subcourse_category',['pre'=>''])
        @endforeach
    </ul>
    @if($blockViewModel->getConfig('mobile_course_category'))
    <button data-toggle="collapse" data-target="#{{$blockViewModel->getConfig('id')}}" class="hidden-lg hidden-md TowerCollMenu">
        {{ $blockViewModel->getConfig('mobile_title') }}<i class="fas fa-sort-down"> </i>
    </button>
    <div id="top-menu" class="{{ $blockViewModel->getConfig('mobile_class_div') }}">
        <ul class="{{ $blockViewModel->getConfig('mobile_class_ul') }}">
            @foreach($blockViewModel->getCourseCategories() as $course_category)
                @include('course::widgets.course_category.subcourse_category',['pre'=>'mobile_'])
            @endforeach
			
        </ul>
    </div>
    @endif
</div>
{!! $blockViewModel->getConfig('after_html') !!}