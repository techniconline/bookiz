<div class="col-md-12" id="core.instance.config">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cog"></i>
                {{ $BlockViewModel->getTitlePage() }}
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('course.config.instance.save'),'method'=>'POST','class'=>'form-horizontal','id'=>'config_form','enctype'=>"multipart/form-data"]) !!}
            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp
            {!! FormHelper::legend(trans('course::form.titles.defaults')) !!}
            {!! FormHelper::inputByButton('default_poster',old('default_poster',$BlockViewModel->getSetting('default_poster')),['type'=>'image','title'=>trans('course::form.fields.default_poster'),'helper'=>trans('core::form.helper.image_url')],trans('course::form.fields.default_poster'),[],true) !!}
            {!! FormHelper::select('default_time_unit',['minute'=>trans('course::course.info_unit.minute'),'hour'=>trans('course::course.info_unit.hour')],old('default_time_unit',$BlockViewModel->getSetting('default_time_unit')),['title'=>trans('course::form.fields.default_time_unit'),'helper'=>trans('course::form.helper.default_time_unit')]) !!}
            {!! FormHelper::selectTag('default_feature_group',[],old('default_feature_group',$BlockViewModel->getSetting('default_feature_group')),['data-ajax-url'=>route('core.feature.group.json'),'title'=>trans('course::form.fields.feature_group'),'helper'=>trans('course::form.helper.feature_group')]) !!}
            {!! FormHelper::checkbox('show_enrollment_on_course',1,old('show_enrollment_on_course',$BlockViewModel->getSetting('show_enrollment_on_course')),['title'=>trans('core::form.fields.show_enrollment_on_course'),'label'=>trans('core::form.helper.show_enrollment_on_course')]) !!}
            {{--{!! FormHelper::legend(trans('course::form.titles.info_defaults')) !!}--}}
            {{--{!! FormHelper::input('text','title',old('title',$BlockViewModel->getSetting('title')),['title'=>trans('course::form.fields.title'),'helper'=>trans('course::form.helper.title')]) !!}--}}
            {{--{!! FormHelper::selectTag('fields[]',$BlockViewModel->getFieldsList(),old('fields',$BlockViewModel->getSetting('fields'))--}}
{{--,['title'=>trans("course::form.fields.fields"),'multiple'=>'multiple','helper'=>trans("course::form.fields.fields")]) !!}--}}
            {{--{!! FormHelper::checkbox('show_enrollment',1,old('show_enrollment',$BlockViewModel->getSetting('show_enrollment',1)),['title'=>trans('core::form.fields.show_enrollment'),'label'=>trans('core::form.helper.show_enrollment')]) !!}--}}
            {{--{!! FormHelper::input('text','class_div',old('class_ul',$BlockViewModel->getSetting('class_div')),['title'=>trans('core::form.fields.class_div'),'helper'=>trans('core::form.helper.class_div')]) !!}--}}
            {{--{!! FormHelper::textarea('before_html',old('before_html',$BlockViewModel->getSetting('before_html')),['title'=>trans('core::form.fields.before_html'),'helper'=>trans('core::form.helper.before_html'),'class'=>'direction-ltr']) !!}--}}
            {{--{!! FormHelper::textarea('after_html',old('after_html',$BlockViewModel->getSetting('after_html')),['title'=>trans('core::form.fields.after_html'),'helper'=>trans('core::form.helper.after_html'),'class'=>'direction-ltr']) !!}--}}
            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitOnly() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>