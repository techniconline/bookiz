<div class="row" id="form_feature">
    <div class="col-md-12">
        <a href="{!! route('course.index') !!}" class="btn btn-lg green circle-right margin-bottom-5"><i
                    class="fa fa-list"></i> @lang("course::course.list") </a>
        <div class="portlet box blue " style="display: none">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-course-camera"></i> <i class="fa fa-tags"></i> @lang("course::course.edit_meta_tags")
                    : {!! $course->title or null  !!}
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">
                {{--<form role="form"--}}
                {{--action="{!! isset($course) && $course? route('course.update', ['course_id'=>$course->id]) : route('course.save') !!}"--}}
                {{--method="POST">--}}
                {{--<input type="hidden" name="_method" value="{!! isset($course) && $course?'PUT':'POST' !!}">--}}
                {{--{!! csrf_field() !!}--}}

                <div class="form-body">
                    <div class="form-group">

                        @if(isset($metaGroups)&&$metaGroups)
                            <div class="row">

                                <div class="col-md-12">
                                    @foreach($metaGroups as $metaGroup)

                                        <div class="col-md-3">
                                            @php
                                              $checked = boolval(in_array($metaGroup->id,$metaGroupsActive));
                                            @endphp
                                            {!! FormHelper::checkbox('meta_group_ids[]',$metaGroup->id,$checked
                                            ,['title'=>$metaGroup->title,'disabled'=>'disabled']) !!}

                                        </div>

                                    @endforeach

                                </div>

                            </div>

                        @endif

                    </div>
                </div>
                {{--</form>--}}

            </div>


        </div>

        @if(isset($course) && $course && !empty($metaGroupsActive))

            @foreach($courseMetaGroups as $courseMetaGroup)

                <div class="portlet box blue-dark" style="margin: 5px">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-tags"></i> {!! $courseMetaGroup->group_name !!}
                        </div>
                        <div class="tools">
                            <a href="" class="expand"> </a>
                            {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}

                            <a href="{!! route('course.meta.copy',['course_id'=>$course->id, 'course_meta_group_id'=>$courseMetaGroup->id]) !!}"
                               class="_copy_course_meta_group"
                               title="@lang("course::course.copy_meta_group"): {!! $courseMetaGroup->group_name !!}"
                               data-confirm-message="@lang("course::course.copy_meta_group"): {!! $courseMetaGroup->group_name !!} ?">
                                <i class="fa fa-copy" style="color: white; font-size: 24px"></i> </a>

                            <a href="{!! route('course.meta.delete',['course_id'=>$course->id, 'course_meta_group_id'=>$courseMetaGroup->id]) !!}"
                               class="_del_course_meta_group"
                               title="@lang("course::course.del_meta_group"): {!! $courseMetaGroup->group_name !!}"
                               data-confirm-message="@lang("course::course.del_meta_group"): {!! $courseMetaGroup->group_name !!} ?">
                                <i class="fa fa-remove" style="color: orangered; font-size: 24px"></i> </a>
                        </div>
                    </div>

                    <div class="portlet-body form" style="display: none;">

                    {!! FormHelper::open(['role'=>'form','url'=>route('course.meta.features.value.save', ['course_id'=>$course->id, 'course_meta_group_id'=>$courseMetaGroup->id])
                                ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                    @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                            @foreach($courseMetaGroup->metaGroup->featureGroups as $featureGroup)

                                @foreach($featureGroup->featureGroupRelations as $featureGroupRelation)

                                    @if($featureGroupRelation->feature)

                                        <?php

                                        $activeValue = null;
                                        if ($metaFeatureDataList && isset($metaFeatureDataList[$courseMetaGroup->id][$featureGroupRelation->feature->id])) {
                                            $activeValue = $metaFeatureDataList[$courseMetaGroup->id][$featureGroupRelation->feature->id];
                                        }

                                        ?>

                                        {!!
                                            $view_model->setFeatureGroup($featureGroup)
                                            ->setFeatureModel($featureGroupRelation->feature)
                                            ->setActiveValueFeature($activeValue)
                                            ->getHtml()
                                        !!}

                                    @endif

                                @endforeach

                            @endforeach


                        {!! FormHelper::openAction() !!}
                        {!! FormHelper::submitByCancel(['title'=>trans("course::course.submit"),'class'=>'btn btn green _save_meta'], ['title'=>trans("course::course.cancel"), 'url'=>'#']) !!}
                        {!! FormHelper::closeAction() !!}
                        {!! FormHelper::close() !!}


                    </div>
                </div>

            @endforeach

            <hr>

        @endif

    </div>

</div>

<div>

</div>
