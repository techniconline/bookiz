@php $course=$blockViewModel->getCourse() @endphp
<div class="FullWidth _course1 floatright ">
    <div class="container  ">
        <div class='Coursename'>
            <h1>
                {{ $course->title }}
            </h1>
            <div id="activity_title">
            </div>
        </div>
    </div>
</div>

<div class="container  ">
    <div class="{!! ($blockViewModel->isEnroll())?'col-xl-12 col-lg-12 col-md-12':'col-xl-9 col-lg-8 col-md-8 '; !!} floatright    DIR_ch">
        <div class="_course2">
            @if(isset($activity_view) && $activity_view)
                {!! $activity_view !!}
            @else
                <div class="_course16"><img src="{{ $course->image }}" alt="{{ $course->title }}"></div>
            @endif
        </div>

        <div class="col-xl-12 col-lg-12 col-md-12  floatright box_a_course" id="btn_box_course">

            <div class="col-xl-6 col-lg-5 col-md-5 floatright H_mobile">
                <div class="inside_boxer"><a href="#ex_lock"><i class="fas fa-file-alt"></i>
                        <h5>   @lang('course::course.description') </h5></a></div>

                <div class="inside_boxer"><a href="#session_sec"><i class="fas fa-bars"></i>
                        <h5>    @lang('course::course.activity.sessions')  </h5></a></div>
                <div class="inside_boxer"><a href="#Coach_sec"><i class="fas fa-user-graduate"></i>
                        <h5>   @lang('course::course.teacher_info')</h5></a></div>

                @if($blockViewModel->canAccessComment())
                    <div class="inside_boxer"><a href="#Comment_Sec"><i class="fas fa-comment"></i>
                            <h5>   @lang('course::course.comments')     </h5></a></div>
                @endif
            </div>


            <div class="col-xl-6 col-lg-7 col-md-7 floatright">

                @if(BridgeHelper::getConfig()->getSettings('show_enrollment_on_course','instance','course'))
                    <div class="newbox14">
                        {!! $blockViewModel->getCourseEnrollments() !!}
                    </div>
                @endif
                @if($blockViewModel->isEnroll())
                    <div class="row floatright box_a_course box_a_course2 course_actions_btns">
                        <div class="col-xl-4 col-lg-4 col-md-4 floatright">
                            <button type="button" class="btn btn-warning leftbx1 previous_activity activity-ajax"
                                    data-current-id="{!! $current_activity !!}" data-index="0" disabled>
                                <i class="fas fa-caret-right"></i>
                            @lang('course::course.previous_activity')
                            <!--  <br>
                    <span>-</span> -->
                            </button>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4  mainKey1 floatright">
                            <a href="#session_sec" class="btn btn-default ">
                                لیست جلسات
                            </a>
                        </div>


                        <div class="col-xl-4 col-lg-4 col-md-4  floatright  box_a_course3">
                            <button type="button" class="btn btn-success leftbx2 next_activity activity-ajax"
                                    data-current-id="{!! $current_activity !!}" data-index="0" disabled>
                                <i class="fas fa-caret-left"></i>
                            @lang('course::course.next_activity')
                            <!--  <br>
                    <span>-</span> -->
                            </button>
                        </div>
                    </div>
                @endif

            </div>


        </div>
    </div>
    @if(!$blockViewModel->isEnroll())
        <div class="col-xl-3 col-lg-4 col-md-4  floatleft_important bgg5  DIR_ch    ">

            {{-- <div class="DIR_ch">
                     <ul class="list-group">
                         <li class="items-activity-title">
                             <i class="fas fa-bars bc_file2 "></i>
                             <span> @lang('course::course.activity.sessions') ‌ </span>
                         </li>
                         @if($course->courseSections->count())
                             @foreach($course->courseSections as $section)
                                 @if($blockViewModel->canShowSections())
                                     <li class="list-group-item items-activity-section-title">
                                         <i class="fas fa-minus"></i>
                                         <span>  @lang('course::course.activity.section') {{ $loop->iteration }} @if($blockViewModel->canShowSectionTitle())
                                                 - {{ $section->title }} @endif </span>
                                     </li>
                                 @endif
                                 @php $activities=$course->courseActivities->where('course_section_id',$section->id)->where('active',1)->sortBy('sort'); @endphp
                                 @if($activities->count())
                                     @foreach($activities as $activity)
                                         @if($blockViewModel->hasActivityAccess($activity))
                                             <li class="list-group-item items-activity-item">
                                                 <a href="{{ route('course.activity',['course_id'=>$activity->course_id,'activity_id'=>$activity->id]) }}"
                                                    class="activity-ajax" data-id="{{$activity->id}}">
                                                     <i class="fas fa-play"></i>
                                                     <span> {{ $activity->title }}</span>
                                                 </a>
                                             </li>
                                         @endif
                                     @endforeach

                                 @endif
                             @endforeach
                         @endif
                     </ul>
            </div>--}}
            <div class="clearfix"></div>

            {!! $blockViewModel->getWidget('course_info_block',[
            'show_enrollment'=>1,
             "fields"=> [
               "duration",
               "students",
               "section",
               "sessions",
               "quiz",
               "teachers"
             ], ]   ); !!}

            <div class="_shareMedia">
                <span>@lang('course::course.socials_title')</span>
            </div>
            <div class="_shareMedia">
                <br>
                <a href="#"><i class="fab fa-facebook-square"></i></a>
                <a href="#"><i class="fab fa-twitter"></i></a>
                <a href="#"><i class="fab fa-linkedin-in"></i></a>
                <a href="#"><i class="fab fa-telegram-plane"></i></a>
                <a href="#"><i class="fab fa-google-plus-g"></i></a>
            </div>
        </div>
    @endif
    @if($blockViewModel->isEnroll())
        <div class="col-xl-12 col-lg-12 col-md-12  floatright_important  ">
            @else
                <div class="col-xl-9 col-lg-8 col-md-8  floatright_important  ">
                    @endif

                    {!! BridgeHelper::getBlockHelper()->getBlock('course_relations_block')->render() !!}


                    <div class="col-xl-12 col-lg-12 col-md-12  floatright    box_course2" id="ex_lock">
                        <div class="floatright box_course3">
                            <i class="fas fa-file-alt bc_file "></i>
                            <h6>   @lang('course::course.description') </h6>
                        </div>
                    </div>


                    <div class="floatright col-xl-12 col-lg-12 col-md-12 box_course4">
                        {!! $course->description !!}


                    </div>

                    <!--                       SECOND SECTION                           -->

                    <div class="col-xl-12 col-lg-12 col-md-12  floatright box_course2" id="session_sec">
                        <div class="floatright box_course3">
                            <i class="fas fa-bars bc_file2 "></i>
                            @if($course->type=='normal')
                                <h6>  @lang('course::course.activity.sessions') ‌ </h6>
                            @elseif($course->type=='package')
                                <h6>  @lang('course::course.list') ‌ </h6>
                            @endif
                        </div>
                    </div>

                    <div class="floatright col-xl-12 col-lg-12 col-md-12 box_course4">
                        <div class="col-xl-12 col-lg-12 col-md-12 all-activity">

                            @if($course->type=='normal' && $course->courseSections->count())

                                @foreach($course->courseSections as $section)
                                    @php
                                        $activities=$section->courseActivities;
                                    @endphp

                                    @if($blockViewModel->canShowSections())
                                        <button type="button" class="_faq3" data-toggle="collapse"
                                                data-target="#Ul{{ $loop->index}}"
                                                aria-expanded="{!! ($blockViewModel->canOpenSection($loop->iteration))?'true':'false' !!}">

                                            @if($activities->count())
                                                <i class="fas fa-plus"></i>
                                                <i class="fas fa-minus"></i>
                                            @endif

                                            {{ $blockViewModel->getSectionTitle($section->title,$loop->iteration) }}
                                        </button>
                                    @endif


                                    <div id="Ul{{ $loop->index}}"
                                         class="collapse box_course8 table_Respons {!! ($blockViewModel->canOpenSection($loop->iteration) || !$blockViewModel->canShowSections())?'show':'' !!}">
                                        <table class="table table-striped HiddenUnder500">

                                            <tbody>
                                            @if($activities->count())
                                                @foreach($activities as $activity)
                                                    @php $activityObject=$blockViewModel->getActivityObject($activity) @endphp
                                                    <tr>
                                                        <td>
                                                            @if($blockViewModel->hasActivityAccess($activity))
                                                                <a href="{{ route('course.activity',['course_id'=>$activity->course_id,'activity_id'=>$activity->id]) }}"
                                                                   class="{{ $activityObject->canUseAjax()?'activity-ajax':'' }} activity-items"
                                                                   data-id="{{$activity->id}}"
                                                                   id="activity-id-{{$activity->id}}">
                                                            @endif

                                                            <div class="   col-xl-7 col-md-12 col-12 floatright padd_tbl1 ">
                                                                <i class="fas {{ $activityObject->getIcon() }}"></i>
                                                                        {{ $activity->title }}
                                                            </div>
                                                            <div class="   col-xl-2 col-md-3 col-12 floatright padd_tbl1 center-me-1">
                                                                {{ $blockViewModel->getActivityDuration($activity->duration) }}
                                                            </div>
                                                            <div class="   col-xl-3 col-md-3 col-12 floatright padd_tbl1 center-me-2 ">

                                                                <span class="green_me floatright">  @if($price=$blockViewModel->getActivityPrice($activity,true))   {{ $price }}   @endif </span>


                                                                @if($blockViewModel->hasActivityAccess($activity))

                                                                    <i class="fas {{ $activityObject->getActiveIcon() }} green_me acitivity_clicable" data-id="activity-id-{{$activity->id}}"></i>

                                                                @else


                                                                    @if($blockViewModel->canSaleActivity($activity))
                                                                        {!! $blockViewModel->getActivityCartBtn($activity) !!}
                                                                    @else
                                                                        <i class="fas fa-lock"></i>



                                                                    @endif

                                                                @endif

                                                            </div>
                                                                    @if($blockViewModel->hasActivityAccess($activity))
                                                                </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif

                                            </tbody>
                                        </table>


                                    </div>


                                @endforeach

                            @elseif($course->type=='package')
                                @if($course->coursePackageEnrollment->count())

                                    @foreach($course->coursePackageEnrollment as $coursePackageEnrollment)
                                        <div id="Ul{{ $loop->index}}"
                                             class="box_course8 table_Respons ">
                                            <table class="table table-striped HiddenUnder500">

                                                <tbody>
                                                <tr>
                                                    <td>

                                                        <div class="col-xl-7 col-md-12 col-12 floatright padd_tbl1 ">
                                                            <img width="200px"
                                                                 src="{{ $coursePackageEnrollment->coursePackage->image }}"
                                                                 alt="{{ $coursePackageEnrollment->coursePackage->title }}">

                                                            <a href="{{ route('course.view',['course_id'=>$coursePackageEnrollment->package_course_id]) }}"
                                                               class="text-success "
                                                               data-id="{{$coursePackageEnrollment->course_id}}">
                                                                {{ $coursePackageEnrollment->coursePackage->title }}
                                                            </a>
                                                            <div>
                                                                {{ $coursePackageEnrollment->coursePackage->short_description }}
                                                            </div>
                                                        </div>


                                                        <div class="col-xl-5 col-md-5 col-12 floatright padd_tbl1 center-me-2 ">

                                                            <div class="newbox14">
                                                                {!! $blockViewModel->getCourseEnrollments($coursePackageEnrollment->coursePackage->id) !!}
                                                            </div>

                                                        </div>

                                                    </td>
                                                </tr>
                                                </tbody>

                                            </table>
                                        </div>

                                    @endforeach

                                @endif

                            @else
                                {!! $blockViewModel->getComponent('message',['info'=>trans('course::course.message.no_sections')],'core'); !!}

                            @endif


                        </div>

                    </div>


                    <!--                       THIRD SECTION                           -->


                    <div class="col-xl-12 col-lg-12 col-md-12  floatright    box_course2" id="Coach_sec">
                        <div class="floatright box_course3">
                            <i class="fas fa-user-graduate bc_file2 "></i>
                            <h6>     @lang('course::course.teacher_info')‌ </h6>
                        </div>
                    </div>

                    <div class="floatright col-xl-12 col-lg-12 col-md-12 box_course4">
                        <div class="col-xl-12 col-lg-12 col-md-12">

                            @if($course->courseTeachers->count())
                                {!! $blockViewModel->getCourseTeachers($course->id) !!}
                            @else
                                {!! $blockViewModel->getComponent('message',['info'=>trans('course::course.message.no_teachers')],'core'); !!}
                            @endif


                        </div>

                    </div>


                    <!--                       QTOA SECTION                           -->


                    @if($blockViewModel->canShowQtoaSection())

                        {!! BridgeHelper::getQtoa()->getQtoaObject($course->getTable(), $course->id,  ['with_form_qtoa'=>true, 'with_data_questions'=>true,"container_js" =>"block_js","container_css" => "block_css" ])->getQtoa() !!}

                    @endif


                <!--                       FOURTH SECTION                           -->

                    @if($blockViewModel->canAccessComment())
                        <div class="col-xl-12 col-lg-12 col-md-12  floatright    box_course2" id="Comment_Sec">
                            <div class="floatright box_course3">
                                <i class="fas fa-comment bc_file2 "></i>
                                <h6>  @lang('course::course.comments') </h6>
                            </div>
                        </div>

                        <div class="floatright col-xl-12 col-lg-12 col-md-12 box_course4">


                    {!!  BridgeHelper::getComment()->getCommentObject()->setModel($course->getTable(), $course->id)
                  ->setWithData(true)
                  ->setWithForm(true)
                  ->setConfigs(["perPage" => 15, "container_js" =>
                  "block_js",
                  "container_css" => "block_css" ,
                  "template" => "course::feedback.comment.form_comment" ,
                  "template_list" => "course::feedback.comment.list_comments"])
                  ->init()
                  ->getForm() !!}


                        </div>
                    @endif


                <!--                       FIVE SECTION                           -->

                    @if($blockViewModel->canAccessRate())
                        <div class="col-xl-12 col-lg-12 col-md-12  floatright    box_course2 merg_20">
                            <div class="floatright box_course3">

                                <i class="fas fa-star-half-alt bc_file1"></i>


                                <h6> @lang('course::course.rates')‌ </h6>
                            </div>
                        </div>

                        <div class="floatright col-xl-12 col-lg-12 col-md-12 box_course4">

                            <div class="col-xl-12 col-lg-12 col-md-12  _staring1 _H6_str1">

                                <div class="floatright col-xl-12 col-lg-12 col-md-12 _staring2">


                                    {!!      BridgeHelper::getRate()->getRateObject()
                                        ->setModel($course->getTable(), $course->id)
                                        ->setWithData(true)
                                        ->setWithForm(true)
                                        ->setWithSaveRate(true)
                                        ->setWithTextRate(true)
                                        ->setConfigs(["container_js" => "block_js", "container_css" => "block_style"])
                                        ->init()
                                        ->getForm()
                                    !!}


                                </div>
                            </div>
                        </div>

                    @endif


                </div>
                <br class="CLR"/>
        </div>
</div>

<script>
    var Loaded_Names = [];
    $(document).ready(function () {

        function activity_action_set(current_activity) {
            if (!$(".next_activity,.previous_activity").length) {
                return false
            }
            current_activity = '#activity-id-' + current_activity;
            var listItem = $(".all-activity a.activity-items");
            var currentIndex = listItem.index($(current_activity));
            var next = (currentIndex + 1);
            var prev = (currentIndex - 1);
            if (listItem[next]) {
                title = $(listItem[next]).text().trim();
                $(".next_activity").attr('data-index', next).removeAttr('disabled');
                $(".next_activity span").html(title);
            } else {
                $(".next_activity span").html('-');
                $('.next_activity').attr('disabled', 'disabled');
            }
            if (listItem[prev]) {
                title = $(listItem[prev]).text().trim();
                $(".previous_activity").attr('data-index', prev).removeAttr('disabled');
                $(".previous_activity span").html(title);
            } else {
                $('.previous_activity').attr('disabled', 'disabled');
                $(".previous_activity span").html('-');
            }
        }

        var loader = '' +
            '<div class="sk-cube-grid">\n' +
            '  <div class="sk-cube sk-cube1"></div>\n' +
            '  <div class="sk-cube sk-cube2"></div>\n' +
            '  <div class="sk-cube sk-cube3"></div>\n' +
            '  <div class="sk-cube sk-cube4"></div>\n' +
            '  <div class="sk-cube sk-cube5"></div>\n' +
            '  <div class="sk-cube sk-cube6"></div>\n' +
            '  <div class="sk-cube sk-cube7"></div>\n' +
            '  <div class="sk-cube sk-cube8"></div>\n' +
            '  <div class="sk-cube sk-cube9"></div>\n' +
            '</div>';
        // Add smooth scrolling to all links
        $(".activity-ajax").on('click', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            if (!href || href == '#') {
                return false;
            }
            $("._course2").html(loader);
            $.getJSON(href, {ajax: 1}, function (result) {
                var callBackFunc = false;
                if (result.data.content) {
                    var callBackFunc = function () {
                        $("._course2").html(result.data.content);
                        if ($('._course2 .clickable').length) {
                            $('._course2 .clickable').trigger('click');
                        }
                    };
                }
                if (result.data.assets && Array.isArray(result.data.assets)) {
                    var iScriptLastIndex = -1;
                    result.data.assets.forEach(function (item, index) {
                        isScript = item.src.indexOf(".js");
                        if (isScript) {
                            iScriptLastIndex = index;
                        }
                    });
                    result.data.assets.forEach(function (item, index) {
                        isScript = item.src.indexOf(".js");
                        if (iScriptLastIndex == index) {
                            loadDynamicFile(item, isScript, callBackFunc);
                        } else {
                            loadDynamicFile(item, isScript, false);
                        }
                    });

                } else {
                    callBackFunc();
                }
            });
            $('.next_activity,.previous_activity').attr('disabled', 'disabled');
            current_id = $(this).attr('data-id');
            if (current_id) {
                activity_action_set(parseInt(current_id));
            }
            var body = $("html, body");
            var top = ($("._course2").offset().top - 50);
            body.stop().animate({scrollTop: top}, 400, 'swing');


            $("#activity_title").html($(this).children(":first").html());


        });

        function loadDynamicFile(file, isScript, $callback) {
            if (isScript && $callback && Loaded_Names.includes(file.name)) {
                $callback();
            }
            if (file && !Loaded_Names.includes(file.name)) {
                Loaded_Names.push(file.name);
                if (isScript) {
                    if ($callback) {
                        $.getScript(file.src, function () {
                            setTimeout(function () {
                                $callback();
                            }, 20);
                        });
                    } else {
                        $.getScript(file.src);
                    }
                } else if (isScript) {
                    $('.modal-head').append('<link href="' + file.src + '" rel="stylesheet" type="text/css">');
                }
            }
        }

        if ($(".next_activity").length) {
            var current_activity = $(".next_activity").attr("data-current-id");
            activity_action_set(current_activity);
            $(".previous_activity,.next_activity").click(function (e) {
                var index = parseInt($(this).attr('data-index'));
                if (index || index == 0) {
                    var listItem = $(".all-activity a.activity-items");
                    if (listItem[index]) {
                        $(listItem[index]).trigger('click');
                    }
                }
            });
        }
    });
</script>
