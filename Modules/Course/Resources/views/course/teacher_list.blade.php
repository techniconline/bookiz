@if(isset($teachers) && $teachers)
    <div class="row margin-top-10 _teacher">
        <hr>
        <div class="col-md-12">

            <div class="portlet box green _list_teachers">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i><i class="fa fa-users"></i>@lang("course::course.teachers")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                    </div>
                </div>

                <div class="portlet-body" style="display: none">

                    <table class="table table-striped table-hover">
                        <tbody>
                        <tr>
                            <td class="_list">

                                @foreach($teachers as $index => $teacher)

                                    <div class="col-md-2 ">
                                        <label class="mt-checkbox mt-checkbox-outline"> {!! $teacher->user->full_name !!}
                                            <input type="checkbox" {!! $activeTeachers&&in_array($teacher->id, $activeTeachers)?'checked':'' !!} value="{!! $teacher->id !!}" name="teachers[]"/>
                                            <span></span>
                                        </label>
                                    </div>

                                @endforeach

                            </td>

                        </tr>

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>
@endif