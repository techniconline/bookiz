@if(isset($viewModel->listCourses) && $viewModel->listCourses)
    <div class="container  ">
        <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12 col-12 floatright pad_top2"  >
                @php $listCourses = $viewModel->listCourses @endphp
                @foreach($listCourses as $item)
				
				 <div class=" col-xl-3 col-md-6 col-sm-6 col-12 floatright mrg10_1 course-box"  >

					  <div class="item col-xl-12 col-md-12 col-sm-12 col-12 floatright">
                        <a href="{{ route('course.view',['id'=>$item->id]) }}">
                            <img src="{!! $item->image !!}" class="_Full100precent2"/>
                        </a>
                        <h3>
                            <a href="{{ route('course.view',['id'=>$item->id]) }}">
                                    {{ $item->title }}
                                </a>
                        </h3>

                        <div class="FullWidth text-align-right fontSize18">
                     
                            <span class="fontSize12 _box008">
                                @if($students = $viewModel->getStudents($item->id,true))
                                    <i class="fas fa-user-graduate"></i>
                                    {{ $students  }}
                                @endif
                            </span>
						 
									
									 <span class="fontSize12   floatleft  box_008 "> 
								  <i class="fas fa-clock "></i> 
                                    {{ $viewModel->getDuration($item->id,true) }}
								  </span>
								  
								  
                                
                            <div class="floatigt fontSize12 FullWidth btn_home_01  ">
                                <a href="{{ route('course.view',['id'=>$item->id]) }}">
                                    <i class="fas fa-shopping-cart "></i>
                                    <span >
                                         {!! $viewModel->getCourseEnrollmentsHtml($item->id) !!}
                                    </span>
                                </a>
                            </div>
                        </div>
                        <br class="CLR"/>
                  
				  
				    </div>
				  
				  
				  </div>
				  
				  
				  
                  

                @endforeach
                <div class="paginate">
                    {!! $listCourses->links() !!}
                </div>


            </div>
        </div>
    </div>
@endif

