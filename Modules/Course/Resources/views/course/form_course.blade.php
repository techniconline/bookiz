<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("course::course.".($course?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>
{{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>(isset($course) && $course? route('course.update', ['course_id'=>$course->id]) : route('course.save'))
                        ,'method'=>(isset($course) && $course?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("course::course.tabs.public") </a>
                                </li>
                                <li>
                                    <a href="#sections" data-toggle="tab"> @lang("course::course.tabs.sections") </a>
                                </li>
                                <li>
                                    <a href="#advance" data-toggle="tab"> @lang("course::course.tabs.advance") </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::input('text','title',old('title',$course?$course->title:null)
                                        ,['required'=>'required','title'=>trans("course::course.title"),'helper'=>trans("course::course.title")]) !!}


                                    {{--@if(!$course)--}}

                                        {{--<input type="hidden" value="1" name="count_sections">--}}

                                    {{--@endif--}}

                                    {!! FormHelper::selectTag('teachers[]',[],old('teachers',$course?$activeTeachers:null),['multiple'=>'multiple', 'data-ajax-url'=>route('course.teacher.get_teacher_by_api')
                                      , 'title'=>trans("course::course.teachers"),'helper'=>trans("course::course.select_teacher")]) !!}

                                    {!! FormHelper::select('instance_id',$instances?$instances->pluck('name','id')->toArray():[],old('instance_id',$course?$course->instance_id:app('getInstanceObject')->getCurrentInstanceId())
                                    ,['title'=>trans("course::course.instances"),'helper'=>trans("course::course.select_instance")
                                    ,'placeholder'=>trans("course::course.select_instance")]) !!}

                                    {!! FormHelper::selectTag('categories[]',[],old('categories',$course?$activeCategories:null),['required'=>'required','multiple'=>'multiple', 'data-ajax-url'=>route('course.category.get_category_by_api')
                                         , 'title'=>trans("course::course.categories"),'helper'=>trans("course::course.select_category")]) !!}

                                    {!! FormHelper::editor('description',old('description',$course?$course->description:null)
                                    ,['title'=>trans("course::course.description"),'helper'=>trans("course::course.description")]) !!}

                                    {!! FormHelper::inputByButton('image',old('image',$course?$course->image_in_admin:null),['type'=>'image','title'=>trans('course::course.image_url'),'helper'=>trans('course::course.image_url')],trans('course::course.select_file'),[],true) !!}

                                </div>
                                <div class="tab-pane" id="sections">

                                    @include("course::section.form_simple_section")

                                </div>
                                <div class="tab-pane" id="advance">
                                    {!! FormHelper::input('text','code',old('code',$course?$course->code: "CRS".app('getInstanceObject')->getCurrentInstanceId()."-".strtoupper(str_random(10)) )
                                                     ,['title'=>trans("course::course.code"),'helper'=>trans("course::course.code")]) !!}

                                    {!! FormHelper::datetime('active_date',old('active_date',$course?$course->active_date:null)
                                    ,['title'=>trans("course::course.active_date"),'helper'=>trans("course::course.active_date")
                                        ,'date-year-current'=>1,'date-year-before'=>2,'set-default'=>0]) !!}

                                    {!! FormHelper::datetime('expire_date',old('expire_date',$course?$course->expire_date:null)
                                    ,['title'=>trans("course::course.expire_date"),'helper'=>trans("course::course.expire_date")
                                        ,'date-year-current'=>2,'date-year-before'=>2,'set-default'=>0]) !!}

                                    {!! FormHelper::input('text','quantity',old('quantity',$course?$course->quantity:1)
                                    ,['required'=>'required','title'=>trans("course::course.quantity"),'helper'=>trans("course::course.quantity")]) !!}

                                    {!! FormHelper::select('type',$types,old('type',$course?$course->type:"normal")
                                    ,['title'=>trans("course::course.type"),'helper'=>trans("course::course.select_type")
                                    ,'placeholder'=>trans("course::course.select_type")]) !!}

                                    {!! FormHelper::select('active',trans("course::course.statuses"),old('active',$course?$course->active:1)
                                    ,['title'=>trans("course::course.status"),'helper'=>trans("course::course.select_status")
                                    ,'placeholder'=>trans("course::course.select_status")]) !!}

                                    {!! FormHelper::textarea('short_description',old('short_description',$course?$course->short_description:null)
                                    ,['title'=>trans("course::course.short_description"),'helper'=>trans("course::course.short_description")]) !!}

                                    {!! FormHelper::legend(trans("course::course.configs")) !!}


                                    <div class="col-md-6">
                                        {!! FormHelper::select('params[access_comment]',trans("course::course.config_items.access_comments"),old('params[access_comment]',isset($course->params_json->access_comment)?$course->params_json->access_comment:"only_user")
                                                        ,['title'=>trans("course::course.config_items.access_comment"),'helper'=>trans("course::course.config_items.access_comment")
                                                        ,'placeholder'=>trans("course::course.config_items.access_comment")]) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! FormHelper::select('params[confirm_comment]',trans("course::course.config_items.confirm_comments"),old('params[confirm_comment]',isset($course->params_json->confirm_comment)?$course->params_json->confirm_comment:"by_admin")
                                                        ,['title'=>trans("course::course.config_items.confirm_comment"),'helper'=>trans("course::course.config_items.confirm_comment")
                                                        ,'placeholder'=>trans("course::course.config_items.confirm_comment")]) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! FormHelper::select('params[access_rate]',trans("course::course.config_items.access_rates"),old('params[access_rate]',isset($course->params_json->access_rate)?$course->params_json->access_rate:"only_user")
                                                        ,['title'=>trans("course::course.config_items.access_rate"),'helper'=>trans("course::course.config_items.access_rate")
                                                        ,'placeholder'=>trans("course::course.config_items.access_rate")]) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! FormHelper::select('params[show_section_title]',trans("course::course.config_items.show_section_items"),old('params[show_section_title]',isset($course->params_json->show_section_title)?$course->params_json->show_section_title:"enable")
                                                        ,['title'=>trans("course::course.config_items.show_section_title"),'helper'=>trans("course::course.config_items.show_section_title")
                                                        ,'placeholder'=>trans("course::course.config_items.show_section_title")]) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! FormHelper::select('params[show_sections]',trans("course::course.config_items.show_section_items"),old('params[show_sections]',isset($course->params_json->show_sections)?$course->params_json->show_sections:"enable")
                                                        ,['title'=>trans("course::course.config_items.show_sections"),'helper'=>trans("course::course.config_items.show_sections")
                                                        ,'placeholder'=>trans("course::course.config_items.show_sections")]) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! FormHelper::select('params[view_status_sections]',trans("course::course.config_items.view_status_section_items"),old('params[view_status_sections]',isset($course->params_json->view_status_sections)?$course->params_json->view_status_sections:"first_open")
                                                        ,['title'=>trans("course::course.config_items.view_status_sections"),'helper'=>trans("course::course.config_items.view_status_sections")
                                                        ,'placeholder'=>trans("course::course.config_items.view_status_sections")]) !!}
                                    </div>

                                    <div class="col-md-6">
                                        {!! FormHelper::select('params[show_default_section_title]',trans("course::course.config_items.show_section_items"),old('params[show_default_section_title]',isset($course->params_json->show_default_section_title)?$course->params_json->show_default_section_title:"enable")
                                                        ,['title'=>trans("course::course.config_items.show_default_section_title"),'helper'=>trans("course::course.config_items.show_default_section_title")
                                   ,'placeholder'=>trans("course::course.config_items.show_default_section_title")]) !!}
                                    </div>

                                    <div class="col-md-6">
                                        {!! FormHelper::select('params[default_show_session_time]',trans("course::course.config_items.default_show_session_times"),old('params[default_show_session_time]',isset($course->params_json->default_show_session_time)?$course->params_json->default_show_session_time:"session")
                                                        ,['title'=>trans("course::course.config_items.default_show_session_time"),'helper'=>trans("course::course.config_items.default_show_session_time")
                                   ,'placeholder'=>trans("course::course.config_items.default_show_session_time")]) !!}
                                    </div>

                                    <div class="col-md-6">

                                        {!! FormHelper::input('text','params[min_show_item_enrollment_user]',old('params[min_show_item_enrollment_user]', isset($course->params_json->min_show_item_enrollment_user)?$course->params_json->min_show_item_enrollment_user:5 )
                                                ,['title'=>trans("course::course.config_items.min_show_item_enrollment_user"),'helper'=>trans("course::course.config_items.min_show_item_enrollment_user")]) !!}
                                    </div>

                                    <div class="col-md-6">
                                        {!! FormHelper::select('params[active_qtoa]',trans("course::course.config_items.show_section_items"),old('params[active_qtoa]',isset($course->params_json->active_qtoa)?$course->params_json->active_qtoa:"disable")
                                                        ,['title'=>trans("course::course.config_items.show_section_qtoa"),'helper'=>trans("course::course.config_items.show_section_qtoa")
                                   ,'placeholder'=>trans("course::course.config_items.show_section_qtoa")]) !!}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancelWithLanguage(['title'=>trans("course::course.submit")], ['title'=>trans("course::course.cancel"), 'url'=>route('course.index')], ['selected'=>$course?$course->language_id:null]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>
