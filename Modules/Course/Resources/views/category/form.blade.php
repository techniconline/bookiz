<div class="row" id="form_category">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("course::category.add_category")
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">

                {!! FormHelper::open(['role'=>'form','url'=>(route("course.category.save"))
                     ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="parent_id" value="0" class="_parent_id">
                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-10') @endphp

                <div class="col-md-6">
                    {!! FormHelper::input('text','title',old('title',null)
                    ,['required'=>'required','title'=>trans("course::category.title"),'helper'=>trans("course::category.title")]) !!}
                </div>
                <div class="col-md-6">
                    {!! FormHelper::input('text','alias',old('alias',null)
                    ,['required'=>'required','title'=>trans("course::category.alias"),'helper'=>trans("course::category.alias")]) !!}
                </div>

                <div class="col-md-6 _language_list">
                    {!! FormHelper::select('language_id',$languages,old('language_id',null)
                    ,['title'=>trans("course::category.select_language"),'helper'=>trans("course::category.select_language"),
                    'placeholder'=>trans("course::category.select_language")]) !!}
                </div>

                <div class="col-md-6 _instance_list">
                    {!! FormHelper::select('instance_id',$instances,old('instance_id',null)
                    ,['title'=>trans("course::category.instance"),'helper'=>trans("course::category.instance"),
                    'placeholder'=>trans("course::category.instance")]) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::input('text','sort',old('sort',1)
                    ,['title'=>trans("course::category.sort"),'helper'=>trans("course::category.sort")]) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::input('text','attributes[btn_icon]',old('attributes[btn_icon]',null)
                    ,['title'=>trans("course::category.attributes.btn_icon"),'helper'=>'{fa fa-user} <i class="fa fa-user"></i>']) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::input('text','attributes[btn_font_color]',old('attributes[btn_font_color]',null)
                    ,['title'=>trans("course::category.attributes.btn_font_color"),'helper'=>'#ff3333']) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::input('text','attributes[btn_background_color]',old('attributes[btn_background_color]',null)
                    ,['title'=>trans("course::category.attributes.btn_background_color"),'helper'=>'#ff3333']) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::select('active',trans("course::course.statuses"),old('active',1)
                        ,['title'=>trans("course::course.status"),'helper'=>trans("course::course.select_status")
                         ,'placeholder'=>trans("course::course.select_status")]) !!}

                </div>

                <div class="col-md-12">
                    {!! FormHelper::textarea('short_description',old('short_description',null)
                         ,['title'=>trans("course::category.short_description"),'helper'=>trans("course::category.short_description")]) !!}
                </div>

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("course::category.submit")], ['title'=>trans("course::category.cancel"), 'url'=>route('course.category.index')]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}


            </div>
        </div>
    </div>
</div>