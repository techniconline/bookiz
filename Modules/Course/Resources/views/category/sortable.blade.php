<div class="col-md-12 _filter" id="sort_section">
   
    <ul class="">
	<li> {!! FormHelper::label('label_name_group', trans("course::course.sortable") , ['class'=>'']) !!}
	</li>
        @foreach(trans("course::course.sort_items") as $key => $item)
        <li class="">
            <a class="_item_sort {!! (($key=="new"  && !request()->get("sort_by")) || ($key==request()->get("sort_by")))?"item_active":null !!}" href="?{!! http_build_query(array_merge(["sort_by"=>$key], request()->except("sort_by"))) !!}" >
                {!! $item !!}
            </a>
        </li>
        @endforeach
    </ul>

    {{--{!! FormHelper::select('sort_by',trans("course::course.sort_items"),old('sort_by',$selected)--}}
                {{--,['title'=>trans("course::course.sort_by"), "class"=>"_sorting"]) !!}--}}
</div>
<script>
    jQuery(document).ready(function () {



    });
</script>