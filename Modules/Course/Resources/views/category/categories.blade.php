<div class="col-md-12 _filter" id="course_categories_list">
<i class="fas fa-align-justify _iconToper"></i>
{!! FormHelper::label('label_name_group', trans("course::course.categories") , ['class'=>'']) !!}
    <ul class="">
        <li class="_item_category_dept_0">
            <a class="_item_category cat_mainLink" href="{!! route("course.category.list.courses", ["id" => 0, "alias" => str_replace(" ","-",trans("course::course.all_categories"))]).'?'.http_build_query(request()->except("category_id")) !!}" >
                {{ trans("course::course.all_categories") }}
            </a>
            <div class="sidenav">
                @foreach($viewModel->getCourseCategories() as $course_category)
                    @include('course::category.sub_categories',['dept'=>1])
                @endforeach
            </div>
        </li>
    </ul>
</div>