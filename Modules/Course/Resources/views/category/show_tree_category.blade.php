<div class="row">
    <div class="col-md-12 padding-tb-10">
        <a href="{!! route('course.category.index') !!}"
           class="btn btn-lg blue circle-right">
            <i class="fa fa-list"></i>
            @lang("course::category.categories_root_list")
        </a>
    </div>
</div>
<div style="display: none">
    @include("course::category.form")
</div>

{!!  \Modules\Course\Providers\Helpers\Category\CategoryHelperFacade::createTreeByNestable($categories)  !!}

