<div class="col-md-12 _filter" id="course_enrollment_list">
   <i class="fas fa-tag _iconToper"></i> {!! FormHelper::label('label_name_group', trans("course::course.enrollments") , ['class'=>'']) !!}
    <ul class="">
        <li class="">
            <a class="_item_category cat_mainLink"
               href="?{!! http_build_query(array_merge(["enrollment_id"=>0], request()->except("category_id", "enrollment_id"))) !!}">
                {{ trans("course::course.all_enrollments") }}
            </a>
            <ul>
                @foreach($viewModel->getEnrolmentList() as $enrollment)
                    @include('course::enrollment.enrollment')
                @endforeach
            </ul>
        </li>
    </ul>
</div>