<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("course::enrollment.".($courseEnrollment?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>(isset($courseEnrollment) && $courseEnrollment? route('course.enrollments.update', ['course_enrollment_id'=>$courseEnrollment->id]) : route('course.enrollments.save'))
                        ,'method'=>(isset($courseEnrollment) && $courseEnrollment?'PUT':'POST'),'class'=>'form-horizontal','id'=>'form-enrollment','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <input type="hidden" name="course_id" value="{!! $course_id !!}">

                {!! FormHelper::select('enrollment_id',$enrollments,old('enrollment_id',$courseEnrollment?$courseEnrollment->enrollment_id:null)
                ,['title'=>trans("course::enrollment.name"),'helper'=>trans("course::enrollment.select_name"), 'class'=>"_enrollment"
                ,'placeholder'=>trans("course::enrollment.select_name"),'data-url'=>route("course.enrollments.showEnrollment",["enrollment_id"=>0,"course_enrollment_id"=>isset($courseEnrollment->id)?$courseEnrollment->id:null])]) !!}

                {!! FormHelper::input('text','title',old('title',$course?$course->title:null)
                ,['disabled'=>'disabled','title'=>trans("course::enrollment.title_course"),'helper'=>trans("course::enrollment.title_course")]) !!}

                {!! FormHelper::datetime('start_date',old('start_date',$courseEnrollment?$courseEnrollment->start_date:null)
                ,['title'=>trans("course::enrollment.start_date"),'helper'=>trans("course::enrollment.start_date")
                    ,'date-year-current'=>1,'date-year-before'=>2,'set-default'=>0]) !!}

                {!! FormHelper::datetime('end_date',old('end_date',$courseEnrollment?$courseEnrollment->end_date:null)
                ,['title'=>trans("course::enrollment.end_date"),'helper'=>trans("course::enrollment.end_date")
                    ,'date-year-current'=>2,'date-year-before'=>2,'set-default'=>0]) !!}

                {!! FormHelper::select('role_id',BridgeHelper::getAccess()->getRoles('course',null,true),old('role_id',$courseEnrollment?$courseEnrollment->role_id:null)
                ,['title'=>trans("course::enrollment.role"),'helper'=>trans("course::enrollment.select_role")
                ,'placeholder'=>trans("course::enrollment.select_role")]) !!}

                {!! FormHelper::select('duration_type',trans('course::data.enrollment_duration_type'),old('duration_type',$courseEnrollment?$courseEnrollment->duration_type:null)
                ,['title'=>trans("course::enrollment.duration_type"),'helper'=>trans("course::enrollment.select_duration_type")
                ,'placeholder'=>trans("course::enrollment.select_duration_type"),'class'=>"_duration_type"]) !!}

                {!! FormHelper::input('text','duration',old('duration',$courseEnrollment?$courseEnrollment->duration:0)
                ,['title'=>trans("course::enrollment.duration"),'helper'=>trans("course::enrollment.duration"),'class'=>"duration",'parent_class'=>'duration_box']) !!}

                {!! FormHelper::select('active',trans("course::enrollment.statuses"),old('type',$courseEnrollment?$courseEnrollment->active:null)
                ,['title'=>trans("course::enrollment.status"),'helper'=>trans("course::enrollment.select_status")
                ,'placeholder'=>trans("course::enrollment.select_status")]) !!}

                {{--{!! FormHelper::textarea('params',old('params',$courseEnrollment?$courseEnrollment->params:null)--}}
                {{--,['title'=>trans("course::enrollment.params").'(JSON)','helper'=>trans("course::enrollment.params")]) !!}--}}
                <div id="_enrollment_rendered">
                @if(isset($viewCourseEnrollment) && $viewCourseEnrollment)
                    {!! $viewCourseEnrollment !!}
                @endif
                </div>

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("course::enrollment.submit")], ['title'=>trans("course::enrollment.cancel")
                            , 'url'=>route('course.enrollments.index',['course_id'=>$course_id])]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>

