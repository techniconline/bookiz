<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("course::enrollment.".($enrollment?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>(isset($enrollment) && $enrollment? route('course.enrollment.update', ['enrollment_id'=>$enrollment->id]) : route('course.enrollment.save'))
                        ,'method'=>(isset($enrollment) && $enrollment?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp


                {!! FormHelper::input('text','name',old('name',$enrollment?$enrollment->name:null)
                ,['required'=>'required','title'=>trans("course::enrollment.name"),'helper'=>trans("course::enrollment.name")]) !!}

                {!! FormHelper::input('text','alias',old('alias',$enrollment?$enrollment->alias:null)
                ,['required'=>'required','title'=>trans("course::enrollment.alias"),'helper'=>trans("course::enrollment.alias")]) !!}

                {!! FormHelper::input('text','priority',old('priority',$enrollment?$enrollment->priority:0)
                ,['title'=>trans("course::enrollment.priority"),'helper'=>trans("course::enrollment.priority")]) !!}

                {!! FormHelper::checkbox('concurrent',1,old('concurrent',$enrollment?$enrollment->concurrent:null)
                ,['title'=>trans("course::enrollment.concurrent")]) !!}

                {!! FormHelper::select('active',trans("course::enrollment.statuses"),old('type',$enrollment?$enrollment->active:null)
                ,['title'=>trans("course::enrollment.status"),'helper'=>trans("course::enrollment.select_status")
                ,'placeholder'=>trans("course::enrollment.select_status")]) !!}

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("course::enrollment.submit")], ['title'=>trans("course::enrollment.cancel")
                            , 'url'=>route('course.enrollment.index')]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>

