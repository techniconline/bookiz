<div class="core14-box">
    @if($blockViewModel->getData('cost'))
        <span class="text-danger core14-discounted-cost">
            {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($blockViewModel->getData('cost') , $blockViewModel->getData('currency_id'),true) }}
        </span>
        @else
        <span class="text-danger core14-discounted-without-cost"></span>
    @endif
    <span class="text-success core14-cost">
         {{ BridgeHelper::getCurrencyHelper()->getPriceByCurrencyRate($blockViewModel->getData('sale_cost') , $blockViewModel->getData('currency_id'),true) }}</span>
    </span>

    @php $data=$blockViewModel->getCartData() @endphp
    {!! BridgeHelper::getCartHelper()->getAddToCart($data['item_type'],$data['item_id'],$data['options']) !!}
</div>