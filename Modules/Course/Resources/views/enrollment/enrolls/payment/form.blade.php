<div class="row margin-top-10">
    <div class="col-md-12">

        <div class="portlet box green _list_course_activity_items">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bell-o"></i>@lang("course::enrollment.name")
                    : @lang("course::enrollment.enrolls.payment")
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title=""
                       title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block">

                <div class="row">

                    <div class="col-md-12 text-center">

                        <div class="col-md-5">
                            {!! FormHelper::input('text','configs[sale_cost]',old('configs[sale_cost]',$blockViewModel->getData('sale_cost'))
                                ,['required'=>'required','class'=>'_source','title'=>trans("course::enrollment.payments.sale_cost"),'helper'=>trans("course::enrollment.payments.sale_cost_help")]) !!}
                        </div>

                        <div class="col-md-5 col-md-offset-2">
                            {!! FormHelper::input('text','configs[cost]',old('configs[cost]',$blockViewModel->getData('cost'))
                            ,['title'=>trans("course::enrollment.payments.cost"),'class'=>'_destination','helper'=>trans("course::enrollment.payments.cost")]) !!}
                        </div>

                        <div class="col-md-5">
                            {!! FormHelper::select('configs[currency_id]',BridgeHelper::getCurrencyHelper()->getCurrencyList(true),old('configs[currency_id]'
                            ,$blockViewModel->getData('currency_id',BridgeHelper::getCurrencyHelper()->getCurrentCurrency()->id))
                            ,['required'=>'required','title'=>trans("course::enrollment.payments.currency"),'helper'=>trans("course::enrollment.payments.select_currency")
                            ,'placeholder'=>trans("course::enrollment.payments.select_currency")]) !!}
                        </div>

                        <div class="col-md-5 col-md-offset-2">
                            {!! FormHelper::select('configs[type_tax]',[],old('configs[type_tax]',$blockViewModel->getData('type_tax'))
                            ,['title'=>trans("course::enrollment.payments.tax_types"),'helper'=>trans("course::enrollment.payments.select_tax_type")
                            ,'placeholder'=>trans("course::enrollment.payments.select_tax_type")]) !!}
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>
</div>
<script>

    jQuery(document).ready(function () {

        $('body').on('keyup', 'input._source', function (event) {
            event.preventDefault();
            var $clicked = $(this);
            var $destination = $('input._destination');
            $destination.val($clicked.val());
        });

    });

</script>

