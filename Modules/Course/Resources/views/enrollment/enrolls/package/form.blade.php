<div class="row margin-top-10">
    <div class="col-md-12">

        <div class="portlet box green _list_course_activity_items">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bell-o"></i>@lang("course::enrollment.name")
                    : @lang("course::enrollment.enrolls.package") ({!! $blockViewModel->getData('title') !!})
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title=""
                       title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block">

                <div class="row">

                    <div class="col-md-12 text-center">

                        {!! FormHelper::open(['role'=>'form','url'=>route('course.enrollments.package.save',['course_id'=>$blockViewModel->getData('id')]) ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                        @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                        {!! FormHelper::selectTag('courses[]',[],old('courses[]',$blockViewModel->getData('')),['multiple'=>'multiple' ,'data-ajax-url'=>$blockViewModel->getUrlSearchCourse() , 'title'=>trans("course::enrollment.package.course"),'helper'=>trans("course::enrollment.package.select_course")]) !!}

                        {!! FormHelper::textarea('description',old('description',$blockViewModel->getData('')) ,['title'=>trans("course::enrollment.package.description"),'helper'=>trans("course::enrollment.package.description")]) !!}

                        {!! FormHelper::openAction() !!}
                        {!! FormHelper::submitByCancel(['title'=>trans("course::course.submit")], ['title'=>trans("course::course.cancel"), 'url'=>route('course.enrollments.package.index',["course_id"=>$blockViewModel->getData('id')])]) !!}
                        {!! FormHelper::closeAction() !!}
                        {!! FormHelper::close() !!}


                    </div>

                </div>

            </div>
        </div>

    </div>
</div>


