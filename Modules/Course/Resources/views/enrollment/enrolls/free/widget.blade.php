<div class="core14-box">
    <span class="text-danger core14-discounted-without-cost"></span>
    <span class="text-success core14-cost">
          @lang('course::enrollment.frees.text')
    </span>
    @php $data=$blockViewModel->getCartData() @endphp
    {!! BridgeHelper::getEnrollmentHelper()->getAddToEnrollment($data['item_type'],$data['item_id'],$data['options'],$data['attributes']) !!}
</div>