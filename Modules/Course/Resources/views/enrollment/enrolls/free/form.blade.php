<div class="row margin-top-10">
    <div class="col-md-12">

        <div class="portlet box green _list_course_activity_items">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bell-o"></i>@lang("course::enrollment.name") : @lang("course::enrollment.enrolls.free")
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title=""
                       title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2> @lang("course::enrollment.has_not_configs") </h2>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


