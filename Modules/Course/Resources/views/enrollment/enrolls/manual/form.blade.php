<div class="row margin-top-10">
    <div class="col-md-12">

        <div class="portlet box green _list_course_activity_items">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bell-o"></i>@lang("course::enrollment.name")
                    : @lang("course::enrollment.enrolls.manual") ({!! $blockViewModel->getData('title') !!})
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title=""
                       title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block">

                <div class="row">

                    <div class="col-md-12 text-center">

                        {!! FormHelper::open(['role'=>'form','url'=>route('course.enrollments.manual.save',['course_id'=>$blockViewModel->getData('id')]) ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                        @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                        {!! FormHelper::selectTag('users[]',[],old('users[]',$blockViewModel->getData('enrollment_users')),['multiple'=>'multiple' ,'data-ajax-url'=>$blockViewModel->getUrlSearchUser() , 'title'=>trans("course::enrollment.manuals.user"),'helper'=>trans("course::enrollment.manuals.select_user")]) !!}

                        {!! FormHelper::textarea('description',old('description',$blockViewModel->getData('enrollment_description')) ,['title'=>trans("course::enrollment.manuals.description"),'helper'=>trans("course::enrollment.manuals.description")]) !!}

                        {!! FormHelper::select('role_id',BridgeHelper::getAccess()->getRoles('course',null,true),old('role_id')
,['title'=>trans("course::enrollment.role"),'helper'=>trans("course::enrollment.select_role")
,'placeholder'=>trans("course::enrollment.select_role")]) !!}

                        {!! FormHelper::openAction() !!}
                        {!! FormHelper::submitByCancel(['title'=>trans("course::course.submit")], ['title'=>trans("course::course.cancel"), 'url'=>route('course.enrollments.user.list.index',["course_id"=>$blockViewModel->getData('id')])]) !!}
                        {!! FormHelper::closeAction() !!}
                        {!! FormHelper::close() !!}


                    </div>

                </div>

            </div>
        </div>

    </div>
</div>


