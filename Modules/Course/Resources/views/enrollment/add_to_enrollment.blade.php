<div class="{{ isset($attributes['parent_class'])?$attributes['parent_class']:'add-cart-box' }}">
{!! FormHelper::open(['role'=>'form','url'=>route('course.user.enroll.free.item',['item'=>$item_type,'item_id'=>$item_id]),'method'=>'POST','class'=>isset($attributes['form_class'])?$attributes['form_class']:'add-cart-form','']) !!}
    {!! FormHelper::hidden('item',$item_type) !!}
    {!! FormHelper::hidden('item_id',$item_id) !!}

    @foreach($options as $name=>$value)
    {!! FormHelper::hidden('options['.$name.']',$value) !!}
    @endforeach
    @if($user = BridgeHelper::getAccess()->getUser())
<button type="submit" class="enrollment-add-submit btn btn-success core14-btn {{isset($attributes['button_class'])?$attributes['button_class']:''}} ">
    <i class="fas fa-user-plus"></i>
    <span>{{isset($attributes['button_title'])?$attributes['button_title']:trans('course::enrollment.enroll_to_course')}}</span>
</button>
    @else
        <a class="enrollment-add-submit btn btn-success core14-btn {{isset($attributes['button_class'])?$attributes['button_class']:''}} " href="{!! url('user/login').'?'.http_build_query(['referer'=>url()->current()]) !!}">
            <i class="fas fa-user-lock"></i>
            <span>{{trans('course::enrollment.login_for_enroll_to_course')}}</span>
        </a>
    @endif
{!! FormHelper::close() !!}
</div>