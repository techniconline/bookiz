<li class="">
    <a class="_item_enrollment {!! ($enrollment->id == $selected)?"active":"" !!}"
       href="?{!! http_build_query(array_merge(["enrollment_id"=>$enrollment->id], request()->except("category_id", "enrollment_id"))) !!}">
        {{ $enrollment->name }}
    </a>
</li>
