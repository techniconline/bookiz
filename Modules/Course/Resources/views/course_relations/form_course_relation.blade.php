<div class="row _relation_course">
    <div class="col-md-12">

        <div class="portlet box green ">
            <div class="portlet-title">
                <div class="caption">

                    <i class="fa fa-list"></i>{!! $viewModel->getTitle() !!}
                </div>
            </div>
            <div class="portlet-body">
                {!! FormHelper::open(['role'=>'form','url'=>route('course.relations.save', ['course_id'=>$viewModel->getCourseId()])
                        ,'method'=>'POST','class'=>'form-horizontal',"id"=>"form_relation"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-7') @endphp

                {!! FormHelper::select('type',$viewModel->getTypes(),old('type',"normal"),['title'=>trans("course::course.type"),'helper'=>trans("course::course.select_type"),'placeholder'=>trans("course::course.select_type")]) !!}

                {!! FormHelper::selectTag('relation_course_id[]',[],null,['multiple'=>'multiple', 'data-ajax-url'=>route('course.json'), 'title'=>trans("course::course.course_title"),'helper'=>trans("course::course.select_course")]) !!}


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("course::course.submit")], ['title'=>trans("course::course.cancel") , 'url'=>route('course.index')]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}


            </div>

            <div class="col-md-12 ">
                {!!  $viewModel->getGridHtml() !!}
            </div>

        </div>



    </div>
</div>

