 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        آموزش سبک زندگی با محوریت حفظ و مفاهیم جزء سی (سوره‌های ناس تا زلزال)
    </title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />


    <!-- Latest compiled and minified CSS -->


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" />
    <link href="/themes/quran/assets/api/medu/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/themes/quran/assets/api/medu/css/cssbank.css" rel="stylesheet" type="text/css" />
    <link href="/themes/quran/assets/api/medu/css/fa-template.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
window.vasleSettings = {
    app_id: "axHGPXHP",
}
</script><script type="text/javascript">
!function(){function t(){var t=n.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://www.retain.ir/app/axHGPXHP/widget/?"+Math.random().toString(34).slice(2);var e=n.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=window,a=e.Vasle;if("function"==typeof a)a("reattach_activator"),a("update",e.vasleSettings);else{var n=document,c=function(){c.c(arguments)};c.q=[],c.c=function(t){c.q.push(t)},e.Vasle=c,e.attachEvent?e.attachEvent("onload",t):e.addEventListener("load",t,!1)}}();
</script>

    <script src="/themes/quran/assets/api/medu/js/jquery.min.js" type="text/javascript"></script>
    <style>
        .table, .table thead tr th {
            background-color: #FFF;
            direction: rtl;
            text-align: center;
        }
		
		.absoldiv2 p {
    background: #fff;
    padding: 30px 20px;
    font-size: 20px;
    line-height: 2;
	    border-radius: 20px;
}

        .mybr, .mybr2 {
            display: none;
        }

        @media(max-width:767px) {
            .Scrool__ {
                overflow: scroll;
            }

            .Scrool__ .table-hover {
                width: 1000px !important;
            }
        }

        @media(max-width:490px) {
            .mybr {
                display: block;
            }
        }

        @media(max-width:407px) {
            .mybr2 {
                display: block;
            }
        }



    </style>


</head>


<body>


<div class="bgg1 ">
    <div class="container-fluid Logo">

        <img src="/themes/quran/assets/api/medu/images/logo.png" />
    </div>


    <div class="container-fluid TowerMaker ">
        <div class="container  ">
            <img src="/themes/quran/assets/api/medu/img/QuranHeader.png" class="imgtop1" />
            <h5>
                دوره آموزشی ضمن خدمت
            </h5>
            <h1>
                آموزش سبک زندگی با محوریت حفظ و مفاهیم جزء سی (سوره‌های ناس تا زلزال)
            </h1>
            <button type="button" class="btn btn-large btn-primary pull-left register-block" style="font-size: 20px; margin-left: 15px; padding: 10px 20px;" >ثبت نام در دوره</button>
        </div>
    </div>
</div>




<div class="bgg3 ">
    <div  class="container absoldiv1">
        <p dir="rtl" style="text-align: center;"><strong>به نام خدا</strong></p>

        <p dir="rtl" style="text-align: center;"><strong>قابل توجه معلمان، مدیران و کلیه پرسنل محترم وزارت آموزش و پروش</strong></p>

		
		<br>
        <center>
            <table class="table table-responsive" dir="rtl" style="border:1px solid #00000a" width="553">
                <colgroup>
                    <col width="197" />
                    <col width="327" />
                </colgroup>
                <tbody>
                <tr>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="197">
                        <p style="margin-bottom: 10px; text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>منابع آموزشی </b></font></font></font></font></span></span></span></p>
                    </td>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="327">
                        <p style="margin-bottom: 10px; text-align: center;">
						<span style="line-height:115%"><span style="orphans:2">
						
						
						<span style="widows:2">
						<font color="#ff0000"><font size="3"><font style="font-size: 13pt"><b>1-ویدیوهای آموزشی دوره </b></font></font></font>
						<br/>
						<br/>
						<font color="#ff0000"><font size="3"><font style="font-size: 13pt"><a href="https://s.goftino.com/dl/5bd982492b49f1034731ec11/u1mxffps.pdf"><b>2-کتاب درسنامه دوره به صورت فایل PDF </b></a></font></font></font>

						</span></span></span>
						

	
						
						</p>
                    </td>
                </tr>
				
				     <tr>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="197">
                        <p style="margin-bottom: 10px; text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>آزمون </b></font></font></font></font></span></span></span></p>
                    </td>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="327">
                        <p style="margin-bottom: 10px; text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt">20 سوال چهارگزینه ای (بدون نمره منفی)</font></font></font></p>
                    </td>
                </tr>
				
				
				
				
                <tr>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="197">
                        <p style="margin-bottom: 10px; text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>آزمون </b></font></font></font></font></span></span></span></p>
                    </td>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="327">
                        <p style="margin-bottom: 10px; text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>از ساعت </b></font></font></font></font><font color="#ff0000"><font size="3"><font style="font-size: 13pt"><b>10 </b></font></font><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>صبح </b></font></font></font></font><font size="3"><font style="font-size: 13pt"><b>97/11/01 </b></font></font></font><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>تا ساعت </b></font></font></font></font><font color="#ff0000"><font size="3"><font style="font-size: 13pt"><b>10 </b></font></font><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>صبح </b></font></font></font></font><font size="3"><font style="font-size: 13pt"><b>97/11/05</b> </font></font></font></span></span></span>(120 ساعت به صورت پیوسته)</p>
                    </td>
                </tr>
                <tr>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="197">
                        <p style="margin-bottom: 10px; text-align: center;">تعداد تکرار برای هر نفر</p>
                    </td>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="327">
                        <p style="margin-bottom: 10px; text-align: center;">هر نفر می تواند در صورت عدم قبولی در طول مدت برگزاری آزمون دو بار دیگر در آزمون شرکت نماید </p>
                    </td>
                </tr>
                <tr>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="197">
                        <p style="margin-bottom: 10px; text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>حدنصاب قبولی در دوره </b></font></font></font></font></span></span></span></p>
                    </td>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="327">
                        <p style="margin-bottom: 10px; text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>کسب نمره </b></font></font></font></font><font size="3"><font style="font-size: 13pt"><b>12 </b></font></font><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>از </b></font></font></font></font><font size="3"><font style="font-size: 13pt"><b>20</b></font></font></span></span></span></p>
                    </td>
                </tr>
                <tr>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="197">
                        <p style="text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>اساتید دوره آموزشی</b></font></font></font></font></span></span></span></p>

                        <p style="margin-bottom: 10px; text-align: center;">&nbsp;</p>
                    </td>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="327">
                        <p style="text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>استاد حمید محمدی، مولف درسنامه</b></font></font></font></font></span></span></span></p>

                        <p style="text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>استاد محمدعلی خسروی، مدرس آموزش مفاهیم</b></font></font></font></font></span></span></span></p>

                        <p style="margin-bottom: 10px; text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>آقای علی حبیبی، مربی آموزش حفظ</b></font></font></font></font></span></span></span></p>
                    </td>
                </tr>
                <tr>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="197">
                        <p style="margin-bottom: 10px; text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>تعداد جلسات </b></font></font></font></font><font size="3"><font style="font-size: 13pt"><b>(</b></font></font><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>ویدیو</b></font></font></font></font><font size="3"><font style="font-size: 13pt"><b>) </b></font></font><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>دوره</b></font></font></font></font></span></span></span></p>
                    </td>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="327">
                        <p style="margin-bottom: 10px; text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font size="3"><font style="font-size: 13pt"><b>41 </b></font></font><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>جلسه آموزشی</b></font></font></font></font></span></span></span></p>
                    </td>
                </tr>
                <tr>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="197">
                        <p style="margin-bottom: 10px; text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>جمع ساعت آموزشی</b></font></font></font></font></span></span></span></p>
                    </td>
                    <td style="border:1px solid #00000a; padding-top:0in; padding-bottom:0in; padding-left:0.08in; padding-right:0.08in" width="327">
                        <p style="margin-bottom: 10px; text-align: center;"><span style="line-height:115%"><span style="orphans:2"><span style="widows:2"><font color="#ff0000"><font size="3"><font style="font-size: 13pt"><b>12 </b></font></font><font face="Arial"><font face="B Nazanin"><font size="3"><font style="font-size: 13pt"><b>ساعت</b></font></font></font></font></font></span></span></span></p>
                    </td>
                </tr>
                </tbody>
            </table>
        </center>
		
		
        <p dir="rtl" style="text-align: justify;">	<span style="color:#e74c3c;">راهنمای پرداخت وجه: </span> </p>

        <p dir="rtl" style="text-align: justify;">
		
		
		<span style="color:#e74c3c;">- </span> 
			با تکمیل فرم زیر و کلیک روی دکمه ثبت نام و پرداخت، به پنل  پرداخت اینترنتی متصل می‌شوید. برای پرداخت وجه ثبت نام به یک کارت بانکی عضو شتاب که برای آن رمز دوم و کد CVV2 دریافت کرده اید مورد نیاز است</p>

        <p dir="rtl" style="text-align: justify;"><span style="color:#e74c3c;">- </span> 	کد  CVV2روی کارت یا پشت کارت بانکی درج شده است</p>

        <p dir="rtl" style="text-align: justify;"><span style="color:#e74c3c;">- </span>	 رمز دوم با مراجعه به عابربانک‌ها و در منوی عملیات رمز دستگاه‌های عابربانک عضو شتاب قابل دریافت است.</p>

       
        <p align="right" dir="rtl" style="text-align:left">&nbsp;</p>


        <p dir="rtl" style="margin-bottom: 13px; text-align: justify;"><br />
            هرگونه پرسش در خصوص نحوه برگزاری دوره یا طرح مشکلات احتمالی در دسترسی به سامانه آموزشی را از ساعت 9 الی 21 با شماره تلفن 88176609 &ndash; 021 داخلی 3 مطرح کنید و یا از طریق گفتگوی آنلاین سایت با ما در میان بگذارید.</p>

        <p dir="rtl" style="text-align: left;"><strong>با آرزوی توفیق</strong></p>

        <p dir="rtl" style="text-align: left;"><strong>موسسه فرهنگی رهپویان دانش و اندیشه</strong></p>

    </div>

    <div  class="container  absoldiv2">
<p>در صورتیکه عملیات بانکی مطابق راهنمای ویدیوی زیر تکمیل نشود ثبت نام شما تکمیل نشده است. در این صورت چنانچه مبلغی از حساب شما کسر شده باشد ظرف مدت 48 ساعت از طرف بانک عامل به حساب شما بگشت داده خواهد شد.</p>
       <video style='background: #ffffffd6; padding: 20px; border-radius: 20px;' width="100%" controls>
  <source src="https://medu.farayad.org/storage/medu/help-medu-min.mp4" type="video/mp4">

</video>


    </div>

    <div  class="container absoldiv4 ">
        <h4> توضیحات دوره  </h4>   </div>


    <div  class="container absoldiv3">

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fltr">
            <div class="row">


                <div class="icon1 ">
                    <a href="#">
                        <img src="/themes/quran/assets/api/medu/images/blank.png" class="ImgBlock" alt="blk">

                    </a>
                </div>
            </div>
            <div class="row IconH3">
                <h3>

                        41 جلسه
                </h3>

            </div>


        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fltr">
            <div class="row">


                <div class="icon2 ">
                    <a href="#">
                        <img src="/themes/quran/assets/api/medu/images/blank.png" class="ImgBlock" alt="blk">

                    </a>
                </div>
            </div>
            <div class="row IconH3">
                <h3>
                        12 ساعت
                </h3>

            </div>


        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fltr">
            <div class="row">


                <div class="icon3 ">
                        <img src="/themes/quran/assets/api/medu/images/blank.png" class="ImgBlock" alt="blk">

                </div>
            </div>
            <div class="row IconH3">
                <h3>
                        استاد حمید محمدی،<br>
                        استاد محمدعلی خسروی،<br>
                        آقای علی حبیبی
                </h3>

            </div>


        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fltr">
            <div class="row">


                <div class="icon4">
                    <a href="#">
                        <img src="/themes/quran/assets/api/medu/images/blank.png" class="ImgBlock" alt="blk">

                    </a>
                </div>
                <div class="row IconH3">
                    <h3>
                        <a href="#">
                            12.000 ريال
                        </a>
                    </h3>

                </div>
            </div>



        </div>





    </div>

</div>






<div class="bgg1 ">



    <div class="container-fluid TowerMaker pad40px ">
        <div class="container  ">
            {{--<div class="Scrool__">--}}



                {{--<table class="table table-hover">--}}
                    {{--<thead>--}}
                    {{--<tr>--}}


                        {{--<th>       نام دوره آموزشی</th>--}}
                        {{--<th>            ساعت ضمن خدمت </th>--}}
                        {{--<th>           هزینه ثبت‌نام   </th>--}}
                        {{--<th>       زمان آزمون </th>--}}
                        {{--<th>       مهلت ثبت‌نام    </th>--}}
                    {{--</tr>--}}
                    {{--</thead>--}}
                    {{--<tbody>--}}
                    {{--<tr>--}}


                        {{--<td>--}}
                            {{--مفاهیم پایه فناوری اطلاعات--}}

                        {{--</td>--}}
                        {{--<td> 12</td>--}}

                        {{--<td>97/06/20</td>--}}
                        {{--<td>--}}
                            {{--97/06/31--}}

                        {{--</td>--}}
                        {{--<td  class="redme">97/08/14</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}


                        {{--<td>--}}
                            {{--استفاده از کامپیوتر و مدیریت فایل ها--}}

                        {{--</td>--}}
                        {{--<td>8</td>--}}


                        {{--<td>97/07/02</td>--}}
                        {{--<td>--}}
                            {{--97/07/07--}}

                        {{--</td>--}}
                        {{--<td class="redme">97/08/15</td>--}}


                    {{--</tr>--}}
                    {{--<tr>--}}


                        {{--<td>--}}
                            {{--واژه پردازها--}}

                        {{--</td>--}}
                        {{--<td>26</td>--}}


                        {{--<td>97/07/09</td>--}}
                        {{--<td>--}}
                            {{--97/07/14--}}

                        {{--</td>--}}
                        {{--<td  class="redme">97/08/19</td>--}}



                    {{--</tr>--}}

                    {{--<tr>--}}


                        {{--<td>--}}
                            {{--صفحه گسترده--}}

                        {{--</td>--}}
                        {{--<td>26</td>--}}


                        {{--<td>97/07/16</td>--}}
                        {{--<td>--}}
                            {{--97/07/21--}}

                        {{--</td>--}}
                        {{--<td  class="redme">97/08/20</td>--}}



                    {{--</tr>--}}

                    {{--<tr>--}}


                        {{--<td>--}}
                            {{--بانک های اطلاعاتی--}}

                        {{--</td>--}}
                        {{--<td>26</td>--}}

                        {{--<td>97/07/23</td>--}}
                        {{--<td>--}}
                            {{--97/07/28--}}

                        {{--</td>--}}
                        {{--<td  class="redme">97/08/21</td>--}}




                    {{--</tr>--}}

                    {{--<tr>--}}


                        {{--<td>--}}
                            {{--ارائه مطالب--}}


                        {{--</td>--}}
                        {{--<td>20</td>--}}

                        {{--<td>97/07/30</td>--}}
                        {{--<td>--}}

                            {{--97/08/06--}}
                        {{--</td>--}}
                        {{--<td  class="redme">97/08/22</td>--}}



                    {{--</tr>--}}

                    {{--<tr>--}}


                        {{--<td>--}}
                            {{--اطلاعات و ارتباطات--}}

                        {{--</td>--}}
                        {{--<td>12</td>--}}

                        {{--<td>97/08/07</td>--}}
                        {{--<td>--}}
                            {{--97/08/13--}}
                        {{--</td>--}}
                        {{--<td  class="redme">97/08/23</td>--}}

                    {{--</tr>--}}

                    {{--<tr>--}}
                        {{--<td colspan="6">--}}
                            {{--<strong>--}}
                                {{--جمع کل ساعات دروس ICDL معادل 130 ساعت میباشد--}}
                            {{--</strong>--}}
                        {{--</td>--}}

                    {{--</tr>--}}

                    {{--</tbody>--}}
                {{--</table>--}}



            {{--</div>--}}



            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgg98 fltr" id="register">
                <div class="row ">
                    <div class="col-lg-6 col-md-8 col-sm-12 NoFloat">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                        @if ($errors->has('first_name'))
                                <div class="alert alert-danger">
                                <strong>{{ $errors->first('first_name') }}</strong>
                                </div>
                        @endif
                        @if ($errors->has('last_name'))
                                <div class="alert alert-danger">
                                <strong>{{ $errors->first('last_name') }}</strong>
                                </div>
                        @endif
                        @if ($errors->has('national_code'))
                                <div class="alert alert-danger">
                                <strong>{{ $errors->first('national_code') }}</strong>
                                </div>
                        @endif
                    </div>
                    </div>
                    <br>
                    {!! FormHelper::open(['role'=>'form','url'=>$url,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
                    {!! FormHelper::hidden('step','enroll') !!}

                    <div class="col-lg-6 col-md-8 col-sm-12 NoFloat">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="span_name ptrnbg">
                   نام
                    </span>
                            <span class="span_txt let_txt">
                        <input id="Text1" type="text"  name="first_name" value="{{old('first_name',$user->first_name)}}" required />

                    </span>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="span_name ptrnbg">
                        نام خانوادگی
                    </span>
                            <span class="span_txt let_txt">
                        <input id="Text1" type="text" name="last_name" value="{{old('last_name',$user->last_name)}}" required   />
                    </span>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="span_name ptrnbg">
                        شماره ملی
                    </span>
                            <span class="span_txt let_txt">
                        <input id="Text1" class="toEnDigit" type="text" name="national_code" value="{{old('national_code',$user->national_code)}}" required  />
                    </span>
                        </div>

                        {{--<div class="col-lg-12 col-md-12 col-sm-12">--}}
                    {{--<span class="span_name ptrnbg">--}}
                          {{--رمز عبور--}}
                    {{--</span>--}}
                            {{--<span class="span_txt let_txt">--}}
                        {{--<input id="Text1" type="password"  name="password" value="" required  />--}}
                    {{--</span>--}}
                        {{--</div>--}}

                        {{--<div class="col-lg-12 col-md-12 col-sm-12">--}}
                    {{--<span class="span_name ptrnbg">--}}
                         {{--تکرار رمز عبور--}}
                    {{--</span>--}}
                            {{--<span class="span_txt let_txt">--}}
                        {{--<input id="Text1" type="password"  name="password_confirmation" value="" required  />--}}
                    {{--</span>--}}

                        {{--</div>--}}

                        <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="span_name ptrnbg">
                        مبلغ پرداختی
                    </span>
                            <span class="span_txt let_txt">
                                @foreach($enrollmentsList as $idEnroll=>$enrollment)
                        <input id="Text1" type="text" value="{{ $enrollment }}" />
                                    <input type="hidden" name="enrollment_id" value="{{ $idEnroll }}"/>
                                    @endforeach
                    </span>
                        </div>
                        <button class="btn span_name2 form-control3 ptrnbg btn-success"> پرداخت و ثبت نام در دوره </button>

                    </div>
                    <br>
                    {!! FormHelper::close() !!}
                </div>
            </div>


        </div>
    </div>
</div>

























<div class="bgg6">

</div>

<script type="text/javascript">
    $(function () {
        $('.register-block').click(function(e){
            scrollTop();
        });
        if($('.alert-danger').length){
            scrollTop();
        }
        $(document).on("keyup",".toEnDigit",function() {
            var $value = $(this).val();
            var newValue="";
            for (var i=0;i<$value.length;i++)
            {
                var ch=$value.charCodeAt(i);
                if (ch>=1776 && ch<=1785)
                {
                    // european digit range
                    var newChar=ch-1728    ;
                    newValue=newValue+String.fromCharCode(newChar);
                }
                else
                    newValue=newValue+String.fromCharCode(ch);
            }
            $(this).val(newValue);
        });
    });

    function scrollTop(){
        var x = $("#register").position();
        $top=x.top;
        var body = $("html, body");
        body.stop().animate({scrollTop:$top}, 500, 'swing');
    }
</script>

</body>
</html>
