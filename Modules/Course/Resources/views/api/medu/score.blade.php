 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        آموزش سبک زندگی با محوریت حفظ و مفاهیم جزء سی (سوره‌های ناس تا زلزال)
    </title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />


    <!-- Latest compiled and minified CSS -->


    <link media="all" type="text/css" rel="stylesheet" href="/themes/default/assets/css/bootstrap.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="/themes/default/assets/fontawesome/css/all.css">
    <link media="all" type="text/css" rel="stylesheet" href="/themes/default/assets/fonts/fontiran.css">
    <link media="all" type="text/css" rel="stylesheet" href="/themes/default/assets/css/baseColor.css?v=1420.css">
    <link media="all" type="text/css" rel="stylesheet" href="/themes/default/assets/css/owl.carousel.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="/assets/global/plugins/sweetalert/sweetalert2.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="/themes/admin/assets/global/css/components-rtl.min.css">

    <script src="/themes/default/assets/js/jquery.min.js"></script>
    <script src="/themes/default/assets/js/bootstrap.min.js"></script>
    <style type="text/css">
        .img-responsive{
            width: 100% !important;
        }
        .box-image{
            box-shadow: 0px 0px 21px -4px rgba(0,0,0,0.52);
        }
        .box{
            text-align: right !important;
            direction: rtl !important;
            line-height: 200%;
        }
        .box h3{
            line-height: 200%;
        }
        .box h3 span{
            font-size: 25px;
            font-weight: bold;
        }
        .form-control {
            border: 1px solid #aaa !important;
            width: 100% !important;
            padding: 5px !important;
        }
        .alert-danger{
            font-size: 22px;
        }
    </style>

</head>


<body>
<header class="FullWidth" id="Header">
    <div class="TopLine">   </div>
</header>
<div class="FullWidth">
    <div class="container-fluid">
        <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-12 col-xs-12 text-center">
            <div class="box">
                <h3><b> کاربر گرامی به شماره کارمندی {{ $personalCode }}</b></h3>
                <br>
                <h3>شما توانستید در آزمون دوره "آموزش سبک زندگی با محوریت حفظ و مفاهیم سوره های ناس تا زلزال" با موفقیت نمره قبولی  <span class="text-success">{{ $score }}</span> را کسب کنید . </h3>
                <br>
                <a href="http://ltms.medu.ir/" class="btn btn-success">برگشت به سامانه آموزشی آموزش و پرورش </a>
            </div>
        </div>
    </div>
</div>

</body>
</html>
