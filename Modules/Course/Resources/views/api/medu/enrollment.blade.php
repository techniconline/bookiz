{!! FormHelper::open(['role'=>'form','url'=>$url,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
{!! FormHelper::hidden('step','enroll') !!}

@php  FormHelper::setCustomAttribute('label','class','col-md-4')->setCustomAttribute('element','class','col-md-8') @endphp
@if(!$user->id)
{!! FormHelper::input('text','first_name',old('first_name',$user->first_name),['required'=>'required','title'=>trans('user::form.fields.first_name'),'helper'=>trans('user::form.helper.first_name')]) !!}
{!! FormHelper::input('text','last_name',old('last_name',$user->last_name),['required'=>'required','title'=>trans('user::form.fields.last_name'),'helper'=>trans('user::form.helper.last_name')]) !!}
{!! FormHelper::input('text','national_code',old('national_code',$user->national_code),['title'=>trans('user::form.fields.national_code'),'helper'=>trans('user::form.helper.national_code')]) !!}

{!! FormHelper::input('password','password',null,['required'=>'required','title'=>trans('user::form.fields.password'),'helper'=>trans('user::form.helper.password')]) !!}
{!! FormHelper::input('password','password_confirmation',null,['required'=>'required','title'=>trans('user::form.fields.password_confirmation'),'helper'=>trans('user::form.helper.password_confirmation')]) !!}
@endif
{!! FormHelper::select('enrollment_id',$enrollmentsList,old('enrollment_id',null),['title'=>trans('course::enrollment.select_name')]) !!}

{!! FormHelper::openAction() !!}
{!! FormHelper::submitOnly(['title'=>trans('course::enrollment.enroll_course')]) !!}
{!! FormHelper::closeAction() !!}
{!! FormHelper::close() !!}