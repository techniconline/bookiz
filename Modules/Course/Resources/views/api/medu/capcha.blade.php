 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        آموزش سبک زندگی با محوریت حفظ و مفاهیم جزء سی (سوره‌های ناس تا زلزال)
    </title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />


    <!-- Latest compiled and minified CSS -->


    <link media="all" type="text/css" rel="stylesheet" href="/themes/default/assets/css/bootstrap.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="/themes/default/assets/fontawesome/css/all.css">
    <link media="all" type="text/css" rel="stylesheet" href="/themes/default/assets/fonts/fontiran.css">
    <link media="all" type="text/css" rel="stylesheet" href="/themes/default/assets/css/baseColor.css?v=1420.css">
    <link media="all" type="text/css" rel="stylesheet" href="/themes/default/assets/css/owl.carousel.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="/assets/global/plugins/sweetalert/sweetalert2.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="/themes/admin/assets/global/css/components-rtl.min.css">

    <script src="/themes/default/assets/js/jquery.min.js"></script>
    <script src="/themes/default/assets/js/bootstrap.min.js"></script>
    <style type="text/css">
        .img-responsive{
            width: 100% !important;
        }
        .box-image{
            box-shadow: 0px 0px 21px -4px rgba(0,0,0,0.52);
        }
        .box{
            text-align: right !important;
            direction: rtl !important;
        }
        .form-control {
            border: 1px solid #aaa !important;
            width: 100% !important;
            padding: 5px !important;
        }
        .alert-danger{
            font-size: 22px;
        }
    </style>

</head>


<body>
<header class="FullWidth" id="Header">
    <div class="TopLine">   </div>
</header>
<div class="FullWidth">
    <div class="container-fluid">
        <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-12 col-xs-12 text-center">
            <img src="/themes/quran/assets/image/time.png" alt="" class="img-responsive box-image"/>
            <div class="box">
                <p><b> کاربر گرامی; با سلام و ادب، </b>  ممکن است در ساعات خاصی ارائه خدمات آموزش مجازی مطلوب به شما با مشکلاتی روبرو شود. کارشناسان مرکز به طور مستمر در حال بهبود و توسعه سامانه می باشند.</p>
                <p><b> لطفا جهت استفاده بهینه از سامانه سعی کنید در ساعات کم بار تر به سامانه مراجعه کنید.</b></p>
            </div>
            <div class="box">
                <hr>
                <form class="text-center"  method="get">
                    <input type="hidden" value="{{ request('Co') }}" name="Co"/>
                    <input type="hidden" value="{{ request('q') }}" name="q"/>
                    @if($error)
                        <div class="alert alert-danger text-center">
                            <strong> {{ $error }}</strong>
                        </div>
                    @endif
                    @if($hasForm)
                    <p class="h4 mb-4">{{ $q }}</p>
                    <!-- Name -->
                    <input type="number" name="result" required="required" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="نتیجه سوال را وارد نمایید.">

                    <!-- Sign in button -->
                    <button class="btn btn-info btn-block" type="submit">ورود به آزمون یا دوره</button>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>
