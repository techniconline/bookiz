 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        آموزش سبک زندگی با محوریت حفظ و مفاهیم جزء سی (سوره‌های ناس تا زلزال)
    </title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />


    <!-- Latest compiled and minified CSS -->


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" />
    <link href="/themes/quran/assets/api/medu/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/themes/quran/assets/api/medu/css/cssbank.css" rel="stylesheet" type="text/css" />
    <link href="/themes/quran/assets/api/medu/css/fa-template.css" rel="stylesheet" type="text/css" />

    <script src="/themes/quran/assets/api/medu/js/jquery.min.js" type="text/javascript"></script>
    <style>
        .table, .table thead tr th {
            background-color: #FFF;
            direction: rtl;
            text-align: center;
        }
		
		.absoldiv2 p {
            background: #fff;
            padding: 30px 20px;
            font-size: 20px;
            line-height: 2;
                border-radius: 20px;
        }

        .mybr, .mybr2 {
            display: none;
        }

        @media(max-width:767px) {
            .Scrool__ {
                overflow: scroll;
            }

            .Scrool__ .table-hover {
                width: 1000px !important;
            }
        }

        @media(max-width:490px) {
            .mybr {
                display: block;
            }
        }

        @media(max-width:407px) {
            .mybr2 {
                display: block;
            }
        }



    </style>


</head>


<body>

<div class="container">
    <div class="jumbotron text-center">
        <p>به نام خدا</p>
        <h3 style="margin-top: 20px;color: orangered;">اطلاعیه مهم آزمون دوره</h3>
        <h2 style="font-family: myfont;margin-top: 20px;margin-bottom: 20px;color: red;">"آموزش سبک زندگی مبتنی بر آموزه‌های دینی و قرآنی جزء 30 قرآن کریم"</h2>
        <p class="text-right">به اطلاع کلیه همکاران محترم که در دوره عمومی "آموزش سبک زندگی مبتنی بر آموزه‌های دینی و قرآنی جزء 30 قرآن کریم" ثبت­ نام کرده‌اند می­رساند، به دلیل استقبال زیاد همکاران محترم و درخواست های مکرر برای تمدید مهلت ثبت نام، <span style="color: red;">آزمون دوره تا تاریخ 97/10/08 به تعویق می افتد.</span> </p>
        <p style="color: red;">لازم به ذکر است همکارانی که آزمون را با موفقیت پشت سر گذاشته اند، نمره آنها ثبت شده است و نیازی به شرکت مجدد در آزمون 97/10/08 ندارند.</p>
    </div>

    <div class="row">
        <div class="col-md-12">
            <p class="text-right">شناسنامه آزمون:</p>

            <table class="table table-striped table-inverse" dir="rtl">
                <tr>
                    <td style="width: 35%;" class="text-right">روش آزمون</td>
                    <td style="width: 65%" class="text-right"><span style="color: red;">20 سوال چهارگزینه ای به مدت 20 دقیقه</span> از بخش های مفاهیم (12نمره) و حفظ (8 نمره) (<span style="color: red;">بدون نمره منفی</span>)</td>
                </tr>
                <tr style="">
                    <td class="text-right">منابع آزمون (مطالعه یکی از دو منبع کافی است)</td>
                    <td class="text-right" style="color: red;">1- فیلم های آموزشی دوره 2- کتاب درسنامه به صورت فایل PDF</td>
                </tr>
                <tr>
                    <td class="text-right">آزمون</td>
                    <td class="text-right">از <span style="color: red;">10 صبح 97/10/08</span> تا <span style="color: red;">10 صبح 97/10/14</span> ( به صورت پیوسته)</td>
                </tr>
                <tr>
                    <td class="text-right">تکرار</td>
                    <td class="text-right">به شرط عدم قبولی در اولین سعی تا 2 بار امکان تکرار وجود دارد</td>
                </tr>
                <tr>
                    <td class="text-right">حدنصاب قبولی در دوره</td>
                    <td class="text-right">کسب نمره <span style="color: red;"> 12 از 20</span></td>
                </tr>
                <tr>
                    <td class="text-right">جمع ساعت آموزشی</td>
                    <td class="text-right" style="color: red;">12 ساعت</td>
                </tr>
            </table>
            <p style="color: darkgreen; text-align: left" class="">با آرزوی توفیق فرایاد</p>
        </div>
        <div class="col-md-12">
            {{--<a href="{{ url()->full() }}&step=1" class="btn btn-primary pull-left btn-large">ورود به کلاس درس</a>--}}
        </div>
    </div>
</div>

</body>
</html>
