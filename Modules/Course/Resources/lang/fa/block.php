<?php

return [
    'module' => 'درس',
    'courses_block'=>'بلاک درس ها',
    'course_relations_block'=>'بلاک درس ها مرتبط',
    'course_info_block'=>'بلاک اطلاعات درس',
    'advanced_block'=>'پیشرفته',
    'course_category_block'=>'بلاک دسته بندی دوره ها',
    'course_category'=>'دسته بندی دوره ها',
    'advanced'=>'پیشرفته',
    'mobile'=>'موبایل',
];