<?php

return [
    'titles' => [
        'defaults' => 'پیش فرض های درس',
    ],
    'fields' => [
        'default_poster' => 'پوستر پیش فرض درس',
        'fullname' => 'نام و نام خانوادگی',
        'message' => 'متن مورد نظر شما',
        'email' => 'ایمیل شما',

        'course_category' => 'دسته بندی',
        'course_category_deep' => 'عمق دسته بندی',
        'mobile_course_category' => 'دسته بندی موبایلی',
        'show_title' => 'نمایش عنوان',
        'class_div' => 'کلاس پدر',
        'class_ul' => 'کلاس UL',
        'class_li' => 'کلاس LI',
        'class_link' => 'کلاس لینک',
        'files_html' => 'فایل های مورد نیاز js , css',
        'templates' => [
            'slider'=>'اسلایدری',
            'list_box'=>'باکس - لیستی',
        ],
        'id' => 'شناسه',
        'title' => 'عنوان',
        'is_random' => 'انتخاب تصادفی',
        'is_mobile_box' => 'در موبایل',
        'template' => 'حالت نمایش',
        'logo_image' => 'لوگو',
        'sort_by' => 'مرتب سازی',
        'category' => 'دسته بندی',
        'take_number' => 'تعداد نمایش',
        'type_relations' => 'نوع ارتباط',
        'show_color' => 'نمایش آیکون',
        'show_icon' => 'استفاده از رنگ تنظیمات',
        'show_type_list' => 'نوع نمایش لیست',
        'list_types' => [
            'box_list'=>'به صورت جعبه ی',
            'normal'=>'عادی',
        ],

    ],
    'helper' => [
        'is_random' => 'انتخاب تصادفی',
        'is_mobile_box' => 'در موبایل',
        'template' => 'حالت نمایش',
        'logo_image' => 'لوگو',
        'sort_by' => 'مرتب سازی',
        'category' => 'دسته بندی',
        'course_category' => '',
        'show_title' => '',
        'mobile_course_category' => '',
        'class_div' => '',
        'class_ul' => '',
        'class_li' => '',
        'class_link' => '',
        'course_category_deep' => 'عمق منو',
        'show_type_list' => 'نوع نمایش لیست',

    ],
];