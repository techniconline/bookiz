<?php

return [
    'single_sales'=>[
        'disabled'=>'ندارد',
        'only_students'=>'فقط برای اعضای ثبت نامی',
        'only_users'=>'فقط برای کاربران غیر عضو',
        'all_user'=>'برای همه کاربران'
    ],
    'enrollment_duration_type'=>[
        'no_expierd'=>'بدون تاریخ مصرف',
        'expierd_date'=>'تاریخ پایان ثبت نام',
        'expierd_duration'=>'تعیین مدت زمان',
    ]
];