<?php

return [
    'text_separator'=>'،',
    'list' => 'لیست مدرسان',
    'select_status' => 'انتخاب وضعیت',
    'select_user' => 'انتخاب کاربر',
    'select_education' => 'انتخاب مدرک تحصیلی',
    'confirm_delete' => 'آیا از حذف اطمینان دارید؟',
    'confirm' => 'آیا اطمینان دارید؟',

    'delete' => 'حذف مدرس',
    'add' => 'افزودن مدرس',
    'edit' => 'ویرایش مدرس',

    'name' => 'نام',
    'field_study' => 'نام رشته تحصیلی',
    'location_study' => 'محل تحصیل',
    'short_description' => 'توضیح کوتاه',
    'description' => 'توضیح',
    'education' => 'آخرین مدرک تحصیلی',
    'educations' => ['diploma' => 'دیپلم', 'bachelor' => 'لیسانس', 'master' => 'فوق لیسانس', 'doctor' => 'دکترا'],
    'teaching_time' => 'زمان تدریس',

    'unit_time' => 'دقیقه',

    'cancel' => 'انصراف',
    'submit' => 'ثبت',
    'status' => 'وضعیت',
    'statuses' => [
        "0" => "حذف شده",
        "1" => "فعال",
        "2" => "غیرفعال",
    ],

];