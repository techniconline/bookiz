<?php

return [
    'group' => [
        'courses' => 'مدیریت دوره ها',
    ],
    'config'=>[
      'instance'=>'تنظیمات درس',
    ],
    'categories_list' => 'لیست شاخه ها',
    'courses_list' => 'لیست دوره ها',
    'teacher_list'=>'لیست اساتید',
    'enrollment_list'=>'لیست روشهای ثبت نام',

];