<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 9/6/18
 * Time: 3:36 PM
 */
namespace Modules\Course\Sale;

use Modules\Core\Patterns\Sale\Item\SaleItemPattern;
use Modules\Course\Models\Course\CourseActivity As CourseActivityModel;
use Modules\Course\Traits\Activity\ActivitySaleTrait;
use Modules\Course\Traits\Page\ActivityViewModelTrait;

class CourseActivity extends SaleItemPattern
{
    use ActivitySaleTrait;
    use ActivityViewModelTrait;

    protected $type='course_activity';
    protected $itemID=false;


    private $activity=false;
    private $options=[];

    public function setItemId($item_id){
        $this->itemID=$item_id;
        $this->activity=CourseActivityModel::find($item_id);
        return $this;
    }


    public function setOptions($options){
        $this->options=$options;
        return $this;
    }


    public function canSaleItem(){
        if(!$this->activity || !isset($this->options['course_id']) || !$this->options['course_id']){
            return false;
        }
        $accessActivities=$this->getUserActivityAccess(null,$this->options['course_id']);
        if(isset($accessActivities[$this->activity->id])){
            return false;
        }
        return true;
    }

    public function getMaxQtyForSale(){
        return 1;
    }

    public function getSaleItem(){
        $data=$this->getCartData($this->activity,$this->options['course_id']);
        $data['options']['cat_ids']=$this->getCategoryIds();
        return $data;
    }

    public function getItemQty(){
        return 1;
    }

    public function getItemExtraData(){
        if(!$this->activity){
            return false;
        }
        $url=route("course.view", ["course_id" => $this->activity->course_id]);
        $data=[
            'image'=>null,
            'url'=>$url,
        ];
        return $data;
    }



    public function setOrderComplete($order,$item){
        if($this->canSaleItem()){
            $result=$this->addUserToActivity($order->user_id,$this->activity->id,$this->options['course_id']);
            if($result){
                return trans('course::messages.activity_add_success_message',['id'=>$this->activity->id,'name'=>$this->activity->title]);
            }
        }
        return false;
    }


    public function getCategoryIds(){
        $cats=[];
        return $cats;
    }



}