<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 9/6/18
 * Time: 3:36 PM
 */
namespace Modules\Course\Sale;

use Modules\Core\Patterns\Sale\Item\SaleItemPattern;
use Modules\Course\Models\Enrollment\CoursePackageEnrollment;
use Modules\Course\Traits\Page\EnrollmentViewModelTrait;
use Modules\Course\Models\Course\Course as CourseModel;
use Modules\Course\Models\Course\CourseCategory;

class Course extends SaleItemPattern
{
    use EnrollmentViewModelTrait;

    protected $type='course';
    protected $itemID=false;

    private $course=false;
    private $options=[];
    private $enrollment=false;

    public function setItemId($item_id){
        $this->itemID=$item_id;
        $this->course=CourseModel::find($item_id);
        return $this;
    }


    public function setOptions($options){
        $this->options=$options;
        return $this;
    }


    public function setEnrollments($user_id=null){
        $role=$this->setUserRoleAndAccessOnCourse($this->course->id,$user_id);
        if($role){
            return false;
        }
        $this->enrollment=$this->getCourseEnrollment($this->course->id,$this->options['enrollment_id']);
        if($this->enrollment){
            return true;
        }
        return false;
    }

    public function canSaleItem(){
        if(!$this->course || !isset($this->options['enrollment_id']) || !$this->options['enrollment_id']){
            return false;
        }
        return $this->setEnrollments();
    }

    public function getMaxQtyForSale(){
        return 1;
    }

    public function getSaleItem(){
        $item=$this->enrollment->getCartData();
        $item['name']=$this->course->title;
        $item['options']['product_key']=$this->course->code;
        $item['options']['cat_ids']=$this->getCategoryIds();
        $item['qty']=1;
        return $item;
    }

    public function getItemQty(){
        return 1;
    }


    public function getItemExtraData(){
        if(!$this->course){
            return false;
        }
        $data=[
            'id'=>$this->course->id,
            'image'=>$this->course->image,
            'url'=>$this->course->url_single_course,
        ];
        return $data;
    }


    public function setOrderComplete($order,$item){
        $this->setEnrollments($order->user_id);
        if($this->enrollment){
            try{
                $methodName = 'setOrderFor'.get_uppercase_by_underscores($this->course->type);
                return $this->$methodName($order);
            }catch (\Exception $exception){
                report($exception);
            }

        }
        return false;
    }



    public function setOrderForPackage($order)
    {
        $messages = [];
        $messages[] = $this->setOrderForNormal($order);
        $coursePackageEnrollments = CoursePackageEnrollment::active()->with('coursePackage')->where('course_id', $this->course->id)->get();
        foreach ($coursePackageEnrollments as $coursePackage) {
            $result=$this->enrollUserOnCoursePackage($order->user_id,$this->options['enrollment_id'],$coursePackage->package_course_id,$coursePackage->course_id);
            if($result){
                $messages[] = trans('course::enrollment.enroll_success_message',['id'=>$coursePackage->package_course_id,'name'=>$coursePackage->coursePackage->title]);
            }
        }
        return $messages;
    }

    /**
     * @param $order
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function setOrderForNormal($order)
    {
        if($this->isEnroll()){
            return trans('course::enrollment.is_enroll_message',['id'=>$this->course->id,'name'=>$this->course->title]);
        }

        // set course_enrollment TODO
        $result=$this->enrollUserOnCourse($order->user_id,$this->options['enrollment_id'],$this->course->id);
        if($result){
            return trans('course::enrollment.enroll_success_message',['id'=>$this->course->id,'name'=>$this->course->title]);
        }else{
            return trans('course::enrollment.enroll_error_message',['id'=>$this->course->id,'name'=>$this->course->title]);
        }
    }


    public function getCategoryIds(){
        $cats=[];
        if($this->course->category_id){
            $cats[]=$this->course->category_id;
            $CourseCategory=new CourseCategory();
            $categories=CourseCategory::with('parentRecursive')->where('id',$this->course->category_id)->get();
            $parents= $CourseCategory->getParentsIds($categories);
            $cats=$parents->toArray();
        }
        return $cats;
    }



}