<?php

namespace Modules\Blog\Providers\Helpers\Category;

use Illuminate\Support\ServiceProvider;

class BlogCategoryHelperServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->registerThisHelperBuilder();
        $this->app->alias("blogCategory", "Modules\Blog\Providers\Helpers\Category");
    }

    protected function registerThisHelperBuilder(){
        $this->app->singleton("blogCategory", function (){
            return new BlogCategoryHelper();
        });
    }

}