<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('instance_id');
            $table->unsignedInteger('language_id');
            $table->unsignedInteger('category_id');
            $table->string('title')->index();
            $table->string('slug')->nullable()->index();
            $table->string('image')->nullable();
            $table->mediumText('short_description')->nullable();
            $table->longText('description')->nullable();
            $table->text('params')->nullable()->comment("json, configs or ...");
            $table->enum('type',['single'])->default('single')->nullable();
            $table->timestamp('active_date')->nullable();
            $table->timestamp('expire_date')->nullable();
            $table->unsignedBigInteger('view_count')->nullable()->default(0);
            $table->boolean("is_public")->nullable()->default(1);
            $table->tinyInteger('active')->default('2')->comment('1: active, 0: delete, 2:disable');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('blogs', function (Blueprint $table) {
            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('language_id')->references('id')->on('languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('category_id')->references('id')->on('blog_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blogs', function (Blueprint $table) {
            $table->dropForeign('blogs_instance_id_foreign');
            $table->dropForeign('blogs_language_id_foreign');
            $table->dropForeign('blogs_category_id_foreign');
        });

        Schema::dropIfExists('blogs');
    }
}
