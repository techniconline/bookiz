<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogCategoryRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_category_relations', function (Blueprint $table) {
            $table->unsignedInteger('blog_id')->index();
            $table->unsignedInteger('category_id')->index();
        });

        Schema::table('blog_category_relations', function (Blueprint $table) {
            $table->foreign('blog_id')->references('id')->on('blogs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

             $table->foreign('category_id')->references('id')->on('blog_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_category_relations', function (Blueprint $table) {
            $table->dropForeign('blog_category_relations_course_id_foreign');
            $table->dropForeign('blog_category_relations_categories_id_foreign');
        });

        Schema::dropIfExists('blog_category_relations');
    }
}
