<?php

namespace Modules\Blog\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Block\Block;
use Modules\Core\Models\Module\Module;


class BlockTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('currencies')->delete();

        $module_id=Module::where('name','blog')->first()->id;

        $block = new Block();
        $block->fill(array(
                'module_id'=>$module_id,
				'name' => 'blogs_block',
				'namespace' => 'Modules\Blog\ViewModels\Blocks\BlogsBlock',
				'active' => 1,
			))->save();



	}
}