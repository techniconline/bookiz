<?php

namespace Modules\Blog\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Module\Module;

class ModuleTableSeeder extends Seeder {

	public function run()
	{
		// CurrenciesSeeder
        $module=new Module();
        $module->fill(array(
				'name' =>'blog',
				'version' => '2017010101',
			))->save();

	}
}