<?php

namespace Modules\Blog\ViewModels\Blog;

use Modules\Blog\Models\Blog\BlogCategory;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Blog\Models\Blog;
use Modules\Blog\Models\Blog\BlogCategoryRelation;
use Modules\Blog\Providers\Helpers\Category\BlogCategoryHelperFacade as BlogCategoryHelper;


class CategoryByBlogsViewModel extends BaseViewModel
{

    public $listBlogs;

    /**
     * @return $this
     * @throws \Throwable
     */
    public function listBlogs()
    {
        $viewModel =& $this;
        $current_category = $this->request->get("category_id");
        $current_sortable = $this->request->get("sort_by");
        $model = new BlogCategory();
        if ($current_category) {
            $categories = $model->getChildesId($current_category);
            $this->request->offsetSet("category_id", $categories->toArray());
        }
        $model = new Blog();
        $this->listBlogs = $model->setRequestItems($this->request->all())
            ->filterCurrentInstance()
            ->filterByCategories()
            ->with(["blogCategories"])
            //->filterRelations(["blog", "blogCategory"])
            ->sortBy()
            ->active()
            ->paginate($this->perPage);

        if (app("getInstanceObject")->isApi()) {
            $this->listBlogs = $this->decorateList($this->listBlogs);
            return $this->setDataResponse($this->listBlogs)->setResponse(true);
        }
        $this->setTitlePage(trans('blog::blog.list'));
        return $this->renderedView('blog::blog.blogs_list', compact('viewModel', 'current_category', 'current_sortable'));
    }

    public function getBlogCategories()
    {
        $blogCategories = BlogCategoryHelper::getNestableBlogCategories();
        return $blogCategories;
    }

    /**
     * @param null $selected
     * @return string
     * @throws \Throwable
     */
    public function renderCategoriesView($selected = null)
    {
        $viewModel =& $this;
        return view('blog::category.categories', compact("selected", "viewModel"))->render();
    }

    /**
     * @param null $selected
     * @return string
     * @throws \Throwable
     */
    public function renderSortableView($selected = null)
    {
        $viewModel =& $this;
        $this->request->offsetUnset("category_id");
        return view('blog::category.sortable', compact("selected", "viewModel"))->render();
    }

}
