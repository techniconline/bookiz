<?php

namespace Modules\Blog\ViewModels\Blog;


use Illuminate\Http\Request;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Traits\Feature\FormFeatureBuilder;
use Modules\Blog\Models\Blog;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Blog\Models\Blog\BlogCategoryRelation;


class BlogViewModel extends BaseViewModel
{

    use GridViewModel;
    use FormFeatureBuilder;

    private $blog_assets;
    private $blog_meta_assets;

    public function __construct()
    {
//        $this->setModel(new Blog());
    }

    public function setGridModel()
    {
        $this->Model = Blog::enable()->filterCurrentInstance()->with('blogCategory', 'instance');
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $blog_assets = [];

        //use bested assets
        if ($this->blog_assets) {
            $blog_assets [] = ['container' => 'up_theme_js_2', 'src' => ("assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"), 'name' => 'ui.widget'];

            $blog_assets [] = ['container' => 'plugin_general_2', 'src' => ("jquery-ui/jquery-ui.min.js"), 'name' => 'jquery-ui'];
            $blog_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/plupload.full.min.js"), 'name' => 'plupload.full'];
            $blog_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/jquery.ui.plupload.js"), 'name' => 'jquery.ui.plupload'];
            $blog_assets [] = ['container' => 'plugin_general_2', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/i18n/fa.js"), 'name' => 'fa.js'];

            $blog_assets [] = ['container' => 'general_style', 'src' => ("jquery-ui/jquery-ui.min.css"), 'name' => 'jquery-ui'];
            $blog_assets [] = ['container' => 'general_style', 'src' => ("vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css"), 'name' => 'jquery.ui.plupload.css'];

        }

        if ($this->blog_assets || $this->blog_meta_assets) {
            $blog_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/blog/blog.js"), 'name' => 'blog-backend'];
        }

        return $blog_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('title', trans('blog::blog.title'), true);
        $this->addColumn('active_text', trans('blog::blog.status'), false);
        $this->addColumn('category', trans('blog::blog.category'), false);
        $this->addColumn('instance', trans('blog::blog.instance'), false);

        /*add action*/
        $add = array(
            'name' => 'blog.create',
            'parameter' => null
        );
        $this->addButton('create_blog', $add, trans('blog::blog.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);


        $show = array(
            'name' => 'blog.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('blog::blog.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('blog::blog.confirm_delete')];
        $delete = array(
            'name' => 'blog.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('blog::blog.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('title', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->category = $row->blogCategory->title;
        $row->instance = $row->instance->name;
        return $row;
    }


    /**
     * @return $this
     */
    protected function setAssetsEditBlog()
    {
        $this->blog_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('blog::blog.list'));
        $this->generateGridList()->renderedView("blog::blog.index", ['view_model' => $this], "blog_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createBlog()
    {
        $instances = Instance::active()->get();

        $blog = new Blog();

        $types = $blog->getTypes();

        $blog = $blog->enable()->with([
            'language'
            , 'instance'
            , 'blogCategory'
        ])->find($this->request->get('blog_id'));

        $activeCategories = null;
        if ($blog) {
            $activeCategories = BlogCategoryRelation::where('blog_id', $blog->id)->pluck('category_id')->toArray();
        }

        $this->setTitlePage(trans('blog::blog.' . ($blog ? 'edit' : 'add')));
        return $this->renderedView("blog::blog.form_blog", ['blog' => $blog
            , 'types' => $types, 'activeCategories' => $activeCategories, 'instances' => $instances], "form");
    }

    /**
     * @return BlogViewModel
     * @throws \Throwable
     */
    protected function editBlog()
    {
        return $this->createBlog();
    }

    /**
     * @return $this
     */
    protected function saveBlog()
    {
        $response = $this->saveBlogService();
        $blog = isset($response['blog']) ? $response['blog'] : null;
        if ($response['action']) {
            return $this->redirect(route('blog.edit', ['blog_id' => $blog->id]))->setDataResponse($blog)->setResponse(true, trans("blog::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, isset($response['message']) ? $response['message'] : trans("blog::messages.alert.mis_data"), isset($blog->errors) ? $blog->errors : null);
    }

    /**
     * @return $this|array
     */
    private function saveBlogService()
    {
        $blog = new Blog();

        $blog_id = $this->request->get('blog_id');

        $this->request = $this->requestValuesUpdate(['active_date' => 'Y-m-d H:i:s', 'expire_date' => 'Y-m-d H:i:s']);
        if ($blog_id) {
            $blog = $blog->enable()->find($blog_id);
            if (!$blog) {
                return ['action' => false, 'blog' => $blog, 'redirect' => route('blog.edit', ['blog_id' => $this->request->get('blog_id')])];
            }
        }

        $categories = $this->request->get('categories');
        $category_id = $categories ? $categories[0] : null;
        $this->request->offsetSet('category_id', $category_id);
        if (!$slug = $this->request->get("slug")) {
            $this->request->request->set('slug', create_alias($this->request->get("title")));
        }
        $this->request->offsetSet('params', json_encode($this->request->get("params")));

        $this->validate($this->request, $blog->rules);
        if (($blog->fill($this->request->all()))) {

            $blog->save();
            $this->saveCategories($blog->id);

            $blog->updated = $blog_id ? true : false;

            $response = ['action' => true, 'blog' => $blog];
        } else {
            $response = ['action' => false, 'blog' => $blog];
        }

        return $response;
    }


    /**
     * @return $this
     */
    protected function destroyBlog()
    {
        $blog_id = $this->request->get('blog_id');
        $blog = Blog::enable()->find($blog_id);

        if ($blog && $blog->delete()) {
            return $this->setResponse(true, trans("blog::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("blog::messages.alert.del_un_success"));

    }

    /**
     * @param $blog_id
     * @return bool
     */
    protected function saveCategories($blog_id)
    {
        $categories = $this->request->get('categories');
        BlogCategoryRelation::where('blog_id', $blog_id)->delete();

        if (empty($categories)) {
            return false;
        }

        $insertItems = [];
        foreach ($categories as $category) {
            $insertItems[] = ['blog_id' => $blog_id, 'category_id' => $category];
        }
        $blogCR = new BlogCategoryRelation();
        $result = $blogCR->insert($insertItems);
        return $result;
    }

    /**
     * @return BlogViewModel
     */
    protected function getBlogsByCategory()
    {
        $model = new BlogCategoryRelation();
        $list = $model->setRequestItems($this->request->all())->filters()->isVisible()->with('blog')->simplePaginate();
        $list = $this->decorateList($list, 'blog');
        return $this->setDataResponse($list)->setResponse($list ? true : false);
    }

    /**
     * @return BlogViewModel
     */
    protected function show()
    {
        $blog = new Blog();
        $blog = $blog->enable()->with([
            'language'
            , 'instance'
            , 'blogCategory'
        ])->find($this->request->get('blog_id'));
        $blog = $this->decorateAttributes($blog);
        return $this->setDataResponse($blog)->setResponse($blog ? true : false);

    }

}
