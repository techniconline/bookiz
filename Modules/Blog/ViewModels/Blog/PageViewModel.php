<?php

namespace Modules\Blog\ViewModels\Blog;

use BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Blog\Models\Blog;

class PageViewModel extends BaseViewModel
{

    private $blog;

    /**
     * @return $this|void
     * @throws \Throwable
     */
    public function viewBlog()
    {
        $blockViewModel =& $this;
        $this->blog = Blog::with(['blogCategories','blogCategory'])->active()->find($this->request->get('blog_id'));
        if (!$this->blog) {
            return abort(404, 'Page Not Find');
        }
    
        if (app("getInstanceObject")->isApi()) {
            $this->blog = $this->decorateAttributes($this->blog);
            return $this->setDataResponse($this->blog)->setResponse(true);
        }
        $this->setTitlePage($this->blog->title);
        return $this->renderedView('blog::blog.page', ['blockViewModel'=>$blockViewModel]);
    }

    /**
     * @return CategoryByBlogsViewModel
     */
    public function getCategoryByBlogs()
    {
        return new CategoryByBlogsViewModel();
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getJsonData()
    {
        $this->blog->rate = BridgeHelper::getRate()->getRateObject()->setModel($this->blog->getTable(), $this->blog->id)->setWithData(true)->init()->getData();
        $this->blog->image = strlen($this->blog->image) ? $this->blog->image : BridgeHelper::getConfig()->getSettings('default_poster', 'instance', 'blog');
        return $this;
    }

    public function getBlog()
    {
        return $this->blog;
    }
}
