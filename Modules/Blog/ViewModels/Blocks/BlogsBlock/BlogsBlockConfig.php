<?php

namespace Modules\Blog\ViewModels\Blocks\BlogsBlock;


use Modules\Blog\Models\Blog\BlogCategory;
use Modules\Core\Traits\Block\MasterBlockConfig;

class BlogsBlockConfig
{
    use MasterBlockConfig;

    public $baseClass='BlogsBlock';

    public function __construct(){
        $this->setDefaultName(trans('blog::block.blogs_block'));
        $this->addTab('blogs_block',trans('blog::block.blogs_block'),'blog::widgets.blogs.config.form');
        $this->addTab('advanced_block',trans('blog::block.advanced_block'),'blog::widgets.blogs.config.advanced');
    }


    /**
     * @return array
     */
    public function getTemplateList(){
        $list=[
            'slider'=>trans("blog::form.fields.templates.slider"),
            'list_box'=>trans("blog::form.fields.templates.list_box"),
        ];
        return $list;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getSortList(){
        $list=trans("blog::blog.sort_items");
        return $list;
    }

    /**
     * @return array
     */
    public function getBlogCategoriesList()
    {
        $blog_categoryList = BlogCategory::filterLanguage()->filterCurrentInstance()->with("parentRecursive")->get();
        $list = [];
        $list[0] = "...";
        foreach ($blog_categoryList as $blog_category) {
            $this->parents = [];
            $this->getParents($blog_category);
            $list[$blog_category->id] = implode(" > ",$this->parents) ." > ". $blog_category->title;
        }
        return $list;
    }

    private $parents = [];

    /**
     * @param $row
     * @param bool $first
     * @return mixed
     */
    public function getParents($row)
    {
        if ($row->parentRecursive) {
            $this->parents[] = $this->getParents($row->parentRecursive, false);
        }
        return $row->title;
    }

}