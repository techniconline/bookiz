<?php

namespace Modules\Blog\ViewModels\Blocks\BlogsBlock;

use Modules\Core\Traits\Block\MasterBlock;

use Modules\Core\Traits\Decorate\DecorateData;
use Modules\Blog\Models\Blog;

class BlogsBlock
{

    use MasterBlock;
    use DecorateData;

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $blockViewModel =& $this;
        return view('blog::widgets.blogs.' . $this->getConfig("template", "slider"), compact('blockViewModel'))->render();
    }

    /**
     * @return array
     */
    public function renderJson()
    {

        $blogs = $this->renderDataBlogs($this->getBlogsList());
        $block = $this->setDataBlock($blogs)->getBlock();
        return $block;
    }

    /**
     * @param $blogs
     * @return mixed
     */
    private function renderDataBlogs($blogs)
    {
        return $this->decorateList($blogs, null);
    }

    /**
     * @param bool $paginate
     * @return mixed
     */
    public function getBlogsList($paginate = false)
    {
        $model = new Blog();
        $take = $this->getConfig("blog_count_show", 8);
        $items = [
            "category_id" => $this->getConfig("blog_category_id", 0),
            "sort_by" => $this->getConfig("sort_by", "new"),
        ];
        $data = $model->setRequestItems($items)
            ->filterLanguage()->filterCurrentInstance()
            ->filterByCategories()->active()
            ->sortBy();

        if ($paginate) {
            $data = $data->paginate($take);
        } else {
            if ($take) {
                $data = $data->take($take);
            }
            $data = $data->get();
        }

        return $data;
    }


    /**
     * @return mixed
     */
    public function getBlogsListPaginate()
    {
        return $this->getBlogsList(true);
    }


}
