<?php

namespace Modules\Blog\ViewModels\Search;


use Illuminate\Http\Request;
use Modules\Blog\Models\Blog;
use Modules\Core\ViewModels\BaseViewModel;

class SearchViewModel extends BaseViewModel
{
    public $listBlogs;

    public function __construct()
    {
        parent::__construct();
        $this->setModel(new Blog());
    }

    protected function searchBlogs()
    {
        $viewModel =& $this;
        $model = new Blog();
        $result = $model->setRequestItems($this->request->all())->active()->searching()->paginate($this->perPage);
        $this->listBlogs = $this->decorateList($result,null);
        return $this->renderedView('blog::blog.list', compact('viewModel'));
    }
}
