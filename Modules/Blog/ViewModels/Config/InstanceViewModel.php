<?php

namespace Modules\Blog\ViewModels\Config;

use Modules\Core\ViewModels\Config\SettingViewModel;

class InstanceViewModel extends SettingViewModel
{

    public $setting_name='instance';
    public $module_name='blog';

    public function getConfigForm(){
        $BlockViewModel=&$this;
        $this->setTitlePage(trans('blog::menu.config.instance'));
        return $this->renderedView('blog::config.instance',compact('BlockViewModel'));
    }


    public function save(){
        $rules=[];
        $this->requestValuesUpdate();
        $data=$this->request->all();
        $result=$this->saveConfig($data);
        if($result){
            return $this->redirectBack()->setResponse($result,trans('core::messages.alert.save_success'));
        }
        return $this->redirectBack()->setResponse($result,trans('core::messages.alert.error'));

    }

}
