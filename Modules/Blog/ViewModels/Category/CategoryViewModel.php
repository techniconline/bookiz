<?php

namespace Modules\Blog\ViewModels\Category;

use Illuminate\Support\Facades\Route;
use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Core\ViewModels\Traits\MenuTrait;
use Modules\Blog\Models\Blog\BlogCategory as Category;

class CategoryViewModel extends BaseViewModel
{

    use GridViewModel;
    use MenuTrait;

    private $nestable_assets;

    public function __construct()
    {

    }

    public function getRowsUpdate($row)
    {
//        $row->post_category_id = $this->getPostCategory($row->post_category_id);
//        if ($row->publish) {
//            $row->publish = view('blog::grid.elements.text', ['class' => 'text-success', 'text' => getGeneralData()->getPublishData($row->publish)])->render();
//        } else {
////            $row->publish = view('blog::grid.elements.text', ['class' => 'text-danger', 'text' => getGeneralData()->getPublishData($row->publish)])->render();
//        }

        return $row;
    }


    public function setGridModel()
    {
        $this->Model = Category::whereNull('parent_id')->filterCurrentInstance()->enable();
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $category_assets = [];

        //use bested assets
        if ($this->nestable_assets) {
            $category_assets [] = ['container' => 'theme_style', 'src' => ("assets/global/plugins/jquery-nestable/jquery.nestable.css"), 'name' => 'jquery.nestable'];
            $category_assets [] = ['container' => 'theme_js', 'src' => ("assets/global/plugins/jquery-nestable/jquery.nestable-rtl.js"), 'name' => 'jquery.nestable'];
            $category_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/blog/category.js"), 'name' => 'category-backend'];
        }

        return $category_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return mixed
     */
    private function getList()
    {
        return Category::active()->filters()->get();
    }

    private function generateGridCategoryList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('id', trans('blog::validations.attributes.id'), true)
            ->addColumn('title', trans('blog::category.title'), true)
//            ->addColumn('post_category_id', trans('blog::validations.attributes.category'), true)
            //->addColumn('publish',trans('blog::validations.attributes.publish'),true)
            ->addColumn('created_at', trans('blog::validations.attributes.date'), true);

        $add = array(
            'name' => 'blog.category.create',
            'parameter' => null
        );
        $this->addButton('create_root_category', $add, trans('blog::category.add_category_root'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);

        /*add action*/
//        $add = array(
//            'name' => 'blog.category.create',
//            'parameter' => null
//        );
//        $this->addAction('add', $add, "افزودن منوی اصلی (ROOT)", 'fa fa-plus', false, ['target' => '_blank', 'class' => 'btn btn-sm btn-danger']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('blog::category.confirm_delete')];
        $delete = array(
            'name' => 'blog.category.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('blog::category.delete_category'), 'fa fa-trash', false, $options);

        $show = array(
            'name' => 'blog.category.show',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('blog::category.categories_list'), 'fa fa-list', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('id', 'text', ['op' => '=']);
        $this->addFilter('title', 'text');
//        $this->addFilter('category', 'select', ['field' => 'post_category_id', 'options' => $this->getPostCategory(), 'title' => trans('blog::validations.attributes.category')]);

        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('blog::category.categories_root_list'));
        $this->generateGridCategoryList()->renderedView("blog::category.index", ['view_model' => $this], "category_list");
        return $this;
    }

    /**
     * active assets for method called!
     * @return $this
     */
    protected function setAssetsShowCategoryTree()
    {
        $this->nestable_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function showCategoryTree()
    {
        $instances = Instance::active()->get();
        $instances = $instances ? $instances->pluck('name', 'id') : [];
        $languages = Language::active()->get();
        $languages = $languages ? $languages->pluck('name', 'id') : [];

        $category = Category::enable()->find($this->request->get('category_id', 0));
        $this->setTitlePage(trans("blog::category.list_category", ["root_category" => $category ? ' ( ' . $category->title . ' ) ' : '...']));
        $root_id = $category ? $category->root_id : 0;
        $categories = $this->getNestable(Category::enable()->where('root_id', $root_id)->with(['instance'])->get(), null, $root_id);
        return $this->renderedView("blog::category.show_tree_category", ['categories' => $categories, 'instances' => $instances, 'languages' => $languages], "tree_categories");;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createCategory()
    {
        $instances = Instance::active()->get();
        $instances = $instances ? $instances->pluck('name', 'id') : [];
        $languages = Language::active()->get();
        $languages = $languages ? $languages->pluck('name', 'id') : [];

        return $this->renderedView("blog::category.form", ['instances' => $instances, 'languages' => $languages,], "edit_category");
    }

    /**
     * @return $this
     */
    protected function destroyCategory()
    {
        $category_id = $this->request->get('category_id');
        $category = Category::enable()->find($category_id);

        if ($category->delete()) {
            return $this->setResponse(true, trans("blog::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("blog::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveCategory()
    {
        $category = new Category();
        if ($category_id = $this->request->get("category_id")) {
            $category = $category->enable()->find($category_id);

            if (!$category) {
                return $this->redirect(route('blog.category.show', ['category_id' => $this->request->get('category_id')]))->setResponse(false, trans("blog::messages.alert.not_find_data"));
            }

        } else {
            $this->setRootId();
        }

        if (is_int((int)$this->request->get('parent_id')) && !$this->request->get('parent_id')) {
            $this->request->request->set('parent_id', null);
        }

        if ($attributes = $this->request->get("attributes")) {
            $this->request->request->set('attributes', json_encode($attributes));
        }
        if (!$alias = $this->request->get("alias")) {
            $this->request->request->set('alias', create_alias($this->request->get("title")));
        }

        if (($category->fill($this->request->all())->isValid())) {
            $category->save();
            $category->url_edit = route('blog.category.update', ['category_id' => $category->id]);
            $category->url_add_child = route('blog.category.add_child', ['category_id' => $category->id]);
            $category->url_delete = route('blog.category.delete', ['category_id' => $category->id]);
            $category->updated = $category_id ? true : false;
            return $this->redirect(route('blog.category.show', ['category_id' => $category->id]))->setDataResponse($category)->setResponse(true, trans("blog::messages.alert.save_success"));
        }

        return $this->redirect(route('blog.category.create'))->setResponse(false, trans("blog::messages.alert.mis_data"), $category->errors);
    }

    /**
     * @return $this
     */
    private function setRootId()
    {
        $this->request->request->add(['root_id' => 0]);
        if ($parent_id = $this->request->get("parent_id")) {
            $parent = Category::enable()->find($parent_id);
            if ($parent) {

                $this->request->request->add([
                    'instance_id' => $parent->instance_id,
                    'language_id' => $parent->language_id,
                    'root_id' => $parent->root_id,
                ]);
            }
        } else {
            $lastRoot = Category::enable()->orderBy('root_id', 'DESC')->first();
            if ($lastRoot) {
                $this->request->request->add(['root_id' => $lastRoot->root_id + 1]);
            }
        }
        return $this;
    }

    /**
     * @return $this
     */
    protected function changePosition()
    {
        $category_ids = array_values($this->request->only('prev', 'next', 'category_id', 'parent_id'));
        $categories = Category::enable()->whereIn('id', $category_ids)->get();

        if (!$categories)
            return $this;

        $sourceCategory = $categories->where('id', $this->request->get('category_id'))->first();
        $prevCategory = $categories->where('id', $this->request->get('prev'))->first();
        $nextCategory = $categories->where('id', $this->request->get('next'))->first();
        $parentCategory = $categories->where('id', $this->request->get('parent_id'))->first();

        // change parent
        $this->changeParent($sourceCategory, $parentCategory);

        //change sort
        $categoriesSorting = $this->changeSorting($sourceCategory, $this->request->get('parent_id'), $prevCategory, $nextCategory);

        return $this->setResponse(true, trans("blog::messages.alert.save_success"));

    }

    /**
     * @param $category
     * @param $new_parent
     * @return bool
     */
    private function changeParent($category, $new_parent)
    {
        if ($category && $new_parent && $new_parent->id != $category->parent_id) {
            $category->parent_id = $new_parent->id;
            return $category->save();
        }
        return false;
    }

    /**
     * @param $category
     * @param $parent_id
     * @param null $prev
     * @param null $next
     * @return $this
     */
    private function changeSorting($category, $parent_id, $prev = null, $next = null)
    {
        $source_id = $category->id;
        $parentCategoryList = Category::enable()->where('parent_id', $parent_id)->orderBy('sort')->get();
        $categoriesSortingId = $parentCategoryList->pluck('id')->toArray();
        $collection = collect($categoriesSortingId);
        unset($categoriesSortingId[array_search($source_id, $collection->toArray())]);
        $collection = collect($categoriesSortingId)->values();
        $array = $collection->toArray();
        $offset = 0;
        $length = 0;
        if ($prev) {
            $offset = array_search($prev->id, $array) + 1;
        } elseif ($next) {
            $offset = array_search($next->id, $array);
        }

        $collection->splice($offset, $length, [$source_id]);
        $updatedCategoryId = $collection->each(function ($category_id, $index) {
            $categoryUpd = Category::enable()->find($category_id);
            $categoryUpd->sort = $index;
            $categoryUpd->save();
            return $category_id;
        });

        return $updatedCategoryId;
    }


    /**
     * @return $this
     */
    public function getCategoryByApi()
    {
        $q = false;
        $selected = false;
        $type = $this->request->get('_type');
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }
        $model = new Category();
        $model = $model->active()->filterCurrentInstance()->select('id', 'title as text');
        if ($q) {

            $model->where(function ($query) use ($q) {

                $query->where('title', 'LIKE', '%' . $q . '%');

            });
            $items = $model->get();

        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $items = $type == 'query' ? $model->take(20)->get() : collect([]);
        }

        return $this->setDataResponse($items)->setResponse(true);
    }

    /**
     * @return CategoryViewModel
     */
    protected function getRootCategories()
    {
        $model = new Category();
        $list = $model->setRequestItems($this->request->all())
            ->filters()->getRootCategory()->active()->get();
        $list = $this->decorateList($list);
        return $this->setDataResponse($list)->setResponse(true);
    }

    /**
     * @return CategoryViewModel
     */
    protected function getChildesCategories()
    {
        $model = new Category();
        $list = $model->setRequestItems($this->request->all())
            ->filters()->active()->get();
        $list = $this->decorateList($list);
//        $categories = $this->getNestable(Category::active()->where('parent_id', $parent_id)->withCount('blogs')->get(), $parent_id, null);
        return $this->setDataResponse($list)->setResponse(true);
    }


}
