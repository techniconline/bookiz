<?php

namespace Modules\Blog\Traits\Blog;


trait BlogCategoryRelation
{

    public function scopeIsVisible($query)
    {
        $query = $query->join("blogs", function ($join) {
            $join->on('blogs.id', '=', $this->getTable() . '.blog_id')->where("blogs.active", 1);
        })->join("blog_categories", function ($join) {
            $join->on('blog_categories.id', '=', $this->getTable() . '.category_id')->where("blog_categories.active", 1);
        });
        return $query;
    }

    public function scopeSortBy($query)
    {
        if ($sort_by = $this->getRequestItem("sort_by")) {
            $sArr = explode("_", $sort_by);
            foreach ($sArr as &$item) {
                $item = ucfirst($item);
            }
            $methodName = implode("", $sArr);
            if(method_exists($this, "getSortBy".$methodName)){
                $query = $this->{"getSortBy".$methodName}($query);
            }

        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    private function getSortByNew($query)
    {
        $query = $query->whereHas("blog", function ($q){
            return $q->orderBy("created_at", "DESC");
        });
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    private function getSortByViewCount($query)
    {
        $query = $query->whereHas("blog", function ($q){
            return $q->orderBy("view_count", "DESC");
        });
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByCategories($query)
    {

        if ($category = $this->getRequestItem("category_id")) {

            $query = $query->whereHas("blogCategories", function ($q)use($category){
                if (is_array($category) && $category) {
                    $q = $q->whereIn("category_id", $category);
                } else {
                    $q = $q->where("category_id", $category);
                }
                return $q;
            });


        }
        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blogCategory()
    {
        return $this->belongsTo('Modules\Blog\Models\Blog\BlogCategory', 'category_id')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blogCategoryByFilter()
    {
        return $this->belongsTo('Modules\Blog\Models\Blog\BlogCategory', 'category_id')->active()->filterLanguage()->filterCurrentInstance();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blog()
    {
        return $this->belongsTo('Modules\Blog\Models\Blog')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blogByFilter()
    {
        return $this->belongsTo('Modules\Blog\Models\Blog')->active()->filterLanguage()->filterCurrentInstance();
    }
}
