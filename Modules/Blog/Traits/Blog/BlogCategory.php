<?php

namespace Modules\Blog\Traits\Blog;


use Illuminate\Database\Eloquent\Collection;

trait BlogCategory
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'title.required' => trans('blog::errors.blog.title_required'),
            'language_id.required' => trans('blog::errors.blog.language_id_required'),
            'instance_id.required' => trans('blog::errors.blog.instance_id_required'),
            'root_id.required' => trans('blog::errors.blog.root_id_required'),

        ];
        return $this;
    }



    /**
     * @return mixed
     */
    public function getBlogsCountAttribute()
    {
        $cats = $this->getChildesId($this->id);
        $count = \Modules\Blog\Models\Blog\BlogCategoryRelation::whereIn("category_id",$cats )->with("blog")->filterRelations(["blog"])->count("blog_id");
        return $count;
    }

    /**
     * @param $value
     * @return array|mixed
     */
    public function getAttributesAttribute($value)
    {
        if ($value) {
            $items = json_decode($value);
            $items = collect($items)->map(function ($value, $key){
                if(strpos($key, "icon")!==false){
                    return $value?:"fas fa-caret-left";
                }elseif(strpos($key, "color")!==false){
                    return $value?:"#DDDDDD";
                }
                return $value?:"fas fa-caret-left";
            });
            return $items;
        } else {
            return $this->getAttributesDefault();
        }
    }

    /**
     * @return array
     */
    private function getAttributesDefault()
    {
        return [
            "btn_icon" => "fas fa-caret-left",
            "btn_font_color" => "#FFFFFF",
            "btn_background_color" => "#FF3333",
        ];
    }

    /**
     * @return int
     */
    public function getChildesCountAttribute()
    {
        return $this->categoryChildes()->count();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeGetRootCategory($query)
    {
        return $query->whereNull("parent_id");
    }

    /**
     * @return string
     */
    public function getUrlGetChildesAttribute()
    {
        return route("blog.category.api.get_childes_categories", ["parent_id" => $this->id]);
    }

    public function getUrlBlogsAttribute()
    {
        return route("blog.category.list.blogs", ["id" => $this->id, "alias" => $this->alias?:str_replace(" ","-",$this->title)]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentBlogCategory()
    {
        return $this->belongsTo('Modules\Blog\Models\Blog\BlogCategory', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentRecursive()
    {
        return $this->parentBlogCategory()->with('parentRecursive');
    }

    /**
     * @param $category_id
     * @return null
     */
    public function getRootParent($category_id)
    {
        $results = $this->where("id", $category_id)->with("parentRecursive")->first();
        return $this->getParent($results);
    }

    /**
     * @param $parent
     * @return null
     */
    private function getParent($parent)
    {
        if ($parent->parent_id === null){
            return $parent;
        }else{
            return $this->getParent($parent->parentRecursive);
        }
        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function blogs()
    {
        return $this->hasMany('Modules\Blog\Models\Blog', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoryChildes()
    {
        return $this->hasMany('Modules\Blog\Models\Blog\BlogCategory', 'parent_id', 'id')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childrenRecursive()
    {
        return $this->categoryChildes()->with('childrenRecursive');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function blogsCategories()
    {
        return $this->belongsToMany('Modules\Blog\Models\Blog', 'blog_category_relations', 'category_id');
    }

    /**
     * @param $category_id
     * @return mixed
     */
    public function getChildesId($category_id)
    {
        $result = $this->where("id", $category_id)->with("childrenRecursive")->get();
        $result = $this->getIds($result);
        return $result;
    }

    /**
     * @param Collection $data
     * @return \Illuminate\Support\Collection|static
     */
    private function getIds(Collection $data)
    {
        return $data->map(function ($category) {
            $cat[] = $category->id;
            if ($category->childrenRecursive) {
                $cat[] = $this->getIds($category->childrenRecursive);
            }
            return $cat;
        })->flatten();
    }

}
