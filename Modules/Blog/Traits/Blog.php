<?php

namespace Modules\Blog\Traits;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;

trait Blog
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [
            'title.required' => trans('blog::errors.blog.title_required'),
            'language_id.required' => trans('blog::errors.blog.language_id_required'),
            'instance_id.required' => trans('blog::errors.blog.instance_id_required'),
            'category_id.required' => trans('blog::errors.blog.category_id_required'),
            'type.required' => trans('blog::errors.blog.instance_id_required'),

        ];
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getParamsJsonAttribute()
    {
        if($this->params){
            return json_decode($this->params);
        }
        return null;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeSearching($query)
    {
        $query = $query->filterCurrentInstance();
        $query = $query->filterLanguage();
        if ($term = $this->getRequestItem('text')) {
            $query = $query->where(function ($q) use ($term) {
                $q->orWhere('title', 'LIKE', '%' . $term . '%');
                $q->orWhere('short_description', 'LIKE', '%' . $term . '%');
            });
        }

        return $query;
    }


    /**
     * @return mixed
     */
    public function getRateAttribute()
    {
        return BridgeHelper::getRate()->getRateObject()->setModel($this->getTable(), $this->id)->setWithData(true)->init()->getData();
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getImageAttribute($value)
    {
        return strlen($value) ? url($value) : ((BridgeHelper::getConfig()->getSettings('default_poster', 'instance', 'blog'))?url(BridgeHelper::getConfig()->getSettings('default_poster', 'instance', 'blog')):$value);
    }
    
    /**
     * @return string
     */
    public function getUrlSingleBlogAttribute()
    {
        return route("blog.view", ["blog_id" => $this->id]);
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        $newTypes = [];

        foreach ($this->types as $type) {
            $newTypes[$type] = trans("blog::blog." . $type);
        }
        return $newTypes;
    }

    public function scopeSortBy($query)
    {
        $sorted = false;
        if ($sort_by = $this->getRequestItem("sort_by")) {
            $sArr = explode("_", $sort_by);
            foreach ($sArr as &$item) {
                $item = ucfirst($item);
            }
            $methodName = implode("", $sArr);
            if(method_exists($this, "getSortBy".$methodName)){
                $sorted = true;
                $query = $this->{"getSortBy".$methodName}($query);
            }

        }
        if(!$sorted){
            return $query->orderBy("created_at", "DESC");;
        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    private function getSortByNew($query)
    {
        return $query->orderBy("created_at", "DESC");
    }

    /**
     * @param $query
     * @return mixed
     */
    private function getSortByViewCount($query)
    {
        return $query->orderBy("view_count", "DESC");
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByCategories($query)
    {

        if ($category = $this->getRequestItem("category_id")) {

            $query = $query->whereHas("blogCategories", function ($q)use($category){
                if (is_array($category) && $category) {
                    $q = $q->whereIn("category_id", $category);
                } else {
                    $q = $q->where("category_id", $category);
                }
                return $q;
            });


        }
        return $query;
    }
    
    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getActiveTextAttribute()
    {
        return trans('blog::blog.statuses.' . $this->active);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo('Modules\Core\Models\Instance\Instance');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Modules\Core\Models\Language\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blogCategory()
    {
        return $this->belongsTo('Modules\Blog\Models\Blog\BlogCategory', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function blogCategories()
    {
        return $this->belongsToMany('Modules\Blog\Models\Blog\BlogCategory', 'blog_category_relations', null, 'category_id');
    }

}
