<?php

namespace Modules\Blog\Models\Blog;


use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Blog\Traits\Blog\BlogCategory as TraitModel;

class BlogCategory extends BaseModel
{

    use TraitModel;
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['parent_id', 'instance_id', 'language_id', 'root_id', 'title', 'short_description', 'alias', 'key_trans', 'attributes', 'sort', 'active', 'deleted_at', 'created_at', 'updated_at'];
    public $api_fields = ['id', 'parent_id', 'root_id', 'title', 'short_description', 'alias', 'key_trans', 'attributes', 'sort'];
    public $list_fields = ['id', 'parent_id', 'root_id', 'title', 'short_description', 'alias', 'key_trans', 'attributes', 'sort'];

    public $api_append_fields = ['url_get_childes', 'url_blogs'];
    public $list_append_fields = ['url_get_childes', 'url_blogs', 'blogs_count', 'childes_count'];

    public $messages = [];
    public $rules = [
        'title' => 'required',
        'instance_id' => 'required',
        'language_id' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $appends = ['url_get_childes'];
}
