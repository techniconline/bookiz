<?php

namespace Modules\Blog\Models\Blog;

use Modules\Core\Models\BaseModel;
use Modules\Blog\Traits\Blog\BlogCategoryRelation as TraitModel;

class BlogCategoryRelation extends BaseModel
{
    use TraitModel;
    /**
     * @var array
     */
    protected $fillable = ['blog_id', 'category_id'];

}
