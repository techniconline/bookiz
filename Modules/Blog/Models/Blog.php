<?php

namespace Modules\Blog\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Blog\Traits\Blog as TraitModel;

class Blog extends BaseModel
{
    use TraitModel;
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'language_id', 'category_id', 'title', 'slug', 'short_description', 'description'
        , 'type', 'image', 'active_date', 'expire_date', 'params', 'view_count', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $api_fields = ['id', 'title', 'slug', 'short_description', 'image'];
    public $list_fields = ['id', 'title', 'slug', 'short_description', 'image'];
    public $api_append_fields = [];
    public $list_append_fields = [];

    const SYSTEM_NAME = 'blog';
    public $messages = [];
    public $rules = [
        'title' => 'required',
        'language_id' => 'required',
        'instance_id' => 'required',
        'category_id' => 'required',
        'active' => 'required',
    ];

    public $types = ["single"];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $appends = ['active_text'];

}
