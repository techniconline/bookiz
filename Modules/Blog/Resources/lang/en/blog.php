<?php

return [
    'list' => 'لیست مطالب',
    'blog_view' => 'مشاهده مطلب',
    'socials_title' => 'با دوستان خود به اشتراک بگذارید',
    'instances' => 'سامانه های سیستم',
    'languages' => 'زبانهای سیستم',
    'select_language' => 'انتخاب زبان',
    'select_instance' => 'انتخاب سامانه',
    'select_status' => 'انتخاب وضعیت',
    'select_file' => 'انتخاب فایل',
    'confirm_delete' => 'آیا از حذف اطمینان دارید؟',
    'delete' => 'حذف مطلب',
    'add' => 'افزودن مطلب',
    'edit' => 'ویرایش مطلب',
    'image_url' => 'تصویر',
    'title' => 'عنوان',
    'slug' => 'مشخصه مطلب',
    'short_description' => 'توضیح کوتاه',
    'description' => 'توضیحات مطلب',
    'type' => 'نوع مطلب',
    "single" => "تک صفحه",

    'active_date' => 'تاریخ فعال شدن',
    'expire_date' => 'تاریخ غیر فعال شدن',
    'created_at' => 'تاریخ ایجاد',
    'view_count' => 'تعداد بازدید',
    'categories' => 'دسته بندی ها',
    'category' => 'دسته بندی',
    'select_category' => 'انتخاب دسته بندی',
    'select_type' => 'انتخاب نوع',
    'sort' => 'مرتب سازی (الویت)',
    'sortable' => 'مرتب سازی',
    'sort_items' => [
        "new" => "جدیدترین",
        "view_count" => "پربازدید ها",
    ],
    'all_categories' => 'همه دسته بندی ها',

    'instance' => 'مشخصه سامانه (instance)',
    'cancel' => 'انصراف',
    'submit' => 'ثبت',
    'status' => 'وضعیت',
    'statuses' => [
        "0" => "حذف شده",
        "1" => "فعال",
        "2" => "غیرفعال",
    ],

    'configs' => 'تنظیمات',
    'config_items' => [
        'access_comment' => 'دسترسی نظر دهی',
        'access_comments' => [
            "all" => "همه",
            "only_user" => "کاربران",
            "disable" => "بسته باشد",
        ],
        'confirm_comment' => 'تایید نظرات',
        'confirm_comments' => [
            "by_admin" => "توسط ادمین",
            "all_accept" => "بدون تایید",
        ],
        'access_rate' => 'دسترسی امتیاز دهی',
        'access_rates' => [
            "all" => "همه",
            "only_user" => "کاربران",
            "disable" => "بسته باشد",
        ],
    ],


];