<?php

return [
    'group' => [
        'blogs' => 'مدیریت مطالب',
    ],
    'config'=>[
      'instance'=>'تنظیمات مطلب',
    ],
    'categories_list' => 'لیست شاخه ها',
    'blogs_list' => 'لیست مطالب',

];