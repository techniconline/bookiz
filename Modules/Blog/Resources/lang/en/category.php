<?php

return [
    'categories_list'=>'لیست شاخه ها',
    'categories_root_list'=>'لیست شاخه های اصلی',
    'list_category'=>'لیست زیر مجموعه :root_category',
    'instance_list'=>'سامانه های سیستم',
    'language'=>'زبانهای سیستم',
    'select_language'=>'انتخاب زبان',
    'confirm_delete'=>'آیا از حذف اطمینان دارید؟',
    'delete_category'=>'حذف شاخه',
    'add_category_root'=>'ایجاد شاخه اصلی',
    'add_category'=>'ایجاد شاخه',
    'edit_category'=>'ویرایش اطلاعات شاخه',
    'title'=>'عنوان',
    'short_description'=>'توضیح کوتاه',
    'alias'=>'مشخصه',
    'sort'=>'مرتب سازی (الویت)',
    'key_trans'=>'مشخصه ترجمه',
    'instance'=>'مشخصه سامانه (instance)',
    'cancel'=>'انصراف',
    'submit'=>'ثبت',
    'attributes'=>[
        "btn_background_color"=>"رنگ پشت لینک",
        "btn_font_color"=>"رنگ فونت",
        "btn_icon"=>"آیکون لینک",
    ],
];