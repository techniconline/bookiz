<?php

return [
    'module' => 'مطلب',
    'blogs_block'=>'بلاک مطالب',
    'blog_info_block'=>'بلاک اطلاعات مطلب',
    'advanced_block'=>'پیشرفته',
    'blog_category_block'=>'بلاک دسته بندی مطالب',
    'blog_category'=>'دسته بندی دوره ها',
    'advanced'=>'پیشرفته',
    'mobile'=>'موبایل',
];