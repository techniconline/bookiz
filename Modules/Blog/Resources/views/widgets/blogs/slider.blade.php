    {!! $blockViewModel->getConfig('before_html') !!}

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad_btn_50">
        <div class="container Hsticky ">
            <div class="row bg1 ">
                <h4 class="bgcolor5  brd-rad-05 Shadow3 ">
                    <span class="bgcolor2">
                        @if($image_logo=$blockViewModel->getConfig('logo_image'))
                        <img src="{{ url($image_logo) }}" alt="{{ $blockViewModel->getConfig('title') }}" />
                         @endif
                    </span>&nbsp;&nbsp;&nbsp;{{ $blockViewModel->getConfig('title') }}
                </h4>
            </div>
        </div>
        <div class="container  ">
            <div class="row">
                @foreach($blockViewModel->getBlogsList() as $blog)
                    <div class=" col-lg-3 col-md-4  col-sm-6 col-xs-12      ">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mrg_top_15 mrg_btn_10  ItemBx">
                            <a href="{{ route('blog.view',['id'=>$blog->id]) }}">
                            <div class="row bgcolor6 bx_5 brd-rad-05  NoOverflow ">
                                <img src="{{ $blog->image }}" class="_Full100precent2" />
                            </div>
                            <div class="row   bx_4  brd-rad-10 noSpan pad_10 ">
                                <h3 class="LimitedtString">
                                    {{ $blog->title }}
                                </h3>
                                <div>
                                    {{ $blog->short_description }}
                                </div>
                                <div class="row myR ">
                                    <h4 class="color1 fltr  mrg_right_05 _30percent">
{{--                                        {!! $blockViewModel->getCourseEnrollmentsHtml($blog->id) !!}--}}
                                    </h4>
                                </div>
                                <div class="row myR ">
                                    <h4 class="color1 fltr  mrg_right_30 _30percent">
                                        <i class="far fa-user color3"></i><span class="color12 mrg_right_05">
{{--                                            {!! $blockViewModel->getStudents($blog->id,true) !!}--}}
                                    </span>
                                    </h4>
                                    <h4 class="color1 fltr  mrg_right_30 _30percent">
                                        <i class="far fa-clock color3"></i><span class="color12 mrg_right_05">
{{--                                              {!! $blockViewModel->getDuration($blog->id,true) !!}--}}
                                    </span>
                                    </h4>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    {!! $blockViewModel->getConfig('after_html') !!}