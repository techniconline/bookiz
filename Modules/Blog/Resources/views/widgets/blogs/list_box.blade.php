    {!! $blockViewModel->getConfig('before_html') !!}
	
	
		
		
		
<div class="container "> 

    <div class="row  floatright    ">
	
		 <div class="  titlelogo   ">
            <h6>
                جدیدترین مقالات
            </h6>
        </div>
	
	
	
       
        <div class="container marg40_top  row-flex">
          
                @php $blogs = $blockViewModel->getBlogsListPaginate() @endphp
                @foreach($blogs as $item)
				
                       <div class=" col-xl-4 col-md-4 col-sm-6   floatright mrg10_1  row-flex">
                            <div class="item col-xl-12 col-md-12 col-sm-12  floatright blogList_ row-flex " >
                                <a href="{{ route('blog.view',['id'=>$item->id]) }}">
                                  
                                        <img src="{{ strlen($item->image)?$item->image:BridgeHelper::getConfig()->getSettings('default_poster','instance','blog') }}"
                                        class="_Full100precent2"  />
                                  
                                  
                                        <h4 class="LimitedtString">
                                            {{ $item->title }}
                                        </h4>   </a>
                                  <!--  <p>
									         {!! $item->short_description !!}</p> -->
 
                             
							 
							 
							 
							 <div class="floatigt   FullWidth btn_home_01 blogList_2 row-flex  ">
                           <a href="{{ route('blog.view',['id'=>$item->id]) }}">
                                     
                               
                                     مشاهده  مقاله
									 
                                </a>
                            </div>
							
							
							
                            </div>
                        </div>
				   

                   
                @endforeach
                <div class="paginate">
                    {!! $blogs->links() !!}
                </div>
        
        </div>
    
	
	
	</div>
	
	</div>
    {!! $blockViewModel->getConfig('after_html') !!}