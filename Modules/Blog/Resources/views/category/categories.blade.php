<div class="col-md-12 _filter" id="course_categories_list">
<i class="fas fa-align-justify _iconToper"></i>
 {!! FormHelper::label('label_name_group', trans("blog::blog.categories") , ['class'=>'']) !!} 
    <ul class="">
        <li class="_item_category_dept_0">
            <a class="_item_category cat_mainLink" href="{!! route("blog.category.list.blogs", ["id" => 0, "alias" => str_replace(" ","-",trans("blog::blog.all_categories"))]).'?'.http_build_query(request()->except("category_id")) !!}" >
                {{ trans("blog::blog.all_categories") }}
            </a>
            <ul>
                @foreach($viewModel->getBlogCategories() as $blog_category)
                    @include('blog::category.sub_categories',['dept'=>1])
                @endforeach
            </ul>
        </li>
    </ul>
</div>