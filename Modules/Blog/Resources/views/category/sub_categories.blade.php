@php $dept=isset($dept)?$dept:1; @endphp
<li class="_item_category_dept_{{ $dept }}">
{{--    {!! dd($blog_category) !!}--}}
    <a class="_item_category  {!! ($blog_category["id"] == $selected)?"active":"" !!}" href="{!! $blog_category["url_blogs"].'?'.http_build_query(request()->except("category_id")) !!}" >
        {{--<i class="{!! $blog_category["attributes"]["btn_icon"] !!}"></i>--}}
        {{ $blog_category["title"] }}
    </a>
    @if(isset($blog_category["childes"]) && $blog_category["childes"]->count())
        <ul class="submenu _sub_dept_{{ $dept }}">
            @foreach($blog_category["childes"] as $blog_category)
                @include('blog::category.sub_categories',['dept'=>$dept+1])
            @endforeach
        </ul>
    @endif
</li>