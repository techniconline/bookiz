<div class="row">
    <div class="col-md-12 padding-tb-10">
        <a href="{!! route('blog.category.index') !!}"
           class="btn btn-lg blue circle-right">
            <i class="fa fa-list"></i>
            @lang("blog::category.categories_root_list")
        </a>
    </div>
</div>
<div style="display: none">
    @include("blog::category.form")
</div>

{!!  \Modules\Blog\Providers\Helpers\Category\BlogCategoryHelperFacade::createTreeByNestable($categories)  !!}

