<div class="row" id="form_category">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("blog::category.add_category")
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">

                {!! FormHelper::open(['role'=>'form','url'=>(route("blog.category.save"))
                     ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="parent_id" value="0" class="_parent_id">
                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-10') @endphp

                <div class="col-md-6">
                    {!! FormHelper::input('text','title',old('title',null)
                    ,['required'=>'required','title'=>trans("blog::category.title"),'helper'=>trans("blog::category.title")]) !!}
                </div>
                <div class="col-md-6">
                    {!! FormHelper::input('text','alias',old('alias',null)
                    ,['required'=>'required','title'=>trans("blog::category.alias"),'helper'=>trans("blog::category.alias")]) !!}
                </div>

                <div class="col-md-6 _language_list">
                    {!! FormHelper::select('language_id',$languages,old('language_id',null)
                    ,['title'=>trans("blog::category.select_language"),'helper'=>trans("blog::category.select_language"),
                    'placeholder'=>trans("blog::category.select_language")]) !!}
                </div>

                <div class="col-md-6 _instance_list">
                    {!! FormHelper::select('instance_id',$instances,old('instance_id',null)
                    ,['title'=>trans("blog::category.instance"),'helper'=>trans("blog::category.instance"),
                    'placeholder'=>trans("blog::category.instance")]) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::input('text','sort',old('sort',1)
                    ,['title'=>trans("blog::category.sort"),'helper'=>trans("blog::category.sort")]) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::input('text','attributes[btn_icon]',old('attributes[btn_icon]',null)
                    ,['title'=>trans("blog::category.attributes.btn_icon"),'helper'=>'{fa fa-user} <i class="fa fa-user"></i>']) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::input('text','attributes[btn_font_color]',old('attributes[btn_font_color]',null)
                    ,['title'=>trans("blog::category.attributes.btn_font_color"),'helper'=>'#ff3333']) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::input('text','attributes[btn_background_color]',old('attributes[btn_background_color]',null)
                    ,['title'=>trans("blog::category.attributes.btn_background_color"),'helper'=>'#ff3333']) !!}
                </div>

                <div class="col-md-6">
                    {!! FormHelper::select('active',trans("blog::blog.statuses"),old('active',1)
                        ,['title'=>trans("blog::blog.status"),'helper'=>trans("blog::blog.select_status")
                         ,'placeholder'=>trans("blog::blog.select_status")]) !!}

                </div>

                <div class="col-md-12">
                    {!! FormHelper::textarea('short_description',old('short_description',null)
                         ,['title'=>trans("blog::category.short_description"),'helper'=>trans("blog::category.short_description")]) !!}
                </div>

                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("blog::category.submit")], ['title'=>trans("blog::category.cancel"), 'url'=>route('blog.category.index')]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}


            </div>
        </div>
    </div>
</div>