<div class="col-md-12" id="core.instance.config">
    <div class="portlet box blue ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cog"></i>
                {{ $BlockViewModel->getTitlePage() }}
            </div>
        </div>
        <div class="portlet-body form">
            {!! FormHelper::open(['role'=>'form','url'=>route('blog.config.instance.save'),'method'=>'POST','class'=>'form-horizontal','id'=>'config_form','enctype'=>"multipart/form-data"]) !!}
            @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp
            {!! FormHelper::legend(trans('blog::form.titles.defaults')) !!}
            {!! FormHelper::inputByButton('default_poster',old('default_poster',$BlockViewModel->getSetting('default_poster')),['type'=>'image','title'=>trans('blog::form.fields.default_poster'),'helper'=>trans('core::form.helper.image_url')],trans('blog::form.fields.default_poster'),[],true) !!}
            {!! FormHelper::openAction() !!}
            {!! FormHelper::submitOnly() !!}
            {!! FormHelper::closeAction() !!}
            {!! FormHelper::close() !!}
        </div>
    </div>
</div>