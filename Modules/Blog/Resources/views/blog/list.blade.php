@if(isset($viewModel->listBlogs) && $viewModel->listBlogs)
    <div class="container  ">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4">



                </div>

                <div class="col-md-8">

                    @php $listBlogs = $viewModel->listBlogs @endphp
                    @foreach($listBlogs as $blog)
                        <div class="col-lg-3 col-md-4  col-sm-6 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mrg_top_15 mrg_btn_10  ItemBx">
                                <a href="{{ route('blog.view',['id'=>$blog['id']]) }}">
                                    <div class="row bgcolor6 bx_5 brd-rad-05  NoOverflow ">
                                        <img src="{{ $blog['image'] }}"
                                             class="_Full100precent2"/>
                                    </div>
                                    <div class="row   bx_4  brd-rad-10 noSpan pad_10 ">
                                        <h3 class="LimitedtString">
                                            {{ $blog['title'] }}
                                        </h3>
                                        <div>
                                            {!! $blog['short_description'] !!}
                                        </div>
                                        <div class="spn_1">
                                            {!! BridgeHelper::getRate()->getRateObject()
                                                ->setModel("blogs", $blog['id'])
                                                ->setWithData(true)
                                                ->setWithForm(true)
                                                ->setWithSaveRate(false)
                                                ->setWithTextRate(false)
                                                ->setConfigs(["container_js" => "block_js", "container_css" => "block_style"])
                                                ->init()
                                                ->getForm()
                                                !!}
                                        </div>

                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                    <span class="paginate">
                        {!! $listBlogs->appends(\Illuminate\Support\Facades\Input::except('page'))->links() !!}
                    </span>

                </div>

            </div>
        </div>
    </div>
@endif

