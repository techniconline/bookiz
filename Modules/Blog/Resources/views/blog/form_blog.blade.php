<div class="row" id="form_blog">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("blog::blog.".($blog?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>

            <div class="portlet-body form">

                {!! FormHelper::open(['role'=>'form','url'=>(isset($blog) && $blog? route('blog.update', ['blog_id'=>$blog->id]) : route('blog.save'))
                        ,'method'=>(isset($blog) && $blog?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp


                {!! FormHelper::input('text','title',old('title',$blog?$blog->title:null)
                ,['required'=>'required','title'=>trans("blog::blog.title"),'helper'=>trans("blog::blog.title")]) !!}

                {!! FormHelper::input('text','slug',old('slug',$blog?$blog->slug:null)
                ,['title'=>trans("blog::blog.slug"),'helper'=>trans("blog::blog.slug")]) !!}

                {!! FormHelper::datetime('active_date',old('active_date',$blog?$blog->active_date:null)
                ,['title'=>trans("blog::blog.active_date"),'helper'=>trans("blog::blog.active_date")
                    ,'date-year-current'=>1,'date-year-before'=>2,'set-default'=>0]) !!}

                {!! FormHelper::datetime('expire_date',old('expire_date',$blog?$blog->expire_date:null)
                ,['title'=>trans("blog::blog.expire_date"),'helper'=>trans("blog::blog.expire_date")
                    ,'date-year-current'=>2,'date-year-before'=>2,'set-default'=>0]) !!}

                {!! FormHelper::select('instance_id',$instances?$instances->pluck('name','id')->toArray():[],old('instance_id',$blog?$blog->instance_id:null)
                ,['title'=>trans("blog::blog.instances"),'helper'=>trans("blog::blog.select_instance")
                ,'placeholder'=>trans("blog::blog.select_instance")]) !!}

                {!! FormHelper::selectTag('categories[]',[],old('categories',$blog?$activeCategories:null),['required'=>'required','multiple'=>'multiple', 'data-ajax-url'=>route('blog.category.get_category_by_api')
                     , 'title'=>trans("blog::blog.categories"),'helper'=>trans("blog::blog.select_category")]) !!}

                {!! FormHelper::select('type',$types,old('type',$blog?$blog->type:null)
                ,['title'=>trans("blog::blog.type"),'helper'=>trans("blog::blog.select_type")
                ,'placeholder'=>trans("blog::blog.select_type")]) !!}

                {!! FormHelper::select('active',trans("blog::blog.statuses"),old('active',$blog?$blog->active:1)
                ,['title'=>trans("blog::blog.status"),'helper'=>trans("blog::blog.select_status")
                ,'placeholder'=>trans("blog::blog.select_status")]) !!}

                {!! FormHelper::textarea('short_description',old('short_description',$blog?$blog->short_description:null)
                ,['title'=>trans("blog::blog.short_description"),'helper'=>trans("blog::blog.short_description")]) !!}

                {!! FormHelper::editor('description',old('description',$blog?$blog->description:null)
                ,['title'=>trans("blog::blog.description"),'helper'=>trans("blog::blog.description")]) !!}

                {!! FormHelper::inputByButton('image',old('image',$blog?$blog->image:null),['type'=>'image','title'=>trans('blog::blog.image_url'),'helper'=>trans('blog::blog.image_url')],trans('blog::blog.select_file'),[],true) !!}

                {!! FormHelper::legend(trans("blog::blog.configs")) !!}


                <div class="col-md-6">
                    {!! FormHelper::select('params[access_comment]',trans("blog::blog.config_items.access_comments"),old('params[access_comment]',isset($blog->params_json->access_comment)?$blog->params_json->access_comment:null)
                                    ,['title'=>trans("blog::blog.config_items.access_comment"),'helper'=>trans("blog::blog.config_items.access_comment")
                                    ,'placeholder'=>trans("blog::blog.config_items.access_comment")]) !!}
                </div>
                <div class="col-md-6">
                    {!! FormHelper::select('params[confirm_comment]',trans("blog::blog.config_items.confirm_comments"),old('params[confirm_comment]',isset($blog->params_json->confirm_comment)?$blog->params_json->confirm_comment:null)
                                    ,['title'=>trans("blog::blog.config_items.confirm_comment"),'helper'=>trans("blog::blog.config_items.confirm_comment")
                                    ,'placeholder'=>trans("blog::blog.config_items.confirm_comment")]) !!}
                </div>
                <div class="col-md-6">
                    {!! FormHelper::select('params[access_rate]',trans("blog::blog.config_items.access_rates"),old('params[access_rate]',isset($blog->params_json->access_rate)?$blog->params_json->access_rate:null)
                                    ,['title'=>trans("blog::blog.config_items.access_rate"),'helper'=>trans("blog::blog.config_items.access_rate")
                                    ,'placeholder'=>trans("blog::blog.config_items.access_rate")]) !!}
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancelWithLanguage(['title'=>trans("blog::blog.submit")], ['title'=>trans("blog::blog.cancel"), 'url'=>route('blog.manage.index')], ['selected'=>$blog?$blog->language_id:null]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>