@if(isset($viewModel->listBlogs) && $viewModel->listBlogs)

   <div class="FullWidth _course1 floatright  pad_ini1">
    <div class="container " >

             {!! $viewModel->renderSortableView($current_category) !!}
    </div>
</div>



        <div class="container   ">
          
               <div class="col-xl-3 col-md-4 col-sm-12 col-12 floatright">

			 
               
                    {!! $viewModel->renderCategoriesView($current_category) !!}
                </div>
                <div class="col-xl-9 col-md-8 col-sm-12 col-12 floatright pad_top2  row-flex">
                    @php $listBlogs = $viewModel->listBlogs @endphp
                    @foreach($listBlogs as $item)
                        <div class=" col-xl-6 col-md-6 col-sm-12   floatright mrg10_1  row-flex">
                            <div class="item col-xl-12 col-md-12 col-sm-12 col-12 floatright blogList_ row-flex " >
                                <a href="{{ route('blog.view',['id'=>$item->id]) }}">
                                  
                                        <img src="{{ strlen($item->image)?$item->image:BridgeHelper::getConfig()->getSettings('default_poster','instance','blog') }}"
                                        class="_Full100precent2"  />
                                  
                                  
                                        <h4 class="LimitedtString">
                                            {{ $item->title }}
                                        </h4>   </a>
                                    <p>
									         {!! $item->short_description !!}</p>
 
                             
							 
							 
							 
							 <div class="floatigt   FullWidth btn_home_01 blogList_2 row-flex  ">
                           <a href="{{ route('blog.view',['id'=>$item->id]) }}">
                                     
                               
                                     مشاهده  مقاله
									 
                                </a>
                            </div>
							
							
							
                            </div>
                        </div>
                    @endforeach
                    <div class="paginate">
                        {!! $listBlogs->links() !!}
                    </div>
                </div>
 

           
        </div>
   
@endif

