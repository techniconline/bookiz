@php $blog=$blockViewModel->getBlog() @endphp

<div class="container " >
  <div class="col-xl-3 col-md-4 col-sm-12 col-12 floatright blog4" >
	   
		  {!! $blockViewModel->getCategoryByBlogs()->renderCategoriesView() !!}
	
		  
		 
</div> 
 
    <div class="col-xl-9 col-md-8 col-sm-12 col-12 floatright " > 
	 <!--   <div class="col-xl-12 col-md-12 col-sm-12 col-12 floatright " >-->
		
		  <div class="col-xl-12 col-md-12 col-sm-12 col-12 floatright blog1" >
 
  
  <i class="fas fa-file-alt _iconToper"></i>
  
		  <h1>
		       {{ $blog->title }}
			   </h1>
		  
		  <div class='data_palce'>
		    <div class="col-lx-4 col-md-4 col-sm-12  floatright">
              <!--  @lang('blog::blog.created_at')  :-->انتشار: 
                {!! $blog->created_at !!} 
        </div>
		
		   
             <div class="col-lx-4 col-md-4 col-sm-12  floatright">
                @lang('blog::blog.category'): {!! $blog->blogCategory->title !!} 
        </div>
		   <div class="col-lx-4 col-md-4 col-sm-12  floatright">
             <!--  @lang('blog::blog.view_count') :                {!! $blog->view_count !!} -->
        </div>
          
		
		 </div>
		 
		    </div>
			
	  <div class="col-xl-12 col-md-12 col-sm-12 col-12 floatright blog2 " >
	    <!-- <img src="{{ $blog->image }}" class="img_blog_main" alt="{{ $blog->title }}">-->
		    <div class="Blogcontent"> {!! $blog->description !!}	  </div>
			
			
						
	  <div class="col-xl-12 col-md-12 col-sm-12 col-12 floatright shareBox   " >
	  <span>
	  اشتراک‌گذاری در شبکه‌های اجتماعی
	    </span>
		
		
		
					<a id='telegram' href="" target="_blank"><i class="fab fa-telegram"></i></a>
                    <a id='linkedin' href="" target="_blank"> <i class="fab fa-linkedin"></i> </a>
                    <a id='facebook' href="" target="_blank"><i class="fab fa-facebook-square"></i></a>
                    <a id='twitter' href="" target="_blank"><i class="fab fa-twitter-square"></i></a>
                    <a id='google' href="" target="_blank"><i class="fab fa-google-plus-square"></i></a>
                    <a id='sorosh' href="" target="_blank"><div class='soroshicon'></div></a>
			

	    </div>
		
		 <div class="col-xl-12 col-md-12 col-sm-12 col-12 floatright shareBox   " >
	  <span>
	 امتیازدهی
	    </span>
	     
   {!!      BridgeHelper::getRate()->getRateObject()
    ->setModel($blog->getTable(), $blog->id)
    ->setWithData(true)
    ->setWithForm(true)
    ->setWithSaveRate(true)
    ->setWithTextRate(true)
    ->setConfigs(["container_js" => "block_js", "container_css" => "block_style"])
    ->init()
    ->getForm()
     !!}
	    </div>
		
		
		
		
	  </div>
	  
	
	   


  <div class="col-xl-12 col-md-12 col-sm-12 col-12 floatright blog1" >
 
  <i class="fas fa-comment _iconToper"></i>
 
  
		  <h5>
		 نظرات کاربران
			   </h5>
		  
	 
		    </div>


  <div class="col-xl-12 col-md-12 col-sm-12 col-12 floatright blog2" >
		   
		 
		
		
	     {!!  BridgeHelper::getComment()->getCommentObject()->setModel($blog->getTable(), $blog->id)
        ->setWithData(true)
        ->setWithForm(true)
        ->setConfigs(["perPage" => 15, "container_js" => "block_js", "container_css" => "block_css" , "template" => "blog::feedback.comment.form_comment" , "template_list" => "blog::feedback.comment.list_comments"])
        ->init()
        ->getForm() !!}
	 
			
			
	</div>		
  

 </div>
 
 
 
 
 </div>
 
 
 <script>
 $( document ).ready(function() {
	 var link = window.location.href;
	 var title= $('head title').html();
    $('#telegram').attr("href","https://telegram.me/share/url?url="+link+"&text="+title);
    $('#linkedin').attr("href","https://www.linkedin.com/shareArticle?url="+link+"&title="+title);
    $('#facebook').attr("href","https://www.facebook.com/sharer.php?u="+link);
	$('#twitter').attr("href","https://twitter.com/intent/tweet?url="+link+"&text="+title);
	$('#google').attr("href","https://plus.google.com/share?url="+link+"&text="+title);
	$('#sorosh').attr("href","soroush://share?url="+link+"&text="+title);

});
 </script>