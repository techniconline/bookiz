<?php

return [
    'version' => '20180619001',
    'blocks' => [
        'left' => [],
        'top' => []
    ],
    'menus' => [
        'management' => [],
        'management_admin' => [
            [
                'alias' => '', //**
                'route' => 'blog.category.index', //**
                'key_trans' => 'blog::menu.categories_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-sitemap', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_blogs',
            ],

            [
                'alias' => '', //**
                'route' => 'blog.manage.index', //**
                'key_trans' => 'blog::menu.blogs_list', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-list', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_blogs',
            ],
            ['alias' => 'blog.config.instance.index', //**
                'route' => 'blog.config.instance.index', //**
                'key_trans' => 'blog::menu.config.instance', //**
                'order' => null, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-cog', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_manage_settings',
            ],

        ],
    ],
    'group_menu' => [
        'management' => [],
        'management_admin' => [
            ['alias' => 'admin_blogs', //**
                'key_trans' => 'blog::menu.group.blogs', //**
                'icon_class' => 'fa fa-edit',
                'before' => '',
                'after' => '',
                'order' => 10,
            ],
        ]
    ],

];
