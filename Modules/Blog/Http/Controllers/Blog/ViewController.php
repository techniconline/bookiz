<?php

namespace Modules\Blog\Http\Controllers\Blog;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class ViewController extends Controller
{

    /**
     * Show the specified resource.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function view($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['blog_id' => $id], $request)->setViewModel('blog.page')
            ->setActionMethod("viewBlog")->response();
    }

    /**
     * @param $category_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function listBlogs($category_id, $alias, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(["category_id"=>$category_id], $request)->setViewModel("blog.categoryByBlogs")
            ->setActionMethod("listBlogs")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(["category_id"=>0], $request)->setViewModel("blog.categoryByBlogs")
            ->setActionMethod("listBlogs")->response();
    }

}
