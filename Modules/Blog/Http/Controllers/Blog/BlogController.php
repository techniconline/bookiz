<?php

namespace Modules\Blog\Http\Controllers\Blog;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class BlogController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('blog')->setActionMethod("getViewListIndex")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(MasterViewModel $masterViewModel)
    {
        return $masterViewModel->setViewModel('blog')
            ->setActionMethod("createBlog")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('blog')
            ->setActionMethod("saveBlog")->response();
    }

    /**
     * Show the specified resource.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['blog_id' => $id], $request)->setViewModel('blog')
            ->setActionMethod("editBlog")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['blog_id' => $id], $request)->setViewModel('blog')
            ->setActionMethod("saveBlog")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['blog_id' => $id], $request)->setViewModel('blog')
            ->setActionMethod("destroyBlog")->response();
    }

    /**
     * @param $category_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getBlogsByCategory($category_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['category_id' => $category_id], $request)->setViewModel('blog')
            ->setActionMethod("getBlogsByCategory")->response();
    }

    public function show($blog_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['blog_id' => $blog_id], $request)->setViewModel('blog')
            ->setActionMethod("show")->response();
    }

}
