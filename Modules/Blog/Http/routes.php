<?php
Route::group(['middleware' => ['web'], 'prefix' => 'blog', 'as' => 'blog.', 'namespace' => 'Modules\Blog\Http\Controllers'], function () {

    Route::get('search', 'SearchController@search')->name('search');

    Route::group(['namespace' => '\Blog'], function () {
        Route::get('/', 'ViewController@index')->name('index');
        Route::get('/view/{id}', 'ViewController@view')->name('view');
//        Route::get('/view/{blog_id}/activity/{activity_id}', 'ViewController@activity')->name('activity');
//        Route::get('/view/{blog_id}/activity/{activity_id}/get', 'ViewController@getActivity')->name('getActivity');
        Route::get('category/{category_id}/blogs/{alias}', 'ViewController@listBlogs')->name('category.list.blogs');

    });

    Route::group(['prefix' => 'category', 'as' => 'category.', 'namespace' => '\Category'], function () {

        Route::group(['middleware' => 'api', 'as' => 'api.'], function () {
            Route::get('/get/root/list', 'CategoryController@getRootCategories')->name('get_root_categories');
            Route::get('/get/{parent_id}/childes/list', 'CategoryController@getChildesCategories')->name('get_childes_categories');
        });
    });


});
Route::group(['middleware' => ['web', 'auth', 'admin'], 'prefix' => 'blog', 'as' => 'blog.', 'namespace' => 'Modules\Blog\Http\Controllers'], function () {

    Route::group(['prefix' => 'config', 'as' => 'config.', 'namespace' => '\Config'], function () {
        Route::get('/instance/index', 'InstanceController@index')->name('instance.index');
        Route::post('/instance/save', 'InstanceController@save')->name('instance.save');
    });

    Route::group(['namespace' => '\Blog', 'prefix' => 'manage'], function () {
        Route::get('/', 'BlogController@index')->name('manage.index');
        Route::get('/create', 'BlogController@create')->name('create');
        Route::get('/{blog_id}/edit', 'BlogController@edit')->name('edit');
        //Route::get('/{blog_id}/show', 'BlogController@show')->name('show');
        Route::post('/save', 'BlogController@store')->name('save');
        Route::put('/{blog_id}/update', 'BlogController@update')->name('update');
        Route::delete('/{blog_id}/delete', 'BlogController@destroy')->name('delete');

        Route::group(['middleware' => 'api', 'as' => 'api.',], function () {
            Route::get('/get/{category_id}/list', 'BlogController@getBlogsByCategory')->name('get_blogs_by_category');
        });


    });

    Route::group(['prefix' => 'category', 'as' => 'category.', 'namespace' => '\Category'], function () {
        Route::get('/', 'CategoryController@index')->name('index');
        Route::get('/create', 'CategoryController@create')->name('create');
        Route::get('/api/get', 'CategoryController@getCategoryByApi')->name('get_category_by_api');
        Route::post('/save', 'CategoryController@store')->name('save');
        Route::post('/add/child/{parent_id}', 'CategoryController@addChild')->name('add_child');
        Route::put('/{category_id}/change_position', 'CategoryController@changePosition')->name('change_position');
        Route::put('/{category_id}/update', 'CategoryController@update')->name('update');
        Route::get('/{category_id}/show', 'CategoryController@show')->name('show');
        Route::delete('/{category_id}/delete', 'CategoryController@destroy')->name('delete');

    });

});
