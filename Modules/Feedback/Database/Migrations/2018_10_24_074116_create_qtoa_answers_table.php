<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQtoaAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qtoa_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('question_id');
            $table->longText('answer');
            $table->string('media')->nullable();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('liked')->nullable()->default(0);
            $table->unsignedInteger('dislike')->nullable()->default(0);
            $table->unsignedInteger('views')->nullable()->default(0);
            $table->boolean('is_hot')->nullable()->default(0);
            $table->boolean('published')->nullable()->default(0)->index();
            $table->tinyInteger('active')->nullable()->default(1)->index();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('qtoa_answers', function (Blueprint $table) {

            $table->foreign('question_id')->references('id')->on('qtoa_questions')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qtoa_answers', function (Blueprint $table) {
            $table->dropForeign('qtoa_answers_question_id_foreign');
            $table->dropForeign('qtoa_answers_user_id_foreign');
        });
        Schema::dropIfExists('qtoa_answers');
    }
}
