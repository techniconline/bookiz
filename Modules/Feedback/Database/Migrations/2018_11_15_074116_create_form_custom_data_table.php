<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormCustomDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_custom_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('form_custom_id')->index();
            $table->string('user_name')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->longText('data')->nullable()->comment('json data');
            $table->tinyInteger('active')->default(1)->index();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('form_custom_data', function (Blueprint $table) {
            $table->foreign('form_custom_id')->references('id')->on('form_customs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('form_custom_data', function (Blueprint $table) {
            $table->dropForeign('form_custom_data_form_custom_id_foreign');
        });
        Schema::dropIfExists('form_custom_data');
    }
}
