<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormCustomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_customs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('instance_id');
            $table->string('name');
            $table->string('alias');
            $table->text('description')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('form_customs', function (Blueprint $table) {
            $table->foreign('instance_id')->references('id')->on('instances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('form_customs', function (Blueprint $table) {
            $table->dropForeign('form_customs_instance_id_foreign');
        });
        Schema::dropIfExists('form_customs');
    }
}
