<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQtoaGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qtoa_grades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('question_id')->index();
            $table->unsignedBigInteger('answer_id')->nullable()->index();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('vote')->nullable()->default(0);
            $table->unsignedInteger('views')->nullable()->default(0);
            $table->tinyInteger('active')->nullable()->default(1)->index();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('qtoa_grades', function (Blueprint $table) {

            $table->foreign('question_id')->references('id')->on('qtoa_questions')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('answer_id')->references('id')->on('qtoa_answers')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qtoa_grades', function (Blueprint $table) {
            $table->dropForeign('qtoa_grades_question_id_foreign');
            $table->dropForeign('qtoa_grades_answer_id_foreign');
            $table->dropForeign('qtoa_grades_user_id_foreign');
        });
        Schema::dropIfExists('qtoa_grades');
    }
}
