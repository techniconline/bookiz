<?php

namespace Modules\Feedback\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Module\Module;

class ModuleTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('currencies')->delete();
		// CurrenciesSeeder
        $module=new Module();
        $module->fill(array(
				'name' =>'feedback',
				'version' => '2017010101',
			))->save();

	}
}