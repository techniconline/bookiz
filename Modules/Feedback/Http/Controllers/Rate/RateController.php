<?php

namespace Modules\Feedback\Http\Controllers\Rate;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\MasterViewModel;

class RateController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        $user = BridgeHelper::getAccess()->getUser();
        return $masterViewModel->setItemsRequest(["user_id" => $user ? $user->id : null], $request)->setViewModel('rate')
            ->setActionMethod("saveRate")->response();
    }

    /**
     * @param $model_type
     * @param $model_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeRate($model_type, $model_id, MasterViewModel $masterViewModel, Request $request)
    {
        $user = BridgeHelper::getAccess()->getUser();
        return $masterViewModel->setItemsRequest(["user_id" => $user ? $user->id : null, 'model_type' => $model_type, 'model_id' => $model_id], $request)
            ->setViewModel('rate')
            ->setActionMethod("saveRate")->response();
    }

    /**
     * Show the specified resource.
     * @param $model_type
     * @param $model_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getRates($model_type, $model_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['model_type' => $model_type], $request)
            ->setItemsRequest(['model_id' => $model_id])
            ->setViewModel('rate')
            ->setActionMethod("getRates")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['rate_id' => $id], $request)->setViewModel('rate')
            ->setActionMethod("destroyRate")->response();
    }

}
