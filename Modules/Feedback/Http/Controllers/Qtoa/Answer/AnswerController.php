<?php

namespace Modules\Feedback\Http\Controllers\Qtoa\Answer;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\MasterViewModel;

class AnswerController extends Controller
{

    /**
     * @param $question_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($question_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $question_id], $request)->setViewModel('qtoa.answer.answerGrid')->setActionMethod("getListGrid")->response();
    }

    /**
     * @param $question_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create($question_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $question_id], $request)->setViewModel('qtoa.answer.answer')->setActionMethod("createAnswer")->response();
    }

    /**
     * @param $answer_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($answer_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['answer_id' => $answer_id], $request)->setViewModel('qtoa.answer.answer')->setActionMethod("editAnswer")->response();
    }

    /**
     * @param $answer_id
     * @param $state_publish
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePublish($answer_id, $state_publish, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['answer_id' => $answer_id, 'state_publish' => $state_publish], $request)->setViewModel('qtoa.answer.answer')->setActionMethod("changePublish")->response();
    }

    /**
     * @param $model_type
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function search($model_type, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['model_type' => $model_type], $request)->setViewModel('qtoa.answer.answer')->setActionMethod("searchInModel")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('qtoa.answer.answer')->setActionMethod("saveAnswer")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['answer_id' => $id], $request)->setViewModel('qtoa.answer.answer')->setActionMethod("saveAnswer")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['answer_id' => $id], $request)->setViewModel('qtoa.answer.answer')->setActionMethod("destroyAnswer")->response();
    }

    /**
     * @param $question_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getList($question_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $question_id], $request)->setViewModel('qtoa.answer.answerGrid')->setActionMethod("getList")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteAll(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('qtoa.question')->setActionMethod("deleteAll")->response();
    }

    /**
     * @param $state_publish
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePublishAll(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['state_publish' => 'active'], $request)->setViewModel('qtoa.answer.answer')->setActionMethod("changePublishAll")->response();
    }

    /**
     * @param $state_publish
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeUnPublishAll(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['state_publish' => 'disable'], $request)->setViewModel('qtoa.answer.answer')->setActionMethod("changePublishAll")->response();
    }

    /**
     * @param $question_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function audioSave($question_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $question_id],$request)->setViewModel('qtoa.answer.answer')->setActionMethod("audioSave")->response();
    }



}
