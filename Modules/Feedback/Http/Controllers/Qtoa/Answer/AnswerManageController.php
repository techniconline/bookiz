<?php

namespace Modules\Feedback\Http\Controllers\Qtoa\Answer;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\MasterViewModel;

class AnswerManageController extends Controller
{

    /**
     * @param $question_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($question_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $question_id], $request)->setViewModel('qtoa.answer.answerGridManage')->setActionMethod("getListGrid")->response();
    }

    /**
     * @param $model_type
     * @param $model_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getListAnswersGrid($model_type, $model_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['model_type' => $model_type, 'model_id' => $model_id], $request)->setViewModel('qtoa.answer.answerGridManage')->setActionMethod("getListAnswersGrid")->response();
    }

    /**
     * @param $question_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create($question_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $question_id], $request)->setViewModel('qtoa.answer.answerManage')->setActionMethod("createAnswer")->response();
    }

    /**
     * @param $answer_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($answer_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['answer_id' => $answer_id], $request)->setViewModel('qtoa.answer.answerManage')->setActionMethod("editAnswer")->response();
    }

    /**
     * @param $answer_id
     * @param $state_publish
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePublish($answer_id, $state_publish, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['answer_id' => $answer_id, 'state_publish' => $state_publish], $request)->setViewModel('qtoa.answer.answerManage')->setActionMethod("changePublish")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('qtoa.answer.answerManage')->setActionMethod("saveAnswer")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['answer_id' => $id], $request)->setViewModel('qtoa.answer.answerManage')->setActionMethod("saveAnswer")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['answer_id' => $id], $request)->setViewModel('qtoa.answer.answerManage')->setActionMethod("destroyAnswer")->response();
    }


}
