<?php

namespace Modules\Feedback\Http\Controllers\Qtoa;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\MasterViewModel;

class QuestionController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('qtoa.questionGrid')->setActionMethod("getListGrid")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setViewModel('qtoa.question')->setActionMethod("createQuestion")->response();
    }

    /**
     * @param $question_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($question_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $question_id], $request)->setViewModel('qtoa.question')->setActionMethod("editQuestion")->response();
    }

    /**
     * @param $question_id
     * @param $state_publish
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePublish($question_id, $state_publish, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $question_id, 'state_publish' => $state_publish], $request)->setViewModel('qtoa.question')->setActionMethod("changePublish")->response();
    }

    /**
     * @param $model_type
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function search($model_type, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['model_type' => $model_type], $request)->setViewModel('qtoa.question')->setActionMethod("searchInModel")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('qtoa.question')->setActionMethod("saveQuestion")->response();
    }

    /**
     * Show the specified resource.
     * @param $model_type
     * @param $model_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getQuestions($model_type, $model_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['model_type' => $model_type, 'model_id' => $model_id], $request)->setViewModel('qtoa.question')->setActionMethod("getQuestions")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $id], $request)->setViewModel('qtoa.question')->setActionMethod("saveQuestion")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $id], $request)->setViewModel('qtoa.question')->setActionMethod("destroyQuestion")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteAll(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('qtoa.question')->setActionMethod("deleteAll")->response();
    }

    /**
     * @param $state_publish
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePublishAll(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['state_publish' => 'active'], $request)->setViewModel('qtoa.question')->setActionMethod("changePublishAll")->response();
    }

    /**
     * @param $state_publish
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeUnPublishAll(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['state_publish' => 'disable'], $request)->setViewModel('qtoa.question')->setActionMethod("changePublishAll")->response();
    }


}
