<?php

namespace Modules\Feedback\Http\Controllers\Qtoa;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\MasterViewModel;

class QuestionManageController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('qtoa.questionGrid')->setActionMethod("getListGrid")->response();
    }

    /**
     * Show the specified resource.
     * @param $model_type
     * @param $model_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getListQuestionsGrid($model_type, $model_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['model_type' => $model_type, 'model_id' => $model_id], $request)->setViewModel('qtoa.questionGridManage')->setActionMethod("getListQuestionsGrid")->response();
    }

    /**
     * @param $model_type
     * @param $model_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create($model_type, $model_id,MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['model_type' => $model_type, 'model_id' => $model_id], $request)->setViewModel('qtoa.questionManage')->setActionMethod("createQuestion")->response();
    }

    /**
     * @param $question_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($question_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $question_id], $request)->setViewModel('qtoa.questionManage')->setActionMethod("editQuestion")->response();
    }

    /**
     * @param $question_id
     * @param $state_publish
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePublish($question_id, $state_publish, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $question_id, 'state_publish' => $state_publish], $request)->setViewModel('qtoa.questionManage')->setActionMethod("changePublish")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('qtoa.questionManage')->setActionMethod("saveQuestion")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $id], $request)->setViewModel('qtoa.questionManage')->setActionMethod("saveQuestion")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['question_id' => $id], $request)->setViewModel('qtoa.questionManage')->setActionMethod("destroyQuestion")->response();
    }

}
