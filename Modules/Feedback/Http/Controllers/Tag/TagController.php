<?php

namespace Modules\Feedback\Http\Controllers\Tag;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class TagController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setViewModel('tag')
            ->setActionMethod("getViewListIndex")->response();
    }


    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setViewModel('tag')
            ->setActionMethod("createTag")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getTagByApi(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['is_ajax'=>1],$request)->setViewModel('tag')
            ->setActionMethod("getTagByApi")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('tag')
            ->setActionMethod("saveTag")->response();
    }

    /**
     * Show the specified resource.
     * @param $tag_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($tag_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['tag_id' => $tag_id], $request)->setViewModel('tag')
            ->setActionMethod("editTag")->response();
    }


    /**
     * Update the specified resource in storage.
     * @param $tag_id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($tag_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['tag_id' => $tag_id], $request)->setViewModel('tag')
            ->setActionMethod("saveTag")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $tag_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($tag_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['tag_id' => $tag_id], $request)->setViewModel('tag')
            ->setActionMethod("destroyTag")->response();
    }

}
