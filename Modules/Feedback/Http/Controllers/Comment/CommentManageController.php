<?php

namespace Modules\Feedback\Http\Controllers\Comment;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class CommentManageController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('comment.commentGrid')->setActionMethod("getListGrid")->response();
    }

   
    /**
     * @param $comment_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($comment_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['comment_id' => $comment_id], $request)->setViewModel('comment.commentManage')->setActionMethod("showComment")->response();
    }

    /**
     * @param $comment_id
     * @param $state_publish
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePublish($comment_id, $state_publish, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['comment_id' => $comment_id, 'state_publish' => $state_publish], $request)->setViewModel('comment.commentManage')->setActionMethod("changePublish")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['comment_id' => $id], $request)->setViewModel('comment.commentManage')->setActionMethod("destroyComment")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteAll(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('comment.commentManage')->setActionMethod("deleteAll")->response();
    }

    /**
     * @param $state_publish
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePublishAll(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['state_publish' => 'active'], $request)->setViewModel('comment.commentManage')->setActionMethod("changePublishAll")->response();
    }

    /**
     * @param $state_publish
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeUnPublishAll(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['state_publish' => 'disable'], $request)->setViewModel('comment.commentManage')->setActionMethod("changePublishAll")->response();
    }


}
