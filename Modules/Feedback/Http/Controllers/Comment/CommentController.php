<?php

namespace Modules\Feedback\Http\Controllers\Comment;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\MasterViewModel;

class CommentController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MasterViewModel $masterViewModel, Request $request)
    {
        $user = BridgeHelper::getAccess()->getUser();
        return $masterViewModel->setItemsRequest(["user_id" => $user ? $user->id : null],$request)
            ->setViewModel('comment')
            ->setActionMethod("saveComment")->response();
    }

    /**
     * @param $model_type
     * @param $model_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeComment($model_type, $model_id, MasterViewModel $masterViewModel, Request $request)
    {
        $user = BridgeHelper::getAccess()->getUser();
        return $masterViewModel->setItemsRequest(["user_id" => $user ? $user->id : null, 'model_type' => $model_type, 'model_id' => $model_id], $request)
            ->setViewModel('comment')
            ->setActionMethod("saveComment")->response();
    }

    /**
     * Show the specified resource.
     * @param $model_type
     * @param $model_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getComments($model_type, $model_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['model_type' => $model_type], $request)->setItemsRequest(['model_id' => $model_id])
            ->setViewModel('comment')
            ->setActionMethod("getComments")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['comment_id' => $id], $request)->setViewModel('comment')
            ->setActionMethod("saveComment")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['comment_id' => $id], $request)->setViewModel('comment')
            ->setActionMethod("destroyComment")->response();
    }

}
