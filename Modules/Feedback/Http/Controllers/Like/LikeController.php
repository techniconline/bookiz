<?php

namespace Modules\Feedback\Http\Controllers\Like;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\MasterViewModel;

class LikeController extends Controller
{

    /**
     * @param $model_type Question, Answer, Course
     * @param $model_id
     * @param $like_value
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeLike($model_type, $model_id, $like_value, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['like_value' => $like_value, 'model_type' => $model_type, 'model_id' => $model_id], $request)
            ->setViewModel('like')->setActionMethod("saveLike")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function listLiked(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('like')->setActionMethod("listLike")->response();
    }

}
