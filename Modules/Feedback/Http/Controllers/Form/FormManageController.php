<?php

namespace Modules\Feedback\Http\Controllers\Form;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\ViewModels\MasterViewModel;

class FormManageController extends Controller
{

    /**
     * @param $form_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($form_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['form_id' => $form_id], $request)
            ->setViewModel('form.formGrid')->setActionMethod("getListGrid")->response();
    }

   
    /**
     * @param $form_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($form_id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['form_id' => $form_id], $request)->setViewModel('form.formManage')->setActionMethod("showComment")->response();
    }


    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['form_id' => $id], $request)->setViewModel('form.formManage')->setActionMethod("destroyComment")->response();
    }


}
