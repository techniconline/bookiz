<?php

namespace Modules\Feedback\Http\Controllers\Form;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\MasterViewModel;

class FormController extends Controller
{

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setRequest($request)->setViewModel('form.formGrid')->setActionMethod("getListGridForm")->response();
    }

    /**
     * @param $form_id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($form_id, MasterViewModel $masterViewModel, Request $request)
    {
        $user = BridgeHelper::getAccess()->getUser();
        return $masterViewModel->setItemsRequest(["user_id" => $user ? $user->id : null, "form_custom_id"=>$form_id],$request)
            ->setViewModel('form.page.form')
            ->setActionMethod("saveForm")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setViewModel('form')
            ->setActionMethod("createForm")->response();
    }


    /**
     * @param $alias
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($alias, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['alias' => $alias], $request)->setViewModel('form.page.form')
            ->setActionMethod("showForm")->response();
    }

    /**
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['form_id' => $id], $request)->setViewModel('form')
            ->setActionMethod("editForm")->response();
    }

    /**
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeForm(MasterViewModel $masterViewModel, Request $request)
    {
        $user = BridgeHelper::getAccess()->getUser();
        return $masterViewModel->setRequest($request)
            ->setViewModel('form')
            ->setActionMethod("saveForm")->response();
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['form_id' => $id], $request)->setViewModel('form')
            ->setActionMethod("saveForm")->response();
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param MasterViewModel $masterViewModel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, MasterViewModel $masterViewModel, Request $request)
    {
        return $masterViewModel->setItemsRequest(['form_id' => $id], $request)->setViewModel('form')
            ->setActionMethod("destroyForm")->response();
    }

}
