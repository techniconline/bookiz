<?php

Route::group(['middleware' => 'web', 'prefix' => 'feedback', 'as' => 'feedback.', 'namespace' => 'Modules\Feedback\Http\Controllers'], function () {

    //for comment
    Route::group(['prefix' => 'comment', 'as' => 'comment.', 'namespace' => '\Comment'], function () {

        Route::post('/save', 'CommentController@store')->name('save');
        Route::post('/{module_type}/{module_id}/save', 'CommentController@storeComment')->name('storeComment');
        Route::put('/{comment_id}/update', 'CommentController@update')->name('update');
        Route::get('/{module_type}/{module_id}/get', 'CommentController@getComments')->name('getComments');
        Route::delete('/{comment_id}/delete', 'CommentController@destroy')->name('delete');

        Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'manage', 'as' => 'manage.'], function () {
            Route::get('/', 'CommentManageController@index')->name('index');

            Route::get('/{comment_id}/show', 'CommentManageController@show')->name('show');
            Route::post('/{comment_id}/{state}/publish', 'CommentManageController@changePublish')->name('change_publish');
            Route::delete('/{comment_id}/delete', 'CommentManageController@destroy')->name('destroy');

            Route::post('/all/actions/active/publish', 'CommentManageController@changePublishAll')->name('change_publish_all');
            Route::post('/all/actions/disable/publish', 'CommentManageController@changeUnPublishAll')->name('change_unpublish_all');
            Route::post('/all/actions/delete', 'CommentManageController@deleteAll')->name('delete_all');

        });

    });

    //for form
    Route::group(['prefix' => 'form', 'as' => 'form.', 'namespace' => '\Form'], function () {

        Route::get('show/{alias}', 'FormController@show')->name('show');
        Route::post('data/{form_id}/save', 'FormController@store')->name('save');

        Route::group(['middleware' => ['auth', 'admin']], function () {
            Route::get('/', 'FormController@index')->name('index');
            Route::get('/create', 'FormController@create')->name('create');
            Route::post('/save', 'FormController@storeForm')->name('storeForm');
            Route::get('/{form_id}/edit', 'FormController@edit')->name('edit');
            Route::put('/{form_id}/update', 'FormController@update')->name('update');
            Route::get('/get', 'FormController@getForms')->name('getForms');
            Route::delete('/{form_id}/delete', 'FormController@destroy')->name('delete');

        });

        Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'manage', 'as' => 'manage.'], function () {

            Route::get('/{form_id}/', 'FormManageController@index')->name('index');
            Route::get('/{form_id}/show', 'FormManageController@show')->name('show');
            Route::delete('/{form_id}/delete', 'FormManageController@destroy')->name('destroy');

        });

    });


    //for rate
    Route::group(['prefix' => 'rate', 'as' => 'rate.', 'namespace' => '\Rate'], function () {

        Route::post('/save', 'RateController@store')->name('save');
        Route::post('/{module_type}/{module_id}/save', 'RateController@storeRate')->name('storeRate');
        Route::get('/{module_type}/{module_id}/get', 'RateController@getRates')->name('getRates');
        Route::delete('/{rate_id}/delete', 'RateController@destroy')->name('delete');

    });

    //for like
    Route::group(['prefix' => 'like', 'as' => 'like.', 'namespace' => '\Like'], function () {

        Route::post('/save', 'LikeController@store')->name('save');
        Route::post('/{module_type}/{module_id}/{value_like}/save', 'LikeController@storeLike')->name('storeLike');
        Route::get('/{module_type}/{module_id}/get', 'LikeController@getLikes')->name('getLikes');
        Route::get('/list/get', 'LikeController@listLiked')->name('listLiked');
        Route::delete('/{like_id}/delete', 'LikeController@destroy')->name('delete');

    });

    //for qtoa
    Route::group(['prefix' => 'qtoa', 'as' => 'qtoa.', 'namespace' => '\Qtoa'], function () {

        Route::get('/{model_type}/search', 'QuestionController@search')->name('search');

        Route::group(['prefix' => 'question', 'as' => 'question.'], function () {

            Route::get('/', 'QuestionController@index')->name('index');
            Route::get('/create', 'QuestionController@create')->name('create');
            Route::get('/{model_type}/{model_id}/getQuestions', 'QuestionController@getQuestions')->name('get_questions');
            Route::post('/save', 'QuestionController@store')->name('save');
            Route::get('/{question_id}/edit', 'QuestionController@edit')->name('edit');
            Route::put('/{question_id}/update', 'QuestionController@update')->name('update');
            Route::post('/{question_id}/{state}/publish', 'QuestionController@changePublish')->name('change_publish');
            Route::delete('/{question_id}/delete', 'QuestionController@destroy')->name('destroy');

            Route::post('/all/actions/active/publish', 'QuestionController@changePublishAll')->name('change_publish_all');
            Route::post('/all/actions/disable/publish', 'QuestionController@changeUnPublishAll')->name('change_unpublish_all');
            Route::post('/all/actions/delete', 'QuestionController@deleteAll')->name('delete_all');


            Route::group(['prefix' => 'manage', 'as' => 'manage.'], function () {
                Route::get('/{model_type}/{model_id}/create', 'QuestionManageController@create')->name('create');
                Route::get('/{model_type}/{model_id}/get_list_questions', 'QuestionManageController@getListQuestionsGrid')->name('get_list_questions');
                Route::post('/save', 'QuestionManageController@store')->name('save');
                Route::get('/{question_id}/edit', 'QuestionManageController@edit')->name('edit');
                Route::put('/{question_id}/update', 'QuestionManageController@update')->name('update');
                Route::post('/{question_id}/{state}/publish', 'QuestionManageController@changePublish')->name('change_publish');
                Route::delete('/{question_id}/delete', 'QuestionManageController@destroy')->name('destroy');
            });


            Route::group(['prefix' => 'answer', 'as' => 'answer.', 'namespace' => '\Answer'], function () {
                Route::get('{question_id}/list', 'AnswerController@index')->name('index');
                Route::get('{question_id}/create', 'AnswerController@create')->name('create');
                Route::get('{question_id}/getList', 'AnswerController@getList')->name('get_list');
                Route::post('/save', 'AnswerController@store')->name('save');

                Route::post('{question_id}/voice/save', 'AnswerController@audioSave')->name('voice.save');

                Route::get('/{answer_id}/edit', 'AnswerController@edit')->name('edit');
                Route::put('/{answer_id}/update', 'AnswerController@update')->name('update');
                Route::post('/{answer_id}/{state}/publish', 'AnswerController@changePublish')->name('change_publish');
                Route::delete('/{answer_id}/delete', 'AnswerController@destroy')->name('destroy');

                Route::post('/all/actions/active/publish', 'AnswerController@changePublishAll')->name('change_publish_all');
                Route::post('/all/actions/disable/publish', 'AnswerController@changeUnPublishAll')->name('change_unpublish_all');
                Route::post('/all/actions/delete', 'AnswerController@deleteAll')->name('delete_all');

                Route::group(['prefix' => 'manage', 'as' => 'manage.'], function () {
                    Route::get('{question_id}/list', 'AnswerManageController@index')->name('index');
                    Route::get('{model_type}/{model_id}/get_list_answers', 'AnswerManageController@getListAnswersGrid')->name('get_list_answers');

                    Route::post('/save', 'AnswerManageController@store')->name('save');
                    Route::get('/{answer_id}/edit', 'AnswerManageController@edit')->name('edit');
                    Route::put('/{answer_id}/update', 'AnswerManageController@update')->name('update');
                    Route::post('/{answer_id}/{state}/publish', 'AnswerManageController@changePublish')->name('change_publish');
                    Route::delete('/{answer_id}/delete', 'AnswerManageController@destroy')->name('destroy');

                });

            });

        });


    });

});
