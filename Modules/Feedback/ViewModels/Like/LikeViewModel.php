<?php

namespace Modules\Feedback\ViewModels\Like;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Feedback\Models\Like\Like;

class LikeViewModel extends BaseViewModel
{


    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return $this
     */
    protected function destroyLike()
    {
        $like_id = $this->request->get('like_id');
        $like = Like::active()->find($like_id);

        if ($like->delete()) {
            return $this->setResponse(true, trans("feedback::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.del_un_success"));

    }

    /**
     * @param int $like_value
     * @param null $model_id
     * @param null $model_type
     * @param null $user_id
     * @return $this
     */
    public function saveLike($like_value = null, $model_id = null, $model_type = null, $user_id = null)
    {
        $user_id = $user_id ?: 0;
        $user = BridgeHelper::getAccess()->getUser();
        $user_id = $user_id ?: ($user ? $user->id : 0);
        if (!$user_id) {
            return $this->setResponse(false, trans("feedback::messages.alert.not_access"));
        }
        $model_id = $model_id ?: $this->request->get("model_id");
        $model_type = $model_type ?: $this->request->get("model_type");
        $like_value = $like_value ?: isset($this->request) ? $this->request->get("like_value") : 0;
        $update = false;
        $like = Like::active()->where("user_id", $user_id)
            ->where("model_id", $model_id)
            ->where("model_type", $model_type)
            ->first();

        if (!$like) {
            $like = new Like();
        } else {
            if ($like_value == $like->like) {
                return $this->setResponse(false, trans("feedback::messages.alert.has_value"));
            } else {
                $update = true;
            }
        }

        $access = $this->checkAccess($model_type, $model_id);
        if (!$access) {
            return $this->setResponse(false, trans("feedback::messages.alert.not_access"));
        }

        $items = [
            "like" => $like_value,
            "user_id" => $user_id,
            "model_id" => $model_id,
            "model_type" => $model_type,
        ];

        if (($like->fill($items)->isValid())) {
            if ($like->save()) {
                $this->updateModel($model_type, $model_id, $like_value, $update);
                return $this->setDataResponse($like)->setResponse(true, trans("feedback::messages.alert.save_success"));
            } else {
                return $this->setResponse(false, trans("feedback::messages.alert.save_un_success"));
            }
        }

        return $this->setResponse(false, trans("feedback::messages.alert.mis_data"), $like->errors);
    }

    /**
     * @param $model_type
     * @param $model_id
     * @param $value_like
     * @param bool $update
     * @return bool
     */
    public function updateModel($model_type, $model_id, $value_like, $update = false)
    {
        $model = $this->getModelType($model_type);
        if (!$model) {
            return false;
        }

        if (method_exists($model, 'updateLike')) {
            return $model->updateLike($model_id, $value_like, $update);
        } else {
            return false;
        }

    }

    /**
     * @param $model_type
     * @param $model_id
     * @return bool
     */
    public function checkAccess($model_type, $model_id)
    {
        return true;
        $model = $this->getModelType($model_type);
        if (!$model) {
            return false;
        }
        $data = $model->find($model_id);

        $params = $data ? $data->params_json : null;
        if (!$params) {
            return false;
        }

        $access_like = isset($params->access_like) ? $params->access_like : "disable";

        $access = false;
        if ($access_like != "disable") {

            if ($access_like == "only_user") {
                $user = BridgeHelper::getAccess()->getUser();
                $access = $user ? true : false;
            } else {
                $access = true;
            }

        }

        return $access;
    }

    /**
     * @param $model_type
     * @return bool
     */
    private function getModelType($model_type)
    {
        $like = new Like();
        $sArr = explode("_", $model_type);
        foreach ($sArr as &$item) {
            $item = ucfirst($item);
        }
        $methodName = implode("", $sArr);
        if (method_exists($like, "get" . $methodName)) {
            return $like->{"get" . $methodName}();
        }
        return false;
    }


    /**
     * @return $this
     */
    protected function getLikes()
    {
        $likes = Like::active()
            ->filterByModel()
            ->get();
        if ($likes) {
            return $this->setDataResponse($likes)->setResponse(true);
        }
        return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));

    }

    /**
     * @param null $user
     * @param null $like
     * @return $this
     * @throws \Throwable
     */
    public function listLike($user = null, $like = null)
    {
        $user = $user ?: BridgeHelper::getAccess()->getUser();
        $user_id = $user ? $user->id : 0;
        $like = is_null($like)?$this->request->get('like'):$like;
        $model = new Like();
        $likes = $model->setRequestItems(request()->all())->active()
            ->filterByModel()
            ->filterByUser($user_id)
            ->where(function ($q)use($like){
                if (!is_null($like)){
                    if ($like){
                        $q->where('like',1);
                    }else{
                        $q->where('like',0);
                    }
                }
            })
            ->paginate();

        if (get_instance()->isApi()) {
            return $this->setDataResponse($likes)->setResponse(boolval($likes));
        }

        $this->setTitlePage(trans('feedback::like.list'));
        return $this->renderedView("feedback::like.form_like", ['likes' => $likes], "form");

    }


}
