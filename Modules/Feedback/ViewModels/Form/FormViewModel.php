<?php

namespace Modules\Feedback\ViewModels\Form;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Feedback\Models\Form\Form;
use Modules\Feedback\Models\FormCustom\FormCustom;

class FormViewModel extends BaseViewModel
{


    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createForm()
    {

        $form = new FormCustom();

        $form = $form->enable()->find($this->request->get('form_id'));

        $this->setTitlePage(trans('feedback::form.' . ($form ? 'edit' : 'add')));
        return $this->renderedView("feedback::form.form", ['form' => $form], "form");
    }

    /**
     * @throws \Throwable
     */
    protected function editForm()
    {
        return $this->createForm();
    }


    /**
     * @return $this
     */
    protected function destroyForm()
    {
        $form_id = $this->request->get('form_id');
        $form = FormCustom::active()->find($form_id);

        if ($form->delete()) {
            return $this->setResponse(true, trans("feedback::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveForm()
    {
        $form = new FormCustom();
        if ($form_id = $this->request->get("form_id")) {
            $form = $form->active()->find($form_id);
            if (!$form) {
                return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));
            }

        }

        $this->request->request->add(['instance_id' => app('getInstanceObject')->getCurrentInstanceId()]);

        if (($form->fill($this->request->all())->isValid())) {
            $form->active = 1;
            $form->save();
            $form->updated = $form_id ? true : false;
            return $this->redirect(route('feedback.form.edit', ['form_id' => $form->id]))->setResponse(true, trans("feedback::messages.alert.save_success"));

        }

        return $this->redirectBack()->setResponse(false, trans("feedback::messages.alert.mis_data"), $form->errors);

    }

    /**
     * @return $this
     */
    protected function getForms()
    {
        $form = new FormCustom();
        $forms = $form->setRequestItems($this->request->all())
            ->active()
            ->get();
        if ($forms) {
            return $this->setDataResponse($forms)->setResponse(true);
        }
        return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));

    }


}
