<?php

namespace Modules\Feedback\ViewModels\Form\Page;

use Modules\Core\Models\Currency\Currency;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Feedback\Models\FormCustom\FormCustom;
use Modules\Feedback\Models\FormCustom\FormCustomData;
use Modules\Sale\Models\Cart;
use Modules\Sale\ViewModels\Order\SaveViewModel;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\User\ViewModels\Auth\RegisterViewModel;

class FormViewModel extends BaseViewModel
{


    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @param null $alias
     * @return $this
     * @throws \Throwable
     */
    protected function showForm($alias = null)
    {

        $form = new FormCustom();
        $viewModel =& $this;
        $alias = $alias ?: $this->request->get('alias');
        $form = $form->active()->where('alias', $alias)->first();

        if (!$form) {
            return $this->redirect(url('/'))->setResponse(false, trans("feedback::messages.alert.not_find_data"));
        }

        $this->getConsultants();
        $this->setTitlePage(trans('feedback::form.' . ($alias)));
        return $this->renderedView("feedback::form.page." . $alias, ['form' => $form, 'form_data' => null, 'view_model' => $viewModel], "form");
    }

    /**
     * @return mixed
     */
    public function getConsultants()
    {
        $userModel = BridgeHelper::getUser()->getUserModel();
        $list = $userModel->active()
            ->filterCurrentInstance()
            ->with('roles.role')->whereHas('roles.role', function ($q) {
                $q->where('name', 'consultant');
            })->get();
        $list = $list->pluck('full_name', 'id');
        return $list;
    }

    /**
     * @return array
     */
    public function getDates()
    {
        $Date = now();
        $dates = [];
        for ($i = 1; $i < 14; $i++) {
            $date = date('Y-m-d', strtotime($Date . ' + ' . $i . ' days'));
            if (strtolower(date('D', strtotime($date))) == 'fri') {
                continue;
            }
            $dates[$date] = DateHelper::setDateTime($date)->getLocaleFormat(trans('core::date.datetime.date_long'));;
        }

        return $dates;
    }

    /**
     * @return array
     */
    public function getTimes()
    {
        $list = [
            '9-12' => '9 ~ 12',
            '12-15' => '12 ~ 15',
            '15-17' => '15 ~ 17',
        ];
        return $list;
    }

    /**
     * @return array
     */
    public function getPrice()
    {
        $list = [
            0 => trans("feedback::form.data.data.free"),
            FormCustom::PRICE_OF_CONSULATE => FormCustom::PRICE_OF_CONSULATE .trans("feedback::form.data.data.unit"),
        ];
        return $list;
    }

    /**
     * @return array
     */
    public function getPayment()
    {
        $list = [
            '-' => '-',
            'online' => trans("feedback::form.data.data.online"),
        ];
        return $list;
    }

    /**
     * @return $this
     */
    protected function saveForm()
    {
        $form = new FormCustomData();
        $dataArr = $this->request->get('data');
        $data = json_encode($dataArr);
        $this->request->offsetSet('data', $data);
//        dd($this->request->all());
        if (($form->fill($this->request->all())->isValid())) {
            $form->active = 1;
            $form->save();
            if ($dataArr['price']){
                $this->request->request->add(['item_type' => $form->getTable(), 'item_id' => $form->id]);
                $this->request->request->add($dataArr);
                return $this->payment();
            }
            return $this->redirectBack()->setResponse(true, trans("feedback::messages.alert.save_success"));
        }
        return $this->redirectBack()->setResponse(false, trans("feedback::messages.alert.mis_data"), $form->errors);

    }

    protected function payment()
    {
        $result = $this->getUser();
        if ($result['action']) {
            $resultCart = $this->createCartItem();
            if ($resultCart['action']) {
                $this->request->request->add(['option'=>['payment'=>'zarinpal']]);
                $orderViewModel = new SaveViewModel();
                return $orderViewModel->setRequest($this->request)->saveOrder();
            }
            return $this->redirectBack()->setResponse(false, trans("feedback::messages.alert.error"));
        } else {
            return $this->redirectBack()->setResponse(false, trans("feedback::messages.alert.not_find_data"));
        }
    }

    /**
     * @return array
     */
    private function getUser()
    {
        if (($connector = $this->request->get('email')) || ($connector = $this->request->get('mobile'))) {

            $userKey = UserRegisterKey::active()->where(function ($q) {
                if ($email = $this->request->get('email')) {
                    $value = $email;
                } elseif ($mobile = $this->request->get('mobile')) {
                    $value = $mobile;
                } else {
                    $value = 0;
                }
                $q->where('value', $value);
            })->first();

            $registerViewModel = new RegisterViewModel();
            $resultRegister = $login = false;
            if ($userKey) {
                auth()->loginUsingId($userKey->user_id);
            } else {
                $pass = '12345678';
                $this->request->request->add([
                    'password' => $pass
                    , 'password_confirmation' => $pass
                    , 'connector' => $connector
                    , 'confirm' => 1
                    , 'first_name' => $this->request->get('user_name')
                    , 'last_name' => '-'
                ]);
                $resultRegister = $registerViewModel->setRequest($this->request)->register()->getResponse();
            }


            if ($user = auth()->user()) {
                $user->confirm = 1;
                $user->save();
                $this->request->request->add(['user_id' => $user->id]);
                return ['action' => true, 'user' => $user];
            } else {
                return ['action' => false];
            }
        } else {
            return ['action' => false];
        }
    }

    protected function createCartItem()
    {
        $price = $this->request->get('price');
        $cartModel = new Cart();
        $currency = Currency::active()->where('code', 'IRR')->first();
        $user = auth()->user();

        if (!$price || !$user) {
            return ['action' => false];
        }

        $data = [
            'user_id' => $user->id,
            'amount' => $price,
            'payable_amount' => $price,
            'currency_id' => $currency ? $currency->id : 1
        ];

        if ($cartModel->fill($data)) {
            $cartModel->save();
            $cart_id = $cartModel->id;
            if ($cart_id) {
                $cartItemModel = new Cart\CartItem();
                $data = [
                    'name' => trans('feedback::form.request-of-counseling'),
                    'cart_id' => $cart_id,
                    'item_type' => $this->request->get('item_type'),
                    'item_id' => $this->request->get('item_id'),
                    'options' => json_encode(['product_key'=>""]),
                    'qty' => 1,
                    'amount' => $price,
                    'total_amount' => $price,
                    'currency_id' => $currency ? $currency->id : 1
                ];
                if ($res = $cartItemModel->insert($data)) {
                    return ['action' => true, 'data' => $cartModel];
                }
                return ['action' => false, 'data' => null];
            }
            return ['action' => false, 'data' => null];
        }
        return ['action' => false, 'data' => null];
    }

}
