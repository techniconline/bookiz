<?php

namespace Modules\Feedback\ViewModels\Form;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Feedback\Models\Comment\Comment;
use Modules\Feedback\Models\FormCustom\FormCustom;
use Modules\Feedback\Models\Qtoa\QtoaQuestion;

class FormGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $form_assets;
    private $grid;
    private $filter_items = [];

    public function __construct()
    {
        $this->useModel = new FormCustom();
    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->enable();
    }

    /**
     *
     */
    public function setGridFormModel()
    {
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->filterCurrentInstance()->enable();
    }


    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $form_assets = [];

        //use bested assets
        if ($this->form_assets) {

        }

        return $form_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }


    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('name', trans('feedback::form.name'), true);
        $this->addColumn('alias', trans('feedback::form.alias'), false);

        $show = array(
            'name' => 'feedback.form.manage.show',
            'parameter' => ['id']
        );
        $this->addAction('content_show', $show, trans('feedback::form.show'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('feedback::form.confirm_delete')];
        $delete = array(
            'name' => 'feedback.form.manage.destroy',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('feedback::form.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('name', 'text');
        $this->addFilter('alias', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        if($this->grid=='forms'){
            $row->url = str_replace(url('/'),"",route('feedback.form.show', $row->alias));
        }
        return $row;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getListGrid()
    {
        $this->setTitlePage(trans('feedback::form.data.list'));
        $this->generateGridList()->renderedView("feedback::form.index", ['view_model' => $this], "form_list");
        return $this;
    }


    /**
     * @return $this
     */
    private function generateGridFormList()
    {

        $this->grid = 'forms';
        $this->setGridFormModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('name', trans('feedback::form.name'), true);
        $this->addColumn('alias', trans('feedback::form.alias'), false);
        $this->addColumn('url', trans('feedback::form.url'), false);

        $add = array(
            'name' => 'feedback.form.create',
            'parameter' => []
        );
        $this->addButton('create_form', $add, trans('feedback::form.create'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);

        $show = array(
            'name' => 'feedback.form.edit',
            'parameter' => ['id']
        );
        $this->addAction('content_edit', $show, trans('feedback::form.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'feedback.form.manage.index',
            'parameter' => ['id']
        );
        $this->addAction('content_list', $show, trans('feedback::form.list_data'), 'fa fa-list', false, ['target' => '', 'class' => 'btn btn-sm btn-primary']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('feedback::form.confirm_delete')];
        $delete = array(
            'name' => 'feedback.form.manage.destroy',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('feedback::form.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('name', 'text');
        $this->addFilter('alias', 'text');
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getListGridForm()
    {
        $this->setTitlePage(trans('feedback::form.list'));
        $this->generateGridFormList()->renderedView("feedback::form.index", ['view_model' => $this], "form_list");
        return $this;
    }


}
