<?php

namespace Modules\Feedback\ViewModels\Comment;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Feedback\Models\Comment\Comment;
use Modules\Feedback\Models\Qtoa\QtoaQuestion;

class CommentGridViewModel extends BaseViewModel
{

    use GridViewModel;

    private $model_id;
    private $model_type;
    private $filter_items = [];

    public function __construct()
    {
        $this->useModel = new Comment();
    }

    public function setGridModel()
    {
        $this->setModelType();
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->enable()->filterByModel();
    }

    /**
     * @param null $model_type
     * @param null $model_id
     * @return $this
     */
    public function setModelType($model_type = null, $model_id = null)
    {
        if (!$this->model_type || !$model_type) {
            $this->filter_items['model_type'] = $this->model_type = $model_type ?: request()->get("model_type", $this->model_type);
        }

        if (!$this->model_id || !$model_id) {
            $this->filter_items['model_id'] = $this->model_id = $model_id ?: request()->get("model_id", $this->model_id);
        }

        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $comment_assets = [];

        //use bested assets
        if ($this->comment_assets) {

        }

        return $comment_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }


    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $all_actions['delete'] = ['title' => trans('feedback::comment.all_delete'), 'route' => 'feedback.comment.manage.delete_all', 'method' => 'POST', 'icon' => 'fa fa-times', 'class' => 'btn btn-warning btn-sm', 'data-message-confirm' => trans('feedback::comment.confirm')];
        $all_actions['publish'] = ['title' => trans('feedback::comment.all_publish'), 'route' => 'feedback.comment.manage.change_publish_all', 'method' => 'POST', 'icon' => 'fa fa-eye', 'class' => 'btn btn-primary btn-sm', 'data-message-confirm' => trans('feedback::comment.confirm')];
        $all_actions['unpublish'] = ['title' => trans('feedback::comment.all_unpublish'), 'route' => 'feedback.comment.manage.change_unpublish_all', 'method' => 'POST', 'icon' => 'fa fa-eye-slash', 'class' => 'btn btn-danger btn-sm', 'data-message-confirm' => trans('feedback::comment.confirm')];
        $this->setActionRows($all_actions);
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('name', trans('feedback::comment.name'), true);
        $this->addColumn('model_type', trans('feedback::comment.model_type'), false);
        $this->addColumn('model_id', trans('feedback::comment.model_id'), false);
        $this->addColumn('email', trans('feedback::comment.email'), false);
        $this->addColumn('publish', trans('feedback::comment.published'), false);

        $show = array(
            'name' => 'feedback.comment.manage.show',
            'parameter' => ['id']
        );
        $this->addAction('content_show', $show, trans('feedback::comment.show'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'feedback.comment.manage.change_publish',
            'parameter' => ['id', 'published']
        );
        $this->addAction('publish', $show, trans('feedback::comment.publish_change'), 'fa fa-eye-slash', false
            , ['target' => '', 'class' => 'btn btn-sm btn-primary _ajax_confirm', 'data-method' => "POST", 'data-message-confirm' => trans('feedback::comment.confirm')]);


        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('feedback::comment.confirm_delete')];
        $delete = array(
            'name' => 'feedback.comment.manage.destroy',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('feedback::comment.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('name', 'text');
        $this->addFilter('email', 'text');
        $this->addFilter('model_type', 'text',['title'=>trans('feedback::comment.model_type')]);
        $this->addFilter('model_id', 'text',['title'=>trans('feedback::comment.model_id')]);
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->publish = trans('feedback::comment.publish_states.' . $row->active);
        $row->published = $row->active!=1 ? 'active' : 'disable';
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
        if ($row->active==1) {
            $action['publish']->icon = 'fa fa-eye';
        } else {
            $action['publish']->icon = 'fa fa-eye-slash';
        }
        return $action;

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getListGrid()
    {
        $this->setTitlePage(trans('feedback::comment.list'));
        $this->generateGridList()->renderedView("feedback::comment.index", ['view_model' => $this], "comment_list");
        return $this;
    }


}
