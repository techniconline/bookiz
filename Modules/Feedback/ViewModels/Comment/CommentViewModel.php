<?php

namespace Modules\Feedback\ViewModels\Comment;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Feedback\Models\Comment\Comment;

class CommentViewModel extends BaseViewModel
{


    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return $this
     */
    protected function destroyComment()
    {
        $comment_id = $this->request->get('comment_id');
        $comment = Comment::active()->find($comment_id);

        if ($comment->delete()) {
            return $this->setResponse(true, trans("feedback::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveComment()
    {
        $comment = new Comment();
        if ($comment_id = $this->request->get("comment_id")) {
            $comment = $comment->active()->find($comment_id);
            if (!$comment) {
                return $this->redirectBack()->setResponse(false, trans("feedback::messages.alert.not_find_data"));
            }

        }

        $model_type = $this->request->get("model_type");
        $model_id = $this->request->get("model_id");
        $access = $this->checkAccess($model_type, $model_id);
        if (!$access) {
            return $this->redirectBack()->setResponse(false, trans("feedback::messages.alert.mis_data_access"));
        }

        if(isset($access["access_comment"]) && !$access["access_comment"]){
            return $this->redirectBack()->setResponse(false, trans("feedback::messages.alert.not_access"));
        }

        if (($comment->fill($this->request->all())->isValid())) {
            $comment->active = isset($access["active"]) ? $access["active"] : 2;
            $comment->save();
            $comment->updated = $comment_id ? true : false;
            return $this->redirectBack()->setDataResponse($comment)->setResponse(true, trans("feedback::messages.alert.save_success"));
        }

        return $this->redirectBack()->setResponse(false, trans("feedback::messages.alert.mis_data"), $comment->errors);
    }

    /**
     * @param $model_type
     * @param $model_id
     * @return array|bool
     */
    public function checkAccess($model_type, $model_id)
    {
        $model = $this->getModelType($model_type);
        if (!$model) {
            return false;
        }
        $data = $model->find($model_id);
        $params = $data ? $data->params_json : null;
        if (!$params) {
            return false;
        }

        $confirm_comment = isset($params->confirm_comment) ? $params->confirm_comment : "by_admin";
        $access_comment = isset($params->access_comment) ? $params->access_comment : "disable";

        $access = false;
        if ($access_comment != "disable") {

            if ($access_comment == "only_user") {
                $user = BridgeHelper::getAccess()->getUser();
                $access = $user ? true : false;
            } else {
                $access = true;
            }

        }

        $active = $confirm_comment == "by_admin" ? 2 : 1;

        return ["active" => $active, "access_comment" => $access];
    }

    /**
     * @param $model_type
     * @return bool
     */
    private function getModelType($model_type)
    {
        $comment = new Comment();
        $methodName = get_uppercase_by_underscores($model_type);
        if (method_exists($comment, "get" . $methodName)) {
            return $comment->{"get" . $methodName}();
        }
        return false;
    }

    /**
     * @return $this
     */
    protected function getComments()
    {
        $comment = new Comment();
        $comments = $comment->setRequestItems($this->request->all())
            ->active()
            ->filterByModel()
            ->filterByUser()
            ->get();
        if ($comments) {
            return $this->setDataResponse($comments)->setResponse(true);
        }
        return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));

    }


}
