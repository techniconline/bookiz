<?php

namespace Modules\Feedback\ViewModels\Comment;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Feedback\Models\Comment\Comment;

class CommentManageViewModel extends BaseViewModel
{

    public $user;
    public $comment;
    public $by_action_form = false;

    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @param $model_type
     * @return bool
     */
    private function getModelType($model_type)
    {
        $comment = new Comment();
        $methodName = get_uppercase_by_underscores($model_type);
        if (method_exists($comment, "get" . $methodName)) {
            return $comment->{"get" . $methodName}();
        }
        return false;
    }


    /**
     * @return $this
     * @throws \Throwable
     */
    protected function showComment()
    {
        $comment = new Comment();
        $comment = $comment->enable()->with(['user'])->find($this->request->get('comment_id'));
        $this->comment = $comment;
        $viewModel =& $this;
        $this->setTitlePage(trans('feedback::comment.show'));
        return $this->renderedView("feedback::comment.form_show", ['comment' => $comment, 'viewModel' => $viewModel], "form");
    }


    /**
     * @param null $model_type
     * @return $this
     */
    public function searchInModel($model_type = null)
    {
        $model_type = $model_type ?: ($this->request ? $this->request->get('model_type') : null);
        if (!$model_type) {
            return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));
        }

        $model = $this->getModelType($model_type);
        if (!$model) {
            return $this->setResponse(false, trans("feedback::messages.alert.not_find_model"));
        }

        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }
        $model = $model->filterCurrentInstance()->select('id', 'title as text');
        if ($q) {
            $model->where(function ($query) use ($q) {
                $query->where('title', 'LIKE', '%' . $q . '%');
            });
            $items = $model->enable()->get();

        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $items = collect([]);
        }

        return $this->setDataResponse($items)->setResponse(true);
    }

    /**
     * @return $this
     */
    protected function destroyComment()
    {
        $comment_id = $this->request->get('comment_id');
        $comment = Comment::enable()->find($comment_id);

        if ($comment->delete()) {
            return $this->setResponse(true, trans("feedback::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.del_un_success"));

    }


    /**
     * @return $this
     */
    protected function changePublish()
    {
        $comment_id = $this->request->get('comment_id');
        $state = $this->request->get('state_publish');
        $response = $this->serviceChangePublish($state, $comment_id);
        return $this->setResponse($response['action'], $response['message']);
    }

    /**
     * @param null $state_publish
     * @param $comment_id
     * @return array
     */
    public function serviceChangePublish($state_publish, $comment_id)
    {
        $state = $state_publish ?: 'disable';
        $state_publish = $state == 'active' ? 1 : 2;
        $comment = new Comment();
        $model = $comment->find($comment_id);
        $model->active = $state_publish;

        if ($model->save()) {
            return ['action' => true, 'message' => trans("feedback::messages.alert.save_success"), 'data' => $comment];
        }
        return ['action' => false, 'message' => trans("feedback::messages.alert.save_un_success")];
    }


    /**
     * @return $this
     */
    public function deleteAll()
    {
        $ids = $this->request->get('row_items');

        if (empty($ids)) {
            return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));
        }

        $comments = Comment::enable()->whereIn('id', $ids)->get();
        $res = false;
        foreach ($comments as $comment) {
            $r = $comment->delete();
            if ($r && !$res) {
                $res = true;
            }
        }

        if ($res) {
            return $this->setResponse(true, trans("feedback::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    public function changePublishAll()
    {

        $ids = $this->request->get('row_items');

        if (empty($ids)) {
            return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));
        }
        $state = $this->request->get('state_publish');
        $response = [];
        foreach ($ids as $id) {
            $response[] = $this->serviceChangePublish($state, $id);
        }

        if ($response) {
            return $this->setDataResponse($response)->setResponse(true, trans("feedback::messages.alert.save_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.error"));
    }


}
