<?php

namespace Modules\Feedback\ViewModels\Rate;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Feedback\Models\Rate\Rate;

class RateViewModel extends BaseViewModel
{


    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return $this
     */
    protected function destroyRate()
    {
        $rate_id = $this->request->get('rate_id');
        $rate = Rate::active()->find($rate_id);

        if ($rate->delete()) {
            return $this->setResponse(true, trans("feedback::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.del_un_success"));

    }

    /**
     * @param int $rate_value
     * @param null $model_id
     * @param null $model_type
     * @param null $user_id
     * @return $this
     */
    public function saveRate($rate_value = null, $model_id = null, $model_type = null, $user_id = null)
    {
        $user_id = $user_id?:$this->request->get("user_id");
        $model_id = $model_id?:$this->request->get("model_id");
        $model_type = $model_type?:$this->request->get("model_type");
        $rate_value = $rate_value?:isset($this->request)?$this->request->get("rate"):3;

        $rate = Rate::active()->where("user_id", $user_id)
            ->where("model_id", $model_id)
            ->where("model_type",$model_type)
            ->first();

        if(!$rate){
            $rate = new Rate();
        }

        $access = $this->checkAccess($model_type, $model_id);
        if(!$access){
            return $this->setResponse(false, trans("feedback::messages.alert.not_access"));
        }

        $items = [
            "rate"=>$rate_value,
            "user_id"=>$user_id,
            "model_id"=>$model_id,
            "model_type"=>$model_type,
        ];

        if (($rate->fill($items)->isValid())) {
            $rate->save();
            return $this->setDataResponse($rate)->setResponse(true, trans("feedback::messages.alert.save_success"));
        }

        return $this->setResponse(false, trans("feedback::messages.alert.mis_data"), $rate->errors);
    }

    /**
     * @param $model_type
     * @param $model_id
     * @return bool
     */
    public function checkAccess($model_type, $model_id)
    {
        $model = $this->getModelType($model_type);
        if (!$model) {
            return false;
        }
        $data = $model->find($model_id);

        $params = $data ? $data->params_json : null;
        if (!$params) {
            return false;
        }

        $access_rate = isset($params->access_rate) ? $params->access_rate : "disable";

        $access = false;
        if ($access_rate != "disable") {

            if ($access_rate == "only_user") {
                $user = BridgeHelper::getAccess()->getUser();
                $access = $user ? true : false;
            } else {
                $access = true;
            }

        }

        return $access;
    }

    /**
     * @param $model_type
     * @return bool
     */
    private function getModelType($model_type)
    {
        $rate = new Rate();
        $sArr = explode("_", $model_type);
        foreach ($sArr as &$item) {
            $item = ucfirst($item);
        }
        $methodName = implode("", $sArr);
        if (method_exists($rate, "get" . $methodName)) {
            return $rate->{"get" . $methodName}();
        }
        return false;
    }


    /**
     * @return $this
     */
    protected function getRates()
    {
        $rates = Rate::active()
            ->filterByModel()
            ->get();
        if($rates){
            return $this->setDataResponse($rates)->setResponse(true);
        }
        return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));

    }


}
