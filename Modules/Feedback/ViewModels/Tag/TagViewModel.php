<?php

namespace Modules\Feedback\ViewModels\Tag;


use Illuminate\Http\Request;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Feedback\Models\Tag\Tag;
use Modules\User\Models\User;


class TagViewModel extends BaseViewModel
{

    use GridViewModel;

    private $tag_assets;

    public function __construct()
    {

    }

    public function setGridModel()
    {
        $this->Model = Tag::active()->with('user');
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $tag_assets = [];

        //use bested assets
        if ($this->tag_assets) {
            $tag_assets [] = ['container' => 'up_theme_js_2', 'src' => ("assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"), 'name' => 'ui.widget'];

            $tag_assets [] = ['container' => 'plugin_general_2', 'src' => ("jquery-ui/jquery-ui.min.js"), 'name' => 'jquery-ui'];
        }

        if ($this->tag_assets) {
            $tag_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/tag/tag.js"), 'name' => 'tag-backend'];
        }

        return $tag_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();
        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('name', trans('feedback::tag.name'), true);
        $this->addColumn('education', trans('feedback::tag.education'), false);
        $this->addColumn('teaching_time', trans('feedback::tag.teaching_time'), false);

        /*add action*/
        $add = array(
            'name' => 'feedback.tag.create',
            'parameter' => null
        );
        $this->addButton('create_tag', $add, trans('feedback::tag.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);


        $show = array(
            'name' => 'feedback.tag.edit',
            'parameter' => ['id']
        );
        $this->addAction('content', $show, trans('feedback::tag.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('feedback::tag.confirm_delete')];
        $delete = array(
            'name' => 'feedback.tag.delete',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('feedback::tag.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('name', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->name = $row->user->full_name;
        $row->education = trans('feedback::tag.educations.' . $row->education_key);
        return $row;
    }


    /**
     * @return $this
     */
    protected function setAssetsEditTag()
    {
        $this->tag_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function getViewListIndex()
    {
        $this->setTitlePage(trans('feedback::tag.list'));
        $this->generateGridList()->renderedView("feedback::tag.index", ['view_model' => $this], "tag_list");
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createTag()
    {

        $tag = new Tag();
        $users = User::active()->get();
        $tag = $tag->active()->with([
            'user'
        ])->find($this->request->get('tag_id'));

        $this->setTitlePage(trans('feedback::tag.' . ($tag ? 'edit' : 'add')));
        return $this->renderedView("feedback::tag.form_tag", ['tag' => $tag, 'users' => $users], "form");
    }

    /**
     * @return TagViewModel
     * @throws \Throwable
     */
    protected function editTag()
    {
        return $this->createTag();
    }

    /**
     * @return $this
     */
    protected function destroyTag()
    {
        $tag_id = $this->request->get('tag_id');
        $tag = Tag::enable()->find($tag_id);

        if ($tag && $tag->delete()) {
            return $this->setResponse(true, trans("feedback::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveTag()
    {
        $response = $this->saveTagService();
        $tag = $response['tag'];
        if ($response['action']) {
            return $this->redirect(route('feedback.tag.edit', ['tag_id' => $tag->id]))->setDataResponse($tag)->setResponse(true, trans("feedback::messages.alert.save_success"));
        }
        return $this->redirect(isset($response['redirect']) ? $response['redirect'] : route('feedback.tag.create'))->setResponse(false, trans("feedback::messages.alert.mis_data"), isset($tag->errors) ? $tag->errors : null);
    }

    /**
     * @return $this|array
     */
    private function saveTagService()
    {
        $tag = new Tag();
        if ($tag_id = $this->request->get("tag_id")) {
            $tag = $tag->enable()->find($tag_id);
            if (!$tag) {
                return ['action' => false, 'tag' => $tag, 'redirect' => route('feedback.tag.edit', ['tag_id' => $this->request->get('tag_id')])];
            }

        }

        $education = $tag->searchEnumItem($this->request->get('education'), $tag->educations, 'value');
        $this->request->offsetSet('education', $education);

        if (($tag->fill($this->request->all())->isValid()) || ($tag_id && $tag)) {
            $tag->save();
            $tag->updated = $tag_id ? true : false;
            $response = ['action' => true, 'tag' => $tag];
        } else {
            $response = ['action' => false, 'tag' => $tag];
        }

        return $response;
    }

    /**
     * @return $this
     */
    public function getTagByApi()
    {
        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }
        $model = new Tag();
        $model = $model->setRequestItems($this->request->all())->active()->with('user');
        if ($q) {
            $items = $model->filterUser()->get();
        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $items = collect([]);
        }
        $items = $items->map(function ($item) {
            $item->text = $item->user->full_name;
            return $item;
        });
        return $this->setDataResponse($items)->setResponse(true);
    }


}
