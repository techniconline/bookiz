<?php

namespace Modules\Feedback\ViewModels\Qtoa\Answer;

use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Feedback\Models\Qtoa\QtoaAnswer;

class AnswerGridViewModel extends BaseViewModel
{

    use GridViewModel;
    private $question_id;
    private $model_type;
    private $model_id;
    private $filter_items = [];
    public function __construct()
    {
        $this->useModel = new QtoaAnswer();
    }

    /**
     * @param null $question_id
     * @return $this
     */
    public function setQuestionId($question_id = null)
    {
        if(!$this->question_id || $question_id){
            $this->filter_items['question_id'] = $this->question_id = $question_id?:($this->request?$this->request->get('question_id'):0);
        }
        return $this;
    }


    /**
     * @param null $model_type
     * @param null $model_id
     * @return $this
     */
    public function setModelType($model_type = null, $model_id = null)
    {
        if (!$this->model_type || !$model_type) {
            $this->filter_items['model_type'] = $this->model_type = $model_type ?: request()->get("model_type", $this->model_type);
        }

        if (!$this->model_id || !$model_id) {
            $this->filter_items['model_id'] = $this->model_id = $model_id ?: request()->get("model_id", $this->model_id);
        }

        return $this;
    }

    /**
     *
     */
    public function setGridModel()
    {
        $this->setQuestionId();
        $this->setModelType();
        $model = $this->useModel;
        $this->Model = $model->setRequestItems($this->filter_items)->enable()->filterQuestion()->with(['user','qtoaQuestion']);
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $qtoa_assets = [];

        //use bested assets
        if ($this->qtoa_assets) {

        }

        return $qtoa_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }


    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();

        $all_actions['delete'] = ['title' => trans('feedback::qtoa.all_delete'), 'route' => 'feedback.qtoa.question.answer.delete_all', 'method' => 'POST', 'icon' => 'fa fa-times', 'class' => 'btn btn-warning btn-sm', 'data-message-confirm' => trans('feedback::qtoa.confirm')];
        $all_actions['publish'] = ['title' => trans('feedback::qtoa.all_publish'), 'route' => 'feedback.qtoa.question.answer.change_publish_all', 'method' => 'POST', 'icon' => 'fa fa-eye', 'class' => 'btn btn-primary btn-sm', 'data-message-confirm' => trans('feedback::qtoa.confirm')];
        $all_actions['unpublish'] = ['title' => trans('feedback::qtoa.all_unpublish'), 'route' => 'feedback.qtoa.question.answer.change_unpublish_all', 'method' => 'POST', 'icon' => 'fa fa-eye-slash', 'class' => 'btn btn-danger btn-sm', 'data-message-confirm' => trans('feedback::qtoa.confirm')];
        $this->setActionRows($all_actions);

        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('answer', trans('feedback::qtoa.answer'), false);
        $this->addColumn('question_id', trans('feedback::qtoa.question'), true);
        $this->addColumn('model_type', trans('feedback::qtoa.model_type'), false);
        $this->addColumn('model_id', trans('feedback::qtoa.model_id'), false);
        $this->addColumn('liked', trans('feedback::qtoa.liked'), false);
        $this->addColumn('dislike', trans('feedback::qtoa.dislike'), false);
        $this->addColumn('views', trans('feedback::qtoa.views'), false);
        $this->addColumn('is_hot', trans('feedback::qtoa.is_hot'), false);
        $this->addColumn('publish', trans('feedback::qtoa.published'), false);

        /*add action*/
        $add = array(
            'name' => 'feedback.qtoa.question.index',
            'parameter' => null
        );
        $this->addButton('list_question', $add, trans('feedback::qtoa.list_question'), 'fa fa-list', false, 'before', ['target' => '', 'class' => 'btn btn-primary']);

        /*add action*/
        $add = array(
            'name' => 'feedback.qtoa.question.answer.create',
            'parameter' => ["question_id" => ['value' => $this->question_id]]
        );
        $this->addButton('create_answer', $add, trans('feedback::qtoa.answer_field.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);


        $show = array(
            'name' => 'feedback.qtoa.question.answer.edit',
            'parameter' => ['id']
        );
        $this->addAction('content_edit', $show, trans('feedback::qtoa.edit'), 'fa fa-plus-circle', false, ['target' => '', 'class' => 'btn btn-sm btn-success']);

        $show = array(
            'name' => 'feedback.qtoa.question.answer.change_publish',
            'parameter' => ['id', 'published']
        );
        $this->addAction('publish', $show, trans('feedback::qtoa.publish_change'), 'fa fa-eye-slash', false
            , ['target' => '', 'class' => 'btn btn-sm btn-primary _ajax_confirm', 'data-method' => "POST", 'data-message-confirm' => trans('feedback::qtoa.confirm')]);

        $show = array(
            'name' => 'feedback.like.storeLike',
            'parameter' => ["model_type"=>["value"=>$this->useModel->getTable()], "id", "value_like" => ["value" => 1]]
        );
        $this->addAction('like_question', $show, trans('feedback::qtoa.like'), 'fa fa-thumbs-o-up', false
            , ['target' => '', 'class' => 'btn btn-sm btn-default _ajax_confirm', 'data-method' => "POST", 'data-message-confirm' => trans('feedback::qtoa.confirm')]);


        $show = array(
            'name' => 'feedback.like.storeLike',
            'parameter' => ["model_type"=>["value"=>$this->useModel->getTable()], "id", "value_like" => ["value" => 0]]
        );
        $this->addAction('dislike_question', $show, trans('feedback::qtoa.dlike'), 'fa fa-thumbs-o-down', false
            , ['target' => '', 'class' => 'btn btn-sm btn-default _ajax_confirm', 'data-method' => "POST", 'data-message-confirm' => trans('feedback::qtoa.confirm')]);


        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('feedback::qtoa.confirm_delete')];
        $delete = array(
            'name' => 'feedback.qtoa.question.answer.destroy',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('feedback::qtoa.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('title', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->publish = trans('feedback::qtoa.publish_states.'.$row->published);
        $row->published = $row->published?'disable':'active';
        $row->question_id = isset($row->qtoaQuestion->question)?$row->qtoaQuestion->question:'-';
        $row->model_type = isset($row->qtoaQuestion->model_type)?$row->qtoaQuestion->model_type:'-';
        $row->model_id = isset($row->qtoaQuestion->model_id)?$row->qtoaQuestion->model_id:'-';
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
        if ($row->published) {
            $action['publish']->icon = 'fa fa-eye';
        }else{
            $action['publish']->icon = 'fa fa-eye-slash';
        }
        return $action;

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getListGrid()
    {
        $this->setTitlePage(trans('feedback::qtoa.answer_list'));
        $this->generateGridList()->renderedView("feedback::qtoa.index", ['view_model' => $this], "qtoa_list");
        return $this;
    }


    /**
     * @param bool $json
     * @return $this
     * @throws \Throwable
     */
    public function getList($json = false)
    {
        $this->setQuestionId();
        $model = $this->useModel;
        $result = $model->setRequestItems(['question_id'=>$this->question_id])->active()
            ->filterQuestion()->with(['user'])
            ->orderBy("id", "DESC")
            ->paginate($this->perPage,['*'],'page_answer');
        if($result){
            if($json){
                return $this->setDataResponse($result)->setResponse(true);
            }else{
                return $this->renderedView('feedback::qtoa.front.list_answers', ['list' => $result])->setResponse(true);
            }
        }
        if($json){
            return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));
        }
        return $this->renderedView('feedback::qtoa.front.list_answers', ['list' => $result])->setResponse(false);
    }

}
