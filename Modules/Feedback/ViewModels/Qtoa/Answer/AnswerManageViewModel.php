<?php

namespace Modules\Feedback\ViewModels\Qtoa\Answer;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Feedback\Models\Qtoa\QtoaAnswer;
use Modules\Feedback\Models\Qtoa\QtoaQuestion;
use Modules\Feedback\ViewModels\Qtoa\QuestionViewModel;

class AnswerManageViewModel extends BaseViewModel
{

    private $qtoa_assets;
    private $question_id;
    private $answerViewModel;

    public function __construct()
    {
        $this->answerViewModel = new AnswerViewModel();
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @param null $question_id
     * @return $this
     */
    public function setQuestionId($question_id = null)
    {
        if (!$this->question_id) {
            $this->question_id = $question_id ?: ($this->request ? $this->request->get('question_id') : 0);
        }
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $qtoa_assets = [];

        if ($this->qtoa_assets) {
            $qtoa_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/qtoa/qtoa.js"), 'name' => 'qtoa-backend'];
        }

        return $qtoa_assets;
    }


    /**
     * @return $this
     */
    protected function setAssetsCreateAnswer()
    {
        $this->qtoa_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createAnswer()
    {
        $this->setQuestionId();
        $model = new QtoaAnswer();
        $model = $model->find($this->request->get('answer_id'));
        $modelQuestion = new QtoaQuestion();
        $question_id = $model ? $model->question_id : $this->question_id;
        $question = $modelQuestion->find($question_id);

        $viewModel =& $this;

        $this->setTitlePage(trans('feedback::qtoa.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("feedback::qtoa.answer.form", ['answer' => $model, 'question' => $question, 'view_model' => $viewModel, 'append_route' => '.manage'], "form");
    }

    /**
     * @return QuestionViewModel
     */
    public function getQuestionViewModel()
    {
        return new QuestionViewModel();
    }

    /**
     * @return AnswerViewModel
     * @throws \Throwable
     */
    protected function editAnswer()
    {
        return $this->createAnswer();
    }

    /**
     * @return $this
     */
    protected function destroyAnswer()
    {
        $answer_id = $this->request->get('answer_id');
        $answer = QtoaAnswer::enable()->find($answer_id);

        if ($answer->delete()) {
            return $this->setResponse(true, trans("feedback::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveAnswer()
    {
        $answer_id = $this->request->get("answer_id");
        $data = $this->request->all();
        $response = $this->answerViewModel->serviceSaveAnswer($answer_id, $data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('feedback.qtoa.question.answer.manage.index', ['question_id' => $response['data']->question_id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @return $this
     */
    protected function changePublish()
    {
        $answer_id = $this->request->get('answer_id');
        $state = $this->request->get('state_publish');
        $response = $this->answerViewModel->serviceChangePublish($state, $answer_id);
        return $this->setResponse($response['action'], $response['message']);
    }

    /**
     * @param $model_type
     * @param $model_id
     * @return array|bool
     */
    public function checkAccess($answer_id)
    {
        return true;
        //TODO check Access
        $model = $this->getModelType($model_type);
        if (!$model) {
            return false;
        }
        $data = $model->find($model_id);
        $params = $data ? $data->params_json : null;
        if (!$params) {
            return false;
        }

        $confirm_question = isset($params->confirm_question) ? $params->confirm_question : "by_admin";
        $access_question = isset($params->access_question) ? $params->access_question : "disable";

        $access = false;
        if ($access_question != "disable") {

            if ($access_question == "only_user") {
                $user = BridgeHelper::getAccess()->getUser();
                $access = $user ? true : false;
            } else {
                $access = true;
            }

        }

        $active = $confirm_question == "by_admin" ? 2 : 1;

        return ["active" => $active, "access_question" => $access];
    }

}
