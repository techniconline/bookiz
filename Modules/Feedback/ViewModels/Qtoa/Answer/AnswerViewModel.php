<?php

namespace Modules\Feedback\ViewModels\Qtoa\Answer;

use Illuminate\Support\Facades\Storage;
use Modules\Code\Libraries\Uploader\UploaderLibrary;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Feedback\Models\Qtoa\QtoaAnswer;
use Modules\Feedback\Models\Qtoa\QtoaQuestion;
use Modules\Feedback\ViewModels\Qtoa\QuestionViewModel;

class AnswerViewModel extends BaseViewModel
{

    private $qtoa_assets;
    private $question_id;

    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @param null $question_id
     * @return $this
     */
    public function setQuestionId($question_id = null)
    {
        if (!$this->question_id) {
            $this->question_id = $question_id ?: ($this->request ? $this->request->get('question_id') : 0);
        }
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $qtoa_assets = [];

        if ($this->qtoa_assets) {
            $qtoa_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/qtoa/qtoa.js"), 'name' => 'qtoa-backend'];
        }

        return $qtoa_assets;
    }


    /**
     * @return $this
     */
    protected function setAssetsCreateAnswer()
    {
        $this->qtoa_assets = true;
        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createAnswer()
    {
        $this->setQuestionId();
        $model = new QtoaAnswer();
        $model = $model->find($this->request->get('answer_id'));
        $modelQuestion = new QtoaQuestion();
        $question_id = $model ? $model->question_id : $this->question_id;
        $question = $modelQuestion->find($question_id);

        $viewModel =& $this;

        $this->setTitlePage(trans('feedback::qtoa.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("feedback::qtoa.answer.form", ['answer' => $model, 'question' => $question, 'view_model' => $viewModel], "form");
    }

    /**
     * @return QuestionViewModel
     */
    public function getQuestionViewModel()
    {
        return new QuestionViewModel();
    }

    /**
     * @return AnswerViewModel
     * @throws \Throwable
     */
    protected function editAnswer()
    {
        return $this->createAnswer();
    }

    /**
     * @return $this
     */
    protected function destroyAnswer()
    {
        $answer_id = $this->request->get('answer_id');
        $answer = QtoaAnswer::enable()->find($answer_id);

        if ($answer->delete()) {
            return $this->setResponse(true, trans("feedback::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveAnswer()
    {
        $answer_id = $this->request->get("answer_id");
        $data = $this->request->all();
        $response = $this->serviceSaveAnswer($answer_id, $data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('feedback.qtoa.question.answer.index', ['question_id' => $response['data']->question_id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @param $answer_id
     * @param $data
     * @return array
     */
    public function serviceSaveAnswer($answer_id, $data)
    {
        $answer = new QtoaAnswer();
        $user = BridgeHelper::getAccess()->getUser();
        $data['user_id'] = $user ? $user->id : null;
        $access = $this->checkAccess($answer_id);
        if (!$access) {
            return ['action' => false, 'message' => trans("feedback::messages.alert.not_access")];
        }

        if ($answer_id) {
            $answer = $answer->enable()->find($answer_id);
            if (!$answer) {
                return ['action' => false, 'message' => trans("feedback::messages.alert.not_find_data")];
            }
        }

        if ($this->isValidRequest($data, $answer->rules, $answer->messages, "POST")->isValidRequest) {
            $answer->fill($data);
            if ($answer->save()) {
                return ['action' => true, 'message' => trans("feedback::messages.alert.save_success"), 'data' => $answer];
            } else {
                return ['action' => false, 'message' => trans("feedback::messages.alert.save_un_success")];
            }
        }
        return ['action' => false, 'message' => trans("feedback::messages.alert.mis_data"), 'errors' => $this->errors];
    }

    /**
     * @return $this
     */
    protected function changePublish()
    {
        $answer_id = $this->request->get('answer_id');
        $state = $this->request->get('state_publish');
        $response = $this->serviceChangePublish($state, $answer_id);
        return $this->setResponse($response['action'], $response['message']);
    }

    /**
     * @param null $state_publish
     * @param $answer_id
     * @return array
     */
    public function serviceChangePublish($state_publish, $answer_id)
    {
        $state = $state_publish ?: 'disable';
        $state_publish = $state == 'active' ? 1 : 0;
        $answer = new QtoaAnswer();
        $model = $answer->find($answer_id);
        $model->published = $state_publish;

        if ($model->save()) {
            return ['action' => true, 'message' => trans("feedback::messages.alert.save_success"), 'data' => $answer];
        }
        return ['action' => false, 'message' => trans("feedback::messages.alert.save_un_success")];
    }

    /**
     * @param $model_type
     * @param $model_id
     * @return array|bool
     */
    public function checkAccess($answer_id)
    {
        return true;
        //TODO check Access
        $model = $this->getModelType($model_type);
        if (!$model) {
            return false;
        }
        $data = $model->find($model_id);
        $params = $data ? $data->params_json : null;
        if (!$params) {
            return false;
        }

        $confirm_question = isset($params->confirm_question) ? $params->confirm_question : "by_admin";
        $access_question = isset($params->access_question) ? $params->access_question : "disable";

        $access = false;
        if ($access_question != "disable") {

            if ($access_question == "only_user") {
                $user = BridgeHelper::getAccess()->getUser();
                $access = $user ? true : false;
            } else {
                $access = true;
            }

        }

        $active = $confirm_question == "by_admin" ? 2 : 1;

        return ["active" => $active, "access_question" => $access];
    }

    /**
     * @return $this
     */
    protected function getAnswers()
    {
        $answer = new QtoaAnswer();
        $answer = $answer->setRequestItems($this->request->all())
            ->active()
            ->filterByUser()
            ->get();
        if ($answer) {
            return $this->setDataResponse($answer)->setResponse(true);
        }
        return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));

    }

    /**
     * @return $this
     */
    public function deleteAll()
    {
        $ids = $this->request->get('row_items');

        if (empty($ids)) {
            return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));
        }

        $questions = QtoaAnswer::active()->whereIn('id', $ids)->get();
        $res = false;
        foreach ($questions as $question) {
            $r = $question->delete();
            if ($r && !$res) {
                $res = true;
            }
        }

        if ($res) {
            return $this->setResponse(true, trans("feedback::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    public function changePublishAll()
    {

        $ids = $this->request->get('row_items');

        if (empty($ids)) {
            return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));
        }
        $state = $this->request->get('state_publish');
        $response = [];
        foreach ($ids as $id) {
            $response[] = $this->serviceChangePublish($state, $id);
        }

        if ($response) {
            return $this->setDataResponse($response)->setResponse(true, trans("feedback::messages.alert.save_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.error"));
    }


    /**
     * @return $this
     */
    public function audioSave()
    {
        $question_id = $this->request->get('question_id');
        $data_file = $this->request->file('data')->getSize();
        $answer = null;
        if ($question_id && $data_file) {
            $question = QtoaQuestion::find($question_id);
            $data = [
                'question_id' => $question->id,
                'answer' => 'voice',
            ];
            $answer = $this->serviceSaveAnswer(0, $data);
            if ($question && $answer['action']) {
                $answer = $answer['data'];
                $res = $this->saveVoice($answer);

                if ($res) {
                    $answer->media = $res;
                    $answer->save();
                }

                return $this->setResponse($res ? true : false, $res?trans("feedback::messages.alert.save_success"):trans("feedback::messages.alert.error"));
            }

        }
        return $this->setDataResponse($answer)->setResponse(false, trans("feedback::messages.alert.not_find_data"));
    }

    /**
     * @param $answer
     * @return bool
     */
    protected function saveVoice($answer)
    {
        $fileAddress = null;
        if ($this->request->hasFile('data')) {
            $file = $this->request->file('data');
            $fileName = 'record_' . time() . '.' . 'webm';
            $currentInstance = app("getInstanceObject")->getCurrentInstance();

            $path = $currentInstance->name . DIRECTORY_SEPARATOR . 'questions' . DIRECTORY_SEPARATOR
                . $answer->question_id . DIRECTORY_SEPARATOR . 'answers' . DIRECTORY_SEPARATOR . $answer->id . DIRECTORY_SEPARATOR . $fileName;
            $res = Storage::disk(UploaderLibrary::STORAGE_PUBLIC_MINIO)->put($path, fopen($file, 'r+'));
            if ($res) {
                return $path;
            }
            return false;
        }
        return false;
    }

}
