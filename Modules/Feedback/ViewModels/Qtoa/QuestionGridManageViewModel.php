<?php

namespace Modules\Feedback\ViewModels\Qtoa;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Core\ViewModels\Grid\GridViewModel;
use Modules\Feedback\Models\Qtoa\QtoaQuestion;

class QuestionGridManageViewModel extends BaseViewModel
{

    use GridViewModel;

    private $model_id;
    private $model_type;
    private $filter_items = [];

    public function __construct()
    {
        $this->useModel = new QtoaQuestion();
    }

    public function setGridModel()
    {
        $this->setModelType();
        $this->Model = $this->useModel->setRequestItems($this->filter_items)->enable()->filterCurrentInstance()
            ->filterByModel()
//            ->filterCurrentLanguage()
            ->with('instance');
    }

    /**
     * @param null $model_type
     * @param null $model_id
     * @return $this
     */
    public function setModelType($model_type = null, $model_id = null)
    {
        if (!$this->model_type || !$model_type) {
            $this->filter_items['model_type'] = $this->model_type = $model_type ?: request()->get("model_type", $this->model_type);
        }

        if (!$this->model_id || !$model_id) {
            $this->filter_items['model_id'] = $this->model_id = $model_id ?: request()->get("model_id", $this->model_id);
        }

        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $qtoa_assets = [];

        //use bested assets
        if ($this->qtoa_assets) {

        }

        return $qtoa_assets;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }


    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    private function generateGridList()
    {
        $this->setGridModel();

        $all_actions['delete'] = ['title' => trans('feedback::qtoa.all_delete'), 'route' => 'feedback.qtoa.question.delete_all', 'method' => 'POST', 'icon' => 'fa fa-times', 'class' => 'btn btn-warning btn-sm', 'data-message-confirm' => trans('feedback::qtoa.confirm')];
        $all_actions['publish'] = ['title' => trans('feedback::qtoa.all_publish'), 'route' => 'feedback.qtoa.question.change_publish_all', 'method' => 'POST', 'icon' => 'fa fa-eye', 'class' => 'btn btn-primary btn-sm', 'data-message-confirm' => trans('feedback::qtoa.confirm')];
        $all_actions['unpublish'] = ['title' => trans('feedback::qtoa.all_unpublish'), 'route' => 'feedback.qtoa.question.change_unpublish_all', 'method' => 'POST', 'icon' => 'fa fa-eye-slash', 'class' => 'btn btn-danger btn-sm', 'data-message-confirm' => trans('feedback::qtoa.confirm')];
        $this->setActionRows($all_actions);

        $this->hasIndex = false;
        /*add columns to grid*/
        $this->addColumn('question', trans('feedback::qtoa.question'), true);
        $this->addColumn('model_type', trans('feedback::qtoa.model_type'), false);
        $this->addColumn('model_id', trans('feedback::qtoa.model_id'), false);
        $this->addColumn('liked', trans('feedback::qtoa.liked'), false);
        $this->addColumn('dislike', trans('feedback::qtoa.dislike'), false);
        $this->addColumn('views', trans('feedback::qtoa.views'), false);
        $this->addColumn('is_hot', trans('feedback::qtoa.is_hot'), false);
        $this->addColumn('publish', trans('feedback::qtoa.published'), false);

        /*add action*/
        $add = array(
            'name' => 'feedback.qtoa.question.manage.create',
            'parameter' => ["model_type" => ['value' => $this->model_type],"model_id" => ['value' => $this->model_id]]
        );
        $this->addButton('create_question', $add, trans('feedback::qtoa.add'), 'fa fa-plus', false, 'before', ['target' => '', 'class' => 'btn btn-danger']);


        $show = array(
            'name' => 'feedback.qtoa.question.manage.edit',
            'parameter' => ['id']
        );
        $this->addAction('content_edit', $show, trans('feedback::qtoa.edit'), 'fa fa-edit', false, ['target' => '', 'class' => 'btn btn-sm btn-default']);


        $show = array(
            'name' => 'feedback.qtoa.question.answer.manage.index',
            'parameter' => ['id']
        );
        $this->addAction('content_list_answer', $show, trans('feedback::qtoa.answer_list'), 'fa fa-list', false, ['target' => '', 'class' => 'btn btn-sm btn-default']);

        $show = array(
            'name' => 'feedback.qtoa.question.change_publish',
            'parameter' => ['id', 'published']
        );
        $this->addAction('publish', $show, trans('feedback::qtoa.publish_change'), 'fa fa-eye-slash', false
            , ['target' => '', 'class' => 'btn btn-sm btn-default _ajax_confirm', 'data-method' => "POST", 'data-message-confirm' => trans('feedback::qtoa.confirm')]);

        $show = array(
            'name' => 'feedback.like.storeLike',
            'parameter' => ["model_type"=>["value"=>$this->useModel->getTable()], "id", "value_like" => ["value" => 1]]
        );
        $this->addAction('like_question', $show, trans('feedback::qtoa.like'), 'fas fa-thumbs-up', false
            , ['target' => '', 'class' => 'btn btn-sm btn-default _ajax_confirm', 'data-method' => "POST", 'data-message-confirm' => trans('feedback::qtoa.confirm')]);


        $show = array(
            'name' => 'feedback.like.storeLike',
            'parameter' => ["model_type"=>["value"=>$this->useModel->getTable()], "id", "value_like" => ["value" => 0]]
        );
        $this->addAction('dislike_question', $show, trans('feedback::qtoa.dlike'), 'fas fa-thumbs-down', false
            , ['target' => '', 'class' => 'btn btn-sm btn-default _ajax_confirm', 'data-method' => "POST", 'data-message-confirm' => trans('feedback::qtoa.confirm')]);


        /*delete and publish action*/
        $options = ['class' => "btn btn-sm btn-danger _ajax_delete", 'data-message-confirm' => trans('feedback::qtoa.confirm_delete')];
        $delete = array(
            'name' => 'feedback.qtoa.question.destroy',
            'parameter' => 'id',
        );
        $this->addAction('delete', $delete, trans('feedback::qtoa.delete'), 'fa fa-trash', false, $options);

        /**/

        $this->action_text = false;

        /*filter*/
        $this->addFilter('title', 'text');
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowsUpdate($row)
    {
        $row->publish = trans('feedback::qtoa.publish_states.' . $row->published);
        $row->published = $row->published ? 'disable' : 'active';
        return $row;
    }

    /**
     * @param $row
     * @return array
     */
    public function getActionsUpdate($row)
    {
        $action = $this->actions;
        if ($row->published) {
            $action['publish']->icon = 'fa fa-eye';
        } else {
            $action['publish']->icon = 'fa fa-eye-slash';
        }
        return $action;

    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function getListQuestionsGrid()
    {
        $this->setTitlePage(trans('feedback::qtoa.list'));
        $this->generateGridList()->renderedView("feedback::qtoa.front.manage.list_questions", ['view_model' => $this], "qtoa_list_questions");
        return $this;
    }

}
