<?php

namespace Modules\Feedback\ViewModels\Qtoa;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Feedback\Models\Qtoa\QtoaQuestion;
use Modules\Feedback\ViewModels\Qtoa\Answer\AnswerGridViewModel;

class QuestionManageViewModel extends BaseViewModel
{

    private $qtoa_assets;
    private $model_id;
    private $model_type;
    private $questionViewModel;

    public function __construct()
    {
        $this->questionViewModel = new QuestionViewModel();
    }


    /**
     * @param $question
     * @return $this
     */
    private function setModelTypeByQuestion($question)
    {
        if($question && !$this->model_type && !$this->model_id){
            if($question){
                $this->model_id = $question->model_id;
                $this->model_type = $question->model_type;
            }
        }
        return $this;
    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $qtoa_assets = [];

        if ($this->qtoa_assets) {
            $qtoa_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/qtoa/qtoa.js"), 'name' => 'qtoa-backend'];
        }

        return $qtoa_assets;
    }


    /**
     * @param null $model_type
     * @param null $model_id
     * @return $this
     */
    public function setModelType($model_type = null, $model_id = null)
    {
        if (!$this->model_type || !$model_type) {
            $this->model_type = $model_type ?: request()->get("model_type", $this->model_type);
        }

        if (!$this->model_id || !$model_id) {
            $this->model_id = $model_id ?: request()->get("model_id", $this->model_id);
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function setAssetsCreateQuestion()
    {
        $this->qtoa_assets = true;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModelTypeText()
    {
        return $this->model_type;
    }

    /**
     * @return mixed
     */
    public function getModelId()
    {
        return $this->model_id;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createQuestion()
    {
        $this->setModelType();
        $model = new QtoaQuestion();
        $model = $model->find($this->request->get('question_id'));
        $viewModel =& $this;

        $this->setModelTypeByQuestion($model)->setTitlePage(trans('feedback::qtoa.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("feedback::qtoa.question.form", ['question' => $model, 'view_model' => $viewModel, 'append_route' => '.manage'
            , 'url_back' => route('feedback.qtoa.question.manage.get_list_questions', ['model_type' => $this->model_type, 'model_id' => $this->model_id])], "form");
    }

    /**
     * @return QuestionViewModel
     * @throws \Throwable
     */
    protected function editQuestion()
    {
        return $this->createQuestion();
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getModelTypes()
    {
        $model = new QtoaQuestion();
        return $model->getModelTypes();
    }

    /**
     * @return string
     */
    public function getDefaultModelType()
    {
        if ($this->model_type) {
            return $this->getModelTypeText();
        }
        return 'courses';
    }

    /**
     * @return array
     */
    public function getModelTypesAttrs()
    {
        $types = $this->getModelTypes();
        $attrs = [];
        foreach ($types as $key => $type) {
            $attrs[$key] = ['data-url' => route('feedback.qtoa.search', ['model_type' => $key])];
        }
        return $attrs;
    }

    /**
     * @return $this
     */
    protected function destroyQuestion()
    {
        $question_id = $this->request->get('question_id');
        $question = QtoaQuestion::active()->find($question_id);

        if ($question->delete()) {
            return $this->setResponse(true, trans("feedback::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveQuestion()
    {
        $this->setModelType();
        $model_type = $this->model_type;
        $model_id = $this->model_id;
        $data = $this->request->all();
        $data['instance_id'] = app('getInstanceObject')->getCurrentInstanceId();
        $response = $this->questionViewModel->serviceSaveQuestion($model_type, $model_id, $data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('feedback.qtoa.question.manage.edit', ['question_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @return $this
     */
    protected function changePublish()
    {
        $question_id = $this->request->get('question_id');
        $state = $this->request->get('state_publish');
        $response = $this->questionViewModel->serviceChangePublish($state, $question_id);
        return $this->setResponse($response['action'], $response['message']);
    }


    /**
     * @param $model_type
     * @param $model_id
     * @return array|bool
     */
    public function checkAccess($model_type, $model_id)
    {
        return true;
        //TODO check Access
        $model = $this->getModelType($model_type);
        if (!$model) {
            return false;
        }
        $data = $model->find($model_id);
        $params = $data ? $data->params_json : null;
        if (!$params) {
            return false;
        }

        $confirm_question = isset($params->confirm_question) ? $params->confirm_question : "by_admin";
        $access_question = isset($params->access_question) ? $params->access_question : "disable";

        $access = false;
        if ($access_question != "disable") {

            if ($access_question == "only_user") {
                $user = BridgeHelper::getAccess()->getUser();
                $access = $user ? true : false;
            } else {
                $access = true;
            }

        }

        $active = $confirm_question == "by_admin" ? 2 : 1;

        return ["active" => $active, "access_question" => $access];
    }

    /**
     * @param $model_type
     * @return bool
     */
    private function getModelType($model_type)
    {
        $question = new QtoaQuestion();
        $sArr = explode("_", $model_type);
        foreach ($sArr as &$item) {
            $item = ucfirst($item);
        }

        $methodName = implode("", $sArr);
        if (method_exists($question, "get" . $methodName)) {
            return $question->{"get" . $methodName}();
        }
        return false;
    }

}
