<?php

namespace Modules\Feedback\ViewModels\Qtoa;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Core\ViewModels\BaseViewModel;
use Modules\Course\Traits\Page\EnrollmentViewModelTrait;
use Modules\Feedback\Models\Qtoa\QtoaQuestion;
use Modules\Feedback\ViewModels\Qtoa\Answer\AnswerGridViewModel;

class QuestionViewModel extends BaseViewModel
{

    use EnrollmentViewModelTrait;

    private $qtoa_assets;
    private $configs;
    private $model_id;
    private $model_type;
    private $data_questions;

    public function __construct()
    {

    }

    /**
     * @return array|mixed|string
     */
    public function getAssetVersion()
    {
        return '1.0';
    }

    /**
     * @return array|mixed
     */
    public function getAccess()
    {
        return ["user_management/all"];
    }

    /**
     * @return $this|mixed
     */
    public function getLayout()
    {
        $this->layout = "layout";
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAssets()
    {
        $qtoa_assets = [];

        if ($this->qtoa_assets) {
            $qtoa_assets [] = ['container' => 'theme_js_2', 'src' => ("assets/js/backend/qtoa/qtoa.js"), 'name' => 'qtoa-backend'];
        }

        return $qtoa_assets;
    }


    /**
     * @param null $model_type
     * @param null $model_id
     * @return $this
     */
    public function setModelType($model_type = null, $model_id = null)
    {
        if (!$this->model_type || !$model_type) {
            $this->model_type = $model_type ?: request()->get("model_type", $this->model_type);
        }

        if (!$this->model_id || !$model_id) {
            $this->model_id = $model_id ?: request()->get("model_id", $this->model_id);
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function setAssetsCreateQuestion()
    {
        $this->qtoa_assets = true;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModelTypeText()
    {
        return $this->model_type;
    }

    /**
     * @return mixed
     */
    public function getModelId()
    {
        return $this->model_id;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    protected function createQuestion()
    {
        $this->setModelType();
        $model = new QtoaQuestion();
        $model = $model->find($this->request->get('question_id'));
        $viewModel =& $this;

        $this->setTitlePage(trans('feedback::qtoa.' . ($model ? 'edit' : 'add')));
        return $this->renderedView("feedback::qtoa.question.form", ['question' => $model, 'view_model' => $viewModel], "form");
    }

    /**
     * @return QuestionViewModel
     * @throws \Throwable
     */
    protected function editQuestion()
    {
        return $this->createQuestion();
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getModelTypes()
    {
        $model = new QtoaQuestion();
        return $model->getModelTypes();
    }

    /**
     * @return string
     */
    public function getDefaultModelType()
    {
        if ($this->model_type) {
            return $this->getModelTypeText();
        }
        return 'courses';
    }

    /**
     * @return array
     */
    public function getModelTypesAttrs()
    {
        $types = $this->getModelTypes();
        $attrs = [];
        foreach ($types as $key => $type) {
            $attrs[$key] = ['data-url' => route('feedback.qtoa.search', ['model_type' => $key])];
        }
        return $attrs;
    }

    /**
     * @return $this
     */
    protected function destroyQuestion()
    {
        $question_id = $this->request->get('question_id');
        $question = QtoaQuestion::enable()->find($question_id);

        if ($question->delete()) {
            return $this->setResponse(true, trans("feedback::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    protected function saveQuestion()
    {
        $this->setModelType();
        $model_type = $this->model_type;
        $model_id = $this->model_id;
        $data = $this->request->all();
        $data['instance_id'] = app('getInstanceObject')->getCurrentInstanceId();
        $user = BridgeHelper::getAccess()->getUser();
        $data['user_id'] = $user ? $user->id : null;
        $response = $this->serviceSaveQuestion($model_type, $model_id, $data);
        if (!$response['action']) {
            $this->redirectBack();
        } else {
            $this->redirect(route('feedback.qtoa.question.edit', ['question_id' => $response['data']->id]));
        }
        return $this->setDataResponse(isset($response['data']) ? $response['data'] : [])
            ->setResponse($response['action'], $response['message'], isset($response['errors']) ? $response['errors'] : []);
    }

    /**
     * @param $model_type
     * @param $model_id
     * @param $data
     * @return array
     */
    public function serviceSaveQuestion($model_type, $model_id, $data)
    {
        $question = new QtoaQuestion();
        $access = $this->checkAccess($model_type, $model_id);
        if (!$access) {
            return ['action' => false, 'message' => trans("feedback::messages.alert.not_access")];
        }

        $question_id = isset($data['question_id']) ? $data['question_id'] : 0;
        if ($question_id) {
            $question = $question->enable()->find($question_id);
            if (!$question) {
                return ['action' => false, 'message' => trans("feedback::messages.alert.not_find_data")];
            }
        }

        if ($this->isValidRequest($data, $question->rules, $question->messages, "POST")->isValidRequest) {
            $question->fill($data);
            if ($question->save()) {
                return ['action' => true, 'message' => trans("feedback::messages.alert.save_success"), 'data' => $question];
            } else {
                return ['action' => false, 'message' => trans("feedback::messages.alert.save_un_success")];
            }
        }
        return ['action' => false, 'message' => trans("feedback::messages.alert.mis_data"), 'errors' => $this->errors];
    }

    /**
     * @return $this
     */
    protected function changePublish()
    {
        $question_id = $this->request->get('question_id');
        $state = $this->request->get('state_publish');
        $response = $this->serviceChangePublish($state, $question_id);
        return $this->setResponse($response['action'], $response['message']);
    }

    /**
     * @param null $state_publish
     * @param $question_id
     * @return array
     */
    public function serviceChangePublish($state_publish, $question_id)
    {
        $state = $state_publish ?: 'disable';
        $state_publish = $state == 'active' ? 1 : 0;
        $question = new QtoaQuestion();
        $model = $question->find($question_id);
        $model->published = $state_publish;

        if ($model->save()) {
            return ['action' => true, 'message' => trans("feedback::messages.alert.save_success"), 'data' => $question];
        }
        return ['action' => false, 'message' => trans("feedback::messages.alert.save_un_success")];
    }

    /**
     * @param $model_type
     * @param $model_id
     * @return array|bool
     */
    public function checkAccess($model_type, $model_id)
    {
        return true;
        //TODO check Access
        $model = $this->getModelType($model_type);
        if (!$model) {
            return false;
        }
        $data = $model->find($model_id);
        $params = $data ? $data->params_json : null;
        if (!$params) {
            return false;
        }

        $confirm_question = isset($params->confirm_question) ? $params->confirm_question : "by_admin";
        $access_question = isset($params->access_question) ? $params->access_question : "disable";

        $access = false;
        if ($access_question != "disable") {

            if ($access_question == "only_user") {
                $user = BridgeHelper::getAccess()->getUser();
                $access = $user ? true : false;
            } else {
                $access = true;
            }

        }

        $active = $confirm_question == "by_admin" ? 2 : 1;

        return ["active" => $active, "access_question" => $access];
    }

    /**
     * @param $model_type
     * @return bool
     */
    private function getModelType($model_type)
    {
        $question = new QtoaQuestion();
        $methodName = get_uppercase_by_underscores($model_type);
        if (method_exists($question, "get" . $methodName)) {
            return $question->{"get" . $methodName}();
        }
        return false;
    }

    /**
     * @param bool $json
     * @param bool $byResponse
     * @return $this
     * @throws \Throwable
     */
    public function getQuestions($json = false, $byResponse = true)
    {
        $this->setModelType();
        $viewModel =& $this;
        $question = new QtoaQuestion();
        $questions = $question->setRequestItems(['model_type' => $this->model_type, 'model_id' => $this->model_id])
            ->active()
            ->filterByModel()
            ->filterByUser()
            ->with("user")
            ->where(function ($q){
                if($this->getDataConfigs('hot_questions')){
                     $q->where('is_hot',1);
                }
                if($this->getDataConfigs('my_questions') && $this->getDataConfigs('user_id')){
                     $q->active()->where('user_id',$this->getDataConfigs('user_id'));
                }
            })
            ->orderBy("id", "DESC")
            ->paginate($this->perPage);

        $this->data_questions = $questions;
        $this->setUserRoleAndAccessOnCourse($this->model_id);
        $this->decorate();
        if ($json) {
            if ($byResponse) {
                return $this->setDataResponse($this->data_questions)->setResponse($this->data_questions ? true : false, !$this->data_questions ? trans("feedback::messages.alert.not_find_data") : '');
            } else {
                return $this->data_questions;
            }
        } else {
            if ($byResponse) {
                return $this->renderedView('feedback::qtoa.front.list_qtoa', ['viewModel' => $viewModel])->setResponse(true);
            } else {
                return view('feedback::qtoa.front.list_qtoa', ['viewModel' => $viewModel])->render();
            }
        }

    }

    /**
     * @param $configs
     * @return $this
     */
    public function setDataConfigs($configs)
    {
        $this->configs = $configs;
        return $this;
    }

    /**
     * @param $key
     * @return null
     */
    public function getDataConfigs($key)
    {
        if(isset($this->configs->$key)){
            return $this->configs->$key;
        }
        return null;
    }

    public function getList($json = false)
    {
        $result = $this->getQuestions($json, false);
        if ($json) {
            return $this->setDataResponse($result)->setResponse(true);
        } else {
            $viewModel =& $this;
            return $this->renderedView('feedback::qtoa.front.list', ['viewModel' => $viewModel, 'result' => $result]);
        }

    }

    public function getContent()
    {
        return $this->getDataQtoa();
    }


    /**
     * @return mixed
     */
    public function getDataQtoa()
    {
        return $this->data_questions;
    }

    /**
     * @return $this
     */
    private function decorate()
    {
        if ($this->data_questions) {
            $answerViewModel = new AnswerGridViewModel();
            $data_questions = ($this->data_questions->getCollection())->map(function ($question, $index) use ($answerViewModel) {
                $question->answers = $answerViewModel->setQuestionId($question->id)->getList(true);
                if (!$question->answers->response['action']) {
                    $question->answers = collect();
                    return $question;
                } else {
                    $question->answers = $question->answers->response['data'];
                }
                $question->answers->setPath(route('feedback.qtoa.question.answer.get_list', ['question_id' => $question->id]));
                return $question;
            });
            $this->data_questions->setCollection($data_questions);

            $this->data_questions->setPath(route('feedback.qtoa.question.get_questions', ['model_type' => $this->model_type, 'model_id' => $this->model_id]));
        }
        return $this;
    }

    /**
     * @param null $model_type
     * @return $this
     */
    public function searchInModel($model_type = null)
    {
        $model_type = $model_type ?: ($this->request ? $this->request->get('model_type') : null);
        if (!$model_type) {
            return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));
        }

        $model = $this->getModelType($model_type);
        if (!$model) {
            return $this->setResponse(false, trans("feedback::messages.alert.not_find_model"));
        }

        $q = false;
        $selected = false;
        if ($this->request->has('q')) {
            $q = $this->request->get('q');
        } elseif ($this->request->has('selected')) {
            $inputs = explode(',', $this->request->get('selected'));
            $selected = [];
            foreach ($inputs as $select) {
                $select = trim($select);
                if (strlen($select)) {
                    $selected[] = $select;
                }
            }
        }
        $model = $model->filterCurrentInstance()->select('id', 'title as text');
        if ($q) {
            $model->where(function ($query) use ($q) {
                $query->where('title', 'LIKE', '%' . $q . '%');
            });
            $items = $model->enable()->get();

        } elseif ($selected && count($selected)) {
            $model->whereIn('id', $selected);
            $items = $model->get();

        } else {
            $items = $model->orderBy('id', 'DESC')->take(20)->get();
        }

        return $this->setDataResponse($items)->setResponse(true);
    }

    /**
     * @return $this
     */
    public function deleteAll()
    {
        $ids = $this->request->get('row_items');

        if (empty($ids)) {
            return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));
        }


        $questions = QtoaQuestion::active()->whereIn('id', $ids)->get();
        $res = false;
        foreach ($questions as $question) {
            $r = $question->delete();
            if ($r && !$res) {
                $res = true;
            }
        }

        if ($res) {
            return $this->setResponse(true, trans("feedback::messages.alert.del_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.del_un_success"));

    }

    /**
     * @return $this
     */
    public function changePublishAll()
    {

        $ids = $this->request->get('row_items');

        if (empty($ids)) {
            return $this->setResponse(false, trans("feedback::messages.alert.not_find_data"));
        }
        $state = $this->request->get('state_publish');
        $response = [];
        foreach ($ids as $id) {
            $response[] = $this->serviceChangePublish($state, $id);
        }

        if ($response) {
            return $this->setDataResponse($response)->setResponse(true, trans("feedback::messages.alert.save_success"));
        }
        return $this->setResponse(false, trans("feedback::messages.alert.error"));
    }

}
