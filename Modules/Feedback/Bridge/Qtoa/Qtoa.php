<?php

namespace Modules\Feedback\Bridge\Qtoa;

use Modules\Course\Traits\Page\EnrollmentViewModelTrait;
use Modules\Feedback\Traits\Qtoa\QtoaTrait;

/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class Qtoa
{
    use QtoaTrait;
    use EnrollmentViewModelTrait;

    /**
     *  sample:
     * BridgeHelper::getQtoa()->getQtoaObject()->setModel("courses", 1)
     * ->setWithData(true)
     * ->setWithForm(true)
     * ->setConfigs(["perPage" => 15, "container_js" => "inline", "container_css" => "inline_style" , "template" => "feedback::comment.front.form_comment" , "template_list" => "feedback::comment.front.list_comments"])
     * ->init()
     * ->getForm()
     *
     * add assets sample in $configs: assets_rate = ['modules/feedback/comment.js', 'modules/feedback/comment.css']
     * @param $model_type
     * @param $model_id
     * @param array $options
     * @return $this
     */
    public function getQtoaObject($model_type, $model_id, array $options = ['with_form_qtoa'=>true, 'with_data_questions'=>true])
    {
        $this->setUserRoleAndAccessOnCourse($model_id);
        return $this->setModel($model_type, $model_id)->setConfigs($options)->init();
    }

    /**
     * @param array $options
     * @return $this
     */
    public function getObject(array $options = ['with_form_qtoa'=>true, 'with_data_questions'=>true])
    {
        return $this->setConfigs($options);
    }

}