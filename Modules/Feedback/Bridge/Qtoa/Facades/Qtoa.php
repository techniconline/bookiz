<?php
namespace Modules\Feedback\Bridge\Qtoa\Facades;

use Illuminate\Support\Facades\Facade;

class Qtoa extends Facade
{

    /**
     * get sms service
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Qtoa'; }

}