<?php

namespace Modules\Feedback\Bridge\Rate;

use Modules\Feedback\Traits\Rate\RateTrait;

/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class Rate
{
    use RateTrait;

    /**
     * sample:
     BridgeHelper::getRate()->getRateObject()
    ->setModel("courses", 1)
    ->setWithData(true)
    ->setWithForm(true)
    ->setWithSaveRate(false)
    ->setWithTextRate(false)
    ->setConfigs(["container_js" => "inline", "container_css" => "inline_style", "template" => "feedback::rate.front.form_rate"])
    ->init()
    ->getForm()
     *
     * add assets sample in $configs: assets_rate = ['modules/feedback/rate.js', 'modules/feedback/rate.css']
     * @return $this
     */
    public function getRateObject()
    {
        return $this;
    }

    /**
     * $options = [user_id=>int, with_data_rate=>bool, with_form_rate=>bool
     *              , with_save_rate=>bool
     *              , with_text_rate=>bool
     *              , container_css=>string
     *              , container_js=>string
     *              , template=>string
     *              , assets_rate=>array ]
     * @param $model_name
     * @param $model_id
     * @param array $options
     * @return $this
     */
    public function getRateView($model_name, $model_id, $options = ["with_data_rate"=>true, "with_form_rate"=>true, "with_save_rate"=>true, "with_text_rate"=>true])
    {
        $this->setModel($model_name, $model_id);
        if($options){
            $this->setConfigs($options);
        }
        return $this->init()->getForm();
    }

    /**
     * @param $model_name
     * @param $model_id
     * @param int $rate_value
     * @param null $user_id
     * @return mixed
     */
    public function saveRateObject($model_name, $model_id, $rate_value = 3, $user_id = null)
    {
        return $this->setModel($model_name, $model_id)->setRateValue($rate_value)->setUserId($user_id)->boot()->saveRate();
    }

}