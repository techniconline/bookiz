<?php
namespace Modules\Feedback\Bridge\Rate\Facades;

use Illuminate\Support\Facades\Facade;

class Rate extends Facade
{

    /**
     * get sms service
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Rate'; }

}