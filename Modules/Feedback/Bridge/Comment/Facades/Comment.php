<?php
namespace Modules\Feedback\Bridge\Comment\Facades;

use Illuminate\Support\Facades\Facade;

class Comment extends Facade
{

    /**
     * get sms service
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Comment'; }

}