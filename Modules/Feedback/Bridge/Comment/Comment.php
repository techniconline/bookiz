<?php

namespace Modules\Feedback\Bridge\Comment;

use Modules\Feedback\Traits\Comment\CommentTrait;

/**
 * Class FormHelper
 * @package Modules\Core\Providers\Helpers\Form
 */
class Comment
{
    use CommentTrait;

    /**
     *  sample:
    BridgeHelper::getComment()->getCommentObject()->setModel("courses", 1)
    ->setWithData(true)
    ->setWithForm(true)
    ->setConfigs(["perPage" => 15, "container_js" => "inline", "container_css" => "inline_style" , "template" => "feedback::comment.front.form_comment" , "template_list" => "feedback::comment.front.list_comments"])
    ->init()
    ->getForm()
     *
     * add assets sample in $configs: assets_rate = ['modules/feedback/comment.js', 'modules/feedback/comment.css']
     * @return $this
     */
    public function getCommentObject()
    {
        return $this;
    }


    /**
     * @return \Modules\Feedback\Models\Comment\Comment
     */
    public function getCommentModel()
    {
        return new \Modules\Feedback\Models\Comment\Comment();
    }

}