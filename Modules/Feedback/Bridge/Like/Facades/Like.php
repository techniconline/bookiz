<?php
namespace Modules\Feedback\Bridge\Like\Facades;

use Illuminate\Support\Facades\Facade;

class Like extends Facade
{

    /**
     * get sms service
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Like'; }

}