<?php

namespace Modules\Feedback\Bridge\Like;

/**
 * Class FormHelper
 */
class Like
{

    /**
     * @return \Modules\Feedback\Models\Like\Like
     */
    public function getLikeModel()
    {
        return new \Modules\Feedback\Models\Like\Like();
    }

    /**
     * @return string
     */
    public function getLikeClass()
    {
        return \Modules\Feedback\Models\Like\Like::class;
    }

}