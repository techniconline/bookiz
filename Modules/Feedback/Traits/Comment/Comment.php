<?php

namespace Modules\Feedback\Traits\Comment;


use Modules\Blog\Models\Blog;
use Modules\Course\Models\Course\Course;
use Modules\Entity\Models\Entity;
use Modules\Video\Models\Video\Video;

trait Comment
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [

        ];
        return $this;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByModel($query)
    {
        $query->where(function ($q){
            if(($model_type = $this->getRequestItem('model_type')) && ($model_id = $this->getRequestItem('model_id'))){
                $q->where('model_type', $model_type);
                $q->where('model_id', $model_id);
            }

        });
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByUser($query)
    {
        $query->where(function ($q){

            if($user_id = $this->getRequestItem('user_id')){
                $q->where('user_id', $user_id);
            }

        });
        return $query;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function comment()
    {
        return $this->belongsTo('Modules\Feedback\Models\Comment\Comment', 'parent_id')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commentChildes()
    {
        return $this->hasMany('Modules\Feedback\Models\Comment\Comment','parent_id','id')->active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childrenRecursive()
    {
        return $this->commentChildes()->with('childrenRecursive');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getCourses()
    {
        return Course::active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getBlogs()
    {
        return Blog::active();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getVideos()
    {
        return Video::active();
    }

    /**
     * @return mixed
     */
    public function getEntities()
    {
        return Entity::active();
    }

}
