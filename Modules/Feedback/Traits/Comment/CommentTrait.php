<?php

namespace Modules\Feedback\Traits\Comment;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Feedback\ViewModels\Comment\CommentViewModel;


trait CommentTrait
{

    public $model_name;
    public $model_id;
    public $user_id;
    public $user;
    public $by_action_form = true;
    public $with_data_comments;
    public $with_form_comment;
    public $view_form_comment;
    public $view_list_comments;
    public $data_comments;
    public $assets_comment = ['modules/feedback/comment.js'];
    private $perPage = 15;
    private $container_css = "inline_style";
    private $container_js = "inline";
    private $template = "feedback::comment.front.form_comment";
    private $template_list = "feedback::comment.front.list_comments";

    /**
     * @param $model_name
     * @param $model_id
     * @return $this
     */
    public function setModel($model_name, $model_id)
    {
        $this->model_name = $model_name;
        $this->model_id = $model_id;
        return $this;
    }

    /**
     * @param null $user
     * @return $this
     */
    public function setUser($user = null)
    {
        $user = $user ?: BridgeHelper::getAccess()->getUser();
        if ($user) {
            $this->user_id = $user->id;
            $this->user = $user;
        }
        return $this;
    }


    /**
     * @param array $configs
     * @return $this
     */
    public function setConfigs(array $configs)
    {
        foreach ($configs as $key => $config) {
            $this->{$key} = $config;
        }
        return $this;
    }

    /**
     * @param int $perPage
     * @return $this
     */
    public function setPerPage($perPage = 15)
    {
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @param bool $with_data
     * @return $this
     */
    public function setWithData($with_data = true)
    {
        $this->with_data_comments = $with_data;
        return $this;
    }

    /**
     * @param bool $with_form
     * @return $this
     */
    public function setWithForm($with_form = true)
    {
        $this->with_form_comment = $with_form;
        return $this;
    }

    /**
     * @return $this
     */
    private function setForm()
    {
        if (!$this->with_form_comment) {
            return $this;
        }
        $view = view($this->template, ["viewModel" => $this]);

        $this->view_form_comment = $view;
        return $this;
    }

    /**
     * @param $template
     * @return $this
     */
    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return $this
     */
    public function getForm()
    {
        return $this->view_form_comment;
    }

    /**
     * @return mixed
     */
    public function getDataComments()
    {
        return $this->data_comments;
    }

    /**
     * @return $this
     */
    private function setDataComments()
    {
        if (!$this->with_data_comments) {
            return $this;
        }

        $comment = new \Modules\Feedback\Models\Comment\Comment();
        $this->data_comments = $comment
            ->setRequestItems(["model_id" => $this->model_id, "model_type" => $this->model_name])
            ->active()
            ->filterByModel()
            ->filterByUser()
            ->with("user")
            ->orderBy("id", "DESC")
            ->paginate($this->perPage);

        return $this->setViewListComments();
    }

    /**
     * @return $this
     */
    private function setViewListComments()
    {
        if ($this->data_comments) {
            $view = view($this->template_list, ["viewModel" => $this]);
            $this->view_list_comments = $view;
        }
        return $this;
    }

    /**
     * @param $template
     * @return $this
     */
    public function setTemplateList($template)
    {
        $this->template_list = $template;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getViewListComments()
    {
        return $this->view_list_comments;
    }

    /**
     * @return $this
     */
    public function init()
    {
        $commentViewModel = new CommentViewModel();
        $access = $commentViewModel->checkAccess($this->model_name, $this->model_id);

        if((!$access) || (isset($access["access_comment"]) && !$access["access_comment"])){
            return $this;
        }

        $this->setUser()->setAssetsOnTheme();
        return $this->setDataComments()->setForm();
    }

    /**
     * @return $this
     */
    public function setAssetsOnTheme()
    {
        if ($this->assets_comment) {
            $assets = [];
            foreach ($this->assets_comment as $value) {
                if (strlen($value)) {
                    $name = basename($value);
                    if (strpos($name, '.css') != false) {
                        $assets [] = ['container' => $this->container_css ?: "inline_style", 'src' => trim($value), 'name' => $name];
                    } elseif (strpos($name, '.js') != false) {
                        $assets [] = ['container' => $this->container_js ?: "inline", 'src' => trim($value), 'name' => $name];
                    }
                }
            }
            $this->setAssetsOnRoute($assets);
        }

        return $this;
    }

    /**
     * @param $assets
     * @return $this
     */
    public function setAssetsOnRoute($assets)
    {
        if (count($assets)) {
            app('getInstanceObject')->setActionAssets($assets);
        }
        return $this;
    }


}
