<?php

namespace Modules\Feedback\Traits\Like;


use Modules\Blog\Models\Blog;
use Modules\Core\Traits\Decorate\DecorateData;
use Modules\Course\Models\Course\Course;
use Modules\Entity\Models\Entity;
use Modules\Feedback\Models\Qtoa\QtoaAnswer;
use Modules\Feedback\Models\Qtoa\QtoaQuestion;
use Modules\User\Models\User;

trait Like
{

    use DecorateData;

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [

        ];
        return $this;
    }


    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByModel($query)
    {
        $query->where(function ($q) {

            if (($model_type = $this->getRequestItem('model_type', 'entity'))) {
                $q->where('model_type', $model_type);
            }

            if ( ($model_id = $this->getRequestItem('model_id'))) {
                $q->where('model_id', $model_id);
            }

        });
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByUser($query, $user_id = null)
    {
        $user_id = $user_id ?: $this->getRequestItem('user_id');
        $query->where(function ($q) use ($user_id) {

            if ($user_id) {
                $q->where('user_id', $user_id);
            } else {
                $q->where('user_id', 0);
            }

        });
        return $query;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getLikeTextAttribute()
    {
        return $this->getLikeState($this->like);
    }

    /**
     * @param $like
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getLikeState($like)
    {
        return trans('feedback::like.like_states.' . $like);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection|mixed|null
     */
    public function getModelDataAttribute()
    {
        $model_type = $this->model_type;
        $model = get_uppercase_by_underscores($model_type);
        $model = "get" . $model;
        if (method_exists($this, $model) && $this->model_id) {
            $model = $this->{$model}();
            $data = $model->find($this->model_id);
            if ($data) {
                $data = $this->decorateAttributes($data);
            }
            return $data;
        }
        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     */
    public function getCourses()
    {
        return new Course();
    }

    /**
     * @return Entity
     */
    public function getEntity()
    {
        return new Entity();

    }

    /**
     */
    public function getBlogs()
    {
        return new Blog();

    }

    /**
     */
    public function getQtoaQuestions()
    {
        return new  QtoaQuestion();
    }

    /**
     */
    public function getQtoaAnswers()
    {
        return new  QtoaAnswer();
    }

}
