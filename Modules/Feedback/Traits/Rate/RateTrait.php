<?php

namespace Modules\Feedback\Traits\Rate;

use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Feedback\ViewModels\Rate\RateViewModel;

trait RateTrait
{

    public $model_name;
    public $model_id;
    public $user;
    public $rate_value;
    public $view_model;

    public $user_id;
    public $types;
    public $with_data_rate;
    public $with_form_rate;
    public $view_form_rate;
    public $data_rate = 5;
    public $data_count_rate = 0;
    public $assets_rate;

    public $with_save_rate = true;
    public $with_text_rate = true;
    private $container_css = "inline_style";
    private $container_js = "inline";
    private $template = "feedback::rate.front.form_rate";

    /**
     * @param $model_name
     * @param $model_id
     * @return $this
     */
    public function setModel($model_name, $model_id)
    {
        $this->model_name = $model_name;
        $this->model_id = $model_id;
        return $this;
    }

    /**
     * @param $user
     * @return $this
     */
    public function setUser($user = null)
    {
        if($this->user_id){
           return $this;
        }

        $user = $user ?: BridgeHelper::getAccess()->getUser();
        if ($user) {
            $this->user_id = $user->id;
            $this->user = $user;
        }
        return $this;
    }

    /**
     * @param $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setRateValue($value)
    {
        $this->rate_value = $value;
        return $this;
    }

    /**
     * @param array $configs
     * @return $this
     */
    public function setConfigs(array $configs)
    {
        foreach ($configs as $key => $config) {
            $this->{$key} = $config;
        }
        return $this;
    }

    /**
     * @param bool $with_data
     * @return $this
     */
    public function setWithData($with_data = true)
    {
        $this->with_data_rate = $with_data;
        return $this;
    }

    /**
     * @param bool $with_form
     * @return $this
     */
    public function setWithForm($with_form = true)
    {
        $this->with_form_rate = $with_form;
        return $this;
    }

    /**
     * @param array $types
     * @return $this
     */
    public function setTypes($types = ['ambiance'=>['title'=>'Ambiance']])
    {
        $this->types = $types;
        return $this;
    }

    /**
     * @param null $key
     * @return null
     */
    public function getTypes($key = null)
    {
        if ($key){
            if (isset($this->types[$key])){
                return $this->types[$key];
            }else{
                return null;
            }
        }
        return $this->types;
    }

    /**
     * @return $this
     */
    private function setForm()
    {
        if (!$this->with_form_rate) {
            return $this;
        }

        $view = view($this->template, ["viewModel" => $this]);
        $this->view_form_rate = $view;
        return $this;
    }

    /**
     * @param $template
     * @return $this
     */
    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return $this
     */
    public function getForm()
    {
        return $this->view_form_rate;
    }

    /**
     * @return mixed
     */
    public function getTotalRate()
    {
        return $this->data_rate;
    }

    /**
     * @return int
     */
    public function getCountRated()
    {
        return $this->data_count_rate;
    }

    /**
     * @param $key
     * @return array
     */
    public function getTotalRateType($key)
    {
        $model = $this->getModelTypes($key);
        $rate = new \Modules\Feedback\Models\Rate\Rate();
        $rate = $rate->setRequestItems(["model_id" => $this->model_id, "model_type" => $model])
            ->active()
            ->filterByModel()
            ->filterByUser();
        $data_rate = $rate->avg("rate");
        $data_rate = $data_rate ?: 5;
        $data_rate = number_format($data_rate,1);
        $data_count_rate = $rate->count("rate");
        return ['rate'=>$data_rate, 'count_rate'=>$data_count_rate];
    }

    /**
     * @param $type
     * @return string
     */
    public function getModelTypes($type)
    {
        if ($this->model_name){
            $model = $this->model_name."_".$type;
            return $model;
        }
        return $type;
    }

    /**
     * @return $this
     */
    private function setDataRate()
    {
        if (!$this->with_data_rate) {
            return $this;
        }

        $rate = new \Modules\Feedback\Models\Rate\Rate();
        $rate = $rate
            ->setRequestItems(["model_id" => $this->model_id, "model_type" => $this->model_name])
            ->active()
            ->filterByModel()
            ->filterByUser();

        $this->data_rate = $rate->avg("rate");
        $this->data_rate = $this->data_rate ?: 5;
        $this->data_rate = number_format($this->data_rate,1);

        $this->data_count_rate = $rate->count("rate");

        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (get_instance()->isApi()){
            if ($types = $this->getTypes()){
                $typeData = [];
                foreach ($types as $key => $type) {
                    $typeData[$key]['values'] = $this->getTotalRateType($key);
                    $typeData[$key]['options'] = $type;
                }
                $this->setTypes([]);
                return $typeData;
            }
        }
        return [
            'data_rate' => $this->data_rate,
            'data_count_rate' => $this->data_count_rate,
        ];
    }

    /**
     * @return mixed
     */
    public function saveRate()
    {
        $result = $this->view_model->saveRate($this->rate_value,$this->model_id,$this->model_name,$this->user_id);
        return $result;
    }

    /**
     * @return $this
     */
    public function boot()
    {
        $viewModel = new RateViewModel();
        $this->view_model = $viewModel;
        $access = $viewModel->checkAccess($this->model_name, $this->model_id);

        if(!$access){
            return $this;
        }

        $this->setUser();
        return $this;
    }

    /**
     * @return $this
     */
    public function init()
    {
        $this->boot()->initTheme()->initDataForm();
        return $this;
    }

    /**
     * @return $this
     */
    public function initTheme()
    {
        $this->setAssets()->setAssetsOnTheme();
        return $this;
    }

    /**
     * @return $this
     */
    public function initDataForm()
    {
        $this->setDataRate()->setForm();
        return $this;
    }

    /**
     * @param array $assets
     * @return $this
     */
    public function setAssets($assets = [])
    {
        $this->assets_rate = ['modules/feedback/rate.js', 'modules/feedback/rate.css'];
        if ($assets) {
            $this->assets_rate = $assets;
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function setAssetsOnTheme()
    {
        if ($this->assets_rate) {
            $assets = [];
            foreach ($this->assets_rate as $value) {
                if (strlen($value)) {
                    $name = basename($value);
                    if (strpos($name, '.css') != false) {
                        $assets [] = ['container' => $this->container_css ?: "inline_style", 'src' => trim($value), 'name' => $name];
                    } elseif (strpos($name, '.js') != false) {
                        $assets [] = ['container' => $this->container_js ?: "inline", 'src' => trim($value), 'name' => $name];
                    }
                }
            }

            $this->setAssetsOnRoute($assets);
        }

        return $this;
    }

    /**
     * @param $assets
     * @return $this
     */
    public function setAssetsOnRoute($assets)
    {
        if (count($assets)) {
            app('getInstanceObject')->setActionAssets($assets);
        }
        return $this;
    }

    /**
     * @param bool $status
     * @return $this
     */
    public function setWithSaveRate($status = false)
    {
        $this->with_save_rate = $status;
        return $this;
    }

    /**
     * @param bool $status
     * @return $this
     */
    public function setWithTextRate($status = false)
    {
        $this->with_text_rate = $status;
        return $this;
    }


}
