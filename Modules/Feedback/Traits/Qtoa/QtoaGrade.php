<?php

namespace Modules\Feedback\Traits\Qtoa;

use Modules\User\Models\User;

trait QtoaGrade
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function qtoaAnswer()
    {
        return $this->belongsTo(\Modules\Feedback\Models\Qtoa\QtoaAnswer::class, 'answer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function qtoaQuestion()
    {
        return $this->belongsTo(\Modules\Feedback\Models\Qtoa\QtoaQuestion::class, 'question_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
