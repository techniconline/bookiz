<?php

namespace Modules\Feedback\Traits\Qtoa;

use Illuminate\Pagination\Paginator;
use Modules\Core\Providers\Helpers\Bridge\Facades\BridgeHelper;
use Modules\Feedback\ViewModels\Qtoa\Answer\AnswerGridViewModel;
use Modules\Feedback\ViewModels\Qtoa\QuestionViewModel;


trait QtoaTrait
{

    public $model_name;
    public $model_id;
    public $user_id;
    public $user;
    public $my_questions = false;
    public $hot_questions = false;
    public $with_data_questions;
    public $with_form_qtoa;
    public $view_qtoa;
    public $view_form_qtoa;
    public $view_form_qtoa_answer;
    public $view_list_questions;
    public $data_questions;
    public $assets_question = ['modules/feedback/qtoa/question.js'];
    private $perPage = 10;
    private $container_css = "inline_style";
    private $container_js = "inline";
    private $template = "feedback::qtoa.front.form_qtoa";
    private $template_answer = "feedback::qtoa.front.form_answer";
    private $template_qtoa = "feedback::qtoa.front.qtoa";
    private $template_list = "feedback::qtoa.front.list";

    /**
     * @param $model_name
     * @param $model_id
     * @return $this
     */
    public function setModel($model_name, $model_id)
    {
        $this->model_name = $model_name;
        $this->model_id = $model_id;
        return $this;
    }

    /**
     * @param null $user
     * @return $this
     */
    public function setUser($user = null)
    {
        $user = $user ?: BridgeHelper::getAccess()->getUser();
        if ($user) {
            $this->user_id = $user->id;
            $this->user = $user;
        }
        return $this;
    }

    /**
     * @param array $configs
     * @return $this
     */
    public function setConfigs(array $configs)
    {
        $this->my_questions = false;
        $this->hot_questions = false;
        foreach ($configs as $key => $config) {
            $this->{$key} = $config;
        }
        return $this;
    }

    /**
     * @param int $perPage
     * @return $this
     */
    public function setPerPage($perPage = 15)
    {
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @param bool $with_data
     * @return $this
     */
    public function setWithData($with_data = true)
    {
        $this->with_data_questions = $with_data;
        return $this;
    }

    /**
     * @param bool $with_form
     * @return $this
     */
    public function setWithForm($with_form = true)
    {
        $this->with_form_qtoa = $with_form;
        return $this;
    }

    /**
     * @return $this
     */
    private function setForm()
    {
        if (!$this->with_form_qtoa) {
            return $this;
        }
        $view = view($this->template, ["viewModel" => $this]);

        $this->view_form_qtoa = $view;
        return $this;
    }

    /**
     * @param $question_id
     * @return $this
     */
    private function setFormAnswer($question_id)
    {
        if (!$this->with_form_qtoa) {
            return $this;
        }
        $view = view($this->template_answer, ["viewModel" => $this, 'question_id' => $question_id]);

        $this->view_form_qtoa_answer = $view;
        return $this;
    }

    /**
     * @param $template
     * @return $this
     */
    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return $this
     */
    private function setDataQtoas()
    {
        if (!$this->with_data_questions) {
            return $this;
        }

        $question = new QuestionViewModel();
        $this->data_questions = $question->setDataConfigs($this)->setModelType($this->model_name, $this->model_id)->getList();
        return $this->setViewListQtoas($this->data_questions->response['content']);

    }

    /**
     * @param null $view
     * @return $this
     */
    private function setViewListQtoas($view = null)
    {
        $this->view_list_questions = $view;
        if ($this->data_questions && !$view) {
            $view = view($this->template_list, ["viewModel" => $this]);
            $this->view_list_questions = $view;
        }
        return $this;
    }

    /**
     * @param $template
     * @return $this
     */
    public function setTemplateList($template)
    {
        $this->template_list = $template;
        return $this;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->view_list_questions;
    }

    /**
     * @return $this
     */
    public function getForm()
    {
        return $this->view_form_qtoa;
    }

    /**
     * @return mixed
     */
    public function getQtoa()
    {
        return $this->setQtoaForm()->view_qtoa;
    }

    /**
     * @return $this
     */
    private function setQtoaForm()
    {
        if (!$this->template_qtoa) {
            return $this;
        }

        $view = view($this->template_qtoa, ["viewModel" => $this]);
        $this->view_qtoa = $view;
        return $this;
    }

    /**
     * @param $question_id
     * @return string
     */
    public function getFormAnswer($question_id)
    {
        $this->setFormAnswer($question_id);
        return $this->view_form_qtoa_answer;
    }

    /**
     * @return mixed
     */
    public function getDataQtoa()
    {
        return $this->data_questions;
    }

    /**
     * @return $this
     */
    public function init()
    {
        $questionViewModel = new QuestionViewModel();
        $access = $questionViewModel->checkAccess($this->model_name, $this->model_id);
        if ((!$access)) {
            return $this;
        }
        $this->setUser()->setAssetsOnTheme()->setDataQtoas()->setForm();
        return $this;
    }

    /**
     * @return $this
     */
    public function setAssetsOnTheme()
    {
        if ($this->assets_question) {
            $assets = [];
            foreach ($this->assets_question as $value) {
                if (strlen($value)) {
                    $name = basename($value);
                    if (strpos($name, '.css') != false) {
                        $assets [] = ['container' => $this->container_css ?: "inline_style", 'src' => trim($value), 'name' => $name];
                    } elseif (strpos($name, '.js') != false) {
                        $assets [] = ['container' => $this->container_js ?: "inline", 'src' => trim($value), 'name' => $name];
                    }
                }
            }
            $this->setAssetsOnRoute($assets);
        }

        return $this;
    }

    /**
     * @param $assets
     * @return $this
     */
    public function setAssetsOnRoute($assets)
    {
        if (count($assets)) {
            app('getInstanceObject')->setActionAssets($assets);
        }
        return $this;
    }


}
