<?php

namespace Modules\Feedback\Traits\Qtoa;

use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;
use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\Course\Models\Course\Course;
use Modules\User\Models\User;

trait QtoaQuestion
{

    public function scopeActive($query)
    {
        $query = parent::scopeActive($query);
        $query = $query->published();
        return $query;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getModelTypes()
    {
        return trans('feedback::qtoa.model_types');
    }

    /**
     * @param $question_id
     * @param $value_like
     * @param bool $update
     * @return bool
     */
    public function updateLike($question_id, $value_like, $update = false)
    {
        $row = \Modules\Feedback\Models\Qtoa\QtoaQuestion::find($question_id);
        if(!$row){
            return false;
        }

        if ($value_like) {
            $old = (int)($row->liked);
            $old_dislike = (int)($row->dislike);
            $row->liked = $old + 1;
            if ($update) {
                $row->dislike = $old_dislike - 1;
            }
        } else {
            $old = (int)($row->dislike);
            $old_liked = (int)($row->liked);
            $row->dislike = $old + 1;
            if ($update) {
                $row->liked = $old_liked - 1;
            }
        }
        $row->save();
        return true;
    }

    /**
     * @return null
     */
    public function getCreatedDateAttribute()
    {
        if ($this->created_at){
            return DateHelper::setDateTime($this->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        }
        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo(Instance::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function qtoaAnswers()
    {
        return $this->hasMany(\Modules\Feedback\Models\Qtoa\QtoaAnswer::class, 'question_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function qtoaGrades()
    {
        return $this->hasMany(\Modules\Feedback\Models\Qtoa\QtoaGrade::class, 'question_id');
    }

    /**
     * @return mixed
     */
    public function getCourses()
    {
        return new Course();
    }

}
