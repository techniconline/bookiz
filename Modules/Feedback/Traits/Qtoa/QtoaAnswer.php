<?php

namespace Modules\Feedback\Traits\Qtoa;

use Modules\Core\Providers\Helpers\Date\Facades\DateHelper;
use Modules\User\Models\User;

trait QtoaAnswer
{

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        $query = parent::scopeActive($query);
        $query = $query->published();
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterQuestion($query)
    {
        $question_id = $this->getRequestItem('question_id');
        if ($question_id) {
            if (is_array($question_id)) {
                $query = $query->whereIn('question_id', $question_id);
            } else {
                $query = $query->where('question_id', $question_id);
            }
        }
        $query = $this->scopeFilterByModelType($query);
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByModelType($query)
    {
        $query = $query->whereHas('qtoaQuestion', function ($q){
            $model_type = $this->getRequestItem('model_type');
            $model_id = $this->getRequestItem('model_id');
            if($model_type){
                $q = $q->where('model_type',$model_type);
            }
            if($model_id){
                $q = $q->where('model_id',$model_id);
            }
            return $q;
        });
        return $query;
    }


    /**
     * @param $answer_id
     * @param $value_like
     * @param bool $update
     * @return bool
     */
    public function updateLike($answer_id, $value_like, $update = false)
    {
        $row = \Modules\Feedback\Models\Qtoa\QtoaAnswer::find($answer_id);
        if (!$row) {
            return false;
        }
        if ($value_like) {
            $old = (int)($row->liked);
            $old_dislike = (int)($row->dislike);
            $row->liked = $old + 1;
            if ($update) {
                $row->dislike = $old_dislike - 1;
            }
        } else {
            $old = (int)($row->dislike);
            $old_liked = (int)($row->liked);
            $row->dislike = $old + 1;
            if ($update) {
                $row->liked = $old_liked - 1;
            }
        }
        $row->save();
        return true;
    }

    /**
     * @param $value
     * @return null
     */
    public function getMediaAttribute($value)
    {
        if($value){
            return url('storage'.DIRECTORY_SEPARATOR.$value);
        }
        return null;
    }


    /**
     * @return null
     */
    public function getCreatedDateAttribute()
    {
        if ($this->created_at){
            return DateHelper::setDateTime($this->created_at)->getLocaleFormat(trans('core::date.datetime.medium'));
        }
        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function qtoaQuestion()
    {
        return $this->belongsTo(\Modules\Feedback\Models\Qtoa\QtoaQuestion::class, 'question_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function qtoaGrades()
    {
        return $this->hasMany(\Modules\Feedback\Models\Qtoa\QtoaGrade::class, 'answer_id');
    }
}
