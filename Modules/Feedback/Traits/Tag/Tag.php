<?php

namespace Modules\Feedback\Traits\Tag;


trait Tag
{

    /**
     * @return $this
     */
    public function messages()
    {
        $this->messages = [

        ];
        return $this;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByModel($query)
    {
        $query->where(function ($q){

            if(($model_type = $this->getRequestItem('model_type')) && ($model_id = $this->getRequestItem('model_id'))){
                $q->where('model_type', $model_type);
                $q->where('model_id', $model_id);
            }

        });
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFilterByUser($query)
    {
        $query->where(function ($q){

            if($user_id = $this->getRequestItem('user_id')){
                $q->where('user_id', $user_id);
            }

        });
        return $query;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Models\User\User');
    }

}
