<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("feedback::qtoa.".($question?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>
{{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form'
                        ,'url'=>(isset($question) && $question? route('feedback.qtoa.question'.(isset($append_route)?$append_route:'').'.update', ['course_id'=>$question->id]) : route('feedback.qtoa.question'.(isset($append_route)?$append_route:'').'.save'))
                        ,'method'=>(isset($question) && $question?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                @if($view_model->getModelId())

                    <input type="hidden" name="model_type" value="{!! $view_model->getDefaultModelType() !!}">
                    <input type="hidden" name="model_id" value="{!! $view_model->getModelId() !!}">

                @endif

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("feedback::qtoa.tabs.public") </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::input('text','question',old('question',$question?$question->question:null)
                                        ,['required'=>'required','title'=>trans("feedback::qtoa.question"),'helper'=>trans("feedback::qtoa.question")]) !!}

                                    @php

                                        $disabled = '0';
                                        $default=null;
                                        if($view_model->getModelId()){
                                            $disabled = 'disabled';
                                        }

                                    @endphp

                                    {!! FormHelper::select('model_type',$view_model->getModelTypes() ,old('model_type',$question?$question->model_type:$view_model->getDefaultModelType())
                                    ,[$disabled=>$disabled,'title'=>trans("feedback::qtoa.model_type"),'helper'=>trans("feedback::qtoa.model_type")
                                    ,'placeholder'=>trans("feedback::qtoa.model_type"), 'id'=>'_model_type_search'],$view_model->getModelTypesAttrs()) !!}

                                    @foreach($view_model->getModelTypes() as $key => $item)
                                        @php

                                        $is_active = false;
                                        $disabled = 'disabled';
                                        $default=null;
                                        if ($question && $question->model_type == $key){
                                            $is_active = true;
                                            $default = old('model_id',$question->model_id);
                                            if($view_model->getModelId()){
                                                $disabled = 'disabled';
                                            }else{
                                                $disabled = '0';
                                            }
                                        }elseif($key == $view_model->getDefaultModelType()){
                                            $is_active = true;
                                            $default = old('model_id',$view_model->getModelId());
                                            if($view_model->getModelId()){
                                                $disabled = 'disabled';
                                            }else{
                                                $disabled = '0';
                                            }
                                        }

                                        @endphp
                                    <div style="display: <?=$is_active?'':'none'?>" id="<?='_model_'.$key?>" class="_model_types">

                                        {!! FormHelper::selectTag('model_id',[],$default,[$disabled=>$disabled,'data-ajax-url'=>route('feedback.qtoa.search',['model_type'=>$key])
                                             , 'title'=>trans("feedback::qtoa.model_id"),'helper'=>trans("feedback::qtoa.model_id"), 'id'=>'_model_search_'.$key]) !!}

                                    </div>

                                    @endforeach


                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancelWithLanguage(['title'=>trans("feedback::qtoa.submit")], ['title'=>trans("feedback::qtoa.cancel"), 'url'=>isset($url_back)?$url_back:route('feedback.qtoa.question.index')], ['selected'=>$question?$question->language_id:null]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>
