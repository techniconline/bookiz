<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("feedback::qtoa.answer_field.".($answer?'edit':'add'))
                </div>
            </div>
            {{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">

                @if(isset($question) && $question)
                    {!! FormHelper::open(['role'=>'form','url'=>route('feedback.qtoa.question.answer'.(isset($append_route)?$append_route:'').'.save',['question_id'=>$question->question_id]) ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                    <input type="hidden" class="fa fa-plus-circle" name="question_id" value="{!! $question->id !!}">

                    @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">

                                <div class="tab-pane active" id="public">

                                    @if(isset($answer) && $answer)
                                        {!! FormHelper::textarea('answer_user',$answer->answer ,['disabled'=>'disabled','title'=>trans("feedback::qtoa.answer_user"),'helper'=>trans("feedback::qtoa.answer_user")]) !!}
                                    @endif

                                    {!! FormHelper::input('text','question',old('question',isset($question->question)?$question->question:null)
                                        ,['disabled'=>'disabled','title'=>trans("feedback::qtoa.question"),'helper'=>trans("feedback::qtoa.question")]) !!}

                                    {!! FormHelper::select('model_type',$view_model->getQuestionViewModel()->getModelTypes() ,old('model_type',isset($question->model_type)?$question->model_type:'')
                                    ,['disabled'=>'disabled', 'title'=>trans("feedback::qtoa.model_type"),'helper'=>trans("feedback::qtoa.model_type")
                                    ,'placeholder'=>trans("feedback::qtoa.model_type"), 'id'=>'_model_type_search'],$view_model->getQuestionViewModel()->getModelTypesAttrs()) !!}

                                    {!! FormHelper::selectTag('model_id',[],isset($question->model_id)?$question->model_id:0
                                        ,['disabled'=>'disabled','data-ajax-url'=>route('feedback.qtoa.search',['model_type'=>isset($question->model_type)?$question->model_type:''])
                                     , 'title'=>trans("feedback::qtoa.model_id"),'helper'=>trans("feedback::qtoa.model_id"), 'id'=>'_model_search_'.isset($question->model_type)?$question->model_type:'']) !!}


                                    {!! FormHelper::textarea('answer',old('answer',null) ,['title'=>trans("feedback::qtoa.answer_new"),'helper'=>trans("feedback::qtoa.answer_new")]) !!}


                                </div>
                            </div>
                        </div>
                    </div>


                    {!! FormHelper::openAction() !!}
                    {!! FormHelper::submitByCancel(['title'=>trans("feedback::qtoa.submit")], ['title'=>trans("feedback::qtoa.cancel"), 'url'=>route('feedback.qtoa.question.answer.index',['question_id'=>$question->id])]) !!}
                    {!! FormHelper::closeAction() !!}
                    {!! FormHelper::close() !!}
                @endif

            </div>


        </div>

    </div>

</div>
