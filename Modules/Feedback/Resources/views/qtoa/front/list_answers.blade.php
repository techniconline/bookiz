@if(isset($list) && $list)
    <div class="_rows">
        @foreach($list as $index => $answer)
            @php $model_type = $answer->getTable() @endphp
            <div class="_row_answer floatright col-xl-12 col-lg-12 col-md-12 NoPadd">
                <div class="floatright col-xl-2 col-lg-2 col-md-2 col-12 _staring11  ">



						  <span class="_nameBox">
                                   
                        <img alt="{{ $answer->user?$answer->user->full_name:null }}"
                             class="avatar_user"
                             src="{!! $answer->user?$answer->user->small_avatar_url:null !!}"
                                {{--src="http://www.farayad.local/storage/user/3f/49/3f49044c1469c6990a665f46ec6c0a41/3f49044c1469c6990a665f46ec6c0a41_avatar_thumbnail.jpg?v=1543390383"--}}
                        />    <br/>
                              {!! $answer->user?$answer->user->full_name:'-' !!}
                              <br/>
                              {!! $answer->created_date !!}
                                </span>


                </div>

                <div class="floatright col-xl-10 col-lg-10 col-md-10 col-12 _staring11 qta_01">
                    <h6>
                        {!! $answer->answer !!}
                    </h6>
                    @if($answer->media)
                        <a href="{!! $answer->media !!}" class="btn btn-primary"><i class="fa fa-download"></i></a>
                        <audio controls>
                            {{--<source src="horse.ogg" type="audio/ogg">--}}
                            <source src="{!! $answer->media !!}" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    @endif


                    <div class="floatleft">

                        <div class="flotleft box_course3  ">
                            <a href="{!! route('feedback.like.storeLike', ['model_type'=>$model_type, 'id'=>$answer->id, 'value_like'=>0]) !!}"
                               class="_like">


                                <i class="fas fa-thumbs-up fa-flip-vertical fa-flip-horizontal bc_file4"></i>
                                <span class="redme">  {!! $answer->dislike !!}  </span>
                            </a>
                        </div>
                        <div class="flotleft box_course3  ">
                            <a href="{!! route('feedback.like.storeLike', ['model_type'=>$model_type, 'id'=>$answer->id, 'value_like'=>1]) !!}"
                               class="_dislike">
                                <i class="fas fa-thumbs-up bc_file5"></i>
                                <span>  {!! $answer->liked !!} </span>
                            </a>

                        </div>
                    </div>


                </div>

            </div>
        @endforeach
    </div>
    <div class="pagination_answer">{!! $list->links() !!}</div>
@endif