@if($questions = $viewModel->getDataQtoa())
	

    <ul id="list_question" class="_row_question" data-confirm-text="@lang('feedback::qtoa.confirm_like')">
        @foreach($questions as $index => $question)

            @php $model_type = $question->getTable() @endphp
            <li class="  _ansW1">
                <div class="floatright col-xl-2 col-lg-2 col-md-2 col-12 _staring11">
                   
						
						
					  <span class="_nameBox">
                                   
                        <img alt="{{ $question->user?$question->user->full_name:null }}"
                             class="avatar_user"
                             src="{!! $question->user?$question->user->small_avatar_url:null !!}"
                                {{--src="http://www.farayad.local/storage/user/3f/49/3f49044c1469c6990a665f46ec6c0a41/3f49044c1469c6990a665f46ec6c0a41_avatar_thumbnail.jpg?v=1543390383"--}}
                        />    <br/>
                          {!! $question->user?$question->user->full_name:'-' !!}
                          <br/>
                          {!! $question->created_date !!}
                                </span>
								
								
								
                </div>

                <div class="floatright col-xl-10 col-lg-10 col-md-10 col-12 _staring11 _question _ansW2">

                    <div class="flotleft box_course3 box_course7">
                        <i class="fas fa-comment bc_file4 box_course36 "></i>

                    </div>

                    <p>{!! $question->question !!}</p>


                    <div class="floatleft">


                        <div class="flotleft box_course3 box_course34 ">
                            <a href="{!! route('feedback.like.storeLike', ['model_type'=>$model_type, 'id'=>$question->id, 'value_like'=>0]) !!}"
                               class="_dislike">
                                <i class="fas fa-thumbs-up bc_file5"></i>
                                <span>{!! $question->liked !!}</span>
                            </a>

                        </div>
                        <div class="flotleft box_course3 box_course34 ">
                            <a href="{!! route('feedback.like.storeLike', ['model_type'=>$model_type, 'id'=>$question->id, 'value_like'=>1]) !!}"
                               class="_like">


                                <i class="fas fa-thumbs-up fa-flip-vertical fa-flip-horizontal bc_file4"></i>
                                <span class="redme">    {!! $question->dislike !!}   </span>
                            </a>
                        </div>








					 </div>
                </div>
                <div class="_staring11 _question _ansW2">
                    <div class="floatright_important martg10 box_course34">
                        <a class="btn btn-success2 btnL_newQues noBg  _answer_user_active"><i
                                    class="far fa-comment"></i> @lang('feedback::qtoa.answer_to_question')</a>
                        @if($viewModel->hasAccess('teacher'))
                        <a class="btn btn-success2 btnL_newQues noBg  _voiceanswer"> <i
                                    class="fas fa-microphone"></i> @lang('feedback::qtoa.voiceanswer')</a>
                        @endif

                        @if($question->answers->count())
                            <a class="btn btn-success2 btnL_newQues noBg _answer_list_active"> <i
                                        class="fas fa-list-ul"></i> @lang('feedback::qtoa.answer_list')</a>
                        @endif


                        <div class="_answer_user_form" style="display: none">
                            {!! BridgeHelper::getQtoa()->getObject()->getFormAnswer($question->id) !!}
                        </div>

                        <div class="_answers" style="display: none{{--{!! !$index?'block':'none' !!}--}}">
                            @include('feedback::qtoa.front.list_answers', ['list'=>$question->answers])
                        </div>


                    </div>

                </div>

            </li>
        @endforeach
    </ul>
	
	
	
	
@endif





<div class="pagination_question">{!! $questions->links() !!}</div>