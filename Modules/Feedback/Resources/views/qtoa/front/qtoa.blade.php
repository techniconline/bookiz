<div class="col-xl-12 col-lg-12 col-md-12  floatright    box_course2" id="Qtoa_system">


    <div class="col-xl-12 col-lg-12 col-md-12  floatright  ">

        @if($viewModel->hasAccess('teacher'))
            <a href="{!! route('feedback.qtoa.question.answer.manage.get_list_answers',['model_type'=>$viewModel->model_name,'model_id'=>$viewModel->model_id]) !!}"
               class="btn btn-primary btn-sm">
                <i class="fas fa-comments"></i> <span>@lang('feedback::qtoa.manage_answer') </span>
            </a>
            <a href="{!! route('feedback.qtoa.question.manage.get_list_questions',['model_type'=>$viewModel->model_name,'model_id'=>$viewModel->model_id]) !!}"
               class="btn btn-warning btn-sm">
                <i class="fas fa-comment-alt"></i>
                <span> @lang('feedback::qtoa.manage_question')</span>
            </a>
        @endif

        <div class="floatright box_course3">

            <i class="fas fa-comment bc_file2 "></i>
            <h6>  @lang('feedback::qtoa.qtoa_title') </h6>
        </div>
    </div>


</div>


<div class="  QTA smooth_link">


    <br> <a class="btn btn-success2 btn-sm QTA_a _add_question ">@lang('feedback::qtoa.add')</a>
    <!-- Nav pills -->
    <ul class="nav nav-pills" role="tablist">
        <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#menu2"> @lang('feedback::qtoa.my_questions') </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#menu1">@lang('feedback::qtoa.hot_questions') </a>
        </li>

        <li class="nav-item">
            <a class="nav-link active" data-toggle="pill" href="#menu0">@lang('feedback::qtoa.new_questions')</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div id="menu0" class="container tab-pane active"><br>
            <div class="floatright   box_course4">

                {!! BridgeHelper::getQtoa()->getQtoaObject($viewModel->model_name, $viewModel->model_id,  ['with_form_qtoa'=>true, 'with_data_questions'=>true,"container_js" =>"block_js","container_css" => "block_css" ])->getForm() !!}

            </div>
        </div>
        <div id="menu1" class="container tab-pane fade"><br>
            <div class="floatright   box_course4">

                {!! BridgeHelper::getQtoa()->getQtoaObject($viewModel->model_name, $viewModel->model_id,  ['hot_questions'=>true, 'with_form_qtoa'=>true, 'with_data_questions'=>true,"container_js" =>"block_js","container_css" => "block_css" ])->getForm() !!}


            </div>
        </div>
        <div id="menu2" class="container tab-pane fade"><br>
            <div class="floatright  box_course4">

                {!! BridgeHelper::getQtoa()->getQtoaObject($viewModel->model_name, $viewModel->model_id,  ['my_questions'=>true, 'with_form_qtoa'=>true, 'with_data_questions'=>true,"container_js" =>"block_js","container_css" => "block_css" ])->getForm() !!}


            </div>
        </div>
    </div>
</div>