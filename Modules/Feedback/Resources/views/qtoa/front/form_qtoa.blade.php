{!! $viewModel->get() !!}
<div id="form_qtoa" style="display: none">

    {!! FormHelper::open(['role'=>'form','url'=> route('feedback.qtoa.question.save') ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
    <input type="hidden" name="model_type" value="{!! $viewModel->model_name !!}">
    <input type="hidden" name="model_id" value="{!! $viewModel->model_id !!}">

    @php  FormHelper::setCustomAttribute('label','class','col-md-12')->setCustomAttribute('element','class','col-md-12') @endphp

    {!! FormHelper::legend(trans("feedback::qtoa.question")) !!}


        <div class="col-md-12" id="NewQuestion">
            {!! FormHelper::textarea('question',old('question',null) ,['title'=>trans("feedback::qtoa.question"),'helper'=>trans("feedback::qtoa.question")]) !!}
        </div>


    {!! FormHelper::openAction() !!}
    {!! FormHelper::submitOnly(['title'=>trans("feedback::comment.submit"),'data-confirm-message'=>trans("feedback::comment.confirm")]) !!}
    {!! FormHelper::closeAction() !!}
    {!! FormHelper::close() !!}


</div>
