<div class="form_qtoa_answer">
    {!! FormHelper::open(['role'=>'form','url'=>route('feedback.qtoa.question.answer.save') ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
    <input type="hidden" name="question_id" value="{!! $question_id !!}">

    @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-12 mrgbtn10') @endphp

    {{--    {!! FormHelper::legend(trans("feedback::qtoa.answer")) !!}--}}

    {!! FormHelper::textarea('answer',old('answer',null) ,['id'=>'answer_'.rand(1111,9999),'title'=>trans("feedback::qtoa.answer"),'helper'=>trans("feedback::qtoa.answer")]) !!}

    {!! FormHelper::openAction() !!}
    {!! FormHelper::submitOnly(['title'=>trans("feedback::qtoa.submit"),'data-confirm-message'=>trans("feedback::qtoa.confirm")]) !!}
    {!! FormHelper::closeAction() !!}
    {!! FormHelper::close() !!}

    @if($viewModel->hasAccess('teacher'))
    <div class="_voice_recorder">

        <div class="_audio">

            <div class="quran-info-box quran-info-box___Left animated fadeInLeft">
                <div class="quran-info-base">
                    <p class="small-size">
                     
						{!! trans("feedback::qtoa.text_recorder") !!}
                    </p>
                    <p class="small-size">
                
							{!! trans("feedback::qtoa.text_player") !!}
                    </p>
                    <div class="info-overlay ">
                        <a class="btn btn-danger btn-record"><i class="fa fa-microphone"></i></a>

                        <a class="btn btn-success btn-record-play"><i class="fa fa-play"></i></a>

                        <a data-confirm-message="{!! trans("feedback::qtoa.confirm") !!}"
                           href="{!! route('feedback.qtoa.question.answer.voice.save',['question_id'=>$question_id]) !!}"
                           class="btn btn-success btn-save-record">
						   <i  class="fa fa-save"></i> {!! trans("feedback::comment.submit") !!}</a>
                    </div>
                </div>
            </div>
            <div class="hidden">
                <audio controls="controls" id="verse_player">
                    <source src="" type="audio/mpeg"/>
                    این مرورگر پخش فایل صوتی را پشتیبانی نمی کند.
                </audio>
            </div>
            <div class="hidden">
                <audio controls src="" id="verse-record"></audio>
            </div>


        </div>

    </div>
    @endif

</div>