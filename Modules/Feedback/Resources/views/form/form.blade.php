<div class="row" id="form_feature">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> @lang("feedback::form.".($form?'edit':'add'))
                </div>
                {{--<div class="tools">--}}
                {{--<a href="" class="collapse"> </a>--}}
                {{--<a href="#portlet-config" data-toggle="modal" class="config"> </a>--}}
                {{--<a href="" class="reload"> </a>--}}
                {{--<a href="" class="remove"> </a>--}}
                {{--</div>--}}
            </div>
{{--{!! dd($errors) !!}--}}
            <div class="portlet-body form">


                {!! FormHelper::open(['role'=>'form','url'=>(isset($form) && $form? route('feedback.form.update', ['form_id'=>$form->id]) : route('feedback.form.storeForm'))
                        ,'method'=>(isset($form) && $form?'PUT':'POST'),'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable-line boxless tabbable-reversed">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#public" data-toggle="tab">  @lang("feedback::form.tabs.public") </a>
                                </li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="public">

                                    {!! FormHelper::input('text','name',old('name',$form?$form->name:null)
                                        ,['required'=>'required','title'=>trans("feedback::form.title"),'helper'=>trans("feedback::form.title")]) !!}

                                    {!! FormHelper::input('text','alias',old('alias',$form?$form->alias:null)
                                        ,['required'=>'required','title'=>trans("feedback::form.alias"),'helper'=>trans("feedback::form.alias")]) !!}

                                    {!! FormHelper::editor('description',old('description',$form?$form->description:null)
                                    ,['title'=>trans("feedback::form.description"),'helper'=>trans("feedback::form.description")]) !!}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("feedback::form.submit")], ['title'=>trans("feedback::form.cancel"), 'url'=>route('feedback.form.index')]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>
