<div class="head-image parallax  BG2"></div>

<div class="page-line parallax">
    <div class="container">

        <div class="page-title">
            {!! $form->name !!}
        </div>
        <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                <div class="page-info">
                    <p>
                        {!! $form->description !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="" id="form_data" style="padding: 2em">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="portlet-body form">

                @partial('component.error')

                {!! FormHelper::open(['role'=>'form','url'=> route('feedback.form.save', ['form_id'=>$form->id])
                        ,'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

                @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

                <div class="row">
                    <div class="col-md-12">

                        {!! FormHelper::input('text','user_name',old('user_name',$form_data?$form_data->user_name:null)
                            ,['required'=>'required','title'=>trans("feedback::form.data.user_name")]) !!}

                        {!! FormHelper::input('text','email',old('email',$form_data?$form_data->email:null)
                            ,['required'=>'required','title'=>trans("feedback::form.data.email")]) !!}

                        {!! FormHelper::input('text','mobile',old('mobile',$form_data?$form_data->mobile:null)
                            ,['required'=>'required','title'=>trans("feedback::form.data.mobile")]) !!}


                        {!! FormHelper::select('data[consultant]',$view_model->getConsultants(),old('data[consultant]',$form_data?$form_data->data->consultant:null)
                        ,['title'=>trans("feedback::form.data.data.consultant")
                        ,'placeholder'=>trans("feedback::form.data.data.select_consultant")]) !!}

                        {!! FormHelper::select('data[date]',$view_model->getDates(),old('data[date]',$form_data?$form_data->data->date:null)
                        ,['title'=>trans("feedback::form.data.data.date")
                        ,'placeholder'=>trans("feedback::form.data.data.select_date")]) !!}

                        {!! FormHelper::select('data[time]',$view_model->getTimes(),old('data[time]',$form_data?$form_data->data->time:null)
                        ,['title'=>trans("feedback::form.data.data.time")
                        ,'placeholder'=>trans("feedback::form.data.data.select_time")]) !!}

                        {!! FormHelper::select('data[price]',$view_model->getPrice(),old('data[price]',$form_data?$form_data->data->price:null)
                        ,['title'=>trans("feedback::form.data.data.price")
                        //,'placeholder'=>trans("feedback::form.data.data.select_price")
                        ]) !!}

                        {!! FormHelper::select('data[payment]',$view_model->getPayment(),old('data[payment]',$form_data?$form_data->data->payment:null)
                        ,['title'=>trans("feedback::form.data.data.payment")
                        //,'placeholder'=>trans("feedback::form.data.data.select_price")
                        ]) !!}


                        {!! FormHelper::editor('data[description]',old('data[description]',$form_data?$form_data->data->description:null)
                        ,['title'=>trans("feedback::form.data.data.description")]) !!}

                    </div>
                </div>


                {!! FormHelper::openAction() !!}
                {!! FormHelper::submitByCancel(['title'=>trans("feedback::form.submit"), 'class'=>'btn btn-success green _save'], ['title'=>trans("feedback::form.cancel"),'class'=>'btn btn-default gray', 'url'=>url('/')]) !!}
                {!! FormHelper::closeAction() !!}
                {!! FormHelper::close() !!}

            </div>


        </div>

    </div>

</div>
