<div id="form_comment">
    @if(isset($viewModel->by_action_form) && $viewModel->by_action_form)
        {!! FormHelper::open(['role'=>'form','url'=>route('feedback.comment.storeComment',["model"=>$viewModel->model_name, "model_id"=>$viewModel->model_id]),'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
    @endif
    @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

    {!! FormHelper::legend(trans("feedback::comment.show")) !!}

    {!! FormHelper::input('text','name',old('name',$comment?$comment->name:null) ,['required'=>'required','title'=>trans("feedback::comment.name"),'helper'=>trans("feedback::comment.name")]) !!}

    {!! FormHelper::input('text','email',old('email',$comment?$comment->email:null) ,['required'=>'required','title'=>trans("feedback::comment.email"),'helper'=>trans("feedback::comment.email")]) !!}

    {!! FormHelper::textarea('comment',old('comment',$comment?$comment->comment:null) ,['title'=>trans("feedback::comment.comment"),'helper'=>trans("feedback::comment.comment")]) !!}

        {!! FormHelper::openAction() !!}
    @if(isset($viewModel->by_action_form) && $viewModel->by_action_form)
        {!! FormHelper::submitOnly(['title'=>trans("feedback::comment.submit"),'data-confirm-message'=>trans("feedback::comment.confirm")]) !!}
    @endif
        {!! FormHelper::closeAction() !!}
        {!! FormHelper::buttonLink(['title'=>trans("feedback::comment.back"), 'url'=>route('feedback.comment.manage.index')]) !!}
        {!! FormHelper::close() !!}

</div>
