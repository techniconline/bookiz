<div class="form_rate{!! $viewModel->user_id?"":"0" !!}" data-confirm-message="{!! trans("feedback::rate.confirm") !!}">
    @if($viewModel->with_save_rate)
        {!! FormHelper::open(['role'=>'form','url'=>route('feedback.rate.storeRate',["model"=>$viewModel->model_name, "model_id"=>$viewModel->model_id]),'method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
        @php  FormHelper::setCustomAttribute('label','class','col-md-2')->setCustomAttribute('element','class','col-md-8') @endphp

    @endif

    @if($viewModel->with_text_rate)
        <span class="rate"> {!! trans("feedback::rate.rated_text",["total_rate"=> $viewModel->getTotalRate(), "count_rated"=> $viewModel->getCountRated()]) !!}</span>
    @endif

    <fieldset class="rating">
        @php $strRand = str_random(5) @endphp
        @for($i = 10; $i>0; $i--)
            <input type="checkbox" {!! $viewModel->with_save_rate?"":"disabled" !!}
            id="star{!! is_float($i/2)?((int)($i/2))."half":((int)($i/2)) !!}_{!! $strRand !!}"
                   {!! (floor($viewModel->getTotalRate() * 2) / 2)==($i/2)?'checked="checked"':null !!}
                   class="_rate" name="rate" value="{{$i/2}}"/>
            <label class="{!! is_float($i/2)?"half":"full" !!}"
                   for="star{!! is_float($i/2)?((int)($i/2))."half":((int)($i/2)) !!}_{!! $strRand !!}" title="{{$i/2}} stars"></label>
        @endfor
    </fieldset>

    @if($viewModel->with_save_rate)
        {!! FormHelper::close() !!}
    @endif
</div>
