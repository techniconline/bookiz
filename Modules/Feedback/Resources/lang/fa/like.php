<?php

return [
    'cancel' => 'انصراف',
    'submit' => 'ثبت',
    'add' => 'افزودن',
    'edit' => 'ویرایش',
    'confirm' => 'آیااطمینان دارید؟',
    'confirm_delete' => 'آیا از حذف اطمینان دارید؟',
    'user'=>'کاربر',
    'model_id'=>'شناسه مدل',
    'model_type'=>'مدل',
    'liked'=>'پسندیده',
    'dislike'=>'نپسندیده',
    'like_states'=>[
        0=>'نپسندیده',
        1=>'پسندیده',
    ]
];