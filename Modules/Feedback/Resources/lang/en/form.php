<?php

return [
    'tabs'=>[
        'public'=>'عمومی',
    ],
    'action' => 'عملیات',
    'add' => 'افزودن فرم',
    'edit' => 'ویرایش فرم',
    'delete' => 'حذف',
    'url' => 'آدرس',
    'cancel' => 'انصراف',
    'submit' => 'ثبت',
    'back' => 'برگشت',
    'status' => 'وضعیت',
    'email' => 'پست الکترونیک',
    'name' => 'نام',
    'description' => 'توضیح',
    'show' => 'نمایش',
    'create' => 'ایجاد فرم',
    'alias' => 'مشخصه',
    'list' => 'فرمها',
    'title' => 'عنوان',

    'request-of-counseling' => 'فرم مشاوره حضوری',
    'confirm' => 'آیااطمینان دارید؟',
    'confirm_delete' => 'آیا از حذف اطمینان دارید؟',

    'data'=>[
        'user_name'=>'نام و نام خانوادگی',
        'email'=>'پست الکترونیک',
        'mobile'=>'شماره تماس',
        'data'=>[
            'free'=>'رایگان',
            'unit'=>'ریال',
            'date'=>'تاریخ',
            'price'=>'هزینه(ریال)',
            'select_date'=>'انتخاب تاریخ',
            'select_price'=>'انتخاب هزینه',
            'time'=>'ساعت',
            'select_time'=>'انتخاب ساعت',
            'name_consultants'=>'نام مشاور',
            'description' => 'توضیح',
            'payment' => 'روش پرداخت',
            'online' => 'به صورت آنلاین',
            'select_consultant' => 'انتخاب مشاور',
            'consultant' => 'مشاور',
        ]
    ]

];