<?php

return [
    'cancel' => 'انصراف',
    'submit' => 'ثبت',
    'back' => 'برگشت',
    'status' => 'وضعیت',
    'email' => 'پست الکترونیک',
    'name' => 'نام',
    'show' => 'نمایش',
    'comment' => 'نظر',
    'list' => 'نظرات',
    'title' => 'درج نظر',
    'confirm' => 'آیااطمینان دارید؟',
    'confirm_delete' => 'آیا از حذف اطمینان دارید؟',

    'model_id'=>'شناسه مدل',
    'model_type'=>'مدل',
    'publish_states'=>[
        2=>'خیر',
        1=>'بله',
    ],
    'all_delete'=>'حذف دسته جمعی',
    'all_publish'=>'انتشار دسته جمعی',
    'all_unpublish'=>'عدم انتشار دسته جمعی',
    'model_types'=>[
        'courses'=>'دوره ها',
    ],
    'publish_change'=>'تغییر وضعیت انتشار',
    'published'=>'وضعیت انتشار',

];