<?php

return [
    'alert' => [
        'save_success'=>'عملیات ذخیره سازی با موفقیت انجام شد.',
        'update_success'=>'عملیات بروزرسانی سازی با موفقیت انجام شد.',
        'del_success'=>'عملیات حذف با موفقیت انجام شد.',
        'save_un_success'=>'عملیات ذخیره سازی با موفقیت انجام نشد.',
        'update_un_success'=>'عملیات بروزرسانی سازی با موفقیت انجام نشد.',
        'del_un_success'=>'عملیات حذف با موفقیت انجام نشد.',
        'mis_data' => 'اطلاعات ناقص است!',
        'has_value' => 'اطلاعات شما قبلا ذخیره شده است!',
        'mis_data_access' => 'اطلاعات دسترسی ناقص است!',
        'not_access' => 'دسترسی وجود ندارد!',
        'not_find_data' => 'اطلاعات یافت نشد!',
        'find_data' => 'اطلاعات یافت نشد!',
        'info' => 'پیام سیستم',
        'error' => 'خطا در سیستم!',
        'process_error' => 'خطا در پردازش رخ داده!',

        'uploadFileIsNotValid'=>'فایل ارسالی معتبر نیست!',
        'uploadFileNotFind'=>'فایل یافت نشد!',
        'uploadFileUnSuccess'=>'خطا در ذخیره سازی!',
        'uploadFileSuccess'=>'فایل با موفقیت ذخیره شد.',
        'maxFileUpload'=>'حداکثر فایل قابل ارسال :max_upload فایل می باشد.',

    ],
];