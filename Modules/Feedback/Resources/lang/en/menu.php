<?php

return [
    'group' => [
        'feedback'=>'مدیریت بازخوردها',
    ],
    'admin_qtoa'=>'مدیریت پرسش و پاسخ',
    'admin_comments'=>'مدیریت نظرات',
    'admin_rate'=>'مدیریت امتیاز دهی',
    'admin_tag'=>'مدیریت تگ ها',

];