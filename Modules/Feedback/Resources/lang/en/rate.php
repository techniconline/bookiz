<?php

return [
    'cancel' => 'انصراف',
    'submit' => 'ثبت',
    'status' => 'وضعیت',
    'email' => 'پست الکترونیک',
    'name' => 'نام',
    'reviews' => 'نظر',
    'rated_text' => '<span style="color: orangered">:total_rate</span> از 5 / <span style="color: orangered">:count_rated</span> رای',
    'title' => 'درج امتیاز',
    'confirm' => 'آیااطمینان دارید؟',


];