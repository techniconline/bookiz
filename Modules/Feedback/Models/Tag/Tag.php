<?php

namespace Modules\Feedback\Models\Tag;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Feedback\Traits\Tag\Tag as TraitModel;

class Tag extends Model
{

    use TraitModel;
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['model_id', 'model_type', 'title', 'user_id', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $messages = [];
    public $rules = [
        'title' => 'required',
        'model_type' => 'required',
        'model_id' => 'required',
        'user_id' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
