<?php

namespace Modules\Feedback\Models\Qtoa;


use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Feedback\Traits\Qtoa\QtoaGrade as TraitModel;

class QtoaGrade extends BaseModel
{

    use TraitModel;
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['question_id', 'answer_id', 'user_id', 'vote', 'views', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $api_fields = ['id', 'vote', 'views'];
    public $list_fields = ['id', 'vote', 'views'];
    public $api_append_fields = [];
    public $list_append_fields = [];

    public $messages = [];
    public $rules = [];

    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
