<?php

namespace Modules\Feedback\Models\FormCustom;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;

class FormCustomData extends BaseModel
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'form_custom_data';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['form_custom_id', 'user_name', 'email', 'mobile', 'data', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formCustom()
    {
        return $this->belongsTo(FormCustom::class);
    }
}
