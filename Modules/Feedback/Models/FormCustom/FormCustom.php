<?php

namespace Modules\Feedback\Models\FormCustom;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Core\Models\Instance\Instance;

class FormCustom extends BaseModel
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['instance_id', 'name', 'alias', 'description', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    const PRICE_OF_CONSULATE = 500000; //rial

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formCustomData()
    {
        return $this->hasMany(FormCustomData::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo(Instance::class);
    }

}
