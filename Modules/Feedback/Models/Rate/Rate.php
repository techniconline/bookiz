<?php

namespace Modules\Feedback\Models\Rate;


use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Feedback\Traits\Rate\Rate as TraitModel;

class Rate extends BaseModel
{
    use TraitModel;
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['model_id', 'model_type', 'rate', 'user_id', 'active', 'deleted_at', 'created_at', 'updated_at'];

    public $messages = [];
    public $rules = [
        'rate' => 'required',
        'model_type' => 'required',
        'model_id' => 'required',
        'user_id' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
