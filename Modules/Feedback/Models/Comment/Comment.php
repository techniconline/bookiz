<?php

namespace Modules\Feedback\Models\Comment;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Models\BaseModel;
use Modules\Feedback\Traits\Comment\Comment as TraitModel;

class Comment extends BaseModel
{

    use TraitModel;
    use SoftDeletes;
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['parent_id', 'model_id', 'model_type', 'name', 'email', 'comment', 'user_id', 'active', 'deleted_at', 'created_at', 'updated_at'];


    public $messages = [];
    public $rules = [
        'comment' => 'required',
        'model_type' => 'required',
        'model_id' => 'required',
        'name' => 'required',
    ];

    public $timestamps = true;
    protected $dates = ['deleted_at'];


}
