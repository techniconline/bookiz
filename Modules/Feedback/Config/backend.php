<?php

return [
    'version' => '20181024001',
    'blocks' => [
        'left' => [],
        'top' => []
    ],
    'menus' => [
        'management' => [],
        'management_admin' => [
            ['alias' => '', //**
                'route' => 'feedback.qtoa.question.index', //**
                'key_trans' => 'feedback::menu.admin_qtoa', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-question-circle', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_feedback',
            ],
            ['alias' => '', //**
                'route' => 'feedback.comment.manage.index', //**
                'key_trans' => 'feedback::menu.admin_comments', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-comments', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_feedback',
            ],
            ['alias' => '', //**
                'route' => '', //**
                'key_trans' => 'feedback::menu.admin_rate', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-heart-o', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_feedback',
            ],
            ['alias' => '', //**
                'route' => '', //**
                'key_trans' => 'feedback::menu.admin_tag', //**
                'order' => 1, //if null end menu and if zero (0) top menu
                'after' => '',
                'before' => '',
                'icon_class' => 'fa fa-tags', //fa-XXXXX
                'class' => '',
                'id' => '',

                'group_menu' => 'admin_feedback',
            ],
        ],

    ],
    'group_menu' => [
        'management_admin' => [
            ['alias' => 'admin_feedback', //**
                'key_trans' => 'feedback::menu.group.feedback', //**
                'icon_class' => 'fa fa-recycle',
                'before' => '',
                'after' => '',
                'order' => 10,
            ],
        ],
    ],

];
