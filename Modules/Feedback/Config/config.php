<?php

return [
    'name' => 'Feedback',
    'permissions'=>[
        'feedback.comment.save'=>true,
        'feedback.comment.storeComment'=>true,
        'feedback.comment.update'=>true,
        'feedback.comment.getComments'=>true,
        'feedback.comment.delete'=>true,
        'feedback.rate.save'=>true,
        'feedback.rate.storeRate'=>true,
        'feedback.rate.getRates'=>true,
        'feedback.rate.delete'=>true,
        'feedback.qtoa.question.index'=>['type' => '|', 'access' => ['core/is.super.admin','core/is.admin']],

    ],
];
