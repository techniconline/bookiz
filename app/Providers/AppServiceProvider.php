<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        if(env('LOG_QUERY_IN_FILE', false)){
            DB::listen(function ($query){
               Log::useDailyFiles(storage_path('/logs/sql_query.log'));
               Log::info('START ------------------'. date('Y-m-d H:i:s'));
               Log::info('QUERY: ', ['q'=>$query->sql, 'time'=> $query->time]);
               Log::info('END ------------------'. date('Y-m-d H:i:s'));
            });
            //        file_put_contents(storage_path('/logs/var/p.log'),serialize(['hidden'=>$this->getHidden(), 'table'=>$this->getTable()])."\r\n",FILE_APPEND);

        }

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('path.public', function() {
            return base_path(env('PUBLIC_FOLDER','public'));
        });
    }
}
