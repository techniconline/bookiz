<?php

namespace App\Http;

use App\Http\Middleware\Language;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        Language::class,
        \Barryvdh\Cors\HandleCors::class,
        \Modules\Core\Http\Middleware\CheckRequestUrl::class,
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
        \Modules\Core\Http\Middleware\Loader::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
//            \Modules\Core\Http\Middleware\CheckRequestUrl::class,
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \Modules\Core\Http\Middleware\ChangeLoader::class, //change current instance
//            \Modules\User\Http\Middleware\AuthJWT::class,
            \Modules\User\Http\Middleware\GuestToken::class, //change current instance
            \Modules\User\Http\Middleware\UserActivateCheck::class, //change current instance
            \Modules\Log\Http\Middleware\Logger::class, //logger
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
//        'checkRequestUrl' => \Modules\Core\Http\Middleware\CheckRequestUrl::class,
//        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth' => \Modules\User\Http\Middleware\Auth::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'loader' => \Modules\Core\Http\Middleware\Loader::class,
        'admin' => \Modules\User\Http\Middleware\IsAdminCheck::class, //change current instance

        'jwt-auth' => \Modules\User\Http\Middleware\AuthJWT::class

    ];
}
