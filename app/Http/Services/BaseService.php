<?php

namespace App\Http\Services;

use Closure;
use Illuminate\Support\Facades\Cache;

class BaseService
{

    protected $response;
    protected $view;

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function response()
    {
        if ($this->view) {
            return $this->view;
        }

        if (array_key_exists('redirect', $this->response)) {
            return redirect($this->response['redirect']);
        }
        return $this->response;
    }

    /**
     * @return $this
     */
    public function setActionTrue()
    {
        $this->response['action'] = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function setActionFalse()
    {
        $this->response['action'] = false;
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->response['data'] = $data;
        return $this;
    }

    /**
     * @param $errors
     * @return $this
     */
    public function setErrors($errors)
    {
        $this->response['errors'] = $errors;
        return $this;
    }

    /**
     * @param $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->response['message'] = $message;
        return $this;
    }

    /**
     * @param null $response
     * @return $this
     */
    public function setResponse($response = null)
    {
        $this->response = $response ?: $this->response;
        if (!array_key_exists('_token', $this->response)) {
            $this->response['_token'] = csrf_token();
        }
        return $this;
    }

    /**
     * @param $view
     * @return $this
     */
    public function setView($view)
    {
        if (view()->exists($view)) {
            $this->view = view($view)->with('data', $this->response);
        }
        return $this;
    }

    /**
     * @param $key
     * @param Closure $whatToCache
     * @param null $duration
     * @return mixed
     */
    public function caching($key, Closure $whatToCache, $duration = null)
    {

        if (!env('CACHE_ENABLE', false)) {
            return $value = $whatToCache();
        }

        if (!Cache::has($key)) {
            $value = $whatToCache();
            if (!$value) {
                return $value;
            }

            if (!$duration) {
                Cache::forever($key, $value);
            } else {
                Cache::put($key, $value, $duration);
            }

        } else {
            $value = Cache::get($key);
        }
        return $value;
    }

}
