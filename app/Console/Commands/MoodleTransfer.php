<?php

namespace App\Console\Commands;

use App\Console\Commands\Moodle\UserMover;
use Illuminate\Console\Command;
use App\Console\Commands\Moodle\CourseMover;

class MoodleTransfer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moodle:transfer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Moodle Transfer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $courseMover=new CourseMover();
//        $courseMover->createTransferQueue();
//        $courseMover->moveCourses();

        //$userMover=new UserMover();
        //$userMover->createTransferQueue();
        //$userMover->moveUsers();
    }

}
