<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 10/16/18
 * Time: 11:30 AM
 */
namespace App\Console\Commands\Moodle;

use Modules\Video\Models\Video\Video as VideoModel;
use Modules\Video\Models\Video\VideoUse;

Trait Video
{
    public function addVideoRecord($videoRecord,$videoFile){
        $video=VideoModel::where('hash_key',$videoRecord->file_hash)->first();
        if(!$video){
            if($videoRecord->time < 0){
                $videoRecord->time=0;
            }
            $video=new VideoModel;
            $video->title=$videoRecord->file_name;
            $video->instance_id=$this->getInstance()->id;
            $video->language_id=$this->getLanguage()->id;
            $video->hash_key=$videoRecord->file_hash;
            if(strlen($videoRecord->file_poster)){
                $videoFile['has_poster']=true;
                $videoFile['poster']=$videoRecord->file_poster;
                $video->has_poster=true;
                $video->show_poster=true;
            }else{
                $videoFile['has_poster']=false;
                $video->has_poster=false;
                $video->show_poster=false;
            }
            $video->extension=$this->getVideoExt($videoRecord->file_name);
            $video->info=$this->getVideoInfo($videoRecord);
            $video->status_convert='finished';
            $video->video_time=(int)$videoRecord->time;
            $video->active=1;
            $video->save();
            $this->currentItem->files[]=$videoFile;
        }
        return $video;
    }



    public function addVideoUseRecord($newActivity,$videoModel,$videoFile){
        $videoUse=VideoUse::where('hash_key',$videoFile['use'])->where('video_id',$videoModel->id)->first();
        if(!$videoUse){
            $videoUse=new VideoUse;
            $videoUse->video_id=$videoModel->id;
            $videoUse->instance_id=$this->getInstance()->id;
            $videoUse->hash_key=$videoFile['use'];
            $videoUse->model='course_activities';
            $videoUse->model_id=$newActivity->id;
            $videoUse->options=json_encode(["vote"=>"1","comment"=>"1","random_questions"=>"0"]);
            $videoUse->save();
        }
        return $videoUse;
    }

    public function getVideoInfo($videoRecord){
        $data=unserialize($videoRecord->data);
        if(!$data){
            return null;
        }
        $data=(array)$data;
        $qualities=[];
        foreach($data as $quality=>$size){
           $qualities[$quality]=$size;
        }
        $ratio='16:9';
        if($videoRecord->width && $videoRecord->height){
            $ratio_height=number_format(($videoRecord->height / $videoRecord->width),2);
            $ratio='1:'.$ratio_height;
        }
        $info=array (
            'status' => 'finished',
            'info' =>
                array (
                    'width' => $videoRecord->width,
                    'height' => $videoRecord->height,
                    'ratio' => $ratio,
                    'duration' => $videoRecord->time,
                    'bitRate' => '0',
                    'qualities' =>$qualities,
                ),
            'converted_at' => '2018-10-18 00:00:00',
            'updated_at' => '2018-10-18 00:00:00',
            'created_at' => '2018-10-18 00:00:00',
        );
        return json_encode($info);
    }
    public function getVideoExt($str){
        $ext=substr($str,strrpos($str,'.')+1);
        if(strlen($ext)){
            return $ext;
        }
        return 'mp4';
    }



}