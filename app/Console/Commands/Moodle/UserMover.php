<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 10/10/18
 * Time: 2:54 PM
 */

namespace App\Console\Commands\Moodle;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use DateHelper;
use Modules\Core\Models\Currency\Currency;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Enrollment;
use Modules\Course\Models\Enrollment\CourseUserEnrollment;
use Modules\Sale\Models\Order;
use Modules\Sale\Models\Order\OrderItems;
use Modules\Sale\Models\Payment;
use Modules\User\Models\Register\UserRegisterKey;
use Modules\User\Models\User;

class UserMover extends Master
{

    private $enrllsWays = false;
    private $currencies = false;

    private $currentItem = false;

    public function moveUsers()
    {
        try {
            $transfersQueues = $this->getUserQueue();
            if ($transfersQueues) {
                foreach ($transfersQueues as $transfersItem) {
                    $this->currentItem = $this->getTransferObject($transfersItem);
                    //if ($transfersItem->last_id == 73561) {
                    $this->makeNewUser();
                    // }
                }
            }
        } catch (\Exception $e) {
            report($e);
            return false;
        }
    }

    public function createTransferQueue()
    {
        $users = $this->getMoodleDb()->select("SELECT * FROM `mdl_user` WHERE `verify_type` > 0  AND `confirmed`=1  AND deleted='0' 
ORDER BY `mdl_user`.`id` ASC");
        foreach ($users as $user) {
            $data = [
                'type' => 'user',
                'last_id' => $user->id,
                'status' => 'start',
            ];
            $result = $this->addToTransferTable($data);
            if ($result) {
                echo 'Success: user (' . $user->id . ') add to transfer table.' . "\r\n";
            } else {
                echo 'Error: user (' . $user->id . ') can not add to transfer table.' . "\r\n";
            }
        }
    }


    public function makeNewUser()
    {
        $moodleUser = $this->getFirst($this->getMoodleDb()->select('SELECT * FROM `mdl_user` WHERE  `id` = ' . $this->currentItem->last_id));
        if ($moodleUser) {
            /*if ($this->currentItem->status == 'start' && !$this->currentItem->new_id) {
                $this->addLog($this->currentItem, 'success', 'Find Moodle User.');
                $result = $this->createUserOnFarayad($moodleUser);
            } else {
                $result = true;
            }

            if ($this->currentItem->status == 'create') {
                $this->uploadUserFileAndTeacher($moodleUser);
            }

            if ($this->currentItem->status == 'section') {
                $this->UserEnrollments($moodleUser);
            }
            if ($this->currentItem->status == 'file') {
                echo "Complete:" . $this->currentItem->new_id ."\r\n";
            }else{
                echo "Not Complete:" . $this->currentItem->last_id ."\r\n";
            }*/
            if ($moodleUser->picture != 0 && isset($this->currentItem->data['file']) && !$this->currentItem->data['file']['saved']) {
                echo "start id: ".$moodleUser->id."\r\n";
                $this->uploadUserFileAndTeacher($moodleUser);
            }
        } else {
            $this->addLog($this->currentItem, 'error', 'Not Find Moodle User.');
        }
    }


    public function createUserOnFarayad($moodleUser)
    {
        if ($moodleUser->verify_type == 2) {
            $value = $moodleUser->mobile;
        }else{
            $value = $moodleUser->email;
        }
        if(empty($value)){
            $this->saveLog('user can not add connector: '.$moodleUser->id);
            return false;
        }
        DB::beginTransaction();
        try {
            $userConnector=UserRegisterKey::where('value',$value)->first();
            if($userConnector){
                $user=User::find($userConnector->user_id);
            }else{
                $user = new User;
            }
            /*add user*/
            $instance_id = $this->getInstance()->id;
            $user->instance_id = $instance_id;
            $user->username = $moodleUser->username;
            $user->password = $moodleUser->password;
            $user->first_name = $moodleUser->firstname;
            $user->last_name = $moodleUser->lastname;
            $user->confirm = 1;
            if (strlen($moodleUser->national) && is_numeric($moodleUser->national)) {
                $user->national_code = $moodleUser->national;
            }
            if ($moodleUser->national == 'amozesh') {
                $user->instance_id = 5;
            }
            $user->save();

            $user->addDefaultMeta();

            /*add connector*/
            $canMobile=false;
            if($moodleUser->verify_type==3){
                $canMobile=true;
                $moodleUser->verify_type=1;
            }
            if(!$userConnector) {
                $userConnector = new UserRegisterKey;
            }
            $userConnector->user_id = $user->id;
            $userConnector->connector_id = $moodleUser->verify_type;
            $userConnector->value = $value;
            $userConnector->status = 'active';
            $userConnector->token = null;
            $userConnector->hash = null;
            $userConnector->save();
            if($canMobile){
                $value = $moodleUser->mobile;
                $userConnector=UserRegisterKey::where('value',$value)->first();
                if(!$userConnector) {
                    $userConnector = new UserRegisterKey;
                }
                $userConnector->user_id = $user->id;
                $userConnector->connector_id = 2;
                $userConnector->value = $value;
                $userConnector->status = 'active';
                $userConnector->token = null;
                $userConnector->hash = null;
                $userConnector->save();
            }
            /*add default Role*/
            if ($moodleUser->picture != 0) {
                $this->currentItem->data['file'] = ['has' => true, 'saved' => false];
            }
            $this->currentItem->new_id = $user->id;
            $this->currentItem->status = 'create';
            $this->addLog($this->currentItem, 'success', 'add user success.');
            $this->updateTransferItem($this->currentItem);

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            $this->addLog($this->currentItem, 'error', 'add user failed.');
            echo $e->getMessage() . "\r\n";
            return false;
        }
    }


    public function uploadUserFileAndTeacher($moodleUser)
    {
        /*picture save*/
        if ($moodleUser->picture != 0 && isset($this->currentItem->data['file']) && !$this->currentItem->data['file']['saved']) {
            try {
                $url = $this->getUrlData($moodleUser->id);
                if($url){
                    $ext = $this->getExtension($url);
                    if($ext){
                        $path = $this->getImageNewPath($moodleUser->id);
                        $file = md5('user_' . $moodleUser->id);
                        $contents = file_get_contents($url);
                        $address = $path . $file . '_avatar.' . $ext;
                        $address2 = $path . $file . '_avatar_thumbnail.' . $ext;
                        Storage::disk('minio')->put($address, $contents);
                        Storage::disk('minio')->put($address2, $contents);
                        $this->addLog($this->currentItem, 'success', 'add user picture success.');
                        echo "Success: ".$moodleUser->id."\r\n";
                    }else{
                        echo "Not Find:".$moodleUser->id;
                    }

                }

            } catch (\Exception $e) {
                echo $e->getMessage()."\r\n";
                $this->addLog($this->currentItem, 'error', 'add user picture failed.');
                report($e);
            }
        }
        /**/
        /*try {
            $is_saved=Teacher::where('user_id',$this->currentItem->new_id)->first();
            if (!$is_saved) {
                $isTeacher = $this->getFirst($this->getMoodleDb()->select("SELECT * FROM `mdl_role_assignments`where `userid`='" . $moodleUser->id . "' AND roleid IN (4,3) limit 0,1"));
                if ($isTeacher) {
                    $data = $this->getFirst($this->getWordpressDb()->select("SELECT *  FROM `mdl_usermeta` WHERE `meta_key`='description' AND CHAR_LENGTH(`meta_value`)>1"));

                    if ($data && !$is_saved) {
                        $teacher = new Teacher;
                        $teacher->education = 3;
                        $teacher->field_study = '-';
                        $teacher->location_study = '-';
                        $teacher->user_id = $this->currentItem->new_id;
                        $teacher->description = $data->meta_value;
                        $teacher->active = 1;
                        $teacher->svae();
                        $this->addLog($this->currentItem, 'success', 'add user teachersuccess.');
                    }
                }
            }
        } catch (\Exception $e) {
            $this->addLog($this->currentItem, 'error', 'add user teacher failed.');
            report($e);
        }*/
      //  $this->currentItem->status = 'section';
       // $this->updateTransferItem($this->currentItem);
    }


    public function UserEnrollments($moodleUser)
    {
        $sql = "SELECT `mdl_user_enrolments`.`userid`,`mdl_enrol`.`enrol`,`mdl_enrol`.`courseid` FROM `mdl_user_enrolments` inner join `mdl_enrol` on `mdl_user_enrolments`.`enrolid`=`mdl_enrol`.`id` where `mdl_user_enrolments`.`userid`='" . $moodleUser->id . "' AND `mdl_enrol`.`courseid` IN (SELECT id FROM `mdl_course` WHERE `category` in (32,48) AND visible=1)";
        $enrollments = $this->getMoodleDb()->select($sql);
        if (count($enrollments)) {
            foreach ($enrollments as $enrollment) {
                $new_course = $this->getFirst($this->getMoodleDb()->select("SELECT * FROM `faryad_transfer` WHERE `type`='course' AND `last_id`='" . $enrollment->courseid . "'"));
                if ($new_course) {
                    try {
                        $this->enrollUserOnCourse($this->currentItem->new_id, $new_course, $enrollment);
                    } catch (\Exception $e) {
                        report($e);
                        $this->saveLog($e->getMessage());
                    }
                } else {
                    $this->addLog($this->currentItem, 'error', 'can not find Course #' . $enrollment->courseid . '.');
                }
            }
        }else{
            echo "no Enrollments. \r\n";
        }
        $this->addLog($this->currentItem, 'success', 'add user enrollemnt success.');
        $this->currentItem->status = 'file';
        $this->updateTransferItem($this->currentItem);
    }

    public function getExtension($url)
    {
        switch (exif_imagetype($url)) {
            case IMAGETYPE_JPEG:
                $extensions = 'jpg';
                break;
            case IMAGETYPE_PNG:
                $extensions = 'png';
                break;
            case IMAGETYPE_GIF:
                $extensions = 'gif';
                break;
            case IMAGETYPE_BMP:
                $extensions = 'bmp';
                break;
            //There is a lot of other image types... I use this 4 just for a example
            default :
                $extensions = null;
                break;
        }
        return $extensions;
    }

    public function getUrlData($id)
    {
        $url = 'https://learn.farayad.org/user/pic.php?id=' . $id;
        try {
            $ch = curl_init();
            $timeout = 15;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $data = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            if ($httpcode == 200) {
                $data = json_decode($data);
                return $data->file;
            }
            return false;
        } catch (Exception $e) {
            return false;
        }
    }


    public function getImageNewPath($id)
    {
        $avatarPath = '/user/';
        $hash = md5('user_' . $id);
        $f1 = substr($hash, 0, 2);
        $f2 = substr($hash, 2, 2);
        $userPath = $avatarPath.$f1 . DIRECTORY_SEPARATOR . $f2 . DIRECTORY_SEPARATOR . $hash . DIRECTORY_SEPARATOR;
        return $userPath;
    }

    public function getAvailableEnrollment()
    {
        if ($this->enrllsWays) {
            return $this->enrllsWays;
        }
        $Enrollments = Enrollment::all();
        $this->enrllsWays = [];
        foreach ($Enrollments as $enrollment) {
            if ($enrollment->alias == 'payment') {
                $this->enrllsWays['onlinepay'] = $enrollment;
            } elseif ($enrollment->alias == 'free') {
                $this->enrllsWays['self'] = $enrollment;
            } else {
                $this->enrllsWays['manual'] = $enrollment;
            }
        }
        return $this->enrllsWays;
    }


    public function getAvailableCurrencies()
    {
        if ($this->currencies) {
            return $this->currencies;
        }
        $currencies = Currency::all();
        $this->currencies = [];
        foreach ($currencies as $currency) {
            $this->currencies[$currency->code] = $currency;
        }
        return $this->currencies;
    }


    public function enrollUserOnCourse($user_id, $transferedCourse, $enrollment)
    {
        $course = Course::find($transferedCourse->new_id);
        if (!$course) {
            $this->saveLog('ERROR:No find Course: ' . $transferedCourse->last_id);
        }
        $allEnrolls = $this->getAvailableEnrollment();
        if ($enrollment->enrol == 'onlinepay') {
            $key = 'onlinepay';
            $enrollmentModel = $allEnrolls[$key];
            $this->makeOrder($user_id, $transferedCourse, $course, $enrollmentModel);
        } elseif ($enrollment->enrol == 'self') {
            $key = 'self';
        } else {
            $key = 'manual';
        }
        $enrollmentModel = $allEnrolls[$key];
        $time = time() + 25720000;
        $expireDate = DateHelper::setDateTime(date(trans('core::date.database.datetime'), $time), null, trans('core::date.database.datetime'))->getDateTime(trans('core::date.database.datetime'));
        $userEnroll = new CourseUserEnrollment;
        $userEnroll->user_id = $user_id;
        $userEnroll->course_id = $course->id;
        $userEnroll->enrollment_id = $enrollmentModel->id;
        $userEnroll->role_id = 2;
        $userEnroll->type = 'enrollment';
        $userEnroll->active_date = date(trans('core::date.database.datetime'), time());
        $userEnroll->expire_date = $expireDate;
        $userEnroll->active = 1;
        $this->saveLog("Success:Add user: " . $user_id . ' On Course: ' . $course->id);
        return $userEnroll->save();
    }


    public function makeOrder($user_id, $transferedCourse, $course, $enrollmentModel)
    {
        /*add payment and order*/
        $sql = "SELECT * FROM `mdl_enrol_onlinepay` WHERE `actionid`='" .$transferedCourse->last_id . "' AND `action`='course' AND `userid`='" . $this->currentItem->last_id . "' AND `transaction_state`='SUCCESS'";
        $payment = $this->getFirst($this->getMoodleDb()->select($sql));
        $currencies = $this->getAvailableCurrencies();
        if ($payment) {
            $instance = $this->getInstance();
            $reference_code = "INS";
            $reference_code .= $instance->id * 3;
            $reference_code .= "-" . strtoupper(str_random(10));
            $order = new Order;
            $order->instance_id = $instance->id;
            $order->reference_code = $reference_code;
            $order->user_id = $user_id;
            $order->state = 'complete';
            $order->amount = $payment->amount + $payment->discount_amount;
            $order->payable_amount = $payment->amount;
            $order->currency_id = $currencies['IRT']->id;
            $order->save();
            $item = new OrderItems;
            $item->order_id = $order->id;
            $item->item_type = 'course';
            $item->item_id = $course->id;
            $item->name = $course->title;
            $item->options = ["enrollment_id" => $enrollmentModel->id, "product_key" => $course->code];
            $item->qty = 1;
            $item->amount = $payment->amount + $payment->discount_amount;
            $item->total_amount = $payment->amount + $payment->discount_amount;
            $item->currency_id = $currencies['IRT']->id;
            $item->save();

            $options = ["payment" => "zarinpal", "reference_code" => $order->reference_code];
            $payment = new Payment;
            $payment->instance_id = $order->instance_id;
            $payment->user_id = $order->user_id;
            $payment->action_id = $order->id;
            $payment->action = 'order';
            $payment->amount = $order->payable_amount;
            $payment->currency_id = $order->currency_id;
            $payment->type = 'online';
            $payment->options = $options;
            $payment->request_id=$payment->request_number;
            $payment->reference_id=$payment->reference_number;
            $payment->description = trans('sale::sale.payment_details', ['id' => $order->id]);
            $payment->state = 'complete';
            $payment->save();
            $this->saveLog("Success:Add order: " . $order->id . ' On payment: ' . $payment->id);
            return true;
        } else {
            $this->saveLog('ERROR:No find payment for user: ' . $user_id . ' And Course: ' . $course->id);
        }
        return true;
    }


    public function saveLog($message, $newLine = true)
    {
        $address = storage_path('logs/transfers-users.log');
        if ($newLine) {
            $message = $message . "\r\n";
        }
        file_put_contents($address, $message, FILE_APPEND);
    }
}