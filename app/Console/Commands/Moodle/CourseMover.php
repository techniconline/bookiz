<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 10/10/18
 * Time: 2:54 PM
 */
namespace App\Console\Commands\Moodle;

use Illuminate\Support\Facades\DB;
use Modules\Course\Models\Course\Course;
use Modules\Course\Models\Course\CourseSection;
use Modules\Course\Models\Course\CourseActivity;
use Modules\Course\Models\Course\CourseCategoryRelation;
use Modules\Course\Models\Enrollment;
use Modules\Course\Models\Enrollment\CourseEnrollment;
use Modules\Core\Models\Currency\Currency;

class CourseMover extends Master
{
    use TextAnalyzor, Video;

    private $enrllsWays=false;
    private $currencies=false;

    private $currentItem = false;

    public function makeNewCourse()
    {
        $moodleCourse = $this->getFirst($this->getMoodleDb()->select('SELECT * FROM `mdl_course` WHERE  `id` = ' . $this->currentItem->last_id));
        $wordpressCourse = $this->getFirst($this->getWordpressDb()->select("SELECT mdl_posts.* FROM `mdl_moodle_sync` inner join mdl_posts on `mdl_moodle_sync`.`wordpress_id`=mdl_posts.ID WHERE `mdl_moodle_sync`.`moodle_id`='" . $this->currentItem->last_id . "' and `moodle_type`='course'"));
        if ($moodleCourse && $wordpressCourse) {
            if ($this->currentItem->status == 'start' && !$this->currentItem->new_id) {
                $this->addLog($this->currentItem, 'success', 'Find Moodle Course and Wordpress Course.');
                $result = $this->createCourseOnFarayad($moodleCourse, $wordpressCourse);
            } else {
                if($this->currentItem->new_id){
                    $result = true;
                }else{
                    $result = false;
                }
            }
            if ($result && $this->currentItem->status == 'create') {
                $this->createCourseSectionOnFarayad($moodleCourse);
            }

            if ($result && $this->currentItem->status == 'section') {
                if ($moodleCourse->format == 'farayad') {
                    $this->createCourseActivityByFarayadFormat($moodleCourse);
                } else {
                    $this->createCourseActivityByButtonFormat($moodleCourse);
                }
            }

            if ($result && $this->currentItem->status == 'activity') {
                $this->createCourseEnrollmentOnFarayad($moodleCourse);
            }
            echo "complete Course_Id ".$moodleCourse->id." \r\n";


        } else {
            $this->addLog($this->currentItem, 'error', 'Not Find Moodle Course Or Wordpress Course.');
        }

    }

    public function createTransferQueue()
    {
        $courses = $this->getMoodleDb()->select("SELECT id FROM `mdl_course` WHERE `category` = 48 AND `visible` = 1 and format IN ('buttons','farayad') ORDER BY `id` ASC");
        foreach ($courses as $course) {
            $data = [
                'type' => 'course',
                'last_id' => $course->id,
                'status' => 'start',
            ];
            $result = $this->addToTransferTable($data);
            if ($result) {
                echo 'Success: course (' . $course->id . ') add to transfer table.' . "\r\n";
            } else {
                echo 'Error: course (' . $course->id . ') can not add to transfer table.' . "\r\n";
            }
        }
    }

    public function moveCourses()
    {
        try {
            $transfersQueues = $this->getCourseQueue();
            if ($transfersQueues) {
                foreach ($transfersQueues as $transfersItem) {
                    $this->currentItem = $this->getTransferObject($transfersItem);
                    //if ($transfersItem->last_id == 116) {
                        $this->makeNewCourse();
                   // }
                }
            }
        } catch (\Exception $e) {
            report($e);
            return false;
        }
    }


    private function createCourseOnFarayad($moodleCourse, $wordpressCourse)
    {
        DB::beginTransaction();
        try {
            $course = new Course();
            $course->instance_id = $this->getInstance()->id;
            $course->language_id = $this->getLanguage()->id;
            $course->category_id = 1;
            $course->title = $wordpressCourse->post_title;
            $course->code = $moodleCourse->idnumber;
            $course->short_description = strip_tags($moodleCourse->summary);
            $course->description = nl2br($wordpressCourse->post_content);
            $course->type = 'normal';
            $course->image = null;
            $course->quantity = 0;
            if ($moodleCourse->format == 'buttons') {
                $course->params = '{"access_comment":"only_user","confirm_comment":"by_admin","access_rate":"only_user","show_section_title":"disable","show_sections":"disable","view_status_sections":"first_open"}';
            } else {
                $course->params = '{"access_comment":"only_user","confirm_comment":"by_admin","access_rate":"only_user","show_section_title":"enable","show_sections":"enable","view_status_sections":"first_open"}';
            }
            $course->view_count = 0;
            $course->active = 1;
            $course->save();
            $insertItem = ['course_id' => $course->id, 'category_id' => 1];
            $courseCR = new CourseCategoryRelation();
            $result = $courseCR->insert($insertItem);
            $this->currentItem->new_id = $course->id;
            $this->currentItem->status = 'create';
            $this->addLog($this->currentItem, 'success', 'add course success.');
            $this->updateTransferItem($this->currentItem);
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            $this->addLog($this->currentItem, 'error', 'add course failed.');
            echo $e->getMessage() . "\r\n";
            return false;
        }

    }

    public function createCourseSectionOnFarayad($moodleCourse)
    {
        DB::beginTransaction();
        try {
            $sections = [];
            if ($moodleCourse->format == 'buttons') {
                $sections[] = [
                    'id' => 1,
                    'title' => 'section1',
                    'sort' => 1,
                ];
            } else {
                $moodleSections = $this->getMoodleDb()->select("SELECT `id`,`section`,`name`  FROM `mdl_course_sections` WHERE `course` = '" . $moodleCourse->id . "'  
ORDER BY `mdl_course_sections`.`section`  ASC");
                foreach ($moodleSections as $moodleSection) {
                    if (strlen($moodleSection->name)) {
                        $sections[] = [
                            'id' => $moodleSection->id,
                            'title' => $moodleSection->name,
                            'sort' => $moodleSection->section,
                        ];
                    }
                }
            }
            $this->currentItem->data['sections'] = [];
            foreach ($sections as $section) {
                $courseSection = new CourseSection;
                $courseSection->course_id = $this->currentItem->new_id;
                $courseSection->title = $section['title'];
                $courseSection->sort = $section['sort'];
                $courseSection->active = 1;
                $courseSection->save();
                $this->currentItem->data['sections'][$section['id']] = $courseSection->id;
            }
            $this->currentItem->status = 'section';
            $this->addLog($this->currentItem, 'success', 'add section success.');
            $this->updateTransferItem($this->currentItem);
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            $this->addLog($this->currentItem, 'error', 'add Section failed.');
            echo $e->getMessage() . "\r\n";
            return false;
        }
    }


    public function createCourseActivityByFarayadFormat($moodleCourse)
    {
        $moodleSections = $this->getMoodleDb()->select("SELECT id,sequence  FROM `mdl_course_sections` WHERE `course` = '" . $moodleCourse->id . "' AND `visible`=1 ORDER BY `mdl_course_sections`.`section`  ASC");
        $moodleModules = $this->getMoodleDb()->select("SELECT `id`,`instance`,`section`  FROM `mdl_course_modules` WHERE `course` = '" . $moodleCourse->id . "' AND `module`=15 AND `visible`=1 AND `deletioninprogress`=0");
        $moduleList = [];
        foreach ($moodleModules as $module) {
            $moduleList[$module->id] = $module;
        }
        DB::beginTransaction();
        $this->currentItem->data['activity'] = [];
        try {
            $sort = 1;
            foreach ($moodleSections as $sectionActivity) {
                $allActivities = explode(',', $sectionActivity->sequence);
                if (!isset($this->currentItem->data['sections'][$sectionActivity->id])) {
                    continue;
                }
                $section_id = $this->currentItem->data['sections'][$sectionActivity->id];
                foreach ($allActivities as $moduleActivity) {
                    if (!empty($moduleActivity) && isset($moduleList[$moduleActivity])) {
                        $module = $moduleList[$moduleActivity];
                        $page = $this->getFirst($this->getMoodleDb()->select("SELECT * FROM `mdl_page` WHERE `course`='" . $moodleCourse->id . "' AND `id`='" . $module->instance . "'"));
                        if ($page) {
                            if ($videos = $this->filter($page->content)) {
                                if (is_array($videos) && count($videos)) {
                                    foreach ($videos as $video) {
                                        $videoRecord = $this->getFirst($this->getMoodleDb()->select("SELECT * FROM `mdl_repository_nimble` where `file_hash`='" . $video['hash'] . "'"));
                                        if ($videoRecord) {
                                            /*add video record*/
                                            $videoModel = $this->addVideoRecord($videoRecord, $video);
                                            if ($videoModel) {
                                                /*add activity*/
                                                $newActivity = new CourseActivity;
                                                $newActivity->title = $page->name;
                                                $newActivity->description = null;
                                                $newActivity->type = 'video';
                                                $newActivity->course_id = $this->currentItem->new_id;
                                                $newActivity->course_section_id = $section_id;
                                                $newActivity->active_date = null;
                                                $newActivity->expire_date = null;
                                                $newActivity->is_public = false;
                                                $newActivity->show_type = 'pay';
                                                $newActivity->single_sales = 'disabled';
                                                $newActivity->duration = $videoModel->video_time_minutes;
                                                $newActivity->params = json_encode(['video_id' => $videoModel->id]);
                                                $newActivity->sort = $sort;
                                                $sort = $sort + 1;
                                                $newActivity->active = 1;
                                                $newActivity->save();
                                                $this->currentItem->data['activity'][$module->id] = ['module' => $module->id, 'page' => $module->instance, 'new_id' => $newActivity->id];
                                                $this->addVideoUseRecord($newActivity, $videoModel, $video);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            echo "complete \r\n";
            DB::commit();
            $this->currentItem->status = 'activity';
            $this->addLog($this->currentItem, 'success', 'add activity success.');
            $this->updateTransferItem($this->currentItem);
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            $this->addLog($this->currentItem, 'error', 'add Activity failed.');
            echo $e->getMessage() . "\r\n";
            return false;
        }
    }


    public function createCourseActivityByButtonFormat($moodleCourse)
    {
        $moodleSections = $this->getMoodleDb()->select("SELECT *  FROM `mdl_course_sections` WHERE `course` = '" . $moodleCourse->id . "' AND `visible`=1 ORDER BY `mdl_course_sections`.`section`  ASC");
        DB::beginTransaction();
        try {
            $sort = 1;
            $this->currentItem->data['activity'] = [];
            foreach ($moodleSections as $sectionActivity) {
                if ($videos = $this->filter($sectionActivity->summary)) {
                    if (is_array($videos) && count($videos)) {
                        $section_id = $this->currentItem->data['sections'][1];
                        foreach ($videos as $video) {
                            $videoRecord = $this->getFirst($this->getMoodleDb()->select("SELECT * FROM `mdl_repository_nimble` where `file_hash`='" . $video['hash'] . "'"));
                            if ($videoRecord) {
                                /*add video record*/
                                $videoModel = $this->addVideoRecord($videoRecord, $video);
                                if ($videoModel) {
                                    /*add activity*/
                                    $newActivity = new CourseActivity;
                                    $newActivity->title = $sectionActivity->name;
                                    $newActivity->description = null;
                                    $newActivity->type = 'video';
                                    $newActivity->course_id = $this->currentItem->new_id;
                                    $newActivity->course_section_id = $section_id;
                                    $newActivity->active_date = null;
                                    $newActivity->expire_date = null;
                                    $newActivity->is_public = false;
                                    $newActivity->show_type = 'pay';
                                    $newActivity->single_sales = 'disabled';
                                    $newActivity->duration = $videoModel->video_time_minutes;
                                    $newActivity->params = json_encode(['video_id' => $videoModel->id]);
                                    $newActivity->sort = $sort;
                                    $sort = $sort + 1;
                                    $newActivity->active = 1;
                                    $newActivity->save();
                                    $this->currentItem->data['activity'][] = ['module' => $sectionActivity->id, 'section' => $sectionActivity->id, 'new_id' => $newActivity->id];
                                    $this->addVideoUseRecord($newActivity, $videoModel, $video);
                                }
                            }
                        }
                    }
                }
            }
            $this->currentItem->status = 'activity';
            $this->addLog($this->currentItem, 'success', 'add activity success.');
            $this->updateTransferItem($this->currentItem);
            DB::commit();
            return true;
        } catch (\Exception $e) {
            report($e);
            $this->addLog($this->currentItem, 'error', 'add Activity failed.');
            echo $e->getMessage() . "\r\n";
            DB::rollBack();
            return false;
        }
    }


    public function createCourseEnrollmentOnFarayad($moodleCourse)
    {
        $moodleEnrollments = $this->getMoodleDb()->select("SELECT * FROM `mdl_enrol` WHERE `courseid` = '" . $moodleCourse->id . "' AND `status`=0");
        $enrollments=$this->getAvailableEnrollment();
        $currencies=$this->getAvailableCurrencies();
        DB::beginTransaction();
        try {
            $this->currentItem->data['enrollments'] = [];
            if(count($moodleEnrollments)) {
                foreach($moodleEnrollments as $moodleEnrollment){
                    if(isset($enrollments[$moodleEnrollment->enrol])){
                        $enrollmentModel=$enrollments[$moodleEnrollment->enrol];
                        $newEnrollment=new CourseEnrollment;
                        $newEnrollment->course_id=$this->currentItem->new_id;
                        $newEnrollment->enrollment_id=$enrollmentModel->id;
                        $newEnrollment->role_id=2;
                        if($moodleEnrollment->enrol=='onlinepay'){
                            $currency_id=2;
                            if(isset($currencies[$moodleEnrollment->currency])){
                                $currency_id=$currencies[$moodleEnrollment->currency]->id;
                            }
                            $newEnrollment->params=json_encode([
                                "sale_cost"=>$moodleEnrollment->cost,
                                "cost"=>$moodleEnrollment->cost,
                                "currency_id"=>$currency_id,
                                "type_tax"=>null
                            ]);
                        }
                        $newEnrollment->save();
                        $this->currentItem->data['enrollments'][$moodleEnrollment->id]=$newEnrollment->id;

                    }
                }
            }
            $this->currentItem->status = 'enrollment';
            $this->addLog($this->currentItem, 'success', 'add enrollment success.');
            $this->updateTransferItem($this->currentItem);
            DB::commit();
        } catch (\Exception $e) {
            report($e);
            $this->addLog($this->currentItem, 'error', 'add enrollment failed.');
            echo $e->getMessage() . "\r\n";
            DB::rollBack();
            return false;
        }
        return true;
    }


    public function getAvailableEnrollment(){
        if($this->enrllsWays){
            return $this->enrllsWays;
        }
        $Enrollments=Enrollment::all();
        $this->enrllsWays=[];
        foreach($Enrollments as $enrollment){
            if($enrollment->alias=='payment'){
                $this->enrllsWays['onlinepay']=$enrollment;
            }elseif($enrollment->alias=='free'){
                $this->enrllsWays['self']=$enrollment;
            }
        }
        return $this->enrllsWays;
    }


    public function getAvailableCurrencies(){
        if($this->currencies){
            return $this->currencies;
        }
        $currencies=Currency::all();
        $this->currencies=[];
        foreach($currencies as $currency){
            $this->currencies[$currency->code]=$currency;
        }
        return $this->currencies;
    }


    public function __destruct()
    {
        if ($this->currentItem) {
            $this->updateTransferItem($this->currentItem);
        }
    }
}