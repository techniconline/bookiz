<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 10/10/18
 * Time: 2:54 PM
 */

namespace App\Console\Commands\Moodle;
use Illuminate\Support\Facades\DB;

use Modules\Core\Models\Instance\Instance;
use Modules\Core\Models\Language\Language;

class Master
{
    private $instance=false;
    private $language=false;

    public function getMoodleDb(){
        return DB::connection('moodle');
    }

    public function getFirst($rows){
        if($rows){
            if(is_array($rows)){
                return $rows[0];
            }
        }
        return $rows;
    }


    public function getWordpressDb(){
        return DB::connection('wordpress');
    }



    public function getCourseQueue(){
       return $this->getMoodleDb()->select("SELECT * FROM `faryad_transfer` WHERE `type` = 'course' Order By `id` ASC");
    }


    public function getUserQueue(){
        return $this->getMoodleDb()->select("SELECT * FROM `faryad_transfer` WHERE `type` = 'user' And `status`!='finished' Order By `id` ASC");
    }



    public function getTransferObject($item){
        if($item->files){
            $item->files=json_decode($item->files,true);
        }else{
            $item->files=[];
        }
        if($item->data){
            $item->data=json_decode($item->data,true);
        }else{
            $item->data=[];
        }
        if($item->messages){
            $item->messages=json_decode($item->messages,true);
        }else{
            $item->messages=[];
        }
        return $item;
    }



    public function addLog($item,$type,$message){
        $item->messages[]=['type'=>$type,'message'=>$message];
    }


    public function updateTransferItem($item){
        $data=[
            'new_id'=>$item->new_id,
            'status'=>$item->status,
            'files'=>json_encode($item->files),
            'data'=>json_encode($item->data),
            'messages'=>json_encode($item->messages)
        ];
        return $this->getMoodleDb()->table('faryad_transfer')
            ->where('id', $item->id)
            ->update($data);
    }

    public function addToTransferTable($data){
        return $this->getMoodleDb()->table('faryad_transfer')->insert($data);
    }



    public function getInstance(){
        if($this->instance){
            return $this->instance;
        }
        $this->instance=Instance::where('default',1)->first();
        return $this->instance;
    }


    public function getLanguage(){
        if($this->language){
            return $this->language;
        }
        $this->language=Language::where('code','fa')->first();
        return $this->language;
    }




    public function getFileList($run=1){
        $trensferItem=$this->getFirst($this->getMoodleDb()->select("SELECT * FROM `faryad_transfer` WHERE `type` = 'course' AND `status`='enrollment' Order By `id` ASC limit 0,2"));
        $files=['files'=>null,'id'=>0];
        if($trensferItem){
            $trensferItem=$this->getTransferObject($trensferItem);
            if(count($trensferItem->files)){
                $transferedList=isset($trensferItem->data['files'])?$trensferItem->data['files']:[];
                $files=['files'=>[],'id'=>$trensferItem->id];
                foreach($trensferItem->files as $file){
                    if(!in_array($file['hash'],$transferedList)){
                        $files['files'][]=$file;
                    }
                }
                if(count($files['files'])){
                    $filescount=count($transferedList);
                    if($filescount > 0){
                        $fileMid=(int)($filescount/2);
                        if(count($files['files']) < $fileMid){
                            $trensferItem->status='file';
                            $this->addLog($trensferItem, 'success', 'add Files success.');
                            $this->updateTransferItem($trensferItem);
                        }
                    }
                    return $files;
                }else{
                    $trensferItem->status='file';
                    $this->addLog($trensferItem, 'success', 'add Files success.');
                    $this->updateTransferItem($trensferItem);
                }
            }else{
                $trensferItem->status='file';
                $this->addLog($trensferItem, 'success', 'add Files success.');
                $this->updateTransferItem($trensferItem);
            }
            return $this->getFileList($run+1);
        }
        return $files;
    }



    public function saveFileOnList($id,$hash){
        $response=['result'=>false];
        $trensferItem=$this->getFirst($this->getMoodleDb()->select("SELECT * FROM `faryad_transfer` WHERE `type` = 'course' AND `status`='enrollment' And `id`='".$id."'"));
        if($trensferItem) {
            $trensferItem = $this->getTransferObject($trensferItem);
            if(!isset($trensferItem->data['files'])){
                $trensferItem->data['files']=[];
            }
            if(!in_array($hash,$trensferItem->data['files'])){
                $trensferItem->data['files'][]=$hash;
                $this->updateTransferItem($trensferItem);
            }
            $response=['result'=>true];
        }
        return $response;
    }

    public function getPosterList(){
        $trensferItems=$this->getMoodleDb()->select("SELECT * FROM `faryad_transfer` WHERE `type` = 'course' AND `status`='file' Order By `id`");
        $files=['files'=>null,'id'=>1];
        if(count($trensferItems)){
            foreach ($trensferItems as $trensferItem){
                $trensferItem = $this->getTransferObject($trensferItem);
                if(count($trensferItem->files)){
                    foreach($trensferItem->files as $file){
                        if($file['has_poster']){
                            $files['files'][]=$file;
                        }
                    }
                }
            }
            if(count($files['files'])){
                return $files;
            }
        }
        return $files;
    }

}