<?php
/**
 * Created by PhpStorm.
 * User: hemmaty
 * Date: 10/16/18
 * Time: 11:30 AM
 */
namespace App\Console\Commands\Moodle;


Trait TextAnalyzor
{


    public function filter($text, array $options = array()) {

        if (!is_string($text) or empty($text)) {
            // non string data can not be filtered anyway
            return null;
        }

        if (stripos($text, '</a>') === false) {
            // Performance shortcut - if not </a> tag, nothing can match.
            return null;
        }


        // Looking for tags.
        $matches = preg_split('/(<[^>]*>)/i', $text, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

        if (!$matches) {
            return  null;
        }

        // Regex to find media extensions in an <a> tag.
        $re = '~<a\s[^>]*href="([^"]*(?:\/hls\/)[^"]*)"[^>]*>~is';

        // We iterate through the given string to find valid <a> tags
        // and build them so that the callback function can check it for
        // embedded content. Then we rebuild the string.
        $videoHashList=[];
        foreach ($matches as $idx => $tag) {
            $newtext = null;
            if (stripos($tag, '<a') !== false) {
                $processed = preg_replace_callback($re, array($this, 'callback'), $tag);
                if(!empty($processed)){
                    $videoHashList[]=['type'=>'video','hash'=>$processed,'use'=>sha1($processed.time().str_random(10))];
                }
            }
        }
        // Return the same string except processed by the above.

        return $videoHashList;

    }


    /**
     * Replace link with embedded content, if supported.
     *
     * @param array $matches
     * @return string
     */
    private function callback(array $matches) {
        // Check if we ignore it.
        if (preg_match('/class="[^"]*nomediaplugin/i', $matches[0])) {
            return null;
        }

        $hash=null;
        $extractor="/(\/hls\/+)(?<key>[a-z0-9-]+)(\/+)/";
        if(preg_match($extractor,$matches[1],$extracts)){
            $hash=$extracts['key'];
        }
        if($hash){
            return $hash;
        }
        return null;

    }




}